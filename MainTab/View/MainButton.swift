//
//  MainButton.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

protocol Main_Button_Delegate {
    func setMain(mode:MainMode)
}
class Main_Button : UIView {
    var delegate: Main_Button_Delegate?
    var mode : MainMode = .Product {
        didSet{
            switch mode {
            case .Main :
                
                imageview.image = #imageLiteral(resourceName: "ic_home")
                
            case .Product:
                
                imageview.image = #imageLiteral(resourceName: "ic_sales")
               
            case .Customer:
                imageview.image = #imageLiteral(resourceName: "ic_business")
               
            case .Service:
                imageview.image = #imageLiteral(resourceName: "ic_service")
                
            }
        }
    }
    let imageview = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        //tintColor = .clear
        layer.cornerRadius = 18.calcvaluex()
        backgroundColor = .clear
        imageview.contentMode = .scaleAspectFill
        
        addSubview(imageview)
        imageview.centerInSuperview(size: .init(width: 18.calcvaluex(), height: 18.calcvaluex()))
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setTap)))
       // isDisable()
    }
    @objc func setTap(){
        delegate?.setMain(mode: self.mode)
    }
    func setSelected(){
        backgroundColor = MajorColor().oceanSubColor
    }
    func setUnSelected() {
        backgroundColor = .clear
    }
    func isDisable(){
        self.isUserInteractionEnabled = false
        self.imageview.tintColor = #colorLiteral(red: 0.8847324354, green: 0.8847324354, blue: 0.8847324354, alpha: 1)
    }
    
    func isEnable(){
        self.isUserInteractionEnabled = true
        self.imageview.tintColor = .clear
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

