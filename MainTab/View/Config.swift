//
//  Config.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
enum MainMode {
    case Main
    case Product
    case Customer
    case Service
}
enum SubMode : Int{
    case News = 0
    case Products = 1
    case Application = 2
    
    case Customers = 3
    case Interview = 4
    case Files = 5
    
    case Product_Management = 6
    case Fix = 7
    case Schedule = 8
    case FAQ = 9
}
