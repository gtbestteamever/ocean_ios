//
//  SubButton.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class CustomView : UIView{
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let frame = self.bounds.insetBy(dx: -5.calcvaluex(), dy: -30.calcvaluey());
            return frame.contains(point) ? self : nil;
    }
}
protocol Sub_MenuButton_Delegate {
    func setSubMenu(mode:SubMode)
}
class Sub_MenuButton : CustomView {
    var delegate : Sub_MenuButton_Delegate?
    let imageview = UIImageView()
    var mode = SubMode.News {
        didSet{
            switch mode {
            case .News :
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "最新消息".localized
            case .Products:
                imageview.image = #imageLiteral(resourceName: "ic_product")
                label.text = "產品介紹".localized
            case .Application:
                imageview.image = #imageLiteral(resourceName: "ic_application")
                label.text = "應用產業".localized
            case .Customers:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "客戶管理".localized
            case .Interview:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "訪談管理".localized
            case .Files:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "業務檔案".localized
            case .Product_Management:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "產品管理".localized
            case .Fix:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "報修服務".localized
            case .Schedule:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "工作排程".localized
            case .FAQ:
                imageview.image = #imageLiteral(resourceName: "ic_news")
                label.text = "常見問題".localized
                
            }
        }
    }
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.3989400268, green: 0.4003421366, blue: 0.7029099464, alpha: 1)
        constrainHeight(constant: 36.calcvaluey())
        layer.cornerRadius = 18.calcvaluey()
        
        addSubview(imageview)
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0),size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        
        addSubview(label)
        label.textColor = .white
        label.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 20.calcvaluex()))
        label.centerYInSuperview()
        label.font = UIFont(name: MainFont().Regular, size: 14.calcvaluex())
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getSelection)))
        
    }
    @objc func getSelection(){
        delegate?.setSubMenu(mode: self.mode)
    }
    func setSelected() {
        backgroundColor = MajorColor().oceanSubColor
    }
    func unSelected(){
        backgroundColor = #colorLiteral(red: 0.3989400268, green: 0.4003421366, blue: 0.7029099464, alpha: 1)
    }
    func isDisable(){
        backgroundColor = #colorLiteral(red: 0.9215381759, green: 0.9215381759, blue: 0.9215381759, alpha: 1)
        isUserInteractionEnabled = false
    }
    func isEnable(){
        backgroundColor = #colorLiteral(red: 0.3989400268, green: 0.4003421366, blue: 0.7029099464, alpha: 1)
        isUserInteractionEnabled = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
