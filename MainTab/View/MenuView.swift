//
//  MenuView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol MenuViewDelegate {
    func setView(mode:SubMode)
}
class MenuView : UIView,Main_Button_Delegate,Sub_MenuButton_Delegate {
    func setSubMenu(mode: SubMode) {
        self.delegate?.setView(mode: mode)
    }
    
    func setMain(mode: MainMode) {
        if mode != mainMode {
            self.mainMode = mode
            
            for i in menuStackView.arrangedSubviews {
                if let vs = i as? Main_Button {
                    if vs.mode == self.mainMode {
                        vs.setSelected()
                    }
                    else{
                        vs.setUnSelected()
                    }
                }
            }
            
            var subMenu = [SubMode]()
            
            if self.mainMode == .Product {
                subMenu = [.News,.Products,.Application]
            }
            else if self.mainMode == .Customer {
                subMenu = [.Customers,.Interview,.Files]
            }
            else if self.mainMode == .Service {
                subMenu = [.Product_Management,.Fix,.Schedule,.FAQ]
                
            }
            setSubMenu(modes: subMenu)
        }
    }
    var mainMode = MainMode.Product
    var menuStackView = Horizontal_Stackview( spacing: 10.calcvaluex(), alignment: .center)
    var subMenuStackView = Horizontal_Stackview(spacing:10.calcvaluex(),alignment: .center)
    var delegate : MenuViewDelegate?
    var mode : SubMode? = .News
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MajorColor().oceanColor
        layer.cornerRadius = 15.calcvaluex()
        
        addSubview(menuStackView)
        menuStackView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 22.calcvaluex(), bottom: 0, right: 0))
        addSubview(subMenuStackView)
        subMenuStackView.anchor(top: topAnchor, leading: menuStackView.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 14.calcvaluex(), bottom: 0, right: 0))
        for i in [MainMode.Main,.Product,.Customer,.Service] {
            let button = Main_Button()
            button.delegate = self
            button.mode = i
            if mainMode == button.mode {
                button.setSelected()
            }


            button.constrainWidth(constant: 36.calcvaluex())
            button.constrainHeight(constant: 36.calcvaluex())

            menuStackView.addArrangedSubview(button)
        }
        
        setSubMenu(modes: [.News,.Products,.Application])
        
    }
    func setSubMenu(modes: [SubMode]) {
        subMenuStackView.safelyRemoveArrangedSubviews()
        for i in modes {
            let button = Sub_MenuButton()
            button.delegate = self
            if i == self.mode {
                button.setSelected()
            }
            button.mode = i
            subMenuStackView.addArrangedSubview(button)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
