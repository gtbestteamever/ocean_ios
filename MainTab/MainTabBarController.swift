//
//  MainTabBarController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
//extension Mor
var menu_bottomInset = 71.calcvaluey()

class MainTabBarController: UITabBarController,MenuViewDelegate,UITabBarControllerDelegate{
    var currentIndex = 0
    func setView(mode: SubMode) {
        print(221,currentIndex,mode.rawValue)
        if currentIndex == mode.rawValue {
            return
        }
        self.menuview.mode = mode
        currentIndex = mode.rawValue
        var vd : UIViewController?

        switch mode {
        case .News:
            vd = NewsController()
        case .Products:
            vd = OceanProductController()
        case .Application:
            vd = BussinessController()
        case .Customers:
            vd = SalesManagementController()
        case .Interview:
            vd = PriceManagementController()
        case .Files:
            vd = SalesFileManagementController()
        case .Product_Management:
            vd = ProductManagementController()
        case .Fix:
            vd = MaintainController()
        case .Schedule:
            vd = ScheduleWorkController()
        case .FAQ:
            vd = FrequentQuestionController()
        }
        
        viewControllers?[mode.rawValue] = customNavigationController(rootViewController: vd ?? UIViewController())
        for i in menuview.subMenuStackView.arrangedSubviews {
            if let vs = i as? Sub_MenuButton {
                if vs.mode == mode {
                    vs.setSelected()
                }
                else{
                    vs.unSelected()
                }
            }
        }
        
        animateToTab(toIndex: mode.rawValue)
        
        
           
            
            
        
       
        
    }
    func animateToTab(toIndex: Int) {
          /*  guard let tabViewControllers = viewControllers,
                let selectedVC = selectedViewController else { return }

            guard let fromView = selectedVC.view,
                let toView = tabViewControllers[toIndex].view,
                let fromIndex = tabViewControllers.firstIndex(of: selectedVC),
                fromIndex != toIndex else { return }


            // Add the toView to the tab bar view
            fromView.superview?.addSubview(toView)

            // Position toView off screen (to the left/right of fromView)
            let screenWidth = UIScreen.main.bounds.size.width
            let scrollRight = toIndex > fromIndex
            let offset = (scrollRight ? screenWidth : -screenWidth)
            toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)

            // Disable interaction during animation
            view.isUserInteractionEnabled = false

        UIView.animate(withDuration: 0.8,
                           delay: 0.0,
                           usingSpringWithDamping: 1,
                           initialSpringVelocity: 0,
                           options: .curveEaseOut,
                           animations: {
                            // Slide the views by -offset
                            fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                            toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)*/

          //  }, completion: { finished in
                // Remove the old view from the tabbar view.
               // fromView.removeFromSuperview()
                self.selectedIndex = toIndex
               // self.view.isUserInteractionEnabled = true
           //})
        }

    let menuview = MenuView()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(changeLang), name: Notification.Name(rawValue : "switchLanguage"), object: nil)
    }
    @objc func changeLang(){
        for (index,_) in menuview.subMenuStackView.arrangedSubviews.enumerated() {
            if let vd = menuview.subMenuStackView.arrangedSubviews[index] as? Sub_MenuButton {
                vd.label.text = vd.label.text?.localized
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moreNavigationController.navigationBar.isHidden = true
        
        view.backgroundColor = .white
        addStatusColor()
        self.tabBar.isHidden = true
        
        view.addSubview(menuview)
        menuview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 18.calcvaluey(), right: 18.calcvaluex()),size: .init(width: 0, height: 53.calcvaluey()))
        menuview.delegate = self
        var vv = [UIViewController]()
        for i in [NewsController(),OceanProductController(),BussinessController(),SalesManagementController(),PriceManagementController(),SalesFileManagementController(),ProductManagementController(),MaintainController(),ScheduleWorkController(),FrequentQuestionController()]
        {
            let nav = customNavigationController(rootViewController: i)
            vv.append(nav)
        }
        viewControllers = vv
        self.delegate = self
        setUp()
    }
    func getUserData(id:String, completion:@escaping (UserD?) -> Void){
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/\(id)", decoderType: UserData.self) { (json) in
            if let json = json{
            do{
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveUserData(data: encode, id: id)
                completion(json.data)
                if UserDefaults.standard.getChosenCurrency() == "" {
                    UserDefaults.standard.saveChosenCurrency(text: json.data.currencies.first?.keys.first ?? "")
                }
                else{
                    if !(json.data.currencies.first?.contains(where: { (key,val) -> Bool in
                        return key == UserDefaults.standard.getChosenCurrency()
                    }) ?? false) {
                        UserDefaults.standard.saveChosenCurrency(text: json.data.currencies.first?.first?.key ?? "")
                    }
                }
                
                if UserDefaults.standard.getChosenGroup() == "" {
                    UserDefaults.standard.saveChosenGroup(text: json.data.currency_groups.first?.keys.first ?? "")
                }
                else{
                    if !(json.data.currency_groups.first?.contains(where: { (key,val) -> Bool in
                        return key == UserDefaults.standard.getChosenGroup()
                    }) ?? false) {
                        UserDefaults.standard.saveChosenCurrency(text: json.data.currency_groups.first?.first?.key ?? "")
                    }
                }
            }
            catch{
                
            }
            }
        }
        
    }
    func setAllTrue(){
        for (index, i) in self.menuview.menuStackView.arrangedSubviews.enumerated() {
            if let tt = self.menuview.menuStackView.arrangedSubviews[index] as? Main_Button {
                tt.isEnable()
            }
        }
        for (index, i) in self.menuview.subMenuStackView.arrangedSubviews.enumerated() {
            if let ss = self.menuview.subMenuStackView.arrangedSubviews[index] as? Sub_MenuButton {
                ss.isEnable()
            }
        }
    }
    var permission = [Permission]() {
        didSet{
            
                if permission.contains(where: { (pr) -> Bool in
                    return pr.slug == "auth.app.all"
                }) {
                    self.setAllTrue()
                    return
                }
            
//            for i in mainbuttons {
// 
//                if permission.contains(where: { (st) -> Bool in
//                    return st.slug == i.slug
//                }) {
//                    i.imageview?.isUserInteractionEnabled = true
//                    i.imageview?.alpha = 1
//                }
//                else{
//                    i.imageview?.isUserInteractionEnabled = false
//                    i.imageview?.alpha = 0.5
//                }
//            }
//            
//            for i in newbuttons {
//                
//                for j in i{
//                    
//                    if permission.contains(where: { (st) -> Bool in
//                        return st.slug == j.slug
//                    }) {
//
//                        j.imageview?.isUserInteractionEnabled = true
//                        j.imageview?.alpha = 1
//                    }
//                    else{
//                        j.imageview?.isUserInteractionEnabled = false
//                        j.imageview?.alpha = 0.5
//                    }
//                }
//            }
            
           

            
        }
    }
    func setUp(){
        
        getUserData(id: UserDefaults.standard.getUserId()) { (user) in
            DispatchQueue.main.async {
                if let user = user{
                    self.permission = user.permissions
                    

                }
            }

        }
        getSetting()
        let id = UserDefaults.standard.getUserId()
        let lang = UserDefaults.standard.getConvertedLanguage()
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/\(id)?language=\(lang)", decoderType: UserData.self) { json in
            //
        }
    }
    func getSetting(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/setting", decoderType: SettingModel.self) { (json) in
            if let json = json?.data, let encode = try? JSONEncoder().encode(json) {
                
                UserDefaults.standard.saveSettingData(data: encode)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

class N_ViewController : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
