//
//  iPhoneMainTabBarController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/19/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit
protocol iPhoneMainTabDelegate {
    func goController(index:Int)
}
class iPhoneMainTabButton : UIView {
    let imageview = UIImageView()
    let label = UILabel()
    var delegate : iPhoneMainTabDelegate?
    init(image:UIImage,text:String) {
        super.init(frame: .zero)
        addSubview(imageview)
        imageview.image = image.withRenderingMode(.alwaysTemplate)
        imageview.contentMode = .scaleAspectFit
        
        imageview.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        imageview.centerXInSuperview()
        
        label.font = UIFont(name: MainFont().Regular, size: 14.calcvaluex())
        
        addSubview(label)
        label.textAlignment = .center
        label.numberOfLines = 2
        
        label.text = text
        label.anchor(top: imageview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 4.calcvaluex()))
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goController)))
    }
    
    func didSelect(){
        imageview.tintColor = .white
        
        label.textColor = .white
        
        backgroundColor = MajorColor().oceanColor
    }
    func unSelect(){
        imageview.tintColor = MajorColor().oceanSubColor
        label.textColor = .black
        backgroundColor = .white
    }
    @objc func goController(){
        self.delegate?.goController(index: self.tag)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class iPhoneMainBar : UIView {
    let stackview = Horizontal_Stackview(distribution:.fillEqually)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        addshadowColor()
        
        addSubview(stackview)
        stackview.fillSuperview()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class iPhoneMainTabBarController: UITabBarController,iPhoneMainTabDelegate {
    func goController(index: Int) {
        selectedIndex = index
        for i in vd.stackview.arrangedSubviews {
            if let vv = i as? iPhoneMainTabButton {
                if vv.tag == index {
                    vv.didSelect()
                }
                else{
                    vv.unSelect()
                }
            }
        }
        
    }
    
    
    let vd = iPhoneMainBar()
    let newsButton = iPhoneMainTabButton(image: #imageLiteral(resourceName: "ic_news"),text: "最新消息".localized)
    let productButton = iPhoneMainTabButton(image: #imageLiteral(resourceName: "ic_info"), text: "產品管理".localized)
    let fixButton = iPhoneMainTabButton(image: #imageLiteral(resourceName: "ic_service_normal"), text: "報修服務".localized)
    let faqButton = iPhoneMainTabButton(image: #imageLiteral(resourceName: "ic_faq_normal"), text: "常見問題".localized)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.moreNavigationController.navigationBar.isHidden = true
        
        view.backgroundColor = .white
        addStatusColor()
        self.tabBar.isHidden = true

        view.addSubview(vd)

            vd.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 71.calcvaluey()))
        let vc = [iPhoneNewsController(),iPhoneProductManagementController(),iPhoneMaintainController(),iPhoneFrequentQuestionController()]
        self.viewControllers = vc
        for (index,i) in [newsButton,productButton,fixButton,faqButton].enumerated(){
            i.tag = index
            i.delegate = self
            if index == selectedIndex {
                i.didSelect()
            }
            else{
                i.unSelect()
            }
            vd.stackview.addArrangedSubview(i)
            
            
        }

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUp()
    }
    func getUserData(id:String, completion:@escaping (UserD?) -> Void){
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/\(id)", decoderType: UserData.self) { (json) in
            if let json = json{
            do{
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveUserData(data: encode, id: id)
                completion(json.data)
                if UserDefaults.standard.getChosenCurrency() == "" {
                    UserDefaults.standard.saveChosenCurrency(text: json.data.currencies.first?.keys.first ?? "")
                }
                else{
                    if !(json.data.currencies.first?.contains(where: { (key,val) -> Bool in
                        return key == UserDefaults.standard.getChosenCurrency()
                    }) ?? false) {
                        UserDefaults.standard.saveChosenCurrency(text: json.data.currencies.first?.first?.key ?? "")
                    }
                }
                
                if UserDefaults.standard.getChosenGroup() == "" {
                    UserDefaults.standard.saveChosenGroup(text: json.data.currency_groups.first?.keys.first ?? "")
                }
                else{
                    if !(json.data.currency_groups.first?.contains(where: { (key,val) -> Bool in
                        return key == UserDefaults.standard.getChosenGroup()
                    }) ?? false) {
                        UserDefaults.standard.saveChosenCurrency(text: json.data.currency_groups.first?.first?.key ?? "")
                    }
                }
            }
            catch{
                
            }
            }
        }
        
    }
    func setUp(){
        
        getUserData(id: UserDefaults.standard.getUserId()) { (user) in
            DispatchQueue.main.async {
                if let user = user{
                    //self.permission = user.permissions
                    

                }
            }

        }
        getSetting()
        let id = UserDefaults.standard.getUserId()
        let lang = UserDefaults.standard.getConvertedLanguage()
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/\(id)?language=\(lang)", decoderType: UserData.self) { json in
            //
        }
    }
    func getSetting(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/setting", decoderType: SettingModel.self) { (json) in
            if let json = json?.data, let encode = try? JSONEncoder().encode(json) {
                
                UserDefaults.standard.saveSettingData(data: encode)
            }
        }
    }
}
