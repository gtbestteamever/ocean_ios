//
//  SplashController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 9/23/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import MBProgressHUD
class SplashController : UIViewController,URLSessionDownloadDelegate {
    var hud = MBProgressHUD()
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        DispatchQueue.main.async {

            self.hud.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
            
             
        }
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        DispatchQueue.main.async {
            self.hud.progress = 1
            if let data = try? Data(contentsOf: location) , let fileName = self.saveURL?.lastPathComponent,let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = documentsDirectory.appendingPathComponent("Welcome").appendingPathComponent(fileName)
                
                try? data.write(to: path)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.hud.hide(animated: true)
                self.navigationController?.pushViewController(LoginController(), animated: false)
            })
            
        }

    }
    func clearFolder(path:URL){
        
            let filePaths = try? FileManager.default.contentsOfDirectory(atPath: path.path)
            
            for filePath in filePaths ?? [] {
                print(7721, path.path + filePath)
                try? FileManager.default.removeItem(atPath: path.path + "/" + filePath)
            }
        

    }
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_logo"))
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(imageview)
        
        imageview.contentMode = .scaleAspectFit
        if UIDevice().userInterfaceIdiom == .pad {
        imageview.centerInSuperview(size: .init(width: 500, height: 200))
        }
        else{
            imageview.centerInSuperview(size: .init(width: 238, height: 48))
        }
        //fetchApi()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            
            self.navigationController?.pushViewController(UIDevice().userInterfaceIdiom == .pad ? LoginController() : iPhoneLoginController(), animated: false)
        })
        //
    }
    var session : URLSession?
    var saveURL : URL?
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/setting", decoderType: SettingModel.self) { json in
            DispatchQueue.main.async {
                if let json = json{
                    if let url_str =  json.data?.apprelated?.main_url?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: url_str),let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let directoryPath = documentsDirectory.appendingPathComponent("Welcome")
                        let final_path = directoryPath.appendingPathComponent(url.lastPathComponent)
                        
                        self.saveURL = url
                        
                        print(661,final_path.path)
                        if !FileManager.default.fileExists(atPath: final_path.path) {
                            self.clearFolder(path: directoryPath)
                            
                                self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.hud.label.text = "下載中...".localized
                                self.hud.label.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
                                self.hud.mode = .annularDeterminate

                                self.hud.progress = 0
                                
                                
                                
                                let request = URLRequest(url: url)

                                self.session = URLSession(configuration: .background(withIdentifier: UUID().uuidString), delegate: self, delegateQueue: nil)
                                
                                self.session?.downloadTask(with: request).resume()
                            

                        }
                        else{
                            self.navigationController?.pushViewController(LoginController(), animated: false)
                        }


                    }
                    else{
                        self.navigationController?.pushViewController(LoginController(), animated: false)
                    }
                }
                else{
                    self.navigationController?.pushViewController(LoginController(), animated: false)
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
}
