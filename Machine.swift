//
//  Machine.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

struct Machine: Decodable {
    var id: Int
    var name : Lang
    var desc : Lang
    var subdesc : [Lang]
    var erp_company_code : String
    var erp_model_code : String
    var erp_machine_code:[MachineCode]
    var machine_img:String
    var view360:[String]
    var partsxy : [xyLocation]
    var slider : SlidingPic
    var specification : [Spec]
    var content : [Con]
    var standard_equipment : Equipment
    var optional : [OpEquip]
    private enum CodingKeys: String,CodingKey {
        case id,name,desc,subdesc = "sub-desc", erp_company_code, erp_model_code,erp_machine_code, machine_img, view360, partsxy = "parts 點位座標", slider, specification = "specification 機械規格", content = "content 資訊類資料", standard_equipment = "standard_equipment 標配", optional = "optional 選配"
    }
    
}
//struct SpecFieldArray {
//    var value = [compareSection(title: "加工範圍", array: [NewSpecField(title: "床面旋徑", decodeId: "swing-over-bed", order: <#Int#>),NewSpecField(title: "兩心間距離", decodeId: "between-centers")]),compareSection(title: "軸向行程", array: [NewSpecField(title: "X1/X2/X3 軸向行程", decodeId: "x1-x2-x3-axis-travel"),NewSpecField(title: "Z1/Z2/Z3 軸向行程", decodeId: "z1-z2-z3-axis-travel")])]
//
//}
struct FilterArray {
    let val1 = [filterSection(title: "工作台尺寸 (mm)".localized,code: "mc-table-work-area", val: [filterData(mode: .lessThan, value: "380"),filterData(mode: .inBetween, value: "381~630"),filterData(mode: .greaterThan, value: "631")],unit: "")]
    let val2 = [filterSection(title: "切削直徑 (mm)".localized, code: "lc-maximum-turning-dia", val: [filterData(mode: .lessThan, value: "350"),filterData(mode: .inBetween, value: "351~550"),filterData(mode: .greaterThan, value: "551")],unit: ""),filterSection(title: "切削長度 (mm)".localized, code: "lc-between-centers", val: [filterData(mode: .lessThan, value: "800"),filterData(mode: .inBetween, value: "801~1500"),filterData(mode: .greaterThan, value: "1501")],unit:"")]
    let val3 = [filterSection(title: "切削直徑 (mm)".localized, code: "lc-maximum-turning-dia", val: [filterData(mode: .lessThan, value: "350"),filterData(mode: .inBetween, value: "351~550"),filterData(mode: .greaterThan, value: "551")],unit: ""),filterSection(title: "切削長度 (mm)".localized, code: "lc-between-centers", val: [filterData(mode: .lessThan, value: "800"),filterData(mode: .inBetween, value: "801~1500"),filterData(mode: .greaterThan, value: "1501")],unit:""),filterSection(title: "型態".localized, code: "Last", val: [filterData(mode: .equal, value: "無銑削".localized),filterData(mode: .equal, value: "有銑削".localized),filterData(mode: .equal, value: "副主軸".localized)], unit: "")]
    let val4 =  [filterSection(title: "工作台長度 (mm)".localized, code: "mc-table-work-area", val: [filterData(mode: .lessThan, value: "1000"),filterData(mode: .inBetween, value: "1001~1200"),filterData(mode: .greaterThan, value: "1201")],unit: ""),filterSection(title: "主軸轉速 (轉)".localized, code: "mc-spindle-speed", val: [filterData(mode: .lessThan, value: "6000"),filterData(mode: .inBetween, value: "6001~10000"),filterData(mode: .greaterThan, value: "10001")],unit:"")]
    let val5 = [filterSection(title: "鋁輪圈範圍 (°)".localized, code: "wheel", val: [filterData(mode: .lessThan, value: "18"),filterData(mode: .inBetween, value: "19~21"),filterData(mode: .greaterThan, value: "22")],unit: "")]
//    let val6 =
}
struct SpecArray {
    static var shared = SpecArray()
    var value = [compareSection]()
    var compareVal = [compareSection]()
    static func constructValue(field:[Field]) -> [compareSection]{
        
        var fArray = field
        if fArray.count != 0{
        fArray.removeFirst()
        }
        var headerArray = [compareSection]()
        var count = -1
        
        for i in fArray {
            
            if i.value == "__" {
                headerArray.append(compareSection(title: i.titles?.getLang() ?? "", array: []))
                count += 1
            }
            else{
                if count != -1 && i.value != "" && i.value != nil {
                    
                    headerArray[count].array.append(NewSpecField(title: i.titles?.getLang() ?? "", decodeId: i.code ?? "",unit: i.affix ?? "",prefix: i.prefix ?? "",order: i.order ?? 0))
                }
                
            }
        }
       
        SpecArray.shared.value = headerArray
        print(555,SpecArray.shared.value)
        
        return headerArray
    
    }
    
    static func setCompareFields(products:[Product]) {
//        if products.count == 0{
//            SpecArray.shared.value = []
//            return
//        }
        if products.count != 0{
        var sampleArray = products
        let sampleProduct = sampleArray.remove(at: 0)
        if let first = sampleProduct.specifications?.first?.fields {
            var sampleFields = SpecArray.constructValue(field: first)
            
            
            for i in sampleArray {
                if let aa = i.specifications?.first?.fields {
                    let addedFields = SpecArray.constructValue(field: aa)
                    
                    sampleFields = shared.doCompareFields(sample: sampleFields, added: addedFields)
                }
            }
            
            SpecArray.shared.value = sampleFields
            
            
            
        }
        }
        
        
    }
    
    func doCompareFields(sample:[compareSection],added:[compareSection]) -> [compareSection] {
        var changingField = sample
        
        for i in added {
            print(337,i.title)
            if !changingField.contains(where: { (cm) -> Bool in
                return cm.title == i.title
            }) {
                changingField.append(i)
                continue
            }
            for k in i.array {
                for (index,m) in changingField.enumerated() {
                    if m.title == i.title {
                    if !m.array.contains(where: { (spec) -> Bool in
                        return spec.decodeId == k.decodeId
                    }) {
                        changingField[index].array.append(k)
                        changingField[index].array = changingField[index].array.sorted(by: { (spec1, spec2) -> Bool in
                            return spec1.order < spec2.order
                        })
                    }
                    }
                }
            }
        }
        return changingField
    }

}
struct Equipment : Codable {
    var lable : [Con]
    
}
struct OpEquip: Codable {
    var name : Lang
    var items: [Item]
}
struct Item : Codable {
    var name : Lang
    var file_url: Lang
    var erp_option_code : [MachineCode]
}
struct Con : Codable {
    var type : Lang
    var name : Lang
    var file_url : Lang
}
struct Spec : Codable {
    var lable: [Lab]
    var unit : [String]
    var Y16: [String]
    var Y19: [String]
    var X22: [String]
    var X25: [String]
    var K28: [String]
    var K32: [String]
    var K36: [String]
    private enum CodingKeys: String,CodingKey {
        case lable,unit,Y16 = "Y-16", Y19 = "Y-19", X22 = "X-22",X25 = "X-25", K28 = "K-28",K32 = "K-32",K36 = "K-36"
    }
}
struct Lab : Codable {
    var type : Lang
    var name : Lang
}
struct SlidingPic : Codable{
    var en : [String]
    var zhTW : [String]
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
}
struct xyLocation : Codable {
    var file_url : Lang
    var x : String
    var y : String
}
struct Lang:Codable {
    var en:String?
    var zhTW:String?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(en, forKey: .en)
        try container.encode(zhTW, forKey: .zhTW)
    }
    func getLang() -> String?{
        return UserDefaults.standard.getConvertedLanguage() == "zh-TW" ? zhTW : en
    }
    func getFileLang() -> String? {
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            if en == nil{
                return zhTW
            }
            return en
        }
        return zhTW
    }
}
struct MachineCode:Codable {
    var id:Int
    var name:String
    var code:String
    var price : Price
}
struct Price:Codable {
    var USD : String
    var TWD : String
}
