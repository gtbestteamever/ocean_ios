//
//  OpenNotificatonView.swift
//  TaichungMC
//
//  Created by Wilson on 2021/10/5.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class OpenNotificationView : UIViewController {
    let contentview = NotificationSampleView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(contentview)
        
        contentview.centerInSuperview(size: .init(width: 500.calcvaluex(), height: 650.calcvaluey()))
        
        contentview.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}

class NotificationSampleView : SampleContainerView {
    let webview = WKWebView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.text = nil
        addSubview(webview)
        webview.anchor(top: xbutton.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
