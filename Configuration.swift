//
//  Configuration.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import WebKit
extension UIView {
    
    func roundCorners2(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}
class customNavigationController : UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
extension UIView {
    func addDiamondMask()
            {
        print(661,self.bounds.width,self.bounds.height)
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: self.bounds.width - 10.calcvaluex(), y: self.bounds.height))

            path.close()

            let shapeLayer = CAShapeLayer()
            shapeLayer.path = path.cgPath
        shapeLayer.fillColor = MajorColor().oceanColor.cgColor
            shapeLayer.strokeColor = MajorColor().oceanColor.cgColor
        self.layer.mask = shapeLayer
        }
}
struct MainFont {
    var Regular = "MicrosoftJhengHeiRegular"
    var Medium = "STHeitiSC-Medium"
    var Light = "STHeitiSC-Light"
}
extension UIViewController {
    func addStatusColor(){
        let statusview = UIView()
        view.addSubview(statusview)
        statusview.backgroundColor = MajorColor().oceanColor
        statusview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.height))
    }


}
var department_code = "D062"
struct GetFrequent {
    func getImageData(d1:FrequentProduct?,d2:FrequentProduct?) -> FrequentProduct?{
        return d2
    }
}
struct NotificationInfo {
    func getInfo(data: [AnyHashable:Any]) {
        let type = data["type"] as? String ?? ""
        let url = data["url"] as? String ?? ""
        let id = data["id"] as? String ?? ""
       
        print(771,type,url)
        if type == "public" {
        let v = OpenNotificationView()
        v.modalPresentationStyle = .overCurrentContext
        if let url = URL(string: url) {
            url.loadWebview(webview: v.contentview.webview)
        }
        General().getTopVc()?.present(v, animated: false, completion: nil)
        }
        else if type == "interview" {
            let vd = PriceHistoryController()
            
            vd.modalPresentationStyle = .fullScreen
            vd.id = id
            General().getTopVc()?.present(vd, animated: true, completion: nil)
        }
        else if type == "upkeep" {
            let hud = JGProgressHUD()
            hud.show(in: General().getTopVc()?.view ?? UIView())

            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(id)", decoderType: FixInfoData.self) { (json) in
                    DispatchQueue.main.async {
                        hud.dismiss()
                        if let json = json?.data {
                            let ft = FixPopInfoController()
                            ft.infoview.commentheader.data = json
                            ft.modalPresentationStyle = .overCurrentContext
                            General().getTopVc()?.present(ft, animated: false, completion: nil)
                        }
                    }

                }
        }
        else if type == "upkeep_task" {
            let vd = WorkDetailController()
            //print(self.current_work_array[indexPath.row])
            vd.id = id
            vd.modalPresentationStyle = .fullScreen
            General().getTopVc()?.present(vd, animated: true, completion: nil)
        }
        
    }
}
extension String{
    
    var localized: String{
        
        var lang = UserDefaults.standard.getConvertedLanguage()
        
        if lang == "zh-TW" {
            lang = "zh-Hant"
        }
        else{
            lang = "en"
        }
        let bundle = Bundle(path: Bundle.main.path(forResource: lang, ofType: "lproj")!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
        
    }
}
class SampleContainerView : UIView {
    var label : UILabel = {
       let label = UILabel()
        label.text = "建立料管"
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        return label
    }()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        
        return xbutton
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        clipsToBounds = true
        //addshadowColor()
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        label.centerXInSuperview()
        
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum StockMode {
    case Choices
    
    case Fields
    
    case Stocks
}
class StockApi {
    static let shared = StockApi()
    var cjson : [ApplicationData]?
    var stocks : [Stock]?
    var fields : [S_Fields]?
    func tryCall<T:Codable>( group: DispatchGroup,data:Data?,decodable:T.Type,mode: StockMode) {
        var token = ""
        var update_parameter = ""
        var parameter = ""
        var dataAny : Any?
        if mode == .Choices {
            update_parameter = "api-or/v1/stock_categories/check"
            parameter = "api-or/v1/stock_categories"
        }
        else if mode == .Fields {
            update_parameter = "api-or/v1/stock_fields/check"
            parameter = "api-or/v1/stock_fields"
        }
        else{
            update_parameter = "api-or/v1/stocks/check"
            parameter = "api-or/v1/stocks"
        }
        if let choice = data{
            
            if let choiceJson = try? JSONDecoder().decode(decodable, from: choice) {

                if let at = choiceJson as? Application {
                    token = at.meta.update_token
                    
                    dataAny = at.data
                    
                }
                if let at = choiceJson as? StockFields {
                    token = at.meta.update_token
                    dataAny = at.data
                }
                
                if let at = choiceJson as? Stocks {
                    token = at.meta.update_token
                    dataAny = at.data
                }
                
                NetworkCall.shared.postCall(parameter: update_parameter, dict: ["check_token":token], decoderType: UpdateClass.self) { (json) in
                    if let json = json{
                        if json.data ?? false {
                            if let tt = dataAny as? [ApplicationData] {
                                self.cjson = tt
                            }
                            if let tt = dataAny as? [Stock] {
                                self.stocks = tt
                            }
                            if let tt = dataAny as? [S_Fields] {
                                self.fields = tt
                            }
                            
                            group.leave()
                        }
                        else{
                            NetworkCall.shared.getCall(parameter: parameter, decoderType: decodable) { (json) in
                                

                                if let json = json {
                                    if let dd = json as? Application {
                                        self.cjson = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStockChoice(data: encode)
                                            
                                        }

                                    }
  
                                    
                                    if let dd = json as? Stocks {
                                        self.stocks = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStocks(data: encode)
                                            
                                        }

                                    }
 
                                    
                                    if let dd = json as? StockFields {
                                        self.fields = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStockFields(data: encode)
                                            
                                        }

                                    }
                                    group.leave()


                                }

                                    
                                
                                

                                
                            }
                        }
                    }
                    else{
                        group.leave()
                    }
                }
            }
            else{
                group.leave()
            }

   
        }
        else{
            NetworkCall.shared.getCall(parameter: parameter, decoderType: decodable) { (json) in
                

                if let json = json{
                    if let dd = json as? Application {
                        self.cjson = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStockChoice(data: encode)
                            
                        }

                    }
                    
                    if let dd = json as? Stocks {
                        self.stocks = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStocks(data: encode)
                            
                        }

                    }

                    
                    if let dd = json as? StockFields {
                        self.fields = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStockFields(data: encode)
                            
                        }

                    }
                    group.leave()

                }
                else{
                    group.leave()
                }
                    
                
            }
        }
    }
}

extension UserDefaults {
    func removeAllKey(){
//        let domain = Bundle.main.bundleIdentifier!
//        print(77,Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
//        print(99,self.getUserId())
//        self.removePersistentDomain(forName: domain)
//        print(66,self.getUserId())
//        print(88,Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                print(22,key)
                if key != "AppLanguage" && key != "showPrice"{
                    print("-------")
                    defaults.removeObject(forKey: key)
                }
                
            }
    }
    func getShowPrice() -> Bool {
        return self.bool(forKey: "showPrice")
    }
    func saveShowPrice(bool:Bool) {
        self.set(bool, forKey: "showPrice")
    }
    func saveSettingData(data:Data) {
        self.setValue(data, forKey: "Setting")
    }
    func getSettingData() -> Data?{
        return self.data(forKey: "Setting")
    }
    func saveHistory(data:Data) {
        self.setValue(data, forKey: "History")
    }
    func loadHistory() -> Data?{
        return self.data(forKey: "History")
    }
    func saveChosenCurrency(text:String) {
        self.setValue(text, forKey: "Currency")
    }
    func getChosenCurrency() -> String {
        return self.string(forKey: "Currency") ?? ""
    }
    func saveChosenGroup(text:String) {
        self.setValue(text, forKey: "Group")
    }
    func getChosenGroup() -> String{
        return self.string(forKey: "Group") ?? ""
    }
    func saveUserData(data:Data ,id:String) {
        self.setValue(data, forKey: "\(id)-user")
    }
    func getUserData(id:String) -> Data?{
        return self.data(forKey: "\(id)-user")
    }
    func saveUserId(id:String) {
        self.setValue(id, forKey: "UserId")
    }
    func getUserId() -> String{
        self.string(forKey: "UserId") ?? ""
    }
    func saveToken(token:String) {

        self.setValue(token, forKey: "Token")
    }
    func getToken() -> String {
        return self.string(forKey: "Token") ?? ""
    }
    func saveAppLanguage(lang:String) {
        self.setValue(lang, forKey: "AppLanguage")
    }
    func getLanguage() -> String{
        return self.string(forKey: "AppLanguage") ?? ""
    }
    func getConvertedLanguage() -> String{
        var lang = self.getLanguage()
        if lang == "zh-Hant" || lang == "zh-TW" || lang == "zh"{
            lang = "zh-TW"
        }
        else{
            lang = "en"
        }
        return lang
    }
    func saveUpdateToken(token:String,id:String) {
        self.setValue(token, forKey: "\(id)-token")
    }
    func getUpdateToken(id:String) -> String {
        return self.string(forKey: "\(id)-token") ?? ""
    }
    func saveCategoryData(data:Data) {
        self.setValue(data, forKey: "Category")
    }
    func getCategoryData() -> Data? {
        return self.data(forKey: "Category")
    }
    func setCustomersData(data:Data) {
        return self.setValue(data, forKey: "Customers")
    }
    func getCustomersData() -> Data?{
        return self.data(forKey: "Customers")
    }
    func saveCatValueData(data:Data,id:String) {
        self.setValue(data, forKey: "\(id)-data")
    }
    func getCatValueData(id:String) -> Data? {
        return self.data(forKey: "\(id)-data")
    }
    
    func getStockChoice() -> Data?{
        return self.data(forKey: "StockChoice")
    }
    func saveStockChoice(data:Data){
        self.setValue(data, forKey: "StockChoice")
    }
    
    func getStocks() -> Data?{
        return self.data(forKey: "Stocks")
    }
    func saveStocks(data:Data){
        self.setValue(data, forKey: "Stocks")
    }
    
    func getStockFields() -> Data?{
        return self.data(forKey: "StockFields")
    }
    func saveStockFields(data:Data){
        self.setValue(data, forKey: "StockFields")
    }
}
struct MajorColor {
    var mainColor = #colorLiteral(red: 0.0002849259472, green: 0.02924042009, blue: 0.5033084154, alpha: 1)
    var subColor = #colorLiteral(red: 0.2697770298, green: 0.2701570988, blue: 1, alpha: 1)
    var oceanColor = #colorLiteral(red: 0.0002849259472, green: 0.02924042009, blue: 0.5033084154, alpha: 1)
    var oceanSubColor = #colorLiteral(red: 0.2697770298, green: 0.2701570988, blue: 1, alpha: 1)
    var oceanGrayColor = #colorLiteral(red: 0.8133818507, green: 0.8101347685, blue: 0.8136510849, alpha: 1)
    var oceanlessColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
}

struct AppURL {
    var baseURL:String = {
        //http://61.66.123.79:8080
        //http://192.168.1.51:8000
        //http://0c1e27690ae7.jp.ngrok.io/
        //http://victor.public.192.168.1.51.xip.io/
        //return "http://192.168.1.81:8002/"
        return Bundle.main.infoDictionary?["API_URL"] as? String ?? ""
       //return "http://33df9835f583.jp.ngrok.io/"
    }()
    var uploadURL:String = {
        return (Bundle.main.infoDictionary?["API_URL"] as? String ?? "") + "uploads/public/"
    }()
}
extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let horizontalRatio = targetSize.width / size.width
            let verticalRatio = targetSize.height / size.height

            let ratio = max(horizontalRatio, verticalRatio)
            let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
            UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
            draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage!
   }
    func convertToHtmlString() -> String{
        
        let data = self.jpegData(compressionQuality: 0.1)
        let base64 = data?.base64EncodedString()
        let imageForTheHtml = "data:application/png;base64," + (base64 ?? "")
        return "<!DOCTYPE html>" +
            "<html lang=\"ja\">" +
            "<head>" +
            "<meta charset=\"UTF-8\">" +
            "<style type=\"text/css\">" +
            "html{margin:0;padding:0;}" +
            "body {" +
            "margin: 0;" +
            "padding: 0;" +
            "color: #ffffff;" +
            "font-size: 90%;" +
            "line-height: 1.6;" +
            "background: white;" +
            "}" +
            "img{" +
            "position: absolute;" +
            "top: 0;" +
            "bottom: 0;" +
            "left: 0;" +
            "right: 0;" +
            "margin: auto;" +
            "max-width: 100%;" +
            "max-height: 100%;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body id=\"page\">" +
            "<img src='\(imageForTheHtml)'/> </body></html>"
    }
    func loadWebview(webview:WKWebView) {
        
            webview.loadHTMLString(convertToHtmlString(), baseURL: nil)

    }
}
extension URL{
    func convertToHtmlString() -> String{
        
        var urlString = ""
        if self.absoluteString.contains("http://") || self.absoluteString.contains("https://") {
            urlString = self.absoluteString
        }
        else{
            urlString = "file://\(self.path)"
        }
        
        return "<!DOCTYPE html>" +
            "<html lang=\"ja\">" +
            "<head>" +
            "<meta charset=\"UTF-8\">" +
            "<style type=\"text/css\">" +
            "html{margin:0;padding:0;}" +
            "body {" +
            "margin: 0;" +
            "padding: 0;" +
            "color: #ffffff;" +
            "font-size: 90%;" +
            "line-height: 1.6;" +
            "background: white;" +
            "}" +
            "img{" +
            "position: absolute;" +
            "top: 0;" +
            "bottom: 0;" +
            "left: 0;" +
            "right: 0;" +
            "margin: auto;" +
        "width: 100%;" +
        "height:100%;" +
        "object-fit:contain;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body id=\"page\">" +
            "<img src='\(urlString)'/> </body></html>"
    }
    
    func loadWebview(webview:WKWebView) {
        if self.pathExtension == "png" || self.pathExtension == "jpg" || self.pathExtension == "jpeg" {
            webview.loadHTMLString(convertToHtmlString(), baseURL: nil)
        }
        else{
            webview.load(URLRequest(url: self))
            
        }
    }
}
struct OpenFile {
    func open(url:URL) {
        let mt = ExpandFileController()
        mt.modalPresentationStyle = .overCurrentContext
        
        
        url.loadWebview(webview: mt.imgv)
        
        General().getTopVc()?.present(mt, animated: false, completion: nil)
    }
    
    func open(data:Data,mimeType:String) {
        let mt = ExpandFileController()
        mt.modalPresentationStyle = .overCurrentContext
        
        mt.imgv.load(data, mimeType: mimeType, characterEncodingName: "", baseURL: NSURL() as URL)
        

        General().getTopVc()?.present(mt, animated: false, completion: nil)
    }
}
struct GetUser {
    func isPortal() -> Int {
        if let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let user = try? JSONDecoder().decode(UserData.self, from: dt) {
            return user.data.is_portal ?? 1
        }
        return 1
    }
    func getUser() -> UserData? {
        if let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let user = try? JSONDecoder().decode(UserData.self, from: dt) {
            return user
        }
        return nil
    }
    func isSaleManager() -> Bool {
        
        if let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let user = try? JSONDecoder().decode(UserData.self, from: dt) {
            return user.data.roles.contains { rt in
                return rt.kinds.contains { ss in
                    return ss.code == "after-sales-service"
                } && (rt.manages?.contains(where: { mt in
                    return mt.code == user.data.username
                }) ?? false)
            }

        }
        
        
        return false
    }
    
    func isAfterSale() -> Bool{
        if let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let user = try? JSONDecoder().decode(UserData.self, from: dt),let kind = user.data.roles.first(where: { (rt) -> Bool in
            if let _ = rt.kinds.first(where: { (kind) -> Bool in
                return kind.code == "after-sales-service"
            }) {
                return true
            }
            return false
        }) {
            return true
        }
        return false
    }
}
class General {
    static let shared = General()
    
    func getTopVc() -> UIViewController?{
        if #available(iOS 13, *)
        {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                return topController
            }
        }
        else{
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                return topController
            }
        
        }
        return nil
    }
    let activity : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: .whiteLarge)
        //act.backgroundColor = #colorLiteral(red: 0.8815405894, green: 0.8815405894, blue: 0.8815405894, alpha: 1)
        act.backgroundColor = #colorLiteral(red: 0.8848801295, green: 0.8848801295, blue: 0.8848801295, alpha: 1)
        act.hidesWhenStopped = true
        
        return act
    }()
    func showActivity(vd:UIView){
        
        vd.addSubview(activity)
        activity.centerInSuperview(size: .init(width: 50.calcvaluex(), height: 50.calcvaluex()))
        
        activity.startAnimating()
    }
    func stopActivity(vd:UIView){
        
        for i in vd.subviews {
            if let act = i as? UIActivityIndicatorView {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    act.stopAnimating()
                    act.removeFromSuperview()
                }
                
            }
        }
        
    }
}
