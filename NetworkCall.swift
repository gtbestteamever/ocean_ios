//
//  NetworkCall.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import FirebaseMessaging
class NetworkCall {

    var urlSession : URLSession?
    static let shared = NetworkCall()
    func getError(data:Data) {
        if let error = try? JSONDecoder().decode(ErrorApi.self, from: data).errors.first {
            let alertController = UIAlertController(title: "", message: nil, preferredStyle: .alert)
            if error.status == "401"
            {
                alertController.title = "提示 : 帳號已被登出".localized
                let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
                    UserDefaults.standard.saveToken(token: "")
                    Messaging.messaging().deleteToken { (_) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                            keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
                        }
                    }

                }
                alertController.addAction(action)
            }
            else if error.status == "500" {
                alertController.title = "提示 : api維修中".localized
                let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
                alertController.addAction(action)
            }
            else{
                let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
                alertController.addAction(action)
            }
            DispatchQueue.main.async {
                General().getTopVc()?.present(alertController, animated: true, completion: nil)
            }

        }
    }
    
    func getCall<T : Decodable>(parameter:String?,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if !Reachability.isConnectedToNetwork() {
           // print("NO Connection")
            completion(nil)
            
            return
        }
        if let parameter = parameter {
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(url)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            print(UserDefaults.standard.getToken())
            request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let data = data{
                    print(data)
                    do{
                        let json = try JSONDecoder().decode(decoderType, from: data)
                        completion(json)
                    }
                    catch let error{
                        print(error)
                        self.getError(data: data)
                        
                        
                        completion(nil)
                    }
                }
            }.resume()
        }
        else{
            completion(nil)
        }
    }
    
    func postCall<T : Decodable>(parameter:String?,dict:[String:String],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            print(991,"\(AppURL().baseURL)\(parameter)")
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(321,url,dict)
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = data
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            print(720,try JSONSerialization.jsonObject(with: data, options: []))
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCallSort<T : Decodable>(parameter:String?,dict:[[String:Any]],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
           
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = data
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCall<T : Decodable>(parameter:String?,param:[String:Any],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(221,url.absoluteString)
            do {
                let data = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = data
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func getDelete<T : Decodable>(parameter:String?,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            
            do {
                var request = URLRequest(url: url)
                request.httpMethod = "Delete"
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func getPostString(params:[String:String]) -> String
        {
            var data = [String]()
            for(key, value) in params
            {
                data.append(key + "=\(value)")
            }
            return data.map { String($0) }.joined(separator: "&")
        }
     func postCallform<T : Codable>(parameter:String?,dict:[String:String],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
         if let parameter = parameter {
             if !Reachability.isConnectedToNetwork() {
                 completion(nil)
                 return
             }
             guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                 return}
             print(111,url)
             do {
                 //let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

                 var request = URLRequest(url: url)
                 request.httpMethod = "POST"
                 request.httpBody = getPostString(params: dict).data(using: .utf8)
                 //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                 request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                 URLSession.shared.dataTask(with: request) { (data, response, error) in
                     if let data = data{
                         
                         do{

                             let json = try JSONDecoder().decode(decoderType, from: data)
                             completion(json)
                         }
                         catch let error{
                             print(error)
                            self.getError(data: data)
                             completion(nil)
                         }
                     }
                 }.resume()

                 
             }
             catch {
                 completion(nil)
             }


         }
         else{
             completion(nil)
         }
     }
    
    
    func postCallZip<T : Decodable>(parameter:String?,data:Data,fileName:String = "upload.zip",decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(3312,url.absoluteString)
            do {
                
                
             let boundary = "Boundary-\(UUID().uuidString)"
//                var body = ""
//                body += "--\(boundary)\r\n"
//                body += "Content-Disposition:form-data; name=\"zip_file\""
//                body += "; filename=\"upload.zip\"\r\n"
//                        + "Content-Type: \"content-type header\"\r\n\r\n\(String(data:data,encoding: .utf8) ?? "")\r\n"
//
//                body += "--\(boundary)--\r\n";
                let httpBody = NSMutableData()
                
                httpBody.append(convertFileData(fieldName: "zip_file", fileName: fileName, mimeType: "file", fileData: data, using: boundary))
                httpBody.appendString("--\(boundary)--")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = httpBody as Data

                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data{
                       
                        do{
                            print(772, try JSONSerialization.jsonObject(with: data, options: []))
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(331,error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCallMultipleData<T : Decodable>(parameter:String?,param:[String:String],data:[Data],dataParam:String,dataName:[String], memeString: [String],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)) {
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(3312,url.absoluteString)
            do {
                
             
             let boundary = "Boundary-\(UUID().uuidString)"

                let httpBody = NSMutableData()
                httpBody.append(convertParam(data: param,using:boundary))
                
                for index in 0 ..< data.count {
                    print(557,dataParam,dataName[index],memeString[index],data[index])
                    httpBody.append(convertFileData(fieldName: dataParam, fileName: dataName[index], mimeType: memeString[index], fileData: data[index], using: boundary))
                }
                
                
                
                httpBody.appendString("--\(boundary)--")
                //print(998,String(data: httpBody as Data, encoding: .utf8))
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = httpBody as Data
                
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")

                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data{
                       
                        do{
                            print(661,try? JSONSerialization.jsonObject(with: data, options: []))
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(331,error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCallAny<T : Decodable>(parameter:String?,param:Any,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)) {
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(221,url.absoluteString)
            
            do {
                
                
                let json = try JSONSerialization.data(withJSONObject: param, options: [])
                print(NSString(string: String(data: json, encoding: .utf8) ?? ""))
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = json
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data{
                       
                        do{

                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(331,error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCallZip2<T : Decodable>(parameter:String?,param:[String:String],data:Data,dataName:String,dataParam: String, memeString: String,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(3312,url.absoluteString)
            do {
                
             
             let boundary = "Boundary-\(UUID().uuidString)"

                let httpBody = NSMutableData()
                httpBody.append(convertParam(data: param,using:boundary))
                httpBody.append(convertFileData(fieldName: dataParam, fileName: dataName, mimeType: memeString, fileData: data, using: boundary))
                
                
                httpBody.appendString("--\(boundary)--")

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = httpBody as Data
                
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")

                urlSession?.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data{
                       
                        do{
                            
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(331,error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
//    func postCallZip3<T : Decodable>(parameter:String?,param:[String:Int],data:Data,dataName:String,dataParam: String, memeString: String,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
//        
//        if let parameter = parameter {
//            if !Reachability.isConnectedToNetwork() {
//                completion(nil)
//                return
//            }
//            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
//                return}
//            print(3312,url.absoluteString)
//            do {
//                
//             
//             let boundary = "Boundary-\(UUID().uuidString)"
//
//                let httpBody = NSMutableData()
//                httpBody.append(convertParam(data: param,using:boundary))
//                httpBody.append(convertFileData(fieldName: dataParam, fileName: dataName, mimeType: memeString, fileData: data, using: boundary))
//                
//                
//                httpBody.appendString("--\(boundary)--")
//
//                var request = URLRequest(url: url)
//                request.httpMethod = "POST"
//                request.httpBody = httpBody as Data
//                
//                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
//
//                urlSession?.dataTask(with: request) { (data, response, error) in
//                    
//                    if let data = data{
//                       
//                        do{
//                            //print(77,try? JSONSerialization.jsonObject(with: data, options: []))
//                            let json = try JSONDecoder().decode(decoderType, from: data)
//                            completion(json)
//                        }
//                        catch let error{
//                            print(331,error)
//                            self.getError(data: data)
//                            completion(nil)
//                        }
//                    }
//                }.resume()
//
//                
//            }
//            catch {
//                completion(nil)
//            }
//
//
//        }
//        else{
//            completion(nil)
//        }
//    }
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
      let data = NSMutableData()
        
      data.appendString("--\(boundary)\r\n")
      data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        
        data.append(fileData)

        data.appendString("\r\n")

        
      return data as Data
    }
    func convertParam(data:[String:String],using:String) -> Data{
        let dt = NSMutableData()
        for (key,i) in data{
            dt.appendString("--\(using)\r\n")
            dt.appendString("Content-Disposition:form-data; name=\"\(key)\"")
            dt.appendString("\r\n\r\n\(i)\r\n")
        }
        return dt as Data
        
    }
//    func convertParamInt(data:[String:Int],using:String) -> Data{
//        let dt = NSMutableData()
//        for (key,i) in data{
//            dt.appendString("--\(using)\r\n")
//            dt.appendString("Content-Disposition:form-data; name=\"\(key)\"")
//            dt.appendString("\r\n\r\n\(i)\r\n")
//        }
//        return dt as Data
//
//    }
}
extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}
