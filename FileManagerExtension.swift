//
//  FileManagerExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/14/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
var downloadFolder = "Download"
var isDir : ObjCBool = false
extension FileManager {
    func createDirectory(name:String){
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let DirPath = DocumentDirectory.appendingPathComponent(name)
        if !self.fileExists(atPath: DirPath!.path) {
            
        do
        {
            try self.createDirectory(atPath: DirPath!.path, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error as NSError
        {
            print("Unable to create directory \(error.debugDescription)")
        }
        }
        print("Dir Path = \(DirPath!)")
        
        
    }
    
    func saveToDirectory(data:Data,folder:String = downloadFolder,fileName:String) {
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let DirPath = DocumentDirectory.appendingPathComponent(folder)
        print(113,DirPath)
        guard let filePath = DirPath?.appendingPathComponent(fileName) else {return}
        do{
            try data.write(to: filePath)
        }
        catch {
            print(error.localizedDescription)
        }
        
    }
    func getFile(name:String,folder:String = downloadFolder) -> URL?{
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let DirPath = DocumentDirectory.appendingPathComponent(folder)
        
        guard let filePath = DirPath?.appendingPathComponent(name) else {return nil}
        
        if checkExist(fileName: name) {
            return filePath
        }
        
        return nil
        
    }
    func checkExist(folder:String = downloadFolder,fileName:String) -> Bool{
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let DirPath = DocumentDirectory.appendingPathComponent(folder)
        guard let filePath = DirPath?.appendingPathComponent(fileName) else {return true}
        
        return self.fileExists(atPath: filePath.path)
    }
    
    func deleteFile(name:String,folder:String = downloadFolder) {
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let DirPath = DocumentDirectory.appendingPathComponent(folder)
        guard let filePath = DirPath?.appendingPathComponent(name) else {return}
        if self.checkExist(fileName: name) {
            do{
                try FileManager.default.removeItem(at: filePath)
            }
            catch{
                
            }
        }
    }
}
