//
//  User.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/19/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct Account : Codable {
    
    var user: User
    var access_token:String
}

struct User : Codable {
    var id:String
    var attributes : UserAttributes
}

struct UserAttributes:Codable {
    var username:String
    var name:String
}


struct UpdateClass : Codable {
    var data: Bool?
}

struct UserData : Codable {
    var data : UserD
    
}
struct UserAccount: Codable {
    var code : String?
    var name: String?
    var bussiness_number : String?
    var tel: String?
    var contact : String?
    var address : String?
}
struct UserD : Codable {
    var id : String
    //var attributes : UserDAttribute?
    var is_portal : Int?
    var name : String?
    var username : String?
    var mainrole_name : String?
    //String
    //[[String:String]]
    var currencies : [[String:String]]
    var currency_groups : [[String:String]]
    var account : UserAccount?
    var discount_percent : Int?
    var permissions : [Permission]
    var roles : [Role]
}

struct UserDAttribute : Codable{

}
struct Role : Codable {
    //var pivot : RolePivot
    var manages : [ManageRole]?
    var kinds : [RoleKind]
}
struct ManageRole : Codable{
    var code : String
    var name : String
}
struct RolePivot : Codable{
    var user_id : Int
    var role_id : Int
}
struct RoleKind : Codable {
    var code : String
}
struct Permission : Codable {
    var slug : String
}

struct Logout: Codable{
    var data:Bool
    var message: String
}
