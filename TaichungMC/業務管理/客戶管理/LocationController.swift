//
//  LocationController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class LocationController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let container = UIView()
        container.layer.cornerRadius = 15.calcvaluex()
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        view.addSubview(container)
        container.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 146.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 573.calcvaluex(), height: 476.calcvaluey()))
        container.centerXInSuperview()
        
        let header = UILabel()
        header.text = "訪談定位"
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(header)
        header.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        header.centerXInSuperview()
        
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        xbutton.imageView?.contentMode = .scaleAspectFill
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(closewindow), for: .touchUpInside)
        let map = UIView()
        map.layer.borderColor = #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1)
        map.layer.borderWidth = 1.calcvaluex()
        map.backgroundColor = #colorLiteral(red: 0.9716169238, green: 0.9766408801, blue: 0.9807491899, alpha: 1)
        
        container.addSubview(map)
        map.anchor(top: header.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 533.calcvaluex(), height: 299.calcvaluey()))
        map.centerXInSuperview()
        
        let confirmbutton = UIButton()
        confirmbutton.setTitle("確認", for: .normal)
        confirmbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        confirmbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        confirmbutton.setTitleColor(.white, for: .normal)
        confirmbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.addSubview(confirmbutton)
        confirmbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        confirmbutton.centerXInSuperview()
        confirmbutton.layer.cornerRadius = 48.calcvaluey()/2
        confirmbutton.addTarget(self, action: #selector(closewindow), for: .touchUpInside)
    }
    @objc func closewindow(){
        self.dismiss(animated: true, completion: nil)
    }
}
