//
//  CustomerDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import XlsxReaderWriter

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
class CustomerCircleView : UIView {
    let namelabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        layer.borderColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        namelabel.textColor = .white
        namelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        addSubview(namelabel)
        namelabel.centerInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CustomerDotCircleView : UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        layer.borderColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        
        imageview.tintColor = .white
        addSubview(imageview)
        imageview.centerInSuperview(size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NewCustomerTopLeftViewCell : UITableViewCell {
    let img = UIImageView(image: #imageLiteral(resourceName: "ic_content_user"))
    let nLabel = UILabel()
    let vLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        img.tintColor = MajorColor().oceanColor
        backgroundColor = .clear
        selectionStyle = .none
        let seperator = UIView()
        seperator.constrainWidth(constant: 8.calcvaluex())
        nLabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        nLabel.text = "統編："
        
        vLabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        vLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        //vLabel.numberOfLines = 0
        //vLabel.text = "84980915"
        let sep = UIView()
        
        let stackview = UIStackView(arrangedSubviews: [img,seperator,nLabel,vLabel,sep])
        stackview.spacing = 5.calcvaluex()
        stackview.axis = .horizontal
        stackview.distribution = .fill
        
        img.constrainWidth(constant: 24.calcvaluey())
        img.constrainHeight(constant: 24.calcvaluey())
        contentView.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 9.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        
        let nSep = UIView()
        nSep.backgroundColor = #colorLiteral(red: 0.897361517, green: 0.8974906802, blue: 0.8973332047, alpha: 1)
        contentView.addSubview(nSep)
        nSep.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 4.calcvaluex()),size: .init(width: 0, height: 1.5.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class UnderlinedLabel: UILabel {

override var text: String? {
    didSet {
        guard let text = text else { return }
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
        }
    }
}
protocol New2CustomerDelegate{
    func showUser()
}
class New2CustomerTopLeftViewCell : UITableViewCell {
    let img = UIImageView(image: #imageLiteral(resourceName: "ic_content_user"))
    let nLabel = UILabel()
    let vLabel = UnderlinedLabel()
    var delegate: New2CustomerDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        img.tintColor = MajorColor().oceanColor
        backgroundColor = .clear
        selectionStyle = .none
        let seperator = UIView()
        seperator.constrainWidth(constant: 8.calcvaluex())
        nLabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        nLabel.text = "統編："
        
        vLabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        
        vLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        vLabel.text = "84980915"
        vLabel.isUserInteractionEnabled = true
        vLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showUser)))
        let sep = UIView()
        let stackview = UIStackView(arrangedSubviews: [img,seperator,nLabel,vLabel,sep])
        stackview.spacing = 5.calcvaluex()
        stackview.axis = .horizontal
        stackview.distribution = .fill
        img.constrainWidth(constant: 24.calcvaluey())
        img.constrainHeight(constant: 24.calcvaluey())
        contentView.addSubview(stackview)
        stackview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 9.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        let nSep = UIView()
        nSep.backgroundColor = #colorLiteral(red: 0.897361517, green: 0.8974906802, blue: 0.8973332047, alpha: 1)
        contentView.addSubview(nSep)
        nSep.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 4.calcvaluex()),size: .init(width: 0, height: 1.5.calcvaluey()))
    }
    @objc func showUser(){
 
        delegate?.showUser()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomScrollIndicator : UIView{
    let topScroll = UIView()
    let bottomScroll = UIView()
    var topScrollAnchor : AnchoredConstraints!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        bottomScroll.backgroundColor = #colorLiteral(red: 0.768540442, green: 0.7686763406, blue: 0.7685418725, alpha: 1)
        
        addSubview(bottomScroll)
        bottomScroll.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 4.calcvaluex(), height: 0))
        bottomScroll.centerXInSuperview()
        bottomScroll.layer.cornerRadius = 4.calcvaluex()/2
        
        addSubview(topScroll)
        topScroll.layer.cornerRadius = 6.calcvaluex()/2
        topScroll.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        topScrollAnchor = topScroll.anchor(top: bottomScroll.topAnchor, leading: nil, bottom: bottomScroll.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 50.calcvaluey(), right: 0),size: .init(width: 6.calcvaluex(), height: 0))
        topScroll.centerXInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct TopLeftModel {
    let img : UIImage
    let header : String
    var val:String?
}
class NewCustomerTopLeftView : WorkDetailView,UITableViewDelegate,UITableViewDataSource{

    

    var company : Company?{
        didSet{
            headerlabel.text = "\(company?.data?.code ?? "") \(company?.data?.name ?? "")"
            //company?.data?.business_number
            datav[0].val = company?.data?.business_number
            datav[1].val = company?.data?.tel
            datav[2].val = company?.data?.fax
            datav[3].val = company?.data?.contact
            datav[4].val = company?.data?.address
            datav[5].val = "\(company?.data?.sales?.code ?? "") \(company?.data?.sales?.name ?? "")"
            //datav[6].val = company?.data?.business_number
            self.tb.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datav.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 5{
            let cell = New2CustomerTopLeftViewCell(style: .default, reuseIdentifier: "cell2")
            cell.img.image = datav[indexPath.row].img
            cell.vLabel.text = datav[indexPath.row].val
            cell.nLabel.text = datav[indexPath.row].header + "："
            cell.delegate = delegate
            return cell
        }
        let cell = NewCustomerTopLeftViewCell(style: .default, reuseIdentifier: "cell")
        cell.img.image = datav[indexPath.row].img
        cell.vLabel.text = datav[indexPath.row].val
        cell.nLabel.text = datav[indexPath.row].header + "："
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 34.calcvaluey()
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
        let headerlabel = UILabel()
    let tb = UITableView(frame: .zero, style: .grouped)
    var scrollDown = false
    var delegate : New2CustomerDelegate?
    let scrollIndicator = CustomScrollIndicator()
    
    var datav = [TopLeftModel(img: #imageLiteral(resourceName: "ic_folder").withRenderingMode(.alwaysOriginal), header: "統編".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_phone").withRenderingMode(.alwaysOriginal), header: "電話".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_fax").withRenderingMode(.alwaysOriginal), header: "傳真".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_person").withRenderingMode(.alwaysOriginal), header: "聯絡人".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_address").withRenderingMode(.alwaysOriginal) , header: "地址".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_person").withRenderingMode(.alwaysOriginal), header: "業務員".localized, val: ""),TopLeftModel(img: #imageLiteral(resourceName: "ic_person").withRenderingMode(.alwaysOriginal), header: "代理商".localized, val: "")]
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(headerlabel)
        headerlabel.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 21.calcvaluey(), left: 21.calcvaluex(), bottom: 0, right: 21.calcvaluex()))
        headerlabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
       // headerlabel.text = "230 巧新科技工業股份有限公司"
        headerlabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        addSubview(tb)
        tb.backgroundColor = .clear
        tb.anchor(top: headerlabel.bottomAnchor, leading: headerlabel.leadingAnchor, bottom: bottomAnchor, trailing: headerlabel.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 100.calcvaluey(), right: 0))
        tb.isScrollEnabled = false
        tb.separatorStyle = .none
        tb.flashScrollIndicators()
        tb.contentInset = .init(top: 0, left: 0, bottom: 68.calcvaluey(), right: 0)
        tb.bounces = false
        tb.delegate = self
        tb.dataSource = self
//        let pan = UIPanGestureRecognizer(target: self, action: #selector(panning))
//        tb.addGestureRecognizer(pan)
      //  pan.cancelsTouchesInView = false

//        addSubview(scrollIndicator)
//        scrollIndicator.anchor(top: topview.bottomAnchor, leading: nil, bottom: tb.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 20.calcvaluex(), right: 0),size: .init(width: 24.calcvaluex(), height: 0))
//        scrollIndicator.isUserInteractionEnabled = true
//        scrollIndicator.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(npanning)))
    }
    @objc func npanning(pan:UIPanGestureRecognizer) {
        let vt = pan.velocity(in: self)

        if vt.y > 0{

            
            if !scrollDown {
            print("scroll Down")
                scrollIndicator.topScrollAnchor.top?.constant = 50.calcvaluey()
                scrollIndicator.topScrollAnchor.bottom?.constant = 0
                UIView.animate(withDuration: 0.4) {
                    self.layoutIfNeeded()
                }
                
            tb.scrollToRow(at: IndexPath(row: 5, section: 0), at: .bottom, animated: true)
                scrollDown = true
            }
        }
        else{
            if scrollDown {
                scrollIndicator.topScrollAnchor.top?.constant = 0.calcvaluey()
                scrollIndicator.topScrollAnchor.bottom?.constant = -50.calcvaluey()
                UIView.animate(withDuration: 0.4) {
                    self.layoutIfNeeded()
                }
            print("scroll Up")
            tb.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                scrollDown = false
            }
        }
    }
    @objc func panning(pan:UIPanGestureRecognizer){
        let vt = pan.velocity(in: self)
        
        if vt.y > 0{
            if scrollDown {
                scrollIndicator.topScrollAnchor.top?.constant = 0.calcvaluey()
                scrollIndicator.topScrollAnchor.bottom?.constant = -50.calcvaluey()
                UIView.animate(withDuration: 0.4) {
                    self.layoutIfNeeded()
                }
            print("scroll Up")
            tb.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                scrollDown = false
            }
        }
        else{
            if !scrollDown {
            print("scroll Down")
                scrollIndicator.topScrollAnchor.top?.constant = 50.calcvaluey()
                scrollIndicator.topScrollAnchor.bottom?.constant = 0
                UIView.animate(withDuration: 0.4) {
                    self.layoutIfNeeded()
                }
                
            tb.scrollToRow(at: IndexPath(row: 5, section: 0), at: .bottom, animated: true)
                scrollDown = true
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerTopLeftView:NewCustomerTopLeftView {
    var detailButton = AddButton4()
    let button = AddButton4()
    let dotview = CustomerDotCircleView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        

        button.titles.text = "查看詳細資料".localized
        topview.addSubview(button)
        button.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()))
        button.centerYInSuperview()

        
        addSubview(detailButton)
        detailButton.titles.text = "編輯客戶".localized
        
        detailButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 11.calcvaluex(), bottom: 12.calcvaluey(), right: 11.calcvaluex()))

        detailButton.addTarget(self, action: #selector(goSeeDetail), for: .touchUpInside)
    }
    @objc func goSeeDetail(){
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum CustomDetailViewMode{
    case NoInterView
    case Normal
    case WillShare
    case AllSelect

}
class CustomDetailView:WorkDetailView {
    let button = AddButton4()
    let stackview = UIStackView()
    var showButton : Bool = false{
        didSet{
            if showButton {
                button.isHidden = false
                stackview.isHidden = false
            }
            else{
                button.isHidden = true
                stackview.isHidden = true
            }
        }
    }
    var mode : CustomDetailViewMode = .NoInterView {
        didSet{
            switch mode {
            case .NoInterView :
                stackview.isHidden = true
               // self.titlelabelAnchor?.constant = 0.calcvaluex()
            case .Normal :
                stackview.isHidden = false
                cancelButton.isHidden = true
                selectAllButton.isHidden = true
              //  self.titlelabelAnchor?.constant = 0.calcvaluex()
            case .WillShare:
                stackview.isHidden = false
                cancelButton.isHidden = false
                selectAllButton.isHidden = false
                selectAllButton.setTitle("全選".localized, for: .normal)
                selectAllButtonAnchor?.constant = 40.calcvaluex()
               // self.titlelabelAnchor?.constant = 15.calcvaluex()
                
            case .AllSelect:
            
                selectAllButton.setTitle("取消全選".localized, for: .normal)
                selectAllButtonAnchor?.constant = 65.calcvaluex()
                //self.titlelabelAnchor?.constant = 20.calcvaluex()
            

            }
        }
    }
    var shareButton : UIButton = {
        let shareButton = UIButton(type: .custom)
        shareButton.setTitle("分享".localized, for: .normal)
        shareButton.setTitleColor(#colorLiteral(red: 0.5895374169, green: 0.595374421, blue: 0.595374421, alpha: 1), for: .normal)
        shareButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            shareButton.constrainWidth(constant: 50.calcvaluex())
           
        }
        else{
            shareButton.constrainWidth(constant: 40.calcvaluex())
        }
        //shareButton.backgroundColor = .red
        return shareButton
    }()
    var cancelButton : UIButton = {
        let shareButton = UIButton(type: .custom)
        shareButton.setTitle("取消".localized, for: .normal)
        shareButton.setTitleColor(#colorLiteral(red: 0.5895374169, green: 0.595374421, blue: 0.595374421, alpha: 1), for: .normal)
        shareButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            shareButton.constrainWidth(constant: 50.calcvaluex())
           
        }
        else{
            shareButton.constrainWidth(constant: 40.calcvaluex())
        }
        return shareButton
    }()
    lazy var selectAllButton : UIButton = {
        let shareButton = UIButton(type: .custom)
        shareButton.setTitle("全選".localized, for: .normal)
        shareButton.setTitleColor(#colorLiteral(red: 0.5895374169, green: 0.595374421, blue: 0.595374421, alpha: 1), for: .normal)
        shareButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            selectAllButtonAnchor = shareButton.widthAnchor.constraint(equalToConstant: 60.calcvaluex())
        }
        else{
            selectAllButtonAnchor = shareButton.widthAnchor.constraint(equalToConstant: 40.calcvaluex())
        }
        
        selectAllButtonAnchor?.isActive = true
        //shareButton.constrainWidth(constant: 40.calcvaluex())
        return shareButton
    }()
    var selectAllButtonAnchor:NSLayoutConstraint?
    init(showbutton:Bool,title:String) {
        super.init(frame: .zero)
  
        self.showButton = showbutton
        button.titles.text = title
        addSubview(button)
        button.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 11.calcvaluex(), bottom: 12.calcvaluey(), right: 11.calcvaluex()))
       

//
//
//        topview.addSubview(stackview)
//        stackview.anchor(top: topview.topAnchor, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: button.leadingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex()))
//                stackview.axis = .horizontal
//                stackview.distribution = .fill
//                stackview.spacing = 16.calcvaluex()
//
//                stackview.addArrangedSubview(cancelButton)
//                stackview.addArrangedSubview(selectAllButton)
//                stackview.addArrangedSubview(shareButton)
//        stackview.addArrangedSubview(UIView())
    }

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol CustomerTopRightViewDelegate {
    func goToInterview(id:String)
    func didSelectAllDetail()
    func removeSelectAll()
    func appendDetail(interview:Interview)
    func removeDetail(interviews:Interview)
    func shareSelectedInterviews()
    func didCancelSharing()
}
class CustomerTopRightView : CustomDetailView,UITableViewDelegate,UITableViewDataSource {
    func didSelectSharing(tag: Int) {
        interviews[tag].didSelect = !(interviews[tag].didSelect ?? false)
        if interviews[tag].didSelect == true {
            delegate.appendDetail(interview: interviews[tag])
        }
        else{
            delegate.removeDetail(interviews: interviews[tag])
        }
        self.tableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CustomerTopRightViewCell(style: .default, reuseIdentifier: "ce")
        cell.tag = indexPath.row
        cell.backgroundColor = .clear
        cell.mode = self.mode
        
//        if indexPath.row == interviews.count - 1{
//            cell.seperator.isHidden = true
//        }
//        else{
//            cell.seperator.isHidden = false
//        }
        cell.datelabel.text = interviews[indexPath.row].date
       // cell.checkbox.isSelected = interviews[indexPath.row].didSelect ?? false
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66.calcvaluey()
    }
    var interviews = [Interview]() {
        didSet{
            tableview.reloadData()
        }
    }
    let tableview = UITableView(frame: .zero, style: .plain)
    var delegate:CustomerTopRightViewDelegate!
    override init(showbutton: Bool, title: String) {
        super.init(showbutton: showbutton, title: title)
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: button.topAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right:0))
        tableview.delegate = self
        tableview.showsVerticalScrollIndicator = false
        tableview.dataSource = self
        tableview.separatorStyle = .none
        shareButton.addTarget(self, action: #selector(goSharing), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelSharing), for: .touchUpInside)
        selectAllButton.addTarget(self, action: #selector(selectAllData), for: .touchUpInside)
    }

    @objc func goSharing(){
        if mode == .Normal {
            mode = .WillShare
            self.tableview.reloadData()
        }
        else if mode == .WillShare || mode == .AllSelect{
            delegate.shareSelectedInterviews()
        }
        
    }
    @objc func cancelSharing(){
        mode = .Normal
        removeAllSelect()
        delegate.didCancelSharing()
    }
    func removeAllSelect(){
        for (index,_) in interviews.enumerated() {
            interviews[index].didSelect = false
        }
        delegate.removeSelectAll()
    }
    func selectAll(){
        for (index,_) in interviews.enumerated() {
            interviews[index].didSelect = true
        }
        delegate.didSelectAllDetail()

    }
    @objc func selectAllData(){
        if mode == .WillShare {
            mode = .AllSelect
            selectAll()
        }
        else{
            mode = .WillShare
            removeAllSelect()
        }
        
        self.tableview.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if mode == .WillShare {
            self.didSelectSharing(tag: indexPath.row)
        }
        else{
        delegate.goToInterview(id:interviews[indexPath.row].id)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerTopRightViewCell:UITableViewCell {

    let circleview = UIView()
    let datelabel = UILabel()
    let seperator = UIView()
    var checkbox : UIButton = {
        let check = UIButton(type: .custom)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_pressed"), for: .selected)
        return check
    }()
    let imageViewd = UIImageView(image: #imageLiteral(resourceName: "ic_arrow").withRenderingMode(.alwaysTemplate))
    var mode : CustomDetailViewMode = .Normal {
        didSet{
            if self.mode == .WillShare || self.mode == .AllSelect {
                circleview.isHidden = true
                checkbox.isHidden = false
            }
            else{
                circleview.isHidden = false
                checkbox.isHidden = true
            }
        }

    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        //contentView.backgroundColor = .red
        imageViewd.tintColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        contentView.addSubview(imageViewd)
        imageViewd.anchor(top: nil, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 30.calcvaluex()),size: .init(width: 18.calcvaluex(), height: 18.calcvaluex()))
        
        imageViewd.centerYInSuperview()
//        circleview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
//        selectionStyle = .none
//        addSubview(circleview)
//        circleview.centerYInSuperview()
//        circleview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 33.calcvaluex(), bottom: 0, right: 0),size: .init(width: 10.calcvaluey(), height: 10.calcvaluey()))
//        circleview.layer.cornerRadius = 10.calcvaluey()/2
//        addSubview(checkbox)
//        checkbox.translatesAutoresizingMaskIntoConstraints = false
//        checkbox.centerXAnchor.constraint(equalTo: circleview.centerXAnchor).isActive = true
//        checkbox.centerYAnchor.constraint(equalTo: circleview.centerYAnchor).isActive = true
//        checkbox.widthAnchor.constraint(equalToConstant: 30.calcvaluex()).isActive = true
//        checkbox.heightAnchor.constraint(equalToConstant: 30.calcvaluex()).isActive = true
        //checkbox.isHidden = true
        datelabel.font = UIFont(name: MainFont().Medium, size: 18.calcvaluex())
        //datelabel.text = "12月10日 下午2:24"
        datelabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        contentView.addSubview(datelabel)
        datelabel.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: imageViewd.leadingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 18.calcvaluex()))
        datelabel.centerYInSuperview()
        
        seperator.backgroundColor = #colorLiteral(red: 0.8133818507, green: 0.8101347685, blue: 0.8136510849, alpha: 1)
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 15.calcvaluex(), bottom: 0, right: 15.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerBottomView : CustomDetailView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ct", for: indexPath)
        cell.backgroundColor = .clear
        

        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 364.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 26.calcvaluex()
    }
    
    
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    override init(showbutton: Bool, title: String) {
        super.init(showbutton: showbutton, title: title)
        collectionview.backgroundColor = .clear
        addSubview(collectionview)
        collectionview.contentInset = .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex())
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        collectionview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.register(CustomerBottomViewCell.self, forCellWithReuseIdentifier: "ct")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerBottomViewCell: UICollectionViewCell {
    let container = UIView()
    let statuslab = StatusLabel(text: "已擱置", color: #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1))
    let mainDeviceLab = QuoteLabel(text: "多軸複合加工機")
    let subDeviceLab = QuoteLabel(text: "VMT-X400")
    let secondsubDeviceLab = QuoteLabel(text: "Vc-P106")
    let createLabel = CreatTimeLabel(text: "建立時間 10月05日下午4:47")
    let modifiedLabel = CreatTimeLabel(text: "上次修改 10月10日下午4:47")
    let priceLab = PriceLabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8145063519, green: 0.8166118264, blue: 0.82153368, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        
        addSubview(container)
        container.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 200.calcvaluey()))
        container.centerYInSuperview()
        container.addSubview(statuslab)
        statuslab.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluex(), left: 16.calcvaluey(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        
        container.addSubview(mainDeviceLab)
        mainDeviceLab.anchor(top: statuslab.bottomAnchor, leading: statuslab.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        mainDeviceLab.layer.cornerRadius = 24.calcvaluey()/2
        
        container.addSubview(subDeviceLab)
        subDeviceLab.anchor(top: mainDeviceLab.bottomAnchor, leading: mainDeviceLab.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        subDeviceLab.layer.cornerRadius = 24.calcvaluey()/2
        
        container.addSubview(secondsubDeviceLab)
        secondsubDeviceLab.anchor(top: subDeviceLab.topAnchor, leading: subDeviceLab.trailingAnchor, bottom: subDeviceLab.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0))
        secondsubDeviceLab.layer.cornerRadius = 24.calcvaluey()/2
        
        container.addSubview(createLabel)
        createLabel.anchor(top: subDeviceLab.bottomAnchor, leading: subDeviceLab.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0))
        container.addSubview(modifiedLabel)
        
        modifiedLabel.anchor(top: createLabel.bottomAnchor, leading: createLabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        container.addSubview(priceLab)
        priceLab.anchor(top: nil, leading: nil, bottom: modifiedLabel.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom:0, right: 16.calcvaluex()))
        priceLab.text = "$90,399 USD"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerDetailController: SampleController,CustomerTopRightViewDelegate,New2CustomerDelegate {
    func didCancelSharing() {
        removeDirectory()
    }
    
    func shareSelectedInterviews() {
        removeDirectory()

        if selectedDetailArray.count == 0{
            let alertController = UIAlertController(title: "請至少選擇一個紀錄", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let name = self.company?.data?.name {
               
               self.writeToFolder(fileName: name)
                let documentPath = documentsDirectory.appendingPathComponent("interview_record/\(name)_訪談紀錄.xlsx")
               
                let spreadSheet = BRAOfficeDocumentPackage.open(documentPath.path)
                
                let worksheet : BRAWorksheet = spreadSheet?.workbook.worksheets[0] as! BRAWorksheet
                

                
                for (index,i) in self.selectedDetailArray.enumerated() {
                    for j in InterviewExcelFields().fields {
                        let cell = worksheet.cell(forCellReference: "\(j.letter)\(index + 2)", shouldCreate: true)
                        switch j.name {
                        case "客戶名稱":
                            cell?.setStringValue(i?.account?.name ?? "")
                        case "訪談地點":
                            cell?.setStringValue(i?.address ?? "")
                        case "拜訪日期":
       
                            cell?.setStringValue(i?.date ?? "")
                        case "預計下單日":
                            cell?.setStringValue(i?.order_date ?? "")
                        case "提醒日期(由預計下單日推算)":
                            let notifyMode = NotifyMode(rawValue: i?.order_reminder_date ?? "")
                            cell?.setStringValue(notifyMode?.info.description)
                        case "拜訪時間":
                            var period = ""
                            for (index,i) in (i?.interview_period ?? []).enumerated(){
                                if index == 0{
                                    period += i
                                }
                                else{
                                    period += " ; \(i)"
                                }
                            }
                            cell?.setStringValue(period)
                        case "備註":
                            cell?.setStringValue(i?.description ?? "")
                        case "預估成交率":
                            cell?.setIntegerValue(i?.rating ?? 0)
                        case "訪談機台":
                            var machineName = ""
                            for (index,i) in (i?.products ?? []).enumerated(){
                                if index == 0{
                                    machineName += (i.full_name ?? "")
                                }
                                else{
                                    machineName += " ; \(i.full_name ?? "")"
                                }
                            }
                            cell?.setStringValue(machineName)
                        case "折扣":
                            cell?.setStringValue(i?.discount ?? "")
                        case "總金額":
                        
 
                                if let m = i{
                                    cell?.setFloatValue(Float(self.getTotalPrice(info: m)))
                                }
                                
                            
                        
                        default :
                            ()
                        }
                        
                    }
                }

                //spreadSheet?.save()
                spreadSheet?.save()
                let activity = UIActivityViewController(activityItems: [documentPath], applicationActivities: nil)
                
                activity.popoverPresentationController?.sourceView = self.toprightview.stackview.arrangedSubviews[1]
                activity.popoverPresentationController?.sourceRect = self.toprightview.stackview.arrangedSubviews[1].frame

                // 3
                self.present(activity, animated: true, completion: nil)
            }
        }

    }
    func getTotalPrice(info:InterViewInfo) -> Int {
        var total : Int = 0
        for i in info.products {
            var sTotal : Int = 0
            sTotal += (i.getProductsPrices()?.price ?? 0)
            for j in i.options ?? []{
                if j.selected == true {
                    sTotal += (j.getProductsPrices()?.price ?? 0)
                }
            }
            for m in i.audit ?? []{
                for k in m.content {
                    if let price = k.price?.integerValue, let qty = k.qty{
                        sTotal += price * qty
                    }
                }
            }
            total += (sTotal * (i.qty ?? 1))
        }
        return total
    }
//    func calculateTotal(addedMachine:){
//        var total : Double = 0
//
//        for i in addedMachine {
//            if let pr = i.product {
//
//                total += (pr.getProductsPrices()?.price ?? 0)
//
//            }
//
//            for j in i.options {
//                total += j.option?.getProductsPrices()?.price ?? 0
//            }
//            for m in i.customOptions {
//                if m.is_Cancel {
//                    continue
//                }
//                for j in m.options {
//                    total += Double(j.amount) * (j.price.doubleValue ?? 0)
//                }
//            }
//            total = (total * Double(i.qty))
//        }
//        currentTotal = total
//        totalLabel.text = "$ \(total)"
//
//    }
    
//    func shareSelectedInterviews() {
//        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let name = self.company.data.name {
//            let documentPath = documentsDirectory.appendingPathComponent("interview_record/\(name)_訪談紀錄.xlsx")
//        //let spreadSheet =
//    }
    
    func removeDetail(interviews: Interview) {
        if let index = self.selectedDetailArray.firstIndex(where: { (info) -> Bool in
            return info?.id.stringValue == interviews.id
        }) {
            selectedDetailArray.remove(at: index)
        }
        
    }
    
    func didSelectAllDetail() {
        selectedDetailArray = self.detailInfoArray
    }
    
    func removeSelectAll() {
        selectedDetailArray = []
    }
    
    func appendDetail(interview: Interview) {
        if let detail = self.detailInfoArray.first(where: { (info) -> Bool in
            return info.id.stringValue == interview.id
        }) {
            selectedDetailArray.append(detail)
        }
        
        
    }
    
    func goToInterview(id: String) {
        print(id)
//        let vd = InterviewController()
//        vd.mode = .Review
//        vd.id = id
//        
//        vd.modalPresentationStyle = .fullScreen
//                            self.present(vd, animated: true, completion: nil)
        
        
        let vd = PriceHistoryController()
        vd.modalPresentationStyle = .fullScreen
        vd.id = id
        self.present(vd, animated: true, completion: nil)
    }

    var company : Company? {
        didSet{
            topleftview.company = self.company
            toprightview.interviews = self.company?.data?.interviews ?? []
        }
    }
    
    var id : String?
    func showUser() {
        let con = ShowSalesInfoController()
        con.company = topleftview.company
        con.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        con.modalPresentationStyle = .overCurrentContext
        self.present(con, animated: false, completion: nil)
    }
    

    
    let topleftview = CustomerTopLeftView()
    let toprightview = CustomerTopRightView(showbutton: true, title: UserDefaults.standard.getConvertedLanguage() == "en" ? "+ Interview" : "建立訪談")
//    let bottomview = CustomerBottomView(showbutton: true, title: "建立報價")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        titleview.label.text = "客戶資料".localized
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeDirectory()
    }
    func writeToFolder(fileName:String){
        guard let url = Bundle.main.url(forResource: "New", withExtension: "xlsx") else {return}
        if let data = try? Data(contentsOf: url), let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,let _ = try? FileManager.default.createDirectory(at: documentsDirectory.appendingPathComponent("interview_record"), withIntermediateDirectories: true, attributes: nil) {
            
        
        let dataPath = documentsDirectory.appendingPathComponent("interview_record/\(fileName)_訪談紀錄.xlsx")
            

                try? data.write(to: dataPath)
            
        
        }
        

    }
    func removeDirectory(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let interview_record = documentsDirectory.appendingPathComponent("interview_record")

            do {
                try FileManager.default.removeItem(at: interview_record)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    
    @objc func goToCreateInterview(){

       
        
        let vd = InterviewController()
        //要加入
        vd.mode = .New
        if let customer = company?.data {

            vd.interInfo.customer = Customer(id: customer.id, code: customer.code, name: customer.name ?? "", contact: "" , business_number: customer.business_number ?? "", sales: customer.sales)
        }
        vd.view.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.modalPresentationStyle = .fullScreen

        self.present(vd, animated: true, completion: nil)
    }
    @objc func gotoCreatePrice() {
        
        let vd = CreatePriceController()
        vd.stackview = LeftStackView(textarray: ["報價資訊","報價機台","調整價格"])
        vd.mode = .Editing
        vd.view.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.modalPresentationStyle = .fullScreen
        
        self.present(vd, animated: true, completion: nil)
    }
    
    @objc func goCustomerList(){
        let vd = CustomerListController()
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        self.present(vd, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        topleftview.titlelabel.text = "客戶資訊".localized
        topleftview.delegate = self
        toprightview.titlelabel.text = "訪談紀錄".localized
        //bottomview.titlelabel.text = "報價單"
        topleftview.detailButton.addTarget(self, action: #selector(goToEdit), for: .touchUpInside)
        view.addSubview(topleftview)
        topleftview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: menu_bottomInset + 26.calcvaluey(), right: 0),size: .init(width: 478.calcvaluex(), height: 0))
        topleftview.button.addTarget(self, action: #selector(goSeeDetailView), for: .touchUpInside)
        view.addSubview(toprightview)
        toprightview.button.addTarget(self, action: #selector(goToCreateInterview), for: .touchUpInside)
        toprightview.anchor(top: topleftview.topAnchor, leading: topleftview.trailingAnchor, bottom: topleftview.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        
//        view.addSubview(bottomview)
//        bottomview.anchor(top: topleftview.bottomAnchor, leading: topleftview.leadingAnchor, bottom: view.bottomAnchor, trailing: toprightview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 26.calcvaluey(), right: 0))
//        bottomview.button.addTarget(self, action: #selector(gotoCreatePrice), for: .touchUpInside)
        toprightview.delegate = self
        //topleftview.addButton.addTarget(self, action: #selector(goCustomerList), for: .touchUpInside)
        topleftview.dotview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goCustomerList)))
//        topleftview.addButton.addTarget(self, action: #selector(goAddCustomer), for: .touchUpInside)
        
        fetchApi()
    }
    @objc func goSeeDetailView(){
        let vd = AddNewCustomerController()
        vd.company = self.company
        vd.sendOutView.isHidden = true
        vd.addContact.isHidden = true
        vd.detailView.canEdit = false
        vd.contactView.canEdit = false
        vd.modalPresentationStyle = .fullScreen

        self.present(vd, animated: true, completion: nil)
    }
    @objc func goToEdit(){
        let startTime = CFAbsoluteTimeGetCurrent()
        let vd = AddNewCustomerController()
        vd.company = self.company
        vd.currentEditMode = true
        vd.modalPresentationStyle = .fullScreen
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        print(225,timeElapsed)
        self.present(vd, animated: true, completion: nil)
    }
    let hud = JGProgressHUD()
    func fetchApi(){
        self.topleftview.isHidden = true
        self.toprightview.isHidden = true
                
                hud.show(in: self.view)
                NetworkCall.shared.getCall(parameter: "api-or/v1/account/\(id ?? "")", decoderType: Company.self) { (json) in
                    DispatchQueue.main.async {
                        self.topleftview.isHidden = false
                        self.toprightview.isHidden = false
                        
                        if let json = json {
                            self.company = json
                            
                            if json.data?.interviews.count == 0{
                                self.toprightview.mode = .NoInterView
                            }
                            else{
                                self.toprightview.mode = .Normal
                            }
                            
                            self.fetchDetailInterviews()
                            
                        }
                    }
        
                }
    }
    var detailInfoArray = [InterViewInfo]()
    var selectedDetailArray = [InterViewInfo?]()
    func fetchDetailInterviews(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/account_interviews/\(self.company?.data?.code ?? "")", decoderType: DetailInterViewInfosArray.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
                if let json = json {
                    self.detailInfoArray = json.data
                }
            }

        }
    }
    @objc func goAddCustomer(){
        
        let vd = AddCustomerController()
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve

        self.present(vd, animated: true, completion: nil)
    }
}
