//
//  InterviewExcelField.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/28/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
struct InterviewExcelFields {
    var fields = [ExcelField(letter: "A", name: "客戶名稱"),ExcelField(letter: "B", name: "訪談地點"),ExcelField(letter: "C", name: "拜訪日期"),ExcelField(letter: "D", name: "預計下單日"),ExcelField(letter: "E", name: "提醒日期(由預計下單日推算)"),ExcelField(letter: "F", name: "拜訪時間"),ExcelField(letter: "G", name: "備註"),ExcelField(letter: "H", name: "預估成交率"),ExcelField(letter: "I", name: "訪談機台"),ExcelField(letter: "J", name: "折扣"),ExcelField(letter: "K", name: "總金額")]
}

struct ExcelField {
    var letter : String
    var name : String
}
