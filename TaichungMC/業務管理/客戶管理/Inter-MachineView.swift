//
//  Inter-MachineView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit


class selectedMachineView : UIView {
    let machineLabel : PaddedLabel = {
       let label = PaddedLabel()
        label.backgroundColor = MajorColor().oceanColor
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = .white
       // label.text = "高效率線軌 / NP 16 / 2軸"
        label.layer.cornerRadius = 17.calcvaluey()
        label.clipsToBounds = true
        return label
    }()
    let seperator : UIView = {
       let seperator = UIView()
        seperator.backgroundColor = #colorLiteral(red: 0.8346384168, green: 0.8347591162, blue: 0.8346119523, alpha: 1)
        return seperator
    }()
    let cancelButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return button
    }()
    let priceLabel : UILabel = {
       let pL = UILabel()
        pL.textColor = MajorColor().oceanColor
        pL.text = "$145萬"
        pL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        pL.textAlignment = .right
        return pL
    }()
    var cancelButtonAnchor : AnchoredConstraints?
   let counter = CounterView(minCount: 1)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(machineLabel)
        machineLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 27.5.calcvaluey(), left: 36.calcvaluex(), bottom: 27.5.calcvaluey(), right: 0))
        machineLabel.trailingAnchor.constraint(lessThanOrEqualTo: centerXAnchor).isActive = true
       // machineLabel.centerYInSuperview()
        machineLabel.numberOfLines = 0
        addSubview(counter)
        counter.centerYInSuperview()
        counter.anchor(top: nil, leading: machineLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 130.calcvaluex(), height: 50.calcvaluey()))
        addSubview(seperator)
        seperator.anchor(top: nil, leading: machineLabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 28.3.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(cancelButton)
        cancelButtonAnchor = cancelButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        cancelButton.centerYInSuperview()
        
        addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: cancelButton.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 28.calcvaluex()))
        priceLabel.centerYInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class d_discountview : UIView {
    let insertField = customizeDetailTextField()
    let priceField = customizeDetailTextField()
    let cancelButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return button
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        insertField.arrow.isHidden = true
        insertField.attributedPlaceholder = NSAttributedString(string: "請輸入", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        
        let minus = UILabel()
        minus.text = "-"
        minus.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        minus.textColor = MajorColor().oceanColor
        
        priceField.addSubview(minus)
        minus.anchor(top: nil, leading: priceField.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        minus.centerYInSuperview()
        priceField.textColor = MajorColor().oceanColor
        priceField.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        addSubview(insertField)
        insertField.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 500.calcvaluex(), height: 52.calcvaluey()))
        insertField.centerYInSuperview()
        
        addSubview(cancelButton)
        cancelButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        cancelButton.centerYInSuperview()
        
        addSubview(priceField)
        priceField.arrow.isHidden = true
        priceField.anchor(top: insertField.topAnchor, leading: insertField.trailingAnchor, bottom: insertField.bottomAnchor, trailing: cancelButton.leadingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex()))
        priceField.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class discountCustomView : selectedEquipView {
    var addedDiscount = [Discount](){
        didSet{
            stackview.safelyRemoveArrangedSubviews()
            for (index,_) in addedDiscount.enumerated() {
                let vd = d_discountview()
                
                vd.tag = index
//                vd.firstIndex = self.tag
//                vd.delegate = self.delegate
                vd.constrainHeight(constant: 70.calcvaluey())
                vd.cancelButton.tag = index
                vd.cancelButton.addTarget(self, action: #selector(self.removeDiscount), for: .touchUpInside)
                stackview.addArrangedSubview(vd)
            }
        }
    }
    @objc func removeDiscount(sender:UIButton) {
        print(sender.tag)
        delegate?.removeDiscount(index:sender.tag)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //self.addField.label.text = "增加折扣"
        
        self.nameLabel.text = "折扣項目"
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class addedCustomView : selectedEquipView,UITextFieldDelegate {
    
    var firstIndex : Int?
    let modeView = customizeDetailTextField()
    var fileButton : UIButton = {
        let bt = UIButton(type: .custom)
        bt.backgroundColor = .white
        bt.setTitle("合審單附檔", for: .normal)
        bt.layer.cornerRadius = 6.calcvaluex()
        bt.setTitleColor(#colorLiteral(red: 0.3893008302, green: 0.7144385251, blue: 0.1983495111, alpha: 1), for: .normal)
        bt.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        bt.layer.borderWidth = 1.calcvaluex()
        bt.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        return bt
    }()
    var status : Int?
    var isCancel : Bool?
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == modeView {
            delegate?.modifyCustomStatus(index: self.firstIndex ?? 0, subIndex: self.tag,textField : textField)
            return false
        }
        return true
    }
    var addedCustomOptions = [AddedOption](){
        didSet{
            if (mode == .Editing && isCancel == false) || mode == .New {
                modeView.isUserInteractionEnabled = true
                modeView.arrow.isHidden = false
            }
            else{
                modeView.isUserInteractionEnabled = false
                modeView.arrow.isHidden = true
            }
            if stackview.arrangedSubviews.count == addedCustomOptions.count {
               
                return
            }
            stackview.safelyRemoveArrangedSubviews()
            for (index,op) in addedCustomOptions.enumerated() {
                
                let vd = customizingDetailView(mode: mode)
                if index == addedCustomOptions.count - 1{
                    vd.seperator.isHidden = true
                }
                else{
                    vd.seperator.isHidden = false
                }
                vd.mode = mode
                if mode == .New || (mode == .Editing && status == 0) {
                    vd.cancelButton.isHidden = false
                    vd.cancel_view.isHidden = false
                    vd.selectionView.isUserInteractionEnabled = true
                    vd.selectionView.arrow.isHidden = false
                    vd.infoField.isUserInteractionEnabled = true
                    vd.counter.isUserInteractionEnabled = true


                    vd.assetTap.isEnabled = true
                    vd.assetLongTap.isEnabled = true

                }
                else{
                    vd.cancelButton.isHidden = true
                   // vd.cancel_view.isHidden = true
                    vd.selectionView.isUserInteractionEnabled = false
                    vd.selectionView.arrow.isHidden = true
                    vd.infoField.isUserInteractionEnabled = false
                    vd.counter.isUserInteractionEnabled = false
                    if op.files.count > 0{
                        vd.assetTap.isEnabled = true
                    
                    }
                    else{
                        vd.assetTap.isEnabled = false
                    }
                    vd.assetLongTap.isEnabled = false
                }
                
                if op.price.doubleValue == nil{
                    //vd.extraField.priceField.isHidden = true
                    //vd.customField.priceField.isHidden = true
                }
                else{
                   // vd.extraField.priceField.text = "\(op.price)"
                   // vd.customField.priceField.text = "\(op.price)"
                 //   vd.extraField.priceField.isHidden = false
                 //   vd.customField.priceField.isHidden = false
                    
                }
                vd.delegate = self.delegate
                vd.tag = index
                vd.secondIndex = self.tag
                vd.firstIndex = self.firstIndex
                vd.addedOptions = op
                
                if op.reply != "" && statusDict[modeView.text ?? ""] == 5{
                    vd.replyStack.isHidden = false
                    if op.mode == .Date {
                        vd.replayField.text = op.price
                    }
                    else{
                        vd.replayField.text = op.reply
                        
                    }
                    vd.replayField.textViewDidChange(vd.replayField)
                }
                else{
                    vd.replyStack.isHidden = true
                }
               // vd.constrainHeight(constant: 92.calcvaluey())
                vd.cancelButton.tag = index
                vd.cancelButton.addTarget(self, action: #selector(self.removeCustom), for: .touchUpInside)
                stackview.addArrangedSubview(vd)
            }
        }
    }
    @objc func removeCustom(sender:UIButton) {
        print(661)
        
        self.delegate?.removeCustom(index: self.firstIndex ?? 0, subIndex: self.tag,thirdIndex:sender.tag)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        self.addField.label.text = "增加 備註/客製化".localized
        
        self.nameLabel.text = "備註/客製化".localized
        //要加入
        stackviewanchor?.top = nil
        stackview.distribution = .fill
        stackview.alignment = .fill
        addSubview(modeView)
        modeView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: UserDefaults.standard.getLanguage() == "en" ? 280.calcvaluex() : 200.calcvaluex(), bottom: 0, right: 0),size: .init(width: 128.calcvaluex(), height: 52.calcvaluey()))
        stackviewanchor?.top = stackview.topAnchor.constraint(equalTo: modeView.bottomAnchor)
        stackviewanchor?.top?.isActive = true
        modeView.delegate = self
        addSubview(fileButton)
        fileButton.anchor(top: modeView.topAnchor, leading: modeView.trailingAnchor, bottom: modeView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 128.calcvaluex(), height: 0))
        //replayFieldAnchor?.height?.constant = 52.calcvaluey()

        //self.layoutSubviews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        

        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class selectedEquipView : UIView {
    var set: Bool?
    var showing:Bool? {
        didSet{
            if showing == true{
                addField.isHidden = false
            }
            else{
                addField.isHidden = true
            }
            for i in stackview.arrangedSubviews {
                if let st = i as? selectedMachineView {
                    if showing == true
                    {
                        st.cancelButton.isHidden = false
                    }
                    else{
                        st.cancelButton.isHidden = true
                    }
                }

            }
        }
    }
    var mode: InterviewMode?
    var delegate : SelectedMachineCellDelegate?
    let seperator : UIView = {
       let seperator = UIView()
        seperator.backgroundColor = #colorLiteral(red: 0.8346384168, green: 0.8347591162, blue: 0.8346119523, alpha: 1)
        return seperator
    }()
    
    let nameLabel : UILabel = {
       let nameLabel = UILabel()
        nameLabel.text = "選配".localized
        nameLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluey())
        nameLabel.textColor = #colorLiteral(red: 0.4914312959, green: 0.4915053844, blue: 0.4914150834, alpha: 1)
        return nameLabel
    }()
    var selectedOptions = [SelectedOption](){
        didSet{
            
            stackview.safelyRemoveArrangedSubviews()
            for (index,i) in selectedOptions.enumerated() {
                let vd = selectedMachineView()
                //vd.backgroundColor = .red
                if set == true{
                    vd.cancelButtonAnchor?.width?.constant = 0.calcvaluex()
                    vd.cancelButtonAnchor?.trailing?.constant = 5.calcvaluex()
                }
                else{
                    vd.cancelButtonAnchor?.width?.constant = 36.calcvaluex()
                    vd.cancelButtonAnchor?.trailing?.constant = -36.calcvaluex()
                }
                vd.counter.isHidden = true
                if mode == .Editing || mode == .New{
                    vd.cancelButton.isHidden = false
                }
                else{
                    vd.cancelButton.isHidden = true
                }
                vd.machineLabel.text = i.name
                let totalPrice = (i.option?.getProductsPrices()?.price ?? 0) * (i.qty)
                vd.priceLabel.text = "$\(totalPrice)"
                
//                vd.machineLabel.backgroundColor = .clear
//                vd.machineLabel.textColor = #colorLiteral(red: 0.1123098508, green: 0.07669112831, blue: 0.06499900669, alpha: 1)
//                vd.machineLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
                //vd.machineLabel.textAlignment = .left
                vd.seperator.isHidden = true
                //vd.constrainHeight(constant: .calcvaluey())
                vd.cancelButton.tag = index
                vd.cancelButton.addTarget(self, action: #selector(removeOptions), for: .touchUpInside)
                stackview.addArrangedSubview(vd)
            }
            
            
        }
    }
    @objc func removeOptions(sender:UIButton) {
        self.delegate?.removeOption(index: self.tag, subIndex: sender.tag)
    }
    let addField = AddButton4()
    let stackview : UIStackView = {
       let stack = UIStackView()
        
        stack.distribution = .fillEqually
        stack.axis = .vertical
        return stack
    }()
   //要加入
    var stackviewanchor: AnchoredConstraints?
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 28.3.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))

        
        
//        addSubview(nameLabel)
//        nameLabel.anchor(top: topAnchor, leading: seperator.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        addField.titles.text = "加入選配"
        addSubview(addField)
        addField.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 33.calcvaluex(), bottom: 0, right: 33.calcvaluex()))
        
        addSubview(stackview)
        
        //要加入
        stackviewanchor = stackview.anchor(top: addField.bottomAnchor, leading: leadingAnchor, bottom: seperator.topAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 20.calcvaluey(), right: 0))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
