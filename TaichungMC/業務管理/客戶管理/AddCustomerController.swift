//
//  AddCustomerController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct FormData {
    let header : String
    let subheader : String
}
class AddCustomerController: UIViewController {
    let container = UIView()
    let customerheader = UILabel()
    let registString = [ FormData(header: "客戶公司", subheader: "久大行銷股份有限公司"),FormData(header: "姓名", subheader: "請輸入姓名") ,FormData(header: "帳號", subheader: "請輸入信箱"),FormData(header: "密碼", subheader: "請輸入8碼以上數字與英文組合"),FormData(header: "確認密碼", subheader:"請再次輸入密碼") ]
    let registbutton = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        container.layer.cornerRadius = 15.calcvaluex()
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 695.calcvaluey()))
        
        let xbutton = UIButton(type: .custom)
        xbutton.imageView?.contentMode = .scaleAspectFill
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        
        customerheader.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        customerheader.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        customerheader.text = "註冊客戶帳號"
        
        container.addSubview(customerheader)
        customerheader.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        customerheader.centerXInSuperview()
        
        let stackview = UIStackView()
        stackview.spacing = 16.calcvaluey()
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        
        registString.forEach { (st) in
            let vt = AddMachineFormView(text: st.header, placeholdertext: st.subheader)
            
            stackview.addArrangedSubview(vt)
        }
        container.addSubview(stackview)
        stackview.anchor(top: customerheader.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 104.calcvaluey(), right: 43.calcvaluex()))
        
        registbutton.setTitle("註冊", for: .normal)
        registbutton.setTitleColor(.white, for: .normal)
        registbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        registbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        registbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.addSubview(registbutton)
        registbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluex(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        registbutton.centerXInSuperview()
        
        registbutton.layer.cornerRadius = 48.calcvaluey()/2
    }
    
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
