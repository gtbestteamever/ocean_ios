//
//  InterviewInfoViewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/26/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class InterviewInfoViewController: UIViewController {
    let container = UIView()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        return xbutton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.backgroundColor = #colorLiteral(red: 0.8796791561, green: 0.8796791561, blue: 0.8796791561, alpha: 1)
        container.layer.cornerRadius = 6.calcvaluex()
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 600.calcvaluex(), height: 500.calcvaluey()))
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 32.calcvaluex(), height: 32.calcvaluex()))
        xbutton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
}
