//
//  AttachFileWindowController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class AttachFileWindowController: UIViewController {
    var filePaths = [FilePath]()
    let container = UIView()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()
    init(attachFiles:[FilePath]){
        super.init(nibName: nil, bundle: nil)
        self.filePaths = attachFiles
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func sendFile(remote: String) {
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

    
        vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        guard let url = URL(string: remote) else {return}
        url.loadWebview(webview: vd.imgv)
        //vd.imgv.loadRequest(URLRequest(url: url))

        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
        
        
    }
    func sendFile(file: Files?) {
        
        
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

    
        vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        
        
        if let mt = file {
            if let g = mt.getLang() {
                
                if g.count > 0 {
                    print(232,g[0].path)
                    switch g[0].path {
                    
                    case .string(let ft):
                        if let a = ft {
                            //fileString = a
                            guard let url = a.convertTofileUrl() else {return}

                            print(3321,url)
                            let request = URLRequest(url: url)
                            url.loadWebview(webview: vd.imgv)
                            //vd.imgv.loadRequest(request)
                        }
                    case .arrayString(let st):
                        if let a = st?.first{
                            guard let url = a?.convertTofileUrl() else {return}

                            print(3321,url)
                            let request = URLRequest(url: url)
                            url.loadWebview(webview: vd.imgv)
                        }
                            //vd.imgv.loadRequest(request)                        }
                    default:
                        ()
                    }
                }
            }
        }
        else{
            vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        }
        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
        

    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let height = 50.calcvaluex() + (CGFloat(filePaths.count) * 50.calcvaluey())
        
        container.backgroundColor = #colorLiteral(red: 0.8796791561, green: 0.8796791561, blue: 0.8796791561, alpha: 1)
        container.layer.cornerRadius = 6.calcvaluex()
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 600.calcvaluex(), height: height))
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        xbutton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        tableview.backgroundColor = .clear
        tableview.dataSource = self
        tableview.delegate = self
        
        container.addSubview(tableview)
        tableview.anchor(top: xbutton.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 6.calcvaluex(), bottom: 6.calcvaluey(), right: 6.calcvaluex()))
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
}

extension AttachFileWindowController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filePaths.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        cell.textLabel?.text = filePaths[indexPath.row].title
        cell.textLabel?.textAlignment = .center
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filePath = filePaths[indexPath.row]
        print(filePath)
        if let remote = filePath.remote {
         sendFile(remote: remote)
        }
        else{
        sendFile(file: filePaths[indexPath.row].files)
        }
    }
}
