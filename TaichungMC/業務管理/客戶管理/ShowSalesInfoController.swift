//
//  ShowSalesInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/13.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ShowSalesInfoController: UIViewController {
    let newVd = UIView()
    let closeButton = UIButton(type: .custom)
    let salesHeader = SalesHeaderView()
    let numberView = SalesFormView(text: "編號".localized)
    let unitView = SalesFormView(text: "單位".localized)
    let mailView = SalesFormView(text: "信箱".localized)
    let accountView = SalesFormView(text: "帳號".localized)
    var company : Company?
    override func viewDidLoad() {
        super.viewDidLoad()
        newVd.addshadowColor(color: .white)
        newVd.backgroundColor = .white
        view.addSubview(newVd)
        newVd.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 500.calcvaluey()))
        newVd.layer.cornerRadius = 10.calcvaluex()
        
        
        closeButton.contentVerticalAlignment = .fill
        closeButton.contentHorizontalAlignment = .fill

        closeButton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        closeButton.addTarget(self, action: #selector(closing), for: .touchUpInside)
        
        
        for (index,i) in [numberView,unitView,mailView].enumerated() {
            i.constrainWidth(constant: 486.calcvaluex())
            i.constrainHeight(constant: 90.calcvaluey())
//            i.textfield.text = ["2488","國內營業","2dasd@gmail.com"][index]
        }
        salesHeader.constrainHeight(constant: 92.calcvaluey())
        let spacer = UIView()
        let spacer1 = UIView()
        spacer1.constrainHeight(constant: 14.calcvaluey())
        
        let spacer2 = UIView()
        let spacer3 = UIView()
        let spacer4 = UIView()
        for i in [spacer2,spacer3,spacer4] {
            i.constrainHeight(constant: 12.calcvaluey())
        }
        
        
        let stackview = UIStackView(arrangedSubviews: [salesHeader,spacer1,numberView,spacer2,unitView,spacer3,mailView,spacer4,UIView()])
        stackview.axis = .vertical
        stackview.alignment = .center
        stackview.distribution = .fill
        newVd.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 34.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        newVd.addSubview(closeButton)
        closeButton.anchor(top: newVd.topAnchor, leading: nil, bottom: nil, trailing: newVd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(company?.data)
        salesHeader.nameLabel.text = self.company?.data?.sales?.name
        numberView.textfield.text = self.company?.data?.sales?.code
        unitView.textfield.text = self.company?.data?.sales?.department_name
        mailView.textfield.text = self.company?.data?.sales?.email
    }
    @objc func closing(){
        print(3321)
        self.dismiss(animated: false, completion: nil)
    }
}
