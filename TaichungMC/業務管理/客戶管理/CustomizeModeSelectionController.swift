//
//  CustomizeModeSelectionController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
let statusDict = ["草稿".localized : 0, "新合審".localized:1,"待回覆".localized:2 ,"待審核".localized:3, "待定價".localized :4 ,"完成".localized:5, "已取消".localized:6]
let dictStatus = [ 0 : "草稿", 1:"新合審",2:"待回覆", 3:"待審核", 4:"待定價"  ,5:"完成", 6:"已取消"]
let colorStataus = [ "草稿".localized : #colorLiteral(red: 0.6621296406, green: 0.662226975, blue: 0.6621083617, alpha: 1), "新合審".localized:#colorLiteral(red: 0.03734050319, green: 0.328607738, blue: 0.5746202469, alpha: 1),"待回覆".localized:#colorLiteral(red: 0.1008876637, green: 0.5688499212, blue: 0.5781469941, alpha: 1), "待審核".localized:#colorLiteral(red: 0.9513289332, green: 0.6846997142, blue: 0.1314668059, alpha: 1), "待定價".localized:#colorLiteral(red: 0.2379338741, green: 0.6737315059, blue: 0.9689859748, alpha: 1)  ,"完成".localized:MajorColor().oceanColor, "已取消".localized:#colorLiteral(red: 0.4757143259, green: 0.4757863283, blue: 0.4756985307, alpha: 1)]


enum NotifyMode : String {
    case OneDay = "1"
    case ThreeDay = "3"
    case SevenDay = "7"
    
    var info : (code:Int,description:String) {
        switch self{
        case .OneDay:
            return (1,"一天前")
        case .ThreeDay:
            return (3,"三天前")
        case .SevenDay:
            return (7,"七天前")
        }
    }
}
enum AddressType {
    case Ocean
    case Customer
    case Other
    
    var info : String {
        switch self{
        case .Ocean:
            return "歐群端".localized
        case .Customer:
            return "客戶端".localized
        case .Other:
            return "其他".localized
        }
    }
}
protocol CustomizeModeDelegate {
    func didSelect(mode : AddedMode, index:Int,subIndex:Int,thirdIndex:Int)
    func setCustomer(customer: Customer)
    
    func setCustomStatus(index:Int,subIndex: Int, status : Int)
    func setNotifyDate(notifyMode:NotifyMode)
    func setAddressType(mode:AddressType)
}
class CustomizeModeSelectionController : UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotify {
            return notifyarry.count
        }
        if isCustomers {
            return customersList.count
        }
        else if isStatus {
            return statusArray.count
        }
        
        if isAddressType {
            return addressType.count
        }
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        cell.textLabel?.numberOfLines = 2
        if isCustomers {
            cell.textLabel?.text = customersList[indexPath.row].name
        }
        else if isNotify {
            cell.textLabel?.text = notifyarry[indexPath.row].info.description
        }
        else if isStatus {
            cell.textLabel?.text = statusArray[indexPath.row]
            cell.textLabel?.textColor = colorStataus[statusArray[indexPath.row]]
        }
        else if isAddressType {
            cell.textLabel?.text = addressType[indexPath.row].info
            
        }
        else{
        if array[indexPath.row] == .Date {
            cell.textLabel?.text = "交期".localized
        }
        else if array[indexPath.row] == .Custom {
            cell.textLabel?.text = "客製".localized
        }
        else{
            cell.textLabel?.text = "附件".localized
        }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCustomers {
            delegate?.setCustomer(customer: customersList[indexPath.row])
        }
        else if isStatus {
            
            delegate?.setCustomStatus(index:self.firstIndex ?? 0,subIndex: self.secondIndex ?? 0, status : statusDict[statusArray[indexPath.row]] ?? 0)
        }
        else if isNotify {
            delegate?.setNotifyDate(notifyMode: self.notifyarry[indexPath.row])
        }
        else if isAddressType {
            delegate?.setAddressType(mode: addressType[indexPath.row])
        }
        else{
            delegate?.didSelect(mode: array[indexPath.row], index: self.firstIndex ?? 0, subIndex: self.secondIndex ?? 0,thirdIndex: self.tag)
        }
    }
    var isStatus : Bool = false
    var isNotify : Bool = false
    var isAddressType : Bool = false
    var statusArray = [String]()
    let tableView = UITableView(frame: .zero, style: .plain)
    var delegate : CustomizeModeDelegate?
    var array : [AddedMode] = [.Date,.Custom,.Extra]
    var notifyarry : [NotifyMode] = [.OneDay,.ThreeDay,.SevenDay]
    var addressType : [AddressType] = [.Ocean,.Customer,.Other]
    var isCustomers : Bool = false
    var customersList = [Customer]()
    var firstIndex : Int?
    var secondIndex : Int?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor()
        backgroundColor = .white
        
        addSubview(tableView)
        tableView.showsVerticalScrollIndicator = false
        tableView.fillSuperview()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
