//
//  Inter-ExtraView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import GrowingTextView
class sigField : UIView {
    let label : UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
        return label
    }()
    let imageView = UIImageView()
    let addSigButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        button.setTitle("新增", for: .normal)
        button.layer.cornerRadius = 16.calcvaluey()
        return button
    }()
    let importButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = MajorColor().oceanColor
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        button.setTitle("匯入", for: .normal)
        button.layer.cornerRadius = 16.calcvaluey()
        return button
    }()
    var mode : InterviewMode?
    var signi : interViewFile? {
        didSet{
            if signi?.image == nil {
                if signi?.id == "" {
                    imageView.isHidden = true
                    if mode == .Editing || mode == .New{
                        addSigButton.isHidden = false
                        importButton.isHidden = false
                    }
                    else{
                        addSigButton.isHidden = true
                        importButton.isHidden = true
                    }
                    
                    
                }
                else{
                    if signi?.path_url != "" , let url = URL(string: signi?.path_url ?? ""){
                        imageView.sd_setImage(with: url, completed: nil)
                    }
                    else{
                        imageView.image = signi?.image
                    }
                    imageView.isHidden = false
                    addSigButton.isHidden = true
                    importButton.isHidden = true
                }
            }
            else{
                imageView.image = signi?.image
                imageView.isHidden = false
                addSigButton.isHidden = true
                importButton.isHidden = true
            }
        }
    }
    let xbutton = UIButton(type: .custom)
    init(text:String) {
        super.init(frame: .zero)
        
        label.text = text
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        
        addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.addshadowColor()
        imageView.backgroundColor = .white
        imageView.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
        imageView.isUserInteractionEnabled = true
        xbutton.setImage(#imageLiteral(resourceName: "ic_multiplication"), for: .normal)
        imageView.addSubview(xbutton)
        xbutton.anchor(top: imageView.topAnchor, leading: nil, bottom: nil, trailing: imageView.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 2.calcvaluex()),size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        imageView.isHidden = true
        addSubview(addSigButton)
        addSigButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 9.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 32.calcvaluey()))
        addSigButton.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        
        addSubview(importButton)
        importButton.anchor(top: addSigButton.topAnchor, leading: addSigButton.trailingAnchor, bottom: addSigButton.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class InsertCustomizationView : UIView {
    //let nameField = customizeDetailTextField()
    let counter = CounterView(minCount: 1)
    let priceField = customizeDetailTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
//        addSubview(nameField)
//        nameField.arrow.isHidden = true
//        nameField.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 226.calcvaluex(), height: 52.calcvaluey()))
//        nameField.centerYInSuperview()
//        nameField.attributedPlaceholder = NSAttributedString(string: "輸入品項",attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        addSubview(counter)
        counter.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 114.calcvaluex(), height: 36.calcvaluey()))
        counter.centerYInSuperview()
        addSubview(priceField)
        priceField.arrow.isHidden = true
        priceField.attributedPlaceholder = NSAttributedString(string: "輸入價錢",attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        priceField.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        priceField.textColor = MajorColor().oceanColor
        priceField.anchor(top: nil, leading: counter.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 52.calcvaluey()))
        priceField.centerYInSuperview()
        priceField.isUserInteractionEnabled = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PaddingTextField : UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
}
class customizeDetailTextField : PaddingTextField {
    let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 15.calcvaluey()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
       // text = "交期"
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        
        addSubview(arrow)
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 10.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        arrow.centerYInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customizingDetailView : UIView,UITextFieldDelegate,FormTextViewDelegate,InfoTextViewDelegate{
    func textViewReloadHeight(height: CGFloat) {
        //
    }
    

    
    func textViewReloadHeight(height: CGFloat,mode:FormTextViewMode?) {
        var ht = height
        if mode == .Info{
        if addedOptions?.mode == .Date{
            if height < 52.calcvaluey() {
                ht = 52.calcvaluey()
            }
        }
        else{
            if height < 92.calcvaluey() {
                ht = 92.calcvaluey()
            }
        }
        infofieldAnchor?.constant = ht
        }
        else{
            if height < 52.calcvaluey() {
                ht =  52.calcvaluey()
            }
            replyAnchor?.constant = ht
        }
        
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        delegate?.sendCustomName(index:self.firstIndex ?? 0,subIndex:self.secondIndex ?? 0,thirdIndex: self.tag,text:text)
    }
    

    var secondIndex : Int?
    var mode: InterviewMode? {
        didSet{
            if mode == .Review {
                dateField.attributedPlaceholder = nil
                infoField.attributedPlaceholder = nil
            }
        }
    }
    var delegate : SelectedMachineCellDelegate?
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == selectionView {
            delegate?.sendCustomChoice(index:self.firstIndex ?? 0,subIndex: self.secondIndex ?? 0,thirdIndex:self.tag,textField:textField)
            return false
        }
        
        if textField == fileAssetView {
            return false
        }
        

        return true
    }

    var addedOptions : AddedOption? {
        didSet{
            if let price = addedOptions?.price, let d_price = price.doubleValue ,addedOptions?.mode != .Date{

                
                priceField.isHidden = false
                priceField.text = "$ \(d_price)"
            }
           // priceField.isHidden = false
          
            //print(331,infoField.frame.width)
            counter.currentCount = addedOptions?.amount ?? 1
            counter.delegate = self.delegate
            counter.firstIndex = firstIndex
            counter.tag = self.tag
            counter.subIndex = self.secondIndex
            infoField.text = addedOptions?.text
            
            if let _ = addedOptions?.files.first {
                fileAssetView.tintColor = MajorColor().oceanColor
            }
            else{
                fileAssetView.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
            }
            if addedOptions?.mode == .Custom {
                selectionView.text = "客製".localized
                //infofieldAnchor?.constant = 92.calcvaluey()
               // infoField.minHeight = 92.calcvaluey()
                
                counter.isHidden = false
                infoField.isHidden = false


                
            }
            else if addedOptions?.mode == .Date
            {
               // infofieldAnchor?.constant = 52.calcvaluey()
               // infoField.minHeight = 52.calcvaluey()
                
                counter.isHidden = true
                selectionView.text = "交期".localized

            }
            else{
                //infofieldAnchor?.constant = 92.calcvaluey()
              //  infoField.minHeight = 92.calcvaluey()
               
                counter.isHidden = false
                infoField.isHidden = false
                selectionView.text = "附件".localized

            }
            if mode == .Review{
                // 482
                // 307
                replayField.n_width = 506.calcvaluex()
                if addedOptions?.mode == .Date {
                    infoField.n_width = 454.calcvaluex()
                }
                else{

                    infoField.n_width = 328.calcvaluex()
                }
            }
            else{
                // 433
                //404
                replayField.n_width = 555.calcvaluex()
                if addedOptions?.mode == .Date {
                    infoField.n_width = 503.calcvaluex()
                }

                else{
                    infoField.n_width = 377.calcvaluex()
                }
            }
            self.infoField.textViewDidChange(infoField)
           self.replayField.textViewDidChange(replayField)
            
        }
    }

    var firstIndex : Int?
    let selectionView = customizeDetailTextField()
    let cancelButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return button
    }()
    let fileAssetView = UIImageView()
    let dateField = customizeDetailTextField()
    let customField = InsertCustomizationView()
    let extraField = GrowingTextView()
    var replayField = FormInfoField(placeText: "", mode: .Reply)
    var infoField = FormInfoField(placeText: "請輸入品項".localized, mode: .Info)
    lazy var assetTap = UITapGestureRecognizer(target: self, action: #selector(addAsset))
    lazy var assetLongTap = UILongPressGestureRecognizer(target: self, action: #selector(removeAsset))

    @objc func addAsset(){
        print(6621,mode)
        if let firstIndex = firstIndex, let subIndex = secondIndex {
            
            if let st = addedOptions?.files.first {
                delegate?.showCustomAttachFile(file:st)
            }
            else{
                if mode == .New || mode == .Editing{
            delegate?.addCustomFile(index: firstIndex, subIndex: subIndex, thirdIndex: self.tag, textField: fileAssetView)
                }
            }
        }
        
    }
    @objc func removeAsset(sender:UILongPressGestureRecognizer){
        
        if sender.state == .began {
            if let firstIndex = firstIndex, let subIndex = secondIndex , let _ = addedOptions?.files.first,mode == .New || mode == .Editing{
                delegate?.removeCustomFile(index: firstIndex, subIndex: subIndex, thirdIndex: self.tag, textField: fileAssetView,addedFile: self.addedOptions?.files.first ?? nil)
        }
        }
    }
    let h_stack = UIStackView()
    let v_stack = UIStackView()
    let counter = CounterView(minCount: 1)
    let priceField = customizeDetailTextField()
    let cancel_view = UIView()
    var infofieldAnchor:NSLayoutConstraint?
    var replyAnchor : NSLayoutConstraint?
    let t_stack = UIStackView()
    let seperator = UIView()
    let replyStack = UIStackView()
    func setView() {
        
        selectionView.delegate = self
        let imageContentView = UIView()
        fileAssetView.isUserInteractionEnabled = true
        fileAssetView.image = #imageLiteral(resourceName: "ic_attach_file_message").withRenderingMode(.alwaysTemplate)
                
                fileAssetView.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
                fileAssetView.contentMode = .scaleAspectFit

                fileAssetView.addGestureRecognizer(assetTap)
        fileAssetView.addGestureRecognizer(assetLongTap)

        imageContentView.addSubview(fileAssetView)
        imageContentView.constrainWidth(constant: 40.calcvaluex())
        imageContentView.constrainHeight(constant: 40.calcvaluey())
        fileAssetView.constrainWidth(constant: 32.calcvaluex())
        fileAssetView.constrainHeight(constant: 32.calcvaluey())
fileAssetView.centerYInSuperview()
fileAssetView.centerXInSuperview()
        let con = UIView()
        con.addSubview(selectionView)
        selectionView.anchor(top: con.topAnchor, leading: con.leadingAnchor, bottom: nil, trailing: con.trailingAnchor,size: .init(width: 0, height: 52.calcvaluey()))
        //con.constrainWidth(constant: 128.calcvaluex())
//        let red = UIView()
//        red.backgroundColor = .red
//
//       // red.constrainHeight(constant: 52.calcvaluex())
//        red.constrainWidth(constant: 128.calcvaluex())
        priceField.arrow.isHidden = true
        priceField.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        priceField.text = "$ 2000000"
        priceField.textColor = MajorColor().oceanColor
        priceField.constrainHeight(constant: 40.calcvaluey())
//        let blue = UIView()
//        blue.backgroundColor = .blue
        //let green = UIView()
        counter.constrainWidth(constant: 130.calcvaluex())
        counter.constrainHeight(constant: 36.calcvaluex())
        priceField.isHidden = true
        counter.isHidden = true
        
        let topstackview = UIStackView()
        topstackview.axis = .horizontal
        topstackview.distribution = .fill
        topstackview.spacing = 12.calcvaluex()
        topstackview.alignment = .center
        topstackview.addArrangedSubview(imageContentView)
        topstackview.addArrangedSubview(counter)
        
        let rightStackView = UIStackView()
        
        rightStackView.axis = .vertical
    
        rightStackView.distribution = .fill
        rightStackView.alignment = .trailing
        rightStackView.spacing = 8.calcvaluey()
        rightStackView.addArrangedSubview(topstackview)
        rightStackView.addArrangedSubview(priceField)
        rightStackView.addArrangedSubview(UIView())
        
        let mainStackview = UIStackView()
        
        mainStackview.distribution = .fill
        mainStackview.alignment = .fill
        mainStackview.spacing = 12.calcvaluex()
        mainStackview.axis = .horizontal
        
        cancel_view.isUserInteractionEnabled = true
        cancel_view.addSubview(cancelButton)
        cancelButton.centerInSuperview(size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        cancel_view.constrainWidth(constant: 36.calcvaluex())

        mainStackview.addArrangedSubview(con)

        
        mainStackview.addArrangedSubview(infoField)
        mainStackview.addArrangedSubview(rightStackView)
 

        let f_stack = UIStackView()
        f_stack.distribution = .fill
        f_stack.alignment = .fill
        f_stack.spacing = 8.calcvaluey()
        f_stack.axis = .vertical

        replyStack.distribution = .fill
        replyStack.alignment = .fill
        replyStack.spacing = 12.calcvaluex()
        replyStack.axis = .horizontal
        replyStack.isHidden = true
        
        replayField.q_delegate = self
        let replyText = UILabel()
        replyText.text = "回覆 :"
        replyText.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        replyText.textColor = .black
        replyText.textAlignment = .right
        let con3 = UIView()
        con3.addSubview(replyText)
        replyText.anchor(top: con3.topAnchor, leading: con3.leadingAnchor, bottom: nil, trailing: con3.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        con3.constrainWidth(constant: 128.calcvaluex())
        replyStack.addArrangedSubview(con3)
        replyStack.addArrangedSubview(replayField)
        f_stack.addArrangedSubview(mainStackview)
        f_stack.addArrangedSubview(replyStack)

        
        t_stack.distribution = .fill
        t_stack.alignment = .center
        t_stack.spacing = 12.calcvaluex()
        t_stack.axis = .horizontal

        t_stack.addArrangedSubview(f_stack)
        t_stack.addArrangedSubview(cancelButton)

        addSubview(t_stack)
//        t_stack.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 36.calcvaluex(), bottom: 12.calcvaluey(), right: 36.calcvaluex()))
        t_stack.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        //replayField.isHidden = true
        replayField.isUserInteractionEnabled = false
        replyAnchor = replayField.heightAnchor.constraint(equalToConstant: 52.calcvaluey())
        replyAnchor?.isActive = true
        infoField.q_delegate = self
        infoField.t_delegate = self

        infofieldAnchor = infoField.heightAnchor.constraint(equalToConstant: 52.calcvaluey())
        infofieldAnchor?.isActive = true


        
        infoField.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        
        seperator.backgroundColor = #colorLiteral(red: 0.8778060241, green: 0.8778060241, blue: 0.8778060241, alpha: 1)
        
        addSubview(seperator)
        seperator.anchor(top: t_stack.bottomAnchor, leading: t_stack.leadingAnchor, bottom: bottomAnchor, trailing: t_stack.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 1.calcvaluey()))
        
    }
    init(mode: InterviewMode?) {
        super.init(frame: .zero)
        setView()
        
//        fileAssetView.setImage(#imageLiteral(resourceName: "ic_attach_file_message").withRenderingMode(.alwaysTemplate), for: .normal)
//        fileAssetView.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
//        
//        fileAssetView.contentVerticalAlignment = .fill
//        fileAssetView.contentHorizontalAlignment = .fill
//        fileAssetView.addGestureRecognizer(assetTap)
//        fileAssetView.addGestureRecognizer(assetLongTap)
//        
//        h_stack.spacing = 16.calcvaluex()
//        h_stack.axis = .horizontal
//        h_stack.distribution = .fill
//        h_stack.alignment = .center
//        h_stack.addArrangedSubview(fileAssetView)
//        h_stack.addArrangedSubview(selectionView)
//        h_stack.addArrangedSubview(dateField)
//        h_stack.addArrangedSubview(customField)
//        h_stack.addArrangedSubview(extraField)
//        h_stack.addArrangedSubview(cancelButton)
//        
//        v_stack.spacing = 8.calcvaluey()
//        v_stack.axis = .vertical
//        v_stack.distribution = .fill
//        v_stack.alignment = .fill
//        v_stack.addArrangedSubview(h_stack)
//        v_stack.addArrangedSubview(infoField)
//        v_stack.addArrangedSubview(replayField)
//
//
//        infoField.constrainHeight(constant: 52.calcvaluey())
//        infoField.delegate = self
//        infoField.isHidden = true
//        infoField.arrow.isHidden = true
//        infoField.attributedPlaceholder = NSAttributedString(string: "輸入品項",attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
//        fileAssetView.constrainWidth(constant: 40.calcvaluex())
//        fileAssetView.constrainHeight(constant: 40.calcvaluey())
//        
//        
//        addSubview(v_stack)
//        v_stack.fillSuperview(padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
//
//        selectionView.delegate = self
//        selectionView.constrainHeight(constant: 52.calcvaluex())
//        selectionView.constrainWidth(constant: 128.calcvaluex())
//        
//        
//        cancelButton.constrainHeight(constant: 36.calcvaluex())
//        cancelButton.constrainWidth(constant: 36.calcvaluex())
//
//        dateField.constrainHeight(constant: 52.calcvaluey())
//        customField.constrainHeight(constant: 52.calcvaluey())
//        extraField.constrainHeight(constant: 52.calcvaluey())
//        dateField.text = ""
//        dateField.attributedPlaceholder = NSAttributedString(string: "請告知", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)])
//        dateField.arrow.isHidden = true
//
//        customField.isHidden = true
//
//        extraField.isHidden = true
//
//        
//        replayField.isUserInteractionEnabled = false
//        replayField.arrow.isHidden = true
//        replayField.constrainHeight(constant: 52.calcvaluey())
//
//        replayField.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class PaddedLabel : UILabel {
    override var intrinsicContentSize: CGSize {
        let contentsize = super.intrinsicContentSize
        return .init(width: contentsize.width + 64.calcvaluex(), height: contentsize.height + 16.calcvaluey())
    }
}

class PaddedLabel2 : UILabel {
    override var intrinsicContentSize: CGSize {
        let contentsize = super.intrinsicContentSize
        return .init(width: contentsize.width + 32.calcvaluex(), height: contentsize.height + 16.calcvaluey())
    }
}

class CounterView : UIView , UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let st = textField.text?.integerValue, st > 1 {
            currentCount = st
            if let firstIndex = self.firstIndex,let subIndex = self.subIndex {
                delegate?.setCustomCount(index:firstIndex,subIndex:subIndex,thirdIndex: self.tag,amount:currentCount)
                return
            }
            if let firstIndex = self.firstIndex {
                delegate?.setMachineQty(index: firstIndex,qty:currentCount)
            }
        }
        else{
            numberLabel.text = "\(currentCount)"
        }
    }
    var delegate : SelectedMachineCellDelegate?
    //var editMachinedelegate:EditMachineDelegate?
    let sep1 = UIView()
    
    let sep2 = UIView()
    var firstIndex : Int?
    var subIndex : Int?
    let numberLabel : UITextField = {
       let nL = UITextField()
        //nL.text = "1"
        nL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        nL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nL.textAlignment = .center
        return nL
    }()
    let addCounterButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("+", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 22.calcvaluex())
        button.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
        return button
    }()
    let minusCounterButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("-", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 22.calcvaluex())
        button.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
        return button
    }()
    
    var currentCount = 0 {
        didSet{
            if currentCount < minCount {
                currentCount = minCount
            }
            numberLabel.text = "\(currentCount)"
        }
    }
    var minCount = 0
    init(minCount : Int) {
        super.init(frame: .zero)
        self.minCount = minCount
        self.currentCount = minCount
        numberLabel.text = "\(minCount)"
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.9008185267, green: 0.9009482265, blue: 0.9007901549, alpha: 1)
        layer.cornerRadius = 25.calcvaluey()
        sep1.backgroundColor = #colorLiteral(red: 0.9111731052, green: 0.9113041759, blue: 0.9111443758, alpha: 1)
        sep2.backgroundColor = #colorLiteral(red: 0.9111731052, green: 0.9113041759, blue: 0.9111443758, alpha: 1)
        
        addSubview(sep1)
        sep1.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding:.init(top: 0, left: 41.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        addSubview(sep2)
        sep2.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding:.init(top: 0, left: 0, bottom: 0, right: 41.calcvaluex()),size: .init(width: 1.calcvaluex(), height: 0))
        
        addSubview(numberLabel)
        numberLabel.anchor(top: topAnchor, leading: sep1.trailingAnchor, bottom: bottomAnchor, trailing: sep2.leadingAnchor)
        numberLabel.delegate = self
        addSubview(minusCounterButton)
        minusCounterButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: sep1.leadingAnchor)
        
        addSubview(addCounterButton)
        addCounterButton.anchor(top: topAnchor, leading: sep2.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        minusCounterButton.addTarget(self, action: #selector(minusCount), for: .touchUpInside)
        addCounterButton.addTarget(self, action: #selector(addCount), for: .touchUpInside)
    }
    @objc func minusCount(){
        currentCount = currentCount - 1
        
        if currentCount < minCount {
            currentCount = minCount
        }
        
        self.numberLabel.text = "\(currentCount)"
        if let firstIndex = self.firstIndex,let subIndex = self.subIndex {
            delegate?.setCustomCount(index:firstIndex,subIndex:subIndex,thirdIndex: self.tag,amount:currentCount)
            return
        }
        if let firstIndex = self.firstIndex {
            delegate?.setMachineQty(index: firstIndex,qty:currentCount)
        }

        
        
        
    }
    @objc func addCount(){
       
        currentCount = currentCount + 1
        self.numberLabel.text = "\(currentCount)"
       // print(firstIndex,subIndex,delegate)
        if let firstIndex = self.firstIndex,let subIndex = self.subIndex {
            delegate?.setCustomCount(index:firstIndex,subIndex:subIndex,thirdIndex: self.tag,amount:currentCount)
            return
        }
        if let firstIndex = self.firstIndex {
            delegate?.setMachineQty(index: firstIndex,qty:currentCount)
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class addExtraField : UIView {
    let addButton = UIButton(type: .custom)
    let label : UILabel = {
       let lB = UILabel()
        lB.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        lB.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return lB
    }()
    init(text: String) {
        super.init(frame: .zero)
        label.text = text
        addButton.setImage(#imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate), for: .normal)
        addButton.tintColor = .white
        addButton.backgroundColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        addButton.layer.cornerRadius = 12.calcvaluey()

        addSubview(addButton)
        addButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        //addButton.centerYInSuperview()
        
        addSubview(label)
        label.anchor(top: nil, leading: addButton.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
