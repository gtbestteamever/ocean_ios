//
//  InterviewController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import ZIPFoundation
import CoreLocation
import MapKit
import AVFoundation
import Photos
import JGProgressHUD

extension String {
     struct NumFormatter {
         static let instance = NumberFormatter()
     }

     var doubleValue: Double? {
         return NumFormatter.instance.number(from: self)?.doubleValue
     }

     var integerValue: Int? {
         return NumFormatter.instance.number(from: self)?.intValue
     }
}
enum InterviewMode {
    case Editing
    case addPhoto
    case Normal
    case Review
    case New
}

enum FieldMode{
    case Info
    
    case Machine
    
    case Discount
    
    case Sign
}
class InterviewController: SampleController,CLLocationManagerDelegate {
    var id = "" {
        didSet{
            if id != ""{
            self.reloadAllData()
            }
        }
    }
    var isHistory = false
    var editMode = false
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue = manager.location else {return}
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(locValue, preferredLocale: Locale(identifier: "zh-Hant")) { (placeMark, error) in
                if let first = placeMark?.first {
                    
                    var address = ""
                    
                    if let area = first.subAdministrativeArea {
                        address += area
                    }
                    if let locale = first.locality {
                        address += locale
                    }
                    
                    if let fare = first.thoroughfare {
                        address += fare
                    }
                    
                    if let subfare = first.subThoroughfare {
                        address += "\(subfare)號"
                    }
                    
                    self.interInfo.location = address
                    self.createEditInterviewCell()
                    UIView.setAnimationsEnabled(false)
                    
                    self.rightview.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
                    UIView.setAnimationsEnabled(true)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       //
    }
    var stackview = LeftStackView(textarray: [UserDefaults.standard.getConvertedLanguage() == "en" ? "Information": "訪談資料",UserDefaults.standard.getConvertedLanguage() == "en" ? "Machine" :"訪談機台"])
    
    var fieldsArray :[FieldMode] = [.Info,.Machine]
    var stackviewanchor : AnchoredConstraints!
    let rightview = UITableView(frame: .zero, style: .grouped)
    var previousButton : NewTypeButton!
    let topstackview = UIStackView()
    //var originalMachine = [AddedProduct]()
    var mode = InterviewMode.New {
        didSet{
            
            self.preloadData()
        }
    }
    var discountInfo = DiscountInfo(discount_reason: nil, discount: nil)
    var addedMachine = [AddedProduct]()
    var discountArray = [Discount]()
    var signitureArray = [interViewFile(id: "", tag: ["sign"], path: "", path_url: "", image: nil)]
    //要加入
    var cellHeights = [IndexPath:CGFloat]()
    var interInfo = interViewInfo(id: "", status: .string("A"),customer:nil, location: "", visitDate: "", orderDate: "", notifyDate: nil, moreInfo: "",periods:[],files: [],delivery_date: "", code : "")
    var totalPriceView = UIView()
    let locationManager = CLLocationManager()
    var discountCell = DiscountViewCell(style: .default, reuseIdentifier: "discountCell")
    var salesName = ""
    var currentTotal : Int = 0
    func calculateTotal(){
        var total : Int = 0
        
        for i in addedMachine {
            if let pr = i.product {
                
                total += (pr.getProductsPrices()?.price ?? 0)
                
            }
            
            for j in i.options {
                total += j.option?.getProductsPrices()?.price ?? 0
            }
            for m in i.customOptions {
                if m.is_Cancel {
                    continue
                }
                for j in m.options {
                    total += j.amount * (j.price.integerValue ?? 0)
                }
            }
            total = (total * (i.qty))
        }
        currentTotal = total
        if let discount = discountInfo.discount {
            let d_price = discount
            total = total - d_price
        }
        
        totalLabel.text = "$ \(total)  \(UserDefaults.standard.getChosenCurrency())"
        
    }
    let priceLabel : UILabel = {
       let pL = UILabel()
        pL.text = "\("總金額".localized)："
        pL.font = UIFont(name: "Roboto-Regular", size: 24.calcvaluex())
        pL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return pL
    }()
    let totalLabel : UILabel = {
        let pL = UILabel()
         pL.text = "$ 0"
         pL.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
         pL.textColor = MajorColor().oceanColor
         return pL
    }()
    let contentView = UIView()
    let hud = JGProgressHUD()
    let bt = TopViewButton(text: "修改")
    //要加入
    let saveButton = UIButton(type: .custom)
    let notificationButton = UIButton(type: .custom)
    var deleteProductArray = [String]()
    var deleteAuditArray = [Int:[String]]()
    var deleteFileArray = [String]()
    
    var machincellsArray = [SelectedMachineCell]()
    var EditInterviewCell = EditingInterviewCell(style: .default, reuseIdentifier: "cell")

    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        topstackview.spacing = 16.calcvaluex()
        topstackview.axis = .horizontal

        if mode == .Normal {
        for (index,val) in ["編輯","分享","建立報價","刪除"].enumerated() {
            let bt = TopViewButton(text: val)
            if index == 2{
                bt.addTarget(self, action: #selector(goPrice), for: .touchUpInside)
            }
            bt.layer.cornerRadius = 38.calcvaluey()/2
            topstackview.addArrangedSubview(bt)
        }
        }else{
            // 要加入
            if mode == .Review || editMode {
                print(1113)
                bt.addTarget(self, action: #selector(goToEdit), for: .touchUpInside)
        bt.layer.cornerRadius = 38.calcvaluey()/2
        self.topstackview.addArrangedSubview(bt)
            }

        }
        topstackview.addArrangedSubview(UIView())
        
        topview.addSubview(topstackview)
        topstackview.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 10.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        titleview.label.text = UserDefaults.standard.getConvertedLanguage() == "en" ? "Interview Record" : "訪談紀錄"
        stackview.spacing = 11.calcvaluey()
        stackview.arrangedSubviews.forEach { (button) in
            
            (button as! NewTypeButton).addTarget(self, action: #selector(changeButton), for: .touchUpInside)
        }
        view.addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        contentView.addSubview(stackview)
        stackviewanchor = stackview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 33.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 200.calcvaluex(), height: 0))

        previousButton = stackview.arrangedSubviews[0] as? NewTypeButton
        let vd = UIView()
        vd.addshadowColor()
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.backgroundColor = .white
        contentView.addSubview(vd)
        vd.addSubview(rightview)
        rightview.fillSuperview()
        //contentView.addSubview(rightview)
        rightview.backgroundColor = .clear
        rightview.layer.cornerRadius = 15.calcvaluex()
        rightview.separatorStyle = .none
        rightview.delegate = self
        rightview.showsVerticalScrollIndicator = false
        rightview.dataSource = self
        //rightview.register(EditInterviewCell.self, forCellReuseIdentifier: "ct")
        rightview.register(EditingInterviewCell.self, forCellReuseIdentifier: "ct")
        //rightview.register(, forCellReuseIdentifier: "ct")
        rightview.contentInset.bottom = 30.calcvaluey()
       // rightview.anchor(top: contentView.topAnchor, leading: stackview.trailingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 42.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
       // rightview.bounces = false
        
        totalPriceView.backgroundColor = .white
        totalPriceView.addshadowColor()
        contentView.addSubview(totalPriceView)
        totalPriceView.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 78.calcvaluey()))
        
        vd.anchor(top: stackview.topAnchor, leading: stackview.trailingAnchor, bottom: totalPriceView.topAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 42.calcvaluex(), bottom: 20.calcvaluey(), right: 26.calcvaluex()))
        totalPriceView.addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: totalPriceView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        priceLabel.centerYInSuperview()
        
        totalPriceView.addSubview(totalLabel)
        totalLabel.anchor(top: nil, leading: priceLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        totalLabel.centerYInSuperview()
        
        saveButton.setTitle("儲存".localized, for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        saveButton.backgroundColor = MajorColor().oceanColor
        notificationButton.setTitle("通知".localized, for: .normal)
        notificationButton.setTitleColor(.white, for: .normal)
        notificationButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        notificationButton.isHidden = true
        notificationButton.backgroundColor = .black
        if mode == .Editing || mode == .New{
            saveButton.isHidden = false
            //notificationButton.isHidden = true
        }
        else{
            saveButton.isHidden = true
            //notificationButton.isHidden = false
        }
        totalPriceView.addSubview(saveButton)
        saveButton.anchor(top: totalPriceView.topAnchor, leading: nil, bottom: totalPriceView.bottomAnchor, trailing: totalPriceView.trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        saveButton.addTarget(self, action: #selector(saveData), for: .touchUpInside)
        totalPriceView.addSubview(notificationButton)
        notificationButton.anchor(top: saveButton.topAnchor, leading: saveButton.leadingAnchor, bottom: saveButton.bottomAnchor, trailing: saveButton.trailingAnchor)
        notificationButton.addTarget(self, action: #selector(goNotification), for: .touchUpInside)
        send_v?.isHidden = true
        //print(self.interInfo.visitDate)
        
    }
    func create_notify_json() -> [String:Any] {
        var parameter = [String:Any]()
        

        var notification = [String:Any]()
        
        notification["title"] = "訪談通知提醒"
        notification["body"] = "\(userattr?.name ?? "") 通知您查看訪談紀錄"
        
        var data = [String:Any]()
        data["type"] = "interview"
        data["id"] = self.interInfo.id
        
        
        var apns = [String:Any]()
        apns["headers"] = ["apns-priority":"10"]
        apns["payload"] = ["aps":["badge":1,"content-available":1,"mutable-content":1]]
        parameter["message"] = ["notification":notification,"data":data,"apns":apns]
        parameter["to_boss"] = 1
        return parameter
    }
    @objc func goNotification(){
        let notification = UIAlertController(title: "確認是否通知主管".localized, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
            let hud = JGProgressHUD()
              hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            hud.show(in: self.view)
            hud.textLabel.text = "通知中..".localized
            let parameter = self.create_notify_json()
              print(112,parameter)
              NetworkCall.shared.postCall(parameter: "api-or/v1/notification", param: parameter, decoderType: UpdateClass.self) { (json) in
                  DispatchQueue.main.async {
                      if let json = json{
                          print(889,json)
                          if json.data ?? false {
                              hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                              
                            hud.textLabel.text = "通知成功".localized
                              hud.dismiss(afterDelay: 1.5, animated: true) {
//                                  self.dismiss(animated: true, completion: nil)
                              }
                          }
                          else{
                              hud.indicatorView = JGProgressHUDErrorIndicatorView()
                              
                              hud.textLabel.text = "通知失敗".localized
                              hud.dismiss(afterDelay: 1.5, animated: true)
                          }
                      }
                      else{
                          hud.indicatorView = JGProgressHUDErrorIndicatorView()
                          
                          hud.textLabel.text = "通知失敗".localized
                          hud.dismiss(afterDelay: 1.5, animated: true)
                      }
                  }

              }
            
            
            
        }
        let action2 = UIAlertAction(title: "取消".localized, style: .cancel, handler: nil)
        
        notification.addAction(action)
        notification.addAction(action2)
        
        self.present(notification, animated: true, completion: nil)
//        let vd = NotificationViewController()
//        vd.interview_id = self.interInfo.id
//        vd.modalPresentationStyle = .fullScreen
//        self.present(vd, animated: true, completion: nil)
    }
    //要加入
    @objc func goToEdit() {
        if mode == .Review {
        mode = .Editing
        
        
            
        }
        else{
            
            mode = .Review
            
            
            self.reloadAllData()
            
        }
        
    }
    var send_v : TopViewButton?
    func addSendForm(){
        send_v = TopViewButton(text: "送出訂單")
        send_v?.layer.cornerRadius = 38.calcvaluey()/2
        topstackview.addArrangedSubview(send_v!)
        //interInfo.status = 3
        send_v?.addTarget(self, action: #selector(sendOutForm), for: .touchUpInside)
    }
    @objc func sendOutForm(){
        self.createDirectory()
        interInfo.status = .string("order")
        saveData()
    }
    func removeSend(){
        send_v?.isHidden = true
    }
    @objc func reloadAllData(){
        self.contentView.isHidden = true
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        var str = ""
        if isHistory {
            str = "api-or/v1/archive_interview/\(id)"
        }
        else{
            str = "api-or/v1/interview/\(id)"
        }
        //self.isHistory = false
        NetworkCall.shared.getCall(parameter: str, decoderType: InterViewInfos.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                self.contentView.isHidden = false
                if let json = json?.data {
                   
                    self.salesName = json.owner.name ?? ""
                    
                    if json.owner.id == GetUser().getUser()?.data.id {
                        
                        if self.mode == .Editing || self.mode == .New{
                            
                            self.notificationButton.isHidden = true
                        }
                        else{
                           
                            self.notificationButton.isHidden = false
                        }
                    }
                    else{
                        self.notificationButton.isHidden = true
                    }
//                    if json.status == 0 || json.status == 1 || json.status == 2{
//                        if self.send_v == nil{
//                        self.addSendForm()
//                        }
//                        else{
//                            self.send_v?.isHidden = false
//                        }
//                    }
//                    else{
//                        if let s = self.send_v {
//                            self.removeSend()
//                        }
//                    }
                    self.interInfo.delivery_date = json.delivery_date ?? ""
                    self.interInfo.code = json.account_order_code ?? ""
                    self.interInfo.id = json.id.stringValue ?? ""
                    self.interInfo.status = json.status
                    self.discountInfo.discount_reason = json.discount_reason
                    self.discountInfo.discount = json.discount?.integerValue
                    
                    if self.discountInfo.discount != nil && self.discountInfo.discount_reason != nil && self.discountInfo.discount != 0{
                        
                        self.fieldsArray = [.Info,.Machine,.Discount]
                        
                    }
                    else{
                        if json.products.count != 0 {
                            if self.mode == .Review && (self.discountInfo.discount == 0 || self.discountInfo.discount == nil) {
                                self.fieldsArray = [.Info,.Machine]
                            }
                            else{
                            self.fieldsArray = [.Info,.Machine,.Discount]
                            }
                        }
                        else{
                        self.fieldsArray = [.Info,.Machine]
                        }
                    }
                   
                    self.interInfo.periods = json.interview_period ?? []
                    self.interInfo.customer = json.account
                    self.interInfo.location = json.address ?? ""
                    self.interInfo.visitDate = json.date ?? ""
                    self.interInfo.orderDate = json.order_date ?? ""
                    
                    self.interInfo.rating = json.rating
                    self.interInfo.moreInfo = json.description ?? ""
                    if let notifyMode = json.order_reminder_date {
                        if notifyMode == "1" {
                            self.interInfo.notifyDate = .OneDay
                        }
                        else if notifyMode == "3" {
                            self.interInfo.notifyDate = .ThreeDay
                        }
                        else if notifyMode == "7" {
                            self.interInfo.notifyDate = .SevenDay
                        }
                        
                    }
                    var filesArray = [interViewFile]()
                    
                    var signD : interViewFile?
                    for i in json.files ?? [] {
                        let inter = interViewFile(id: i.id ?? "", tag: i.tags ?? [], path: i.path ?? "", path_url: i.path_url ?? "", image: nil)
                        filesArray.append(inter)
//                        if i.tags?.contains("file") ?? false {
//                        filesArray.append(inter)
//                        }
//                        else if i.tags?.contains("sign") ?? false{
//                            signD = inter
//                        }
                    }
                    self.interInfo.files = filesArray
                    var count = 1
                    self.addedMachine = self.creatWithProduct(products:json.products, count: &count)
                    self.createMachineCell()
                    if count == 0{
                        if let s = self.send_v {
                            self.removeSend()
                        }
                    }
                    if let sign = signD {
                        self.signitureArray[0] = sign
                    }
                    self.createEditInterviewCell()
                    //self.signitureArray[1].image = UIImage()
                    
                    if !self.editMode {
                        self.mode = .Review
                    }
                    else{
                        self.calculateTotal()
                        self.rightview.reloadData()
                        self.editMode = false
                    }
                    
                            }

            }

        }

    }
    var customAddFileMode = false
    var customFirstIndex = 0
    var customSubIndex = 0
    var customSubSubIndex = 0
    func creatWithProduct(products:[Product],count: inout Int) -> [AddedProduct] {
        var finalData : [AddedProduct] = []
        for i in products {
            
            var options = [SelectedOption]()
            for b in (i.options ?? []).sorted(by: { op1, op2 in
                return op1.order < op2.order
            }) {
                for j in (b.children ?? []).sorted(by: { op1, op2 in
                    return op1.order < op2.order
                }) {
                    for m in (j.children ?? []).sorted(by: { op1, op2 in
                        return op1.order < op2.order
                    }) {
                        if m.selected == true {
                            options.append(SelectedOption(name: "\(j.title) / \(m.title)", option: m,qty: i.qty ?? 1))
                        }
                    }
                }
            }
            //要加入
            var auditArray : [customForm] = []
            var filePathArray = i.audit?.first?.assets?.file ?? []
            
            for i in i.audit ?? [] {
                
                var auditData = [AddedOption]()
                for j in i.content {
                    auditData.append(AddedOption(mode: AddedMode(rawValue: j.type) ?? .Date, text: j.content ?? "", amount: j.qty ?? 0, price: j.price ?? "", reply: j.reply ?? "", id: j.id ?? "",files: j.files ?? []))
                }
               
                if i.status != 6 && i.status != 5{
                    count = 0
                }
                auditArray.append(customForm(id: i.id, status: i.status, is_Cancel: i.status == 6, statusArray: [], options: auditData))
            }
            if i.audit?.count == 0{
                auditArray.append(customForm(id: 0, status: -1, is_Cancel: false, statusArray: [], options: []))
            }
            
            finalData.append(AddedProduct(series: nil, sub_series: nil, product: i,qty: i.qty ?? 1, options: options, customOptions: auditArray,fileAssets: filePathArray))
        }
        
//        for i in finalData {
//            print(8897,i.customOptions)
//        }
        
        return finalData
    }
    func preloadData(){
        self.grabUser()
        if mode == .New || mode == .Editing {
           // print(3321)
            if mode == .New{
                self.interInfo.visitDate = Date().getCurrentTime()
                //self.fetchLocation()
                
            }
            
//            for i in addedMachine {
//                var or = [Int]()
//                for j in i.customOptions {
//                    or.append(j.status)
//                }
//                originalStatus.append(or)
//            }
        
        self.getCustomersList()
        self.createDirectory()
            bt.setTitle("取消修改".localized, for: .normal)
            if mode == .Editing {
                bt.isHidden = false
            }
            else{
                bt.isHidden = true
            }
            
            saveButton.isHidden = false
        }
        else{
            bt.setTitle("修改".localized, for: .normal)
            bt.isHidden = false
            saveButton.isHidden = true
        }
        self.calculateTotal()
        self.createEditInterviewCell()
        self.rightview.reloadData()
        // 要加入
//        self.rightview.insertRows(at: [IndexPath(row: addedMachine.count - 1 , section: 0)], with: .none)
////        self.rightview.reloadSections(IndexSet(arrayLiteral: 0), with: .none)

        
        //self.rightview.reloadData()
       // self.rightview.beginUpdates()

        //self.rightview.endUpdates()
    }
    var user : UserAccount?
    var userattr : UserD?
    func grabUser(){
        if let data = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()) {
            do {
                let json = try JSONDecoder().decode(UserData.self, from: data)
                self.user = json.data.account
                self.userattr = json.data
                
                self.salesName = self.userattr?.name ?? ""
                //要加入
                //self.reloadSectionWithIndex(index: 0)
            }
            catch{
                
            }
        }
    }
    var customersList = [Customer]()
    func getCustomersList(){
        do{
            if let dt = UserDefaults.standard.getCustomersData(){
                let json = try JSONDecoder().decode(Customers.self, from: dt)
                
                NetworkCall.shared.postCall(parameter: "api-or/v1/accounts/check", dict: ["check_token":json.meta?.update_token ?? ""], decoderType: UpdateClass.self) { (jt) in
                    DispatchQueue.main.async {
                        if let jt = jt{
                            if !(jt.data ?? false) {
                                self.getNewData()
                            }
                            else{
                                self.customersList = json.data ?? []
                            }
                        }
                        else{
                            self.customersList = json.data ?? []
                        }
                    }

                }
                
            }
            else{
                getNewData()
            }
            
        }
        catch{
            
        }
    }
    func getNewData(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        NetworkCall.shared.getCall(parameter: "api-or/v1/accounts?pens=100000", decoderType: Customers.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss(afterDelay: 0, animated: true)
                
                self.customersList = json?.data ?? []
                do {
                    let encode = try JSONEncoder().encode(json)
                    UserDefaults.standard.setCustomersData(data: encode)
                }
                catch{
                    
                }


            }

        }
    }
    func constructProducts() -> [[String:Any]]{
        
        var productsArray = [[String:Any]]()
        for (index,i) in addedMachine.enumerated() {
 
            if var pr = i.product {
                print(669,pr.assets)
                for j in i.options {
                    
                    for (index,k) in (pr.options ?? []).enumerated() {
                        for (index2,m) in (k.children ?? []).enumerated() {
                            for (index3,s) in (m.children ?? []).enumerated() {
                                if s.id == j.option?.id {
                                    pr.options?[index].children?[index2].children?[index3].selected = true
                                }
                            }
                        }
                    }
                }
                
                
                
            do{
                let data = try JSONEncoder().encode(pr)
                
                var json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] ?? [:]
                
                //json["quantity"] = 1
                json["qty"] = i.qty
                //要加入
                
                if let dt = json["product_id"] as? String {
                    if dt == "" {
                    json["product_id"] = json["id"]
                    json["id"] = nil
                    }
                }
                else{
                    json["product_id"] = json["id"]
                    json["id"] = nil
                }
                if i.customOptions.first?.status != -1{
                let auitarray = createaudit(options:i.customOptions)
                if let auit = auitarray {
                json["audit"] = auit
                }
                }
                productsArray.append(json)
                
            }
            catch{
                
            }
            }
        }
        return productsArray
    }
    
    func createaudit(options:[customForm]) -> [[String:Any]]? {
        
        if options.count == 0{
            return nil
        }
        var allAudit = [[String:Any]]()
        
        for i in options {
            if i.status == -1{
                continue
            }
            var audit = [String:Any]()
            audit["id"] = i.id
            audit["status"] = i.status
            
            var contents = [[String:Any]]()
            for j in i.options {
                var contdict = [String:Any]()
                contdict["type"] = j.mode.rawValue
                contdict["content"] = j.text
                if j.mode != .Date {
                    contdict["qty"] = j.amount
                    contdict["price"] = j.price
                }
                if let fr = j.files.first {
                    contdict["files"] = self.createauditFileArray(files:fr)
                    
                }
                else{
                    contdict["files"] = nil
                }
                if let del = j.del_files {
                    contdict["del_files"] = del
                }
 
                contents.append(contdict)
            }
            
            audit["content"] = contents


            
            allAudit.append(audit)
        }
        
        return allAudit
    }
    func createauditFileArray(files:AddedOptionFile) -> [[String:Any]]{
        var option = [String:Any]()
        option["id"] = files.id
        option["path"] = files.path
        option["tags"] = files.tags
        option["pathUrl"] = files.pathUrl
        
        return [option]
        
    }
    func getFilesName() -> [[String:Any]] {
        var names = [[String:Any]]()
        for i in interInfo.files {
            var dt = [String:Any]()
            dt["id"] = i.id
            dt["tags"] = i.tag
            dt["path"] = i.path
            dt["path_url"] = i.path_url
            names.append(dt)
        }
        
        var sigDt = [String:Any]()
        let sign = signitureArray[0]
        sigDt["id"] = sign.id
        sigDt["tags"] = sign.tag
        sigDt["path"] = sign.path
        sigDt["path_url"] = sign.path_url
        if sign.path != "" {
        names.append(sigDt)
        }
        
        return names
    }
    var textviewWidth : CGFloat = 0
    var noNeedDismiss = false
    @objc func saveData(){
        
        hud.textLabel.text = "上傳中...".localized
        hud.show(in: self.contentView)
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        var param = [String:Any]()
        param["company_name"] = interInfo.customer?.name
        param["company_code"] = interInfo.customer?.code
        param["address"] = interInfo.location
        param["date"] = interInfo.visitDate
        param["interview_period"] = interInfo.periods
        param["discount_reason"] = discountInfo.discount_reason
        param["discount"] = discountInfo.discount
        if let info = interInfo.notifyDate {
            param["order_reminder_date"] = info.info.code
        }
        //self.interInfo.delivery_date = json.delivery_date ?? ""
        //self.interInfo.code = json.account_order_code ?? ""
        param["delivery_date"] = interInfo.delivery_date
        param["account_order_code"] = interInfo.code
        //要加入
        if interInfo.id != "" {
            print(1112,interInfo.id)
        param["id"] = interInfo.id
        }
        if !deleteProductArray.isEmpty {
            param["del_products"] = deleteProductArray
        }
        
        param["order_date"] = interInfo.orderDate
        param["description"] = interInfo.moreInfo
        param["rating"] = interInfo.rating
        switch interInfo.status {
        case .int(let x):
            if x == 0 {
                if interInfo.customer?.code?.contains("APP") ?? false{
                param["status"] = "expand"
            }
            else{
                param["status"] = "negotiate"
            }
            }
            else{
                if x == 1 {
                    param["status"] = "expand"
                }
                else if x == 2{
                    param["status"] = "negotiate"
                }
                else if x == 3{
                    param["status"] = "order"
                }
                else{
                    param["status"] = "closed"
                }
                
            }
        case .string(let y):
            if y == "A" {
                if interInfo.customer?.code?.contains("APP") ?? false{
                param["status"] = "expand"
            }
            else{
                param["status"] = "negotiate"
            }
            }
            else{
                param["status"] = y
            }
        }

        param["prices"] = ["group":UserDefaults.standard.getChosenGroup(),"currency" : UserDefaults.standard.getChosenCurrency()]
        if interInfo.files.count > 0 || signitureArray.first?.path != "" {
            param["files"] = getFilesName()
        }
            param["products"] = constructProducts()
        
        //param["client_signature"] = ["customer":signitureArray[0].type?.rawValue ?? ""]
        if deleteFileArray.count != 0{
        param["del_files"] = deleteFileArray
        }
        let json = try? JSONSerialization.data(withJSONObject: param, options: [.prettyPrinted])
        if let dt = json {
            let str = String(data: dt, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
            let jsonstr = str?.data(using: .utf8)
            writeDataToFolder(data: jsonstr, name: "api.json")
            zipFile()
        }

        
    }
    @objc var progress : Progress?
    var isObservingProgress = false
    var progressViewKVOContext = 0
    weak var historycon: PriceHistoryController?
    func startObservingProgress()
    {
        guard !isObservingProgress else { return }

        progress = Progress()
        progress?.completedUnitCount = 0
        //self.indicator.progress = 0.0

        self.addObserver(self, forKeyPath: #keyPath(progress.fractionCompleted), options: [.new], context: &progressViewKVOContext)
        isObservingProgress = true
    }
    func stopObservingProgress()
    {
        guard isObservingProgress else { return }

        self.removeObserver(self, forKeyPath: #keyPath(progress.fractionCompleted))
        isObservingProgress = false
        
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(progress.fractionCompleted) {
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:  {


                if self.progress?.isFinished == true {
                     self.progress = nil
                    if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        
                        let dt = try? NSData(contentsOfFile: documentsDirectory.appendingPathComponent("upload.zip").absoluteString.replacingOccurrences(of: "file://", with: ""), options: []) as Data
                    
                    
                    
                        if let dt = dt {
                    NetworkCall.shared.postCallZip(parameter: "api-or/v1/interview/upload", data: dt, decoderType: UpdateClass.self) { (json) in
                        DispatchQueue.main.async {
                            if let json = json {
                                if json.data ?? false {
                                    self.hud.textLabel.text = self.mode == .Editing ? "修改成功".localized:"上傳成功".localized
                                    self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                                    
                                    self.hud.dismiss(afterDelay: 1, animated: true) {
                                        if !self.noNeedDismiss {
                                        self.popSendOut()
                                        }
                                        else{
                                            self.historycon?.data = []
                                            self.historycon?.fetchApi()
                                        }
//                                        else{
//                                            self.preloadData()
//                                        }
                                    }
                                }
                                else{
                                    self.hud.textLabel.text = self.mode == .Editing ? "修改失敗".localized:"上傳失敗".localized
                                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                    self.removeZip()
                                    
                                    self.hud.dismiss(afterDelay: 1)
                                    
                                }
                            }
                            else{
                                self.hud.textLabel.text = self.mode == .Editing ? "修改失敗".localized:"上傳失敗".localized
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.removeZip()
                                self.hud.dismiss(afterDelay: 1)
                                
                            }
                            
                            
                        }
    
                    }
                    }
                                   
                    }
                
                
                
                }
            })
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    func zipFile(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("interview/")
            
            
                
               
                self.startObservingProgress()
                DispatchQueue.global().async {
                    try? FileManager.default.zipItem(at: dataPath, to: documentsDirectory.appendingPathComponent("upload.zip"), shouldKeepParent: true,progress: self.progress)
                    self.stopObservingProgress()
                }

                
 
            
        }
    }
    func fetchLocation(){
        
        

                let authorizationStatus = CLLocationManager.authorizationStatus()
        
        if authorizationStatus == .notDetermined {
            
            
           locationManager.requestAlwaysAuthorization()
        }

        
        print(CLLocationManager.locationServicesEnabled())
        
        locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   locationManager.startUpdatingLocation()
                
    }
    func createDirectory(){
        
           
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dataPath = documentsDirectory.appendingPathComponent("interview")

                print(dataPath)
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)

            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func removeZip(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {



            do {
                

                try FileManager.default.removeItem(at: documentsDirectory.appendingPathComponent("upload.zip"))
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func removeDirectory(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dataPath = documentsDirectory.appendingPathComponent("interview")


            do {
                try FileManager.default.removeItem(at: dataPath)

                try FileManager.default.removeItem(at: documentsDirectory.appendingPathComponent("upload.zip"))
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("interview/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    func removeDataFromFolder(name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("interview/\(name)")
            
            do{
                try FileManager.default.removeItem(at: dataPath)
            }
            catch let er{
                print(er)
            }
            
        }
    }
    func popSendOut(){
        self.removeDirectory()
        self.dismiss(animated: true, completion: nil)
    }
    override func popview() {
        
        if mode == .New || mode == .Editing {
            let controller = UIAlertController(title: "您目前在編輯狀態，離開後變更的內容將遺失，確認是否離開?".localized, message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
                
                self.removeDirectory()
                self.dismiss(animated: true, completion:   nil)
            }
            let action2 = UIAlertAction(title: "取消".localized, style: .cancel, handler: nil)
            
            controller.addAction(action)
            controller.addAction(action2)
            self.present(controller, animated: true, completion: nil)
        }
        else{
        
        self.removeDirectory()
            self.dismiss(animated: true, completion:   nil)
        }
    }
    @objc func goPrice(){
        let vd = CreatePriceController()
        vd.stackview = LeftStackView(textarray: ["報價資訊","報價機台","調整價格"])
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    @objc func goEditing(){
        
        mode = .Editing

        UIView.animate(withDuration: 0.5, animations: {
            self.topstackview.arrangedSubviews.forEach { (vt) in
                vt.removeFromSuperview()
            }

        }) { (_) in
            let bt = TopViewButton(text: "儲存".localized)
            bt.layer.cornerRadius = 38.calcvaluey()/2
            bt.addTarget(self, action: #selector(self.endEditing), for: .touchUpInside)
            self.topstackview.addArrangedSubview(bt)
            self.topstackview.addArrangedSubview(UIView())
        }
        
        rightview.reloadData()


    }
    @objc func endEditing(){
        UIView.animate(withDuration: 0.5, animations: {
            self.topstackview.arrangedSubviews.forEach { (vt) in
                vt.removeFromSuperview()
            }

        }) { (_) in
            for (index,val) in ["編輯","分享","建立報價","刪除"].enumerated() {
                let bt = TopViewButton(text: val)
                if index == 0{
                    bt.addTarget(self, action: #selector(self.goEditing), for: .touchUpInside)
                }
                bt.layer.cornerRadius = 38.calcvaluey()/2
                self.topstackview.addArrangedSubview(bt)
            }
            self.topstackview.addArrangedSubview(UIView())
        }
    }
    @objc func changeButton(sender:NewTypeButton){
        print(3321)
        if previousButton != nil{
            previousButton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
            previousButton.newlabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
            previousButton.newlabel.textColor = .white
        }
        sender.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        sender.newlabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        sender.newlabel.textColor = .white
        previousButton = sender
        if sender.tag == 1 {
            if addedMachine.count == 0{
                return
            }
        }
        rightview.scrollToRow(at: IndexPath(row: 0, section: sender.tag), at: .top, animated: true)
    }
    @objc func addPhoto(){
        mode = .addPhoto
        rightview.beginUpdates()
        rightview.reloadData()
        rightview.endUpdates()
    }

    var modeselection : CustomizeModeSelectionController?
    var savedOptions = [Int:[OptionObj]]()
}






class InterviewCell:UITableViewCell {
    let container = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = .clear
        
        container.layer.cornerRadius = 15.calcvaluex()
        //container.addshadowColor()
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 2.calcvaluex(), bottom: 6.calcvaluey(), right: 2.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class InterviewFirstSectionCell:InterviewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath)
        let imageview = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
        
        cell.addSubview(imageview)
        imageview.fillSuperview()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 17.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 125.calcvaluex(), height: collectionView.frame.height)
    }
    
    let customername = InterviewNormalFormView(text: "客戶名稱".localized)
    let interviewdate = InterviewNormalFormView(text: "拜訪日期".localized)
    let orderdate = InterviewNormalFormView(text: "預計下單日期".localized)
    let moreinfo = MoreInfoView(text: "備註".localized)
    let imagecollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        container.addSubview(customername)
        customername.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 694.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(interviewdate)
        interviewdate.anchor(top: customername.bottomAnchor, leading: customername.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 341.calcvaluey(), height: 80.calcvaluey()))
        container.addSubview(orderdate)
        orderdate.anchor(top: nil, leading: interviewdate.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 341.calcvaluex(), height: 80.calcvaluey()))
        orderdate.centerYAnchor.constraint(equalTo: interviewdate.centerYAnchor).isActive = true
        
        container.addSubview(moreinfo)
        moreinfo.anchor(top: orderdate.bottomAnchor, leading: interviewdate.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluey(), height: 106.calcvaluey()))
        
        (imagecollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        imagecollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellid")
        
        container.addSubview(imagecollectionview)
        imagecollectionview.backgroundColor = .clear
        imagecollectionview.delegate = self
        imagecollectionview.dataSource = self
        imagecollectionview.anchor(top: moreinfo.bottomAnchor, leading: moreinfo.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 26.calcvaluey(), right: 36.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class InterviewSecondSectionCell:InterviewCell {
    let stackview = UIStackView()
    let addButton = UIButton()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        container.addSubview(stackview)
        stackview.spacing = 12.calcvaluex()
        stackview.axis = .horizontal
        stackview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 34.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        
        ["立式加工中心機","Vcenter-P76/106/136","Vc-P76"].forEach { (st) in
            let mt = MachineLabel(text: st)
            mt.layer.cornerRadius = 38.calcvaluey()/2
            stackview.addArrangedSubview(mt)
        }
        stackview.addArrangedSubview(UIView())
        
        addButton.setTitle("選型", for: .normal)
        addButton.setTitleColor(.white, for: .normal)
        addButton.backgroundColor = .black
        addButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        container.addSubview(addButton)
        addButton.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 100.calcvaluex(), height: 38.calcvaluey()))
        addButton.layer.cornerRadius = 38.calcvaluey()/2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class MoreInfoView:UIView {
    let tlabel = UILabel()
    let textfield = MoreInfoTextField()
    init(text:String){
        super.init(frame: .zero)
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        tlabel.text = text
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        addSubview(textfield)

        textfield.anchor(top: tlabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 78.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class MoreInfoTextField:UITextView {
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        layer.cornerRadius = 15.calcvaluex()
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
       
        self.textContainerInset = .init(top: 18.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 29.calcvaluex())
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class InterviewNormalFormView : NormalFormView {
    var text:String?
    var t_mode : InterviewMode? {
        didSet{
        if t_mode == .Review {
            textfield.attributedPlaceholder = nil
        }
        }
    }
    let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    var color : UIColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    init(text:String,placeholdertext:String? = nil,color:UIColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)) {
        super.init(frame: .zero)
        self.color = color
        textfield.padding = 26.calcvaluex()
        self.text = text
        tlabel.text = text
        tlabel.textColor = color
        textfield.autocapitalizationType = .none
        textfield.autocorrectionType = .no
        
        textfield.attributedPlaceholder = NSAttributedString(string: placeholdertext ?? "", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)])
        arrow.isHidden = true
        textfield.addSubview(arrow)
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: textfield.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        arrow.centerYInSuperview()
        
    }
    
    func makeImportant(){
        
        let mutablestring = NSMutableAttributedString(string: "* ", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:MajorColor().oceanColor])
        
        mutablestring.append(NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:self.color]))
        tlabel.attributedText = mutablestring
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class InterviewDateFormView: InterviewNormalFormView,FormToolBarDelegate,UITextFieldDelegate {
    var delegate:EditingInterviewDelegate?
    var format : String = ""
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if mode == "visit"{
            delegate?.setVisitDate(text:formatter.string(from: datepicker.date))
        }
        else if mode == "delivery" {
            delegate?.setDeliveryDate(text: formatter.string(from: datepicker.date))
        }
        else{
        delegate?.setOrderDate(text: formatter.string(from: datepicker.date))
        }
    }
    func toolbarDonePressed() {
       
       // formatter.dateFormat = "yyyyMMdd"
      //  formatter.locale = Locale(identifier: "zh_Hant")
       // formatter.dateStyle = .medium
       // textfield.text = formatter.string(from: datepicker.date)
        self.endEditing(true)
        
       
    }
    var mode = ""
    

    let datepicker = UIDatePicker()
    init(text: String, placeholdertext: String? = nil,formatter:String = "yyyy-MM-dd",mode:String = "order") {
        super.init(text: text, placeholdertext: placeholdertext)
        self.format = formatter
        self.mode = mode
        datepicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datepicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        datepicker.locale = Locale(identifier: "zh_Hant")
        textfield.inputView = datepicker
        
        textfield.tintColor = .clear
        textfield.toolBarDelegate = self
        textfield.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class RightViewTopText: UIView {
    let locimage = UIImageView(image: #imageLiteral(resourceName: "ic_address"))
    let locationtext = UILabel()
    let createDate = UILabel()
    let salesMember = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(locimage)
        locimage.contentMode = .scaleAspectFill
        locimage.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 8.73.calcvaluey(), right: 0),size: .init(width: 17.54.calcvaluey(), height: 0))
        
        addSubview(locationtext)
        locationtext.anchor(top: nil, leading: locimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 10.41.calcvaluex(), bottom: 0, right: 0))
        locationtext.centerYAnchor.constraint(equalTo: locimage.centerYAnchor).isActive = true
        locationtext.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        locationtext.text = "訪談地點：台中市西屯區文心路三段"
        locationtext.textColor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
        
        addSubview(createDate)
        createDate.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor)
        createDate.centerYAnchor.constraint(equalTo: locationtext.centerYAnchor).isActive = true
        createDate.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        createDate.text = "建立時間：10月5日 下午6:30"
        createDate.textColor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
        
        addSubview(salesMember)
        salesMember.anchor(top: nil, leading: nil, bottom: nil, trailing: createDate.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        salesMember.centerYAnchor.constraint(equalTo: createDate.centerYAnchor).isActive = true
        salesMember.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        salesMember.text = "負責業務：詹莉雅"
        salesMember.textColor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol RightTopViewHeaderDelegate {
    func seeInfo()
}
class RightTopViewHeader: UIView{
    let headerlabel = UILabel()
    var infoButton : UIButton = {
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(#imageLiteral(resourceName: "ic_faq_pressed").withRenderingMode(.alwaysTemplate), for: .normal)
        infoButton.tintColor = MajorColor().oceanSubColor
        return infoButton
    }()
    var delegate: RightTopViewHeaderDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        headerlabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        
        headerlabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        addSubview(headerlabel)
        headerlabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 10.calcvaluey(), right: 0))
        addSubview(infoButton)
        infoButton.imageView?.contentMode = .scaleAspectFit
        infoButton.anchor(top: nil, leading: headerlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        infoButton.centerYAnchor.constraint(equalTo: headerlabel.centerYAnchor).isActive = true
        infoButton.addTarget(self, action: #selector(goSeeInfo), for: .touchUpInside)
        
        let sep = UIView()
        sep.backgroundColor = MajorColor().oceanColor
        addSubview(sep)
        sep.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 22.calcvaluex(), bottom: 0, right: 22.calcvaluex()),size: .init(width: 0, height: 2.calcvaluey()))
    }
    @objc func goSeeInfo(){
        delegate?.seeInfo()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class newStatusLabel : UIView {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        label.setContentHuggingPriority(.required, for: .horizontal)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        addSubview(label)
        label.fillSuperview(padding: .init(top: 0, left: 20.calcvaluex(), bottom: 0, right: 20.calcvaluex()))
        constrainHeight(constant: 30.calcvaluey())
        layer.cornerRadius = 30.calcvaluey()/2
        layer.borderWidth = 1.calcvaluex()
    }
    func setColor(color:UIColor) {
        layer.borderColor = color.cgColor
        label.textColor = color
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SectionOneTopViewHeader: RightTopViewHeader {
    let memberlabel = UILabel()
    let statusLabel = newStatusLabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        infoButton.isHidden = true
        
        addSubview(statusLabel)
        headerlabel.text = "訪談資料".localized
        
        statusLabel.anchor(top: nil, leading: headerlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 27.calcvaluex(), bottom: 0, right: 0))
        statusLabel.centerYAnchor.constraint(equalTo: headerlabel.centerYAnchor).isActive = true
        addSubview(memberlabel)
        //memberlabel.text = "負責業務：詹莉雅"
        memberlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        memberlabel.textColor = #colorLiteral(red: 0.2772052288, green: 0.2772502899, blue: 0.2771953344, alpha: 1)
        memberlabel.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 14.calcvaluex()))
        memberlabel.centerYAnchor.constraint(equalTo: headerlabel.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
