//
//  CustomerListController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CustomerContentViewCell: UITableViewCell {
    let seperator = UIView()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_content_call"))
    let titlelabel = UILabel()
    let contentabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        seperator.backgroundColor = #colorLiteral(red: 0.9293077588, green: 0.9294702411, blue: 0.9293094873, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 470.calcvaluex(), height: 1.calcvaluey()))
        addSubview(imageview)
        imageview.anchor(top: nil, leading: seperator.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        imageview.centerYInSuperview()
        titlelabel.text = "電話："
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYInSuperview()
        
        contentabel.text = "04-9876543 #21"
        contentabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        contentabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        addSubview(contentabel)
        contentabel.anchor(top: nil, leading: titlelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        contentabel.centerYInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerContentView:UIView,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CustomerContentViewCell(style: .default, reuseIdentifier: "xx")
        cell.backgroundColor = .clear
        if indexPath.item == 1 {
            cell.seperator.isHidden = true
            cell.imageview.image = #imageLiteral(resourceName: "ic_mail")
            cell.titlelabel.text = "信箱："
            cell.contentabel.text = "gtmc@gtmc.com.tw"
        }
        else{
            cell.imageview.image = #imageLiteral(resourceName: "ic_content_call")
            cell.titlelabel.text = "電話："
            cell.contentabel.text = "04-9876543 #21"
            cell.seperator.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.calcvaluey()
    }
    let tableview = UITableView(frame: .zero, style: .plain)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()

        addshadowColor(color: #colorLiteral(red: 0.9489133954, green: 0.9490793347, blue: 0.948915422, alpha: 1))
        addSubview(tableview)
        tableview.fillSuperview()
        tableview.layer.cornerRadius = 15.calcvaluex()
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerListController: UIViewController {
    let container = UIView()
    var containeranchor:AnchoredConstraints!
    let customerheader = UILabel()
    let tableview = UITableView(frame: .zero, style: .plain)
    
    var current : IndexPath?
    
        let contentContainer = CustomerContentView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        container.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        if #available(iOS 11.0, *) {
            container.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
             container.layer.cornerRadius = 15.calcvaluex()
        } else {
            // Fallback on earlier versions
        }
        
        view.addSubview(container)
        containeranchor = container.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 442.calcvaluey()))
        
        let xbutton = UIButton(type: .custom)
        xbutton.imageView?.contentMode = .scaleAspectFill
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        
        customerheader.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        customerheader.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        customerheader.text = "客戶列表"
        
        container.addSubview(customerheader)
        customerheader.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0))
        
        container.addSubview(tableview)
        //tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: customerheader.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 402.calcvaluex(), height: 0))
        


        container.addSubview(contentContainer)

        contentContainer.anchor(top: tableview.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 542.calcvaluex(), height: 339.calcvaluey()))

    }
    @objc func popview(){
        containeranchor.top?.isActive = false
        container.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView(tableview, didSelectRowAt: IndexPath(item: 0, section: 0))
        view.layoutIfNeeded()

        containeranchor.top?.constant = -442.calcvaluey()
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}
extension CustomerListController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CustomerListCell(style: .default, reuseIdentifier: "ct")
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if indexPath.item == 4 {
            cell.separatorInset = .init(top: 0, left: UIScreen.main.bounds.width, bottom: 0, right: 0)
        }
        else{
            cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if current != nil{
            let cell = tableView.cellForRow(at: current!) as! CustomerListCell
            if current!.item != 4{
            cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: 0)
            }
            cell.container.backgroundColor = .clear
            
            if current!.item > 0 {
                let prevcell = tableView.cellForRow(at: IndexPath(item: current!.item - 1, section: 0))
                prevcell?.separatorInset = .init(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
        let cell = tableView.cellForRow(at: indexPath) as! CustomerListCell
        cell.separatorInset = .init(top: 0, left: UIScreen.main.bounds.width, bottom: 0, right: 0)
        cell.container.backgroundColor = .white
        current = indexPath
        
        if indexPath.item > 0 {
        let prevcell = tableView.cellForRow(at: IndexPath(item: indexPath.item - 1, section: 0))
        prevcell?.separatorInset = .init(top: 0, left: UIScreen.main.bounds.width, bottom: 0, right: 0)
        }
        
    }
    
}

class CustomerListCell:UITableViewCell{
    let container = UIView()
    let circleview = CustomerCircleView()
    let namelabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        container.backgroundColor = .clear
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor(color: #colorLiteral(red: 0.916687429, green: 0.9190559983, blue: 0.9245963693, alpha: 1))
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        
        circleview.namelabel.text = "詹"
        circleview.namelabel.textColor = .white
        container.addSubview(circleview)
        circleview.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 48.calcvaluey(), height: 48.calcvaluex()))
        circleview.centerYInSuperview()
        circleview.layer.cornerRadius = 48.calcvaluey()/2
        
        namelabel.font = UIFont(name: "Roboto-Light", size: 20.calcvaluex())
        namelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        namelabel.text = "詹莉雅"
        container.addSubview(namelabel)
        namelabel.anchor(top: nil, leading: circleview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0))
        namelabel.centerYInSuperview()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
