//
//  Customer.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
import UIKit
import MapKit
struct EmployeesData : Codable{
    var data : [Employer]
}
struct Employer : Codable {
    var id : Int
    var name : String?
}
struct Customers : Codable{
    var data : [Customer]?
    var meta : Meta?
}

struct Customer : Codable {
    var id : String?
    var code : String?
    var name : String?
    var contact : String?
    var business_number : String?
    var address : String?
    var tel : String?

    var sales : Sales?
   // var currentSales : Sales?
    
//    mutating func setCurrentSales(){
//        switch self.sales {
//        case .sales(let x) :
//            self.currentSales = x
//        case .bool(_):
//            ()
//        }
//    }
    
}
struct Sales : Codable  {
    var id : String?
    var code : String
    var name : String
    var email : String?
    var department_code : String?
    var department_name : String?
}
enum SalesType : Codable {
    case bool(Bool)
    case sales(Sales)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(Sales.self) {
            self = .sales(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .sales(let x):
            try container.encode(x)
        }
    }
}


struct DepartmentGroup {
    var name : String?
    var code : String?
    var sales : [SalesCompany]
}

struct SalesCompany {
    var name : String
    var code : String
    var company : [Customer]
    var interviewList : [InterViewList]
}


struct Company : Codable {
    var data : CompanyData?
    var ERR_CODE: String?
}

struct CompanyData : Codable {
    var id : String
    var code : String
    var name : String?
    var tel : String?
    var fax : String?
    var contact : String?
    var address : String?
    var business_number : String?
    var sales : Sales?
    var type : String?
    var other : CompanyOther?
    var staff_num : String?
    var upstream : [String]?
    var downstream : [String]?
    var contacts : [CompanyContact]?
    var interviews : [Interview]
    var machines : [CustomerEquipment]?
    
    
}
struct CompanyOther : Codable{
    var type_description : String?
}
struct CustomerEquipment:Codable {
    var id : Int
    var account_code : String?
    var model : String?
    var brand: String?
    var number : String?
    var manufacturer: String?
}
struct CompanyContact : Codable{
    var id : Int
    var account_code : String?
    var name : String?
    var type : String?
    var mobile : String?
    var interesting:String?
    var phone: String?
    var email : String?
    var birthday : String?
    var other : ContactOther?
}
struct ContactOther : Codable {
    var ext : String?
}
struct Interview : Codable {
    var id : String
    var status : String
    var visited_at : String?
    var estimated_on : String?
    var date : String?
    var rating : Int?
    
    var order_date : String
    
    var didSelect : Bool?
}

struct AddedProduct {
    var series : Series?
    var sub_series : Series?
    
    var product : Product?
    
    var qty: Int = 1
    
    var options : [SelectedOption]
    //要加入
    var customOptions : [customForm]
    
    var fileAssets: [FilePath]?
}
//要加入
struct customForm {
    var id : Int
    var status : Int
    var is_Cancel : Bool
    //var replay : String
    var statusArray : [String]
    var options : [AddedOption]
}
struct SelectedOption {
    var name : String
    var option : Option?
    var qty : Int

}

enum AddedMode : Int{
    case Date = 1
    case Custom = 3
    case Extra = 2
}
struct AddedOption {
    var mode : AddedMode
    var text : String
    var amount : Int
    var price : String
    //要加入
    var reply : String
    var id : String
    var files : [AddedOptionFile] = []
    var del_files : [String]?
}

struct AddedOptionFile : Codable {
    var id : String?
    var path : String?
    var tags : [String] = ["file"]
    var pathUrl : String?
    var image : Data?
    var fileUrl : String?
}


struct Discount {
    var text : String
    var price : Double
}


enum SignitureMode : String{
    case Customer = "CustomerSigniture.jpg"
    case Sales = "SalesSigniture.jpg"
}
struct Signiture {
    var type : SignitureMode?
    var image : UIImage?
    var info : interViewFile?
}

//要加入
struct interViewInfo {
    var id : String
    var status : InterviewStatusEnum
    var customer : Customer?
    var location : String
    var visitDate : String
    var orderDate : String
    var notifyDate : NotifyMode?
    var moreInfo : String
    var rating : Int? = nil
    var periods : [String]
    var files : [interViewFile]
    var delivery_date : String
    var code : String
    var addressType : AddressType?
}

struct interViewFile {
    var id : String
    var tag : [String]
    var path : String
    var path_url : String
    var image : UIImage?
    var fileUrl : String?
}
extension String {
    func getTimeInterval() -> TimeInterval {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        return formatter.date(from: self)?.timeIntervalSince1970 ?? 0
    }
}

extension Date {
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    func getStartingInterval() -> TimeInterval {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let str = formatter.string(from: self) + " 00:00:00"
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: str)?.timeIntervalSince1970 ?? 0
    }
    func getEndInterval() -> TimeInterval {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let str = formatter.string(from: self) + " 23:59:59"
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: str)?.timeIntervalSince1970 ?? 0
    }
//    func getFormatString(str:String) -> String{
//        
//    }
    func getCurrentTime(format:String = "yyyy/MM/dd") -> String {
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = format
        return formatter.string(from: self)
        
    }
    func getCurrentMandarinTime() -> String{
        let comp = Calendar.current.dateComponents([.month,.day], from: self)

        if let month = comp.month,let day = comp.day {
            if UserDefaults.standard.getConvertedLanguage() == "en"{
                return String(format: "%02d-%02d", month,day)
            }
            return "\(month)月\(day)日"
        }
        return ""
        
    }
    func convertToDateShortComponent(text:String,format:String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = format
        let date = formatter.date(from: text)
        
        if let dt = date {
        let comp = Calendar.current.dateComponents([.year,.month,.day], from: dt)
        
        if let year = comp.year, let month = comp.month, let day = comp.day {
            var timeString = ""
            
            timeString += "\(year)年"
            timeString += "\(month)月"
            timeString += "\(day)日"
            if UserDefaults.standard.getConvertedLanguage() == "en"{
                return String(format: "%d-%02d-%02d", year,month,day)
            }
            return timeString
        }
        }
        return ""
    }
    func convertToRangeDate(beginDate:Date?,endDate:Date?) -> String{
        
        if let bt = beginDate, let end = endDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd"
            let start = formatter.string(from: bt)
            let endt = formatter.string(from: end)
            
            return "\(start)-\(endt)"
        }
        return ""
    }
    func convertTimeToChinese() -> String{
        var comp = Calendar.current.dateComponents([.hour,.minute], from: self)
        comp.timeZone = TimeZone(abbreviation: "GMT+8")
        var timeString = ""
        if let hour = comp.hour, let minute = comp.minute {
            if hour < 12 {
                timeString += String(format: " 上午%02d:%02d",hour,minute)
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    return String(format: "%02d:%02d am", hour,minute)
                }
            }
            else{
                let convertDict = [12:12,13:1,14:2,15:3,16:4,17:5,18:6,19:7,20:8,21:9,22:10,23:11]
                timeString += String(format: " 下午%02d:%02d",convertDict[hour] ?? 0,minute)
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    return String(format: "%02d:%02d pm", convertDict[hour] ?? 0,minute)
                }
            }
        }
        
        return timeString
        
    }
    func convertToDateComponent(text:String,format : String = "yyyy/MM/dd HH:mm:ss",onlyDate:Bool=false,addEight : Bool = true) -> String {
        //print(111,text,format)
        let formatter = DateFormatter()
       //formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = format
        let date = formatter.date(from: text)
       
        if let date = date{
        var comp = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: date)
            
        if let year = comp.year, let month = comp.month, let day = comp.day, var hour = comp.hour, let minute = comp.minute {
            
            var timeString = ""
            if onlyDate {
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    return String(format: "%d-%02d-%02d",year,month,day)
                }
                return "\(year)年\(month)月\(day)日"
            }
            if addEight {
            hour += 8
            }
            
            timeString += "\(year)年"
            
            timeString += "\(month)月"
            timeString += "\(day)日"
            
            if hour < 12 {
                timeString += String(format: " 上午%02d:%02d",hour,minute)
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    
                    let newDate = String(format: "%d-%02d-%02d %02d:%02d am",year,month,day,hour,minute)
                    return newDate
                }
            }
            else{
                let convertDict = [12:12,13:1,14:2,15:3,16:4,17:5,18:6,19:7,20:8,21:9,22:10,23:11]
                timeString += String(format: " 下午%02d:%02d",convertDict[hour] ?? 0,minute)
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    
                    let newDate = String(format: "%d-%02d-%02d %02d:%02d pm",year,month,day,convertDict[hour] ?? 0,minute)
                    return newDate
                }
            }

            return timeString
        }
        }
        return ""
    }
}



struct InterViewInfos : Codable {
    var data : InterViewInfo?
   
}
struct DetailInterViewInfosArray:Codable {
    var data : [InterViewInfo]
}
enum DifId : Codable {
    case int(Int), string(String)
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        throw QuantumError.missingValue
    }
    
    enum QuantumError:Error {
        case missingValue
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .int(let x):
            try container.encode(x)
        }
    }
}
extension DifId {
    var stringValue : String? {
        switch self {
        case .string(let string): return string
        case .int(let int): return String(int)
        }
    }
}
struct InterViewInfo : Codable {
    var id : DifId
    var status : InterviewStatusEnum
    var date : String?
    var rating : Int?
    var address : String?
    var order_date : String?
    var updated_at : String?
    
    var account : Customer?
    var description : String?
    var interview_period : [String]?
    var products : [Product]
    var order_reminder_date : String?
    var files : [InterviewFile]?
    var discount_reason : String?
    var discount : String?
    var owner : InterviewOwner
    var editor : InterviewOwner?
    var delivery_date : String?
    var account_order_code : String?
    var export_files : [InterviewPreviewFile]?
}
struct InterviewPreviewFile : Codable {
    var pdf : String
    var excel : String
}
struct InterviewOwner: Codable {
    var username: String?
    var id : String?
    var name : String?
    var employee : InterviewEmployee?
    var roles : [Role]?
    
    func isManager() -> Bool {
        if let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let user = try? JSONDecoder().decode(UserData.self, from: dt) {
            return roles?.contains { rt in
                return rt.kinds.contains { ss in
                    return ss.code == "after-sales-service"
                } && (rt.manages?.contains(where: { mt in
                    return mt.code == user.data.username
                }) ?? false)
            } ?? false

        }
        
        
        return false
    }
}
struct InterviewEmployee:Codable {
    var code: String?
    var name : String?
    var department_code: String?
    var department_name: String?
}
struct InterviewFile : Codable {
    var id : String?
    var tags : [String]?
    var path : String?
    var path_url : String?
    //var image : UIImage?
}

struct InterViewListData : Codable {
    var data : [InterViewList]
}
struct InterViewList : Codable {
    var id : String
    var status : String
    var date : String?
    var rating : Int?
    var order_date : String?
    
    var account : Customer?
    
    //var owner: InterviewOwner
    var owner : String?
}
struct InterviewAndCustomerListData : Codable {
    var data : [InterviewAndCustomer]
    var meta : f_Meta?
}
struct InterviewAndCustomer : Codable {
    var id : String
    
    var status : String?
    var date : String?
    var order_date : String?
    var account_name : String?
    
    var code : String?
    var name : String?
    var tel : String?
    var contact : String?
    var business_number : String?
    var address : String?
}
struct InterViewRecordList : Codable {
    var data : [InterViewRecord]
}
enum InterviewStatusEnum : Codable {
    case int(Int), string(String)
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        throw QuantumError.missingValue
    }
    
    enum QuantumError:Error {
        case missingValue
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .int(let x):
            try container.encode(x)
        }
    }
}
struct InterViewRecord : Codable {
    var id : Int
    var call_id : String?
    var source_id : String?
    var status : InterviewStatusEnum
    var date : String?
    var rating : Int?
    var order_date : String?
    var updated_at : String?
    
    var account : Customer?
   //var editor :
    var editor : InterviewOwner?
    
}
