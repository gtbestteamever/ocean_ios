//
//  NewCustomerView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/31/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit


enum CreateDetailMode {
    case CustomerDetail
    case ContactDetail
}
class CreateCustomerDetailView:WorkDetailView {
    var canEdit = true
    let tableview = UITableView(frame: .zero, style: .grouped)
    var mode : CreateDetailMode?
    var fields = [CustomerField]() {
        didSet{
            tableview.reloadData()
        }
    }
    var isApp = true
    var customerInfo = CustomerCreateInfo(name: "", address: "", business_number: "", company_phone: "", main_contact: "", num_worker: "", industry_type: nil, industry_info: "", equipment_array: [], upper_array: [], down_array: [])
    var contacts = [CustomerContact]()
    var industries = [Industry]() {
        didSet{
            self.tableview.reloadData()
        }
    }
    var anchorConstraint : AnchoredConstraints?
    var remove_machine = [Int]()
    var remove_contact = [Int]()
    init(mode:CreateDetailMode,text:String) {
        super.init(frame: .zero)
        titlelabel.text = text
        self.mode = mode
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        anchorConstraint = tableview.anchor(top: self.topview.bottomAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor,padding: .init(top: self.mode == .CustomerDetail ? 12.calcvaluey() : 0, left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.contentInset.top = self.mode == .CustomerDetail ? 12.calcvaluey() : 6.calcvaluey()
        tableview.contentOffset = .init(x: 0, y: -12.calcvaluey())
        tableview.register(CreateDetailFormCell.self, forCellReuseIdentifier: "form_cell")
        tableview.register(DoubleCustomFieldCell.self, forCellReuseIdentifier: "double_cell")
        tableview.register(DetailAddEqupimentCell.self, forCellReuseIdentifier: "add_cell")
        tableview.register(Upper_DownFactoryCell.self, forCellReuseIdentifier: "up_down")
        tableview.register(DetailEquipmentCell.self, forCellReuseIdentifier: "equip_cell")
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cll")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension Array {
  mutating func remove(at indexes: [Int]) {
    for index in indexes.sorted(by: >) {
      remove(at: index)
    }
  }
}
extension CreateCustomerDetailView: UITableViewDataSource,UITableViewDelegate,DetailFormCellDelegate,AddNewFieldDelegate,RemoveFieldDelegate,RemoveContactDelegate {
    func removeContact(tag: Int) {
        let ct = self.contacts.remove(at: tag)
        print(ct)
        if let id = ct.id {
            remove_contact.append(id)
        }
        self.fields.remove(at: [tag,tag+1,tag+2,tag+3,tag+4])

        
        print(331,remove_contact)
        UIView.performWithoutAnimation {
            self.tableview.reloadData()
        }
        
    }
    
    func setIndustry(industry:Industry) {
        self.customerInfo.industry_type = industry
        
        self.tableview.reloadSections(IndexSet(arrayLiteral: 4), with: .none)
    }
    func setData(mode: Int, index: Int,number:Int, text: String) {
        let c_mode = fields[mode]
        if c_mode == .CustomerName {
            self.customerInfo.name = text
        }
        else if c_mode == .Industry {
            print(6678,text)
            self.customerInfo.industry_info = text
        }
        else if c_mode == .Address {
            self.customerInfo.address = text
        }
        else if c_mode == .NO_Phone {
            if index == 0{
                self.customerInfo.business_number = text
            }
            else{
                self.customerInfo.company_phone = text
            }
        }
        else if c_mode == .MainContact_Worker {
            if index == 0{
                self.customerInfo.main_contact = text
            }
            else{
                self.customerInfo.num_worker = text
            }
        }
        else if c_mode == .Equipment {
            if index == 0{
                self.customerInfo.equipment_array[number].model = text
            }
            else if index == 1{
                self.customerInfo.equipment_array[number].brand = text
            }
            else if index == 3{
                self.customerInfo.equipment_array[number].qty = Int(text) ?? 1
            }
            else{
                self.customerInfo.equipment_array[number].year = text
            }
        }
        else if c_mode == .UpperFactory{
            self.customerInfo.upper_array[number].text = text
        }
        else if c_mode == .DownwardFactory{
            self.customerInfo.down_array[number].text = text
        }
        else if c_mode == .ContactName_PhoneNumber {
            if index == 0{
                self.contacts[number].name = text
            }
            else{
                self.contacts[number].phone_number = text
            }
            
        }
        else if c_mode == .ContactBirth_Interest {
            if index == 0{
                
                self.contacts[number].birthDay = text
                self.tableview.reloadData()
            }
            else{
                self.contacts[number].interest = text
            }
        }
        else if c_mode == .ContactPosition_ExPhone {
            if index == 0{
                self.contacts[number].position = text
            }
            else{
                self.contacts[number].exTel = text
            }
        }
        else if c_mode == .ContactMail {
            self.contacts[number].email = text
        }
        else if c_mode == .ContactOther {
            self.contacts[number].other = text
        }

    }
    
    func removeAt(mode:Int,tag: Int) {
        let c_mode = fields[mode]
        
        if c_mode == .Equipment {
            let ct = customerInfo.equipment_array.remove(at: tag)
            
            if let id = ct.id {
                remove_machine.append(id)
            }
        }
        else if c_mode == .UpperFactory {
            customerInfo.upper_array.remove(at: tag)
        }
        else if c_mode == .DownwardFactory {
            customerInfo.down_array.remove(at: tag)
        }
        self.tableview.reloadData()
    }
    
    func goAddNewField(tag: Int) {
        if fields[tag] == .Equipment {
            customerInfo.equipment_array.append(NewCustomerEquipment(id:nil,model: "", qty: 1, brand: "", year: ""))
        }
        else if fields[tag] == .UpperFactory {
            customerInfo.upper_array.append(FactoryUser(text: ""))
        }
        else if fields[tag] == .DownwardFactory {
            customerInfo.down_array.append(FactoryUser(text: ""))
        }
        self.tableview.reloadData()
    }
    
    func reloadHeight() {
        UIView.performWithoutAnimation {
            self.tableview.beginUpdates()
            self.tableview.endUpdates()
        }

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return fields.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fields[section] == .Equipment {

            
                return customerInfo.equipment_array.count + 1
            
        }
        if fields[section] == .UpperFactory {
   
            return customerInfo.upper_array.count + 1
        }
        if fields[section] == .DownwardFactory {

            return customerInfo.down_array.count + 1
        }
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    let mode = fields[indexPath.section]
//        if mode == .Industry {
//
//        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mode = fields[indexPath.section]
        


        if mode == .CustomerName {
            let cell = tableview.dequeueReusableCell(withIdentifier: "form_cell", for: indexPath) as! CreateDetailFormCell
            cell.frame = tableview.bounds
            cell.tag = indexPath.section
            cell.arrowimage.isHidden = true
            
            cell.stackview.textForm.placeholderLabel.text = "請輸入客戶名稱".localized
            if canEdit && isApp{
                cell.isUserInteractionEnabled = true
                cell.stackview.textForm.placeholderLabel.text = "請輸入客戶名稱".localized
            }
            else{
                
                cell.isUserInteractionEnabled = false
                cell.stackview.textForm.placeholderLabel.text = ""
            }
            cell.stackview.textForm.text = self.customerInfo.name
            cell.delegate = self
            return cell

        }
        else if mode == .Address {
            let cell = tableview.dequeueReusableCell(withIdentifier: "form_cell", for: indexPath) as! CreateDetailFormCell
            cell.arrowimage.isHidden = true
            cell.tag = indexPath.section
            cell.stackview.textForm.placeholderLabel.text = "請輸入公司地址".localized
            cell.stackview.textForm.text = self.customerInfo.address
            cell.delegate = self
            if canEdit && isApp{
                cell.isUserInteractionEnabled = true
                cell.stackview.textForm.placeholderLabel.text = "請輸入公司地址".localized
            }
            else{
                cell.isUserInteractionEnabled = false
                cell.stackview.textForm.placeholderLabel.text = ""
            }
            return cell

        }
        else if mode == .Industry {
            let cell = tableview.dequeueReusableCell(withIdentifier: "form_cell", for: indexPath) as! CreateDetailFormCell
            cell.stackview.textForm.industries = self.industries
            cell.stackview.textForm.con = self
            cell.infoView.isHidden = false
            cell.stackview.textForm.text = self.customerInfo.industry_type?.titles?.getLang()
            
            cell.infoView.text = self.customerInfo.industry_info
            
            cell.tag = indexPath.section
            cell.stackview.textForm.mode = .Industry
            cell.delegate = self
            if canEdit && isApp{
                cell.arrowimage.isHidden = false
                cell.isUserInteractionEnabled = true
                cell.stackview.textForm.placeholderLabel.text = "請選擇行業別".localized
                cell.infoView.placeholderLabel.text = "請輸入說明".localized
            }
            else{
                cell.arrowimage.isHidden = true
                cell.stackview.textForm.placeholderLabel.text = ""
                cell.infoView.placeholderLabel.text = ""
                if canEdit && !isApp{
                    cell.isUserInteractionEnabled = true
                    cell.infoView.isUserInteractionEnabled = true
                    cell.stackview.textForm.isUserInteractionEnabled = false
                    cell.infoView.placeholderLabel.text = "請輸入說明".localized
                }
                else{
                    cell.isUserInteractionEnabled = false
                }
                
                

            }
            return cell
  
        }


        else if mode == .NO_Phone || mode == .MainContact_Worker{
            
            let cell2 = tableview.dequeueReusableCell(withIdentifier: "double_cell", for: indexPath) as? DoubleCustomFieldCell
            cell2?.frame = tableview.frame
            cell2?.delegate = self
            cell2?.tag = indexPath.section
            if mode == .NO_Phone {

                cell2?.firstTextForm.textForm.placeholderLabel.text = "請輸入統一編號".localized
                cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入公司電話".localized
                cell2?.firstTextForm.textForm.text = self.customerInfo.business_number
                cell2?.secondTextForm.textForm.text = self.customerInfo.company_phone
            }
            else if mode == .MainContact_Worker {
                cell2?.firstTextForm.textForm.placeholderLabel.text = "請輸入主要聯絡人".localized
                cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入員工人數".localized
                cell2?.firstTextForm.textForm.text = self.customerInfo.main_contact
                cell2?.secondTextForm.textForm.text = self.customerInfo.num_worker
            }
            else{
                cell2?.firstTextForm.textForm.text = nil
                cell2?.secondTextForm.textForm.text = nil
            }
            if canEdit && isApp{
                cell2?.isUserInteractionEnabled = true

            }
            else{
                cell2?.firstTextForm.textForm.placeholderLabel.text = ""
                cell2?.secondTextForm.textForm.placeholderLabel.text = ""
                if canEdit && !isApp && mode == .MainContact_Worker{
                    cell2?.isUserInteractionEnabled = true
                    cell2?.firstTextForm.textForm.isUserInteractionEnabled = false
                    cell2?.secondTextForm.textForm.isUserInteractionEnabled = true
                    cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入員工人數".localized
                    
                }
                else{
                    cell2?.isUserInteractionEnabled = false
                }
                //cell2?.isUserInteractionEnabled = false

            }
            return cell2 ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
        }
        else if mode == .Equipment{
            if indexPath.row == customerInfo.equipment_array.count {
                let add_cell = tableview.dequeueReusableCell(withIdentifier: "add_cell", for: indexPath) as? DetailAddEqupimentCell
                add_cell?.addMachine.titles.text = "加入機台".localized
                add_cell?.tag = indexPath.section
                add_cell?.delegate = self
                if canEdit {
                    add_cell?.addMachine.isHidden = false
                }
                else{
                    add_cell?.addMachine.isHidden = true
                }
                return add_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
            }
            let equip_cell = tableView.dequeueReusableCell(withIdentifier: "equip_cell", for: indexPath) as? DetailEquipmentCell
            equip_cell?.tag = indexPath.section
            equip_cell?.array_index = indexPath.row
            equip_cell?.xbutton.tag = indexPath.row
            equip_cell?.delegate = self
            equip_cell?.c_delegate = self
            equip_cell?.number_label.text = "\(indexPath.row + 1)."
            equip_cell?.textForm1.text = self.customerInfo.equipment_array[indexPath.row].model
            equip_cell?.doubleForm.firstTextForm.textForm.text = self.customerInfo.equipment_array[indexPath.row].brand
            equip_cell?.doubleForm.secondTextForm.textForm.text = self.customerInfo.equipment_array[indexPath.row].year
            equip_cell?.counter.currentCount = self.customerInfo.equipment_array[indexPath.row].qty
            if canEdit{
                equip_cell?.isUserInteractionEnabled = true
                equip_cell?.xbutton.isHidden = false
            }
            else{
                equip_cell?.isUserInteractionEnabled = false
                equip_cell?.xbutton.isHidden = true
                equip_cell?.textForm1.placeholderLabel.text = ""
                equip_cell?.doubleForm.firstTextForm.textForm.placeholderLabel.text = ""
                equip_cell?.doubleForm.secondTextForm.textForm.placeholderLabel.text = ""
            }
            return equip_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
        }
        else if mode == .UpperFactory || mode == .DownwardFactory{
            if mode == .UpperFactory {
                if indexPath.row == customerInfo.upper_array.count {
                    let add_cell = tableview.dequeueReusableCell(withIdentifier: "add_cell", for: indexPath) as? DetailAddEqupimentCell
                    add_cell?.addMachine.titles.text = "加入上游廠商".localized
                    add_cell?.tag = indexPath.section
                    add_cell?.delegate = self
                    if canEdit {
                        add_cell?.addMachine.isHidden = false
                    }
                    else{
                        add_cell?.addMachine.isHidden = true
                    }
                    return add_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
                }

                
                let up_down_cell = tableview.dequeueReusableCell(withIdentifier: "up_down", for: indexPath) as? Upper_DownFactoryCell
                up_down_cell?.stackview.textForm.placeholderLabel.text = "請輸入上游廠商".localized
                up_down_cell?.tag = indexPath.section
                up_down_cell?.array_index = indexPath.row
                up_down_cell?.xbutton.tag = indexPath.row
                up_down_cell?.tt_delegate = self
                up_down_cell?.delegate = self
                up_down_cell?.stackview.textForm.text = self.customerInfo.upper_array[indexPath.row].text
                if canEdit{
                    up_down_cell?.isUserInteractionEnabled = true
                    up_down_cell?.xbutton.isHidden = false
                }
                else{
                    up_down_cell?.stackview.textForm.placeholderLabel.text = ""
                    up_down_cell?.isUserInteractionEnabled = false
                    up_down_cell?.xbutton.isHidden = true
                }
            return up_down_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
            }
            else if mode == .DownwardFactory{
                if indexPath.row == customerInfo.down_array.count {
                    let add_cell = tableview.dequeueReusableCell(withIdentifier: "add_cell", for: indexPath) as?  DetailAddEqupimentCell
                    add_cell?.addMachine.titles.text = "加入下游廠商".localized
                    add_cell?.tag = indexPath.section
                    add_cell?.delegate = self
                    if canEdit {
                        add_cell?.addMachine.isHidden = false
                    }
                    else{
                        add_cell?.addMachine.isHidden = true
                    }
                    return add_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
                }

                
                let up_down_cell = tableview.dequeueReusableCell(withIdentifier: "up_down", for: indexPath) as? Upper_DownFactoryCell
                up_down_cell?.stackview.textForm.placeholderLabel.text = "請輸入下游廠商".localized
                up_down_cell?.tag = indexPath.section
                up_down_cell?.array_index = indexPath.row
                up_down_cell?.xbutton.tag = indexPath.row
                up_down_cell?.tt_delegate = self
                up_down_cell?.delegate = self
                up_down_cell?.stackview.textForm.text = self.customerInfo.down_array[indexPath.row].text
                if canEdit{
                    up_down_cell?.isUserInteractionEnabled = true
                    up_down_cell?.xbutton.isHidden = false
                }
                else{
                    up_down_cell?.stackview.textForm.placeholderLabel.text = ""
                    up_down_cell?.isUserInteractionEnabled = false
                    up_down_cell?.xbutton.isHidden = true
                }
            return up_down_cell ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
            }
        }
        
        else if mode == .ContactName_PhoneNumber || mode == .ContactBirth_Interest || mode == .ContactPosition_ExPhone{
            let cell2 = tableview.dequeueReusableCell(withIdentifier: "double_cell", for: indexPath) as? DoubleCustomFieldCell
            cell2?.frame = tableview.bounds
            cell2?.tag = indexPath.section
            cell2?.array_index = indexPath.section/5
            cell2?.delegate = self
    

            if mode == .ContactName_PhoneNumber {
                cell2?.firstTextForm.textForm.placeholderLabel.text = "請輸入名字".localized
                cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入手機號碼".localized

                cell2?.firstTextForm.textForm.text = self.contacts[indexPath.section/5].name
                cell2?.secondTextForm.textForm.text = self.contacts[indexPath.section/5].phone_number
                
            }
            else if mode == .ContactBirth_Interest {
                cell2?.firstTextForm.textForm.placeholderLabel.text = "請選擇日期".localized
                cell2?.firstTextForm.textForm.mode = .Date
                cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入興趣".localized
                
                cell2?.firstTextForm.textForm.text = self.contacts[indexPath.section/5].birthDay
                cell2?.secondTextForm.textForm.text = self.contacts[indexPath.section/5].interest
                
            }
            
            else if mode == .ContactPosition_ExPhone {
                cell2?.firstTextForm.textForm.placeholderLabel.text = "請輸入職稱".localized
                cell2?.secondTextForm.textForm.placeholderLabel.text = "請輸入公司分機".localized
                
                cell2?.firstTextForm.textForm.text = self.contacts[indexPath.section/5].position
                cell2?.secondTextForm.textForm.text = self.contacts[indexPath.section/5].exTel
                
            }

            if canEdit{
                cell2?.isUserInteractionEnabled = true
            }
            else{
                cell2?.isUserInteractionEnabled = false
                cell2?.firstTextForm.textForm.placeholderLabel.text = ""
                cell2?.secondTextForm.textForm.placeholderLabel.text = ""
            }

            return cell2 ?? UITableViewCell(style: .default, reuseIdentifier: "cll")
        }
        else if mode == .ContactMail || mode == .ContactOther {
            let cell = tableview.dequeueReusableCell(withIdentifier: "form_cell", for: indexPath) as! CreateDetailFormCell
            cell.frame = tableview.frame
            cell.arrowimage.isHidden = true

            if mode == .ContactMail {
                cell.stackview.textForm.placeholderLabel.text = "請輸入信箱".localized
                
                cell.stackview.textForm.text = self.contacts[indexPath.section/5].email
                
            }
            else{
                cell.stackview.textForm.placeholderLabel.text = "其他".localized
                
                cell.stackview.textForm.text = self.contacts[indexPath.section/5].other
                
            }
            if canEdit {
                cell.isUserInteractionEnabled = true
            }
            else{
                cell.isUserInteractionEnabled = false
                cell.stackview.textForm.placeholderLabel.text = ""
            }
            cell.tag = indexPath.section
            cell.array_index = indexPath.section/5
            cell.delegate = self
            return cell
        }
        return tableview.dequeueReusableCell(withIdentifier: "cll", for: indexPath)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mode = fields[section]
        if mode == .NO_Phone || mode == .MainContact_Worker || mode == .ContactName_PhoneNumber || mode == .ContactBirth_Interest || mode == .ContactPosition_ExPhone || mode == .Industry {
            
            
            let vd = CustomerDoubleHeader()
            
            if mode == .NO_Phone {
                vd.lT.attributedText = "統一編號".localized.convertToAttributedString()
                vd.lT2.text = "公司電話".localized
            }
            else if mode == .Industry {
                vd.lT.text = "行業別".localized
                vd.lT2.text = "行業別說明".localized
            }
            else if mode == .ContactName_PhoneNumber {
                let vd = CustomerTitleDoubleHeader()
                //vd.backgroundColor = .red
                vd.lT.text = "聯絡人".localized
                vd.lT2.text = "聯絡人手機號碼".localized
                vd.contactLabel.text = "\("聯絡人".localized) \((section/5)+1)"
                vd.tag = section/5
                vd.delegate = self
                if canEdit{
                vd.xbutton.isHidden = false
                }
                return vd
            }
            else if mode == .ContactBirth_Interest {
                vd.lT.text = "聯絡人生日".localized
                vd.lT2.text = "聯絡人興趣".localized
            }
            else if mode == .ContactPosition_ExPhone {
                vd.lT.text = "聯絡人職稱".localized
                vd.lT2.text = "聯絡人公司分機".localized
            }
            else{
                vd.lT.text = "主要聯絡人".localized
                vd.lT2.text = "員工人數".localized
            }
            return vd
        }
        let vd = CustomerHeader()
        if mode == .CustomerName {
            vd.lT.attributedText = "客戶公司名稱".localized.convertToAttributedString()
        }
        else if mode == .Address {
            vd.lT.attributedText = "公司地址".localized.convertToAttributedString()
        }
//        else if mode == .Industry {
//            vd.lT.text = "行業別"
//        }
        else if mode == .Equipment{
            vd.lT.text = "場內使用設備".localized
        }
        else if mode == .UpperFactory {
            vd.lT.text = "上游廠商".localized
        }
        else if mode == .DownwardFactory {
            vd.lT.text = "下游廠商".localized
        }
        else if mode == .ContactMail{
            vd.lT.text = "聯絡人信箱".localized
        }
        else if mode == .ContactOther {
            vd.lT.text = "其他".localized
        }

        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let mode = fields[section]
        if mode == .ContactName_PhoneNumber {
            return 90.calcvaluey()
        }
        return 16.calcvaluey()
    }
    
}

class CustomDetailStackView : UIStackView {
    var textForm = FormTextView(placeText: "", mode: .CreateCustomer)
    var formHeightAnchor : NSLayoutConstraint?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.alignment = .fill
        self.axis = .horizontal
        self.spacing = 12.calcvaluey()
        self.distribution = .fillEqually
        //textForm.backgroundColor = .red
        addArrangedSubview(textForm)
        
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol DetailFormCellDelegate {
    func reloadHeight()
    func setData(mode:Int,index:Int,number:Int,text:String)
}
class CreateDetailFormCell:UITableViewCell,FormTextViewDelegate {
    func textViewReloadHeight(height: CGFloat) {
        print(1221)
        //stackview.formHeightAnchor?.constant = height
        delegate?.reloadHeight()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        print(tag,text)
        delegate?.setData(mode: self.tag, index: stackview.tag, number: array_index, text: text)
    }
    
    var array_index = 0
    var delegate: DetailFormCellDelegate?
    let arrowimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    var stackview = CustomDetailStackView()
    var infoView = FormTextView(placeText: "請輸入說明", mode: .DiscountInfo)
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        print(221,self.frame)
        //self.layoutIfNeeded()
        selectionStyle = .none
        contentView.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 26.calcvaluex(), bottom: 12.calcvaluey(), right: 26.calcvaluex()))
        //stackview.addArrangedSubview(infoView)
      //  infoView.isHidden = true
//        contentView.addSubview(arrowimage)
//        arrowimage.isHidden = true
//        arrowimage.contentMode = .scaleAspectFit
//        arrowimage.centerYInSuperview()
//        arrowimage.anchor(top: nil, leading: nil, bottom: nil, trailing: stackview.arrangedSubviews[0].trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        infoView.t_delegate = self
        stackview.textForm.t_delegate = self
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class DoubleCustomFieldCell : UITableViewCell,FormTextViewDelegate {
    override func prepareForReuse() {
        
        self.firstTextForm.textForm.text = nil
        self.secondTextForm.textForm.text = nil
        self.firstTextForm.textForm.textViewDidChange(self.firstTextForm.textForm)
        self.secondTextForm.textForm.textViewDidChange(self.secondTextForm.textForm)
    }
    func textViewReloadHeight(height: CGFloat) {
        delegate?.reloadHeight()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .FirstDouble || mode == .Date{
            
            delegate?.setData(mode: self.tag, index: 0, number: array_index, text: text)
        }
        else{
            delegate?.setData(mode: self.tag, index: 1,number: array_index, text: text)
        }

    }
    
    let firstTextForm = CustomDetailStackView()
    let secondTextForm = CustomDetailStackView()
    let stackview = UIStackView()
    var array_index = 0
    var anchorConstraint : AnchoredConstraints?
    var delegate:DetailFormCellDelegate?
    let verticalStackView = UIStackView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none

        
        firstTextForm.textForm.mode = .FirstDouble
        secondTextForm.textForm.mode = .SecondDouble
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.spacing = 12.calcvaluex()
        stackview.alignment = .top
        stackview.addArrangedSubview(firstTextForm)
        stackview.addArrangedSubview(secondTextForm)
        
        

        contentView.addSubview(stackview)
        anchorConstraint = stackview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 26.calcvaluex(), bottom: 12.calcvaluey(), right: 26.calcvaluex()))
        firstTextForm.textForm.t_delegate = self
        secondTextForm.textForm.t_delegate = self
        

        //firstTextForm.textForm.delegate = self
        //secondTextForm.textForm.delegate = self
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol AddNewFieldDelegate {
    func goAddNewField(tag:Int)
}
class DetailAddEqupimentCell : UITableViewCell {
    let addMachine = AddButton4()
    var delegate:AddNewFieldDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        //addMachine.titles.text = "加入\(text)"
        contentView.addSubview(addMachine)
        addMachine.anchor(top: contentView.topAnchor, leading: nil, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        addMachine.layer.cornerRadius = 50.calcvaluey()/2
        addMachine.centerXInSuperview()
        
        addMachine.addTarget(self, action: #selector(addNewData), for: .touchUpInside)
    }

    @objc func addNewData(){
        delegate?.goAddNewField(tag: self.tag)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol RemoveFieldDelegate{
    func removeAt(mode:Int,tag:Int)
}
class DetailEquipmentCell : UITableViewCell,FormTextViewDelegate,SelectedMachineCellDelegate {
    
    func reloadView() {
        //
    }
    func showFile(filePaths: [FilePath]) {
        //
    }
    
    func setNewOption(index: Int) {
        //
    }
    
    func setNewCustomOption(index: Int, subIndex: Int) {
        //
    }
    
    func removeOption(index: Int, subIndex: Int) {
        //
    }
    
    func removeCustom(index: Int, subIndex: Int, thirdIndex: Int) {
        //
    }
    
    func removeMachine(index: Int) {
        //
    }
    
    func sendCustomChoice(index: Int, subIndex: Int, thirdIndex: Int, textField: UITextField) {
        //
    }
    
    func addCustomFile(index: Int, subIndex: Int, thirdIndex: Int, textField: UIImageView) {
        //
    }
    
    func removeCustomFile(index: Int, subIndex: Int, thirdIndex: Int, textField: UIImageView, addedFile: AddedOptionFile?) {
        //
    }
    
    func setNewDiscount() {
        //
    }
    
    func removeDiscount(index: Int) {
        //
    }
    
    func sendCustomDate(index: Int, subIndex: Int, thirdIndex: Int, text: String) {
        //
    }
    
    func sendCustomName(index: Int, subIndex: Int, thirdIndex: Int, text: String) {
        //
    }
    
    func sendCustomPrice(index: Int, subIndex: Int, thirdIndex: Int, text: String) {
        //
    }
    
    func setCustomCount(index: Int, subIndex: Int, thirdIndex: Int, amount: Int) {
        //
    }
    
    func addNewCustomForm(index: Int) {
        //
    }
    
    func modifyCustomStatus(index: Int, subIndex: Int, textField: UITextField) {
        //
    }
    
    func setMachineQty(index: Int, qty: Int) {
        c_delegate?.setData(mode: self.tag, index: 3, number: self.array_index, text: String(qty))
    }
    
    func showCustomAttachFile(file: AddedOptionFile) {
        //
    }
    
    func textViewReloadHeight(height: CGFloat) {
        //
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .EquipModel {
            c_delegate?.setData(mode: self.tag, index: 0, number: array_index, text: text)
        }
        else if mode == .EquipBrand {
            c_delegate?.setData(mode: self.tag, index: 1, number: array_index, text: text)
        }
        else{
            c_delegate?.setData(mode: self.tag, index: 2, number: array_index, text: text)
        }
    }
    
    let number_label : UILabel = {
       let num = UILabel()
        num.text = "1."
        num.textColor = .black
        num.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        num.textAlignment = .center
        return num
    }()
    var array_index = 0
    var textForm1 = FormTextView(placeText: "", mode: .CreateCustomer)
    var counter = CounterView(minCount: 1)
    var formHeightAnchor : NSLayoutConstraint?
    var doubleForm = DoubleCustomFieldCell()
    let main_stack = UIStackView()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()
    var delegate:RemoveFieldDelegate?
    var c_delegate:DetailFormCellDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textForm1.mode = .EquipModel
        doubleForm.firstTextForm.textForm.mode = .EquipBrand
        doubleForm.secondTextForm.textForm.mode = .EquipYear
        textForm1.placeholderLabel.text = "機台型號".localized
        
        let h_stack = UIStackView()
        h_stack.axis = .horizontal
        h_stack.distribution = .fill
        h_stack.spacing = 8.calcvaluex()
        h_stack.alignment = .fill
        
        h_stack.addArrangedSubview(textForm1)
        formHeightAnchor = textForm1.heightAnchor.constraint(equalToConstant: 52.calcvaluey())
        formHeightAnchor?.isActive = true
        
        h_stack.addArrangedSubview(counter)
        counter.constrainHeight(constant: 48.calcvaluey())
        counter.constrainWidth(constant: 120.calcvaluex())
        
        let v_stack = UIStackView()
        v_stack.axis = .vertical
        v_stack.distribution = .fill
        v_stack.spacing = 8.calcvaluex()
        v_stack.alignment = .fill
        
        
        v_stack.addArrangedSubview(h_stack)
        v_stack.addArrangedSubview(doubleForm)
        main_stack.alignment = .fill
        main_stack.axis = .vertical
        main_stack.spacing = 12.calcvaluey()
        main_stack.distribution = .fill
        

        doubleForm.firstTextForm.textForm.placeholderLabel.text = "廠牌".localized
        doubleForm.secondTextForm.textForm.placeholderLabel.text = "出廠年份".localized
        doubleForm.anchorConstraint?.top?.constant = 0
        doubleForm.anchorConstraint?.leading?.constant = 0
        doubleForm.anchorConstraint?.trailing?.constant = 0
        doubleForm.anchorConstraint?.bottom?.constant = 0
        
        //main_stack.addArrangedSubview(doubleForm)
         var horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 12.calcvaluex()
        horizontalStackView.distribution = .fill
        horizontalStackView.alignment = .center
        
        horizontalStackView.addArrangedSubview(number_label)
        number_label.constrainWidth(constant: number_label.text?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!) ?? 0.0)
        horizontalStackView.addArrangedSubview(v_stack)

        horizontalStackView.addArrangedSubview(xbutton)

        xbutton.constrainWidth(constant: 24.calcvaluex())
        xbutton.constrainHeight(constant: 24.calcvaluex())
        main_stack.addArrangedSubview(horizontalStackView)
        
        

                contentView.addSubview(main_stack)
        main_stack.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 26.calcvaluex(), bottom: 12.calcvaluey(), right: 26.calcvaluex()))
        xbutton.addTarget(self, action: #selector(removeField), for: .touchUpInside)
        
        textForm1.t_delegate = self
        doubleForm.firstTextForm.textForm.t_delegate = self
        doubleForm.secondTextForm.textForm.t_delegate = self
        counter.firstIndex = 0
        counter.delegate = self
    }
    @objc func removeField(sender:UIButton){
        delegate?.removeAt(mode: self.tag, tag: sender.tag)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum CustomerField {
    case CustomerName
    case Address
    case NO_Phone
    case MainContact_Worker
   
    case Industry
    case Equipment
    case UpperFactory
    
    case DownwardFactory
    
    case ContactName_PhoneNumber
    case ContactMail
    case ContactBirth_Interest
    case ContactPosition_ExPhone
    case ContactOther
}
class Upper_DownFactoryCell:CreateDetailFormCell {
    override func sendTextViewData(mode: FormTextViewMode, text: String) {
        delegate?.setData(mode: self.tag, index: 0, number: self.array_index, text: text)
    }
    let number_label : UILabel = {
       let num = UILabel()
        num.text = "1."
        num.textColor = .black
        num.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        num.textAlignment = .center
        return num
    }()

    let h_stackview = UIStackView()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()
    var tt_delegate : RemoveFieldDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        h_stackview.addArrangedSubview(number_label)
        number_label.constrainWidth(constant: number_label.text?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!) ?? 0.0)
        h_stackview.addArrangedSubview(stackview.textForm)
        h_stackview.addArrangedSubview(xbutton)
        xbutton.constrainWidth(constant: 24.calcvaluex())
        xbutton.constrainHeight(constant: 24.calcvaluex())
        h_stackview.distribution = .fill
        h_stackview.axis = .horizontal
        h_stackview.spacing = 12.calcvaluex()
        h_stackview.alignment = .center
        stackview.textForm.isHidden = false
        stackview.addArrangedSubview(h_stackview)
        xbutton.addTarget(self, action: #selector(removeField), for: .touchUpInside)
    }
    @objc func removeField(sender:UIButton){
        tt_delegate?.removeAt(mode: self.tag, tag: sender.tag)
    }
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
