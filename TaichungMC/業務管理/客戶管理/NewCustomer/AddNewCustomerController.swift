//
//  AddNewCustomerController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/28/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD

class AddNewCustomerController: SampleController {
    
    let detailView = CreateCustomerDetailView(mode: .CustomerDetail, text: "客戶公司資訊".localized)
    let contactView = CreateCustomerDetailView(mode: .ContactDetail , text: "客戶聯絡人資訊".localized)
    var container = UIView()
    var currentEditMode = false
    var addContact = AddButton4()
    var sendOutView = UIView()
    var company:Company? {
        didSet{

        }
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        if detailView.canEdit {
            if let com = company {
                print(com.data?.code)
                if com.data?.code.contains("APP") ?? false {
                    detailView.isApp = true
                }
                else{
                    detailView.isApp = false
                }
                titleview.label.text = "編輯客戶公司".localized
            }
            else {
                titleview.label.text = "建立客戶公司".localized
            }
        }
        else{
            titleview.label.text = "查看客戶公司".localized
        }
        
        view.addSubview(container)
        
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        container.addSubview(detailView)
        detailView.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.centerXAnchor,padding: .init(top: 18.calcvaluey(), left: 26.calcvaluex(), bottom: 90.calcvaluey(), right: 13.calcvaluex()))
        
        container.addSubview(contactView)
        contactView.anchor(top: detailView.topAnchor, leading: view.centerXAnchor, bottom: detailView.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 13.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        contactView.anchorConstraint?.bottom?.constant = -74.calcvaluey()
        contactView.addSubview(addContact)
        addContact.centerXInSuperview()
        addContact.anchor(top: contactView.tableview.bottomAnchor, leading: contactView.tableview.leadingAnchor, bottom: contactView.bottomAnchor, trailing: contactView.tableview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 24.calcvaluex(), bottom: 12.calcvaluey(), right: 24.calcvaluex()))

        addContact.titles.text = "加入聯絡人".localized
        addContact.addTarget(self, action: #selector(goAddNewContact), for: .touchUpInside)
        detailView.fields = [.CustomerName,.Address,.NO_Phone,.MainContact_Worker]
        
        contactView.fields = []
        fetchApi()
        
        view.addSubview(sendOutView)
        sendOutView.backgroundColor = .white
        sendOutView.addshadowColor()
        
        sendOutView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 78.calcvaluey()))
        
        let saveButton = UIButton(type: .custom)
        saveButton.setTitle("儲存".localized, for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        saveButton.backgroundColor = MajorColor().oceanColor
        
        sendOutView.addSubview(saveButton)
        saveButton.anchor(top: sendOutView.topAnchor, leading: nil, bottom: sendOutView.bottomAnchor, trailing: sendOutView.trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        
        saveButton.addTarget(self, action: #selector(saveCustomer), for: .touchUpInside)
        
        
    }
    func showAlert(text:String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @objc func saveCustomer(){
        let info = detailView.customerInfo
        if info.name == ""{
            showAlert(text: "請輸入公司名稱".localized)
            return
        }
        if info.address == "" {
            showAlert(text: "請輸入公司地址".localized)
            return
        }
//        if info.business_number == ""{
//            showAlert(text: "請輸入統一編號")
//            return
//        }
        let contacts = contactView.contacts
        var param = [String:Any]()
        if let com = company?.data{
            param["code"] = com.code
        }
        param["name"] = info.name
        param["tel"] = info.company_phone
        param["address"] = info.address
        print(221,info.business_number)
        param["business_number"] = info.business_number
        param["contact"] = info.main_contact
        param["type"] = info.industry_type?.code ?? ""
        param["other"] = ["type_description":info.industry_info]
        param["staff_num"] = info.num_worker
        var up_array = [String]()
        for i in info.upper_array {
            up_array.append(i.text)
        }
        param["upstream"] = up_array
        var down_array = [String]()
        for i in info.down_array {
            down_array.append(i.text)
        }
        param["downstream"] = down_array
        
        var machine_array = [[String:Any]]()
        for i in info.equipment_array {
            var machine = [String:Any]()
            if let id = i.id {
                machine["id"] = id
            }
            machine["model"] = i.model
            machine["number"] = String(i.qty)
            machine["manufacturer"] = i.year
            machine["brand"] = i.brand
            machine["detail"] = ""
            machine_array.append(machine)
        }
        param["machines"] = machine_array
        
        var contact_array = [[String:Any]]()
        for i in contacts {
            var con = [String:Any]()
            if let id = i.id {
                con["id"] = id
            }
            con["type"] = i.position
            con["name"] = i.name
            con["interesting"] = i.interest
            con["mobile"] = i.phone_number
            con["email"] = i.email
            con["birthday"] = i.birthDay
            con["other"] = ["ext":i.other]
            contact_array.append(con)
        }
        param["contacts"] = contact_array
        if detailView.remove_machine.count != 0{
            param["del_machines"] = detailView.remove_machine
        }
        
        if contactView.remove_contact.count != 0{
            param["del_contacts"] = contactView.remove_contact
        }
        let hud = JGProgressHUD()
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        if currentEditMode {
            hud.textLabel.text = "編輯中..".localized
        }
        else{
            hud.textLabel.text = "建立中..".localized
        }
        
        hud.show(in: container)
        
        NetworkCall.shared.postCall(parameter: "api-or/v1/account_detail/store", param: param, decoderType: Company.self) { (json) in
            DispatchQueue.main.async {
                if let json = json{
                    if let error = json.ERR_CODE{
                        let message = error == "H505" ? "統一編號已存在請重新輸入".localized : "建立失敗".localized
                        
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        
                        hud.textLabel.text = message
                        hud.dismiss(afterDelay: 1.5)
                        
                        return
                    }
                    print(889,json)
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    if self.currentEditMode {
                        hud.textLabel.text = "編輯成功".localized
                    }
                    else{
                        hud.textLabel.text = "建立成功".localized
                    }
                    
                    hud.dismiss(afterDelay: 1.5, animated: true) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else{
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    if self.currentEditMode {
                        hud.textLabel.text = "編輯失敗".localized
                    }
                    else{
                        hud.textLabel.text = "建立失敗".localized
                    }
                    hud.dismiss(afterDelay: 1.5)
                }
            }

        }
    }
    @objc func goAddNewContact(){
        let tt : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
        for i in tt{
            contactView.fields.append(i)
        }
        contactView.contacts.append(CustomerContact(id: nil, name: "", phone_number: "", email: "", birthDay: "", interest: "", position: "", exTel: "", other: ""))
        contactView.tableview.reloadData()
    }
    func fetchApi(){
        container.isHidden = true
        NetworkCall.shared.getCall(parameter: "api-or/v1/industries", decoderType: Industries.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    self.container.isHidden = false
                    self.detailView.industries = json.data
                    self.setData()
                }
            }

        }
    }
    func setData() {
        if let com = self.company?.data {
            var upperStream = [FactoryUser]()
            var downStream = [FactoryUser]()
            var equipments = [NewCustomerEquipment]()
            for i in com.upstream ?? []{
                upperStream.append(FactoryUser(text: i))
            }
            for i in com.downstream ?? []{
                downStream.append(FactoryUser(text: i))
            }
            for i in com.machines ?? [] {
                equipments.append(NewCustomerEquipment(id: i.id, model: i.model ?? "", qty: Int(i.number ?? "1") ?? 1, brand: i.brand ?? "", year: i.manufacturer ?? ""))
            }
            let indust = detailView.industries.first { (inn) -> Bool in
                return com.type == inn.code.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            let customInfo = CustomerCreateInfo(name: com.name ?? "", address: com.address ?? "", business_number: com.business_number ?? "", company_phone: com.tel ?? "", main_contact: com.contact ?? "", num_worker: com.staff_num ?? "", industry_type: indust, industry_info: com.other?.type_description ?? "", equipment_array: equipments, upper_array: upperStream, down_array: downStream)
            detailView.customerInfo = customInfo
            detailView.tableview.reloadData()
            
            var contacts = [CustomerContact]()
            var fields = [CustomerField]()
            for i in com.contacts ?? [] {
                contacts.append(CustomerContact(id: i.id, name: i.name ?? "", phone_number: i.mobile ?? "", email: i.email ?? "", birthDay: i.birthday ?? "", interest: i.interesting ?? "", position: i.type ?? "", exTel: i.phone ?? "", other: i.other?.ext ?? ""))
                let cust : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
                for j in cust{
                    fields.append(j)
                }
            }
//            if com.contacts?.count == 0{
//                contacts.append(CustomerContact(id: nil, name: "", phone_number: "", email: "", birthDay: "", interest: "", position: "", exTel: "", other: ""))
//                let tt : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
//                for i in tt{
//                    fields.append(i)
//                }
//            }
            contactView.contacts = contacts
            contactView.fields = fields
        }
    }

}
