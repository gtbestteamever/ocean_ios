//
//  DiscountViewCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/24/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol DiscountDelegate {
    func showDiscountInfo(sender:UIButton)
}
class DiscountViewCell : InterviewCell{
    let stackview = UIStackView()
    let label = UILabel()
    let textView = FormTextView(placeText: "請輸入折扣項目".localized, mode: .DiscountInfo)
    let priceField = FormTextView(placeText: "請輸入".localized, mode: .DiscountPrice,setMinus: true)
    let infoButton = UIButton(type: .custom)
    var delegate:DiscountDelegate?
    var mode : InterviewMode? {
        didSet{
            if mode == .Review{
                textView.placeholderLabel.text = ""
                priceField.placeholderLabel.text = ""
            }
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.spacing = 18.calcvaluex()
        stackview.alignment = .top
        container.addSubview(label)
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        label.text = "折扣項目".localized
       
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        

        priceField.constrainWidth(constant: 180.calcvaluex())
        container.addSubview(stackview)
        stackview.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: container.bottomAnchor, trailing: label.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0))
        stackview.addArrangedSubview(textView)
        stackview.addArrangedSubview(priceField)
//        textField.setContentHuggingPriority(.init(1000), for: .horizontal)

        
        infoButton.setImage(#imageLiteral(resourceName: "ic_product_pressed2").withRenderingMode(.alwaysTemplate), for: .normal)
        infoButton.tintColor = MajorColor().oceanSubColor
        container.addSubview(infoButton)
        infoButton.contentVerticalAlignment = .fill
        infoButton.contentHorizontalAlignment = .fill
        infoButton.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 110.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        infoButton.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        infoButton.addTarget(self, action: #selector(showInfo), for: .touchUpInside)
    }
    @objc func showInfo(sender:UIButton){
        
        delegate?.showDiscountInfo(sender: sender)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
