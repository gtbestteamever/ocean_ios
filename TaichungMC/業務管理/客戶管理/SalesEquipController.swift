//
//  SalesEquipController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/6/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class SalesEquipController : SampleController {
    let controller = SelectEquipController(nextvd: SpecRightView(), mode: .Equip)
    let bt = TopViewButton(text: "儲存".localized)
    weak var con : InterviewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        extrabutton_stackview.addArrangedSubview(bt)
        titleview.label.text = "加入選配".localized
//        controller.leftviewAnchor?.width?.constant = 361.calcvaluex()
//        self.view.layoutIfNeeded()
        controller.arrowimage.isHidden = true
        controller.selectLabel.isUserInteractionEnabled = false
        view.addSubview(controller.view)
        controller.view.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        addChild(controller)
        
        bt.layer.cornerRadius = 38.calcvaluey()/2
        
       
        //topview.addSubview(bt)
        bt.constrainHeight(constant: 38.calcvaluey())

        
        bt.addTarget(self, action: #selector(saveOptions), for: .touchUpInside)
        
    }
    
    @objc func saveOptions(){
        con?.setEquipOption(index:self.view.tag,options: controller.optionsArray.sorted(by: { op1, op2 in
            return (op1.option?.order ?? 0) < (op2.option?.order ?? 0)
        }))
        self.dismiss(animated: true, completion: nil)
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}
