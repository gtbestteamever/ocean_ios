//
//  SalesBottomView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension Int {
    func setInterviewStatus() -> String{
        switch self {
        case 0 :
            return ""
        case 1 :
            return "拓展中"
        case 2 :
            return "洽談中"
        case 3 :
            return "轉訂單"
        case 4 :
            return "結案"
        default :
            ()
        }
        return ""
    }
    
    func setInterViewStatusColor() -> UIColor{
        switch self {
        case 0 :
            return #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        case 1 :
            return #colorLiteral(red: 0.2379338741, green: 0.6737315059, blue: 0.9689859748, alpha: 1)
        case 2 :
            return #colorLiteral(red: 0.1008876637, green: 0.5688499212, blue: 0.5781469941, alpha: 1)
        case 3 :
            return #colorLiteral(red: 0.03734050319, green: 0.328607738, blue: 0.5746202469, alpha: 1)
        case 4 :
            return #colorLiteral(red: 0.4757143259, green: 0.4757863283, blue: 0.4756985307, alpha: 1)
        default :
            ()
        }
        return #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
}
protocol SalesBottomViewDelegate {
//    func showSaleRecord(id:String)
//    func showPriceRecord(id: String)
    func showRecord(id:String)
}
class SalesBottomView: UIView {
    let searchcontroller = SearchTextField()
    let addbutton = AddButton4()
    let tableview = UITableView(frame: .zero, style: .plain)
    var delegate:SalesBottomViewDelegate!
    var searchcontrolleranchor:AnchoredConstraints!
    var total : Int?
    var name : String?
    var currentCustomers = [Customer]() {
        didSet{
            let offset = tableview.contentOffset;
            tableview.reloadData()
            //tableview.layoutIfNeeded() // Force layout so things are updated before resetting the contentOffset.
            tableview.setContentOffset(offset, animated: false)
           // self.tableview.reloadData()
        }
    }
    var isSale = true{
        didSet{
            //self.tableview.reloadData()
        }
    }
    
    var list = [InterviewAndCustomer]() {
        didSet{
           // list.reverse()
           // let offset = tableview.contentOffset;
            //tableview.reloadData()
            //tableview.layoutIfNeeded() // Force layout so things are updated before resetting the contentOffset.
           // tableview.setContentOffset(offset, animated: false)
        }
    }
    var startingDate : String?
    var endDate : String?
    let hud = JGProgressHUD()
    var currentPage : Int = 1
    var isLoadingList = false
    func resetFetch(){
        hud.show(in: self)
        currentPage = 1
        list = []
        
        
        fetchApi()
    }
    var filter_data = FilterClass()
    init(sales:Bool = true) {
        super.init(frame: .zero)
        isSale = sales
//        backgroundColor = .white
//        layer.cornerRadius = 15.calcvaluex()
//        if #available(iOS 11.0, *) {
//            layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        //addshadowColor(color: #colorLiteral(red: 0.9166875482, green: 0.9190561175, blue: 0.9245964885, alpha: 1))
        
//        searchcontroller.attributedPlaceholder = "搜尋客戶公司、統編".convertoSearchAttributedString()
//        searchcontroller.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
//
//        addSubview(searchcontroller)
//        searchcontrolleranchor = searchcontroller.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 262.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
//        searchcontroller.layer.cornerRadius = 38.calcvaluey()/2
//        searchcontroller.isHidden = true
//        addSubview(addbutton)
//        addbutton.titles.text = "建立客戶公司"
//        addbutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//        addbutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 206.calcvaluex(), height: 38.calcvaluey()))
//        addbutton.layer.cornerRadius = 38.calcvaluey()/2
//
        tableview.backgroundColor = .clear
        addSubview(tableview)
//        tableview.contentInset = .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0)
       // tableview.backgroundColor = .red
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.showsVerticalScrollIndicator = false
        tableview.register(SalesBottomViewCell.self, forCellReuseIdentifier: "ce")
        tableview.register(PriceManagementCell.self, forCellReuseIdentifier: "ct")
        //fetchApi()
    }
    func fetchApi(){
        var urlString = ""
        if isSale {
            urlString = "api-or/v1/accounts"
        }
        else{
            urlString = "api-or/v1/interviews"
        }
        urlString += "?pens=20"
        urlString += "&page=\(currentPage)"
        if let name = name , name != ""{
            urlString += "&name=\(name)"
        }
        if let start = startingDate {
            urlString += "&start_date=\(start)"
        }
        if let end = endDate {
            urlString += "&end_date=\(end)"
        }
       
        if let employ = filter_data.employee {
            urlString += "&owner_id=\(employ.id)"
        }
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        
        if let start = filter_data.startTime {
            urlString += "&start_date=\(dateformatter.string(from: start))"
        }
        if let end = filter_data.endTime {
            urlString += "&end_date=\(dateformatter.string(from: end))"
        }
        if filter_data.statusArray.count != 0{
            var text = "["
            for (index,i) in filter_data.statusArray.enumerated() {
                text += "\"\(i.key ?? "")\""
                if index != filter_data.statusArray.count - 1{
                    text += ","
                }
            }
            text += "]"
            urlString += "&statuses=\(text)"
        }
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        NetworkCall.shared.getCall(parameter: urlString, decoderType: InterviewAndCustomerListData.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
                self.isLoadingList = false
                self.total = json?.meta?.last_page
                if let json = json?.data {
                    for i in json {
                        self.list.append(i)
                    }
                   
                    self.tableview.reloadData()
                    self.tableview.tableFooterView = nil
                    if self.currentPage == 1 && self.list.count != 0{
                        self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                }
//                self.hud.dismiss(afterDelay: 0, animated: true) {
//                    self.showView()
//                }
//
//                self.customers = json?.data ?? []
//
//                do {
//                    let encode = try JSONEncoder().encode(json)
//                    UserDefaults.standard.setCustomersData(data: encode)
//                }
//                catch{
//
//                }
//                self.originCustomers = json?.data ?? []
//                self.createData()

            }

        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let spinner = UIActivityIndicatorView(style: .whiteLarge)
}
extension SalesBottomView:UITableViewDelegate,UITableViewDataSource {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList && currentPage < total ?? 0){
            self.isLoadingList = true

            currentPage += 1
            
            self.fetchApi()
            spinner.color = .black
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableview.bounds.width, height: 44.calcvaluey())

                        self.tableview.tableFooterView = spinner
            
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSale {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ce", for: indexPath) as! SalesBottomViewCell
            if indexPath.section < list.count {
            cell.namelabel.text = list[indexPath.section].name
                cell.numberlabel.text = "\("統編".localized)：\(list[indexPath.section].business_number ?? "")"
            }
        return cell
        }
        else{
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ct", for: indexPath) as! PriceManagementCell
            if indexPath.section < list.count {
            cell.toplabel.text = list[indexPath.section].account_name
            cell.timelabel.text = list[indexPath.section].date
            let status = GetStatus().getInterviewStatus()
            if let first = status.first(where: { (st) -> Bool in
                return st.key == list[indexPath.section].status
            })
            {
                cell.statuslabel.label.text = first.getString()
                cell.statuslabel.setColor(color: UIColor().hexStringToUIColor(hex: first.color ?? ""))
            }
            else{
                let dt = Int(list[indexPath.section].status ?? "0")
                cell.statuslabel.label.text = dt?.setInterviewStatus()
                cell.statuslabel.setColor(color: dt?.setInterViewStatusColor() ?? .white)
            }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.showRecord(id:list[indexPath.section].id)
        //tableView.deselectRow(at: indexPath, animated: false)
//        if isSale {
//            delegate.showSaleRecord(id:currentCustomers[indexPath.section].id)
//        }
//        else{
//            delegate.showPriceRecord(id:list[indexPath.section].id)
//        }
        
    }
}

class SalesBottomViewCell:UITableViewCell {
    let container = UIView()
    let namelabel = UILabel()
    let numberlabel = UILabel()
    let rightarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = .white
        container.addshadowColor()
        container.layer.cornerRadius = 15.calcvaluex()
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2.calcvaluex(), left: 2.calcvaluex(), bottom: 2.calcvaluex(), right: 2.calcvaluex()))
        
        namelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        //namelabel.text = "久大行銷股份有限公司"
        namelabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        container.addSubview(namelabel)
        //namelabel.numberOfLines = 2
        namelabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        //namelabel.centerYInSuperview()
        container.addSubview(numberlabel)
        numberlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        numberlabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        
        numberlabel.anchor(top: namelabel.bottomAnchor, leading: namelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        rightarrow.isHidden = true
        rightarrow.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(rightarrow)
        rightarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        rightarrow.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
