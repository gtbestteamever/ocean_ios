//
//  CustomerSelectionView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
protocol customerDelegate {
    func didSelect(data: Customer)
}
class CustomerSelectionView : UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    weak var con2 : AddNewServiceController?
    weak var con : InterviewController?
    weak var con3 : FilterContainerView?
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
    if let text = textField.text,
                   let textRange = Range(range, in: text) {
                   let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
            if updatedText == ""{
                filterList = customersList
            }
            else{
            filterList = customersList.filter({ (cust) -> Bool in
                return cust.name?.contains(updatedText) ?? false
            })
            }
                   
                }
       

        
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.filterList = customersList
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    var customersList = [Customer]() {
        didSet{
            self.filterList = customersList
            
        }
    }
    var filterList = [Customer]() {
        didSet{
            self.tableview.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        cell.textLabel?.text = filterList[indexPath.row].name
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    let container = UIView()
    var delegate : customerDelegate?
    var titleLabel : UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "選擇客戶".localized
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 24.calcvaluex())
        return titleLabel
    }()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()
    let searchController = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .plain)
    
    func getCustomersList(){
        do{
            if let dt = UserDefaults.standard.getCustomersData(){
                let json = try JSONDecoder().decode(Customers.self, from: dt)
                
                NetworkCall.shared.postCall(parameter: "api-or/v1/accounts/check", dict: ["check_token":json.meta?.update_token ?? ""], decoderType: UpdateClass.self) { (jt) in
                    DispatchQueue.main.async {
                        if let jt = jt{
                            if !(jt.data ?? false) {
                                self.getNewData()
                            }
                            else{
                                self.customersList = json.data ?? []
                                self.tableview.reloadData()
                            }
                        }
                        else{
                            self.customersList = json.data ?? []
                            self.tableview.reloadData()
                        }
                    }

                }
                
            }
            else{
                getNewData()
            }
            
        }
        catch{
            
        }
    }
    func getNewData(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        NetworkCall.shared.getCall(parameter: "api-or/v1/accounts?pens=100000", decoderType: Customers.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss(afterDelay: 0, animated: true)
                
                self.customersList = json?.data ?? []
                self.tableview.reloadData()
                do {
                    let encode = try JSONEncoder().encode(json)
                    UserDefaults.standard.setCustomersData(data: encode)
                }
                catch{
                    
                }


            }

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.container.backgroundColor = #colorLiteral(red: 0.9238069322, green: 0.9238069322, blue: 0.9238069322, alpha: 1)
        self.container.layer.cornerRadius = 6.calcvaluex()
        view.addSubview(self.container)
        container.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 500.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 600.calcvaluey()))
        
        
        container.addSubview(titleLabel)
        titleLabel.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0))
        titleLabel.centerXInSuperview()
        
        container.addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        xbutton.addTarget(self, action: #selector(cancelView), for: .touchUpInside)
        
        container.addSubview(searchController)
        searchController.anchor(top: titleLabel.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex()),size: .init(width: 0, height: 50.calcvaluey()))
        searchController.clearButtonMode = .unlessEditing
        searchController.delegate = self
        searchController.layer.cornerRadius = 6.calcvaluex()
        searchController.backgroundColor = #colorLiteral(red: 0.8988132669, green: 0.8988132669, blue: 0.8988132669, alpha: 1)
        searchController.placeholder = "請輸入客戶名稱".localized
        
        container.addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.anchor(top: searchController.bottomAnchor, leading: searchController.leadingAnchor, bottom: container.bottomAnchor, trailing: searchController.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.contentInset.top = 0
        getNewData()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        con2?.customer = self.filterList[indexPath.row]
        con2?.customerlabel.textfield.text = self.filterList[indexPath.row].name
        con2?.addressField.textfield.text = self.filterList[indexPath.row].address
        con2?.manual_address = self.filterList[indexPath.row].address
        con2?.edit_account_code = self.filterList[indexPath.row].code
        con?.setCustomer(customer: self.filterList[indexPath.row])
        var data = con3?.data
        if let index = data?.firstIndex(where: { (nn) -> Bool in
            return nn.mode == .CompanyCode
        }) {
            data?[index].value = self.filterList[indexPath.row]
        }
        else{
            data?.insert(maintainFilterItem(mode: .CompanyCode, value: self.filterList[indexPath.row]), at: 0)
        }
        con3?.data = data ?? []
        con3?.companyCodeField.textfield.text = self.filterList[indexPath.row].name
        self.view.endEditing(true)
        if let delegate = delegate{
            delegate.didSelect(data: self.filterList[indexPath.row])
        }
        self.dismiss(animated: false, completion: nil)
    }
    @objc func cancelView(){
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
    }
}
