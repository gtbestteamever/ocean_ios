//
//  SalesManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/19.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension String {
    func convertoSearchAttributedString() -> NSAttributedString {
        return NSAttributedString(string: self, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
    }
    
    func convertToAttributedString(first:String="* ",firstColor:UIColor = #colorLiteral(red: 0.8249840736, green: 0.1374278367, blue: 0.1237704381, alpha: 1),firstFont:UIFont? = UIFont(name: "Roboto-Regular", size: 16.calcvaluex()),textFont:UIFont? = UIFont(name: "Roboto-Regular", size: 16.calcvaluex()),textColor : UIColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)) -> NSMutableAttributedString {
        let attr = NSMutableAttributedString(string: first, attributes: [NSAttributedString.Key.foregroundColor:firstColor,NSAttributedString.Key.font:firstFont!])
        
        attr.append(NSAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor:textColor,NSAttributedString.Key.font:textFont!]))
        return attr
    }
}
class SalesManagementController: SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SalesBottomViewDelegate {
    func showRecord(id: String) {
        let vd = CustomerDetailController()
        vd.id = id
        //vd.company = json
        //vd.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vd, animated: true)
        //self.present(vd, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == slidecollectionview {
            return group.count
        }
        return currentSalesGroup.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == peoplecollectionview {
            
            
            peopleIndex = indexPath.item
            
            peoplecollectionview.reloadSections(IndexSet(arrayLiteral: 0))
            if currentSalesGroup.count > 0{
                
                self.setCompanySearch(text: bottomview.searchcontroller.text ?? "")
            }
            else{
                bottomview.currentCustomers = []
            }
        }
        else{
            selected_index = indexPath.item
            if selected_index != 0{
                self.slidein()
            }
            else{
                self.peoplecollectionviewheightanchor.constant = 0
                self.view.layoutIfNeeded()
            }
            self.peopleIndex = 0
            currentSalesGroup = group[selected_index].sales
            if currentSalesGroup.count > 0 {
                self.setCompanySearch(text: bottomview.searchcontroller.text ?? "")
            }
            else{
                bottomview.currentCustomers = []
            }
            //UIView.setAnimationsEnabled(false)
            slidecollectionview.reloadSections(IndexSet(arrayLiteral: 0))
            peoplecollectionview.reloadSections(IndexSet(arrayLiteral: 0))
            //UIView.setAnimationsEnabled(true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == peoplecollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid2", for: indexPath) as! PeopleCollectionViewCell
            cell.backgroundColor = .clear
            cell.namelabel.text = currentSalesGroup[indexPath.item].name
            if peopleIndex == indexPath.item {
                cell.didSelect()
            }
            else{
                cell.unSelect()
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! SlideCollectionViewCell
        cell.button.setTitle(group[indexPath.item].name, for: .normal)
        if selected_index == indexPath.item {
            cell.didSelect()
        }
        else{
            cell.unSelect()
        }
//        cell.button.tag = indexPath.item
       // cell.button.addTarget(self, action: #selector(slidein), for: .touchUpInside)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == peoplecollectionview {
            return 16.calcvaluex()
        }
        return 26.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == peoplecollectionview {
            return 16.calcvaluex()
        }
        return 26.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == peoplecollectionview {
            return .init(width: 134.calcvaluex(), height: collectionView.frame.height)
        }
        let groupText = group[indexPath.item].name
        var width = groupText?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) ?? 0
        width += 36.calcvaluex()
        if width < 102.calcvaluex() {
            width = 102.calcvaluex()
        }
        
        return .init(width: width, height: collectionView.frame.height)
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//
//    }
    @objc func slidein(){
        
        peoplecollectionviewheightanchor.constant = 150.calcvaluey()
        self.view.layoutIfNeeded()
        mainview?.hidecircle()
        
//        searchfieldanchor.leading?.constant = 26.calcvaluex()
        
        //self.peoplecollectionview.collectionViewLayout.invalidateLayout()
        UIView.animate(withDuration: 0.4) {
            //self.peoplecollectionview.collectionViewLayout.invalidateLayout()
            self.view.layoutIfNeeded()
        }
    }
    override func popview() {
        peoplecollectionviewheightanchor.constant = 0
        self.view.layoutIfNeeded()
        mainview?.showcircle()
       
        
        self.peoplecollectionview.collectionViewLayout.invalidateLayout()
        UIView.animate(withDuration: 0.4) {
            
            self.view.layoutIfNeeded()
        }
    }
    let searchcontroller = SearchTextField()
    var searchfieldanchor:AnchoredConstraints!
    let slidecollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let peoplecollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var filterView = MaintainFilterView()
    var filter_heightAnchor : NSLayoutConstraint?
    var peoplecollectionviewheightanchor:NSLayoutConstraint!
    var bottomview = SalesBottomView()
    let addbutton = AddButton4()
    weak var mainview:ViewController?
    //var user: UserDAttribute?
    var group = [DepartmentGroup]()
    var currentSalesGroup = [SalesCompany]()
    var customers = [Customer]()
    var originCustomers = [Customer]() {
        didSet{
            filterData = originCustomers
        }
    }
    var peopleIndex = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.getUserData()
        //self.fetchApi()
        bottomview.resetFetch()
        
    }
    var pre_user:UserD?
    func getUserData(){
        group = []
        var sales = SalesCompany(name: "", code: "", company: [], interviewList: [])
        var dep = DepartmentGroup(name: "我的", code: nil, sales: [])
        
        
        if let userdata = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()) {
            let json = try? JSONDecoder().decode(UserData.self, from: userdata)
            self.pre_user = json?.data
            //self.user = json?.data.attributes
            sales.code = self.pre_user?.username ?? ""
        }
        dep.sales.append(sales)

        group.append(dep)
        
    }
    func hideView(){
        self.slidecollectionview.isHidden = true
        self.addbutton.isHidden = true
        self.searchcontroller.isHidden = true
        self.bottomview.isHidden = true
    }
    func showView(){
        self.slidecollectionview.isHidden = false
        self.addbutton.isHidden = false
        self.searchcontroller.isHidden = false
        self.bottomview.isHidden = false
    }
    let hud = JGProgressHUD()
    func fetchApi(){
        
        self.hideView()
        
        hud.show(in: self.view)
        if let customerData = UserDefaults.standard.getCustomersData() {
            if let json = try? JSONDecoder().decode(Customers.self, from: customerData) {
                
                NetworkCall.shared.postCall(parameter: "api-or/v1/accounts/check", dict: ["check_token":json.meta?.update_token ?? ""], decoderType: UpdateClass.self) { (jt) in
                    DispatchQueue.main.async {
                        if let data = jt?.data {
                            if !data {
                                self.getNewData()

                            }
                            else{
                                self.hud.dismiss(afterDelay: 0, animated: true) {
                                    self.showView()
                                }
                                self.customers = json.data ?? []
                                
                                self.originCustomers = json.data ?? []
                                self.createData()
                            }
                        }
                        else{
                            self.getNewData()
//                            self.hud.dismiss(afterDelay: 0, animated: true) {
//                                self.showView()
//                            }
//                            
//                            self.customers = json.data ?? []
//                            self.originCustomers = json.data ?? []
//                            self.createData()
                        }
                    }

                }
            }
            
        }
        else{
            getNewData()
        }
   
    }
    
    func getNewData(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/accounts?pens=100000", decoderType: Customers.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss(afterDelay: 0, animated: true) {
                    self.showView()
                }
                
                self.customers = json?.data ?? []
               
                do {
                    let encode = try JSONEncoder().encode(json)
                    UserDefaults.standard.setCustomersData(data: encode)
                }
                catch{
                    
                }
                self.originCustomers = json?.data ?? []
                self.createData()

            }

        }
    }
    
    var filterData = [Customer]()
    func setData(){
        if selected_index < group.count {
            if peopleIndex < self.group[selected_index].sales.count {
                self.bottomview.currentCustomers = self.group[self.selected_index].sales[self.peopleIndex].company
            }
        }
        else{
            self.bottomview.currentCustomers = []
        }
    }
    func createData() {
        
        
        for i in filterData {
            if let sales = i.sales {
                if sales.code == self.pre_user?.username {
                    
                    group[0].sales[0].company.append(i)
                    continue
                }
                if let groupIndex = group.firstIndex(where: { (gp) -> Bool in
                    return gp.code == sales.department_code
                }) {
                    if let salesIndex = self.group[groupIndex].sales.firstIndex(where: { (st) -> Bool in
                        return st.code == sales.code
                    }) {
                        group[groupIndex].sales[salesIndex].company.append(i)
                    }
                    else{
                        group[groupIndex].sales.append(SalesCompany(name: sales.name, code: sales.code, company: [i], interviewList: []))
                    }
                }
                else{
                    
                    group.append(DepartmentGroup(name: sales.department_name, code: sales.department_code, sales: [SalesCompany(name: sales.name, code: sales.code, company: [i], interviewList: [])]))
                }
            }
        }
        setData()
        
        self.slidecollectionview.reloadData()
        
    }
    let stackview = UIStackView()
    var selected_index = 0
    override func changeLang() {
        super.changeLang()
    
        searchcontroller.attributedPlaceholder = "搜尋業務名稱/客戶名稱".localized.convertoSearchAttributedString()
        addbutton.titles.text = "建立客戶公司".localized
        self.bottomview.tableview.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        settingButton.isHidden = false
        titleview.label.text = "客戶管理".localized
        background_view.image = #imageLiteral(resourceName: "background_introdution")
        extrabutton_stackview.addArrangedSubview(addbutton)
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.alignment = .fill
        stackview.spacing = 6.calcvaluex()
        //stackview.safelyRemoveArrangedSubviews()
        stackview.addArrangedSubview(searchcontroller)
        //stackview.addArrangedSubview(addbutton)
        
        
        
        searchcontroller.attributedPlaceholder = "搜尋業務名稱/客戶名稱".localized.convertoSearchAttributedString()
        searchcontroller.text = ""
        searchcontroller.addshadowColor(color: #colorLiteral(red: 0.9166875482, green: 0.9190561175, blue: 0.9245964885, alpha: 1))
        searchcontroller.layer.cornerRadius = 46.calcvaluey()/2
        //view.addSubview(searchcontroller)
        searchcontroller.backgroundColor = .white
        searchcontroller.constrainHeight(constant: 46.calcvaluey())
       // addbutton.constrainWidth(constant: 206.calcvaluex())
        //searchfieldanchor = searchcontroller.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 333.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        addbutton.titles.text = "建立客戶公司".localized
                addbutton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        //addbutton.layer.cornerRadius = 46.calcvaluey()/2
        

        addbutton.addTarget(self, action: #selector(goAddCustomer), for: .touchUpInside)
        //addbutton.constrainHeight(constant: 38.calcvaluey())
        
        view.addSubview(stackview)
        stackview.anchor(top: topview.bottomAnchor, leading: titleview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width:481.calcvaluex(), height: 46.calcvaluey()))
       // searchcontroller.trailingAnchor.constraint(equalTo: stackview.leadingAnchor,constant: -12.calcvaluex()).isActive = true
        
 
        searchcontroller.delegate = self
//        view.addSubview(slidecollectionview)
//        (slidecollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
//        slidecollectionview.showsHorizontalScrollIndicator = false
//        slidecollectionview.backgroundColor = .clear
//        slidecollectionview.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 78.calcvaluey()))
//        slidecollectionview.delegate = self
//        slidecollectionview.dataSource = self
//        slidecollectionview.register(SlideCollectionViewCell.self, forCellWithReuseIdentifier: "cellid")
//        
//        
//        view.addSubview(peoplecollectionview)
//        peoplecollectionview.backgroundColor = .clear
//        //peoplecollectionview.isHidden = true
//        (peoplecollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
//        peoplecollectionview.showsHorizontalScrollIndicator = false
//        peoplecollectionview.anchor(top: slidecollectionview.bottomAnchor, leading: slidecollectionview.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
//        peoplecollectionviewheightanchor = peoplecollectionview.heightAnchor.constraint(equalToConstant: 0)
//        peoplecollectionviewheightanchor.isActive = true
//        peoplecollectionview.register(PeopleCollectionViewCell.self, forCellWithReuseIdentifier: "cellid2")
//        peoplecollectionview.delegate = self
//        peoplecollectionview.dataSource = self
//        peoplecollectionview.contentInset = .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex())
        view.addSubview(filterView)
        filterView.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        filter_heightAnchor = filterView.heightAnchor.constraint(equalToConstant: 0)
        filter_heightAnchor?.isActive = true
        view.addSubview(bottomview)
        bottomview.anchor(top: filterView.bottomAnchor, leading: stackview.leadingAnchor, bottom: view.bottomAnchor, trailing: stackview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        bottomview.delegate = self
        //bottomview.addbutton.addTarget(self, action: #selector(goAddCustomer), for: .touchUpInside)
        bottomview.searchcontroller.delegate = self
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(stopSearch))
        tapgesture.delegate = self
        view.addGestureRecognizer(tapgesture)
       
        
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //addbutton.isHidden = false
        //addbutton.constrainWidth(constant: 206.calcvaluex())
    }
    @objc func goAddCustomer(){
        let vd = AddNewCustomerController()
        vd.modalPresentationStyle = .fullScreen
        
        self.present(vd, animated: true, completion: nil)
        
    }
    @objc func stopSearch(){
        self.view.endEditing(true)
    }
    
}
extension SalesManagementController : UIGestureRecognizerDelegate,UITextFieldDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let vi = touch.view {
            if vi.isDescendant(of: slidecollectionview) || vi.isDescendant(of: peoplecollectionview) || vi.isDescendant(of: bottomview) {
                return false
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            bottomview.name = text
            bottomview.resetFetch()
//            if text == "" {
//                self.selected_index = 0
//                self.peopleIndex = 0
//                self.filterData = self.originCustomers
//
//                self.getUserData()
//                self.createData()
//                self.selected_index = 0
//                self.peoplecollectionviewheightanchor.constant = 0
//                self.view.layoutIfNeeded()
//            }
//            else{
//            self.filterData = self.originCustomers.filter({ (cus) -> Bool in
//                return (cus.business_number?.contains(text) ?? false) || cus.name.contains(text) || (cus.sales?.name.contains(text) ?? false) || (cus.sales?.department_name.contains(text) ?? false)
//            })
//                self.getUserData()
//                self.createData()
//                self.selected_index = 0
//                if group[0].sales[0].company.count == 0{
//                    self.group.remove(at: 0)
//                    self.selected_index = 0
//                    self.peopleIndex = 0
//                }
//                if group.count > 0{
//                    self.currentSalesGroup = group[selected_index].sales
//                    print(self.selected_index,self.peopleIndex)
//                    self.bottomview.currentCustomers = self.currentSalesGroup[self.peopleIndex].company
//                    print(self.bottomview.list)
//                    if group[selected_index].name != "我的" {
//                    peoplecollectionviewheightanchor.constant = 150.calcvaluey()
//                    self.view.layoutIfNeeded()
//                    }
//                }
//                else{
//                    peoplecollectionviewheightanchor.constant = 0
//                    self.view.layoutIfNeeded()
//                }
//            }
//
//
//            self.peoplecollectionview.reloadData()
//            self.slidecollectionview.reloadData()
//
//
////            else{
////
////                if textField == bottomview.searchcontroller {
////                    self.setCompanySearch(text: text)
////                }
////
////            }
        }
    }
    func setCompanySearch(text:String) {
        
        let originData = self.currentSalesGroup[self.peopleIndex].company
        if text == "" {
            self.bottomview.currentCustomers = originData
        }
        else{
        self.bottomview.currentCustomers = originData.filter({ (cus) -> Bool in
            return (cus.name?.contains(text) ?? false) || (cus.business_number?.contains(text) ?? false)
        })
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
class SlideCollectionViewCell:UICollectionViewCell {
    let button = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        button.backgroundColor = .white
        button.addshadowColor(color: #colorLiteral(red: 0.9160708785, green: 0.9184377789, blue: 0.9239745736, alpha: 1))
        button.setTitle("我的", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(button)
        button.isUserInteractionEnabled = false
        button.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 38.calcvaluey()))
        
        button.layer.cornerRadius = 38.calcvaluey()/2
    }
    func didSelect(){
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    func unSelect(){
        button.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        button.backgroundColor = .white

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PeopleCollectionViewCell: UICollectionViewCell {
    let cv = UIView()
    let userimageview = UIImageView(image: #imageLiteral(resourceName: "ic_user_normal"))
    let namelabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        cv.backgroundColor = .white
        cv.addshadowColor(color: #colorLiteral(red: 0.9489133954, green: 0.9490793347, blue: 0.948915422, alpha: 1))
        cv.layer.cornerRadius = 15.calcvaluex()
       addSubview(cv)
        cv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2, left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 134.calcvaluey()))
        
        cv.addSubview(userimageview)
        userimageview.anchor(top: cv.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 29.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 48.calcvaluex(), height: 48.calcvaluey()))
        userimageview.centerXInSuperview()
        
        namelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        namelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        //namelabel.text = "吳漣序"
        
        cv.addSubview(namelabel)
        namelabel.anchor(top: userimageview.bottomAnchor, leading: nil, bottom: nil, trailing: nil)
        namelabel.centerXInSuperview()
    }
    func didSelect(){
        namelabel.textColor = .white
        userimageview.image = #imageLiteral(resourceName: "ic_user_pressed")
        cv.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    
    func unSelect(){
        namelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        userimageview.image = #imageLiteral(resourceName: "ic_user_normal")
        cv.backgroundColor = .white
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
