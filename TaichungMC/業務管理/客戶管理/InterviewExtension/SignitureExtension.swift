//
//  SignitureExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

extension InterviewController: SignitureProtocol {
    func goSigniture(index:Int) {
        
        let sig = SignitureController()
        sig.saveIndex = index
        sig.con = self
        sig.modalPresentationStyle = .fullScreen
        self.present(sig, animated: true, completion: nil)
    }
    func removeSigniture(index: Int) {
        if signitureArray[index].id != "" {
            deleteFileArray.append(signitureArray[index].id)
        }
        signitureArray[index] = interViewFile(id: "", tag: ["sign"], path: "", path_url: "", image: nil)
        
        removeDataFromFolder(name: SignitureMode.Customer.rawValue)
        

        reloadSectionWithIndex(index: 2)
    }
    
}
