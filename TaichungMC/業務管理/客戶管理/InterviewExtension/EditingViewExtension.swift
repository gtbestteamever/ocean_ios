//
//  EditingViewExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
extension InterviewController: EditingInterviewDelegate{
    func chooseAddressType(textview: UITextView) {
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        modeselection = CustomizeModeSelectionController()
        modeselection?.isAddressType = true
//        modeselection?.tag = subIndex
//        modeselection?.firstIndex = index
        modeselection?.delegate = self
        self.rightview.addSubview(modeselection!)
        modeselection?.tableView.reloadData()
        modeselection?.tableView.layoutIfNeeded()
        modeselection?.anchor(top: textview.bottomAnchor, leading: textview.leadingAnchor, bottom: nil, trailing: textview.trailingAnchor,size: .init(width: 0, height: 210.calcvaluey()))
    }
    
    func setCustomerCode(text: String) {
        interInfo.code = text
        reloadSectionWithIndex(index: 0)
    }
    func setDeliveryDate(text: String) {
        interInfo.delivery_date = text
        reloadSectionWithIndex(index: 0)
    }
    func showAttachFile(file: interViewFile) {
        
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

    
        vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        if file.path_url != "" {
        guard let url = URL(string: file.path_url) else {return}
            url.loadWebview(webview: vd.imgv)
//            if url.pathExtension == "jpg" || url.pathExtension == "png" || url.pathExtension == "jpeg" {
//                vd.imagview.isHidden = false
//                vd.imagview.sd_setImage(with: url, completed: nil)
//            }
//            else{
//            vd.imgv.loadRequest(URLRequest(url: url))
//            }
        }
        else{
            if let fileUrl = file.fileUrl {
                guard let url = URL(string: fileUrl) else {return}
                url.loadWebview(webview: vd.imgv)
                //vd.imgv.loadRequest(URLRequest(url: url))
            }
            else{
            if let imgv = file.image {
                vd.imagview.isHidden = false
                vd.imagview.image = imgv

            }
            }
            
        }
        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
    }
    func sendPeriodArray(array: [String]) {
        self.interInfo.periods = array
        reloadSectionWithIndex(index: 0)
    }
    
    
    func removeFile(index: Int) {
        let dt = interInfo.files.remove(at: index)
        removeDataFromFolder(name: dt.path)
        if dt.id != "" {
            deleteFileArray.append(dt.id)
        }
        self.createEditInterviewCell()
        rightview.beginUpdates()
        rightview.reloadData()
        rightview.endUpdates()
    }
    func setOrderDate(text: String) {
        print(text)
        interInfo.orderDate = text
        reloadSectionWithIndex(index: 0)
    }
    func setInfo(text: String) {
        
        interInfo.moreInfo = text
        reloadSectionWithIndex(index: 0)
    }
    func goSelectFile(sender:UIButton) {
        if interInfo.files.count == 6 {
            let alertController = UIAlertController(title: "一次最多只能上傳6個檔案".localized, message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
            
            return
        }
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = sender
                presenter.sourceRect = sender.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
    }
    func setVisitDate(text: String) {
        
        self.interInfo.visitDate = text
        self.reloadSectionWithIndex(index: 0)
    }
    
    func sendLocation(text: String) {
        self.interInfo.location = text
        self.reloadSectionWithIndex(index: 0)
    }
    func sendRating(text: String) {
        if let rating = Int(text) {
            
            if rating > 0 && rating < 6{
                self.interInfo.rating = rating
                
            }
            else{
                self.interInfo.rating = nil
                let alert = UIAlertController(title: "成交率請輸入1~5", message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            self.interInfo.rating = nil
            let alert = UIAlertController(title: "成交率請輸入1~5", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.reloadSectionWithIndex(index: 0)
    }
    
    
    func chooseNotifyDate(textField: UITextField) {
        print(123,textField)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        modeselection = CustomizeModeSelectionController()
        modeselection?.isNotify = true
//        modeselection?.tag = subIndex
//        modeselection?.firstIndex = index
        modeselection?.delegate = self
        self.rightview.addSubview(modeselection!)
        var height = 210.calcvaluey()
        modeselection?.anchor(top: textField.bottomAnchor, leading: textField.leadingAnchor, bottom: nil, trailing: textField.trailingAnchor,size: .init(width: 0, height: height))
    }
}
