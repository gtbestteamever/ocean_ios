//
//  CustomizeModeExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
extension InterviewController:CustomizeModeDelegate {
    func setAddressType(mode: AddressType) {
        interInfo.addressType = mode
        if mode == .Ocean {
            interInfo.location = "台中市霧峰區中正路1-1號"
        }
        else if mode == .Customer {
            interInfo.location = interInfo.customer?.address ?? ""
        }
        else {
            interInfo.location = ""
        }
        
        reloadSectionWithIndex(index: 0,reloadAll: true)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
    }
    
    func didSelect(mode: AddedMode, index: Int,subIndex: Int,thirdIndex:Int) {
        addedMachine[index].customOptions[subIndex].options[thirdIndex].mode = mode
        reloadSectionWithIndex(index: 1)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
    }
    func setCustomer(customer: Customer) {
        
        interInfo.customer = customer
        if interInfo.addressType == .Customer {
            interInfo.location = customer.address ?? ""
        }
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        //interInfo.name = text
        reloadSectionWithIndex(index: 0,reloadAll: true)
    }
    func setCustomStatus(index: Int, subIndex: Int, status: Int) {
        print(status)
        addedMachine[index].customOptions[subIndex].status = status
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        self.reloadSectionWithIndex(index: 1)
    }
    func setNotifyDate(notifyMode: NotifyMode) {
        self.interInfo.notifyDate = notifyMode
        self.reloadSectionWithIndex(index: 0)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
    }
}
