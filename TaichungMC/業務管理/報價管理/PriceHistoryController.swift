//
//  PriceHistoryController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
class HistoryTotalView : UIView {
    let pricelabel = UILabel()
    let pricetext = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        pricelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        pricelabel.text = "總報價金額："
        pricelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(pricelabel)
        pricelabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        pricelabel.centerYInSuperview()
        
        pricetext.text = "$22,869 USD"
        pricetext.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        pricetext.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(pricetext)
        pricetext.anchor(top: nil, leading: pricelabel.trailingAnchor, bottom: nil, trailing: nil)
        pricetext.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class HistoryCell: UITableViewCell {
    let container = UIView()
    let selectionview = UIView()
    let titlelabel = UILabel()
    let datelabel = UILabel()
    func setColor(){
        imageview.isHidden = false
        //selectionview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    func unsetColor(){
        imageview.isHidden = true
        //selectionview.backgroundColor = .white
    }
    let ownerlabel : UILabel = {
       let oL = UILabel()
        oL.textColor = .black
        
        oL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        return oL
    }()
    var statusLabel = newStatusLabel()
    var statusLabelAnchor : AnchoredConstraints?
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        datelabel.numberOfLines = 2
        backgroundColor = .clear
        selectionview.backgroundColor = .white
        contentView.addSubview(selectionview)
        selectionview.layer.cornerRadius = 15.calcvaluex()
        selectionview.addshadowColor()
        selectionview.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        //selectionview.addshadowColor(color: #colorLiteral(red: 0.8631278276, green: 0.8653584719, blue: 0.8705746531, alpha: 1))
        
//        container.backgroundColor = .white
//        container.layer.cornerRadius = 15.calcvaluex()
//        if #available(iOS 11.0, *) {
//            container.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
//        selectionview.addSubview(container)
//
//        container.fillSuperview(padding: .init(top: 0, left: 11.calcvaluex(), bottom: 0, right: 0))
        selectionview.addSubview(ownerlabel)
        ownerlabel.numberOfLines = 2
        ownerlabel.anchor(top: selectionview.topAnchor, leading: selectionview.leadingAnchor, bottom: nil, trailing: selectionview.trailingAnchor,padding: .init(top: 19.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 8.calcvaluex()))
        
        
        titlelabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        titlelabel.text = "編輯時間".localized
        titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)

        selectionview.addSubview(statusLabel)

        statusLabelAnchor = statusLabel.anchor(top: ownerlabel.bottomAnchor, leading: ownerlabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
       // statusLabel.backgroundColor = .red
       // statusLabel.textColor = .red
       
       // statusLabel.layer.cornerRadius = 4.calcvaluex()
       // statusLabel.layer.borderWidth = 1.calcvaluex()
       // statusLabel.layer.borderColor = UIColor.red.cgColor
        
        
        
        selectionview.addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: statusLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 9.calcvaluex().calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYAnchor.constraint(equalTo: statusLabel.centerYAnchor).isActive = true
        datelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //datelabel.text = "10月十日下午4:47"
        datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        selectionview.addSubview(datelabel)
        datelabel.anchor(top: statusLabel.bottomAnchor, leading: statusLabel.leadingAnchor, bottom: selectionview.bottomAnchor, trailing: nil,padding: .init(top: 12.calcvaluey(), left: 4.calcvaluex(), bottom: 20.calcvaluey(), right: 0))
        
        selectionview.addSubview(imageview)
        imageview.anchor(top: nil, leading: nil, bottom: nil, trailing: selectionview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 8.calcvaluex()),size: .init(width: 17.calcvaluex(), height: 17.calcvaluex()))
        imageview.centerYInSuperview()
        imageview.isHidden = true
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//         layoutIfNeeded()
//        container.roundCorners(corners: [.topRight,.bottomRight], radius: 15.calcvaluex())
//
////        selectionview.addShadow(corners: [.topLeft,.bottomLeft], radius: 15.calcvaluex())
//
//    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class PriceHistoryController: SampleController,UITableViewDelegate,UITableViewDataSource {
    
    var id : String?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryCell(style: .default, reuseIdentifier: "cx")
        let dt = Date().convertToDateComponent(text: data[indexPath.section].updated_at ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        
        cell.ownerlabel.text = data[indexPath.section].editor?.name
        let st = dt.split(separator: " ")
        let first = String(st.first ?? "")
        var second = ""
        if UserDefaults.standard.getLanguage() == "en" {
            second = String((st[1] ) + " " + (st[2] ))
        }
        else{
            second = String(st.last ?? "")
        }
        
        cell.datelabel.text = "\(first)\n\(second)"
        cell.selectionStyle = .none
        
        let status = GetStatus().getInterviewStatus()
        if let first = status.first(where: { (st) -> Bool in
            switch data[indexPath.section].status {
            case .int(let y) :
                return false
            case .string(let x) :
                return x == st.key
            }
            
        }) {
            cell.statusLabel.setColor(color: UIColor().hexStringToUIColor(hex: first.color ?? ""))

            
            cell.statusLabel.label.text = first.getString()
            cell.statusLabelAnchor?.width?.constant = (first.getString()?.widthOfString(usingFont: UIFont(name: "Roboto-Bold", size: 16.calcvaluex())!) ?? 0) + 8.calcvaluex()
        }
        else{
            switch data[indexPath.section].status {
            case .int(let x) :
                cell.statusLabel.setColor(color: x.setInterViewStatusColor())
                

                cell.statusLabel.label.text = x.setInterviewStatus()
            case .string(let s):
                if let x = Int(s) {
                    cell.statusLabel.setColor(color: x.setInterViewStatusColor())

                    cell.statusLabel.label.text = x.setInterviewStatus()
                }
            
            }
        }
        if indexPath.section == self.current {
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    let bt = TopViewButton(text: "修改".localized)
    let bs = TopViewButton(text: "送出訂單".localized)
    let vt = TopViewButton(text: "結案申請".localized)
    let st = TopViewButton(text: "結案".localized)
    let previewButton = TopViewButton(text: "預覽".localized)
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 140.calcvaluey()
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != current {
//            if let curr = self.current {
//        let prevcell = tableView.cellForRow(at: curr) as! HistoryCell
//        prevcell.selectionview.backgroundColor = .white
//            }
//        let cell = tableView.cellForRow(at: indexPath) as! HistoryCell
//
//        cell.selectionview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
//        self.current = indexPath
  

            self.current = indexPath.section
            print(100,data[indexPath.section].call_id ?? "")
            self.tableview.reloadData()
            if indexPath.section == 0{
                fetchWithId(id: data[indexPath.section].call_id ?? "",callfirst:true)
            }
            else{
                fetchWithId(id: data[indexPath.section].call_id ?? "",callfirst:false)
            }
            
        }
    }
     let tableview = UITableView(frame: .zero, style: .plain)
    var current = 0
     let rightview = InterviewController()
    let historybottomview = HistoryTotalView()
    let topstackview = UIStackView()
   
    let container = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        titleview.label.text = "歷史紀錄".localized

        //container.backgroundColor = .red

        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        addChild(rightview)
        rightview.didMove(toParent: self)
        rightview.mode = .Review
        //container.insertSubview(rightview.view, belowSubview: tableview)
        view.insertSubview(rightview.view, belowSubview: topview)
        rightview.topview.isHidden = true
        rightview.slideinview.isHidden = true
//        rightview.topview.isHidden = true
//        rightview.historycon = self
//        rightview.backbutton.isHidden = true
//        rightview.titleview.isHidden = true
//        rightview.contentView.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
//        //rightview.topviewanchor?.height?.constant = 0
//        //rightview.stackviewanchor.width?.constant = 0
       rightview.stackview.isHidden = true
        rightview.view.backgroundColor = .clear
        rightview.contentView.backgroundColor = .clear
//        rightview.separatorStyle = .none
//        rightview.contentInset.bottom += 80.calcvaluey()
        rightview.view.fillSuperview(padding: .init(top: 0, left: 48.calcvaluex(), bottom: 0, right: 0))
        view.addSubview(tableview)
        tableview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 21.calcvaluex(), bottom: 0, right: 0),size: .init(width: 248.calcvaluex(), height: 0))
        
//        view.addSubview(historybottomview)
//        historybottomview.anchor(top: nil, leading: rightview.leadingAnchor, bottom: view.bottomAnchor, trailing: rightview.trailingAnchor,size: .init(width: 0, height: 80.calcvaluey()))
        bt.layer.cornerRadius = 38.calcvaluey()/2
        topview.addSubview(topstackview)
        topstackview.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 10.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        topstackview.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        topstackview.axis = .horizontal
        topstackview.spacing = 10.calcvaluex()
        topstackview.addArrangedSubview(bt)

        previewButton.addTarget(self, action: #selector(goPreview), for: .touchUpInside)
       
        topstackview.addArrangedSubview(previewButton)

        topstackview.addArrangedSubview(bs)
        bs.tag = 1
        bs.addTarget(self, action: #selector(sendOutData), for: .touchUpInside)
        bs.isHidden = true
        vt.tag = 2
        vt.addTarget(self, action: #selector(sendOutData), for: .touchUpInside)
        vt.isHidden = true
        
        
        topstackview.addArrangedSubview(vt)
        
        st.tag = 3
        st.addTarget(self, action: #selector(sendOutData), for: .touchUpInside)
        st.isHidden = true
        topstackview.addArrangedSubview(st)
        
        bt.addTarget(self, action: #selector(goEdit), for: .touchUpInside)
        bt.isHidden = true
        
    }
    @objc func goPreview(){
        let vd = InterviewPreviewController()
        var array = [InterviewPreviewFile]()
        for i in interviewInfo?.products ?? [] {
            for j in i.export_files ?? [] {
                array.append(j)
            }
        }
        vd.data = array
        vd.modalPresentationStyle = .overCurrentContext
        
        self.present(vd, animated: false, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    @objc func goEdit(){
        let interviewController = InterviewController()
        interviewController.editMode = true
        interviewController.mode = .Editing
       
        interviewController.id = self.id ?? ""
        interviewController.modalPresentationStyle = .fullScreen
        
        self.present(interviewController, animated: true, completion: nil)
    }
    var data = [InterViewRecord]()
    let hud = JGProgressHUD()
    func fetchApi(){
       
        hud.show(in: self.container)
        self.tableview.isHidden = true
        self.rightview.view.isHidden = true
        //self.rightview.topstackview
        NetworkCall.shared.getCall(parameter: "api-or/v1/archive_interviews/\(id ?? "")", decoderType: InterViewRecordList.self) { (json) in
           // DispatchQueue.main.async {
            
                if let json = json {
                    self.data = json.data
                    for (index,i) in self.data.enumerated() {
                        self.data[index].call_id = "\(i.id)"
                    }
                    self.fetchSecondApi()
                }
            //}

        }
//        NetworkCall.shared.getCall(parameter: "api-or/v1/interview/\(id ?? "")", decoderType: InterViewInfos.self) { (json) in
//            DispatchQueue.main.async {
//                hud.dismiss()
//                if let json = json?.data {
//                    self.data = [json]
//                    if let first = self.data.first {
//                        self.setData(json:first)
//                    }
//
//                    self.tableview.reloadData()
//                }
//            }
//
//        }
    }
    var interviewInfo : InterViewInfo? {
        didSet{
            rightview.id = interviewInfo?.id.stringValue ?? ""
        }
    }
    @objc func sendOutData(sender:UIButton){
        if sender.tag == 1 {
        if let info = self.interviewInfo {
            let val = checkIsValidSend(info: info)
            if !val.0 {
                var msg = ""
                if val.1 == 0 {
                    msg = "請添加任一機台".localized
                }
                else if val.1 == 1 {
                    msg = "您有合審尚未完成，無法進行轉訂單，如不需合審請將狀態轉為取消，再次送出轉訂單。".localized
                }
                else{
                    msg = "請填寫交貨日期".localized
                }
                
                let alert = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "確定".localized, style:  .cancel , handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
        }
        else{
            return
        }
        }
        
        self.rightview.removeDirectory()
        self.rightview.createDirectory()
        var msg = ""
        var status : InterviewStatusEnum = .string("")
        
        if sender.tag == 1{
            msg = "送出訂單後不可在編輯，確定是否送出？".localized
            status = .string("order")
        }
        else if sender.tag == 2{
            msg = "是否要申請結案?".localized
            status = .string("waiting")
            
        }
        else if sender.tag == 3{
            msg = "是否要結案?".localized
            status = .string("closed")
        }
        let controller = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
            self.rightview.interInfo.status = status
            self.rightview.noNeedDismiss = true
            self.rightview.saveData()
            

        }
        let action2 = UIAlertAction(title: "取消".localized, style: .cancel, handler: nil)
        
        controller.addAction(action)
        controller.addAction(action2)
        self.present(controller, animated: true, completion: nil)

        
    }
    func addSendButton(){
//        if topstackview.arrangedSubviews.count != 2{
//            bs.layer.cornerRadius = 38.calcvaluey()/2
//        topstackview.addArrangedSubview(bs)
//            bs.addTarget(self, action: #selector(sendOutData), for: .touchUpInside)
//        }
        print(21212)
        bs.isHidden = false
        vt.isHidden = false

    }
    func removeSendButton(){
        print("isRemove")
        bs.isHidden = true
        vt.isHidden = true
    }
    func checkIsValidSend(info:InterViewInfo) -> (Bool,Int){
        if info.products.count == 0{
            return (false,0)
        }
       
        if info.delivery_date == nil || info.delivery_date == ""{
            return (false,2)
        }
        var count = 0
        var com_count = 1
        for i in info.products {
            
            if i.audit?.count != 0 && i.audit != nil{
                count = 1
            }
            
            for j in i.audit ?? [] {
                print(3315,j.status)
                if j.status != 5 && j.status != 6{
                    com_count = 0
                }
            }
        }
        print(3316,com_count,count)
        if count == 0 {
            return (true,1)
        }
        
        if com_count == 0{
            return (false,1)
        }

        

        return (true,0)
    }
    func fetchSecondApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/interview/\(id ?? "")", decoderType: InterViewInfos.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
                self.tableview.isHidden = false
                
                if let json = json?.data {
                    switch json.status {
                    case .int(let x) :
                        print(889,x)
                        if x == 1 || x == 2{
                            
                            self.addSendButton()
                        }
                        else{
                            self.removeSendButton()
                        }
                        if x == 3 || x == 4{
                            self.bt.isHidden = true
                        }
                        else{
                            self.bt.isHidden = false
                        }
                    case .string(let y) :
                        if y == "expand" || y == "negotiate" {
                            self.addSendButton()
                        }
                        else{
                            self.removeSendButton()
                        }
                        
                        if y == "order" || y == "closed" || y == "waiting"{
                            self.bt.isHidden = true
                            if y == "waiting" && json.owner.isManager(){
                                self.st.isHidden = false
                            }
                            else{
                                self.st.isHidden = true
                            }
                        }
                        else{
                            self.st.isHidden = true
                            self.bt.isHidden = false
                        }
                    }
//                    self.data = json.data
                    
//                    if json.status == 0 || json.status == 1 || json.status == 2{
//                        self.addSendButton()
//                    }
//                    else{
//                        self.removeSendButton()
//                    }
//                    if self.checkIsValidSend(info: json) {
//                        self.addSendButton()
//                    }
//                    else{
//                        self.removeSendButton()
//                    }

//                    self.fetchSecondApi()
                    self.interviewInfo = json
                    if let info = self.interviewInfo {
                        self.setData(json: info)
                    }
                    
                    self.rightview.view.isHidden = false
                    self.data.insert(InterViewRecord(id: 0, call_id: json.id.stringValue, source_id: "", status: json.status, date: json.date, rating: 0, order_date: "",updated_at: json.updated_at ?? "", account: nil,editor: json.editor), at: 0)
                    
                    self.tableview.reloadData()
                }
            }

        }
    }
    func fetchWithId(id:String,callfirst:Bool) {
        //hud.show(in: self.view)
        //self.rightview.view.isHidden = true

        rightview.isHistory = !callfirst
        rightview.id = id
//        NetworkCall.shared.getCall(parameter: str, decoderType: InterViewInfos.self) { (json) in
//            DispatchQueue.main.async {
//                self.hud.dismiss()
//
//
//                if let json = json?.data {
//                    self.rightview.view.isHidden = false
//                    self.interviewInfo = json
//
//                }
//            }
//
//        }
    }
    func setData(json: InterViewInfo) {
        
//        rightview.interInfo.customer = json.account
//        rightview.interInfo.location = json.address ?? ""
//        rightview.interInfo.visitDate = json.date ?? ""
//        rightview.interInfo.orderDate = json.order_date ?? ""
        var count = 1
        rightview.addedMachine = self.creatWithProduct(products:json.products, count: &count)
//        if count == 0{
//            self.removeSendButton()
//        }
//        rightview.signitureArray[0].image = UIImage()
//        rightview.signitureArray[1].image = UIImage()
//        self.rightview.calculateTotal()
//        self.rightview.rightview.reloadData()
//        self.rightview.view.isHidden = false
    }
    
    func creatWithProduct(products:[Product],count: inout Int) -> [AddedProduct] {
        var finalData : [AddedProduct] = []
        for i in products {
            var options = [SelectedOption]()
            for b in i.options ?? [] {
                for j in b.children ?? [] {
                    for m in j.children ?? [] {
                        if m.selected == true {
                            options.append(SelectedOption(name: "\(j.title) / \(m.title)", option: m,qty: i.qty ?? 1))
                        }
                    }
                }
            }
            //要加入
            var auditArray : [AddedOption] = []
            if let current_audit = i.audit?.first {
                for i in current_audit.content {
                    auditArray.append(AddedOption(mode: AddedMode(rawValue: i.type) ?? .Date, text: i.content ?? "", amount: i.qty ?? 0, price: i.price ?? "", reply: i.reply ?? "", id: i.id ?? ""))
                }
                if current_audit.status != 6 && current_audit.status != 5{
                    count = 0
                }
            }
            
            
            //finalData.append(AddedProduct(series: nil, sub_series: nil, product: i, options: options, customOptions: auditArray))
            
        }
        
        return finalData
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//                tableview.selectRow(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .none)
//                tableView(tableview, didSelectRowAt: IndexPath(item: 0, section: 0))
    }
}
