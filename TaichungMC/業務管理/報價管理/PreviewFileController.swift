//
//  PreviewFileController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
extension Data {

    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    func dataToFile(fileName: String) -> NSURL? {

        // Make a constant from the data
        let data = self

        // Make the file path (with the filename) where the file will be loacated after it is created
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: filePath) {
            return NSURL(fileURLWithPath: filePath)
        }
        do {
            // Write the file from data into the filepath (if there will be an error, the code jumps to the catch block below)
            try data.write(to: URL(fileURLWithPath: filePath))

            // Returns the URL where the new file is located in NSURL
            return NSURL(fileURLWithPath: filePath)

        } catch {
            // Prints the localized description of the error from the do block
            print("Error writing the file: \(error.localizedDescription)")
        }

        // Returns nil if there was an error in the do-catch -block
        return nil

    }

}
class PreviewFileController: SampleController {
    let webview = WKWebView()
    var data : InterviewPreviewFile? {
        didSet{
            if let url = URL(string: (data?.pdf ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") {
                print(112,url.absoluteString)
                url.loadWebview(webview: webview)
            }
        }
    }
    let shareButton = TopViewButton(text: "分享Excel")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleview.label.text = "訪談分享"
        
        view.addSubview(webview)
        webview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 12.calcvaluex(), bottom: 12.calcvaluey(), right: 12.calcvaluex()))
        
        topview.addSubview(shareButton)
        shareButton.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 12.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        shareButton.addTarget(self, action: #selector(goSharing), for: .touchUpInside)
    }

    @objc func goSharing(){
       
        guard let url = URL(string: (data?.excel ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" ) else {return}
        let dt = url.lastPathComponent
        let ss = try? Data(contentsOf: url)
        let tt = ss?.dataToFile(fileName: dt)
        if let sm = tt {
        let activityViewController = UIActivityViewController(activityItems: [sm], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.shareButton
        //activityViewController.popoverPresentationController?.sourceRect = self.shareButton.frame
        activityViewController.popoverPresentationController?.permittedArrowDirections = .up
        // Show the share-view
        self.present(activityViewController, animated: true, completion: nil)
        }
    }
}
