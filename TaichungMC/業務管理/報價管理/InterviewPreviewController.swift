//
//  InterviewPreviewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/10/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class InterviewPreviewController : UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].pdf
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        cell.textLabel?.textAlignment = .center
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vd = PreviewFileController()
        vd.modalPresentationStyle = .fullScreen
        vd.data = data[indexPath.row]
        
        self.present(vd, animated: true, completion: nil)
    }
    let container = SampleContainerView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var data = [InterviewPreviewFile]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(container)
        
        container.label.text = "預覽檔案".localized
        container.centerInSuperview(size: .init(width: 500.calcvaluex(), height: 600.calcvaluey()))
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
        container.addSubview(tableview)
        tableview.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 14.calcvaluex(), bottom: 14.calcvaluey(), right: 14.calcvaluex()))
        tableview.backgroundColor = .clear
        tableview.dataSource = self
        tableview.delegate = self
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
