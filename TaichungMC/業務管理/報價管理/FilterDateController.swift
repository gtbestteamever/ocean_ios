//
//  FilterDateController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD

class filterStatusItemView : UIView {
    var status : Status?
    let imageView : UIImageView = .init(image: #imageLiteral(resourceName: "btn_check_box_normal"))
    let label = UILabel()
    var isSelected = false {
        didSet{
            if isSelected {
                imageView.image = #imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate)
            }
            else{
                imageView.image = #imageLiteral(resourceName: "btn_check_box_normal")
            }
        }

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        imageView.centerYInSuperview()
        imageView.tintColor = MajorColor().oceanSubColor
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1485092471, green: 0.1485092471, blue: 0.1485092471, alpha: 1)
        
        addSubview(label)
        label.anchor(top: imageView.topAnchor, leading: imageView.trailingAnchor, bottom: imageView.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0 ))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol filterStatusDelegate {
    func setFilterStatus(view:filterStatusItemView)
}
class filterStatusView : UIView {
    let label = UILabel()
    let scrollview = UIScrollView()
    let h_stack = Horizontal_Stackview(spacing:12.calcvaluex())
    var delegate: filterStatusDelegate?
    
    var preSelectedStatus : [Status] = [] {
        didSet{
            for i in h_stack.arrangedSubviews {
                if let vv = i as? filterStatusItemView {
                    vv.isSelected = preSelectedStatus.contains(where: { sm in
                        return sm.key == vv.status?.key
                    })
                }
            }
        }
    }
    init(isInterview:Bool = true) {
        super.init(frame: .zero)
        label.text = "訪談狀態".localized
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        
        addSubview(scrollview)
        scrollview.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluex(), left: 0, bottom: 0, right: 0))
        scrollview.showsHorizontalScrollIndicator = false
        scrollview.addSubview(h_stack)
        //scrollview.bounces = false
        h_stack.fillSuperview()
        //h_stack.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        h_stack.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        var status_array = [Status]()
        status_array = isInterview ? GetStatus().getInterviewStatus() : GetStatus().getUpKeepStatus()
        
        
        for i in status_array {
            let ss = filterStatusItemView()
            ss.label.text = i.getString()
            ss.status = i
            ss.isUserInteractionEnabled = true
            ss.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap)))
            h_stack.addArrangedSubview(ss)
        }
        h_stack.addArrangedSubview(UIView())
    }
    @objc func didTap(sender:UITapGestureRecognizer){
        if let vv = sender.view as? filterStatusItemView {
            vv.isSelected = !vv.isSelected
            
            delegate?.setFilterStatus(view: vv)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol EmployeeDelegate{
    func setData(data:Employer)
}
class EmployeeSelectionController : UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.setData(data: data[indexPath.row])
        self.popView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        fetchApi()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        cell.textLabel?.text = data[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    var delegate : EmployeeDelegate?
    let container = SampleContainerView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var data = [Employer]()
    let searchField = SearchTextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.label.text = "業務".localized
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 500.calcvaluex(), height: 580.calcvaluey()))
        container.addSubview(searchField)
        searchField.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 52.calcvaluey()))
        searchField.layer.cornerRadius = 5.calcvaluex()
        searchField.layer.borderWidth = 1.calcvaluex()
        searchField.layer.borderColor = #colorLiteral(red: 0.3939327664, green: 0.3939327664, blue: 0.3939327664, alpha: 1)
        searchField.placeholder = "搜尋業務".localized
        searchField.delegate = self
        searchField.clearButtonMode = .unlessEditing
        container.addSubview(tableview)
        tableview.anchor(top: searchField.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 26.calcvaluey(), right: 26.calcvaluex()))
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        fetchApi()
    }
    func fetchApi(){
        let hud = JGProgressHUD()
        hud.show(in: self.tableview)
        var urlString = "api-or/v1/employees"
        if let text = searchField.text , text != ""{
            urlString += "?name=\(text)"
        }
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        NetworkCall.shared.getCall(parameter: urlString, decoderType: EmployeesData.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                if let json = json?.data {
                    self.data = json
                    self.tableview.reloadData()
                    
                    
                }
                else{
                    self.data = []
                    self.tableview.reloadData()
                }
            }

        }
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
class FilterDateController: UIViewController,UITextFieldDelegate ,EmployeeDelegate,filterStatusDelegate{
    func setFilterStatus(view: filterStatusItemView) {
        if let status = view.status {
            if view.isSelected {
                data.statusArray.append(status)
            }
            else{
                data.statusArray = data.statusArray.filter({ st in
                    return st.key != status.key
                })
            }
        }
        
        print(771,data.statusArray)

    }
    
    
    func setData(data: Employer) {
        employeeField.textfield.text = data.name
        self.data.employee = data
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        employeeClear = true
        self.data.employee = nil
        textField.resignFirstResponder()
        return true
    }
    var employeeClear = false
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if employeeClear {
            employeeClear = false
            return false
        }
        let vd = EmployeeSelectionController()
        vd.delegate = self
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
        return false
    }
    let container = UIView()
    let header = UILabel()
    let employeeField = PriceManagementDateForm(text: "業務".localized, placeholdertext: "請選擇業務".localized)
    let startdateField = PriceManagementDateForm(text: "起始時間".localized, placeholdertext: "請選擇起始時間".localized)
    let enddateField = PriceManagementDateForm(text: "結束時間".localized, placeholdertext: "請選擇結束時間".localized)
    let registbutton = UIButton()
    weak var con : PriceManagementController?
    var data = FilterClass()
    let clearButton = UIButton(type: .custom)
    let filterStatus = filterStatusView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        container.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(container)
        container.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 573.calcvaluex(), height: 0))
        container.centerInSuperview()
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        header.text = "篩選".localized
        container.addSubview(header)
        header.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluex(), left: 0, bottom: 0, right: 0))
        header.centerXInSuperview()
        
        let xbutton = UIButton(type: .custom)
        xbutton.imageView?.contentMode = .scaleAspectFill
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        clearButton.setTitle("清除".localized, for: .normal)
        clearButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        clearButton.setTitleColor(.black, for: .normal)
        container.addSubview(clearButton)
        clearButton.anchor(top: header.centerYAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: -25.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        clearButton.addTarget(self, action: #selector(clearFilter), for: .touchUpInside)
        let v_stack = Vertical_Stackview(spacing:26.calcvaluey())
        v_stack.addArrangedSubview(employeeField)

        employeeField.constrainHeight(constant: 90.calcvaluey())
        v_stack.addArrangedSubview(filterStatus)
        filterStatus.delegate = self
        let hh_stack = Horizontal_Stackview(distribution:.fillEqually,spacing: 12.calcvaluex())
        
        hh_stack.addArrangedSubview(startdateField)
        hh_stack.addArrangedSubview(enddateField)
        v_stack.addArrangedSubview(hh_stack)
        startdateField.constrainHeight(constant: 90.calcvaluey())
        enddateField.constrainHeight(constant: 90.calcvaluey())
        employeeField.textfield.delegate = self
        employeeField.textfield.clearButtonMode = .unlessEditing
        
        registbutton.setTitle("確認".localized, for: .normal)
        registbutton.setTitleColor(.white, for: .normal)
        registbutton.backgroundColor = MajorColor().oceanSubColor
        registbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        registbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.addSubview(registbutton)
        registbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluex(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        registbutton.centerXInSuperview()
        
        registbutton.layer.cornerRadius = 48.calcvaluey()/2
        registbutton.addTarget(self, action: #selector(confirmTime), for: .touchUpInside)
        
        startdateField.tag = 0
        enddateField.tag = 1
        startdateField.delegate2 = self
        enddateField.delegate2 = self

        container.addSubview(v_stack)
        v_stack.anchor(top: header.bottomAnchor, leading: container.leadingAnchor, bottom: registbutton.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 26.calcvaluey(), right: 44.calcvaluex()))
    }
    @objc func clearFilter(){
        data.employee = nil
        data.startTime = nil
        data.endTime = nil
        data.statusArray = []
        employeeField.textfield.text = nil
        filterStatus.preSelectedStatus = []
        startdateField.textfield.text = nil
        startdateField.datepicker.maximumDate = nil
        enddateField.textfield.text = nil
        enddateField.datepicker.minimumDate = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let employ = data.employee {
            employeeField.textfield.text = employ.name
        }
        filterStatus.preSelectedStatus = data.statusArray
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        if let start = data.startTime {
            startdateField.datepicker.date = start
            startdateField.textfield.text = Date().convertToDateComponent(text: dateformatter.string(from: start), format: "yyyy-MM-dd", onlyDate: true, addEight: false)
        }
        if let end = data.endTime {
            enddateField.datepicker.date = end
            enddateField.textfield.text = Date().convertToDateComponent(text: dateformatter.string(from: end), format: "yyyy-MM-dd", onlyDate: true, addEight: false)
        }
    }
    @objc func confirmTime(){
//        if beginDate == nil{
//          showAlert(text: "起始時間")
//            return
//        }
//
//        if endDate == nil{
//            showAlert(text: "結束時間")
//            return
//        }
//        con?.startDate = self.beginDate
//        con?.endDate = self.endDate
//        con?.createDateFilter()
        
        print(3321,self.data.employee)
        delegate?.selectedFilter(filter: self.data)
        popview()
    }
    var delegate : FixFilterDelegate?
    func showAlert(text:String) {
        let alertcon = UIAlertController(title: "請選擇\(text)", message: nil, preferredStyle: .alert)
        alertcon.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
        self.present(alertcon, animated: true, completion: nil)
    }
    var beginDate : Date?
    var endDate : Date?
    @objc func popview(){
        
        self.dismiss(animated: true, completion: nil)
    }
}


extension FilterDateController: InterViewDateDelegate {
    func resetDate(tag: Int) {
        if tag == 0{
            self.data.startTime = nil
            enddateField.datepicker.minimumDate = nil
        }
        else{
            self.data.endTime = nil
            startdateField.datepicker.maximumDate = nil
        }
    }
    
    func sendBeginDate(date: Date) {
        
        beginDate = date
        self.data.startTime = date
        enddateField.datepicker.minimumDate = date
    }
    
    func sendEndDate(date: Date) {
        endDate = date
        self.data.endTime = date
        startdateField.datepicker.maximumDate = date
    }
    

}
