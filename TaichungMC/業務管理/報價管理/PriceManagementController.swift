//
//  PriceManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
protocol InterViewDateDelegate {
    func sendBeginDate(date:Date)
    func sendEndDate(date:Date)
    func resetDate(tag:Int)
}
class PriceManagementDateForm:InterviewDateFormView {
    var delegate2: InterViewDateDelegate?
    var clearV = false
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if clearV {
            clearV = false
            return false
        }
        return true
    }
    override init(text: String, placeholdertext: String? = nil, formatter: String = "yyyy/MM/dd", mode: String = "order") {
        super.init(text: text, placeholdertext: placeholdertext, formatter: formatter, mode: mode)
        textfield.padding = 36.calcvaluex()
        self.tlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textfieldanchor.height?.constant = 61.calcvaluey()
        textfieldanchor.top?.constant = 4.calcvaluey()

        textfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        textfield.clearButtonMode = .unlessEditing
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        delegate2?.resetDate(tag: self.tag)
        clearV = true
        return true
    }
    override func textFieldDidEndEditing(_ textField: UITextField) {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        textField.text = Date().convertToDateShortComponent(text: formatter.string(from: datepicker.date))
        if tag == 0{
            delegate2?.sendBeginDate(date: datepicker.date)
        }
        else{
            delegate2?.sendEndDate(date: datepicker.date)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PriceManagementCell:UITableViewCell {
    let container = UIView()
    let statuslabel = newStatusLabel()
    let toplabel = UILabel()
    let sublabel = UILabel()
    let timelabel = UILabel()
    
//    let machine1 = QuoteLabel(text: "立式加工中心機")
//    let machine2 = QuoteLabel(text: "Vcenter-P76/106/136")
//    let machine3 = QuoteLabel(text: "Vc-P106")
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8710518479, green: 0.8733028769, blue: 0.8785669208, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2.calcvaluex(), bottom: 2, right: 2.calcvaluex()))
        let horizontalStackview = Horizontal_Stackview(spacing:26.calcvaluex(),alignment: .center)
        horizontalStackview.addArrangedSubview(statuslabel)
        
        container.addSubview(horizontalStackview)
        horizontalStackview.fillSuperview(padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 27.calcvaluex(), right: 26.calcvaluex()))

//        container.addSubview(statuslabel)
//        statuslabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        
        //statuslabel.centerYInSuperview()
//        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
//        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.label.text = "已擱置"
//        statuslabel.textAlignment = .center
//        statuslabel.layer.cornerRadius = 30.calcvaluey()/2
//        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
//        statuslabel.layer.borderWidth = 1.calcvaluex()
        
       // container.addSubview(toplabel)
        toplabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
       // toplabel.text = "久大行銷股份有限公司"
        toplabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        //toplabel.anchor(top: container.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        
        
        //container.addSubview(timelabel)
        timelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
       // timelabel.text = "10月05日下午4:47"
       // timelabel.anchor(top: toplabel.bottomAnchor, leading: toplabel.leadingAnchor, bottom:nil , trailing: nil,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        let vertstack = Vertical_Stackview(spacing:3.calcvaluey(),alignment: .leading)
        vertstack.addArrangedSubview(toplabel)
        vertstack.addArrangedSubview(timelabel)
        horizontalStackview.addArrangedSubview(vertstack)
//        addSubview(machine1)
//        machine1.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 0, height: 24.calcvaluey()))
//        machine1.layer.cornerRadius = 24.calcvaluey()/2
//        
//        addSubview(machine2)
//        machine2.anchor(top: machine1.bottomAnchor, leading: nil, bottom: nil, trailing: machine1.trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
//        machine2.layer.cornerRadius = 24.calcvaluey()/2
//        
//        addSubview(machine3)
//        machine3.anchor(top: machine2.bottomAnchor, leading: nil, bottom: nil, trailing: machine2.trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
//        machine3.layer.cornerRadius = 24.calcvaluey()/2
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class DateFilterView : UIView{
    let label : UILabel = {
       let lb = UILabel()
        lb.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        lb.textColor = .white
        lb.textAlignment = .right
        return lb
    }()
    let xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .white

        return xbutton
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MajorColor().oceanColor
        layer.cornerRadius = 38.calcvaluey()/2
        addSubview(xbutton)
        xbutton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 2.calcvaluey(), right: 8.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 0))
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: xbutton.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 2.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PriceManagementController: SalesManagementController,MaintainFilterViewDelegate,FixFilterDelegate {
    func selectedFilter(filter: FilterClass) {
        print(3321,filter.getArray())
        if filter.getArray().count != 0{
            //filterData = filter
            filter_heightAnchor?.constant = 60.calcvaluey()
           // tableviewanchor?.top?.constant = 0
        }
        else{
            //filterData = nil
            filter_heightAnchor?.constant = 0.calcvaluey()
            //tableviewanchor?.top?.constant = 26.calcvaluey()
        }
        filter.isInterview = true
        filterView.data = filter
        self.bottomview.filter_data = filter
        self.view.layoutIfNeeded()
        self.bottomview.resetFetch()
    }
    
    func reloadFilter() {
        print(filterView.data.getArray())
        if filterView.data.getArray().count == 0{
            filter_heightAnchor?.constant = 0.calcvaluey()
            
            self.view.layoutIfNeeded()
        }
        self.bottomview.filter_data = filterView.data
        
        self.bottomview.resetFetch()
    }
    
    override func showRecord(id: String) {
        let vd = PriceHistoryController()
        print(771,id)
        vd.modalPresentationStyle = .fullScreen
        vd.id = id
        self.present(vd, animated: true, completion: nil)
    }
    override  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        
    }
    let datebutton = UIButton()
    let createPricebutton = AddButton4()

    override func viewWillAppear(_ animated: Bool) {
       // super.viewWillAppear(animated)
        self.bottomview.resetFetch()
//        getUserData()
//        fetchApi2()
        NotificationCenter.default.addObserver(self, selector: #selector(changeLang), name: Notification.Name(rawValue : "switchLanguage"), object: nil)
    }
    var startDate : Date?
    var endDate : Date?
    let dateFilter = DateFilterView()
    var dateWidthAnchor:NSLayoutConstraint?
    var filterList = [InterViewList]()
    override func changeLang() {
        
        titleview.label.text = "訪談管理".localized
        searchcontroller.attributedPlaceholder = "搜尋業務名稱/客戶公司".localized.convertoSearchAttributedString()
        datebutton.setTitle("篩選".localized, for: .normal)
        createPricebutton.titles.text = "建立訪談".localized
        filterView.data = filterView.data
        bottomview.tableview.reloadData()
        
    }
    @objc func removeDateFilter(){
        dateFilter.removeFromSuperview()

        bottomview.startingDate = nil
        bottomview.endDate = nil
        
        bottomview.resetFetch()
        dateWidthAnchor?.constant = 138.calcvaluex()
        self.view.layoutIfNeeded()
    }
    func createDateFilter(){
        dateFilter.xbutton.addTarget(self, action: #selector(removeDateFilter), for: .touchUpInside)
        dateFilter.label.text = Date().convertToRangeDate(beginDate: self.startDate, endDate: self.endDate)
        datebutton.addSubview(dateFilter)
        dateWidthAnchor?.constant = 162.calcvaluex()
        dateFilter.layer.cornerRadius = 46.calcvaluey()/2
        dateFilter.fillSuperview(padding: .init(top: 0, left: 0.calcvaluex(), bottom: 0, right: 0))
        
        filterByDate(startDate: self.startDate, endDate: self.endDate)
    }
    func filterByDate(startDate:Date?,endDate:Date?) {
        let startInterval = startDate?.getCurrentTime(format: "yyyy-MM-dd") ?? ""
        let endInterval = endDate?.getCurrentTime(format: "yyyy-MM-dd") ?? ""
        
        bottomview.startingDate = startInterval
        bottomview.endDate = endInterval
        bottomview.resetFetch()
        
    }
    func filterByOriginDate(startDate:Date?,endDate:Date?) {
        let startInterval = startDate?.getCurrentTime(format: "yyyy-MM-dd") ?? ""
        let endInterval = endDate?.getCurrentTime(format: "yyyy-MM-dd") ?? ""
        bottomview.startingDate = startInterval
        bottomview.endDate = endInterval
        bottomview.resetFetch()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        extrabutton_stackview.safelyRemoveArrangedSubviews()
        extrabutton_stackview.addArrangedSubview(UIView())
        extrabutton_stackview.addArrangedSubview(createPricebutton)
        titleview.label.text = "訪談管理".localized
        bottomview.isSale = false
        
        searchcontroller.attributedPlaceholder = "搜尋業務名稱/客戶公司".localized.convertoSearchAttributedString()
        //bottomview.searchcontroller.attributedPlaceholder = "訪談搜尋".convertoSearchAttributedString()
        //bottomview.addbutton.isHidden = true

        datebutton.setTitle("篩選".localized, for: .normal)
        datebutton.setTitleColor(.white, for: .normal)
        datebutton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        datebutton.titleLabel?.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        datebutton.layer.cornerRadius = 46.calcvaluey()/2
        createPricebutton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        createPricebutton.titles.text = "建立訪談".localized
                //createPricebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
                //createPricebutton.layer.cornerRadius = 46.calcvaluey()/2
        stackview.safelyRemoveArrangedSubviews()
        
        dateWidthAnchor = datebutton.widthAnchor.constraint(equalToConstant: 100.calcvaluex())
        dateWidthAnchor?.isActive = true
        //createPricebutton.constrainWidth(constant: 170.calcvaluex())
        stackview.addArrangedSubview(searchcontroller)
        stackview.addArrangedSubview(datebutton)
       // stackview.addArrangedSubview(createPricebutton)
        datebutton.addTarget(self, action: #selector(filterDate), for: .touchUpInside)
        createPricebutton.addTarget(self, action: #selector(createInterView), for: .touchUpInside)
        filterView.delegate = self

    }
    
    @objc func createInterView(){
        let vd = InterviewController()
        vd.mode = .New
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    
    @objc func filterDate(){
        let vd = FilterDateController()
        vd.delegate = self
        vd.data = self.bottomview.filter_data
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        self.present(vd, animated: true, completion: nil)
    }
    func fetchApi2(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/interviews", decoderType: InterViewListData.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    hud.dismiss()
                    
                    self.list = json.data
                    self.createGroup()
                }
            }

        }
    }
    func createGroup(){
        
        
//        for i in self.filterList {
//            let employee = i.owner.employee
//            
//            if i.owner.id == self.pre_user?.id{
//                   
//                    group[0].sales[0].interviewList.append(i)
//                    continue
//                }
//                if let groupIndex = group.firstIndex(where: { (gp) -> Bool in
//                    return gp.code == employee.department_code
//                }) {
//                    if let salesIndex = self.group[groupIndex].sales.firstIndex(where: { (st) -> Bool in
//                        return st.code == employee.code
//                    }) {
//                        group[groupIndex].sales[salesIndex].interviewList.append(i)
//                    }
//                    else{
//                        group[groupIndex].sales.append(SalesCompany(name: employee.name ?? "", code: employee.code ?? "", company: [], interviewList: [i]))
//                    }
//                }
//                else{
//                    
//                    group.append(DepartmentGroup(name: employee.department_name ?? "", code: employee.department_code ?? "", sales: [SalesCompany(name: employee.name ?? "", code: employee.code ?? "", company: [], interviewList: [i])]))
//                }
//            
//        }
//        UIView.performWithoutAnimation {
//            self.slidecollectionview.reloadData()
//        }
//        
//        if selected_index == 0{
//        self.bottomview.list = group[selected_index].sales.first?.interviewList ?? []
//        }
//        else{
//            self.bottomview.list = group[selected_index].sales[self.peopleIndex].interviewList
//        }
    }
    override func fetchApi() {

    }
    
    var list = [InterViewList]() {
        didSet{
            self.filterList = list
        }
    }
    override func popview() {
        mainview?.showcircle()
        
        
        self.peoplecollectionview.collectionViewLayout.invalidateLayout()
        UIView.animate(withDuration: 0.4) {
            
            self.view.layoutIfNeeded()
        }
    }

}
