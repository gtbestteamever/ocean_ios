//
//  FileModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/13/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation



struct ReturnSort : Codable {
    var data : [FolderFile]?
}
struct ReturnFolder : Codable{
    var data : FolderFile?
}

struct ReturnFiles : Codable {
    var data : [FolderFile]?
}
struct FolderFileModel : Codable{
    var data : FolderType?
}

struct FolderType : Codable {
    var pr : [FolderFile]?
    var share : [FolderFile]?
    
    private enum CodingKeys: String,CodingKey {
        case pr = "private", share
    }
}
struct FolderFile : Codable {
    var id : String
    var parent_id : String
    var order : Int
    var name : String
    var type : String
    var size : Int?
    var downloadable : Int?
    var path : String?
    var path_url : String?
    var writers : [String]
    
}

enum FileMode : String{
    case Directory = "資料夾"
    case Image = "圖片"
    case Video = "影片"
    case File = "檔案"
    case Word = "W"
    case Excel = "E"
    case Pdf = "PDF"
    case Powerpoint = "P"
    case None = "N"
}
class ViewerFolderFile {
    var id : String
    var order : Int
    var name : String
    var parent_id : String
    var path_url : String
    var writers : [String]
    var title : String
    var type : FileMode
    var percent : Float = 0
    var isDownloading = false
    var downloadable : Int
    var children: [ViewerFolderFile] = []
    var parent: ViewerFolderFile?
    init(id:String,parent_id:String,path_url:String,title:String,type:String,order:Int,writers:[String],downloadable:Int) {
        self.id = id
        self.parent_id = parent_id
        self.title = title
        self.path_url = path_url
        self.order = order
        self.writers = writers
        let url = URL(string: path_url)
        self.downloadable = downloadable
        if type == "dir" {
            name = "資料夾"
            self.type = FileMode.Directory
        }
        else{
            name = url?.lastPathComponent ?? ""
            let ss = (url?.pathExtension ?? "").lowercased()
            
            if ss == "png" || ss == "jpg" || ss == "jpeg" {
                self.type = FileMode.Image
            }
            else if ss == "pdf" {
                self.type = FileMode.Pdf
            }
            else if ss == "doc" || ss == "docx" {
                self.type = FileMode.Word
            }
            else if ss == "ppt" || ss == "pptx" {
                self.type = FileMode.Powerpoint
            }
            else if ss == "xls" || ss == "xlsx" || ss == "csv"{
                self.type = FileMode.Excel
            }
            else if ss == "mp4" || ss == "mov" || ss == "3pg" || ss == "rmvb" {
                self.type = FileMode.Video
            }
            else{
                self.type = FileMode.None
            }
            
            
        }
        
    }
    
    func add(child: ViewerFolderFile) {
      children.append(child)
      child.parent = self
    }
}
extension ViewerFolderFile {
  // 1
  func search(value: String) -> ViewerFolderFile? {
    
    if value == self.id {
      return self
    }
    // 3
    for child in children {
      if let found = child.search(value: value) {
        return found
      }
    }
    // 4
    return nil
  }
    
    func searchRoot() -> ViewerFolderFile? {
        
        if parent == nil{
            return self
        }
        return parent?.searchRoot() ?? nil
    }
    
    func getAllParent(dt : [ViewerFolderFile]) -> [ViewerFolderFile] {
        var at = dt
        if parent == nil{
            at.append(self)
            return at
        }
        at.append(self)
        return self.parent?.getAllParent(dt: at) ?? []
    }
}

extension ViewerFolderFile: CustomStringConvertible {
  // 2
  var description: String {
    // 3
    var text = "\(id)"
    
   // 4
    if !children.isEmpty {
      text += " {" + children.map { $0.description }.joined(separator: ", ") + "} "
    }
    return text
  }
}
