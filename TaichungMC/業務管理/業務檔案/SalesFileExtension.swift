//
//  SalesFileExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/14/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension SalesFileManagementController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            if text == ""{
                self.current_arr = array[index].children
            }
            else{
                self.filterArray()
            }
            
            self.newCollectionView.reloadData()
        }
    }
}
extension SalesFileManagementController: SalesRootHeaderViewDelegate {
    func setNewRoot(data: ViewerFolderFile, index: Int) {
        array[index] = data
        self.current_arr = array[index].children
        self.filterArray()
        self.newCollectionView.reloadData()
    }
    
    
}
extension SalesFileManagementController: nFileCellDelegate {
    func changeName(data: ViewerFolderFile) {
        let changeController = AddFolderController()
        changeController.data = data
        changeController.changeMode = true
        changeController.modalPresentationStyle = .overCurrentContext
        changeController.parent_con = self
        self.present(changeController, animated: false, completion: nil)
    }
    func openFile(data: ViewerFolderFile) {
       
        if data.type == .Directory {
            if let root = data.searchRoot() {
                if root.id == "-1" {
                    addButton.isHidden = false
                    sortButton.isHidden = false
                }
                else {
                    let id = GetUser().getUser()?.data.id ?? ""
                    if data.writers.contains(id) {
                        sortButton.isHidden = false
                        addButton.isHidden = false
                    }
                    else{
                        sortButton.isHidden = true
                        addButton.isHidden = true
                    }
                }
            }
            
            self.array[index] = data
            self.current_arr = self.array[index].children
            self.filterArray()
            self.newCollectionView.reloadData()
        }
        else{
            let exp = ExpandFileController()
 
            guard let url = URL(string: data.path_url) else {return}
            if let local_url = FileManager.default.getFile(name: url.lastPathComponent) {
                local_url.loadWebview(webview: exp.imgv)
                //exp.imgv.loadRequest(URLRequest(url: local_url))
            }
            else{
                url.loadWebview(webview: exp.imgv)
                //exp.imgv.loadRequest(URLRequest(url: url))
            }
            
            exp.modalPresentationStyle = .overFullScreen
            
            self.present(exp, animated: false, completion: nil)
        }
    }
    
    func removeLocalFile(data: ViewerFolderFile) {
        guard let url = URL(string: data.path_url) else {return}
        FileManager.default.deleteFile(name: url.lastPathComponent)
        self.newCollectionView.reloadData()
    }
    func removeRemoteLocalFile(data: ViewerFolderFile) {
        if let url = URL(string: data.path_url) {
            FileManager.default.deleteFile(name: url.lastPathComponent)
        }
        
        let hud = JGProgressHUD()
        hud.textLabel.text = "刪除中.."
        hud.show(in: self.view)
        
        NetworkCall.shared.getDelete(parameter: "api-or/v1/drive/delete/\(data.id)", decoderType: UpdateClass.self) { (json) in
            DispatchQueue.main.async {
                
                hud.dismiss()
                if let json = json?.data{
                    print(221,json)
                }
                self.fetchApi()
            }

            
        }
    }
    func removeFolder(data: ViewerFolderFile) {
        let hud = JGProgressHUD()
        hud.textLabel.text = "刪除中.."
        hud.show(in: self.view)
        NetworkCall.shared.getDelete(parameter: "api-or/v1/drive/delete/\(data.id)", decoderType: UpdateClass.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                if let json = json?.data{
                    
                }
                self.fetchApi()
                self.newCollectionView.reloadData()
            }

            
        }
    }
    
}
extension SalesFileManagementController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        let id = GetUser().getUser()?.data.id ?? ""
        if (self.current_arr[indexPath.item].searchRoot()?.writers.contains(id) ?? false) || self.current_arr[indexPath.item].searchRoot()?.id == "-1"{
            return sortMode
        }
        
        return false
    }
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        var c_orders = [Int]()
        
        for i in current_arr {
            c_orders.append(i.order)
        }
        let cr = current_arr.remove(at: sourceIndexPath.item)
        

        

        current_arr.insert(cr, at: destinationIndexPath.item)
        
        for (index,i) in c_orders.enumerated(){
            
            current_arr[index].order = i
        }
        


    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == newCollectionView {

            
            return current_arr.count
        }
        
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == newCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newCell", for: indexPath) as! nFileCell
           
            cell.setData(data: self.current_arr[indexPath.item])
            cell.sortMode = self.sortMode
            cell.delegate = self
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! FileCell
        cell.lb.text = array[indexPath.row].searchRoot()?.title.localized
        
        if index == indexPath.item{
            
            if index == 0{
                cell.setSelected(withLock: true)
            }
            else{
                cell.setSelected()
            }
            
        }
        else{
        
        
            
            cell.setUnSelected()
        }
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == newCollectionView {
            return 28.calcvaluey()
        }
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == newCollectionView {
            return .init(width: (collectionView.frame.width - (4 * 24.calcvaluex()) + 4.calcvaluex()) / 5, height: 197.calcvaluey())
        }
        return .init(width: 120.calcvaluex(), height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if index != indexPath.item{
        
        if collectionView == filecollectionview {
            index = indexPath.item
            if let root = array[index].searchRoot() {
                array[index] = root
            }
           
            if array[index].id == "-1" {
                hideButton = false
                addButton.isHidden = false
                sortButton.isHidden = false
            }
            else{
                let id = GetUser().getUser()?.data.id ?? ""
                if array[index].writers.contains(id) {
                    hideButton = false
                    sortButton.isHidden = false
                    addButton.isHidden = false
                }
                else{
                    hideButton = true
                    sortButton.isHidden = true
                    addButton.isHidden = true
                }
                
            }
            self.current_arr = array[index].children
            self.filterArray()
            self.filecollectionview.reloadData()
            self.newCollectionView.reloadData()
            dontCallApi = true
            sortMode = false
        }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if array.count == 0{
            return .zero
        }
        if collectionView == newCollectionView && array[index].getAllParent(dt: []).count != 1{
        return .init(width: collectionView.frame.width, height: 50.calcvaluey())
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader && collectionView == newCollectionView{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! SalesRootHeaderView
            header.tag = index
            header.delegate = self
            if array.count != 0{
            header.data = array[index].getAllParent(dt: []).reversed()
            }
            return header
        }
        
        return UICollectionReusableView()
    }
    
}
