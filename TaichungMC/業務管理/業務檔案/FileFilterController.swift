//
//  FileFilterController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/13/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol FileFilterDelegate {
    func setMode(mode:FileMode)
}
class FileFilterCell : UITableViewCell {
    let topStackView = UIStackView()
    lazy var imageButton = createLabel(text: "圖片".localized)
    lazy var VideoButton = createLabel(text: "影片".localized)
    lazy var FileButton = createLabel(text: "檔案".localized)
    lazy var FolderButton = createLabel(text: "資料夾".localized)
    
    var delegate: FileFilterDelegate?
    var selectedMode = [FileMode]() {
        didSet{
            if selectedMode.contains(.Directory) {
                setLabel(label: FolderButton)
            }
            else{
                unsetLabel(label: FolderButton)
            }
            
            if selectedMode.contains(.Video) {
                setLabel(label: VideoButton)
            }
            else{
                unsetLabel(label: VideoButton)
            }
            if selectedMode.contains(.Image) {
                setLabel(label: imageButton)
            }
            else{
                unsetLabel(label: imageButton)
            }
            if selectedMode.contains(.File) {
                setLabel(label: FileButton)
            }
            else{
                unsetLabel(label: FileButton)
            }
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        let horizontalStackView = UIStackView()
        horizontalStackView.distribution = .fillEqually
        horizontalStackView.spacing = 16.calcvaluex()
        
        horizontalStackView.addArrangedSubview(imageButton)
        horizontalStackView.addArrangedSubview(VideoButton)
        horizontalStackView.addArrangedSubview(FileButton)
        
        topStackView.distribution = .fill
        topStackView.alignment = .leading
        topStackView.spacing = 16.calcvaluey()
        topStackView.axis = .vertical
        topStackView.addArrangedSubview(horizontalStackView)
        topStackView.addArrangedSubview(FolderButton)
        FolderButton.constrainWidth(constant: 118.calcvaluex())
        contentView.addSubview(topStackView)
        topStackView.fillSuperview(padding: .init(top: 12.calcvaluex(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    
    }
    func createLabel(text: String) -> PaddedLabel{
        let pd = PaddedLabel()
        pd.text = text
        pd.layer.cornerRadius = 20.calcvaluey()
        pd.clipsToBounds = true
        pd.backgroundColor = MajorColor().oceanColor
        pd.textAlignment = .center
        pd.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        pd.textColor = .white
        pd.isUserInteractionEnabled = true
        pd.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setMode)))
        return pd
        
        
    }
    func setLabel(label:PaddedLabel){
        label.backgroundColor = MajorColor().oceanColor
    }
    func unsetLabel(label:PaddedLabel) {
        label.backgroundColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
    }
    @objc func setMode(sender:UITapGestureRecognizer){
        print( 221, (sender.view as? PaddedLabel)?.text?.localized)
        if let vs = sender.view as? PaddedLabel,let mode = FileMode(rawValue: vs.text?.localized ?? ""){
            print(221,vs.text?.localized)
            delegate?.setMode(mode: mode)

            
            
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension FileFilterContainerView : UITableViewDelegate,UITableViewDataSource,FileFilterDelegate{
    func setMode(mode: FileMode) {
        if !selectedMode.contains(mode) {
            selectedMode.append(mode)
        }
        else{
            selectedMode = selectedMode.filter({ (ft) -> Bool in
                return ft != mode
            })
        }
        self.tableview.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FileFilterCell
        cell.delegate = self
        cell.selectedMode = self.selectedMode
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cd = UIView()
       
        let label = UILabel()
        if section == 0{
            label.text = "顯示".localized
        }

        else if section == 1{
            label.text = "檔案設定"
        }

        
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        
        cd.addSubview(label)
        label.anchor(top: cd.topAnchor, leading: cd.leadingAnchor, bottom: cd.bottomAnchor, trailing: cd.trailingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        return cd
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
class FileFilterContainerView : UIView {
    var topLabel : UILabel = {
        let tp = UILabel()
        tp.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        tp.textColor = .black
        tp.text = "檔案篩選".localized
        return tp
    }()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var selectedMode = [FileMode]()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
    
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.anchor(top: topLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        tableview.register(FileFilterCell.self, forCellReuseIdentifier: "cell")
        tableview.contentInset = .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0)
        tableview.contentOffset = .init(x: 0, y: -36.calcvaluey())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FileFilterController: UIViewController,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: container) ?? false {
            return false
        }
        return true
    }
    weak var con : SalesFileManagementController?
    let container = FileFilterContainerView()
    var containerAnchor:AnchoredConstraints?
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.backgroundColor = .white
        view.addSubview(container)
        containerAnchor = container.anchor(top: view.topAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 384.calcvaluex(), height: 0))
        
        
        

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func dismissView(){
        hideContainer()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        showContainer()
    }
    
    func showContainer(){
        UIView.animate(withDuration: 0.4) {
            self.containerAnchor?.leading?.constant = -384.calcvaluex()
            self.view.layoutIfNeeded()
        }
    }
    func hideContainer(){
        UIView.animate(withDuration: 0.2) {
            self.containerAnchor?.leading?.constant = 0
            self.view.layoutIfNeeded()
        }completion: { (com) in
            if com {
                self.dismiss(animated: false, completion: {
                    
                    self.con?.selectedMode = self.container.selectedMode
                    self.con?.current_arr = self.con?.array[self.con?.index ?? 0].children ?? []
                    self.con?.filterArray()
                    self.con?.newCollectionView.reloadData()
                })
            }
        }


    }

}
