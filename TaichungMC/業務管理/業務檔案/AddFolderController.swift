//
//  AddFolderController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class AddFolderController: UIViewController {
    let container = UploadFolderView()
    var parent_id : String?
    weak var parent_con : SalesFileManagementController?
    var changeMode: Bool = false {
        didSet{
            if changeMode {
            container.data = data
            container.changeMode = changeMode
            }
        }
    }
    var data : ViewerFolderFile?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 287.calcvaluey()))
        
        container.xbutton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        
        container.button.addTarget(self, action: #selector(createFolder), for: .touchUpInside)
    }
    @objc func createFolder(){
        self.view.endEditing(true)
        let hud = JGProgressHUD()
        if changeMode{
            hud.textLabel.text = "重新命名中....".localized
        }
        else{
        hud.textLabel.text = "建立資料夾中....".localized
        }
        hud.show(in: container)
        if container.folderField.textfield.text == ""{
            var alertMessage = ""
            if changeMode && data?.type != .Directory{
                alertMessage = "請輸入檔案名稱".localized
            }
            else{
                alertMessage = "請輸入資料夾名稱".localized
            }
            let alertController = UIAlertController(title: alertMessage, message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            self.present(alertController, animated: false, completion: nil)
            
            return
        }
        if let dt = data , changeMode , let text = container.folderField.textfield.text{
            
            print(dt.id)
            NetworkCall.shared.postCall(parameter: "api-or/v1/drive/\(dt.id)", dict: ["name" : text], decoderType: FolderFileModel.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json{
                        hud.textLabel.text = "重新命名成功".localized
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        hud.dismiss(afterDelay: 1, animated: true) {
                            self.dismiss(animated: false, completion: nil)
                        }
                        if let parent = self.parent_con {
                            parent.fetchApi()
                        }
                        
                    }
                }

            }
        }
        
        else{
        if let pr = parent_id , let text = container.folderField.textfield.text{
            NetworkCall.shared.postCall(parameter: "api-or/v1/drive/create", dict: ["parent_id" : pr,"name": text], decoderType: ReturnFolder.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json?.data{
                        hud.textLabel.text = "建立成功".localized
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        hud.dismiss(afterDelay: 1)
                        if let parent = self.parent_con {
                            parent.fetchApi()
                        }
                        
                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                    }
                    else{
                        print("Error")
                    }
                }

            }
        }
        }
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
    


}

class UploadFolderView :SampleContainerView {
    let button = confirmButton(type: .custom)
    let folderField = InterviewNormalFormView(text: "資料夾名稱".localized, placeholdertext: "請輸入資料夾名稱".localized)
    var changeMode = false {
        didSet{
            if changeMode {
                label.text = "重新命名".localized
                if data?.type != .Directory  {
                    folderField.tlabel.text = "檔案名稱".localized
                    folderField.textfield.attributedPlaceholder = NSAttributedString(string: "請輸入檔案名稱".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)])
                }
                
                folderField.textfield.text = data?.title
            }
        }
    }
    var data:ViewerFolderFile?
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.text = "新增資料夾".localized
        
       
        addSubview(button)
        button.centerXInSuperview()
        button.anchor(top: nil, leading: nil, bottom: self.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        
        addSubview(folderField)
        folderField.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: button.topAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 46.calcvaluey(), right: 43.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
