//
//  UploadFolderFileController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
import JGProgressHUD
extension UploadFolderFileController:URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        //
    }
    
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        DispatchQueue.main.async {
            print(221,totalBytesSent,totalBytesExpectedToSend)
            let tt = Float(totalBytesSent)/Float(totalBytesExpectedToSend)
            self.hud.progress = tt
        }

        
        
    }


}
extension UploadFolderFileController:UIDocumentPickerDelegate {
    @objc func cancelTap(){
        NetworkCall.shared.urlSession?.invalidateAndCancel()
        hud.hide(animated: true)
        tapGesture.isEnabled = true
        addFileButton.isUserInteractionEnabled = true
        addFolderButton.isUserInteractionEnabled = true
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if let data = try? Data(contentsOf: url), let id = parent_id {
            
            let name = url.lastPathComponent
            let extent = url.pathExtension
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.label.text = "上傳中...".localized
            hud.label.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
            hud.mode = .annularDeterminate
            hud.button.setTitle("取消".localized, for: .normal)
            hud.button.setTitleColor(.black, for: .normal)
            hud.button.titleLabel?.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
            hud.button.addTarget(self, action: #selector(cancelTap), for: .touchUpInside)
            hud.progress = 0
            tapGesture.isEnabled = false
            addFileButton.isUserInteractionEnabled = false
            addFolderButton.isUserInteractionEnabled = false
            NetworkCall.shared.urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
            NetworkCall.shared.postCallZip2(parameter: "api-or/v1/drive/upload", param: ["parent_id":id], data: data, dataName: name, dataParam: "uploads[]", memeString: extent, decoderType: ReturnFiles.self) { (json) in
                DispatchQueue.main.async {
                    self.hud.hide(animated: true)
                    self.hud2.indicatorView = JGProgressHUDSuccessIndicatorView()
                    self.hud2.textLabel.text = "上傳成功".localized
                    self.hud2.show(in: self.view)
                    self.hud2.dismiss(afterDelay: 1, animated: true) {
                        if let json = json?.data, let parent = self.parent_con {
                            parent.fetchApi()
                            
                            self.dismiss(animated: false, completion: nil)
                        }
                    }
                    

                }

            }
        }
    }
    

}
class UploadFolderFileController: UIViewController {
    let addFolderButton = AddButton4(type: .custom)
    let addFileButton = AddButton4(type: .custom)
    lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
    var parent_id : String? {
        didSet{
            print(221,parent_id)
        }
    }
    var hud = MBProgressHUD()
    var hud2 = JGProgressHUD()
    weak var parent_con : SalesFileManagementController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.view.addGestureRecognizer(tapGesture)
        
        view.addSubview(addFolderButton)
        view.addSubview(addFileButton)

        setUpButton(button: addFolderButton)
        setUpButton(button: addFileButton)
        
        addFileButton.titles.text = "檔案".localized
        addFolderButton.titles.text = "資料夾".localized
        
        addFileButton.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: menu_bottomInset + 26.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 0, height: 48.calcvaluey()))
        addFolderButton.anchor(top: nil, leading: nil, bottom: addFileButton.topAnchor, trailing: addFileButton.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 0, height: 48.calcvaluey()))
        addFolderButton.addTarget(self, action: #selector(addFolder), for: .touchUpInside)
        addFileButton.addTarget(self, action: #selector(addFile), for: .touchUpInside)
//        if let dt = UIImage(imageLiteralResourceName: "ic_btn_add").jpegData(compressionQuality: 0.1){
//            let config = URLSessionConfiguration.default
//            NetworkCall.shared.urlSession = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
//            NetworkCall.shared.postCallZip2(parameter: "", param: ["parent_id":"0"], data: dt, dataName: "file.jpg", dataParam: "uploads", memeString: "jpg", decoderType: UpdateClass.self) { (json) in
//                if let json = json {
//                    //
//                }
//            }
//        }
       
    }
    
    @objc func addFile(){
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
           // self.goCameraAlbumAction(type: 0)
        }
      //  controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照".localized, style: .default) { (_) in
           // self.goCameraAlbumAction(type: 1)
        }
       // controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
           
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = addFileButton
            presenter.sourceRect = addFileButton.bounds
            presenter.permittedArrowDirections = .right
            }
        self.present(controller, animated: true, completion: nil)
    }
    @objc func addFolder(){

        let vd = AddFolderController()
        vd.parent_id = self.parent_id
        vd.parent_con = self.parent_con
        vd.modalPresentationStyle = .overCurrentContext
        
        self.present(vd, animated: false, completion: nil)
    }
    
    func setUpButton(button:AddButton4) {
        button.layer.cornerRadius = 48.calcvaluey()/2
        button.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
}
