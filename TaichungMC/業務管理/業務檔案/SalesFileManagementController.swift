//
//  SalesFileManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/4/1.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class ActionButton : UIButton {
    var mode : ScheduleMode = .New
        init(width:CGFloat) {
            super.init(frame: .zero)
        setTitleColor(.white, for: .normal)
            backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
       titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
       layer.cornerRadius = 46.calcvaluey()/2
       constrainWidth(constant: width)
       constrainHeight(constant: 46.calcvaluey())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ColorActionButton : UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitleColor(.white, for: .normal)
        backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
       titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
       layer.cornerRadius = 46.calcvaluey()/2
       constrainWidth(constant: 120.calcvaluex())
       constrainHeight(constant: 46.calcvaluey())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SalesFileManagementController: SampleController {
    let searchcon = SearchTextField()
    let filecollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let newCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let addButton = AddButton4(type: .custom)
    var index = 0
    //var filterButton = UIButton(type: .custom)
    var folderFile : FolderType?
    var array = [ViewerFolderFile]()
    let filterButton = ActionButton(width: 90.calcvaluex())
    let sortButton = ActionButton(width: 90.calcvaluex())
    let cancelSortButton = ActionButton(width: 120.calcvaluex())
    let saveSortButton = ColorActionButton(type: .custom)
    var selectedMode = [FileMode.Image,.Video,.File,.Directory]
    var current_arr = [ViewerFolderFile]()
    var hideButton = false
    var dontCallApi = false
    override func changeLang() {
        super.changeLang()
        saveSortButton.setTitle("儲存排序".localized, for: .normal)
        filterButton.setTitle("篩選".localized, for: .normal)


        
        sortButton.setTitle("排序".localized, for: .normal)
        cancelSortButton.setTitle("取消排序".localized, for: .normal)
        searchcon.attributedPlaceholder = "搜尋檔案".localized.convertoSearchAttributedString()
        //addButton.titles.text = "+".localized
        self.filecollectionview.reloadData()
        self.newCollectionView.reloadData()
    }
    func filterArray(shouldSort:Bool = true){
        if shouldSort {
        current_arr.sort { (vt1, vt2) -> Bool in
            return vt1.order < vt2.order
        }
        }
            current_arr = current_arr.filter({ (vt) -> Bool in

                if (vt.type == .Excel || vt.type == .Pdf || vt.type == .Powerpoint || vt.type == .Word) && selectedMode.contains(.File) {
                    return true
                }
                
                if selectedMode.contains(vt.type) {
                    return true
                }
                return false
                
            })
        
        current_arr = current_arr.filter({ (vt) -> Bool in
            if let txt = searchcon.text {
                if txt == ""{
                    return true
                }
                return vt.title.contains(txt)
            }
            return true
        })
        
    }
    var stackview = UIStackView()
    
    var sortMode = false {
        didSet{
            if sortMode {
                saveSortButton.isHidden = false
                cancelSortButton.isHidden = false
                sortButton.isHidden = true
                addButton.isHidden = true
                sortGesture.isEnabled = true
                self.newCollectionView.reloadData()
            }
            else{
                saveSortButton.isHidden = true
                cancelSortButton.isHidden = true
               
                if !hideButton {
                sortButton.isHidden = false
                addButton.isHidden = false
                sortGesture.isEnabled = false
                }
                if !dontCallApi {
                fetchApi()
                }
                else{
                    dontCallApi = false
                }
            }
            
            
        }
    }

    @objc func goSort() {
        sortMode = true
    }
    @objc func stopSort(){
        sortMode = false
    }
    @objc func saveSort(){
        
        

        var sorted_objects = [[String:Any]]()
        for i in current_arr {
            var st = [String:Any]()
            print(661,i.name,i.order)
            st["id"] = i.id
            st["order"] = i.order
            sorted_objects.append(st)
        }
        let json = try! JSONSerialization.data(withJSONObject: sorted_objects, options: .prettyPrinted)
        
        print((String(data: json, encoding: .utf8) ?? "").trimmingCharacters(in: .newlines) )
        
        let hud = JGProgressHUD()
        hud.textLabel.text = "儲存排序中..."
        hud.show(in: self.view)
        NetworkCall.shared.postCallSort(parameter: "api-or/v1/drive-orders", dict: sorted_objects, decoderType: ReturnSort.self) { (json) in
            DispatchQueue.main.async {
                if let _ = json {
                    
                    hud.textLabel.text = "儲存成功"
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.dismiss(afterDelay: 1, animated: true) {
                        self.sortMode = false
                    }
                    
                }
            }

        }
    }
    lazy var sortGesture = UILongPressGestureRecognizer(target: self, action: #selector(sortPress))
    
    @objc func sortPress(gesture:UILongPressGestureRecognizer) {
        
        switch gesture.state {
        case .began:
            guard let indexPath = newCollectionView.indexPathForItem(at: gesture.location(in: newCollectionView)) else{return}
            
            newCollectionView.beginInteractiveMovementForItem(at: indexPath)
        case .changed:
            print(111)
            newCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: newCollectionView))
        case .ended:
            newCollectionView.endInteractiveMovement()
        default:
            newCollectionView.cancelInteractiveMovement()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = false
        //extrabutton_stackview.addArrangedSubview(searchcon)
        //extrabutton_stackview.addArrangedSubview(filterButton)
        //extrabutton_stackview.addArrangedSubview(saveSortButton)
        stackview.distribution = .fill
        stackview.alignment = .fill
        stackview.axis = .horizontal
        stackview.spacing = 12.calcvaluex()
        saveSortButton.setTitle("儲存排序".localized, for: .normal)
        filterButton.setTitle("篩選".localized, for: .normal)


        
        sortButton.setTitle("排序".localized, for: .normal)
        sortButton.addTarget(self, action: #selector(goSort), for: .touchUpInside)
        cancelSortButton.setTitle("取消排序".localized, for: .normal)
        cancelSortButton.constrainWidth(constant: 120.calcvaluex())
        cancelSortButton.isHidden = true
        cancelSortButton.addTarget(self, action: #selector(stopSort), for: .touchUpInside)
 
        saveSortButton.addTarget(self, action: #selector(saveSort), for: .touchUpInside)
        saveSortButton.isHidden = true
        titleview.label.text = "業務檔案".localized
        
        
        searchcon.attributedPlaceholder = "搜尋檔案".localized.convertoSearchAttributedString()
        
//        view.addSubview(searchcon)
//        searchcon.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluex(), left: 333.calcvaluex(), bottom: 0, right: 150.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchcon.backgroundColor = .white
        searchcon.delegate = self
        searchcon.addshadowColor(color: #colorLiteral(red: 0.9105209708, green: 0.9128736854, blue: 0.9183767438, alpha: 1))
        searchcon.layer.cornerRadius = 46.calcvaluey()/2
//        view.addSubview(filterButton)
//        filterButton.anchor(top: searchcon.topAnchor, leading: searchcon.trailingAnchor, bottom: searchcon.bottomAnchor, trailing: self.view.trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        extrabutton_stackview.safelyRemoveArrangedSubviews()

        extrabutton_stackview.addArrangedSubview(searchcon)
        searchcon.setContentHuggingPriority(.init(1), for: .horizontal)
        extrabutton_stackview.addArrangedSubview(cancelSortButton)
        extrabutton_stackview.addArrangedSubview(saveSortButton)
        extrabutton_stackview.addArrangedSubview(sortButton)
        extrabutton_stackview.addArrangedSubview(filterButton)
        
        filterButton.constrainWidth(constant: 90.calcvaluex())
        filterButton.constrainHeight(constant: 46.calcvaluey())
        searchcon.constrainHeight(constant: 46.calcvaluey())
//        searchcon.constrainWidth(constant: 450.calcvaluex())
//        view.addSubview(stackview)
//        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluex(), left: 333.calcvaluex(), bottom: 0, right: 12.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        let container = UIView()
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        
        view.addSubview(container)
        
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 25.calcvaluex(), bottom: menu_bottomInset + 28.calcvaluey(), right: 25.calcvaluex()))
        container.addSubview(filecollectionview)
        filecollectionview.backgroundColor = .clear
        filecollectionview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 56.calcvaluex(), bottom: 0, right: 56.calcvaluex()),size: .init(width: 0, height: 93.calcvaluey()))
        let sep = UIView()
        sep.backgroundColor = MajorColor().oceanColor
        container.addSubview(sep)
        sep.anchor(top: filecollectionview.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()),size: .init(width: 0, height: 1.5.calcvaluey()))
        filecollectionview.delegate = self
        filecollectionview.dataSource = self
        filecollectionview.register(FileCell.self, forCellWithReuseIdentifier: "cellid")
        (filecollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        filecollectionview.showsHorizontalScrollIndicator = false
        
        container.addSubview(newCollectionView)
        (newCollectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionHeadersPinToVisibleBounds = true
        newCollectionView.backgroundColor = .clear
        
        newCollectionView.anchor(top: filecollectionview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 22.calcvaluex(), bottom: 0, right: 22.calcvaluex()))
        newCollectionView.delegate = self
        newCollectionView.dataSource = self
        newCollectionView.showsVerticalScrollIndicator = false
        newCollectionView.register(nFileCell.self, forCellWithReuseIdentifier: "newCell")
        newCollectionView.register(SalesRootHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")

        newCollectionView.contentInset = .init(top: 2.calcvaluey(), left: 2.calcvaluex(), bottom: 50.calcvaluey(), right: 2.calcvaluex())
    
        container.addSubview(addButton)
        //addButton.titles.text = "+".localized
       // addButton.titles.font = UIFont(name: MainFont().Medium, size: 25.calcvaluex())
        addButton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        addButton.setImage(#imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate), for: .normal)
        addButton.contentVerticalAlignment = .fill
        addButton.contentHorizontalAlignment = .fill
        addButton.tintColor = .white
        addButton.addshadowColor()
        addButton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 20.calcvaluey(), right: 27.calcvaluex()),size: .init(width: 48.calcvaluey(), height: 48.calcvaluey()))
        addButton.layer.cornerRadius = 48.calcvaluey()/2
        
        addButton.addTarget(self, action: #selector(goAddFile), for: .touchUpInside)
    
//        topview.addSubview(filterButton)
//        filterButton.setImage(#imageLiteral(resourceName: "ic_filter"), for: .normal)
//        filterButton.contentHorizontalAlignment = .fill
//        filterButton.contentVerticalAlignment = .fill
//
//        filterButton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
//        filterButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        filterButton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
        sortGesture.isEnabled = false
        newCollectionView.addGestureRecognizer(sortGesture)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    func fetchApi(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/drive/list", decoderType: FolderFileModel.self) { (json) in
            DispatchQueue.main.async {
               
                hud.dismiss()
                if let json = json?.data {
                    self.folderFile = json
                    self.createData()
                }
            }

        }
    }
    func createData(){
       // array = []
        var temp = [ViewerFolderFile]()
        if let fs = self.folderFile, var pr = fs.pr,var share = fs.share {
            temp.append(ViewerFolderFile(id: "-1", parent_id: "0", path_url: "", title: "我的檔案".localized, type: "dir",order: -1,writers: [], downloadable: 1))

            while pr.count != 0 {
               
                for i in pr {
                    if i.parent_id == "0" {
                        temp[0].add(child: ViewerFolderFile(id: i.id, parent_id: i.parent_id, path_url: i.path_url ?? "", title: i.name, type: i.type,order: i.order,writers: i.writers, downloadable: i.downloadable ?? 0))
                        
                        pr = pr.filter({ (fs) -> Bool in
                            return fs.id != i.id
                        })
                        
                        
                        
                    }
                    else{
                        
                        if let ss = temp[0].search(value: i.parent_id) {
                            
                            ss.add(child: ViewerFolderFile(id: i.id, parent_id: i.parent_id, path_url: i.path_url ?? "", title: i.name, type: i.type,order: i.order,writers: i.writers, downloadable: i.downloadable ?? 0))
                            
                            
                        }
                        pr = pr.filter({ (fs) -> Bool in
                            return fs.id != i.id
                        })
                    }
                }
            }
            while share.count != 0 {
               
                for i in share {
                    if i.parent_id == "0" && i.type == "dir"{
                        temp.append(ViewerFolderFile(id: i.id, parent_id: i.parent_id, path_url: i.path_url ?? "", title: i.name, type: i.type,order: i.order,writers: i.writers, downloadable: i.downloadable ?? 0))
                        
                        share = share.filter({ (fs) -> Bool in
                            return fs.id != i.id
                        })
                        
                        
                        
                    }
                    else{
                        for (index) in 1..<temp.count {
                            if let ss = temp[index].search(value: i.parent_id) {
                                
                                ss.add(child: ViewerFolderFile(id: i.id, parent_id: i.parent_id, path_url: i.path_url ?? "", title: i.name, type: i.type,order: i.order,writers: i.writers, downloadable: i.downloadable ?? 0))
                                
                                
                            }
                            share = share.filter({ (fs) -> Bool in
                                return fs.id != i.id
                            })
                        }

                    }
                }
            }
            if array.count == 0{
                self.array = temp
            }else{
                for (index,i) in temp.enumerated() {
                    if index < array.count{
                        let current = array[index]
                        if let find_node = i.search(value: current.id) {

                            temp[index] = find_node
                        }
                    }
                }
                
                self.array = temp
            }
            self.array = self.array.sorted(by: { (vt1, vt2) -> Bool in
                return vt1.order < vt2.order
            })
            self.current_arr = self.array[index].children
            self.filterArray()
            self.filecollectionview.reloadData()
            self.newCollectionView.reloadData()
        }
    }
    @objc func goFilter(){
        let vd = FileFilterController()
        vd.con = self
        vd.container.selectedMode = self.selectedMode
        vd.modalPresentationStyle = .overCurrentContext
        
        self.present(vd, animated: false, completion: nil)
    }
    @objc func goAddFile(){
        let vd = UploadFolderFileController()
        if index == 0 && self.array[index].id == "-1"{
            vd.parent_id = "0"

        }
        else{
            vd.parent_id = self.array[index].id
        }
        
        vd.parent_con = self
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
        
    }
    
}



