//
//  SalesRootHeaderView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/15/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class RootButton : UIView {
    let label = UILabel()
    let rightArrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        rightArrow.contentMode = .scaleAspectFit
        rightArrow.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        addSubview(rightArrow)
        rightArrow.centerYInSuperview()
        rightArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        addSubview(label)
        
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: rightArrow.leadingAnchor,padding: .init(top: 0, left: 0.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol SalesRootHeaderViewDelegate{
    func setNewRoot(data:ViewerFolderFile,index:Int)
}
class SalesRootHeaderView: UICollectionReusableView {
    var delegate : SalesRootHeaderViewDelegate?
    var data = [ViewerFolderFile]() {
        didSet{
            stackview.safelyRemoveArrangedSubviews()
            for (index,i) in data.enumerated(){
                let rt = RootButton()
                rt.label.text = i.title
                rt.label.tag = index
                
                if index == data.count - 1{
                    rt.rightArrow.isHidden = true
                    rt.label.textColor = MajorColor().oceanSubColor
                }
                else{
                    rt.label.isUserInteractionEnabled = true
                    rt.label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goBack)))
                    rt.rightArrow.isHidden = false
                }
                stackview.addArrangedSubview(rt)
            }
            stackview.addArrangedSubview(UIView())
        }
    }
    @objc func goBack(sender:UITapGestureRecognizer){
        if let nTag = sender.view?.tag {
            delegate?.setNewRoot(data: data[nTag], index: self.tag)
        }
        
    }
    let stackview = UIStackView()
    let scrollView = UIScrollView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        stackview.axis = .horizontal
        stackview.distribution = .equalSpacing
        stackview.alignment = .fill
        
//        addSubview(stackview)
//        stackview.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 0, bottom: 8.calcvaluey(), right: 0))
       
        scrollView.addSubview(stackview)
        
        stackview.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, trailing: scrollView.trailingAnchor)

        scrollView.alwaysBounceHorizontal = true
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        scrollView.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 0, bottom: 8.calcvaluey(), right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
