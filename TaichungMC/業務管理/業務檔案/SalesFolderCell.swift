//
//  SalesFolderCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/14/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class FileCell : UICollectionViewCell {
    let imagev = UIImageView(image: #imageLiteral(resourceName: "ic_folder_lock_pressed").withRenderingMode(.alwaysTemplate))
    let lb = UILabel()
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        imagev.tintColor = MajorColor().oceanColor
        backgroundColor = .clear
        addSubview(imagev)
        imagev.contentMode = .scaleAspectFit
        imagev.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 56.calcvaluex(), height: 56.calcvaluex()))
        imagev.contentMode = .scaleAspectFit
        
        addSubview(lb)
        lb.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        lb.numberOfLines = 2
        lb.textColor = MajorColor().oceanColor
        lb.text = "我的檔案"
        lb.textAlignment = .center
        lb.anchor(top: imagev.bottomAnchor, leading: imagev.leadingAnchor, bottom: bottomAnchor, trailing: imagev.trailingAnchor,padding: .init(top: -14.calcvaluey(), left: 0, bottom: 0, right: 0))
        
    }
    func setSelected(withLock:Bool = false){
        if withLock {
            imagev.image = #imageLiteral(resourceName: "ic_folder_lock_pressed").withRenderingMode(.alwaysTemplate)
        }
        else{
            imagev.image = #imageLiteral(resourceName: "ic_folder_pressed").withRenderingMode(.alwaysTemplate)
        }
        
        
        lb.textColor = MajorColor().oceanColor
    }
    func setUnSelected(){
        imagev.image = #imageLiteral(resourceName: "ic_folder_everyone_normal")
        
            
           lb.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
