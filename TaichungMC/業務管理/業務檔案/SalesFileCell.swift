//
//  SalesFileCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/14/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MarqueeLabel
protocol nFileCellDelegate {
    func openFile(data:ViewerFolderFile)
    func removeLocalFile(data:ViewerFolderFile)
    func removeRemoteLocalFile(data:ViewerFolderFile)
    func removeFolder(data:ViewerFolderFile)
    func changeName(data:ViewerFolderFile)
}
class nFileCell : UICollectionViewCell,URLSessionDownloadDelegate {
    var sortMode = false {
        didSet{
            if sortMode {
                imgv.isUserInteractionEnabled = false
                downloadFile.isHidden = true
            }
            else{
                imgv.isUserInteractionEnabled = true
                if f_data?.type != .Directory && f_data?.downloadable == 1{
                downloadFile.isHidden = false
                }
            }
        }
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        DispatchQueue.main.async {
            self.progressView.progress = 1
            if let data = try? Data(contentsOf: location) , let fileName = URL(string: self.f_data?.path_url ?? "")?.lastPathComponent {
                print(441)
                FileManager.default.saveToDirectory(data: data, fileName: fileName)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                
                self.finishDownload()
            })
            
        }

    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        DispatchQueue.main.async {

            self.progressView.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        }
        
    }
    let imgv = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
    let fileType = UIImageView(image: #imageLiteral(resourceName: "ic_video"))
    let downloadFile = UIButton(type: .custom)
    let nLabel = UILabel()
    //let marLabel = MarqueeLabel(frame: .zero, duration: 8.0, fadeLength: 10.0)
    let downloadView = UIView()
    let progressView = UIProgressView(progressViewStyle: .bar)
    var session : URLSession?
    var cancelButton = UIButton(type: .custom)
    var f_data : ViewerFolderFile?
    var delegate : nFileCellDelegate?
    func setData(data:ViewerFolderFile) {
        f_data = data
        let id = GetUser().getUser()?.data.id ?? ""
        
        if (data.searchRoot()?.writers.contains(id) ?? false) || data.searchRoot()?.id == "-1"{
            press.isEnabled = true
        }
        else{
            press.isEnabled = false
        }
        nLabel.text = data.title
       // marLabel.text = data.title + " "
        imgv.contentMode = .scaleAspectFit
        

        switch data.type {
        
        case .Directory :
            imgv.image = #imageLiteral(resourceName: "ic_folder_everyone_normal")
            
            turnOffIconAndDownload()
        case .Excel :
            fileType.image = #imageLiteral(resourceName: "ic_excel")
            imgv.image = #imageLiteral(resourceName: "ic_preset_excel")
            turnOnIconAndDownload()
        case .Image :
            fileType.image = #imageLiteral(resourceName: "ic_picture")
            imgv.contentMode = .scaleAspectFill
            //imgv.image = nil
           imgv.sd_setImage(with: URL(string: data.path_url), completed: nil)
            turnOnIconAndDownload()
        case .Pdf :
            fileType.image = #imageLiteral(resourceName: "ic_pdf")
            imgv.image = #imageLiteral(resourceName: "ic_preset_pdf")
            turnOnIconAndDownload()
        case .Powerpoint :
            fileType.image = #imageLiteral(resourceName: "ic_ppt")
            imgv.image = #imageLiteral(resourceName: "ic_preset_ppt")
            turnOnIconAndDownload()
        case .Video :
            fileType.image = #imageLiteral(resourceName: "ic_video")
            imgv.image = #imageLiteral(resourceName: "ic_video")
            turnOnIconAndDownload()
        case .None :
            turnOffIconAndDownload()
        case .File:
            turnOffIconAndDownload()
        case .Word:
            fileType.image = #imageLiteral(resourceName: "ic_word")
            imgv.image = #imageLiteral(resourceName: "ic_preset_word")
            turnOnIconAndDownload()
        }
        
        if !FileManager.default.checkExist(fileName: URL(string: data.path_url)?.lastPathComponent ?? "") && data.type != .Directory && data.downloadable == 1{
            downloadFile.isHidden = false
        }
        else{
            downloadFile.isHidden = true
        }
//
    }
    func turnOffIconAndDownload(){
        self.downloadFile.isHidden = true
        self.fileType.isHidden = true
    }
    func turnOnIconAndDownload(){
        self.downloadFile.isHidden = false
        self.fileType.isHidden = false
    }
    lazy var press = UILongPressGestureRecognizer(target: self, action: #selector(goDelete))
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        let iv = UIView()
        iv.addshadowColor()
        iv.backgroundColor = .white
        iv.addSubview(imgv)
        imgv.fillSuperview()
        addSubview(iv)
        iv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 137.calcvaluey()))
        imgv.clipsToBounds = true
        imgv.backgroundColor = .white
        imgv.addshadowColor()
       // imgv.layer.borderColor = #colorLiteral(red: 0.8455147021, green: 0.8455147021, blue: 0.8455147021, alpha: 1)
       // imgv.layer.borderWidth = 1.calcvaluex()
        //imgv.contentMode = .scaleAspectFill
        
//        let stackview = UIStackView()
//        stackview.axis = .horizontal
//        stackview.spacing = 15.calcvaluex()
//        stackview.distribution = .fill
//        stackview.alignment = .top
        imgv.contentMode = .scaleAspectFit
//        addSubview(imgv)
//        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 137.calcvaluey()))
        
//        addSubview(stackview)
//        stackview.anchor(top: imgv.bottomAnchor, leading: imgv.leadingAnchor, bottom: bottomAnchor, trailing: imgv.trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
//        fileType.contentMode = .scaleAspectFit
        
        downloadFile.setImage(#imageLiteral(resourceName: "ic_download").withRenderingMode(.alwaysTemplate), for: .normal)
        downloadFile.tintColor = MajorColor().oceanSubColor
        downloadFile.contentVerticalAlignment = .fill
        downloadFile.contentHorizontalAlignment = .fill
        downloadFile.isHidden = true
        addSubview(fileType)
        fileType.anchor(top: iv.bottomAnchor, leading: iv.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        fileType.constrainWidth(constant: 24.calcvaluex())
        fileType.constrainHeight(constant: 24.calcvaluex())
        addSubview(downloadFile)
        downloadFile.anchor(top: fileType.topAnchor, leading: nil, bottom: nil, trailing: iv.trailingAnchor,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        downloadFile.constrainWidth(constant: 24.calcvaluex())
        downloadFile.constrainHeight(constant: 24.calcvaluex())
        addSubview(nLabel)
        
        nLabel.anchor(top: downloadFile.topAnchor, leading: fileType.trailingAnchor, bottom: nil, trailing: downloadFile.leadingAnchor,padding: .init(top: 0, left: 15.calcvaluex(), bottom: 0, right: 14.calcvaluex()))
        nLabel.textAlignment = .center
        nLabel.numberOfLines = 2
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        //nLabel.isHidden = true
        
//        addSubview(marLabel)
//        marLabel.anchor(top: nLabel.topAnchor, leading: nLabel.leadingAnchor, bottom: nil, trailing: nLabel.trailingAnchor)
//        marLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//        marLabel.textAlignment = .center
//        marLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//        marLabel.text = "191203台中精機 外觀展示影片歡迎參觀"
        downloadFile.addTarget(self, action: #selector(goDownload), for: .touchUpInside)
        
//        stackview.addArrangedSubview(fileType)
//        stackview.addArrangedSubview(nLabel)
//        stackview.addArrangedSubview(marLabel)
//        stackview.addArrangedSubview(downloadFile)
        
        addSubview(downloadView)
        downloadView.anchor(top: imgv.topAnchor, leading: imgv.leadingAnchor, bottom: imgv.bottomAnchor, trailing: imgv.trailingAnchor)
        downloadView.isHidden = true
        downloadView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    
        addSubview(progressView)
        progressView.anchor(top: imgv.topAnchor, leading: imgv.leadingAnchor, bottom: nil, trailing: imgv.trailingAnchor,size: .init(width: 0, height: 6.calcvaluey()))
        progressView.progress = 0
        progressView.isHidden = true
        progressView.tintColor = MajorColor().oceanColor
        progressView.trackTintColor = #colorLiteral(red: 0.7102039208, green: 0.7172356428, blue: 0.7172356428, alpha: 1)
        
        downloadView.addSubview(cancelButton)
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        cancelButton.contentVerticalAlignment = .fill
        cancelButton.contentHorizontalAlignment = .fill
        
        cancelButton.centerInSuperview(size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        
        imgv.isUserInteractionEnabled = true
        imgv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFile)))
        
        imgv.addGestureRecognizer(press)
        
    }
    @objc func goDelete(sender:UILongPressGestureRecognizer){

        if sender.state == .began{
            let controller = UIAlertController(title: "選項".localized, message: nil, preferredStyle: .actionSheet)
            let action0 = UIAlertAction(title: "重新命名".localized, style: .default) { (_) in
                if let ft = self.f_data{
                    self.delegate?.changeName(data: ft)
                }
                
            }
            let action1 = UIAlertAction(title: "刪除本地檔案".localized, style: .default, handler: {
            _ in
            if let ft = self.f_data {
                
                self.delegate?.removeLocalFile(data: ft)
            }
            
        })
        let action2 = UIAlertAction(title: "刪除本地以及後端檔案", style: .default, handler: {
            _ in
            if let ft = self.f_data {
               
                self.delegate?.removeRemoteLocalFile(data: ft)
            }
        })
            let action3 = UIAlertAction(title: "刪除資料夾".localized, style: .default) { (_) in
                if let ft = self.f_data {
                    self.delegate?.removeFolder(data: ft)
                }
            }
            if self.f_data?.type == .Directory{
                controller.addAction(action0)
                controller.addAction(action3)
            }
            else{
                controller.addAction(action0)
        controller.addAction(action1)
        controller.addAction(action2)
            }
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = imgv
            presenter.sourceRect = imgv.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(controller, animated: true, completion: nil)
        }
        
    }
    @objc func openFile(){

        if let dt = f_data {
            delegate?.openFile(data: dt)
        }
        
    }
    func showDownload(){
        downloadFile.isHidden = true
        downloadView.isHidden = false
       
        progressView.isHidden = false
    }
    func finishDownload(){
        downloadFile.isHidden = true
        downloadView.isHidden = true
       
        progressView.isHidden = true
//        marLabel.pauseLabel()
//
//        marLabel.isHidden = true
//        nLabel.isHidden = false
    }
    @objc func goDownload(){
        showDownload()
        guard let url = URL(string: f_data?.path_url ?? "") else {return}
        let request = URLRequest(url: url)
        session = URLSession(configuration: .background(withIdentifier: UUID().uuidString), delegate: self, delegateQueue: nil)
        
        session?.downloadTask(with: request).resume()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


