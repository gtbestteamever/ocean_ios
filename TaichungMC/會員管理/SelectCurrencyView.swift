//
//  SelectCurrencyView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol SelectCurrencyViewDelegate {
    func didSelectCurrency(currency:String,index:Int)
}
class SelectCurrencyView: UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = currencies[indexPath.row]
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectCurrency(currency: currencies[indexPath.row],index: self.tag)
    }
    var currencies = [String]()
    var delegate : SelectCurrencyViewDelegate?
    let tableview = UITableView(frame: .zero, style: .grouped)
    init(currencies:[String],delegate:SelectCurrencyViewDelegate) {
        super.init(frame: .zero)
        self.currencies = currencies
        self.delegate = delegate
        backgroundColor = .white
        addshadowColor()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.showsVerticalScrollIndicator = false
        addSubview(tableview)
        tableview.fillSuperview()
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
