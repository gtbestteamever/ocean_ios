//
//  SettingModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/19/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct SettingModel : Codable{
    var data : SettingData?
}

struct SettingData : Codable {
    var upkeep : UpKeepData?
    var upkeeptask : UpKeepTask?
    var upkeeporder : UpKeepOrder?
    var interview : InterviewStatus?
    var apprelated : AppRelated?
}
struct AppRelated : Codable {
    var main_url : String?
}
struct InterviewStatus : Codable {
    var statuses : [Status]
}
struct UpKeepOrder : Codable {
    var component_statuses : [Status]
    var statuses : [Status]
    var hours_of_work_range_fees_select_machine : SelectMachineFee
    var hours_of_work_range_fees_input_machine : InputMachineFee
}
struct SelectMachineFee : Codable {
    var one : Int
    var two : Int
    
    private enum CodingKeys: String,CodingKey {
        case one = "1"
        case two = "2"
    }
}

struct InputMachineFee : Codable{
    var one : Int
    var two : Int
    private enum CodingKeys: String,CodingKey {
        case one = "1"
        case two = "2"
    }
}
struct UpKeepData : Codable{
    var statuses : [Status]
}

struct UpKeepTask : Codable {
    var statuses : [Status]
}

struct Status : Codable {
    var key : String?
    var value : String?
    
    var color : String?
    
    func getString() -> String? {
        if UserDefaults.standard.getConvertedLanguage() == "en"{
            return key
        }
        return value
    }
}


struct GetStatus {
    func getCollectStatus() -> [Int:String] {
        return [0:"cash",1:"transfer",2:"check"]
    }
    func getUpKeepStatus() -> [Status]{
        if let settingData = UserDefaults.standard.getSettingData() , let setting = try? JSONDecoder().decode(SettingData.self, from: settingData) {
            
            return setting.upkeep?.statuses ?? []
        }
        return []
    }
    func getUpKeep(text:String) -> Status? {
        let status_arr = self.getUpKeepStatus()
        return status_arr.first { (st) -> Bool in
            return st.key == text
        }
    }
    
    func getUpKeepTaskStatus() -> [Status] {
        if let settingData = UserDefaults.standard.getSettingData() , let setting = try? JSONDecoder().decode(SettingData.self, from: settingData) {
            
            return setting.upkeeptask?.statuses ?? []
        }
        return []
    }
    
    func getUpkeepOrderStatus() -> UpKeepOrder? {
        if let settingData = UserDefaults.standard.getSettingData() , let setting = try? JSONDecoder().decode(SettingData.self, from: settingData) {
            
            return setting.upkeeporder
        }
        return nil
    }
    
    func getInterviewStatus() -> [Status] {
        if let settingData = UserDefaults.standard.getSettingData() , let setting = try? JSONDecoder().decode(SettingData.self, from: settingData) {
            
            return setting.interview?.statuses ?? []
        }
        return []
    }
}


struct SelectionModel {
    var key : String
    var value : String
}
