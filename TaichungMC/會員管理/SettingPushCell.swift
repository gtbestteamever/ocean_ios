//
//  SettingPushCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/5.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
enum SettingMode {
    case Push
    case Sign
    case Connection
}
class SettingPushCell: UITableViewCell {
    let label = UILabel()
    let pushswitch = UISwitch()
    let buttonimage = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next").withRenderingMode(.alwaysTemplate))
    let connectionlabel = UILabel()
    var mode:SettingMode? {
        didSet{
            switch self.mode {
            case .Push:
                self.pushswitch.isHidden = false
                self.buttonimage.isHidden = true
                self.connectionlabel.isHidden = true
            case .Sign:
                self.pushswitch.isHidden = true
                self.buttonimage.isHidden = false
                self.connectionlabel.isHidden = true
            case .Connection:
                self.pushswitch.isHidden = true
                self.buttonimage.isHidden = true
                self.connectionlabel.isHidden = false
            case .none:
                ()
            }
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(label)
        label.text = "推播通知"
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 38.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
        
        contentView.addSubview(pushswitch)
        pushswitch.anchor(top: nil, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        pushswitch.centerYInSuperview()
        pushswitch.onTintColor = #colorLiteral(red: 0.2697770298, green: 0.2701570988, blue: 1, alpha: 1)
        pushswitch.backgroundColor = #colorLiteral(red: 0.4666113853, green: 0.4666975737, blue: 0.4666124582, alpha: 1)
        layoutIfNeeded()
        pushswitch.layer.cornerRadius = pushswitch.frame.height/2
        pushswitch.isHidden = true
        
        contentView.addSubview(buttonimage)
        buttonimage.tintColor = #colorLiteral(red: 0.5920885801, green: 0.5921953917, blue: 0.5920897126, alpha: 1)
        buttonimage.anchor(top: nil, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        buttonimage.centerYInSuperview()
        buttonimage.isHidden = true
        contentView.addSubview(connectionlabel)
        connectionlabel.text = "良好"
        connectionlabel.textColor = #colorLiteral(red: 0.2697770298, green: 0.2701570988, blue: 1, alpha: 1)
        connectionlabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        connectionlabel.anchor(top: nil, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 32.calcvaluex()))
        connectionlabel.centerYInSuperview()
        connectionlabel.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
