//
//  SettingController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import FirebaseMessaging
import Firebase
class SettingController: UIViewController,UIGestureRecognizerDelegate {
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view?.isDescendant(of: tableview) ?? false {
            return false
        }
        if let cr = currency_view {
            if touch.view?.isDescendant(of: cr) ?? false{
                return false
            }
        }
        return true
    }

    let container = UIView()
    var containeranchor:AnchoredConstraints!
    let versionlabel = UILabel()
    let logoutbutton = UIButton(type: .system)
    var originCon:ViewController?
    let tableview = UITableView(frame: .zero, style: .grouped)
    var currenyModel = [SelectionModel]()
    var groupModel = [SelectionModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bannerimageview = UIImageView(image: #imageLiteral(resourceName: "ic_logo"))
        bannerimageview.contentMode = .scaleAspectFit

        view.addSubview(container)
         
         container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
     //   if UIDevice().userInterfaceIdiom == .pad {
         containeranchor = container.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -432.calcvaluex()),size: .init(width: 432.calcvaluex(), height: 0))
//        }
//        else{
//            containeranchor = container.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -300.calcvaluex()),size: .init(width: 300.calcvaluex(), height: 0))
//        }
         
         versionlabel.text = "v2 5.8.0"
         versionlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
         
         
         view.addSubview(versionlabel)
         versionlabel.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
             versionlabel.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0))

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismisscontroller))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
        
        view.addSubview(logoutbutton)
        logoutbutton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        logoutbutton.setTitle("登出".localized, for: .normal)
        logoutbutton.setTitleColor(.white, for: .normal)
        logoutbutton.addshadowColor(color: #colorLiteral(red: 0.6809694171, green: 0.6804885268, blue: 0.6894040108, alpha: 1))
        logoutbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        logoutbutton.anchor(top: nil, leading: container.leadingAnchor, bottom: versionlabel.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 20.calcvaluex(), bottom: 12.calcvaluey(), right: 20.calcvaluex()),size: .init(width: 0, height: 48.calcvaluey()))
        
        logoutbutton.layer.cornerRadius = 48.calcvaluey()/2
        logoutbutton.addTarget(self, action: #selector(loggingOut), for: .touchUpInside)
        view.addSubview(bannerimageview)
        bannerimageview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 40.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        view.addSubview(tableview)
        tableview.backgroundColor = .clear
        //tableview.rowHeight = 80.calcvaluey()
        tableview.anchor(top: bannerimageview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 100.calcvaluey(), right: 0))
        tableview.showsVerticalScrollIndicator = false
        tableview.dataSource = self
        tableview.delegate = self
        //tableview.isScrollEnabled = false
        if #available(iOS 11.0, *) {
            tableview.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.getUserData()
    }
    var user : UserD? {
        didSet{

            self.tableview.reloadData()
        }
    }
    var currency_view : SelectCurrencyView?
    func getUserData(){
        let data = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId())
        
        if let data = data,let json = try? JSONDecoder().decode(UserData.self, from: data) {
            self.user = json.data
        }
        currenyModel = []
        groupModel = []
        for (key,val) in self.user?.currencies.first ?? [:] {
            currenyModel.append(SelectionModel(key: key, value: val))
        }
        
        for (key,val) in self.user?.currency_groups.first ?? [:] {
            groupModel.append(SelectionModel(key: key, value: val))
        }
    }

    @objc func loggingOut(){
        
        Messaging.messaging().deleteToken { (_) in
            //
            NetworkCall.shared.postCall(parameter: "api-victor/v1/auth/logout", dict: [:], decoderType: Logout.self) { (logout) in
                if let log = logout,log.data {
                    DispatchQueue.main.async {
                        UserDefaults.standard.saveToken(token: "")
                        UserDefaults.standard.removeAllKey()
                        self.slideOff()
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                        keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }



    }
    func slideOff(){
        containeranchor.width?.constant = 0

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.dismiss(animated: false, completion: nil)
            }

    }
    @objc func dismisscontroller(gesture:UITapGestureRecognizer){
        let p = gesture.location(in: self.view)
        if !container.frame.contains(p) {
            slideOff()
        
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containeranchor.trailing?.constant = 0
        
         UIView.animate(withDuration: 0.4) {
             self.view.layoutIfNeeded()
         }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.setAnimationsEnabled(true)
    }
}
extension SettingController:SelectCurrencyViewDelegate {
    func didSelectCurrency(currency: String,index:Int) {
        
        if index == 1{
           if let key = currenyModel.first(where: { (st) -> Bool in
                return st.value == currency
           })?.key {
            UserDefaults.standard.saveChosenCurrency(text: key)
           }
        
        }
        else{
           if let key = groupModel.first(where: { (st) -> Bool in
                return st.value == currency
           })?.key {
            UserDefaults.standard.saveChosenGroup(text: key)
           }
           
        }
        removeCurrencyView()
        
        
        self.tableview.reloadData()
        
        
    }
}
extension SettingController : CurrencyFieldsDelegate {
    func showCurrencies(textField:UITextField) {
        addCurrencyView(pendView:textField)
    }
    
    
    func addCurrencyView(pendView:UIView){
        if currency_view == nil, let user = user{
            let currencies = user.currencies
            let group = user.currency_groups
            print(currencies,group)
            var st = [String]()
            
            if pendView.tag == 1{
                for (_,val) in currencies.first ?? [:]{
                    st.append(val)
                }
               
            }
            else{
                for (_,val) in group.first ?? [:]{
                    st.append(val)
                }
                
            }
            
            currency_view = SelectCurrencyView(currencies: st,delegate: self)
            currency_view?.tag = pendView.tag
            view.addSubview(currency_view!)
            currency_view?.anchor(top: pendView.bottomAnchor, leading: pendView.leadingAnchor, bottom: nil, trailing: pendView.trailingAnchor)
            let heightAnchor = currency_view?.heightAnchor.constraint(equalToConstant: 0)
            heightAnchor?.isActive = true
            self.view.layoutIfNeeded()
            
            let height = CGFloat(st.count) * 40.calcvaluey()
            heightAnchor?.constant = height
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            
            

            
        }
        else{
            removeCurrencyView()
            addCurrencyView(pendView: pendView)
        }
    }
    
    func removeCurrencyView(){
        currency_view?.removeFromSuperview()
        currency_view = nil
    }
}
extension SettingController:UITableViewDelegate,UITableViewDataSource,SettingLanguageDelegate {
    func sendLang(tag: Int) {
        print(881,tag)
        if tag == 0 {
            UserDefaults.standard.saveAppLanguage(lang: "zh-TW")
        }
        else{
            UserDefaults.standard.saveAppLanguage(lang: "en")
        }
        logoutbutton.setTitle("登出".localized, for: .normal)
        tableview.reloadData()
        NotificationCenter.default.post(name: Notification.Name(rawValue : "switchLanguage"), object: nil)
        
        let id = UserDefaults.standard.getUserId()
        let lang = UserDefaults.standard.getConvertedLanguage()
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/\(id)?language=\(lang)", decoderType: UserData.self) { json in
            //
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = SettingLanguageCell(style: .default, reuseIdentifier: "Language")
            cell.delegate = self
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
            
        }
        else if indexPath.row == 1 {
            let cell = SettingCurrencyLabel(style: .default, reuseIdentifier: "cell3")
            cell.delegate = self
//            if indexPath.row == 1{
//                cell.currency.tag = 0
//                cell.pushswitch.isHidden = true
//                cell.topLabel.text = "群組".localized
//                cell.currency.text = groupModel.first(where: { (st) -> Bool in
//                    return st.key == UserDefaults.standard.getChosenGroup()
//                })?.value
//            }
//            else{
                cell.currency.tag = 1
                cell.pushswitch.isHidden = false
                cell.topLabel.text = "產品價格顯示".localized
                cell.currency.text = currenyModel.first(where: { (st) -> Bool in
                    return st.key == UserDefaults.standard.getChosenCurrency()
                })?.value
           // }
            cell.pushswitch.isOn = UserDefaults.standard.getShowPrice()

            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = SettingPushCell(style: .default, reuseIdentifier: "cell")
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            
            if indexPath.row == 2{
                cell.mode = .Push
                cell.label.text = "推播通知".localized
            }
            
            else if indexPath.row == 2{
                cell.mode = .Sign
                cell.label.text = "簽名檔設定"
            }
            else if indexPath.row == 2{
                cell.mode = .Push
                cell.label.text = "產品價格顯示"
            }
            else{
                cell.mode = .Connection
                cell.label.text = "網路環境"
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 142.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        vd.backgroundColor = .clear
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user"))
        vd.addSubview(imageview)
        imageview.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 38.calcvaluex(), bottom: 28.calcvaluey(), right: 0),size: .init(width: 66.calcvaluex(), height: 66.calcvaluey()))
        let topheadertitle = UILabel()
        vd.addSubview(topheadertitle)
        topheadertitle.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 7.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 0))
        topheadertitle.text = user?.account?.name
        topheadertitle.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        let sublabeltitle = UILabel()
        sublabeltitle.text = user?.name
        sublabeltitle.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        vd.addSubview(sublabeltitle)
        sublabeltitle.anchor(top: topheadertitle.bottomAnchor, leading: topheadertitle.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        let sublabelrighttitle = UILabel()
        sublabelrighttitle.text = user?.mainrole_name
        sublabelrighttitle.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        vd.addSubview(sublabelrighttitle)
        sublabelrighttitle.anchor(top: topheadertitle.bottomAnchor, leading: sublabeltitle.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 10.calcvaluex(), bottom: 0, right: 0))
        return vd
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 124.calcvaluey()
        }
        else if indexPath.row == 2 || indexPath.row == 1{
            return 122.calcvaluey()
        }
        return 80.calcvaluey()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        UIView.setAnimationsEnabled(false)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(661)
        removeCurrencyView()
    }
}
extension UIImageView {
    
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let new = CGRect(x: self.bounds.origin.x-25, y: self.bounds.origin.y-25, width: self.bounds.size.width+50, height: self.bounds.height+50)
        return new.contains(point)
    }
}
