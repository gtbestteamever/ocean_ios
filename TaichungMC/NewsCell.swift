//
//  NewsCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/1/6.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension UIView {
    func addShadow(corners: UIRectCorner, radius: CGFloat) {
       self.backgroundColor = UIColor.clear
       let roundedShapeLayer = CAShapeLayer()
       let roundedMaskPath = UIBezierPath(roundedRect: self.bounds,
                                         byRoundingCorners: corners,
                                         cornerRadii: CGSize(width: radius, height: radius))

       roundedShapeLayer.frame = self.bounds
       roundedShapeLayer.fillColor = UIColor.white.cgColor
       roundedShapeLayer.path = roundedMaskPath.cgPath

       self.layer.insertSublayer(roundedShapeLayer, at: 0)

       self.layer.shadowOpacity = 0.4
       self.layer.shadowOffset = CGSize(width: -0.1, height: 4)
       self.layer.shadowRadius = 3
       self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
     }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
class NewsCell: UICollectionViewCell {
    let header = UILabel()
    let subheader = UILabel()
    let colorview = UIView()
    var didscroll = false {
        didSet{
            if self.didscroll {
                colorview.isHidden = false
                clipsToBounds = true
            }
            else{
                colorview.isHidden = true
                clipsToBounds = false
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        layer.masksToBounds = true
        addSubview(colorview)
        colorview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        colorview.isHidden = true
        //colorview.layer.cornerRadius = 12
        colorview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 10.xscalevalue(), height: 0))
        //colorview.roundCorners(corners: [.topLeft,.bottomLeft], radius: 12)
        header.text = "108年度股東常會召"
        header.font = UIFont.boldSystemFont(ofSize: 25)
        //header.backgroundColor = .green
        header.constrainHeight(constant: header.text!.height(withConstrainedWidth: .greatestFiniteMagnitude, font: header.font))

        subheader.text = "10月20日 下午3:25"
        subheader.font = UIFont(name: "HelveticaNeue-Thin", size: 22)
        //subheader.backgroundColor = .red
        subheader.constrainHeight(constant: subheader.text!.height(withConstrainedWidth: .greatestFiniteMagnitude, font: subheader.font))
        let stackview = UIStackView(arrangedSubviews: [UIView(),header,subheader,UIView()])
        stackview.axis = .vertical
        stackview.alignment = .leading
        stackview.spacing = 8
        stackview.distribution = .fillProportionally
        
        
        addSubview(stackview)
        stackview.centerYInSuperview()
        stackview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 64, bottom: 0, right: 64))
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        colorview.roundCorners(corners: [.topLeft,.bottomLeft], radius: 36)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
