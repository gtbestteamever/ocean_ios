//
//  NewsController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import WebKit
class NewsController: SampleController {
    let leftview = UIView()
    let searchText = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var leftviewanchor:AnchoredConstraints!
    
    var prevIndex : Int? = nil
    var rightView = UIView()

    var rightViewAnchor:AnchoredConstraints!
    var rightTopViewShadow = UIView()
    var rightTopView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let bottomView = UIView()
    let bottomWebView = WKWebView()
    let expandPdfButton = UIButton(type: .custom)
    let goToPageButton = UIButton(type: .custom)
    var data = [NewsMessage]()
    var bottomViewAnchor:AnchoredConstraints?
    let indicator = JGProgressHUD()
    override func changeLang() {
        super.changeLang()
        fetchApi()
        //titleview.label.text = titleview.label.text?.localized
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        settingButton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        background_view.image = #imageLiteral(resourceName: "background_news")
        titleview.label.text = "最新消息".localized
        //titleviewanchor.leading?.constant = 350.calcvaluex()
        view.addSubview(leftview)

        leftviewanchor = leftview.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: menu_bottomInset + 20.calcvaluey(), right: 0),size: .init(width: 402.calcvaluex(), height: 0))
        
        
//        leftview.addSubview(searchText)
//        searchText.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
//        searchText.anchor(top:leftview.topAnchor , leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 10.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
//        searchText.layer.cornerRadius = 46.calcvaluey()/2
//        searchText.addShadowColor(opacity: 0.05)
//        searchText.attributedPlaceholder = NSAttributedString(string: "搜尋最新消息", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        
        leftview.addSubview(tableview)
        tableview.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: leftview.bottomAnchor, trailing: leftview.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        
        
        view.addSubview(rightView)
        let stackview = Vertical_Stackview(spacing:20.calcvaluey(),alignment: .fill)
        stackview.addArrangedSubview(rightTopView)
        stackview.addArrangedSubview(bottomWebView)
        rightView.backgroundColor = .white
        rightView.isHidden = true
        rightView.layer.cornerRadius = 15.calcvaluex()
        rightView.addshadowColor()
        rightView.anchor(top: tableview.topAnchor, leading: tableview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 25.calcvaluex(), bottom: menu_bottomInset + 20.calcvaluey(), right:25.calcvaluex()),size: .init(width: 541.calcvaluex(), height: 0))
        
        rightView.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 20.calcvaluey(), left: 20.calcvaluex(), bottom: 20.calcvaluey(), right: 20.calcvaluex()))
        rightTopView.constrainHeight(constant: 74.calcvaluey())
//        rightView.addSubview(rightTopView)
//
//        rightTopView.anchor(top: rightView.topAnchor, leading: rightView.leadingAnchor, bottom: nil, trailing: rightView.trailingAnchor,padding: .init(top: 30.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 74.calcvaluey()))
        rightTopView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)

        rightTopView.layer.cornerRadius = 15.calcvaluex()

        rightTopView.showsHorizontalScrollIndicator = false
        
        rightTopView.delegate = self
        rightTopView.dataSource = self
        (rightTopView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        rightTopView.register(NewsVideoCell.self, forCellWithReuseIdentifier: "cell")
        rightTopView.contentInset = .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex())
        
        
//        rightView.addSubview(bottomWebView)
//        bottomWebView.anchor(top: rightTopView.bottomAnchor, leading: rightTopView.leadingAnchor, bottom: rightView.bottomAnchor, trailing: rightTopView.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 10.calcvaluex(), bottom: 10.calcvaluey(), right: 10.calcvaluex()))
   

//        bottomWebView.scrollView.isScrollEnabled = false
        bottomWebView.navigationDelegate = self
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        view.addSubview(expandPdfButton)

        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        

        rightView.addSubview(expandPdfButton)
        expandPdfButton.isHidden = true
        expandPdfButton.anchor(top: rightView.topAnchor, leading: nil, bottom: nil, trailing: rightView.trailingAnchor,padding: .init(top: 562.calcvaluey(), left: 0, bottom: 0, right: 44.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        
                goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
                goToPageButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
                rightView.addSubview(goToPageButton)
        
                goToPageButton.layer.cornerRadius = 38.calcvaluex()/2
        goToPageButton.addTarget(self, action: #selector(turnPage), for: .touchUpInside)
        goToPageButton.anchor(top: expandPdfButton.bottomAnchor, leading: nil, bottom: nil, trailing: expandPdfButton.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        goToPageButton.isHidden = true
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    
    func fetchApi(){
        self.tableview.isHidden = true
        let lang = UserDefaults.standard.getConvertedLanguage()
        print(771,lang)
        NetworkCall.shared.getCall(parameter: "api-or/v1/message_categories?language=\(lang)&code=news", decoderType: NewsClas.self) { (json) in
            DispatchQueue.main.async {
                if (json?.data.count ?? 0) > 0{
                    self.data = json?.data.first?.messages ?? []
                    self.tableview.isHidden = false
                self.tableview.reloadData()
                }
            }
            

        }
    }
    @objc func turnPage(){
        let con = TurnToViewController()
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }
    @objc func expandPDF(){
        let con = ExpandFileController()
        if let url = URL(string: expandFile) {
            url.loadWebview(webview: con.imgv)
        }
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }
    func hideView(){
        self.rightTopView.isHidden = true
        self.bottomWebView.isHidden = true
        self.bottomView.isHidden = true
        self.expandPdfButton.isHidden = true
    }
    func unHideView(){
        self.rightTopView.isHidden = false
        self.bottomWebView.isHidden = false
        self.bottomView.isHidden = false
        self.expandPdfButton.isHidden = false
    }
    override func popview() {
        prevIndex = nil
        self.tableview.reloadData()
        self.view.layoutIfNeeded()
        
        originCon?.showcircle()
        
        leftviewanchor.leading?.constant = 333.calcvaluex()
        leftviewanchor.trailing?.constant = 0
         
        UIView.animate(withDuration: 0.5, animations: {
           
            self.view.layoutIfNeeded()
        }) { (_) in
            self.hideView()
        }
        
        
    }
    
    var data_List = [String]() {
        didSet{
            self.rightTopView.reloadData()
        }
    }
    var expandFile : String = ""
}

extension NewsController:UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WKNavigationDelegate{
//    func showVideo(index:Int) {
//        let con = ExpandVideoView()
//        if let url = URL(string: data_List[index]) {
//            let request = URLRequest(url: url)
//            con.wideView.loadRequest(request)
//        }
//
//        con.modalPresentationStyle = .overFullScreen
//        self.present(con, animated: false, completion: nil)
//    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicator.dismiss()
        removeShadow(webView: webView)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicator.show(in: rightView)
    }
//    func webViewDidFinishLoad(_ webView: WKWebView) {
//        //bottomWebView.isHidden = false
//        indicator.dismiss()
//        removeShadow(webView: webView)
//
//
//
//    }
//    func webViewDidStartLoad(_ webView: WKWebView) {
//        indicator.show(in: rightView)
//        //bottomWebView.isHidden = true
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data_List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NewsVideoCell
        cell.tag = indexPath.item
        let data = data_List[indexPath.item]
        if let url = URL(string: data) {
            if url.pathExtension.lowercased() == "mp4" {
                cell.button.isHidden = false
            }
            else{
                cell.button.isHidden = true
            }
            if url.pathExtension.lowercased() == "pdf" {
                cell.imgv.image = Thumbnail().pdfThumbnail(url: url, width: 123.calcvaluex())
            }
            else if url.pathExtension.lowercased() == "mp4" {
                cell.imgv.image = Thumbnail().getThumbnailImage(forUrl: url)
            }
            else{
                cell.imgv.sd_setImage(with: url, completed: nil)
            }
        }
       // cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 123.calcvaluex(), height: 74.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let con = ExpandFileController()
        if let url = URL(string: data_List[indexPath.item]) {
            print(11,url.absoluteString)
            url.loadWebview(webview: con.imgv)
//            let request = URLRequest(url: url)
//
//            con.webview.loadRequest(request)
        }
        
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NNewsCell(style: .default, reuseIdentifier: "cell")
        cell.topLabel.text = data[indexPath.row].title
        cell.subLabel.text = data[indexPath.row].date
        if let prev = prevIndex {
            if indexPath.row == prev {
                cell.setColor()
            }
            else{
                cell.unsetColor()
            }
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        rightView.isHidden = false
        data_List = data[indexPath.row].get_SubValue()
        if data_List.count == 0{
            
            rightTopView.isHidden = true
        }
        else{
            
            rightTopView.isHidden = false
        }
        if let url =  data[indexPath.row].getURL() {
            print(771,url.absoluteString)
            expandFile = url.absoluteString
            
            url.loadWebview(webview: bottomWebView)

        }
        else{
            expandFile = "about:blank"
            let req = URLRequest(url: URL(string: "about:blank")!)
            
            bottomWebView.load(req)
        }
        

        prevIndex = indexPath.row
        self.tableview.reloadData()
//        originCon?.hidecircle()
//
//        leftviewanchor.leading?.constant = 26.calcvaluex()
//        leftviewanchor.trailing?.constant = -628.calcvaluex()
//
//        self.unHideView()
//
//        UIView.animate(withDuration: 0.5, animations: {
//
//            self.view.layoutIfNeeded()
//        }) { (_) in
//            self.tableview.reloadData()
//
//        }
    }
    
    func removeShadow(webView: WKWebView) {

        for subview:UIView in webView.scrollView.subviews {
            
            subview.layer.shadowOpacity = 0
            for subsubview in subview.subviews {
                
                subsubview.layer.shadowOpacity = 0
            }
        }
    }
    
    
}
protocol NewsDelegate {
    func showVideo(index:Int)
}
class NewsVideoCell: UICollectionViewCell {
    let imgv = UIImageView()
    let button = UIButton(type: .custom)
    var delegate:NewsDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(imgv)
        imgv.clipsToBounds = true
        imgv.contentMode = .scaleAspectFill
        
        imgv.fillSuperview()
        imgv.layer.borderWidth = 1.calcvaluex()
        imgv.layer.borderColor = #colorLiteral(red: 0.8893501457, green: 0.8893501457, blue: 0.8893501457, alpha: 1)
        
        addSubview(button)
        button.setImage(#imageLiteral(resourceName: "ic_video_piay"), for: .normal)
        button.contentHorizontalAlignment = .fill
        button.contentVerticalAlignment = .fill

        button.centerInSuperview(size: .init(width: 48.calcvaluex(), height: 48.calcvaluex()))
//        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showVideo)))

    }
//    @objc func showVideo(){
//        delegate?.showVideo(index:self.tag)
//    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NNewsCell:UITableViewCell{
    let vd = UIView()
    let topLabel = UILabel()
    let mc = UIView()
       let nd = UIView()
    let subLabel = UILabel()
    let selected_image = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
                contentView.addSubview(mc)
                mc.backgroundColor = .clear
                mc.addShadowColor(opacity: 0.05)
                mc.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 92.calcvaluey()))
        mc.centerYInSuperview()
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.clipsToBounds = true
        mc.addSubview(vd)
        vd.fillSuperview()
        
        vd.addSubview(nd)
         nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
         nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        
        topLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        topLabel.text = "108年度股東常會召"
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        nd.addSubview(topLabel)
        topLabel.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 53.calcvaluey()/2))
        
        subLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        subLabel.text = "10月20日 下午3:25"
        subLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(subLabel)
        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 53.calcvaluey()/2))
        
        vd.addSubview(selected_image)
        selected_image.contentMode = .scaleAspectFill
        selected_image.anchor(top: nil, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 14.calcvaluex()),size: .init(width: 17.calcvaluex(), height: 17.calcvaluex()))
        selected_image.centerYInSuperview()
        selected_image.isHidden = true
    }
    func setColor(){
        selected_image.isHidden = false
//        vd.backgroundColor = MajorColor().mainColor
//        mc.addShadowColor(opacity: 0.3)
    }
    func unsetColor(){
        selected_image.isHidden = true
//        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
//        mc.addShadowColor(opacity: 0.1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
