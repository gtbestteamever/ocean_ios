//
//  iPhoneNewsContentController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/20/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
extension iPhoneNewsContentController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data_List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NewsVideoCell
        cell.tag = indexPath.item
        let data = data_List[indexPath.item]
        if let url = URL(string: data) {
            if url.pathExtension.lowercased() == "mp4" {
                cell.button.isHidden = false
            }
            else{
                cell.button.isHidden = true
            }
            if url.pathExtension.lowercased() == "pdf" {
                cell.imgv.image = Thumbnail().pdfThumbnail(url: url, width: 123.calcvaluex())
            }
            else if url.pathExtension.lowercased() == "mp4" {
                cell.imgv.image = Thumbnail().getThumbnailImage(forUrl: url)
            }
            else{
                cell.imgv.sd_setImage(with: url, completed: nil)
            }
        }
       // cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 123.calcvaluex(), height: 74.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        if let url = URL(string: data_List[indexPath.item]) {
            OpenFile().open(url: url)
        }

    }
}
class iPhoneNewsContentController : SampleController {
    let rightView = UIView()
    var rightTopView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let bottomWebView = WKWebView()
    let expandPdfButton = UIButton(type: .custom)
    var data : NewsMessage?
    var expandFile : String = ""
    var data_List = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        titleview.label.text = "最新消息".localized
        view.addSubview(rightView)
        let stackview = Vertical_Stackview(spacing:20.calcvaluey(),alignment: .fill)
        stackview.addArrangedSubview(rightTopView)
        
        stackview.addArrangedSubview(bottomWebView)
        rightView.backgroundColor = .white
        
        rightView.layer.cornerRadius = 15.calcvaluex()
        rightView.addshadowColor()
        rightView.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 20.calcvaluey(), right:0))
        
        rightView.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 20.calcvaluey(), left: 20.calcvaluex(), bottom: 20.calcvaluey(), right: 20.calcvaluex()))
        rightTopView.constrainHeight(constant: 74.calcvaluey())
//        rightView.addSubview(rightTopView)
//
//        rightTopView.anchor(top: rightView.topAnchor, leading: rightView.leadingAnchor, bottom: nil, trailing: rightView.trailingAnchor,padding: .init(top: 30.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 74.calcvaluey()))
        rightTopView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)

        rightTopView.layer.cornerRadius = 15.calcvaluex()

        rightTopView.showsHorizontalScrollIndicator = false
        
        rightTopView.delegate = self
        rightTopView.dataSource = self
        (rightTopView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        rightTopView.register(NewsVideoCell.self, forCellWithReuseIdentifier: "cell")
        rightTopView.contentInset = .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex())
        
        

     //   bottomWebView.delegate = self
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        view.addSubview(expandPdfButton)

        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        

        rightView.addSubview(expandPdfButton)
        expandPdfButton.isHidden = true
        expandPdfButton.anchor(top: nil, leading: nil, bottom: rightView.bottomAnchor, trailing: rightView.trailingAnchor,padding: .init(top: 562.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 24.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        
        
        data_List = data?.get_SubValue() ?? []
        if data_List.count == 0{
            
            rightTopView.isHidden = true
        }
        else{
            
            rightTopView.isHidden = false
            self.rightTopView.reloadData()
        }
        if let url =  data?.getURL() {
            print(771,url.absoluteString)
            expandFile = url.absoluteString
            
            url.loadWebview(webview: bottomWebView)

        }
        else{
            expandFile = "about:blank"
            let req = URLRequest(url: URL(string: "about:blank")!)
            
            bottomWebView.load(req)
        }
    }
    @objc func expandPDF(){
        
        if let url = URL(string: expandFile) {
            OpenFile().open(url: url)
        }

    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}
