//
//  iPhoneNewsController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/19/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit

class iPhoneNewsCell : UITableViewCell {
    let container = UIView()
    let topLabel = UILabel()
    let subLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        self.backgroundColor = .clear
       
        container.backgroundColor = .white
        container.layer.cornerRadius = 8.calcvaluex()
        container.addshadowColor()
    
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 1.calcvaluey(), left: 1.calcvaluex(), bottom: 12.calcvaluey(), right: 1.calcvaluex()))
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 90.calcvaluey()).isActive = true
        
        container.addSubview(topLabel)
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 18.calcvaluex(), bottom: 0, right: 18.calcvaluex()))
        topLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        topLabel.numberOfLines = 0
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        subLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        
        subLabel.textColor = .lightGray
        //subLabel.text = "10月"
        container.addSubview(subLabel)
        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: container.bottomAnchor, trailing: topLabel.trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension iPhoneNewsController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! iPhoneNewsCell
        cell.topLabel.text = data[indexPath.row].title
        cell.subLabel.text = data[indexPath.row].date
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vd = iPhoneNewsContentController()
        vd.modalPresentationStyle = .fullScreen
        vd.data = self.data[indexPath.row]
        self.present(vd, animated: true, completion: nil)
    }
}
class iPhoneNewsController : SampleController {
    let tableview = UITableView(frame: .zero,style: .grouped)
    var data = [NewsMessage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "最新消息".localized
        
        view.addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: menu_bottomInset + 8.calcvaluey(), right: 0))
        tableview.register(iPhoneNewsCell.self, forCellReuseIdentifier: "cell")
        tableview.showsVerticalScrollIndicator = false
        tableview.delegate = self
         tableview.dataSource = self
        tableview.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    
    func fetchApi(){
        self.tableview.isHidden = true
        let lang = UserDefaults.standard.getConvertedLanguage()
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/message_categories?language=\(lang)&code=news", decoderType: NewsClas.self) { (json) in
            DispatchQueue.main.async {
                if (json?.data.count ?? 0) > 0{
                    self.data = json?.data.first?.messages ?? []
                    self.tableview.isHidden = false
                self.tableview.reloadData()
                }
            }
            

        }
    }
}
