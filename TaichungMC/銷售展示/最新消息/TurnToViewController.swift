//
//  TurnToViewController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/28.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class TurnToViewController: UIViewController {
    let vd = UIView()
    let label = UILabel()
    let form = SalesFormView(text: "頁碼")
    let button = UIButton(type: .custom)
    let xbutton = UIButton(type: .custom)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addShadowColor(opacity: 0.1)
        vd.layer.cornerRadius = 15.calcvaluex()
        view.addSubview(vd)
        vd.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 287.calcvaluey()))
        
        label.text = "跳轉頁面"
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
        label.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        
        label.centerXInSuperview()
        
        vd.addSubview(form)
        form.anchor(top: label.bottomAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 43.calcvaluex()))
        
        form.textfield.attributedPlaceholder = NSAttributedString(string: "請輸入頁碼 1-4", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)])
        
        button.setTitle("確認", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 24.calcvaluey()
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        button.backgroundColor = MajorColor().mainColor
        button.addShadowColor(opacity: 0.1)
        vd.addSubview(button)
        button.anchor(top: nil, leading: nil, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        button.centerXInSuperview()
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        
        vd.addSubview(xbutton)
        xbutton.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
    }
    @objc func closeView(){
        self.dismiss(animated: false, completion: nil)
    }
}
