//
//  BussinessController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/28.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class BussinessController: NewsController,BussinessDelegate {
    func showBusiness(index: Int,first:Bool) {
        innerIndex = index
        
        let requrl = URLRequest(url: URL(string: "about:blank")!)
        videoView.load(requrl)
        if let url = topData[prevIndex ?? 0].messages?[innerIndex].getURL() {
            print(url.absoluteString)
            url.loadWebview(webview: videoView)
        }
        UIView.setAnimationsEnabled(false)
        if !first{
        self.tableview.reloadData()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            UIView.setAnimationsEnabled(true)
        }
       
    }
    
    let videoView = WKWebView()
    var innerIndex = 0
    override func changeLang() {
        super.changeLang()
        fetchNewApi()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        background_view.image = #imageLiteral(resourceName: "background_afterservice1")
        settingButton.isHidden = false
        titleview.label.text = "應用產業".localized
        rightTopViewShadow.removeFromSuperview()
        bottomView.removeFromSuperview()
        expandPdfButton.removeFromSuperview()
        goToPageButton.removeFromSuperview()
        
        rightView.addSubview(videoView)
        videoView.isHidden = true
       
        videoView.clipsToBounds = true
        videoView.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 18.calcvaluex(), bottom: 26.calcvaluey(), right: 18.calcvaluex()))
        
        
    }
    var topData = [News]()
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(animated)
        fetchNewApi()

    }
    func fetchNewApi(){
        let lang = UserDefaults.standard.getConvertedLanguage()
        NetworkCall.shared.getCall(parameter: "api-or/v1/message_categories?language=\(lang)&code=application", decoderType: NewsClas.self) { (json) in
            DispatchQueue.main.async {
                if (json?.data.count ?? 0) > 0{
                    self.data = json?.data.first?.messages ?? []
                    
                    self.tableview.reloadData()
                }
               
            }
            

        }
    }
    override func hideView() {
        super.hideView()
        videoView.isHidden = true
    }
    override func unHideView() {
        super.unHideView()
        videoView.isHidden = false
    }
    func getDataHeight(messages:[NewsMessage]) -> CGFloat {
        
        print(messages.count)
        if messages.count == 0{
            return 90.calcvaluey()
        }
        
        let preHeight = 185.calcvaluey()
        let messageHeight = CGFloat(messages.count) * 25.calcvaluey()
        
        return preHeight + messageHeight
        
    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if prevIndex == indexPath.row {
//            let filter = topData[indexPath.row].messages ?? []
//            return getDataHeight(messages: filter)
//        }
//        return 90.calcvaluey()
//    }
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return topData.count
//    }
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//            let cell = BussinessCell(style: .default, reuseIdentifier: "cell")
//        cell.topLabel.text = topData[indexPath.row].titles?.getLang()
//        cell.messages = topData[indexPath.row].messages ?? []
//        cell.delegate = self
//        cell.prevIndex = innerIndex
//            if let prev = prevIndex {
//                if indexPath.row == prev {
//                    cell.setColor()
//                    cell.addDownView()
//                    
//                }
//                else{
//                    cell.unsetColor()
//                    cell.removeDownView()
//                    
//                }
//            }
//            else{
//                cell.unsetColor()
//                cell.removeDownView()
//                
//            }
//            return cell
//        
//    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        if prevIndex != indexPath.row {
//        prevIndex = indexPath.row
//        innerIndex = 0
//        originCon?.hidecircle()
//        
//        leftviewanchor.leading?.constant = 26.calcvaluex()
//        leftviewanchor.trailing?.constant = -628.calcvaluex()
//        
//        self.unHideView()
//        
//        UIView.animate(withDuration: 0.5, animations: {
//            
//            self.view.layoutIfNeeded()
//        }) { (_) in
//            self.tableview.reloadData()
//                    
//        }
//        }
//    }

}
protocol BussinessDelegate {
    func showBusiness(index:Int,first:Bool)
}
class BussinessCell:UITableViewCell{
    let vd = UIView()
    let topLabel = UILabel()
    let mc = UIView()
       let nd = UIView()
    let subLabel = UILabel()
    let downView = UIView()
    var downViewAnchor:NSLayoutConstraint!
    let stackview = UIStackView()
    var showing = false
    var downviewheight : CGFloat = 0.0
    var messages = [NewsMessage](){
        didSet{
            for (index,i) in messages.enumerated() {
                let md = VdConView(text: i.getDetails()?.name ?? "")
                md.label.text = i.title
                md.tag = index
                md.isUserInteractionEnabled = true
                md.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapping)))
                if index == prevIndex{
                    md.setColor()
                }
                else{
                    md.unsetColor()
                }
                stackview.addArrangedSubview(md)
            }
            if self.messages.count > 0{
            let preHeight = 95.calcvaluey()
            let messageHeight = 25.calcvaluey() * CGFloat(messages.count)
            downviewheight = preHeight + messageHeight
            }
        }
    }
    var prevIndex = 0 {
        didSet{
           // stackview.arrangedSubviews[]
            
            for (index,i) in stackview.arrangedSubviews.enumerated() {
                if let mt = i as? VdConView {
                if index == prevIndex {
                    mt.setColor()
                }
                else{
                    mt.unsetColor()
                    }
                    
                }
            }
        }
    }
    var delegate : BussinessDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
                contentView.addSubview(mc)
                mc.backgroundColor = .clear
                mc.addShadowColor(opacity: 0.05)
        mc.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 10.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 78.calcvaluey()))
       // mc.centerYInSuperview()
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.clipsToBounds = true
        mc.addSubview(vd)
        vd.fillSuperview()
        
        vd.addSubview(nd)
         nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
         nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        
        topLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
       // topLabel.text = "汽機車、自行車零件"
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        nd.addSubview(topLabel)
        topLabel.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        
        topLabel.centerYInSuperview()
        
//        subLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
//        subLabel.text = "10月20日 下午3:25"
//        subLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//
//        vd.addSubview(subLabel)
//        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 53.calcvaluey()/2))
        
        downView.backgroundColor = .white
        downView.layer.cornerRadius = 15.calcvaluex()
        downView.addShadowColor(opacity: 0.1)
        downView.isUserInteractionEnabled = true
        addSubview(downView)
        sendSubviewToBack(downView)
        downView.isHidden = true
        downView.anchor(top: vd.bottomAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: -15.calcvaluey(), left: 10.calcvaluex(), bottom: 0, right: 10.calcvaluex()))
        downViewAnchor = downView.heightAnchor.constraint(equalToConstant: 0)
        downViewAnchor.isActive = true
        
        
        stackview.spacing = 18.calcvaluey()
        stackview.distribution = .fillEqually
        stackview.axis = .vertical

        
        addSubview(stackview)
        stackview.anchor(top: downView.topAnchor, leading: downView.leadingAnchor, bottom: downView.bottomAnchor, trailing: downView.trailingAnchor,padding: .init(top: 37.calcvaluey(), left: 0, bottom: 22.calcvaluey(), right: 0))
        //stackview.fillSuperview(padding: .init(top: 37.calcvaluey(), left: 0, bottom: 22.calcvaluey(), right: 0))
        stackview.isHidden = true
    }
    @objc func tapping(sender:UITapGestureRecognizer){
        if let vd = sender.view {
            print(vd.tag)
            delegate?.showBusiness(index: vd.tag, first: false)
        }
        
    }
    func setColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        mc.addShadowColor(opacity: 0.3)
    }
    
    func addDownView(){

        
        if messages.count > 0 {
            delegate?.showBusiness(index: 0,first:true)
            downView.isHidden = false
            stackview.isHidden = false
        }
        self.layoutIfNeeded()
       print(338,downviewheight)

        downViewAnchor.constant = downviewheight
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
        

        
        
        
    }
    func removeDownView(){
        self.layoutIfNeeded()
        downViewAnchor.constant = 0
        
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
        }) { (_) in
            self.downView.isHidden = true
            self.stackview.isHidden = true
        }
        showing = false
    }
    func unsetColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        mc.addShadowColor(opacity: 0.1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class VdConView:UIView,UIGestureRecognizerDelegate{
    let orangeview = UIView()
    let label = UILabel()
    init(text:String) {
        super.init(frame: .zero)

        orangeview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        orangeview.layer.cornerRadius = 5.calcvaluey()
        
        addSubview(orangeview)
        orangeview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 10.calcvaluey(), height: 10.calcvaluey()))
        
        orangeview.centerYInSuperview()
        
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())

        
        addSubview(label)
        label.anchor(top: nil, leading: orangeview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        label.centerYInSuperview()

    }

    func setColor(){
        orangeview.isHidden = false
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
    }
    func unsetColor(){
        orangeview.isHidden = true
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
