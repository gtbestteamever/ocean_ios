//
//  StockContentController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class seperatorV : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.8591474891, green: 0.8592686057, blue: 0.8591094613, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FieldCell : UITableViewCell {
    let bottomSeperator = seperatorV()
    let centerSeperator = seperatorV()
    let header = UILabel()
    let value = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        selectionStyle = .none
        contentView.addSubview(bottomSeperator)
        bottomSeperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        contentView.addSubview(centerSeperator)
        centerSeperator.anchor(top: contentView.topAnchor, leading: nil, bottom: contentView.bottomAnchor, trailing: nil,size: .init(width: 1.calcvaluex(), height: 0))
        centerSeperator.centerXInSuperview()
        
        header.textColor = .black
        value.textColor = .black
        
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        value.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
        

        
        contentView.addSubview(header)
        header.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        header.centerYInSuperview()
        contentView.addSubview(value)
        value.anchor(top: nil, leading: centerSeperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        value.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turnCell : UITableViewCell {
    let sep1 = seperatorV()
    let sep2 = seperatorV()
    let sep3 = seperatorV()
    let sep4 = seperatorV()
    let header = UILabel()
    let subHeader = UILabel()
    let value1 = UILabel()
    let value2 = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
        contentView.addSubview(sep1)
        
        
        contentView.addSubview(sep2)
        
        contentView.addSubview(sep3)
        
        
        sep1.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 242.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep2.anchor(top: contentView.topAnchor, leading: sep1.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep3.anchor(top: contentView.topAnchor, leading: sep2.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
    
        contentView.addSubview(sep4)
        sep4.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 16.5.calcvaluex(), bottom: 0, right: 16.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
    
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        
        subHeader.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        subHeader.textColor = #colorLiteral(red: 0.5685855746, green: 0.5686681271, blue: 0.568559587, alpha: 1)
        
        contentView.addSubview(header)
        header.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        
        header.centerYInSuperview()
        contentView.addSubview(subHeader)
        subHeader.anchor(top: nil, leading: header.trailingAnchor, bottom: header.bottomAnchor, trailing: nil)
        
        contentView.addSubview(value1)
        contentView.addSubview(value2)
        value1.anchor(top: nil, leading: sep1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        value1.centerYInSuperview()
        
        value2.anchor(top: nil, leading: sep2.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        value2.centerYInSuperview()
        
        value1.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        value2.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        
        value1.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        value2.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turnHeader : UIView {
    let sep1 = seperatorV()
    let sep2 = seperatorV()
    let sep3 = seperatorV()
    let machineLabel = UILabel()
    let requireLabel = UILabel()
    let selectedLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9318111539, green: 0.9319418073, blue: 0.9317700267, alpha: 1)
        
        addSubview(sep1)
        
        
        addSubview(sep2)
        
        addSubview(sep3)
        
        
        sep1.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 242.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep2.anchor(top: topAnchor, leading: sep1.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep3.anchor(top: topAnchor, leading: sep2.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
    
        setLabel(label: machineLabel, text: "機台規格")
        setLabel(label: requireLabel, text: "實際成型需求")
        setLabel(label: selectedLabel, text: "目前選擇的搭配")
        
        addSubview(machineLabel)
        addSubview(requireLabel)
        addSubview(selectedLabel)
        machineLabel.anchor(top: nil, leading: sep1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        machineLabel.centerYInSuperview()
        
        requireLabel.anchor(top: nil, leading: sep2.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        requireLabel.centerYInSuperview()
        selectedLabel.anchor(top: nil, leading: sep3.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        selectedLabel.centerYInSuperview()
    }
    func setLabel(label:UILabel,text:String) {
        label.text = text
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FieldsView : UIView,UITableViewDelegate,UITableViewDataSource{
    
    var stock_fields = [Order_StockField]() {
        didSet{
            
            tableview.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stock_fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FieldCell(style: .default, reuseIdentifier: "cell")
        cell.header.text = stock_fields[indexPath.row].name
        cell.value.text = stock_fields[indexPath.row].value
        if indexPath.row == stock_fields.count - 1{
            cell.bottomSeperator.isHidden = true
        }
        else{
            cell.bottomSeperator.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869761825, green: 0.8871009946, blue: 0.8869369626, alpha: 1).cgColor
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.bounces = false
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        addSubview(tableview)
        tableview.fillSuperview(padding: .init(top: 0, left: 16.5.calcvaluex(), bottom: 0, right: 16.5.calcvaluex()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turningV : UIView {
    let ss = SelectMachineView()
    let bottomView = turnView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869761825, green: 0.8871009946, blue: 0.8869369626, alpha: 1)
        clipsToBounds = true
        addSubview(bottomView)
        bottomView.fillSuperview()
        
        addSubview(ss)
        ss.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        ss.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 306.calcvaluex(), height: 106.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turnView : UITableView,UITableViewDataSource,UITableViewDelegate {
    var lastFour = [Order_StockField](){
        didSet{
            print(888,lastFour)
            self.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = turnCell(style: .default, reuseIdentifier: "cell")
        if indexPath.row == 1{
            cell.sep4.isHidden = true
            cell.header.text = "最大射出率 "
            cell.subHeader.text = "cm3/sec"
            cell.value1.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "machine-max-rate"
            })?.value
            cell.value2.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "real-max-rate"
            })?.value
        }
        else{
            cell.sep4.isHidden = false
            cell.header.text = "最大射壓 "
            cell.subHeader.text = "bar"
            cell.value1.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "machine-max-pressure"
            })?.value
            cell.value2.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "real-max-pressure"
            })?.value
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vv = turnHeader()
        
        return vv
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 49.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)

        separatorStyle = .none
       
        self.bounces = false
        delegate = self
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct Order_StockField {
    var name:String
    var value:String?
    var code: String
    var affix : String?
}
class DescriptionView : UIView {
   let topvd = UIView()
    let textv = UITextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        
        
        if #available(iOS 11.0, *) {
            topvd.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            topvd.roundCorners2([.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: topvd.layer.cornerRadius)
            // Fallback on earlier versions
        }
        topvd.layer.cornerRadius = 6.calcvaluex()
        topvd.backgroundColor = #colorLiteral(red: 0.928375721, green: 0.9285091758, blue: 0.9283465147, alpha: 1)
        addSubview(topvd)
        topvd.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 49.calcvaluey()))
        let label = UILabel()
        label.text = "介紹".localized
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = .black
        
        topvd.addSubview(label)
        label.anchor(top: nil, leading: topvd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
        addSubview(textv)
        textv.textColor = .black
        textv.isEditable = false
        textv.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        textv.anchor(top: topvd.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 36.calcvaluex(), bottom: 6.calcvaluey(), right: 36.calcvaluex()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
class StockContentController:SampleController {
    var stock:Stock? {
        didSet{
            print(999,stock)
            order_f = []
            lastFour = []
            for i in StockApi.shared.fields ?? [] {
   
                if let s = stock?.fields?[i.code ?? ""] {
                    if i.code == "machine-max-pressure" || i.code == "machine-max-rate" || i.code == "real-max-pressure" || i.code == "real-max-rate" {
                        lastFour.append(Order_StockField(name: i.titles?.getLang() ?? "", value: s?.getLang(),code:i.code ?? "",affix: i.affix))
                    }
                    else{
                        order_f.append(Order_StockField(name: i.titles?.getLang() ?? "", value: s?.getLang(),code: i.code ?? "",affix: i.affix))
                    }
                }
            }
            if let index = order_f.firstIndex(where: { (or) -> Bool in
                return or.code == "introduction"
            }) {
                let ss = order_f.remove(at: index)
                fields_view.stock_fields = order_f
                bottomView.textv.text = ss.value
            }

        }
    }
    var lastFour = [Order_StockField]()
    var order_f = [Order_StockField]()
    let full_image = UIImageView()
    let fields_view = FieldsView()
    let bottomView = DescriptionView()
    let nextView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
        backbutton.tintColor = .white
        titleview.label.text = stock?.title
        
        full_image.backgroundColor = .white
        full_image.contentMode = .scaleAspectFit
        full_image.clipsToBounds = true
        full_image.layer.borderWidth = 1.calcvaluex()
        full_image.layer.borderColor = #colorLiteral(red: 0.8730852008, green: 0.8732081056, blue: 0.8730465174, alpha: 1).cgColor
        view.addSubview(full_image)
        full_image.sd_setImage(with: URL(string: stock?.assets?.getImage() ?? ""), completed: nil)
        
        full_image.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 530.calcvaluex(), height: 339.calcvaluey()))
        
        view.addSubview(fields_view)
        fields_view.anchor(top: full_image.topAnchor, leading: full_image.trailingAnchor, bottom: full_image.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    
        view.addSubview(bottomView)
        bottomView.anchor(top: full_image.bottomAnchor, leading: full_image.leadingAnchor, bottom: nil, trailing: fields_view.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 273
                                                                                                                                                                                                                    .calcvaluey()))
        full_image.isUserInteractionEnabled = true
        full_image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expandImage)))
        nextView.addshadowColor()
        nextView.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
//        view.addSubview(nextView)
//        nextView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 76.calcvaluey()))
    }
    @objc func expandImage(){
        let img = StockImageExpandController()
        img.modalPresentationStyle = .fullScreen
        img.img.image = full_image.image
        self.present(img, animated: true, completion: nil)
    }
}


class SelectMachineView : UIView {
    let plusImage = UIImageView(image: #imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate))
    let plusLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        plusImage.tintColor = .white
        plusImage.contentMode = .scaleAspectFit
        plusImage.backgroundColor = #colorLiteral(red: 0.9407916665, green: 0.5144688487, blue: 0.007106156554, alpha: 1)
        plusImage.layer.cornerRadius = 40.calcvaluex()/2
        addSubview(plusImage)
        plusImage.centerXInSuperview()
        
        plusImage.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        
        plusLabel.text = "比對試算機台"
        plusLabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        plusLabel.textColor = #colorLiteral(red: 0.9407916665, green: 0.5144688487, blue: 0.007106156554, alpha: 1)
        
        addSubview(plusLabel)
        plusLabel.anchor(top: plusImage.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 22.calcvaluey()))
        plusLabel.centerXInSuperview()
        plusImage.isUserInteractionEnabled = true
        plusImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelect)))
    }
    @objc func goSelect(){
        print(99999)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
