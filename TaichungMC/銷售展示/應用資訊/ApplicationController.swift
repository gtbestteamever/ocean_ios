//
//  ApplicationController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/9/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class MenuButton : UIView {
    let label = UILabel()
    let underline = UIView()
    init(text:String) {
        super.init(frame: .zero)
        addSubview(label)
        label.text = text
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        label.centerYInSuperview()
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        
        underline.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        underline.addshadowColor(color: .white)
        
        underline.layer.cornerRadius = 3.calcvaluey()
        
        addSubview(underline)
        underline.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,size: .init(width: 50.calcvaluex(), height: 6.calcvaluey()))
        underline.centerXInSuperview()
        
        isUserInteractionEnabled = true
        
        
    }
    func showSelected() {
        label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        underline.isHidden = false
    }
    func showUnSelected(){
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        underline.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemSelectionField : UITextField,UITextFieldDelegate {
    init(text:String){
        super.init(frame: .zero)
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
        
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        layer.cornerRadius = 19.calcvaluey()
        
        font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        self.text = text
        
        
        
        let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
        addSubview(arrow)
        arrow.centerYInSuperview()
        arrow.contentMode = .scaleAspectFit
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.55.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
       delegate = self
    }
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 24.calcvaluex(), dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 24.calcvaluex(), dy: 0)
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 24.calcvaluex(), dy: 0)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class RcSelectionView : UIView {
    let label = UILabel()
    let downArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate))

    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        label.text = "全部".localized
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        layer.cornerRadius = 20.calcvaluey()
        addshadowColor()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        downArrow.contentMode = .scaleAspectFit
        addSubview(downArrow)
        downArrow.tintColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        downArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 14.calcvaluex()),size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        downArrow.centerYInSuperview()
        addSubview(label)
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: downArrow.leadingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
        

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemSelectionView : UIView {
    let label = UILabel()
    var selectionFild : ItemSelectionField?
    init(header:String,text:String) {
        super.init(frame: .zero)
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        label.text = header
        
        selectionFild = ItemSelectionField(text: text)
        
        
        addSubview(label)
        
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        if let selection = selectionFild {
        addSubview(selection)
            selection.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
        }
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class confirmButton:UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("確認".localized, for: .normal)
        setTitleColor(.white, for: .normal)
        titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        layer.cornerRadius = 24.calcvaluey()
        backgroundColor = MajorColor().oceanSubColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ApplicationSelectLabel : UIView {
    let label : UILabel = {
       let ll = UILabel()
        ll.text = "產業類別"
        ll.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        return ll
    }()
    var selectionView = RcSelectionView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        addSubview(selectionView)
        selectionView.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        selectionView.label.text = "全部"
        
        selectionView.layer.cornerRadius = 38.calcvaluey()/2
    }
    func setSelected(){
        label.textColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        selectionView.label.textColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        selectionView.downArrow.tintColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        selectionView.isUserInteractionEnabled = true
    }
    func unSelected(){
        label.textColor = #colorLiteral(red: 0.7596674801, green: 0.7596674801, blue: 0.7596674801, alpha: 1)
        selectionView.label.textColor = #colorLiteral(red: 0.7596674801, green: 0.7596674801, blue: 0.7596674801, alpha: 1)
        selectionView.downArrow.tintColor = #colorLiteral(red: 0.7596674801, green: 0.7596674801, blue: 0.7596674801, alpha: 1)
        selectionView.isUserInteractionEnabled = false
    }
    func setX(){
        
        selectionView.downArrow.image = #imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate)
        selectionView.downArrow.isUserInteractionEnabled = true
    }
    func setDown(){
        selectionView.downArrow.tintColor = .black
        selectionView.downArrow.image = #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate)
        selectionView.downArrow.isUserInteractionEnabled = false
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ApplicationSelectionView : UIView {
    let topview : UILabel = {
       let tp = UILabel()

        tp.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        tp.textColor = #colorLiteral(red: 0.635173471, green: 0.635173471, blue: 0.635173471, alpha: 1)
        tp.textAlignment = .center
        tp.backgroundColor = #colorLiteral(red: 0.9761912227, green: 0.9763311744, blue: 0.9761605859, alpha: 1)
        return tp
    }()
    func setSelected(){
        topview.textColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        container.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        firstSelect.setSelected()
        secondSelect.setSelected()
        addshadowColor()
        confirmB.isHidden = false
    }
    func setUnSelected(){
        addShadowColor(opacity: 0.03)
        topview.textColor = #colorLiteral(red: 0.7596674801, green: 0.7596674801, blue: 0.7596674801, alpha: 1)
        container.backgroundColor = #colorLiteral(red: 0.9897611737, green: 0.9899029136, blue: 0.9897301793, alpha: 1)
        firstSelect.unSelected()
        secondSelect.unSelected()
        confirmB.isHidden = true
    }
    let container = UIView()
    
    let firstSelect = ApplicationSelectLabel()
    let secondSelect = ApplicationSelectLabel()
    var confirmB = confirmButton()
    init(title:String,array:[String]) {
        super.init(frame: .zero)
        layer.cornerRadius = 15.calcvaluey()
        addshadowColor()
        
        addSubview(container)
        container.layer.cornerRadius = 15.calcvaluey()
        container.clipsToBounds = true
        container.backgroundColor = #colorLiteral(red: 0.9897611737, green: 0.9899029136, blue: 0.9897301793, alpha: 1)
        container.fillSuperview()
        
        
        container.addSubview(topview)
        topview.text = title
        topview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,size: .init(width: 0, height: 48.calcvaluey()))
        topview.isHidden = true
        container.addSubview(firstSelect)
        firstSelect.label.text = array[0]
        firstSelect.anchor(top: topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 46.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()),size: .init(width: 0, height: 71.calcvaluey()))
        container.addSubview(secondSelect)
        secondSelect.label.text = array[1]
        secondSelect.anchor(top: firstSelect.bottomAnchor, leading: firstSelect.leadingAnchor, bottom: nil, trailing: firstSelect.trailingAnchor,padding: .init(top: 46.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 71.calcvaluey()))
        
        container.addSubview(confirmB)
        confirmB.isHidden = true
        confirmB.centerXInSuperview()
        confirmB.anchor(top: secondSelect.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 168.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol ApplictionViewDelegate {
    func setCurrent(current:Int)
    func deleteData(current:Int)
}
class ApplicationView : UIView {
    let adLabel : UILabel = {
       let aL = UILabel()
        aL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        aL.text = "＊擇一篩選"
        aL.textColor = #colorLiteral(red: 0.9392504096, green: 0.5119799972, blue: 0.02642006986, alpha: 1)
        return aL
    }()
    var array : [[ApplicationData?]] = [] {
        didSet{
            for (index,i) in self.array.enumerated() {
                for (index2,j) in i.enumerated() {
                    var st : String? = nil
                    if let j = j{
                        st = j.title
                    }
                    else{
                        st = "選擇"
                    }
                    print(index,index2,776,st)
                    if index == 0{
                        if index2 == 0 {
                            s1.firstSelect.selectionView.label.text = st
                        }
                        else{
                            s1.secondSelect.selectionView.label.text = st
                        }
                    }
                    else{
                        if index2 == 0{
                            s2.firstSelect.selectionView.label.text = st
                        }
                        else{
                            s2.secondSelect.selectionView.label.text = st
                        }
                    }
                }
            }
        }
    }
    var s1 = ApplicationSelectionView(title: "篩選方式一", array: ["產業類別".localized,"工件種類".localized])
    var s2 = ApplicationSelectionView(title: "篩選方式二", array: ["產業類別","塑料類別"])
    var current : Int = 0 {
        didSet{
            if current == 0{
            s1.setSelected()
            s2.setUnSelected()
            }
            else{
                s2.setSelected()
                s1.setUnSelected()
            }
        }
    }
    var delegate : ApplictionViewDelegate?
    var s1Anchor:AnchoredConstraints?
    var s2Anchor:AnchoredConstraints?
    var adLabalAnchor:AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        addSubview(adLabel)
        adLabalAnchor = adLabel.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 25.calcvaluey()))
        adLabel.isHidden = true
        addSubview(s1)
        s1Anchor = s1.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 38.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 673.calcvaluex(), height: 553.calcvaluey()))
        s1.setSelected()
        addSubview(s2)
        s2.setUnSelected()
        s2Anchor = s2.anchor(top: s1.topAnchor, leading: s1.trailingAnchor, bottom: s1.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 324.calcvaluex(), height: 0))
        s2.isHidden = true
        s1.tag = 0
        s2.tag = 1
        s1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectWhich)))
        s2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectWhich)))
        s1.firstSelect.selectionView.downArrow.tag = 0
        s1.secondSelect.selectionView.downArrow.tag = 1
        
        s1.firstSelect.selectionView.downArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteChoice)))
        s1.secondSelect.selectionView.downArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteChoice)))
    }
    @objc func deleteChoice(gesture:UIGestureRecognizer) {
        guard let index = gesture.view?.tag else {return}
        delegate?.deleteData(current: index)
        if index == 0{
            s1.firstSelect.selectionView.label.text = "全部".localized
            s1.firstSelect.setDown()
        }
        else{
            s1.secondSelect.selectionView.label.text = "全部".localized
            s1.secondSelect.setDown()
        }
    }
    @objc func selectWhich(sender:UITapGestureRecognizer){
        if sender.view?.tag == 0{
            s1.setSelected()
            s2.setUnSelected()
            current = 0
            delegate?.setCurrent(current: 0)
        }
        else{
            s2.setSelected()
            s1.setUnSelected()
            current = 1
            delegate?.setCurrent(current: 1)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct DefaultApplication {
    var app = ApplicationData(id: "selection", parent_id: nil, key_id: "selection", title: "全部".localized, titles: Lang(en: "All", zhTW: "全部"), children: nil)
}
class ApplicationController: SampleController,SelectionViewDelegate,ApplictionViewDelegate {
    func deleteData(current: Int) {
        array[0][current] = DefaultApplication().app
        setStockData(data: array[0])
        //self.current = current
    }
    
    var current = 0
    func setCurrent(current: Int) {
        setStockData(data: array[current])
        self.current = current
    }
    
    func setSelect(current: Int, index: Int, data: ApplicationData) {
        //print(current,index)
 
            array[current][index] = data
        var all = false
        if data.title == "全部".localized {
            all = true
        }
        switch current {
        case 0:
            if index == 0{
                contentView1.s1.firstSelect.selectionView.label.text = data.title
                if all {
                contentView1.s1.firstSelect.setDown()
                }
                else{
                    contentView1.s1.firstSelect.setX()
                }
            }
            else{
                contentView1.s1.secondSelect.selectionView.label.text = data.title
                if all{
                    contentView1.s1.firstSelect.setDown()
                }
                else{
                    contentView1.s1.secondSelect.setX()
                }
                
            }
        case 1:
            if index == 0{
                contentView1.s2.firstSelect.selectionView.label.text = data.title

            }
            else{
                contentView1.s2.secondSelect.selectionView.label.text = data.title

            }
        default:
            ()
        }
        setStockData(data: self.array[current])
 
    }
    func setStockData(data:[ApplicationData?]){
        if var filterdata = StockApi.shared.stocks {
        //self.filter_data = filterdata
        for i in data {
            if let i = i{
                if i.key_id == "selection" {
                    continue
                }
            filterdata = filterdata.filter({ (st) -> Bool in
                if let categories = st.categories {
                    
                    if categories.contains(i.id ?? "") {
                        return true
                    }
                }

                return false
            })
            }
        }
        
        self.filter_stocks = filterdata
        }
    }
    var filter_data = [Stock]()
    var filter_stocks = [Stock]()
    var array : [[ApplicationData?]] = [[DefaultApplication().app,DefaultApplication().app],[nil,nil]]
    let menuview = UIView()
    let additionButton = MenuButton(text: "加工應用".localized)
    let modelButton = MenuButton(text: "鎖模力估算".localized)

    
    let contentView1 = ApplicationView()
    let contentView2 = UIView()
    override func changeLang() {
        super.changeLang()
        additionButton.label.text = "加工應用".localized
        contentView1.s1.firstSelect.label.text = "產業類別".localized
        contentView1.s1.secondSelect.label.text = "工件種類".localized
        contentView1.s1.firstSelect.selectionView.label.text = array.first?[0]?.titles?.getLang()
        contentView1.s1.secondSelect.selectionView.label.text = array.first?[1]?.titles?.getLang()
        
        contentView1.s1.confirmB.setTitle("確認".localized, for: .normal)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = false
        titleview.label.text = "應用資訊".localized
        
        
        
        view.addSubview(menuview)
        menuview.addshadowColor(color: .white)
        menuview.backgroundColor = .white
        
        menuview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 65.calcvaluey()))
        
        view.addSubview(additionButton)
        additionButton.tag = 1
        additionButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        additionButton.anchor(top: menuview.topAnchor, leading: nil, bottom: menuview.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: -3.calcvaluey(), right: 600.calcvaluex()))
        additionButton.showSelected()
        view.addSubview(modelButton)
        modelButton.tag = 2
        modelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        modelButton.showUnSelected()
        modelButton.anchor(top: menuview.topAnchor, leading: additionButton.trailingAnchor, bottom: menuview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 78.calcvaluex(), bottom: -3.calcvaluey(), right: 0))
        modelButton.isHidden = true
        view.addSubview(contentView1)
        contentView1.delegate = self
        contentView1.anchor(top: menuview.bottomAnchor, leading: additionButton.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        contentView1.s1.firstSelect.selectionView.tag = 0
        contentView1.s1.secondSelect.selectionView.tag = 1
        contentView1.s2.firstSelect.selectionView.tag = 2
        contentView1.s2.secondSelect.selectionView.tag = 3
        contentView1.s1.firstSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
        contentView1.s1.secondSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
        contentView1.s2.firstSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
        contentView1.s2.secondSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
        contentView1.s1.confirmB.tag = 0
        contentView1.s2.confirmB.tag = 1
        contentView1.s1.confirmB.addTarget(self, action: #selector(goNextView), for: .touchUpInside)
        contentView1.s2.confirmB.addTarget(self, action: #selector(goNextView), for: .touchUpInside)
    }
    @objc func goNextView(sender:UIButton){
        var fd = [[String:ApplicationData]]()
        if sender.tag == 0{
            if array[0][0] == nil && array[0][1] == nil{
                print("AA")

                return
            }
//            if array[0][0]?.key_id == "selection" {
//                let vd = ["selection"]
//            }
            
            if let at = array[0][0] {
                let vd = ["industry-kind":at]
                
                fd.append(vd)
            }
            if let at = array[0][1] {
                let vd = ["workpiece-kind":at]
                fd.append(vd)
            }
        }
        else{
            if array[1][0] == nil && array[1][1] == nil{
                print("BB")

                return
            }
            
            if let at = array[1][0] {
                let vd = ["產業類別".localized:at]
                fd.append(vd)
            }
            
            if let at = array[1][1] {
                let vd = ["塑料類別".localized:at]
                fd.append(vd)
                
                
            }
        }
        
        //print(789,fd)
        let vc = StocksController()
        vc.modalPresentationStyle = .fullScreen
        vc.selectionView.data = fd
        vc.con = self
        vc.count = sender.tag
        //vc.filter_data = self.filter_data
        vc.stock_data = filter_stocks
        self.present(vc, animated: true, completion: nil)
    }
    @objc func choice(gesture:UITapGestureRecognizer){
        var title : String? = ""
        var count : Int?
        var index : Int?
        if let vd = gesture.view {
            switch vd.tag {
            case 0:
                title = "industry-kind"
                count = 0
                index = 0
            case 1:
                title = "workpiece-kind"
                count = 0
                index = 1
            case 2:
                title = contentView1.s2.firstSelect.label.text
                count = 1
                index = 0
            case 3:
                title = contentView1.s2.secondSelect.label.text
                count = 1
                index = 1
            default:
                ()
            }
        }
        
            if let dt = StockApi.shared.cjson {
                if var data = dt.first(where: { (ap) -> Bool in
                    if ap.key_id == title {
                        return true
                    }
                    return false
                }) {
                    data.children?.insert(DefaultApplication().app, at: 0)
                    let vd = SelectionViewController()
                    vd.count = count
                    vd.index = index
                    
                    vd.popView.data = data.children
                    vd.popView.cdelegate = self
                    vd.modalPresentationStyle = .overCurrentContext
                    self.present(vd, animated: false, completion: nil)
                    
                }
            }
       // }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    
    var group = DispatchGroup()
    func setArrayData(dt:[ApplicationData]){
        print(78,array)
        for (index,i) in self.array.enumerated() {
            for (index2,j) in i.enumerated() {
                if let m = j{
                    var count = 0
                    for kk in dt {
                        if let first = kk.children?.first(where: { (ap) -> Bool in
                            return ap.id == m.id
                        }) {
                            self.array[index][index2] = first
                            
                            switch index {
                            case 0:
                                if index2 == 0{
                                    self.contentView1.s1.firstSelect.selectionView.label.text = self.array[index][index2]?.title
                                }
                                else{
                                    self.contentView1.s1.secondSelect.selectionView.label.text = self.array[index][index2]?.title
                                }
                            case 1:
                                if index2 == 0{
                                    self.contentView1.s2.firstSelect.selectionView.label.text = self.array[index][index2]?.title
                                }
                                else{
                                    self.contentView1.s2.secondSelect.selectionView.label.text = self.array[index][index2]?.title
                                }
                            default:
                                ()
                            }
                            
                            
                            count = 0
                            break
                        }
                        else{
                            count = 1
                        }
                    }
                    if count == 1{
                        self.array[index][index2] = DefaultApplication().app
                        
                        switch index {
                        case 0:
                            if index2 == 0{
                                self.contentView1.s1.firstSelect.selectionView.label.text = "全部".localized
                            }
                            else{
                                self.contentView1.s1.secondSelect.selectionView.label.text = "全部".localized
                            }
                        case 1:
                            if index2 == 0{
                                self.contentView1.s2.firstSelect.selectionView.label.text = "全部".localized
                            }
                            else{
                                self.contentView1.s2.secondSelect.selectionView.label.text = "全部".localized
                            }
                        default:
                            ()
                        }
                    }
                }
                else{
                    switch index {
                    case 0:
                        if index2 == 0{
                            self.contentView1.s1.firstSelect.selectionView.label.text = "全部".localized
                        }
                        else{
                            self.contentView1.s1.secondSelect.selectionView.label.text = "全部".localized
                        }
                    case 1:
                        if index2 == 0{
                            self.contentView1.s2.firstSelect.selectionView.label.text = "全部".localized
                        }
                        else{
                            self.contentView1.s2.secondSelect.selectionView.label.text = "全部".localized
                        }
                    default:
                        ()
                    }
                }
            }

        }
    }
    func fetchApi(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        contentView1.isHidden = true
        group.enter()
        StockApi.shared.tryCall(group: group, data: UserDefaults.standard.getStockChoice(), decodable: Application.self, mode: .Choices)
        
        group.enter()
        StockApi.shared.tryCall(group: group, data: UserDefaults.standard.getStocks(), decodable: Stocks.self, mode: .Stocks)
        group.enter()
        StockApi.shared.tryCall(group: group, data: UserDefaults.standard.getStockFields(), decodable: StockFields.self, mode: .Fields)
        group.notify(queue: .main) {
            if let dt = StockApi.shared.cjson, let _ = StockApi.shared.fields, let _ = StockApi.shared.stocks {
                
                    
                hud.dismiss()
                print(3321,dt)
                self.setArrayData(dt: dt)
                self.setStockData(data: self.array[self.current])
                
                self.contentView1.isHidden = false
            }
                
               
            
            
            

        }


    }
    @objc func goSelection(sender:UIGestureRecognizer){
        if sender.view?.tag == 1{
            additionButton.showSelected()
            modelButton.showUnSelected()
            contentView1.isHidden = false
            contentView2.isHidden = true
        }
        else{
            additionButton.showUnSelected()
            modelButton.showSelected()
            contentView1.isHidden = true
            contentView2.isHidden = false
            
            
        }
    }
    @objc func selectCareer(){
        print(123)
        let vd = SampleSelectionController()
        
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
}
