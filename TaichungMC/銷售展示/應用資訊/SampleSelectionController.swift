//
//  SampleSelectionController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/9/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SampleCollectionViewCell : UICollectionViewCell {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 19.calcvaluey()
        backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        addSubview(label)
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.fillSuperview(padding: .init(top: 7.calcvaluey(), left: 28.calcvaluex(), bottom: 6.calcvaluey(), right: 28.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SampleSelectionController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    let mainview = UIView()
    var mainviewanchor:AnchoredConstraints?
    var header = UILabel()
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: CustomViewFlowLayout())
    var stringarray : [String] = ["手工具業","車輛產業","自行車業","摩托車業","電動車業","卡/貨車業","輪圈業"]
    var widtharray : [CGFloat] = [130.calcvaluex(),130.calcvaluex(),130.calcvaluex(),130.calcvaluex(),130.calcvaluex(),137.calcvaluex(),112.calcvaluex()]
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        mainview.backgroundColor = .white
        mainview.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(mainview)
        mainview.addshadowColor()
        mainviewanchor = mainview.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 750.calcvaluey()))
        
        let xbutton = UIButton()
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        
        mainview.addSubview(xbutton)
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        xbutton.anchor(top: mainview.topAnchor, leading: nil, bottom: nil, trailing: mainview.trailingAnchor,padding: .init(top: 37.calcvaluey(), left: 0, bottom: 0, right: 28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.text = "選擇產業類別"
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = .black
        mainview.addSubview(header)
        header.anchor(top: mainview.topAnchor, leading: mainview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 34.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        
        mainview.addSubview(collectionview)
        collectionview.anchor(top: header.bottomAnchor, leading: mainview.leadingAnchor, bottom: mainview.bottomAnchor, trailing: mainview.trailingAnchor,padding: .init(top: 40.calcvaluey(), left: 28.calcvaluex(), bottom: 0, right: 0))
        //collectionview.backgroundColor = .red
        collectionview.backgroundColor = .white
        (collectionview.collectionViewLayout as! CustomViewFlowLayout).itemSize = UICollectionViewFlowLayout.automaticSize
        (collectionview.collectionViewLayout as! CustomViewFlowLayout).cellSpacing = 24.calcvaluex()
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.alwaysBounceVertical = true
        collectionview.register(SampleCollectionViewCell.self, forCellWithReuseIdentifier: "abc")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringarray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: stringarray[indexPath.item].width(withConstrainedHeight: 38.calcvaluey(), font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) + 58.calcvaluex(), height: 38.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24.calcvaluey()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 24.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "abc", for: indexPath) as! SampleCollectionViewCell
//        cell.backgroundColor = .blue
        cell.label.text = stringarray[indexPath.item]
        return cell
    }
    @objc func popView(){
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        mainviewanchor?.top?.constant = -744.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        } completion: { (_) in
            //
        }

    }
}
