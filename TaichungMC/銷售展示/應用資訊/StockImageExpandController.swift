//
//  StockImageExpandController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class StockImageExpandController: UIViewController {
    let img = UIImageView()
    let xbutton = UIButton(type: .custom)
    override func viewDidLoad() {
        super.viewDidLoad()
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        view.backgroundColor = .white
        img.contentMode = .scaleAspectFit
        view.addSubview(img)
        img.fillSuperview(padding: .init(top: 100.calcvaluex(), left:100.calcvaluex(), bottom: 100.calcvaluex(), right: 100.calcvaluex()))
        
        view.addSubview(xbutton)
        xbutton.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        xbutton.addTarget(self, action: #selector(dismissAll), for: .touchUpInside)
    }
    @objc func dismissAll(){
        self.dismiss(animated: true, completion: nil)
    }
}
