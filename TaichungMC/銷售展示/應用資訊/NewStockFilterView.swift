//
//  NewStockFilterView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 9/16/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol NewStockFilterDelegate {
    func setNewFilter(type:String,value:ApplicationData)
}
class NewStockFilterView : UIView {
    let tableview = UITableView(frame: .zero, style: .grouped)
    var isResin : Bool = true
    var delegate : NewStockFilterDelegate?
    
    init(isResin:Bool = true) {
        super.init(frame: .zero)
        self.isResin = isResin

        
       
        addSubview(tableview)
        tableview.fillSuperview()
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        tableview.register(StockCollectionViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NewStockFilterView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StockCollectionViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.delegate = self.delegate
        if isResin {
            if indexPath.section == 0{
                cell.data = StockApi.shared.cjson?.first(where: { ap in
                    return ap.key_id == "industry-kind"
                })
            }
            else{
                cell.data = StockApi.shared.cjson?.first(where: { ap in
                    return ap.key_id == "workpiece-kind"
                })
            }
        }
        else{
            if indexPath.section == 0{
                cell.data = StockApi.shared.cjson?.first(where: { ap in
                    return ap.key_id == "industry-kind"
                })
            }
            else{
                cell.data = StockApi.shared.cjson?.first(where: { ap in
                    return ap.key_id == "plastic-kind"
                })
            }
        }
        cell.data?.children?.insert(ApplicationData(id: "", parent_id: "", key_id: "selection", title: "全部".localized, titles: nil, children: nil),at:0)
        cell.frame = tableView.bounds
        cell.layoutIfNeeded()
        cell.collectionview.reloadData()
        cell.anchor?.height?.constant = cell.collectionview.collectionViewLayout.collectionViewContentSize.height
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        let label = UILabel()
        if isResin {
            if section == 0{
                label.text = "選擇產業類別".localized
            }
            else{
                label.text = "選擇工件種類".localized
            }
        }
        else{
            if section == 0{
                label.text = "選擇產業類別"
            }
            else{
                label.text = "選擇塑料類別"
            }
        }
        
        label.font = UIFont(name: "Roboto-Bold", size: 18.calcvaluex())
        label.textColor = .black
        vd.addSubview(label)
        label.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 24.calcvaluex(), bottom: 8.calcvaluey(), right: 8.calcvaluex()))
        
        return vd
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

class StockCollectionViewCell : UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.children?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StockCollectionCell
        cell.backgroundColor = .lightGray
        let dt = data?.children?[indexPath.item].title

        cell.layer.cornerRadius = ((dt?.heightOfString(usingFont: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) ?? 0) + 20.calcvaluey())/2
        cell.label.text = dt
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dt = data?.children?[indexPath.item].title
        let width = (dt?.widthOfString(usingFont: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) ?? 0) + 50.calcvaluex()
        let height = (dt?.heightOfString(usingFont: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) ?? 0) + 20.calcvaluey()
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dt = data, let selectedItem = dt.children?[indexPath.item] {
            delegate?.setNewFilter(type: data?.key_id ?? "", value: selectedItem)
        }
        
    }
    var data : ApplicationData?
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: CustomViewFlowLayout())
    var anchor: AnchoredConstraints?
    let spacer = UIView()
    var delegate : NewStockFilterDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        spacer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        contentView.addSubview(spacer)
        spacer.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 45.calcvaluex()),size: .init(width: 0, height: 2.calcvaluey()))
        contentView.addSubview(collectionview)
        anchor = collectionview.anchor(top: contentView.topAnchor, leading: spacer.leadingAnchor, bottom: spacer.topAnchor, trailing: spacer.trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0),size: .init(width: 0, height: 1.calcvaluey()))
        collectionview.backgroundColor = .clear
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.register(StockCollectionCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StockCollectionCell : UICollectionViewCell {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = .white
        
        addSubview(label)
        label.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
