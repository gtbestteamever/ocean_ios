//
//  SelectionViewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class DynamicCollectionView: UICollectionView {

}
protocol PopDelegate {
    func setHeight(height:CGFloat)
    func popDismiss()
    
}
class SelectPopCell:UICollectionViewCell {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.969394505, green: 0.969530046, blue: 0.9693517089, alpha: 1)
        layer.cornerRadius = 38.calcvaluey()/2
        label.textColor = #colorLiteral(red: 0.5685855746, green: 0.5686681271, blue: 0.568559587, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(label)
        label.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SelectPopView : UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var data : [ApplicationData]?{
        didSet{
            self.collectionview?.reloadData()
        }
    }
    let topLabel = UILabel()
    var count : Int?
    var index : Int?
    var delegate : PopDelegate?
    var cdelegate : SelectionViewDelegate?
    var collectionview : DynamicCollectionView?
    var height:CGFloat? {
        didSet{
            delegate?.setHeight(height: height ?? 0)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        topLabel.text = "選擇分類".localized
        topLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluey())
        topLabel.textColor = #colorLiteral(red: 0.5685855746, green: 0.5686681271, blue: 0.568559587, alpha: 1)
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 32.calcvaluex(), bottom: 0, right: 32.calcvaluex()),size: .init(width: 0, height: 25.calcvaluey()))
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        collectionview = DynamicCollectionView(frame: .zero, collectionViewLayout: CustomViewFlowLayout())
        flowlayout.itemSize = UICollectionViewFlowLayout.automaticSize
        addSubview(collectionview!)
        
        collectionview?.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 40.calcvaluey(), left: 0, bottom: 90.calcvaluey(), right: 32.calcvaluex()))
        collectionview?.backgroundColor = .clear
        
        collectionview?.delegate = self
        collectionview?.dataSource = self
        collectionview?.register(SelectPopCell.self, forCellWithReuseIdentifier: "cell")
       
        
        self.collectionview?.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
       
        height = (collectionview?.collectionViewLayout.collectionViewContentSize.height ?? 0) + 20.calcvaluey()
        collectionview?.heightAnchor.constraint(equalToConstant: height ?? 0).isActive = true
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width : CGFloat = 0
        if let json = data {
            width = json[indexPath.item].titles?.getLang()?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) ?? 0
        }
       
        return .init(width: width + 64.calcvaluex(), height: 38.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectPopCell
        cell.label.text = data?[indexPath.item].titles?.getLang()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = data, let count = count, let index = index{
        cdelegate?.setSelect(current: count, index: index, data: data[indexPath.item])
            delegate?.popDismiss()
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol SelectionViewDelegate {
    func setSelect(current:Int,index:Int,data:ApplicationData)
}
class SelectionViewController:UIViewController,UIGestureRecognizerDelegate,PopDelegate{
    func popDismiss() {
        dismissAll()
    }
    

    
    func setHeight(height: CGFloat) {
        let calcheight = 201.calcvaluey() + height
        popView.heightAnchor.constraint(equalToConstant: calcheight).isActive = true
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.4) {
            self.popView.transform = CGAffineTransform(translationX: 0, y: -calcheight + 25.calcvaluey())
        }
    }
    var count: Int?
    var index:Int?
    let popView = SelectPopView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        popView.delegate = self
        popView.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        popView.layer.cornerRadius = 15.calcvaluey()
        view.addSubview(popView)
        popView.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        let tapGestureRecoginzer = UITapGestureRecognizer(target: self, action: #selector(dismissAll))
        tapGestureRecoginzer.delegate = self
        tapGestureRecoginzer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecoginzer)
        popView.count = count
        popView.index = index
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let vd = touch.view {
            
            if vd.isDescendant(of: popView){
                return false
            }
        }
        return true
    }
    @objc func dismissAll(){
        UIView.animate(withDuration: 0.4) {
            self.popView.transform = .identity
        } completion: { (_) in
            self.dismiss(animated: false, completion: nil)
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
}
