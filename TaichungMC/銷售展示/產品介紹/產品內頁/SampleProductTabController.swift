//
//  SampleProductTabController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/22.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class ProductTabField:UITextField {
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 26.calcvaluex(), dy: 0)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 26.calcvaluex(), dy: 0)
    }
}
class SampleProductTabController: UIViewController{

    let leftview = UIView()
    let rightview = UIView()
    var nextvd : UIView?
    var mode:ControllerMode?
    var fileArray = [FilePath]()
    var originalFileArray = [FilePath]()
    init(nextvd:UIView,mode:ControllerMode) {
        super.init(nibName: nil, bundle: nil)
        self.nextvd = nextvd
        self.mode = mode
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let machineLabel = UILabel()
    let selectLabel = ProductTabField()
    let searchFild = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .grouped)
    
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    var currentIndex = 0
    var leftviewAnchor : AnchoredConstraints?
    var tableviewanchor : AnchoredConstraints?
    let arrowimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        leftview.backgroundColor = .clear
        view.addSubview(leftview)
        leftviewAnchor = leftview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 312.calcvaluex(), height: 0))
        
        rightview.backgroundColor = .clear
        view.addSubview(rightview)
        rightview.anchor(top: view.topAnchor, leading: leftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 90.calcvaluey(), right: 0))

        rightview.addSubview(nextvd!)
        nextvd?.fillSuperview()
//        leftview.addSubview(machineLabel)
//        machineLabel.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
//        machineLabel.text = "機型".localized
//        machineLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//        machineLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
//        leftview.addSubview(selectLabel)
//        selectLabel.anchor(top: view.topAnchor, leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 88.calcvaluex(), bottom: 0, right: 18.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
//        selectLabel.addshadowColor(color: .white)
//        selectLabel.layer.cornerRadius = 38.calcvaluey()/2
//        selectLabel.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
//        selectLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
//        //selectLabel.text = "NP16 2軸"
//        selectLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//        
//       
//        arrowimage.contentMode = .scaleAspectFit
//        
//        selectLabel.addSubview(arrowimage)
//        arrowimage.anchor(top: nil, leading: nil, bottom: nil, trailing: selectLabel.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
//        arrowimage.centerYInSuperview()
        
        
        
        searchFild.addshadowColor(color: .white)
        searchFild.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        searchFild.autocorrectionType = .no
        searchFild.autocapitalizationType = .none
        searchFild.returnKeyType = .search
        searchFild.clearButtonMode = .always
        searchFild.delegate = self
        leftview.addSubview(searchFild)
        searchFild.layer.cornerRadius = 24.calcvaluey()
        
        var searchText = ""
        switch mode {
        case .Spec:
            searchText = "搜尋產品規格".localized
        case .Equip:
            searchText = "搜尋選購配備".localized
        case .Functionality:
            searchText = "搜尋技術文件".localized
        case .Precision:
            searchText = "搜尋精度標準"
        case .Diagram:
            searchText = "搜尋配置圖"
        
        case .none:
            ()
        }
        searchFild.attributedPlaceholder = NSAttributedString(string: searchText, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchFild.anchor(top: view.topAnchor, leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 18.calcvaluex()),size: .init(width: 0, height: 48.calcvaluey()))
        searchFild.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        leftview.addSubview(tableview)
        tableviewanchor = tableview.anchor(top: searchFild.bottomAnchor, leading: leftview.leadingAnchor, bottom: leftview.bottomAnchor, trailing: leftview.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.showsVerticalScrollIndicator = false
        tableview.contentInset = .init(top: 0, left:0, bottom: 72.calcvaluey(), right: 0)
        
//        selectContent.backgroundColor = .clear
////        selectContent.delegate = self
////        selectContent.dataSource = self
//        leftview.addSubview(selectContent)
//        selectContent.addShadowColor(opacity: 0.1)
//
//        selectContent.anchor(top: selectLabel.bottomAnchor, leading: selectLabel.leadingAnchor, bottom: nil, trailing: selectLabel.trailingAnchor)
//        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
//        selectContentConstraint.isActive = true
//
//        selectLabel.delegate = self
//
//        selectContent.addSubview(selectContentTableView)
//        selectContentTableView.layer.cornerRadius = 5.calcvaluex()
//        selectContentTableView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
//        selectContentTableView.fillSuperview()
//        selectContentTableView.delegate = self
//        selectContentTableView.dataSource = self
//
//        selectContentTableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
    }
    var productArray = [Product]()
    var selectedProduct: Product?
    var selectHeight : CGFloat = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tab = tabBarController as? ProductInnerTabBarController {
            selectedProduct = tab.selectedProduct
            if let v = tab.parent as? ProductInnerInfoController {
                v.titleview.label.text = "\(tab.selectedSeries?.titles?.getLang() ?? "") - \(selectedProduct?.titles?.getLang() ?? "")"
            }
//            if tab.selectedProduct != nil {
//                selectedProduct = tab.selectedProduct
//            }
//            else{
//            selectedProduct = tab.product?[tab.productIndex ?? 0].array[tab.productSubIndex ?? 0]?.product
//
//
//                let ntTitle = "\(tab.product?[tab.productIndex ?? 0].title ?? "") \(selectedProduct?.titles?.getLang() ?? "")"
//            selectedProduct?.title = ntTitle
//            }
//            if let k = tab.parent as? ProductInnerInfoController {
//                k.product = selectedProduct
//            }
//
//           productArray = []
//            for i in (tab.product ?? []).filter({ (st) -> Bool in
//                return st.status == 1
//            }) {
//                print(778,i)
//                for j in i.array {
//                    if var m = j{
//                        m.product.title = "\(i.title) \(m.product.titles?.getLang() ?? "")"
//                        m.product.is_disable = i.is_disable
//                        m.product.status = i.status
//                        productArray.append(m.product)
//                    }
//                }
//            }
            
            
            
            setRightView()
        }
        
    }
    func setSpec(products:Product){
        if (products.specifications?.count ?? 0) == 0 {
          
            SpecArray.constructValue(field: [])
            return
        }
        for s in products.specifications ?? [] {
            print(3312,s.fields)
            print("-------")
            SpecArray.constructValue(field: s.fields)
            break
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        popContent()
    }
    func setRightView(){
        if let ab = nextvd as? SpecTableView {
            ab.product = self.selectedProduct
            
        }

        self.selectContentTableView.reloadData()
        self.tableview.reloadData()
    }
}
extension SampleProductTabController:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: selectContentTableView.self))!{
            return false
        }
        return true
    }
    @objc func popContent(){
        self.view.endEditing(true)
      //  selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = selectHeight
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        var id = ""
        if currentIndex < fileArray.count {
            id = fileArray[currentIndex].id ?? ""
        }
        if textField.text == nil || textField.text == ""{
            
            
            if let index = originalFileArray.firstIndex(where: { (ft) -> Bool in
                return ft.id
                 == id
            }) {
                currentIndex = index
            }
            else{
                currentIndex = 0
            }
         fileArray = originalFileArray
        }
        else{
        fileArray = originalFileArray.filter { (ft) -> Bool in
            return (ft.title?.lowercased().contains((textField.text ?? "").lowercased()) ?? false)
        
        }
            if let index = fileArray.firstIndex(where: { (ft) -> Bool in
                return ft.id
                 == id
            }) {
                currentIndex = index
            }
            else{
                currentIndex = 0
            }
        }
        nextvd?.removeFromSuperview()
        if fileArray.count > 0{
        if fileArray[currentIndex].id == "-1x" {
            
            nextvd = SpecTableView()

            if let ab = nextvd as? SpecTableView {
                ab.product = self.selectedProduct
                
            }
        }
        else{
        nextvd = SpecRightView()
        if let av = nextvd as? SpecRightView {
            av.webview.load(URLRequest(url: URL(string: "about:blank")!))
            if currentIndex < fileArray.count {
            if let url = URL(string: fileArray[currentIndex].getUrlString()) {
                av.url = url
                url.loadWebview(webview: av.webview)
               // av.webview.loadRequest(URLRequest(url: url))
            }
            }

        }
        }

        }
        else{
            nextvd = SpecRightView()
        }
        rightview.addSubview(nextvd!)
        nextvd?.fillSuperview()
        self.tableview.reloadData()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == searchFild {
            return true
        }
        showContent()
        return false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == selectContentTableView{
            return productArray.count
        }
       
        return fileArray.count

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectContentTableView {
            let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
            if productArray[indexPath.row].is_disable == 1{
                cell.title.textColor = .lightGray
            }
            else{
                cell.title.textColor = .black
            }
            cell.title.text = productArray[indexPath.row].title
            if indexPath.row == productArray.count - 1{
                cell.seperator.isHidden = true
            }
            else{
                cell.seperator.isHidden = false
            }
            return cell
        }
        let cell = ProductTabCell(style: .default, reuseIdentifier: "cell")
        print(991,fileArray[indexPath.row].title)
        cell.label.text = fileArray[indexPath.row].titles?.getLang()
        

        if indexPath.row == currentIndex{
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(77778)
        if tableView == tableview {
        currentIndex = indexPath.row
        setRightView()
        self.tableview.reloadData()
        }
        else{
            if productArray[indexPath.row].is_disable == 1{
                return
            }
            if let tab = tabBarController as? ProductInnerTabBarController {
                print(9997)
                tab.selectedProduct = productArray[indexPath.row]
                
                if let k = tab.parent as? ProductInnerInfoController {
                    k.product = productArray[indexPath.row]
                }
            }
            if self.selectedProduct?.id != productArray[indexPath.row].id
            {
                currentIndex = 0
            self.selectedProduct = productArray[indexPath.row]

                if let sr = self.selectedProduct {
                    print(113,sr.title)
                    self.setSpec(products: sr)
                }
            self.selectLabel.text = self.selectedProduct?.title
            
            
            self.setRightView()
            }
            self.popContent()
        }
        
       
    }


}
 
class selectTableCell : UITableViewCell {
    let title = UILabel()
    let seperator = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .white
        self.selectionStyle = .none
        contentView.addSubview(title)
        title.textColor = .black
        title.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        title.centerInSuperview()
        
        contentView.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.864822649, green: 0.864822649, blue: 0.864822649, alpha: 1)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductTabCell: UITableViewCell {
    let vd = UIView()
    let mc = UIView()
    let nd = UIView()
    let label = UILabel()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(mc)
        mc.backgroundColor = .clear
        mc.addShadowColor(opacity: 0.05)
//        mc.centerInSuperview(size: .init(width: 268.calcvaluex(), height: 72.calcvaluey()))
        mc.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 26.calcvaluex(), bottom: 6.calcvaluey(), right: 18.calcvaluex()))
        mc.addSubview(vd)
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.fillSuperview()
        vd.layer.cornerRadius = 15.calcvaluex()
        

        vd.clipsToBounds = true
        vd.addSubview(nd)
        nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        
        
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        nd.addSubview(imageview)
        imageview.anchor(top: nil, leading: nil, bottom: nil, trailing: nd.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        imageview.centerYInSuperview()
        imageview.isHidden = true
        
        nd.addSubview(label)
        label.anchor(top: nil, leading: nd.leadingAnchor, bottom: nil, trailing: imageview.leadingAnchor,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 5.calcvaluex()))
        label.centerYInSuperview()
        label.numberOfLines = 2
         
        
    }
    func setColor(){
        imageview.isHidden = false
        //vd.backgroundColor = MajorColor().mainColor
        mc.addShadowColor(opacity: 0.2)
    }
    func unsetColor(){
        imageview.isHidden = true
        //vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        mc.addShadowColor(opacity: 0.05)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
