//
//  ExpandProductController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/22.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class ExpandProductController: UIViewController,HotSpotDelegate {
    func sendHotspot(hotspot: Hotspot) {
        //
    }
    func sendFile(remote: String) {
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

    
        vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        guard let url = URL(string: remote) else {return}
        url.loadWebview(webview: vd.imgv)
        //vd.imgv.loadRequest(URLRequest(url: url))

        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
        
        imgv.hotspots = hotspot
    }
    func sendFile(file: Files?) {
        
        
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

    
        vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        
        
        if let mt = file {
            if let g = mt.getLang() {
                
                if g.count > 0 {
                    print(232,g[0].path)
                    switch g[0].path_url {
                    
                    case .string(let ft):
                        if let a = ft {
                            //fileString = a
                            guard let url = a.convertTofileUrl() else {return}

                            print(3321,url)
                            let request = URLRequest(url: url)
                            url.loadWebview(webview: vd.imgv)
                            //vd.imgv.loadRequest(request)
                        }
                    default:
                        ()
                    }
                }
            }
        }
        else{
            vd.imgv.load(URLRequest(url: URL(string: "about:blank")!))
        }
        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
        
        imgv.hotspots = hotspot

    }
    let newvd = UIView()
    let imgv = hotSpotView()
    let xbutton = UIButton(type: .custom)
    var hotspot = [Hotspot]()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        newvd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        newvd.addshadowColor(color: .white)
        newvd.layer.cornerRadius = 15.calcvaluex()
        view.addSubview(newvd)
        newvd.centerInSuperview(size: .init(width: 972.calcvaluex(), height: 688.calcvaluey()))
        
        newvd.addSubview(imgv)
        imgv.contentMode = .scaleAspectFit
        imgv.fillSuperview(padding: .init(top: 70.calcvaluex(), left: 70.calcvaluex(), bottom: 70.calcvaluex(), right: 70.calcvaluex()))
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        newvd.addSubview(xbutton)
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        xbutton.anchor(top: newvd.topAnchor, leading: nil, bottom: nil, trailing: newvd.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        xbutton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        imgv.selectedIndex = -1
        imgv.delegate = self
        imgv.moveRatio = 2.8
        imgv.scaleRatio = 1.5
        imgv.hotspots = hotspot
    }
    @objc func closeView(){
        self.dismiss(animated: false, completion: nil)
    }
}
