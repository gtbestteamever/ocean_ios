//
//  ProductFeatureController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/21.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import WebKit
protocol ExpandProductDelegate {
    func expandImage(index:Int,mapIndex:Int)
    func go360(index:Int,mapIndex:Int)
    func didScrolling()
    func endScrolling()
    func setNewProduct(product:Product)
}
extension String{
    func convertTofileUrl() -> URL?{
        if let url = URL(string: self.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""){
            print(2212,url.absoluteString)
            return url
        }
        
        return nil
    }
}
class ProductFeatureController:UIViewController {

    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let pdfView = UIView()
    let webview = WKWebView()
    let expandPdfButton = UIButton(type: .custom)
    let goToPageButton = UIButton(type: .custom)
    var preTitle = ""
    var delegate:ExpandProductDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear

        
        view.addSubview(collectionview)
        collectionview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 461.calcvaluex(), height: 0))
        collectionview.backgroundColor = .clear
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.isPagingEnabled = true
        collectionview.isScrollEnabled = false
        collectionview.register(FeatureCell.self, forCellWithReuseIdentifier: "cell")
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        
        pdfView.backgroundColor = .white
        pdfView.addshadowColor(color: .white)
        pdfView.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(pdfView)
        pdfView.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 537.calcvaluex(), height: 656.calcvaluey()))
        
        
        pdfView.addSubview(webview)
        
        webview.backgroundColor = .clear
        webview.navigationDelegate = self
        webview.contentMode = .scaleAspectFit
        webview.fillSuperview(padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        webview.layer.shadowOpacity = 0
//        if let pdf = Bundle.main.url(forResource: "samplePDF", withExtension: "pdf", subdirectory: nil, localization: nil)  {
//           let req = NSURLRequest(url: pdf)
//           webview.loadRequest(req as URLRequest)
//         }
        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)
        
        view.addSubview(expandPdfButton)
        expandPdfButton.anchor(top: pdfView.topAnchor, leading: nil, bottom: nil, trailing: pdfView.trailingAnchor,padding: .init(top: 478.calcvaluey(), left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        
        goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
        goToPageButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
//        view.addSubview(goToPageButton)
//        goToPageButton.anchor(top: expandPdfButton.bottomAnchor, leading: expandPdfButton.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
//        goToPageButton.layer.cornerRadius = 38.calcvaluex()/2
        

       
        
    }
    var topProduct : Product2?
    var product:Product?
    var products = [Product]()
    var originalProducts = [Product]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionview.isHidden = true
        if let tab = self.tabBarController as? ProductInnerTabBarController {
            product = tab.selectedProduct
            
            if let v = tab.parent as? ProductInnerInfoController {
                v.titleview.label.text = tab.selectedSeries?.titles?.getLang()
            }
//                if tab.selectedProduct != nil {
//
//
//                }
//                else{
//            product = tab.product?[tab.productIndex ?? 0].array[tab.productSubIndex ?? 0]?.product
//
//            if let k = tab.parent as? ProductInnerInfoController {
//                k.product = product
//            }
//                }
            
            
            if let product = product {
                products.append(product)
            }
//            for i in (tab.product ?? []).filter({ (pr) -> Bool in
//                return pr.status == 1
//            }){
//
//                for j in i.array {
//                    if var p = j {
//                        p.product.title = "\(i.title) \(p.product.titles?.getLang() ?? "")"
//                        p.product.is_disable = i.is_disable
//                        p.product.status = i.status
//                            pr.append(p.product)
//
//
//
//                    }
//                }
//
//            }
//            products = pr

            
        }
        
        originalProducts = products
        products = products.filter({ (pr) -> Bool in
            return pr.id == product?.id
        })
    
        print(771,products.count)
        webview.load(URLRequest(url: URL(string: "about:blank")!))
        self.collectionview.reloadData()
        
        if let index = products.firstIndex(where: { (p1) -> Bool in
            return p1.id == product?.id
        }) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.collectionview.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: false)
                
                self.collectionview.isHidden = false
            }
        }
        
        
    }
    @objc func expandPDF(){
        let con = ExpandPDFController(text: "")
        if let url = fileUrl{
            url.loadWebview(webview: con.imgv)
            //con.imgv.loadRequest(URLRequest(url: url))
        }
        
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    func removeShadow(webView: WKWebView) {
        for subview:UIView in webView.scrollView.subviews {
            subview.layer.shadowOpacity = 0
            for subsubview in subview.subviews {
                subsubview.layer.shadowOpacity = 0
            }
        }
    }
    var fileUrl: URL?
}
extension ProductFeatureController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WKNavigationDelegate,ExpandProductDelegate,HotSpotDelegate {
    func sendHotspot(hotspot: Hotspot) {
        //
    }
    
    func sendFile(remote: String) {
        print(990,remote)
        webview.load(URLRequest(url: URL(string: "about:blank")!))
        guard let url = URL(string: remote) else {return}
        
        if url.lastPathComponent == ".pdf" {
            goToPageButton.isHidden = false
        }
        else{
            goToPageButton.isHidden = true
        }
            fileUrl = url
        let request = URLRequest(url: url)
        //request.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        General().showActivity(vd: self.webview)
        url.loadWebview(webview: webview)
       // webview.loadRequest(request)
        
        
    }
    
    func setNewProduct(product: Product) {
        if let tab = tabBarController as? ProductInnerTabBarController {
            tab.selectedProduct = product
            if let k = tab.parent as? ProductInnerInfoController {
                k.product = product
            }
        }
        products.removeAll()
        products.append(product)
        webview.load(URLRequest(url: URL(string: "about:blank")!))
        self.collectionview.reloadData()

    }
    
    func sendFile(file: Files?) {
        print(7789)
        webview.load(URLRequest(url: URL(string: "about:blank")!))
        
        
        if let mt = file {
            if let g = mt.getLang() {
                
                if g.count > 0 {
                    
                    switch g[0].path_url {
                    
                    case .string(let ft):
                        if let a = ft {
                            //fileString = a
                            guard let url = a.convertTofileUrl() else {return}
                            
                            if url.lastPathComponent == ".pdf" {
                                goToPageButton.isHidden = false
                            }
                            else{
                                goToPageButton.isHidden = true
                            }
                                fileUrl = url
                            let request = URLRequest(url: url)
                            General().showActivity(vd: self.webview)
                            url.loadWebview(webview: webview)
                            //webview.loadRequest(request)
                        }
                    default:
                        ()
                    }
                }
            }
        }
        else{
            webview.load(URLRequest(url: URL(string: "about:blank")!))
        }
    }
    func didScrolling() {
        collectionview.isScrollEnabled = false
    }
    
    func endScrolling() {
        collectionview.isScrollEnabled = true
    }
    
    func go360(index:Int,mapIndex:Int) {
        let con = Show360Controller()
        if let tab = tabBarController as? ProductInnerTabBarController {
            if let k = tab.parent as? ProductInnerInfoController {  con.nLabel.text = "\(k.titleview.label.text ?? "")-\(self.products[index].titles?.getLang() ?? "")"
            }
        }
        
        con.assets = self.products[index].assets
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    
    func expandImage(index:Int,mapIndex:Int) {
        let con = ExpandProductController()

        con.imgv.img.sd_setImage(with: (self.products[index].maps?[mapIndex].image_url ?? "").convertTofileUrl(), completed: nil)
        
        if let hotspot =
            self.products[index].maps?[mapIndex].hotspots {
            
            con.hotspot = hotspot
        }
        
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        General().stopActivity(vd: self.webview)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeatureCell
        cell.productImgv.delegate = self
        cell.delegate = self
        cell.tag = indexPath.item
        cell.totalProduct = originalProducts
        products[indexPath.item].title = products[indexPath.item].title ?? ""
        cell.product = products[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return
            .init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }


}

class FeatureCell:UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    
    func setSpec(products:Product){
        if (products.specifications?.count ?? 0) == 0 {
          
            SpecArray.constructValue(field: [])
            return
        }
        for s in products.specifications ?? [] {
            print(3312,s.fields)
            print("-------")
            SpecArray.constructValue(field: s.fields)
            break
        }
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: selectContentTableView.self))!{
            return false
        }
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if totalProduct?[indexPath.row].is_disable == 1{
            return
        }
        popContent()
        if let pr = totalProduct?[indexPath.row] {
            setSpec(products: pr)
            delegate?.setNewProduct(product: pr)
 
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalProduct?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
       // if totalProduct?[indexPath.row].
        if totalProduct?[indexPath.row].is_disable == 1{
            cell.title.textColor = .lightGray
        }
        else{
            cell.title.textColor = .black
        }
        cell.title.text = totalProduct?[indexPath.row].title
        if indexPath.row == (totalProduct?.count ?? 0) - 1{
            cell.seperator.isHidden = true
        }
        else{
            cell.seperator.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product?.maps?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 198.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mapCollectionViewCell
        if indexPath.item == selectedIndex {
            cell.backgroundColor = #colorLiteral(red: 0.7637601495, green: 0.7638711333, blue: 0.7637358308, alpha: 1)
        }
        else{
            cell.backgroundColor = #colorLiteral(red: 0.9018597007, green: 0.902017653, blue: 0.9018613696, alpha: 1)
        }

        cell.image.sd_setImage(with: (product?.maps?[indexPath.row].image_url ?? "").convertTofileUrl(), completed: nil)
        return cell
    }
    var selectHeight : CGFloat = 0
    var totalProduct: [Product]? {
        didSet{
            selectHeight = CGFloat(totalProduct?.count ?? 0) * 84.calcvaluey()
            selectContentTableView.reloadData()
        }
    }
   
    var product : Product? {
        didSet{
            
            if let pr = product?.assets?.three360 {
                
                let kt = pr.filter { (file) -> Bool in
                    return file.files?.getLang() != nil || file.remotes?.getLang() != nil
                }
                if kt.count == 0 {
                    featureButton.setUnable()
                }
                else{
                    featureButton.setEnable()
                }
            }
            else{
                featureButton.setUnable()
            }
            productLabel.text = product?.titles?.getLang()

            if product?.maps != nil && product?.maps?.count != 0{
                productImgv.img.sd_setImage(with:  (product?.maps?[selectedIndex].image_url ?? "").convertTofileUrl()) { (image, _, _, _) in
                    self.productImgv.original_image = image ?? UIImage()
                self.productImgv.hotspots = self.product?.maps?[self.selectedIndex].hotspots ?? []
                    self.pageControl.isHidden = false
                self.pageControl.numberOfPages = self.product?.maps?.count ?? 0
            }
                
                }
            else{
                self.pageControl.isHidden = true
                productImgv.removeDotAndImage()
            }

            self.mapCollectionView.reloadData()
        }
    }
    var selectedIndex: Int = 0
    let labelSeperator = UIView()
    let productLabel = UILabel()
    let productImgv = hotSpotView()
    let featureButton = Feature360Button(type: .custom)
        let expandPdfButton = UIButton(type: .custom)
    var mapCollectionView : UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        let map = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        map.backgroundColor = .clear
        return map
    }()
    var menuButton : UIButton = {
        let menuButton = UIButton(type: .custom)
        menuButton.setImage(#imageLiteral(resourceName: "ic_arrow_down"), for: .normal)
        menuButton.contentVerticalAlignment = .fill
        menuButton.contentHorizontalAlignment = .fill
        
        return menuButton
    }()
    var delegate:ExpandProductDelegate?
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        productImgv.img.sd_setImage(with:  (product?.maps?[selectedIndex].image_url ?? "").convertTofileUrl(), completed: nil)
        productImgv.hotspots = product?.maps?[selectedIndex].hotspots ?? []
        print(7899)
        pageControl.currentPage = indexPath.row
        mapCollectionView.reloadData()
    }
    let pageControl = UIPageControl()
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
                labelSeperator.backgroundColor = MajorColor().mainColor
                labelSeperator.layer.cornerRadius = 6.calcvaluex()/2
                addSubview(labelSeperator)
                labelSeperator.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 32.5.calcvaluey(), left: 18.calcvaluex(), bottom: 0, right: 0),size: .init(width: 6.calcvaluex(), height: 46.5.calcvaluey()))
        //featureButton.isHidden = true
        addSubview(featureButton)
        featureButton.addTarget(self, action: #selector(go360), for: .touchUpInside)
        featureButton.addshadowColor(color: .white)
        featureButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 37.calcvaluey(), left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 122.calcvaluex(), height: 40.calcvaluey()))
                //productLabel.text = "NP16(2軸)"
                productLabel.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluex())
                productLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        menuButton.isHidden = true
        addSubview(menuButton)
        menuButton.anchor(top: nil, leading: nil, bottom: nil, trailing: featureButton.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        addSubview(productLabel)
        productLabel.numberOfLines = 2
        productLabel.anchor(top: topAnchor, leading: labelSeperator.trailingAnchor, bottom: nil, trailing: featureButton.leadingAnchor,padding: .init(top: 26.calcvaluey(), left: 18.calcvaluex(), bottom: 0, right: 4.calcvaluex()))
        menuButton.centerYAnchor.constraint(equalTo: productLabel.centerYAnchor).isActive = true
                
                productImgv.contentMode = .scaleAspectFill

        addSubview(productImgv)
        productImgv.anchor(top: productLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 45.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 0, height: 303.calcvaluey()))
        

        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        expandPdfButton.isHidden = false
        addSubview(expandPdfButton)
        expandPdfButton.anchor(top: nil, leading: nil, bottom: productImgv.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 23.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        expandPdfButton.addTarget(self, action: #selector(expandData), for: .touchUpInside)
        
        addSubview(mapCollectionView)
        mapCollectionView.anchor(top: productImgv.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 121.calcvaluey()))
        mapCollectionView.contentInset = .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0)
        mapCollectionView.delegate = self
        mapCollectionView.dataSource = self
        mapCollectionView.bounces = true
        mapCollectionView.register(mapCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        mapCollectionView.showsHorizontalScrollIndicator = false
        
        
        addSubview(pageControl)
        
        pageControl.currentPageIndicatorTintColor = MajorColor().mainColor
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        pageControl.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 86.calcvaluey(), right: 0),size: .init(width: 0, height: 8.calcvaluey()))

        selectContent.backgroundColor = .clear
 
        addSubview(selectContent)
        selectContent.addShadowColor(opacity: 0.1)
 
        selectContent.anchor(top: productLabel.bottomAnchor, leading: productLabel.leadingAnchor, bottom: nil, trailing: menuButton.trailingAnchor)
        
        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
        selectContentConstraint.isActive = true
        
        selectContent.addSubview(selectContentTableView)
        selectContentTableView.layer.cornerRadius = 5.calcvaluex()
        selectContentTableView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        selectContentTableView.fillSuperview()
        selectContentTableView.delegate = self
        selectContentTableView.dataSource = self
        
        selectContentTableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        addGestureRecognizer(tap)
    }
    @objc func popContent(){
        selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = selectHeight
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
    }
    @objc func showMenu(){
        showContent()
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.didScrolling()
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.endScrolling()
    }
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        delegate?.endScrolling()
//    }
    @objc func go360(){
        delegate?.go360(index:self.tag,mapIndex:self.selectedIndex)
    }
    @objc func expandData(){
        delegate?.expandImage(index:self.tag,mapIndex:self.selectedIndex)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class dotView : UIView {
    let label = UILabel()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_search_bar").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
       // layer.borderColor = UIColor.white.cgColor
       // layer.borderWidth = 2.calcvaluex()
        addshadowColor()
        
        backgroundColor = .white
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        label.textAlignment = .center
        addSubview(label)
        label.centerInSuperview()
        
        addSubview(imageview)
        imageview.backgroundColor = .clear
        imageview.contentMode = .scaleAspectFit
        imageview.fillSuperview(padding: .init(top: 6.calcvaluex(), left: 6.calcvaluex(), bottom: 6.calcvaluex(), right: 6.calcvaluex()))
        imageview.isHidden = true
    }
    func isSelected(){
        
        backgroundColor = MajorColor().mainColor
        label.textColor = .white
        imageview.image = imageview.image?.withRenderingMode(.alwaysTemplate)
        imageview.tintColor = .white
        
    }
    func unSelected(){
        
        backgroundColor = .white
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        imageview.image = imageview.image?.withRenderingMode(.alwaysOriginal)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol HotSpotDelegate {
    func sendFile(file:Files?)
    func sendFile(remote:String)
    func sendHotspot(hotspot:Hotspot)
}
extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }

        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }

        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0

        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}
class hotSpotView : UIView {
    var isFrequent = false
    var original_image = UIImage()
    let img = UIImageView()
    var delegate:HotSpotDelegate?
    var scaleRatio : CGFloat = 1
    var moveRatio : CGFloat = 1.05
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(img)
        img.fillSuperview()
        img.contentMode = .scaleAspectFit
    }
    var dotArray = [dotView]()
 
    @objc func didSelect(sender:UITapGestureRecognizer){
        
        if let vd = sender.view as? dotView {

            for i in dotArray {
                if vd.label.text == i.label.text {
                    i.isSelected()
                    if isFrequent {
                        self.delegate?.sendHotspot(hotspot: hotspots[vd.tag])
                    }
                    else{
                    if let remote = hotspots[vd.tag].remote {
                        
                        self.delegate?.sendFile(remote: remote)
                    }
                    else{
                    self.delegate?.sendFile(file: hotspots[vd.tag].files)
                    }
                    }
                }
                else{
                    i.unSelected()
                }
            }
        }
    }
    func removeDotAndImage(){
        self.img.image = nil
        for i in dotArray{
            i.removeFromSuperview()
        }
        dotArray.removeAll()
    }
    var selectedIndex = 0
    var hotspots = [Hotspot](){
        didSet{
            
            self.layoutIfNeeded()
            if img.image == nil{
                return
            }
            let rect = AVMakeRect(aspectRatio: img.image?.size ?? .zero, insideRect: img.bounds)
            
            //print(445,img.contentClippingRect.width,original_image.size.width)
            let widthratio = (rect.width)/(img.image?.size.width ?? 0)
            
            let heightratio = (rect.height)/(img.image?.size.height ?? 0)

            for i in dotArray{
                i.removeFromSuperview()
            }
            dotArray.removeAll()

            for (index,i) in hotspots.enumerated() {
                let dot = dotView()
                if isFrequent {
                    dot.label.isHidden = false
                    dot.imageview.isHidden = true
                }
                dot.label.text = i.coordinate?.id
                dot.label.font = UIFont(name: "Roboto-Medium", size: 18 * scaleRatio)
                if index == selectedIndex{
                    dot.isSelected()
                    if let remote = hotspots[0].remote {
                        self.delegate?.sendFile(remote: remote)
                    }
                    else{
                    self.delegate?.sendFile(file: hotspots[0].files)
                    }
                }
                else{
                    dot.unSelected()
                }
                dot.tag = index
                addSubview(dot)
                let x = CGFloat(NumberFormatter().number(from: i.coordinate?.left ?? "0") ?? 0)
                
                let y = CGFloat(truncating: NumberFormatter().number(from: i.coordinate?.top ?? "0") ?? 0)
                print(x,y,widthratio)
                let width = CGFloat(NumberFormatter().number(from: i.coordinate?.width ?? "0") ?? 0).calcvaluex()
                let height = CGFloat(NumberFormatter().number(from: i.coordinate?.height ?? "0") ?? 0).calcvaluex()
                dot.frame = .init(x: rect.origin.x + (x * widthratio)  , y: rect.origin.y + (y * heightratio) , width: width * scaleRatio , height: height * scaleRatio)
                dot.layer.cornerRadius = (height * scaleRatio)/2
                dot.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelect)))
                dotArray.append(dot)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class mapCollectionViewCell: UICollectionViewCell {
    let image = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9018597007, green: 0.902017653, blue: 0.9018613696, alpha: 1)
        
        addSubview(image)
        image.fillSuperview(padding: .init(top: 8.calcvaluex(), left: 8.calcvaluex(), bottom: 8.calcvaluex(), right: 8.calcvaluex()))
        image.contentMode = .scaleAspectFit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class Feature360Button : UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("360°展示".localized, for: .normal)
        setTitleColor(MajorColor().mainColor, for: .normal)
        titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        layer.cornerRadius = 20.calcvaluey()
        layer.borderWidth = 1.calcvaluex()
        addshadowColor()
        
        setUnable()
    }
    func setEnable(){
        //isHidden = false
        setTitleColor(.white, for: .normal)
        backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        self.isUserInteractionEnabled = true
        layer.borderColor = UIColor.clear.cgColor
    }
    func setUnable(){
        //isHidden = true
        backgroundColor = .white
        setTitleColor(#colorLiteral(red: 0.8966802214, green: 0.8966802214, blue: 0.8966802214, alpha: 1), for: .normal)
        layer.borderColor = #colorLiteral(red: 0.8966802214, green: 0.8966802214, blue: 0.8966802214, alpha: 1).cgColor
        self.isUserInteractionEnabled = false
//        setTitleColor(MajorColor().mainColor.withAlphaComponent(0.4), for: .normal)
//        layer.borderColor = MajorColor().mainColor.withAlphaComponent(0.4).cgColor
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
