//
//  ExpandPDFController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/22.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class ExpandPDFController: UIViewController,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        removeShadow(webView: webView)
    }
    let newvd = UIView()
    let imgv = WKWebView()
    let xbutton = UIButton(type: .custom)
    var stext : String?
    init(text:String) {
        super.init(nibName: nil, bundle: nil)
        stext = text
        if let url = URL(string: text) {
            var req = URLRequest(url: url)
            req.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
            url.loadWebview(webview: imgv)
            //imgv.loadRequest(req)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            newvd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
            newvd.addshadowColor(color: .white)
            newvd.layer.cornerRadius = 15.calcvaluex()
            view.addSubview(newvd)
            newvd.centerInSuperview(size: .init(width: 972.calcvaluex(), height: 688.calcvaluey()))
            
            newvd.addSubview(imgv)
            imgv.backgroundColor = .clear
            imgv.scrollView.showsVerticalScrollIndicator = false
            imgv.fillSuperview(padding: .init(top: 67.calcvaluey(), left: 67.calcvaluex(), bottom: 0, right: 67.calcvaluex()))
        
            imgv.navigationDelegate = self
            //imgv.delegate = self
            xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
            xbutton.tintColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
            newvd.addSubview(xbutton)
            xbutton.contentHorizontalAlignment = .fill
            xbutton.contentVerticalAlignment = .fill
            xbutton.anchor(top: newvd.topAnchor, leading: nil, bottom: nil, trailing: newvd.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
            xbutton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
//            if let pdf = Bundle.main.url(forResource: stext, withExtension: "pdf", subdirectory: nil, localization: nil)  {
//               let req = NSURLRequest(url: pdf)
//               imgv.loadRequest(req as URLRequest)
//             }
        }
        @objc func closeView(){
            self.dismiss(animated: false, completion: nil)
        }
    
func removeShadow(webView: WKWebView) {
    for subview:UIView in webView.scrollView.subviews {
        subview.layer.shadowOpacity = 0
        for subsubview in subview.subviews {
            subsubview.layer.shadowOpacity = 0
        }
    }
}
}
