//
//  ExpandFileController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 11/16/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class ExpandFileController: UIViewController{

    
    let newvd = UIView()
    let imgv = WKWebView()
    let xbutton = UIButton(type: .custom)
    var hotspot = [Hotspot]()
    let imagview = UIImageView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        newvd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        newvd.addshadowColor(color: .white)
        newvd.layer.cornerRadius = 15.calcvaluex()
        view.addSubview(newvd)
        newvd.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ?  972.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 688.calcvaluey()))

        newvd.addSubview(imgv)
        imgv.backgroundColor = .white
        imgv.isOpaque = false
        imgv.contentMode = .scaleAspectFit
        //imgv.scalesPageToFit = true
        if #available(iOS 13.0, *) {
            imgv.scalesLargeContentImage = true
        } else {
            // Fallback on earlier versions
        }
        imgv.fillSuperview(padding: .init(top: 70.calcvaluex(), left: 70.calcvaluex(), bottom: 70.calcvaluex(), right: 70.calcvaluex()))
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        newvd.addSubview(xbutton)
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        xbutton.anchor(top: newvd.topAnchor, leading: nil, bottom: nil, trailing: newvd.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        xbutton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        newvd.isHidden = true
        
        view.addSubview(imagview)
        imagview.anchor(top: imgv.topAnchor, leading: imgv.leadingAnchor, bottom: imgv.bottomAnchor, trailing: imgv.trailingAnchor)
        imagview.contentMode = .scaleAspectFit
        
        imagview.isHidden = true

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        newvd.transform = .init(scaleX: 0.5, y: 0.5)
        newvd.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.newvd.transform = .identity
        }
    }
    @objc func closeView(){
        self.dismiss(animated: false, completion: nil)
    }
}
