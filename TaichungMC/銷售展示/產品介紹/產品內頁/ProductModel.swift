//
//  ProductModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/24/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

struct Series2 {
    var title:Lang
    var products : [Product2]
    var assets:Assets?
    var status:Int?
    var is_disable:Int?
    init(title:Lang,products:[Product2],assets:Assets?,status:Int?,is_disable:Int?) {
        self.title = title
        self.products = products
        self.assets = assets
        self.status = status
        self.is_disable = is_disable
    }
}

struct Product2 {
    var title : String
    var array : [subProduct?]
    var num : Int
    var status:Int?
    var is_disable:Int?
    init(title:String,array:[Product],status:Int?,is_disable:Int?) {
        self.status = status
        self.is_disable = is_disable
        self.title = title
        var num = 0
        
        if array.count % 3 == 0{
            num = array.count/3
        }
        else{
            num = array.count/3 + 1
        }
        self.num = num
        self.array = []
        for i in array {
            self.array.append(subProduct(product: i))
        }
        for i in 0 ..< (num + array.count) {
            if i % 4 == 0{
                self.array.insert(nil, at: i)
            }
        }
    }
}

struct subProduct {
    var product:Product
    var isSelected : Bool
    init(product:Product,isSelected:Bool=false) {
        self.product = product
        self.isSelected = isSelected
    }
}

struct selectedProduct {
    var product:Product
    var seriesTitle:String
    var productTitle:String
    var seriesIndex:Int
    var productIndex:Int
    var subIndex:Int
    var title:String
    var assets:Assets?
}


