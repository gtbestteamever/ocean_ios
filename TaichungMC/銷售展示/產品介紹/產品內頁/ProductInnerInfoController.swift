//
//  ProductInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct ProductButtonModel {
    var name:String
    var selectedImage:UIImage
    var unselectedImage:UIImage
}
class ProductButton : UIView{
    let imgv = UIImageView()
    let seperator = UIView()
    let nLabel = UILabel()
    var pbm:ProductButtonModel?
    init(pbm:ProductButtonModel) {
        super.init(frame: .zero)
        self.pbm = pbm
        imgv.contentMode = .scaleAspectFit
        let tvView = UIView()
        tvView.addSubview(imgv)
        imgv.anchor(top: tvView.topAnchor, leading: tvView.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
//        if UserDefaults.standard.getConvertedLanguage() == "en" {
//            imgv.anchor(top: nil, leading: tvView.leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
//        }
//            else{
//                imgv.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 0, left: 80.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
//            }
//
//        imgv.centerYInSuperview()
        imgv.image = self.pbm?.unselectedImage
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)

        addSubview(seperator)
        seperator.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,size: .init(width: 1.calcvaluex(), height: 45.calcvaluey()))
        seperator.centerYInSuperview()
        nLabel.text = self.pbm?.name
        
        
        tvView.addSubview(nLabel)
        nLabel.anchor(top: tvView.topAnchor, leading: imgv.trailingAnchor, bottom: tvView.bottomAnchor, trailing: tvView.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        nLabel.heightAnchor.constraint(greaterThanOrEqualTo: imgv.heightAnchor).isActive = true
//        if UserDefaults.standard.getConvertedLanguage() == "en" {
//            nLabel.font = UIFont(name: "Roboto-Regular", size: 15.calcvaluex())
//            //nLabel.textAlignment = .center
//           // nLabel.numberOfLines = 2
//            //nLabel.anchor(top: nil, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 0))
//        }
//        else{
//            nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//           // nLabel.anchor(top: nil, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
//        }
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.numberOfLines = 0
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        nLabel.textAlignment = .center
       
        
       // nLabel.centerYInSuperview()
        
        addSubview(tvView)
        //tvView.backgroundColor = .red
        tvView.centerInSuperview()
        tvView.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor,constant: -20.calcvaluex()).isActive = true
    }

    func setSelect(){
        backgroundColor = MajorColor().mainColor
//        if UserDefaults.standard.getConvertedLanguage() == "en"{
//            nLabel.font = UIFont(name: "Roboto-Medium", size: 15.calcvaluex())
//        }
//        else{
//            nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
//        }
//        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.textColor = .white
        imgv.image = self.pbm?.selectedImage
        seperator.isHidden = true
    }
    func unSelect(){
        backgroundColor = .clear
//        if UserDefaults.standard.getConvertedLanguage() == "en"{
//            nLabel.font = UIFont(name: "Roboto-Regular", size: 15.calcvaluex())
//        }
//        else{
//            nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//        }
        //nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        imgv.image = self.pbm?.unselectedImage
        if tag != 6{
        seperator.isHidden = false
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductInnerTabBarController: UITabBarController{

    var productIndex:Int?
    var productSubIndex:Int?
    var product:[Product2]?
    let vd = UIView()
    let stackview = UIStackView()
    var selectedSeries : Series?
    var selectedPreTitle = ""
    var selectedProduct : Product?
    let productButtons = [ProductButton(pbm: ProductButtonModel(name: "機台展示".localized, selectedImage: #imageLiteral(resourceName: "ic_tab_features_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_features_normal"))),ProductButton(pbm: ProductButtonModel(name: "產品規格".localized, selectedImage: #imageLiteral(resourceName: "ic_tab_list_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_list_normal"))),ProductButton(pbm: ProductButtonModel(name: "選配項目".localized, selectedImage: #imageLiteral(resourceName: "ic_tab_optional_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_optional_normal"))),ProductButton(pbm: ProductButtonModel(name: "技術文件".localized, selectedImage: #imageLiteral(resourceName: "ic_tab_accuracy_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_accuracy_normal")))]
    var prevIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.isHidden = true
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addshadowColor(color: .white)

        view.addSubview(vd)
        vd.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 66.calcvaluey()))

        let vwidth = UIScreen.main.bounds.width/CGFloat(productButtons.count)
        for (index,i) in productButtons.enumerated() {
            
        vd.addSubview(i)
            if index == 0{
                i.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,size: .init(width: vwidth, height: 0))
            }
            else{
                i.anchor(top: vd.topAnchor, leading: productButtons[index-1].trailingAnchor, bottom: vd.bottomAnchor, trailing: nil,size: .init(width: vwidth, height: 0))
            }
            i.tag = index
            i.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goButton)))
            
        }
        productButtons[productButtons.count-1].seperator.isHidden = true
        productButtons[0].setSelect()

        let vd = ProductFeatureController()
//        let mt = SampleProductTabController(nextvd: SpecTableView(), mode: .Spec)
        let mt = ProductFunctionalityController(nextvd: SpecTableView(), mode: .Spec)
        viewControllers = [vd,mt,SelectEquipController(nextvd: SpecRightView(), mode: .Equip), ProductFunctionalityController(nextvd: SpecRightView2(), mode: .Functionality)]
    }
    @objc func goButton(sender:UITapGestureRecognizer){
        if prevIndex != 0{
        productButtons[prevIndex-1].seperator.isHidden = false
        }
        productButtons[prevIndex].unSelect()
        if let tag = sender.view?.tag {
            if tag != 0 {
            productButtons[tag - 1].seperator.isHidden = true
            }
            productButtons[tag].setSelect()
            
            self.selectedIndex = tag
            prevIndex = tag
            
            
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
                
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
    }
}
class ProductInnerInfoController: SampleController {
        override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    var seriesArray = [Series2](){
        didSet{
        }
    }
    var optionarray = [OptionObj]()
    var seriesIndex:Int?
    var productIndex: Int?
    var productSubIndex:Int?
    var slProduct: selectedProduct?
    var product : Product? {
        didSet{
            
            
            for (index,i) in seriesArray.enumerated() {
                for (index2,k) in i.products.enumerated() {
                    for (index3,m) in k.array.enumerated() {
                        if m?.product.id == product?.id {
                            slProduct = selectedProduct(product: m!.product, seriesTitle: i.title.getLang() ?? "", productTitle: k.title, seriesIndex: index, productIndex: index2, subIndex: index3, title: m?.product.titles?.getLang() ?? "", assets: i.assets)
                            break
                        }
                    }
                }
            }
        }
    }

    var titleviewanchor:AnchoredConstraints!
    var tabcon = ProductInnerTabBarController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingButton.isHidden = true
        backbutton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        let stackview = UIStackView()
        stackview.spacing = 16.calcvaluex()
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.alignment = .fill
        for (index,i) in ["","","建立訪談".localized].enumerated(){
            if index == 0 || index == 1{
                stackview.addArrangedSubview(UIView())
            }
            else{
            let button = UIButton(type: .custom)
            button.setTitle(i, for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
            button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
                button.constrainWidth(constant: i.widthOfString(usingFont: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!) + 64.calcvaluex())
            button.layer.cornerRadius = 19.calcvaluey()
                button.addTarget(self, action: #selector(goCreateInterview), for: .touchUpInside)
            stackview.addArrangedSubview(button)
            }
        }

        
        view.addSubview(tabcon.view)
        addChild(tabcon)
        tabcon.view.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        (tabBarController as? MainTabBarController)?.menuview.isHidden = true
    }
    @objc func goCreateInterview(){
        let vd = InterviewController()
        vd.modalPresentationStyle = .fullScreen
        vd.mode = .New
        if (product?.getProductsPrices()?.price == 0) || (product?.getProductsPrices()?.price == nil){
            let alertController = UIAlertController(title: "此產品不支援您的貨幣群組，請通知管理員".localized, message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        else{
        var pr = product
        let series = seriesArray[seriesIndex ?? 0]
        let topProduct = seriesArray[seriesIndex ?? 0].products[productIndex ?? 0]
            pr?.full_name = "\(series.title.getLang() ?? "") / \(topProduct.title) / \(product?.titles?.getLang() ?? "")"
        vd.addedMachine.append(AddedProduct(series: nil, sub_series: nil, product: pr, options: [], customOptions: [customForm(id: 0, status: -1, is_Cancel: false, statusArray: [], options: [])],fileAssets:[]))

       vd.savedOptions[0] = self.optionarray
        vd.addedMachine[0].options = []
        for i in self.optionarray {
            if i.selectedRow != 0{
                if let option = i.option?.children?[i.selectedRow]{
                    vd.addedMachine[0].options.append(SelectedOption(name: "\(i.option?.title ?? "") / \(option.title)", option: option,qty: 1))
                }
            }
        }
            vd.fieldsArray = [.Info,.Machine,.Discount]
        vd.createEditInterviewCell()
        vd.createMachineCell()
        self.present(vd, animated: true, completion: nil)
        }
    }
    var selectedProductArray = [selectedProduct]()
    
    @objc func goCompare(){
        let vd = ComparingItemController()
        if let sr = slProduct {
            selectedProductArray.append(sr)
        }
        //vd.selectedProductArray = selectedProductArray
           // vd.con = self
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    @objc func goCustomer(){
        let con = SelectCustomerController()
        
        con.modalPresentationStyle = .overFullScreen
        
        self.present(con, animated: false, completion: nil)
    }

}
