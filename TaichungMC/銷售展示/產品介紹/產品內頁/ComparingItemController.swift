//
//  ComparingItemController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/17.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension Double{
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%0.2f", self)
    }
    func converToPrice() -> String?{
        let price = self
        var dilutePrice : String = ""
        var unit = ""
        if price < 100{
            dilutePrice = "\(Int(price))"
        }
        else if price >= 100 && price < 1000{
            dilutePrice = "\(Int(price / 100))"
            unit = "百"
        }
        else if price >= 1000 && price < 10000{
            dilutePrice = "\(Int(price / 1000))"
            unit = "千"
        }
        else {
            dilutePrice = (price/10000).clean
            
            unit = "萬"
        }
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            return "$\(self)"
        }
        return "$\(self)"
    }
}
extension Int{

    func converToPrice() -> String?{
        let price = self
        var dilutePrice : String = ""
        var unit = ""
        if price < 100{
            dilutePrice = "\(Int(price))"
        }
        else if price >= 100 && price < 1000{
            dilutePrice = "\(Int(price / 100))"
            unit = "百"
        }
        else if price >= 1000 && price < 10000{
            dilutePrice = "\(Int(price / 1000))"
            unit = "千"
        }
        else {
            dilutePrice = String(price/10000)
            
            unit = "萬"
        }
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            return "$\(self)"
        }
        return "$\(self)"
    }
}
class ComparingItemController:SampleController {
    let stackview = UIStackView()
    let bottomTop = UIView()
    let comDetail = UITableView(frame: .zero, style: .grouped)
    var comDetailAnchor : AnchoredConstraints!
    private var lastContentOffset: CGFloat = 0
    var isBottom = false
    var filterOptionArray : [FilterSelection]? {
        didSet{
            if let _ = filterOptionArray {
                filterButton.setImage(#imageLiteral(resourceName: "ic_filtered"), for: .normal)
            }
            else{
                filterButton.setImage(UIImage(named: "ic_filter"), for: .normal)
            }
        }
    }
    let filterButton = UIButton(type: .custom)
    var selectedProductArray = [Product]()
    weak var con : OceanDetailProductController?
    var compareDetailArray = SpecArray.shared.value
    @objc func removeItem(sender:UIButton) {
        let removeItem = selectedProductArray.remove(at: sender.tag)
        print(992,selectedProductArray.count)
        SpecArray.setCompareFields(products: selectedProductArray)
        convertBottomDetail(origin: true)
        stackview.safelyRemoveArrangedSubviews()
        createStackView()
        con?.removeProduct = removeItem
        
    }
    func createStackView(){
        for (ind,i) in selectedProductArray.enumerated(){
            let vd = ItemView(i:i)
            if !UserDefaults.standard.getShowPrice() {
                vd.zLabel.isHidden = true
            }
            vd.cancelButton.tag = ind
            vd.cancelButton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
            vd.constrainWidth(constant: 198.calcvaluex())
            stackview.addArrangedSubview(vd)
        }
        for _ in 0 ..< 3 - selectedProductArray.count + 1 {
            print(772)
            stackview.addArrangedSubview(UIView())
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let contentView = UIView()
        contentView.backgroundColor = .white
        contentView.addshadowColor()
        contentView.layer.cornerRadius = 15.calcvaluex()
        view.addSubview(contentView)
        contentView.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 7.calcvaluey(), left: 25.calcvaluex(), bottom: menu_bottomInset + 20.calcvaluey(), right: 25.calcvaluex()))
        backbutton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        titleview.label.text = "產品比較".localized
        
        stackview.axis = .horizontal
        stackview.spacing = 34.calcvaluex()
        stackview.distribution = .fill
        
        createStackView()

        contentView.addSubview(stackview)
        stackview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 11.calcvaluey(), left: 244.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0.calcvaluex(), height: UserDefaults.standard.getShowPrice() ?  241.calcvaluey() : 203.calcvaluey()))
        
        bottomTop.backgroundColor = .clear

        //bottomTop.addshadowColor(color: .white)
 

//        contentView.addSubview(bottomTop)
//        comDetailAnchor = bottomTop.anchor(top: stackview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: -10.calcvaluey(), right: 0))
//        comDetail.layer.cornerRadius = 15.calcvaluey()
//        comDetail.layer.masksToBounds = true
        let seperator = UIView()
        seperator.backgroundColor = MajorColor().oceanColor
        contentView.addSubview(seperator)
        seperator.anchor(top: stackview.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
        comDetail.backgroundColor = .clear
        contentView.addSubview(comDetail)
        //comDetail.separatorStyle = .none
        comDetailAnchor = comDetail.anchor(top: seperator.bottomAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor)
        comDetail.delegate = self
        comDetail.dataSource = self
        //comDetail.bounces = false
        
        settingButton.isHidden = true
        topview.addSubview(filterButton)
        filterButton.setImage(#imageLiteral(resourceName: "ic_filter"), for: .normal)
        filterButton.contentHorizontalAlignment = .fill
        filterButton.contentVerticalAlignment = .fill
        
        filterButton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 46.calcvaluex(), height: 41.calcvaluex()))
        filterButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        filterButton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
        convertBottomDetail(origin: true)
    }
    
    func convertBottomDetail(origin:Bool){
        if origin {
        compareDetailArray = SpecArray.shared.value
        }
        print(9921,SpecArray.shared.value)
        for (index,m) in compareDetailArray.enumerated() {
            for (index2,j) in m.array.enumerated() {
                for (index3,i) in selectedProductArray.enumerated() {
                for k in i.specifications ?? [] {
                    for t in k.fields {
                        if t.code == j.decodeId {
                            compareDetailArray[index].array[index2].value[index3] = t.value
                        }
                    }
                }
            }
            }
        }
        
        comDetail.reloadData()

    }
    @objc func goFilter(){
        let con = ComparisonFilterController()
        con.filterOptionArray = self.filterOptionArray ?? []
        con.con = self
        //con.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(con, animated: true)
    }

}
extension ComparingItemController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compareDetailArray[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return compareDetailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CompareCell(style: .default, reuseIdentifier: "cell")
        let item = compareDetailArray[indexPath.section].array[indexPath.row]

        cell.headerLabel.text = item.title
        cell.unitLabel.text = item.unit
        if item.value[0] == "NA" {
            cell.firstLabel.text = ""
        }
        else{
        cell.firstLabel.text = item.value[0] == nil ? nil : "\(item.prefix ?? "")\(item.value[0] ?? "")"
        }
        if item.value[1] == "NA" {
            cell.secondLabel.text = ""
        }
        else {
        cell.secondLabel.text = item.value[1] == nil ? nil : "\(item.prefix ?? "")\(item.value[1] ?? "")"
        }
        
        if item.value[2] == "NA" {
            cell.thirdLabel.text = ""
        }
        else{
        cell.thirdLabel.text = item.value[2] == nil ? nil : "\(item.prefix ?? "")\(item.value[2] ?? "")"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CompareHeader()
        vd.headerLabel.text = compareDetailArray[section].title
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = compareDetailArray[indexPath.section].array[indexPath.row]
        return item.title.height(withConstrainedWidth: 102.calcvaluex(), font: UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!) + 28.calcvaluey()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

//        if scrollView.contentOffset.y < 0 {
//            scrollView.contentOffset.y = 0
//        }
//        if (self.lastContentOffset > scrollView.contentOffset.y) {
//            // move up
//            print(scrollView.contentOffset.y)
//            if scrollView.contentOffset.y <= 20.calcvaluey(){
//                if isBottom {
//                    self.changeStackView(down:false)
//                    isBottom = false
//                comDetailAnchor.top?.constant = 12.calcvaluey()
//                    UIView.animate(withDuration: 0.4, animations: {
//                        self.view.layoutIfNeeded()
//                    }) { (_) in
//
//                                   }
//
//                }
//            }
//
//        }
//        else if (self.lastContentOffset < scrollView.contentOffset.y) {
//           // move down
//            print("abc")
//            if !isBottom {
//                self.changeStackView(down:true)
//                isBottom = true
//                comDetailAnchor.top?.constant = UserDefaults.standard.getShowPrice() ? -82.calcvaluey() : -44.calcvaluey()
//
//
//
//                UIView.animate(withDuration: 0.4, animations: {
//                    self.view.layoutIfNeeded()
//                }) { (_) in
//
//                               }
//
//            }
//        }
//
//
//        // update the new position acquired
//        self.lastContentOffset = scrollView.contentOffset.y
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func changeStackView(down:Bool){
        for i in stackview.arrangedSubviews {
            if let vd = i as? ItemView{
                if down{
                //vd.nLabel.setTitle("NT$ 145萬", for: .normal)
                vd.nLabel.setTitleColor(MajorColor().mainColor, for: .normal)
                vd.nLabel.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 18.calcvaluex())
                }
                else{
                   // vd.nLabel.setTitle("高效率速軌", for: .normal)
                    vd.nLabel.setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
                    vd.nLabel.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
                }
            }
        }
    }
    
}
class CompareHeader2: UIView {
    let headerLabel = UILabel()
    let seperator = UIView()
    let valueLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        headerLabel.centerYInSuperview()
        headerLabel.text = "加工範圍"
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(valueLabel)
        valueLabel.anchor(top: nil, leading: centerXAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        valueLabel.centerYInSuperview()
        
        valueLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        valueLabel.textColor = #colorLiteral(red: 0.4391633868, green: 0.4392449856, blue: 0.4391643405, alpha: 1)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareHeader: UIView {
    let headerLabel = UILabel()
    let seperator = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        headerLabel.centerYInSuperview()
        headerLabel.text = "項目".localized
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        seperator.backgroundColor = MajorColor().mainColor
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 2.calcvaluey()))
        

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareCell:UITableViewCell {
    let headerLabel = UILabel()
     let seperator = UIView()
    let unitLabel = UILabel()
    let firstLabel = UILabel()
    let secondLabel = UILabel()
    let thirdLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
        selectedBackgroundView = vd
        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 102.calcvaluex(), height: 0))
        headerLabel.centerYInSuperview()
        headerLabel.text = "床面旋徑"
        headerLabel.numberOfLines = 0
        headerLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(unitLabel)
        unitLabel.text = "mm"
        unitLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        unitLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        unitLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 179.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 16.calcvaluey()))
        unitLabel.centerYInSuperview()
        
        firstLabel.text = "Ø570"
        secondLabel.text = "Ø650"
        thirdLabel.text = "Ø650"
        firstLabel.textAlignment = .center
        secondLabel.textAlignment = .center
        thirdLabel.textAlignment = .center
        firstLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        secondLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        thirdLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        firstLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        secondLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        thirdLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        
        addSubview(firstLabel)
        firstLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 244.calcvaluex(), bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 0))
        firstLabel.centerYInSuperview()
        
        addSubview(secondLabel)
        secondLabel.anchor(top: nil, leading: firstLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 34.calcvaluex(), bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 0))
        secondLabel.centerYInSuperview()
        
        addSubview(thirdLabel)
        thirdLabel.anchor(top: nil, leading: secondLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 34.calcvaluex(), bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 0))
        thirdLabel.centerYInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SpecCell:UITableViewCell {
    
    let headerLabel = UILabel()
     let seperator = UIView()
    let unitLabel = UILabel()
    let firstLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
        selectedBackgroundView = vd
        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        contentView.addSubview(headerLabel)
        headerLabel.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 36.calcvaluex(), bottom: 18.calcvaluey(), right: 0))
        headerLabel.numberOfLines = 0
        headerLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 200.calcvaluex()).isActive = true
        headerLabel.centerYInSuperview()
        //headerLabel.text = "床面旋徑"
        headerLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(unitLabel)

        unitLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        unitLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        unitLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 240.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 16.calcvaluey()))
        unitLabel.centerYInSuperview()
        
       

        
        firstLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())

        
        firstLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        firstLabel.textAlignment = .center
        
        addSubview(firstLabel)

        firstLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 350.calcvaluex(), bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 0))
        firstLabel.centerYInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemLabel : UIButton {

    init(text:String,price:Bool = false) {
        super.init(frame: .zero)
        if price {
            setTitleColor(MajorColor().mainColor, for: .normal)
            titleLabel?.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        }
        else{
            setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
            titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        }
        setTitle(text, for: .normal)

        titleLabel?.textAlignment = .center
        
        isUserInteractionEnabled = false
        layer.cornerRadius = 20.calcvaluey()
        backgroundColor = .white
        addshadowColor()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemView : UIView {
    let imgv = UIImageView()
        let cancelButton = UIButton(type: .custom)
    let nLabel = ItemLabel(text: "高效率速軌")
   // let sLabel = ItemLabel(text: "NP16-2軸")
    let zLabel = ItemLabel(text: "NT$ 145萬",price: true)
    init(i:Product) {
        super.init(frame: .zero)
        backgroundColor = .clear
        imgv.contentMode = .scaleAspectFit
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 159.calcvaluex(), height: 143.calcvaluey()))
        imgv.centerXInSuperview()
        imgv.sd_setImage(with: URL(string: "\(i.parent?.assets?.getMachinePath() ?? "")"), completed: nil)
        cancelButton.setImage(#imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate), for: .normal)
        cancelButton.backgroundColor = MajorColor().oceanColor
        cancelButton.tintColor = .white
         addSubview(cancelButton)
         cancelButton.contentHorizontalAlignment = .fill
         cancelButton.contentVerticalAlignment = .fill
        cancelButton.anchor(top: topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        
        cancelButton.layer.cornerRadius = 14.calcvaluex()


        
        addSubview(nLabel)
        nLabel.anchor(top: imgv.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 40.calcvaluey()))
        nLabel.setTitle(i.titles?.getLang(), for: .normal)
        nLabel.layer.borderColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        nLabel.layer.borderWidth = 1.calcvaluex()
        nLabel.setTitleColor(#colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1), for: .normal)
//        addSubview(sLabel)
//        sLabel.setTitle("\(i.productTitle)-\(i.title)", for: .normal)
//        sLabel.anchor(top: nLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 40.calcvaluey()))
        zLabel.backgroundColor = MajorColor().oceanColor
        zLabel.setTitleColor(.white, for: .normal)
        addSubview(zLabel)
        if let pr = i.getProductsPrices() {

            let price = pr.price ?? 0

            zLabel.setTitle("\(pr.currency) \(price.converToPrice() ?? "$0".localized)", for: .normal)
            
        }
        else{
            zLabel.setTitle("$0".localized, for: .normal)
        }
        

        zLabel.anchor(top: nLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 198.calcvaluex(), height: 40.calcvaluey()))
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
