//
//  ExpandVideoView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import AVKit
import WebKit
class ExpandVideoView: UIViewController {
    let wideView = UIView()
    let xbutton = UIButton(type: .custom)
    let webview = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wideView.backgroundColor = .white
        wideView.addShadowColor(opacity: 0.1)
        wideView.layer.cornerRadius = 15.calcvaluex()

        view.addSubview(wideView)
        wideView.centerInSuperview(size: .init(width: 972.calcvaluex(), height: 688.calcvaluey()))
        
        wideView.addSubview(webview)
        webview.centerInSuperview(size: .init(width: 800.calcvaluex(), height: 600.calcvaluey()))
        webview.isOpaque = false
        webview.backgroundColor = .clear
        xbutton.setImage(#imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .lightGray
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        
        wideView.addSubview(xbutton)
        xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        xbutton.anchor(top: wideView.topAnchor, leading: nil, bottom: nil, trailing: wideView.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
