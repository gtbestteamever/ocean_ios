//
//  ProductFunctionalityController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
enum ControllerMode {
    case Spec
    case Equip
    case Functionality
    case Precision
    case Diagram
    
}
class ProductFunctionalityController: SampleProductTabController {
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableviewanchor?.top?.constant = -58.calcvaluey()
        //searchFild.isHidden = true
    }
    
    override func setRightView() {
        
        rightview.subviews.forEach({$0.removeFromSuperview()})
        nextvd = nil
        
        if mode == .Functionality{
        fileArray = selectedProduct?.assets?.performance ?? []
        }
        else if mode == .Precision {
            fileArray = selectedProduct?.assets?.accuracy ?? []
        }
        else if mode == .Diagram {
            fileArray = selectedProduct?.assets?.diagram ?? []
        }
        else if mode == .Spec{
            print(331,selectedProduct?.assets?.specification)
            fileArray = selectedProduct?.assets?.specification ?? []
            fileArray.insert(FilePath(id: "-1x", remote: "", files: nil, tags: nil, title: "產品規格".localized,titles: Lang(en: "Specification", zhTW: "產品規格")), at: 0)
        }
        
        originalFileArray = fileArray
       // textFieldDidEndEditing(searchFild)

        if fileArray.count > 0{
        if fileArray[currentIndex].id == "-1x" {
            
            nextvd = SpecTableView()

            if let ab = nextvd as? SpecTableView {
                ab.product = self.selectedProduct
            }
        }
        else{
            if mode == .Equip{
                nextvd = SpecRightView()
            }
            else{
        nextvd = SpecRightView2()
            }
            
        if let av = nextvd as? SpecRightView {
            print(113)
            av.product = self.selectedProduct
            av.webview.load(URLRequest(url: URL(string: "about:blank")!))
            if currentIndex < fileArray.count {
            if let url = URL(string: fileArray[currentIndex].getUrlString()) {
                av.url = url
                General().showActivity(vd: av.webview)
                url.loadWebview(webview: av.webview)
                //av.webview.loadRequest(URLRequest(url: url))
                
            }
            }

        }
            if let av = nextvd as? SpecRightView2 {
                
                av.product = self.selectedProduct
                av.webview.load(URLRequest(url: URL(string: "about:blank")!))
                if currentIndex < fileArray.count {
                if let url = URL(string: fileArray[currentIndex].getUrlString()) {
                    av.url = url
                    General().showActivity(vd: av.webview)
                    url.loadWebview(webview: av.webview)
                    //av.webview.loadRequest(URLRequest(url: url))
                    
                }
                }

            }
        }

        }
        else{
            if mode == .Equip{
                nextvd = SpecRightView()
            }
            else{
        nextvd = SpecRightView2()
            }
        }

        UIView.setAnimationsEnabled(false)
        rightview.addSubview(nextvd!)
        nextvd?.fillSuperview()
        
        self.selectContentTableView.reloadData()
        self.tableview.reloadData()
        //print(789,selectedProduct?.assets?.performance)
        
        //UIView.setAnimationsEnabled(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.setAnimationsEnabled(true)
        }
        

        
    }
    @objc func goPlay(){
        print(999)
        
        let videoview = ExpandVideoView()
        
        videoview.modalPresentationStyle = .overFullScreen
        
        self.present(videoview, animated: false, completion: nil)
    }
}
