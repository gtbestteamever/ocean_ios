//
//  VideoPlayerView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class VideoPlayerView: UIView {
    let videolayer = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
    let playButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        videolayer.clipsToBounds = true
        videolayer.contentMode = .scaleAspectFill
        addSubview(videolayer)
        videolayer.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 0, bottom: 92.calcvaluey(), right: 26.calcvaluex()))
        
        videolayer.addSubview(playButton)
        playButton.contentHorizontalAlignment = .fill
        playButton.contentVerticalAlignment = .fill
        playButton.setImage(#imageLiteral(resourceName: "ic_video_piay"), for: .normal)
        
        playButton.centerInSuperview(size: .init(width: 78.calcvaluex(), height: 78.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
