//
//  SpecRightView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class SpecRightView: SpecTableView2,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        General().stopActivity(vd: self.webview)
    }
//    func webViewDidFinishLoad(_ webView: WKWebView) {
//        General().stopActivity(vd: self.webview)
//    }
    
    let vt = UIView()
    var url : URL?
    let webview = WKWebView()
    let expandPdfButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        comDetail.isHidden = true
        vertStackview.addArrangedSubview(webview)
        //comDetail.removeFromSuperview()
//        vt.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
//        vt.layer.cornerRadius = 15.calcvaluex()
//
//        bottomTop.addSubview(vt)
//        vt.fillSuperview()
        
       // vt.addSubview(webview)
        webview.contentMode = .scaleAspectFit
        webview.navigationDelegate = self
        
        webview.backgroundColor = .clear
//        webview.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 8.calcvaluex(), bottom: 8.calcvaluex(), right: 8.calcvaluex()))
        webview.scrollView.showsVerticalScrollIndicator = false
        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        addSubview(expandPdfButton)
        expandPdfButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 30.calcvaluey()
                                                                                                                     , right: 36.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
//
//        vt.addSubview(imgv)
//        imgv.anchor(top: vt.topAnchor, leading: vt.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 35.calcvaluex(), bottom: 0, right: 0),size: .init(width: 254.32.calcvaluex(), height: 367.calcvaluey()))
//        imgv.contentMode = .scaleAspectFill
//        imgv.clipsToBounds = true
//
//        firstLabel.font = UIFont(name: "Roboto-Medium", size: 28.calcvaluex())
//        firstLabel.text = "心軸行程: 80mm"
//        firstLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//
//        vt.addSubview(firstLabel)
//        firstLabel.anchor(top: vt.topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 45.66.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
//
//        secondLabel.font = UIFont(name: "Roboto-Medium", size: 28.calcvaluex())
//        secondLabel.text = "心軸內孔錐度: M.T. #3"
//        secondLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//
//        vt.addSubview(secondLabel)
//        secondLabel.anchor(top: firstLabel.bottomAnchor, leading: firstLabel.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 38.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func expandPDF(){
        let con = ExpandPDFController(text: "")
        
        if let url = self.url{
            url.loadWebview(webview: con.imgv)
            //con.imgv.loadRequest(URLRequest(url: url))
        }
        
        con.modalPresentationStyle = .overFullScreen
        General().getTopVc()?.present(con, animated: false, completion: nil)
    }
}

class SpecRightView2: SpecTableView,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        General().stopActivity(vd: self.webview)
    }
    
    let vt = UIView()
    var url : URL?
    let webview = WKWebView()
    let expandPdfButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        comDetail.removeFromSuperview()
        vt.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vt.layer.cornerRadius = 15.calcvaluex()
        
        bottomTop.addSubview(vt)
        vt.fillSuperview()
        
        vt.addSubview(webview)
        webview.contentMode = .scaleAspectFit
        webview.navigationDelegate = self
        
        webview.backgroundColor = .clear
        webview.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 8.calcvaluex(), bottom: 8.calcvaluex(), right: 8.calcvaluex()))
        webview.scrollView.showsVerticalScrollIndicator = false
        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        addSubview(expandPdfButton)
        expandPdfButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 120.calcvaluey()
                                                                                                                     , right: 36.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
//
//        vt.addSubview(imgv)
//        imgv.anchor(top: vt.topAnchor, leading: vt.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 35.calcvaluex(), bottom: 0, right: 0),size: .init(width: 254.32.calcvaluex(), height: 367.calcvaluey()))
//        imgv.contentMode = .scaleAspectFill
//        imgv.clipsToBounds = true
//
//        firstLabel.font = UIFont(name: "Roboto-Medium", size: 28.calcvaluex())
//        firstLabel.text = "心軸行程: 80mm"
//        firstLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//
//        vt.addSubview(firstLabel)
//        firstLabel.anchor(top: vt.topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 45.66.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
//
//        secondLabel.font = UIFont(name: "Roboto-Medium", size: 28.calcvaluex())
//        secondLabel.text = "心軸內孔錐度: M.T. #3"
//        secondLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//
//        vt.addSubview(secondLabel)
//        secondLabel.anchor(top: firstLabel.bottomAnchor, leading: firstLabel.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 38.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func expandPDF(){
        let con = ExpandPDFController(text: "")
        
        if let url = self.url{
            url.loadWebview(webview: con.imgv)
           // con.imgv.loadRequest(URLRequest(url: url))
        }
        
        con.modalPresentationStyle = .overFullScreen
        General().getTopVc()?.present(con, animated: false, completion: nil)
    }
}

