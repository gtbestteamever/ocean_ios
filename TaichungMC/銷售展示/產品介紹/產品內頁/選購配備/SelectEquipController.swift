//
//  SelectEquipController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
protocol EquipDelegate {
    func goEquip(index:Int,innerTag:Int)
}
struct OptionObj {
    var selectedRow : Int = 0
    var option : Option?
}
class SelectEquipController: SampleProductTabController,EquipDelegate {
    func goEquip(index:Int,innerTag: Int) {
        optionsArray[index].selectedRow = innerTag
        //innerIndex[index] = innerTag
        
        for i in optionsArray {
            if let index = originaloptions.firstIndex(where: { (obj) -> Bool in
                return obj.option?.id == i.option?.id
            }) {
                originaloptions[index].selectedRow = i.selectedRow
            }
        }
        currentIndex = index
        loadFile(inIndex: innerTag)
        calculatePrice()
        if let tab = tabBarController as? ProductInnerTabBarController {
            
            
            if let k = tab.parent as? ProductInnerInfoController {
                k.optionarray = optionsArray
            }
        }
        tableview.reloadData()
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            self.optionsArray = self.originaloptions
        }
        else{
            self.optionsArray = self.originaloptions.filter { (op) -> Bool in
                if let option = op.option,let txt = textField.text{
                    if option.title.lowercased().contains(txt.lowercased()) {
                        return true
                    }
                }
            
                if let _ = op.option?.children?.firstIndex(where: { (op) -> Bool in
                return op.title.lowercased().contains((textField.text ?? "").lowercased())
            }) {
                return true
            }
            return false
        }
        }
        //setOptions()
        tableview.reloadData()
    }
    func loadFile(inIndex:Int) {
        print(771,inIndex)
        
        if let nt = nextvd as? SpecRightView{
            nt.webview.load(URLRequest(url: URL(string: "about:blank")!))
            if currentIndex < self.optionsArray.count {
               
            if let url = URL(string: self.optionsArray[currentIndex].option?.children?[inIndex].getUrlString() ?? "") {
                
                nt.url = url
                let request = URLRequest(url: url)
                General().showActivity(vd: nt.webview)
                url.loadWebview(webview: nt.webview)
                //nt.webview.loadRequest(request)
            }
            }
            //nt.webview.loadRequest()
        }
    }
    var optionsArray = [OptionObj]()
    //var innerIndex = [Int]()
//    var options = [Option]() {
//        didSet{
//            self.tableview.reloadData()
//        }
//    }
    var originaloptions = [OptionObj]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableview.estimatedRowHeight = UITableView.automaticDimension;
        self.tableview.rowHeight = UITableView.automaticDimension;
        setOptions()
        
    }
    func calculatePrice(){
       
        var total = currentPrice
        
        for (ind,val) in originaloptions.enumerated() {

            if let pr = originaloptions[ind].option?.children?[val.selectedRow].getProductsPrices() {
                if let pri = pr.price {
                    total += pri
                }
            }
        }
        if let right = nextvd as? SpecRightView {
            right.priceLabel.text = "\("選配價格".localized)："
            if total != 0{
                right.priceLabel.text! += " \(total.converToPrice() ?? "$0".localized)"
            }
            else{
                right.priceLabel.text! += "$0".localized
            }
            right.priceLabel.text! += " \(currency)"
        }
        


    }
    var currentPrice : Int = 0
    var currency = ""
    func setOptions(){

        var options = (selectedProduct?.options?[0].children ?? []).sorted(by: { (op1, op2) -> Bool in
            return op1.order < op2.order
        }).filter { (op) -> Bool in
            if (op.children?.count ?? 0) > 1{
                return true
            }
            return false
        }
       
        for (ind,i) in options.enumerated() {
            let children = i.children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })
            options[ind].children = children
        }
        //self.originaloptions = options
        
        //optionsArray = []
        if optionsArray.isEmpty {
        for var i in options {
            i.children = i.children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })
            optionsArray.append(OptionObj(selectedRow: 0, option: i))
        }
        }
        
        self.originaloptions = optionsArray
        
        textFieldDidEndEditing(searchFild)
        if let right = nextvd as? SpecRightView {
            
            if let price = selectedProduct?.getProductsPrices(){
                
                currentPrice = price.price ?? 0
                right.priceLabel.text = "\(price.currency) \((price.price ?? 0).converToPrice() ?? "0元".localized)"
                    
                currency = price.currency
                
            }
            else{
                currentPrice = 0
                right.priceLabel.text = "0元".localized
            }
            
            
            
        }
        self.loadFile(inIndex: 0)
        self.calculatePrice()
        tableview.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        leftviewAnchor?.width?.constant = 361.calcvaluex()
        self.view.layoutIfNeeded()
        
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == selectContentTableView {
            return 84.calcvaluey()
        }
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == selectContentTableView {
            return productArray.count
        }
        return optionsArray.count
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectContentTableView {
            let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
            cell.title.text = productArray[indexPath.row].title
            if indexPath.row == productArray.count - 1{
                cell.seperator.isHidden = true
            }
            else{
                cell.seperator.isHidden = false
            }
            return cell
        }
        if tableView == tableview {
            let cell = EquipCell(option: self.optionsArray[indexPath.row].option!)
            
            //cell.frame = tableView.bounds
        cell.tag = indexPath.row
        cell.delegate = self

        return cell
        }
        else{
            let cell = UITableViewCell(style: .default, reuseIdentifier: "ct")
            cell.backgroundColor = .white
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? EquipCell {
            
            if indexPath.row == currentIndex{
                cell.setColor()
            }
            else{
                cell.unsetColor()
            }
            cell.setSelected(index: optionsArray[indexPath.row].selectedRow)
        }
    }
    //var preProduct : Product?
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableview {
            currentIndex = indexPath.row
            loadFile(inIndex: optionsArray[currentIndex].selectedRow)
            tableview.reloadData()
        }
        else{
            if selectedProduct?.id != productArray[indexPath.row].id{
                //preProduct = productArray[indexPath.row]
                self.currentIndex = 0
            self.selectedProduct = productArray[indexPath.row]
            self.selectLabel.text = self.selectedProduct?.title
                textFieldDidEndEditing(searchFild)
                optionsArray = []
                setOptions()
            }
            if let tab = tabBarController as? ProductInnerTabBarController {
                print(9997)
                tab.selectedProduct = productArray[indexPath.row]
                
                if let k = tab.parent as? ProductInnerInfoController {
                    k.product = productArray[indexPath.row]
                }
            }
            self.popContent()
            
            //self.setRightView()
        }
    }
}
class EquipCellSelectView : UIView {
    let imgv = UIButton(type: .custom)
    let nLabel = UILabel()
    let priceLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        imgv.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        imgv.setImage(#imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate), for: .selected)
        imgv.tintColor = #colorLiteral(red: 0.2697770298, green: 0.2701570988, blue: 1, alpha: 1)
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        //imgv.centerYInSuperview()
        
        nLabel.text = "6’’億川"
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        addSubview(nLabel)
        nLabel.numberOfLines = 0
        nLabel.anchor(top: imgv.topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        nLabel.trailingAnchor.constraint(equalTo: self.centerXAnchor,constant: 6.calcvaluex()).isActive = true
        
        
        priceLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        priceLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 22.calcvaluex()),size: .init(width: 0, height: 25.calcvaluey()))
        priceLabel.centerYInSuperview()
    }
    
    func setImgvSelected(){
        imgv.isSelected = true
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        priceLabel.textColor = MajorColor().mainColor
    }
    func unSelectedImgv(){
        imgv.isSelected = false
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        priceLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class EquipCell:UITableViewCell {
    let topView = UIView()
    let topLabel = UILabel()
    let stackview = UIStackView()
    var delegate:EquipDelegate?
    init(option:Option) {
        //super.init(style: style, reuseIdentifier: reuseIdentifier)
        super.init(style: .default, reuseIdentifier: "eq")
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(topView)
        topView.addshadowColor()
        topView.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 3.calcvaluey(), right: 26.calcvaluex()))
        topView.backgroundColor = #colorLiteral(red: 0.8707719445, green: 0.9054962993, blue: 1, alpha: 1)
        topView.layer.cornerRadius = 15.calcvaluex()
        
        let vd = UIView()
        vd.backgroundColor = .white
        vd.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            vd.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        vd.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 21.calcvaluey(), left: 24.calcvaluex(), bottom: 28.calcvaluey(), right: 24.calcvaluex()))



        stackview.axis = .vertical
        stackview.distribution = .fill
        //stackview.alignment = .leading
        stackview.spacing = 14.calcvaluey()
        layoutIfNeeded()
        print(221,stackview.frame)
        for (ind,i) in (option.children ?? []).sorted(by: { (op1, op2) -> Bool in
            return op1.order < op2.order
        }).enumerated() {
            let vc = EquipCellSelectView()
            vc.nLabel.text = i.titles?.getLang()
//            if i.prices != nil && i.prices?.count != 0{
//                let st = NumberFormatter()
//                st.numberStyle = .decimal
//                vc.priceLabel.text = "+\(st.string(from: NSNumber(value: i.prices?[0].price ?? 0)) ?? "")元"
//            }
            
            if let i = i.getProductsPrices() {
                let st = NumberFormatter()
                st.numberStyle = .decimal
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    vc.priceLabel.text = "+$\(st.string(from: NSNumber(value: i.price ?? 0)) ?? "")"
                }
                else{
                vc.priceLabel.text = "+\(st.string(from: NSNumber(value: i.price ?? 0)) ?? "")元"
                }
            }
            //vc.priceLabel.text =
            vc.nLabel.tag = ind
            vc.nLabel.isUserInteractionEnabled = true
            vc.nLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textSelect)))
            vc.imgv.tag = ind
            vc.setImgvSelected()
            vc.imgv.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
            //vc.priceLabel.isHidden = true
            vc.constrainHeight(constant: i.titles?.getLang()?.height(withConstrainedWidth: 115.calcvaluex(), font: vc.nLabel.font) ?? 0)
            stackview.addArrangedSubview(vc)

        }
        topView.addSubview(vd)
        vd.anchor(top: topView.topAnchor, leading: topView.leadingAnchor, bottom: topView.bottomAnchor, trailing: topView.trailingAnchor,padding: .init(top: 42.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
        topView.addSubview(topLabel)
        topLabel.anchor(top: topView.topAnchor, leading: topView.leadingAnchor, bottom: vd.topAnchor, trailing: topView.trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        topLabel.textAlignment = .center
        topLabel.text = option.titles?.getLang()
    }
    @objc func textSelect(sender:UITapGestureRecognizer) {
        print(367,sender.view!.tag)
        delegate?.goEquip(index: self.tag, innerTag: sender.view?.tag ?? 0)
    }
    @objc func buttonSelected(sender:UIButton){
        delegate?.goEquip(index: self.tag, innerTag: sender.tag)
    }
    func setSelected(index:Int){
        for (indext,i) in stackview.arrangedSubviews.enumerated() {
            if let vd = i as? EquipCellSelectView {
                if indext == index{
                    vd.setImgvSelected()
                }
                else{
                    vd.unSelectedImgv()
                }
                
            }
        }

    }
    func setUnselected(index:Int){
        if let vd = stackview.arrangedSubviews[index] as? EquipCellSelectView {
            vd.unSelectedImgv()
        }
    }
    func setColor() {
        topView.backgroundColor = MajorColor().oceanSubColor
        //nd.clipsToBounds = true
        topLabel.textColor = .white
    }
    func unsetColor() {
        topView.backgroundColor = #colorLiteral(red: 0.8707719445, green: 0.9054962993, blue: 1, alpha: 1)
        topLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        //nd.clipsToBounds = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
