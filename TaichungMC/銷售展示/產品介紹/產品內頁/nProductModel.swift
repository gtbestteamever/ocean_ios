//
//  ProductModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/21/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct TopProducts : Codable {
    var data : [Product]
}
struct PLocation: Codable {
    var title : String
}
struct PRadius : Codable {
    var title : String
}
struct Audits : Codable {
    var id : Int
    var status : Int
    
    var content : [AuditContent]
    var assets : Assets?
}
struct AuditContent :Codable {
    var type : Int
    var content: String?
    var qty : Int?
    var price : String?
    //要加入
    var reply : String?
    var id : String?
    var files : [AddedOptionFile]?
}

struct Product : Codable {
    var status : Int?
    var full_name : String?
    var company_code : String?
    var is_disable:Int?
    //要加入
    var product_id : String?
    var id : String
    var order: Int?
    var title:String?
    var titles: Lang?
    var description:[String]?
   var descriptions : LangArray?
    var model_number : String?
    var options : [Option]?
    var prices : [PriceGroup]?
    var price_groups : [String:String]?
    var specifications : [Specification]?
    var maps: [Map]?
    var assets : Assets?
    var audit : [Audits]?
    var qty : Int?
    var export_files : [InterviewPreviewFile]?
    var selected : Bool = false
    var parent : Series?
    private enum CodingKeys : String,CodingKey {
        case status,full_name,company_code,is_disable,product_id,id,order,title,titles,description,descriptions,model_number,options,prices,price_groups,specifications,maps,assets,audit,qty,export_files
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try? container.decode(Int?.self,forKey: .status)
        full_name = try? container.decode(String?.self,forKey: .full_name)
        company_code = try? container.decode(String?.self,forKey: .company_code)
        is_disable = try? container.decode(Int?.self,forKey: .is_disable)
        product_id = try? container.decode(String?.self,forKey: .product_id)
        
        order = try? container.decode(Int?.self,forKey: .order)
        title = try? container.decode(String?.self,forKey: .title)
        titles = try? container.decode(Lang?.self,forKey: .titles)
        description = try? container.decode([String]?.self,forKey: .description)
        
        model_number = try? container.decode(String?.self,forKey: .model_number)
        options = try? container.decode([Option]?.self,forKey: .options)
        prices = try? container.decode([PriceGroup]?.self,forKey: .prices)
        price_groups = try? container.decode([String:String]?.self,forKey: .price_groups)
        specifications = try? container.decode([Specification]?.self,forKey: .specifications)
        maps = try? container.decode([Map]?.self,forKey: .maps)
        
        
        audit = try? container.decode([Audits]?.self,forKey: .audit)
        qty = try? container.decode(Int?.self,forKey: .qty)
        export_files = try? container.decode([InterviewPreviewFile]?.self,forKey: .export_files)
        
        do{
            
            assets = try container.decode(Assets?.self,forKey: .assets)
            
            id = try String(container.decode(Int.self, forKey: .id))
            
        }
        catch let error{
          
            id = try container.decode(String.self, forKey: .id)
        }
        
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(company_code, forKey: .company_code)
        try container.encode(order, forKey: .order)
        try container.encode(status, forKey: .status)
        //要加入
        try container.encode(product_id, forKey: .product_id)
        //try container.encode(del_audit, forKey: .del_audit)
        try container.encode(is_disable, forKey: .is_disable)
        
        try container.encode(price_groups, forKey: .price_groups)
        try container.encode(audit, forKey: .audit)
        try container.encode(specifications, forKey: .specifications)
        try container.encode(maps, forKey: .maps)
        try container.encode(full_name, forKey: .full_name)
        
        
       
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        
        try container.encode(description, forKey: .description)
        try container.encode(descriptions, forKey: .descriptions)
        
        try container.encode(model_number, forKey: .model_number)
        try container.encode(options, forKey: .options)
        
        try container.encode(prices, forKey: .prices)
        try container.encode(assets, forKey: .assets)
        
        try container.encode(qty, forKey: .qty)
        try container.encode(export_files,forKey: .export_files)
    }
    func getProductsPrices(f_currency:String? = nil,f_group:String? = nil) -> PriceGroup?{
        var c_currency = ""
        var c_group = ""
        if let f_c = f_currency, let f_g = f_group {
            c_currency = f_c
            c_group = f_g
        }
        else{
            c_currency = UserDefaults.standard.getChosenCurrency()
            c_group = UserDefaults.standard.getChosenGroup()
        }

        if c_currency != "" && c_group != ""{

                    if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == c_currency && pr.group == c_group
                    }) {
                        return price
                    }
                    else if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == c_currency && pr.group == "X"
                    }) {
                        return price
                    }
  
        }
        
        return nil
        
    }
}
struct Option : Codable {
    var id : String
    var title:String
    var order:Int
    var titles:Lang?
    var prices : [PriceGroup]?
    var remote : String?
    var remotes : Lang?
    var files : Files?
    var children : [Option]?
    var selected : Bool?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(id, forKey: .id)
        try container.encode(order, forKey: .order)
        try container.encode(titles, forKey: .titles)
        try container.encode(prices, forKey: .prices)
        try container.encode(remote, forKey: .remote)
        try container.encode(files, forKey: .files)
        try container.encode(children, forKey: .children)
        try container.encode(remotes, forKey: .remotes)
        try container.encode(selected, forKey: .selected)
        
    }
    
    
    func getUrlString() -> String {
        print(331,remotes)
        if let remote = remotes?.getFileLang() {
            return remote
        }
        
        if let m = files?.getLang() {
            if let ab = m.first {
                switch ab.path_url {
                case .string(let fr):
                    return fr?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                case .arrayString(let fr):
                    if let mt = fr?.first {
                        return mt?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                    }
                    
                default:
                    ()
                }
                
            }
        }
        
        return ""
        
        
    }
    func getProductsPrices(f_currency:String? = nil,f_group:String? = nil) -> PriceGroup?{
        var c_currency = ""
        var c_group = ""
        if let f_c = f_currency, let f_g = f_group {
            c_currency = f_c
            c_group = f_g
        }
        else{
            c_currency = UserDefaults.standard.getChosenCurrency()
            c_group = UserDefaults.standard.getChosenGroup()
        }

        if c_currency != "" && c_group != ""{

                    if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == c_currency && pr.group == c_group
                    }) {
                        return price
                    }
                    else if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == c_currency && pr.group == "X"
                    }) {
                        return price
                    }
  
        }
        
        return nil
        
    }
}
struct Specification : Codable {
    var code: String?
    var title: String?
    var titles : Lang?
    var fields : [Field]
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        try container.encode(fields, forKey: .fields)
        
    }
    
}
struct Field : Codable {
    var code : String?
    var title : String?
    var titles : Lang?
    var prefix : String?
    var affix : String?
    var value : String?
    var order : Int?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        try container.encode(prefix, forKey: .prefix)
        try container.encode(affix, forKey: .affix)
        try container.encode(value, forKey: .value)
        try container.encode(order, forKey: .order)
    }
}
struct LangArray : Codable {
    var en:[String]?
    var zhTW:[String]?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(en, forKey: .en)
        try container.encode(zhTW, forKey: .zhTW)
    }
    
    func getArray() -> [String]? {
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            return en
        }
        return zhTW
    }
}

struct PriceGroup : Codable {
    var group: String
    var currency: String
    var price : Int?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(price, forKey: .price)
        try container.encode(group, forKey: .group)
        try container.encode(currency, forKey: .currency)
    }
}

struct Map : Codable {
    var id : String
    //var image : String?
    var keywords : Lang?
    
    var image_url : String?
    var hotspots : [Hotspot]?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //try container.encode(image, forKey: .image)
        try container.encode(image_url,forKey: .image_url)
        try container.encode(hotspots, forKey: .hotspots)
        try container.encode(id, forKey: .id)
    }
}

struct Hotspot: Codable {
    var coordinate : HotspotLocation?
    var remote : String?
    var remotes : Lang?
    var files: Files?
    var image : String?
    var keywords : Lang?
    var description : String?
    var type : String?
    var descriptions : LangArray?
    var articles : [HotspotArticle]?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinate, forKey: .coordinate)
        try container.encode(remote, forKey: .remote)
        try container.encode(files, forKey: .files)
        try container.encode(remotes, forKey: .remotes)
        
    }

}
struct HotspotArticle : Codable{
    var titles : Lang?
    var descriptions: LangArray?
    
}
struct HotspotLocation: Codable {
    var id : String
    var width : String
    var height : String
    var top : String
    var left : String
}
struct Files : Codable {
    var en:[File]?
    var zhTW:[File]?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(en, forKey: .en)
//        try container.encode(zhTW, forKey: .zhTW)
//    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do{
            zhTW = try container.decode([File].self, forKey: .zhTW)
        }
        catch{
            zhTW = nil
        }
        
        do{
            en = try container.decode([File].self, forKey: .en)
        }
        catch{
            en = nil
        }
    }
    func getLang() -> [File]?{
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            if en == nil{
                return zhTW
            }
            return en
        }
        return zhTW
    }
}

struct File : Codable {
    
    var path: FileType?
    var path_url:FileType?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(path, forKey: .path)
        try container.encode(path_url, forKey: .path_url)
    }
}
enum FileType: Codable {
    case string(String?)
    case arrayString([String?]?)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String?.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode([String?]?.self) {
            self = .arrayString(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .arrayString(let x):
            try container.encode(x)
        }
    }
}
struct NewSpecField {
    var title:String
    var decodeId:String
    var unit : String? = ""
    var prefix : String? = ""
    var value : [String?] = [nil,nil,nil]
    var order : Int = 0
    init(title:String,decodeId:String,unit:String? = "",prefix:String? = "",order:Int) {
        self.title = title
        self.decodeId = decodeId
        self.unit = unit
        self.prefix = prefix
        self.order = order
    }
}
