//
//  SpecDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SpecDetailController: UIViewController {
    let spec = SpecTableView2()
    let machineLabel = UILabel()
    let selectLabel = ProductTabField()
    
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    override func viewDidLoad() {
        super.viewDidLoad()
        spec.bottomTopAnchor.trailing?.constant = 0
        //spec.labelanchor?.width?.constant = 100.calcvaluex()
        view.addSubview(spec)
        spec.fillSuperview()
        
        spec.label.text = "\("標配價格".localized):"
        
        view.addSubview(machineLabel)
        machineLabel.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        machineLabel.text = "機型".localized
        machineLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        machineLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        view.addSubview(selectLabel)
        selectLabel.anchor(top: view.topAnchor, leading: machineLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 22.calcvaluey(), left: 6.calcvaluex(), bottom: 0, right: 0),size: .init(width: 222.calcvaluex(), height: 38.calcvaluey()))
        selectLabel.addshadowColor(color: .white)
        selectLabel.layer.cornerRadius = 38.calcvaluey()/2
        selectLabel.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        selectLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        selectLabel.text = "NP16 2軸"
        selectLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        let arrowimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
        arrowimage.contentMode = .scaleAspectFit
        
        selectLabel.addSubview(arrowimage)
        arrowimage.anchor(top: nil, leading: nil, bottom: nil, trailing: selectLabel.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        arrowimage.centerYInSuperview()
        
                selectContent.backgroundColor = .clear
        //        selectContent.delegate = self
        //        selectContent.dataSource = self
        view.addSubview(selectContent)
                selectContent.addShadowColor(opacity: 0.1)
         
                selectContent.anchor(top: selectLabel.bottomAnchor, leading: selectLabel.leadingAnchor, bottom: nil, trailing: selectLabel.trailingAnchor)
        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
                selectContentConstraint.isActive = true
                
                selectLabel.delegate = self
                
                selectContent.addSubview(selectContentTableView)
                selectContentTableView.layer.cornerRadius = 5.calcvaluex()
        selectContentTableView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
                selectContentTableView.fillSuperview()
                selectContentTableView.delegate = self
                selectContentTableView.dataSource = self
                
                selectContentTableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        view.addGestureRecognizer(tap)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        popContent()
        spec.turnToOption = false
    }
    var productArray = [Product]()
    var selectedProduct: Product?
    var selectHeight : CGFloat = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        spec.turnToOption = true
        if let tab = tabBarController as? ProductInnerTabBarController {
            if tab.selectedProduct != nil {
                selectedProduct = tab.selectedProduct
            }
            else{
            selectedProduct = tab.product?[tab.productIndex ?? 0].array[tab.productSubIndex ?? 0]?.product
                let ntTitle = "\(tab.product?[tab.productIndex ?? 0].title ?? "") \(selectedProduct?.titles?.getLang() ?? "")"
                selectedProduct?.title = ntTitle
            }
           productArray = []
            for i in tab.product ?? [] {
                for j in i.array {
                    if var m = j{
                        m.product.title = "\(i.title) \(m.product.titles?.getLang() ?? "")"
                        productArray.append(m.product)
                    }
                }

            }
            
            
            selectHeight = CGFloat(productArray.count) * 84.calcvaluey()
            selectLabel.text = selectedProduct?.title
            spec.product = selectedProduct
            selectContentTableView.reloadData()
        }
    }
}
extension SpecDetailController:UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: selectContentTableView.self))!{
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectLabel.text = productArray[indexPath.row].title
        if let tab = tabBarController as? ProductInnerTabBarController {
            tab.selectedProduct = productArray[indexPath.row]
        }
        popContent()
        spec.product = productArray[indexPath.row]
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
        cell.title.text = productArray[indexPath.row].titles?.getLang()
        if indexPath.row == productArray.count - 1{
            cell.seperator.isHidden = true
        }
        else{
            cell.seperator.isHidden = false
        }
        return cell
    }
    @objc func popContent(){
        selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = selectHeight
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showContent()
        return false
    }
}

