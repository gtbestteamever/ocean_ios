//
//  SelectCustomerController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit


class SelectCustomerController: UIViewController {
    let vd = UIView()
    let xbutton = UIButton(type: .custom)
    let topLabel = UILabel()
    let formView = SalesFormView(text: "客戶公司")
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.addShadowColor(opacity: 0.1)
        
        view.addSubview(vd)
        vd.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 573.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 287.calcvaluey()))
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small"), for: .normal)
        xbutton.contentHorizontalAlignment = .fill
        xbutton.contentVerticalAlignment = .fill
        
        vd.addSubview(xbutton)
        xbutton.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.addTarget(self, action: #selector(popVd), for: .touchUpInside)
        
        topLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        topLabel.text = "選擇客戶"
        
        vd.addSubview(topLabel)
        topLabel.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        topLabel.centerXInSuperview()
        
        vd.addSubview(formView)
        
        formView.textfield.attributedPlaceholder = NSAttributedString(string: "選擇客戶公司", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)])
        
        formView.anchor(top: topLabel.bottomAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 43.calcvaluex()),size: .init(width: 0, height: 90.calcvaluey()))
        
        let imgv = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
        imgv.contentMode = .scaleAspectFit
        
        formView.textfield.addSubview(imgv)
        imgv.anchor(top: nil, leading: nil, bottom: nil, trailing: formView.textfield.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        imgv.centerYInSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIView.animate(withDuration: 0.1, animations: {
            self.vd.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (_) in
            UIView.animate(withDuration: 0.1) {
                self.vd.transform = .identity
            }
        }
    }
    
    @objc func popVd(){
        UIView.animate(withDuration: 0.2, animations: {
            self.vd.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
}
