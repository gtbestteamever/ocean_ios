//
//  SpecTableView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SpecTableView: UIView {
    let vd = UIView()
    let label = UILabel()
    let priceLabel = UILabel()
    let bottomTop = UIView()
    let comDetail = UITableView(frame: .zero, style: .grouped)
    var bottomTopAnchor:AnchoredConstraints!
    var compareDetailArray = SpecArray.shared.value
    var turnToOption = false
    var product : Product? {
        didSet{
            priceLabel.text = "\("售價".localized)："
            self.options = (product?.options?[0].children ?? []).sorted(by: { (p1, p2) -> Bool in
                return p1.order < p2.order
            })
            print(33,SpecArray.shared.value)
            for (index,m) in compareDetailArray.enumerated() {
                for (index2,j) in m.array.enumerated() {

                    for k in product?.specifications ?? [] {
                        for t in k.fields {
                            if t.code == j.decodeId {

                                compareDetailArray[index].array[index2].value[0] = t.value

                            }
                        }
                    }

                }
            }
            print(998,compareDetailArray)
            self.comDetail.reloadData()
            if let pr = product?.getProductsPrices() {
                
                
                priceLabel.text! += "\(pr.price?.converToPrice() ?? "$0".localized)"
                priceLabel.text! += " \(pr.currency)"
            }
            else{
                priceLabel.text! = "$0".localized
                
            }

        }
    }
    
    var options = [Option]() {
        didSet{
            self.comDetail.reloadData()
        }
    }
    var labelanchor : AnchoredConstraints?
    //let priceLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)

        bottomTop.backgroundColor = .white
        bottomTop.addshadowColor()
        bottomTop.layer.cornerRadius = 15.calcvaluex()
        

            addSubview(bottomTop)
        bottomTopAnchor = bottomTop.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()))
        if UserDefaults.standard.getShowPrice() {
            priceLabel.isHidden = false
        }
        else{
            priceLabel.isHidden = true
        }
        
        let vertStackview = Vertical_Stackview()
        
        vertStackview.addArrangedSubview(priceLabel)
        priceLabel.textColor = MajorColor().mainColor
        priceLabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        priceLabel.textAlignment = .right
        vertStackview.addArrangedSubview(comDetail)
              // comDetail.layer.cornerRadius = 15.calcvaluey()
               //comDetail.layer.masksToBounds = true
        comDetail.backgroundColor = .clear
             //  bottomTop.addSubview(comDetail)
               //comDetail.separatorStyle = .none
               //comDetail.fillSuperview()
        comDetail.contentInset = .init(top: 0, left: 0, bottom: 70.calcvaluey(), right: 0)
        comDetail.delegate = self
        comDetail.dataSource = self
        
        bottomTop.addSubview(vertStackview)
        vertStackview.fillSuperview(padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SpecTableView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if turnToOption {
            return 0
        }
        return compareDetailArray[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if turnToOption {
            return options.count
        }
        return compareDetailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SpecCell(style: .default, reuseIdentifier: "cell")
        if turnToOption {
            cell.headerLabel.text = options[indexPath.section].children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })[indexPath.row].titles?.getLang()
        }
        else{
        let value = compareDetailArray[indexPath.section].array[indexPath.row]
        
        cell.headerLabel.text = value.title
        cell.unitLabel.text = value.unit
            if value.value[0] == "NA" || value.value[0] == "" || value.value[0] == nil{
                cell.firstLabel.text = ""
            }
            else{
            cell.firstLabel.text = "\(value.prefix ?? "")\(value.value[0] ?? "")"
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CompareHeader()
        if turnToOption {
            let vf = CompareHeader2()
            if section % 2 == 0{
                vf.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
            }
            else{
                vf.backgroundColor = .white
            }
            vf.headerLabel.text = options[section].title
            if options[section].children?.count ?? 0 > 0 {
            vf.valueLabel.text =              options[section].children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })[0].title
            }
            return vf

            
           
        }
        else{
        //vd.headerLabel.text = compareDetailArray[section].title
        }
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.calcvaluey()
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50.calcvaluey()
//    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}



class SpecTableView2: UIView {
    let vd = UIView()
    let label = UILabel()
    let priceLabel = UILabel()
    let bottomTop = UIView()
    let comDetail = UITableView(frame: .zero, style: .grouped)
    var bottomTopAnchor:AnchoredConstraints!
    var compareDetailArray = SpecArray.shared.value
    var turnToOption = false {
        didSet{
            
            if turnToOption && !UserDefaults.standard.getShowPrice() {
                bottomTopAnchor.top?.constant = 84.calcvaluey()
                self.layoutIfNeeded()
            }
            
        }
    }
    var product : Product? {
        didSet{
            priceLabel.text = "\("選配價格".localized)："
            self.options = (product?.options?[0].children ?? []).sorted(by: { (p1, p2) -> Bool in
                return p1.order < p2.order
            })
            print(33,SpecArray.shared.value)
            for (index,m) in compareDetailArray.enumerated() {
                for (index2,j) in m.array.enumerated() {

                    for k in product?.specifications ?? [] {
                        for t in k.fields {
                            if t.code == j.decodeId {

                                compareDetailArray[index].array[index2].value[0] = t.value

                            }
                        }
                    }

                }
            }
            print(998,compareDetailArray)
            self.comDetail.reloadData()
            if let pr = product?.getProductsPrices() {
  
                priceLabel.text = "\(pr.price?.converToPrice() ?? "$0".localized)"
                priceLabel.text! += " \(pr.currency)"
            }
            else{
                priceLabel.text = "$0".localized
            }
            
        }
    }
    
    var options = [Option]() {
        didSet{
            self.comDetail.reloadData()
        }
    }
    let vertStackview = Vertical_Stackview(spacing:24.calcvaluey())
    var labelanchor : AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        bottomTop.backgroundColor = .white
        bottomTop.addshadowColor()
        bottomTop.layer.cornerRadius = 15.calcvaluex()
        

        addSubview(bottomTop)
        bottomTopAnchor = bottomTop.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()))
        if UserDefaults.standard.getShowPrice() {
            priceLabel.isHidden = false
        }
        else{
            priceLabel.isHidden = true
        }
        
        
        
        vertStackview.addArrangedSubview(priceLabel)
        priceLabel.textColor = MajorColor().mainColor
        priceLabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        priceLabel.textAlignment = .right
        vertStackview.addArrangedSubview(comDetail)
              // comDetail.layer.cornerRadius = 15.calcvaluey()
               //comDetail.layer.masksToBounds = true
        comDetail.backgroundColor = .clear
             //  bottomTop.addSubview(comDetail)
               //comDetail.separatorStyle = .none
               //comDetail.fillSuperview()
        comDetail.contentInset = .init(top: 0, left: 0, bottom: 70.calcvaluey(), right: 0)
        comDetail.delegate = self
        comDetail.dataSource = self
        
        bottomTop.addSubview(vertStackview)
        vertStackview.fillSuperview(padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        
//        for i in compareDetailArray {
//            for k in i.array {
//                print(77,k)
//            }
//        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SpecTableView2:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if turnToOption {
            return 0
        }
        return compareDetailArray[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if turnToOption {
            return options.count
        }
        return compareDetailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SpecCell(style: .default, reuseIdentifier: "cell")
        if turnToOption {
            cell.headerLabel.text = options[indexPath.section].children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })[indexPath.row].titles?.getLang()
        }
        else{
        let value = compareDetailArray[indexPath.section].array[indexPath.row]
        
        cell.headerLabel.text = value.title
        cell.unitLabel.text = value.unit
            if value.value[0] == "NA" || value.value[0] == "" || value.value[0] == nil{
                cell.firstLabel.text = ""
            }
            else{
            cell.firstLabel.text = "\(value.prefix ?? "")\(value.value[0] ?? "")"
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CompareHeader()
        if turnToOption {
            let vf = CompareHeader2()
            if section % 2 == 0{
                vf.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
            }
            else{
                vf.backgroundColor = .white
            }
            vf.headerLabel.text = options[section].titles?.getLang()
            if options[section].children?.count ?? 0 > 0 {
            vf.valueLabel.text =              options[section].children?.sorted(by: { (op1, op2) -> Bool in
                return op1.order < op2.order
            })[0].titles?.getLang()
            }
            return vf

            
           
        }
        else{
        vd.headerLabel.text = compareDetailArray[section].title
        }
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
