//
//  InnerProductIntroController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension String {

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}
class ProductFilterView : UIView {
    let nLabel = UILabel()
    let vArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_down").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        layer.cornerRadius = 21.calcvaluey()
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.calcvaluex()
        addSubview(nLabel)
        nLabel.text = "篩選".localized
        nLabel.textColor = .white
        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        nLabel.centerYInSuperview()
        addSubview(vArrow)
        vArrow.tintColor = .white
        vArrow.contentMode = .scaleAspectFit
        vArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        vArrow.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductSFilterView : UIView {
    let nLabel = UILabel()
    let vArrow = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate))
    var width: CGFloat?
    
    init(text:String) {
        super.init(frame: .zero)
        backgroundColor = .clear
        layer.cornerRadius = 21.calcvaluey()
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.calcvaluex()
        addSubview(nLabel)
        nLabel.text = text
        nLabel.textColor = .white
        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 46.calcvaluex()))
        nLabel.centerYInSuperview()
        addSubview(vArrow)
        vArrow.tintColor = .white
        vArrow.contentMode = .scaleAspectFit
        vArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        vArrow.centerYInSuperview()
        vArrow.isUserInteractionEnabled = true
        
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareItemView : UIView{
    let imagev = UIImageView()
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let cancelButton = UIButton(type: .custom)
    init(product:selectedProduct) {
        super.init(frame: .zero)
        backgroundColor = .clear
        addSubview(imagev)
        imagev.sd_setImage(with: URL(string: "\(product.assets?.getMachinePath() ?? "")"), completed: nil)
        imagev.contentMode = .scaleAspectFit
        
        imagev.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 63.calcvaluex(), height: 50.calcvaluey()))
        imagev.centerYInSuperview()
        
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        addSubview(cancelButton)
        cancelButton.contentHorizontalAlignment = .fill
        cancelButton.contentVerticalAlignment = .fill
        cancelButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        topLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        bottomLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        bottomLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: imagev.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 33.calcvaluey(), left: 6.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        topLabel.text = product.seriesTitle
        
        addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()))
        bottomLabel.text = "\(product.productTitle)-\(product.title)"
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension UIStackView {
    func safelyRemoveArrangedSubviews() {

        // Remove all the arranged subviews and save them to an array
        let removedSubviews = arrangedSubviews.reduce([]) { (sum, next) -> [UIView] in
            self.removeArrangedSubview(next)
            return sum + [next]
        }

        // Deactive all constraints at once
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))

        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
    
    
}
protocol ComVDelegate {
    func removeItemAt(pr:selectedProduct)
}
class CompareView : UIView {
    let bottomView = UIView()
    let downArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down_circle"))
    var showing = true
    let startButton = UIButton(type: .custom)
    let nLabel = UILabel()
    let seperator = UIView()
    let vLabel = UILabel()
    let stackview = UIStackView()
   // weak var prev : InnerProductIntroController?
    var selectedProducts = [selectedProduct]() {
        didSet{
            setStackview()
        }
    }
    func setStackview(){
        if selectedProducts.count > 0 {
            self.vLabel.isHidden = true
            self.stackview.isHidden = false
        }
        else{
            self.vLabel.isHidden = false
            self.stackview.isHidden = true
        }
        stackview.safelyRemoveArrangedSubviews()
        for (index,i) in selectedProducts.enumerated() {
            addItem(product: i,index:index)
        }
        for _ in 0 ..< (3 - selectedProducts.count) {
            stackview.addArrangedSubview(UIView())
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        bottomView.backgroundColor = .white
        bottomView.addshadowColor(color: .white)
        addSubview(bottomView)
        bottomView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 102.calcvaluey()))
        
        addSubview(downArrow)
        downArrow.contentMode = .scaleAspectFit
        downArrow.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 48.calcvaluex(), bottom: 0, right: 0),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        downArrow.centerYAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        downArrow.isUserInteractionEnabled = true
        
        bottomView.addSubview(startButton)
        startButton.anchor(top: bottomView.topAnchor, leading: nil, bottom: bottomView.bottomAnchor, trailing: bottomView.trailingAnchor,size: .init(width: 170.calcvaluex(), height: 0))
        startButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        startButton.setTitle("開始比較".localized, for: .normal)
        startButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        startButton.setTitleColor(.white, for: .normal)
        
        nLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        nLabel.text = "產品比較".localized
        nLabel.numberOfLines = 2
        bottomView.addSubview(nLabel)
        nLabel.anchor(top: nil, leading: bottomView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 80.calcvaluex(), height: 0))
        nLabel.centerYInSuperview()
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        
        bottomView.addSubview(seperator)
        seperator.anchor(top: nil, leading: nLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 66.calcvaluey()))
        seperator.centerYInSuperview()
        
        bottomView.addSubview(vLabel)
 
        vLabel.text = "最多比較三台機型".localized
        vLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        vLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        vLabel.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        vLabel.centerYInSuperview()
        
        bottomView.addSubview(stackview)
        stackview.distribution = .fillEqually
        stackview.spacing = 24.calcvaluex()
        stackview.axis = .horizontal
        stackview.isHidden = true
        stackview.anchor(top: bottomView.topAnchor, leading: seperator.trailingAnchor, bottom: bottomView.bottomAnchor, trailing: startButton.leadingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
//        addItem()
//        addItem()
//
//        stackview.addArrangedSubview(UIView())
    }
    var delegate:ComVDelegate?
    @objc func removeItem(sender:UIButton) {
        //print(sender.tag)
        var ab = selectedProducts
        let didRemove = ab.remove(at: sender.tag)
        delegate?.removeItemAt(pr: didRemove)
        selectedProducts = ab
    }
    func addItem(product:selectedProduct,index:Int){

        let com = CompareItemView(product: product)
        com.cancelButton.tag = index
        com.cancelButton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
//        let combine = "\(product.productTitle)-\(product.title)".width(withConstrainedHeight: self.frame.height, font: UIFont(name: "Roboto-Regular", size: 14.calcvaluex())!) + 81.calcvaluex()
//        print(991,combine)
//        if combine <= 171.calcvaluex(){
//        com.constrainWidth(constant: 171.calcvaluex())
//        }
//        else{
//            com.constrainWidth(constant: combine)
//        }
            stackview.addArrangedSubview(com)
        
        
        
        

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum InnerMode {
    case Normal
    case Compare
}
protocol InnerProductDelegate {
    func addNewItem(seriesIndex:Int,productIndex:Int,subIndex:Int,pr:selectedProduct?)
    func goToProduct(seriesIndex:Int,productIndex:Int,subIndex:Int)
}
class InnerProductIntroController: SampleController {
    let imgv = UIImageView(image: #imageLiteral(resourceName: "bg_product"))
    let nameLabel = UILabel()
    let cusFilter = ProductFilterView()
    //let cusFilter2 = ProductSFilterView()
    let tableview = UITableView(frame: .zero, style: .plain)
    var stackviewanchor : AnchoredConstraints!
    let stackview = UIStackView()
    let compareButton = UIButton(type: .custom)
    var comparingView = CompareView()
    var mode = InnerMode.Normal
    var comparingViewAnchor:AnchoredConstraints!
    var selectedRow : Int = 0
    var tableDetailArray = [InnerProductIntroCell]()
    var fitlerArr = [filterSection]()
    var seriesArray = [Series2]()
    var calculatedHeight = [CGFloat]()
    
    var series = [Series]() {
        didSet{
            series = series.sorted(by: { (or1, or2) -> Bool in
                return or1.order ?? 0 < or2.order ?? 0
            })
            reCreateWithFiltering()

        }
    }
    func reCreateWithFiltering(){
        self.tableview.isHidden = true
        seriesArray = []
        tableDetailArray = []
        calculatedHeight = []
        for i in series {
            var products = [Product2]()
            for j in (i.children ?? []).filter({ (sr) -> Bool in
                return sr.status == 1
            }).sorted(by: { (s1, s2) -> Bool in
                return (s1.order ?? 0) < (s2.order ?? 0)
            }) {
                var prArray = [Product]()
                for k in (j.products ?? []).sorted(by: { (p1, p2) -> Bool in
                    return (p1.order ?? 0) < (p2.order ?? 0)
                }){
                    if filteredData.count > 0{
                    let ct = convertFilter(pr: k)
                    print(765,ct)
                    if ct == 1{
                        prArray.append(k)
                    }
                    }
                    else{
                        prArray.append(k)
                    }
                    
                }
                //j.title
                if prArray.count != 0{
                    print(3312,j.titles?.getLang())
                    products.append(Product2(title: j.titles?.getLang() ?? "", array: prArray,status: j.status,is_disable: j.is_disable))
                }
            }
            if products.count != 0{
                seriesArray.append(Series2(title: i.titles ?? Lang(), products: products, assets: i.assets,status: i.status,is_disable: i.is_disable))
            }
        }
        for (ind,i) in seriesArray.enumerated() {
        for (ind2,m) in i.products.enumerated() {
            for (ind3,k) in m.array.enumerated() {
                if selectedProductArray.contains(where: { (s1) -> Bool in
                    return s1.product.id == k?.product.id
                }) {
                    seriesArray[ind].products[ind2].array[ind3]?.isSelected = true
                }
            }
            
        }
        }
        
        for i in seriesArray {
            self.tableDetailArray.append(InnerProductIntroCell(mode: self.mode, delegate: self, series: i))
            
            var total = CGFloat(150)
            for j in i.products {
                //print(9987,j.num)
                total += CGFloat(j.num) * 46.calcvaluey()
            }
            self.calculatedHeight.append(total)
            

        }
       
        
        self.tableview.reloadData()
        self.tableview.isHidden = false
    }
    func convertFilter(pr:Product) -> Int{
        var count = 1
        print(99,pr.title)
        for i in filteredData {
            if let m = i{
                
                if let last = pr.specifications?.first {
                    if m.code == "Last" {
                        if m.data.value == "無銑削" {
                            if let fr = last.fields.first(where: { (ft) -> Bool in
                                return ft.code == "lc-milling-speed"
                            }) {
                                
                                if fr.value != "" && fr.value != nil && fr.value != "NA"{
                                    
                                    count = 0
                                }
                            }
                            else {
                                count = 0
                            }
                        }
                        else if m.data.value == "有銑削" {
                            if let fr = last.fields.first(where: { (ft) -> Bool in
                                return ft.code == "lc-milling-speed"
                            }) {
                                if fr.value == nil || fr.value == "" || fr.value == "NA"{
                                    count = 0
                                }
                            }
                            else{
                                count = 0
                            }
                        }
                        else {
                            if let fr = last.fields.first(where: { (ft) -> Bool in
                                return ft.code == "lc-sub-spindle-speed"
                            }) {
                                
                                if fr.value == nil || fr.value == "" || fr.value == "NA"{
                                    
                                    count = 0
                                }
                            }
                            else{
                                count = 0
                            }
                        }
                    }
                    else{
                    if let fr = last.fields.first(where: { (f2) -> Bool in
                        return (f2.code?.contains(m.code) ?? false)
                    }) {
                        print(1001,fr.code)
                        switch m.data.mode {
                        case .lessThan:
                            let kt = fr.value?.split(separator: "x")
                            if (kt?.count ?? 0) > 0 {
                                let d1 = Double(kt?[0].trimmingCharacters(in: .whitespaces) ?? "0") ?? 0
                            let d2 = Double(m.data.value) ?? 0
                            if d1 > d2 {
                                count = 0
                            }
                            }
                            else{
                                count = 0
                            }
                        case .inBetween:
                            let kt = fr.value?.split(separator: "x")
                            if (kt?.count ?? 0) > 0 {
                            let d1 = Double(kt?[0].trimmingCharacters(in: .whitespaces) ?? "0") ?? 0
                            let split = m.data.value.split(separator: "~")
                                
                            let d2 = Double(String(split[0])) ?? 0
                            
                            let d3 = Double(String(split[1])) ?? 0
                            
                            if d1 < d2 {
                                count = 0
                            }
                            
                            if d1 > d3 {
                                count = 0
                            }
                                
                            }
                            else{
                                count = 0
                            }
                        case .equal:
                            let kt = fr.value?.split(separator: "x")
                            if (kt?.count ?? 0) > 0 {
                                if String(kt?[0].trimmingCharacters(in: .whitespaces) ?? "") == m.data.value {
                                count = 0
                            }
                            }
                            else{
                                count = 0
                            }
                        case .greaterThan:
                            let kt = fr.value?.split(separator: "x")
                            if (kt?.count ?? 0) > 0 {
                            let d1 = Double(kt?[0].trimmingCharacters(in: .whitespaces) ?? "0") ?? 0
                            let d2 = Double(m.data.value) ?? 0
                            if d1 < d2 {
                                count = 0
                            }
                            }
                            else{
                                count = 0
                            }
                        }
                        
                    }
                    else{
                        count = 0
                    }

                }
                    
                }
                else{
                    count = 0
                }
                
            }
        }
        print(399,count)
        return count
    }
    var imgvAnchor : AnchoredConstraints?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        //titleview.label.text = "工具機"
        
        imgv.contentMode = .scaleAspectFill
        imgv.clipsToBounds = true
        
        view.addSubview(imgv)
        imgvAnchor = imgv.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 172.calcvaluey()))
        
        nameLabel.font = UIFont(name: "Roboto-Medium", size: 36.calcvaluex())
        nameLabel.textColor = .white
        //nameLabel.text = "工具機"
        imgv.addSubview(nameLabel)
        nameLabel.anchor(top: imgv.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        nameLabel.centerXInSuperview()
        
//        imgv.addSubview(cusFilter)
//        cusFilter.anchor(top: nameLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 108.calcvaluex(), height: 42.calcvaluey()))
//        cusFilter.centerXInSuperview()
        
        cusFilter.constrainWidth(constant: 108.calcvaluex())
        cusFilter.constrainHeight(constant: 42.calcvaluey())
        

        

        stackview.addArrangedSubview(cusFilter)
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.spacing = 12.calcvaluex()
        stackview.alignment = .fill
        view.addSubview(stackview)
        //imgv.isUserInteractionEnabled = true
        stackviewanchor = stackview.anchor(top: nil, leading: nil, bottom: imgv.bottomAnchor, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 36.calcvaluey(), right: 0),size: .init(width: 108.calcvaluex(), height: 42.calcvaluey()))
        stackview.centerXInSuperview()
        
        view.addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.bounces = false
        tableview.contentInset = .init(top: 0, left: 0, bottom: 30.calcvaluey(), right: 0)
        tableview.anchor(top: imgv.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        //tableview.allowsSelection = false
        cusFilter.isUserInteractionEnabled = true
        cusFilter.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goFilter)))
        
        compareButton.setTitle("加入比較".localized, for: .normal)
        compareButton.setTitleColor(.white, for: .normal)
        compareButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        compareButton.backgroundColor = .black
        compareButton.layer.cornerRadius = 21.calcvaluey()
        view.addSubview(compareButton)
        compareButton.addTarget(self, action: #selector(addToCompare), for: .touchUpInside)
        compareButton.anchor(top: imgv.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 48.calcvaluey(), left: 0, bottom: 0, right: -18.calcvaluex()),size: .init(width: 128.calcvaluex(), height: 42.calcvaluey()))
        compareButton.addshadowColor(color: .white)
        //tableview.isHidden = true

        
//        if let first = self.seriesArray.first, let second = first.products.first {
//            if second.array.count > 1{
//                
//                if let last = second.array[1]?.product.specifications?.first {
//                    SpecArray.constructValue(field: last.fields)
//                    
//                }
//            }
//        }


    }
    func resetArray(){
        self.tableDetailArray = []
        for i in seriesArray {
            self.tableDetailArray.append(InnerProductIntroCell(mode: self.mode, delegate: self, series: i))
        }
        
        self.tableview.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        


    }
    @objc func dismissCompare(){
        //mode = .Normal
        if comparingView.showing{
        comparingViewAnchor.top?.constant = -24.calcvaluey()
        UIView.animate(withDuration: 0.4, animations:{
            self.view.layoutIfNeeded()
        }) { (_) in
            self.comparingView.downArrow.image = #imageLiteral(resourceName: "ic_arrow_drop_up_circle")
        }
            comparingView.showing = false
        }
        else{
            comparingViewAnchor.top?.constant = -120.calcvaluey()

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.comparingView.downArrow.image = #imageLiteral(resourceName: "ic_arrow_drop_down_circle")
            }
            comparingView.showing = true
        }
        //self.tableview.reloadData()
    }
    @objc func goComparing(){
        if self.selectedProductArray.count > 0{
        let vd = ComparingItemController()
            //vd.selectedProductArray = self.selectedProductArray
//            for i in self.selectedProductArray {
//                print(223,i.product.specifications)
//            }
            //setSpec(products: self.selectedProductArray[0].product)
           // SpecArray.setCompareFields(products: self.selectedProductArray)
            //vd.con = self
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
        }
        else{
            let alertcon = UIAlertController(title: "至少需要一個機台做比較", message: nil, preferredStyle: .alert)
            alertcon.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertcon, animated: true, completion: nil)
        }
    }
    @objc func addToCompare(){
        
        if mode == .Normal {
            comparingView = CompareView()
            comparingView.delegate = self
            comparingView.startButton.addTarget(self, action: #selector(goComparing), for: .touchUpInside)
            view.addSubview(comparingView)
            
            comparingViewAnchor = comparingView.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 120.calcvaluey()))
            comparingView.downArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissCompare)))
        mode = .Compare
            self.view.layoutIfNeeded()
        comparingViewAnchor.top?.constant = -120.calcvaluey()
            tableview.contentInset = .init(top: 0, left: 0, bottom: 100.calcvaluey(), right: 0)
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
            compareButton.setTitle("取消比較".localized, for: .normal)
            comparingView.showing = true
            


            
        }

        else{
            mode = .Normal
            
            comparingViewAnchor.top?.constant = 0

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.comparingView.removeFromSuperview()
            }
            compareButton.setTitle("加入比較".localized, for: .normal)
            tableview.contentInset = .init(top: 0, left: 0, bottom: 30.calcvaluey(), right: 0)
        }
        comparingView.selectedProducts = selectedProductArray
        for i in tableDetailArray {
            i.mode = self.mode
        }
        self.tableview.reloadData()
    }
    @objc func removeFilter(sender:UITapGestureRecognizer) {
        if let sd = sender.view {
            if let tm = (stackview.arrangedSubviews[sd.tag] as? ProductSFilterView) {
                let kt = tm.nLabel.text?.split(separator: ":")
                if let index = filteredData.firstIndex(where: { (ft) -> Bool in
                    return ft?.title == String(kt?[0] ?? "")
                }) {
                    filteredData[index] = nil
                    addFilter(data: filteredData)
                }
                
            }

        }
        
    }
    var filteredData = [filterSelectionData?]()
    func addFilter(data:[filterSelectionData?]) {
        filteredData = data
        
        for (ind,i) in stackview.arrangedSubviews.enumerated() {
            if ind != 0{
                stackview.removeArrangedSubview(i)
                i.removeFromSuperview()
            }
        }
        var twidth = 108.calcvaluex()
        var tagg = 1
        for i in data {
            var txt = ""
            if let v = i{
                switch v.data.mode {
                case .lessThan:
                    txt = "\(v.title): < \(v.data.value) \(v.unit)"
                case .equal,.inBetween:
                    txt = "\(v.title): \(v.data.value) \(v.unit)"
                case .greaterThan:
                    txt = "\(v.title): \(v.data.value) \(v.unit) >"
                }
            }
            else{
                continue
            }
                        let fv = ProductSFilterView(text: txt)
                            let width = txt.widthOfString(usingFont: fv.nLabel.font) + 72.calcvaluex()
                            fv.constrainHeight(constant: 42.calcvaluey())
                            fv.constrainWidth(constant: width)
                            stackview.addArrangedSubview(fv)
                            twidth += width + 12.calcvaluex()
                            fv.width = width + 12.calcvaluex()
            fv.vArrow.tag = tagg
            fv.vArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeFilter)))
                            tagg += 1
        }
        var didHave = false
        for i in data {
            if let _ = i{
                didHave = true
            }
        }
        if didHave {
        cusFilter.backgroundColor = MajorColor().mainColor
        cusFilter.layer.borderColor = UIColor.clear.cgColor
        }
        else{
            cusFilter.backgroundColor = .clear
            cusFilter.layer.borderColor = UIColor.white.cgColor
        }
        stackviewanchor.width?.constant = twidth
        
        self.view.layoutIfNeeded()
        
       reCreateWithFiltering()
        
    }
    @objc func goFilter(){
        print("abc")
        let md = FilteringViewController()
        md.modalPresentationStyle = .overCurrentContext
        md.con = self
        md.selectedFilter = self.filteredData
        md.filterArr = self.fitlerArr
        self.present(md, animated: false, completion: nil)
    }
    
    var selectedProductArray = [selectedProduct]() {
        didSet{
            comparingView.selectedProducts = self.selectedProductArray
        }
    }
    var removeProduct: selectedProduct? {
        didSet{
            if let rP = removeProduct {
                addNewItem(seriesIndex: rP.seriesIndex, productIndex: rP.productIndex, subIndex: rP.subIndex,pr: rP)
            }

        }
    }
    var lastcontentoffset : CGFloat = 0
    var lastY : CGFloat = 0
}
extension InnerProductIntroController : UITableViewDelegate,UITableViewDataSource,InnerProductDelegate,ComVDelegate{
    func removeItemAt(pr: selectedProduct) {
        addNewItem(seriesIndex: pr.seriesIndex, productIndex: pr.productIndex, subIndex: pr.subIndex,pr:pr)
    }
    
    func addNewItem(seriesIndex: Int, productIndex: Int, subIndex: Int,pr:selectedProduct?) {
        if seriesIndex >= seriesArray.count {
            if let index = selectedProductArray.firstIndex(where: { (s1) -> Bool in
                return s1.product.id == pr?.product.id
            }) {
                selectedProductArray.remove(at: index)
            }
            reCreateWithFiltering()
            comparingView.selectedProducts = selectedProductArray
             return
        }
        if productIndex >= seriesArray[seriesIndex].products.count {
            if let index = selectedProductArray.firstIndex(where: { (s1) -> Bool in
                return s1.product.id == pr?.product.id
            }) {
                selectedProductArray.remove(at: index)
            }
            reCreateWithFiltering()
            comparingView.selectedProducts = selectedProductArray
            return
        }
        if subIndex >= seriesArray[seriesIndex].products[productIndex].array.count {
            if let index = selectedProductArray.firstIndex(where: { (s1) -> Bool in
                return s1.product.id == pr?.product.id
            }) {
                selectedProductArray.remove(at: index)
            }
            reCreateWithFiltering()
            comparingView.selectedProducts = selectedProductArray
            return
        }
        let selected = seriesArray[seriesIndex].products[productIndex].array[subIndex]
        if selected?.product.id == pr?.product.id || pr == nil {
        
        if !(selected?.isSelected ?? false) {
        if selectedProductArray.count < 3{
            let selectedp = selectedProduct(product: selected!.product, seriesTitle: seriesArray[seriesIndex].title.getLang() ?? "", productTitle: seriesArray[seriesIndex].products[productIndex].title, seriesIndex: seriesIndex, productIndex: productIndex, subIndex: subIndex, title: selected?.product.titles?.getLang() ?? "",assets: seriesArray[seriesIndex].assets)
            selectedProductArray.append(selectedp)
            seriesArray[seriesIndex].products[productIndex].array[subIndex]?.isSelected = true
        
            resetArray()
        }
        else{
            let con = UIAlertController(title: "提示".localized, message: "只能選擇三個產品".localized, preferredStyle: .alert)
                        con.addAction(UIAlertAction(title: "確認".localized, style: .cancel, handler: nil))
            
                        self.present(con, animated: true, completion: nil)
        }
        }

        else{

            seriesArray[seriesIndex].products[productIndex].array[subIndex]?.isSelected = false
        
            resetArray()
            if let index = selectedProductArray.firstIndex(where: { (sP) -> Bool in
                return sP.title == selected?.product.titles?.getLang()
            }) {
            selectedProductArray.remove(at: index)
           }
        }
        }
        else{

            if let index = selectedProductArray.firstIndex(where: { (s1) -> Bool in
                return s1.product.id == pr?.product.id
            }) {
                selectedProductArray.remove(at: index)
            }
            reCreateWithFiltering()
//            if selected?.product.id == pr?.product.id {
//                seriesArray[seriesIndex].products[productIndex].array[subIndex]?.isSelected = false
//                self.reCreateWithFiltering()
//            }
        }
        comparingView.selectedProducts = selectedProductArray
        
    }
    func goToProduct(seriesIndex: Int, productIndex: Int, subIndex: Int) {
        let vd2 = ProductInnerInfoController()
        vd2.modalPresentationStyle = .fullScreen
        
        vd2.titleview.label.text = seriesArray[seriesIndex].title.getLang()
        vd2.tabcon.selectedPreTitle = seriesArray[seriesIndex].products[productIndex].title
        
        vd2.selectedProductArray = self.selectedProductArray
        vd2.tabcon.productIndex = productIndex
        vd2.tabcon.productSubIndex = subIndex
        vd2.tabcon.product = seriesArray[seriesIndex].products
        
        vd2.seriesIndex = seriesIndex

        vd2.seriesArray = self.seriesArray
        if let pr = seriesArray[seriesIndex].products[productIndex].array[subIndex]?.product {
            setSpec(products: pr)
        }
        
        self.present(vd2, animated: true, completion: nil)
    }
//    func setSpec(products:[Product2]){
//           
//            for a in products{
//                if a.array.count > 1{
//                    for b in 1 ..< a.array.count {
//                        print(773,a.array[b]?.product.specifications)
//                        if (a.array[b]?.product.specifications?.count ?? 0) == 0{
//                            SpecArray.constructValue(field: [])
//                            continue
//                        }
//                        for s in a.array[b]?.product.specifications ?? [] {
//                            SpecArray.constructValue(field: s.fields)
//                            break
//                        }
//                    }
//                }
//                else{
//                    SpecArray.constructValue(field: [])
//                }
//            }
//        
//    }
    
    func setSpec(products:Product){
        if (products.specifications?.count ?? 0) == 0 {
          
            SpecArray.constructValue(field: [])
            return
        }
        for s in products.specifications ?? [] {
            print(3312,s.fields)
            print("-------")
            SpecArray.constructValue(field: s.fields)
            break
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableDetailArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(7777)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastcontentoffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        
        
        
        if scrollView.contentOffset.y > 10.calcvaluey(){
            print(self.lastcontentoffset,scrollView.contentOffset.y)
        
        if self.lastcontentoffset < scrollView.contentOffset.y {
            imgvAnchor?.height?.constant = 172.calcvaluey()/2
            nameLabel.isHidden = true
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
        }
        else{
            imgvAnchor?.height?.constant = 172.calcvaluey()
            nameLabel.isHidden = false
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableDetailArray[indexPath.row]
        if indexPath.row % 2 == 0{
            cell.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        }
        else{
            cell.backgroundColor = .white
        }
        cell.tag = indexPath.row
        return cell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print(calculatedHeight[indexPath.row])
        if indexPath.row >= calculatedHeight.count {
            return 0
        }
        if calculatedHeight[indexPath.row] <= 120 {
            return 180.calcvaluey()
        }
        return calculatedHeight[indexPath.row].calcvaluey()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
}
class innerCollectionView:UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize{
            invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        return self.contentSize
    }
}
class InnerProductIntroCell:UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    let imgv = UIImageView()
    let seperator = UIView()
    let headerLabel = UILabel()
    let subLabel = UILabel()
    let stackview = UIStackView()
    let collectionView : innerCollectionView = {
        let flowlayout = CustomViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        
        let collection = innerCollectionView(frame: .zero, collectionViewLayout: flowlayout)
        
        
        return collection
        
        
    }()
    var mode = InnerMode.Normal {
        didSet{
            self.collectionView.reloadData()
        }
    }
    var delegate:InnerProductDelegate?
    var series : Series2?
    init(mode:InnerMode,delegate:InnerProductDelegate,series:Series2) {
        super.init(style: .default, reuseIdentifier: "cell")
        self.delegate = delegate
        self.mode = mode
        selectionStyle = .none
        imgv.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
         imgv.contentMode = .scaleAspectFit
         contentView.addSubview(imgv)
         imgv.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 106.calcvaluex(), bottom: 0, right: 0),size: .init(width: 242.calcvaluex(), height: 153.calcvaluey()))
         imgv.centerYInSuperview()
         
         seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
         
         contentView.addSubview(seperator)
         seperator.anchor(top: contentView.topAnchor, leading: imgv.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 42.calcvaluex(), bottom: 46.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
         
        headerLabel.text = series.title.getLang()
         headerLabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
         headerLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
         
         contentView.addSubview(headerLabel)
         headerLabel.anchor(top: contentView.topAnchor, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 52.calcvaluey(), left: 74.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 33.calcvaluey()))
         
//        subLabel.text = series.title.en
//         subLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
//         subLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//
//         contentView.addSubview(subLabel)
//         subLabel.anchor(top: nil, leading: headerLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
//         subLabel.centerYAnchor.constraint(equalTo: headerLabel.centerYAnchor).isActive = true

        
        contentView.addSubview(collectionView)
        
        collectionView.anchor(top: headerLabel.bottomAnchor, leading: headerLabel.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 0, bottom: 20.calcvaluey(), right: 34.calcvaluex()))
        collectionView.isScrollEnabled = false
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(selectionCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(labelCollectionViewCell.self, forCellWithReuseIdentifier: "label")
        collectionView.register(goSelectionCollectionViewCell.self, forCellWithReuseIdentifier: "select")
        self.series = series
        
        if self.series?.is_disable == 1{
            let alphaView = UIView()
            alphaView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            contentView.addSubview(alphaView)
            alphaView.fillSuperview()
        }
        
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return series?.products.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return series?.products[section].array.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        if let cell = collectionView.cellForItem(at: indexPath) as? goSelectionCollectionViewCell {
//            cell.box.image = #imageLiteral(resourceName: "btn_check_box_pressed")
//        }
        if series?.products[indexPath.section].is_disable == 1{
            return
        }
        if mode == .Compare {
        delegate?.addNewItem(seriesIndex: self.tag, productIndex: indexPath.section, subIndex: indexPath.row,pr: nil)
        }
        else{
            delegate?.goToProduct(seriesIndex: self.tag, productIndex: indexPath.section, subIndex: indexPath.row)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! selectionCollectionViewCell
        if indexPath.item % 4 == 0{

                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "label", for: indexPath) as! labelCollectionViewCell
                if indexPath.item == 0{
                    cell.label.text = series?.products[indexPath.section].title
                }
                else{
                    cell.label.text = nil
                }
            if series?.products[indexPath.section].is_disable == 1{
                cell.label.textColor = .lightGray
            }
            else{
                cell.label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
            }
                return cell
            

        }
        else{
            if mode == .Compare {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "select", for: indexPath) as! goSelectionCollectionViewCell
                cell.selection.setTitle(series?.products[indexPath.section].array[indexPath.item]?.product.titles?.getLang(), for: .normal)
                cell.box.image = series?.products[indexPath.section].array[indexPath.item]?.isSelected == true ? #imageLiteral(resourceName: "btn_check_box_pressed") : #imageLiteral(resourceName: "btn_check_box_normal")
                if series?.products[indexPath.section].is_disable == 1{
                    cell.alphaview.backgroundColor = UIColor.white.withAlphaComponent(0.5)
                }
                else{
                    cell.alphaview.backgroundColor = UIColor.white.withAlphaComponent(0)
                }
                return cell
            }
            else{
                if series?.products[indexPath.section].is_disable == 1{
                    cell.alphaview.backgroundColor = UIColor.white.withAlphaComponent(0.5)
                }
                else{
                    cell.alphaview.backgroundColor = UIColor.white.withAlphaComponent(0)
                }
                
                cell.selection.setTitle(series?.products[indexPath.section].array[indexPath.item]?.product.titles?.getLang(), for: .normal)
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item % 4 == 0{
            let width = series?.products[indexPath.section].title.widthOfString(usingFont: UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!) ?? 0
            if width <= 110.calcvaluex() {
                return .init(width: 110.calcvaluex(), height: 32.calcvaluey())
            }
            
            return .init(width: width, height: 32.calcvaluey())
        }
        else{
            if mode == .Compare {
                return .init(width: 117.calcvaluex(), height: 32.calcvaluey())
            }
            //series?.products[indexPath.section].array[indexPath.item]?.product.title
            var width = series?.products[indexPath.section].array[indexPath.item]?.product.title?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Medium", size: 16.calcvaluex())!) ?? 0
            if width < 89.calcvaluex() {
                width = 89.calcvaluex()
            }
            else{
                width += 32.calcvaluex()
            }
            return .init(width: width, height: 32.calcvaluey())
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class selectionCollectionViewCell : UICollectionViewCell {
    let selection = UIButton(type: .custom)
    var alphaview = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        selection.addshadowColor()
        selection.backgroundColor = MajorColor().mainColor
        selection.layer.cornerRadius = 16.calcvaluey()
        selection.setTitleColor(.white, for: .normal)
        selection.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        
        addSubview(selection)
        selection.fillSuperview(padding: .init(top: 1.calcvaluex(), left: 1.calcvaluex(), bottom: 1.calcvaluex(), right: 1.calcvaluex()))
        selection.isEnabled = false
        
        addSubview(alphaview)
        alphaview.layer.cornerRadius = 16.calcvaluey()
       alphaview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class goSelectionCollectionViewCell : UICollectionViewCell {
    let selection = UIButton(type: .custom)
    let box = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal"))
    var alphaview = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(box)
        box.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        box.centerYInSuperview()
        selection.addshadowColor()
        selection.backgroundColor = MajorColor().mainColor
        selection.layer.cornerRadius = 16.calcvaluey()
        selection.setTitleColor(.white, for: .normal)
        selection.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        selection.isEnabled = false
        addSubview(selection)

        
        selection.anchor(top: topAnchor, leading: box.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 1.calcvaluex(), left: 4.calcvaluex(), bottom: 1.calcvaluex(), right: 1.calcvaluex()))
        addSubview(alphaview)
        alphaview.layer.cornerRadius = 16.calcvaluey()
       alphaview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class labelCollectionViewCell : UICollectionViewCell {
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        addSubview(label)
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil)
        label.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class SelectButton : UIView {
    let button = UIButton(type: .custom)
    let box = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal").withRenderingMode(.alwaysTemplate))
    var boxanchor : AnchoredConstraints!
    var buttonanchor:AnchoredConstraints!
    var delegate: InnerProductDelegate?
    init(mode:InnerMode,delegate:InnerProductDelegate){
        super.init(frame: .zero)
        self.delegate = delegate
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
         button.setTitleColor(.white, for: .normal)
         button.addshadowColor(color: .white)
         button.layer.cornerRadius = 16.calcvaluey()
         button.backgroundColor = MajorColor().mainColor
        button.addTarget(self, action: #selector(goProductInfo), for: .touchUpInside)
         addSubview(box)
         box.contentMode = .scaleAspectFit
        if mode == .Compare{
            box.isHidden = false
            box.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        }
        else{
         box.isHidden = true
         boxanchor = box.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 24.calcvaluex()))
        }
        box.isUserInteractionEnabled = true

         box.centerYInSuperview()
        box.tintColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
         addSubview(button)
        if mode == .Compare{
            button.anchor(top: nil, leading: box.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 89.calcvaluex(), height: 32.calcvaluey()))
        }
        else{
         buttonanchor = button.anchor(top: nil, leading: box.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 89.calcvaluex(), height: 32.calcvaluey()))
        }
    }
    @objc func goProductInfo(){
        //delegate?.goToProduct(seriesIndex: <#Int#>, productIndex: <#Int#>, subIndex: <#Int#>)
    }
//    @objc func checkBox(){
//        self.box.tintColor = .clear
//        
//        self.box.image = #imageLiteral(resourceName: "btn_check_box_pressed")
//        delegate?.addNewItem()
//    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
