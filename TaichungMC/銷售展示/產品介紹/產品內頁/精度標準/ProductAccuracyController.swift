//
//  ProductAccuracyController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductAccuracyController: SampleProductTabController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nx = nextvd as? ProductAccuracyView {
            nx.webview.backgroundColor = .clear
            nx.expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside )
            if let pdf = Bundle.main.url(forResource: "NewSamplePDF", withExtension: "pdf", subdirectory: nil, localization: nil)  {
                print(pdf)
               let req = NSURLRequest(url: pdf)
                pdf.loadWebview(webview: nx.webview)
                //nx.webview.loadRequest(req as URLRequest)
             }
        }

    }
    @objc func expandPDF(){
        let con = ExpandPDFController(text: "NewSamplePDF")
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    
}
