//
//  ProductModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import Foundation
struct topData: Codable {
    var data : [Series]?
}

struct Series : Codable {
    var order : Int?
    var id : String?
    var title : String?
    var titles : Lang?
//
    var status : Int?
    var is_disable : Int?
    var products : [Product]?
    var children : [Series]?
    var assets : Assets?
    var update_token: String?
}
struct Assets : Codable {
    var three360 : [FilePath]?
    var background : [FilePath]?
    var machine : [FilePath]?
    var performance : [FilePath]?
    var accuracy : [FilePath]?
    var diagram : [FilePath]?
    var specification : [FilePath]?
    var image : [FilePath]?
    var file : [FilePath]?
    private enum CodingKeys: String,CodingKey {
        case background,machine,three360 = "360degree",performance,accuracy,diagram,specification,image,file
    }
    
    func getImage() -> String {
        if let ab = image {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remotes?.getFileLang() {
                    return remote
                }
                if let m = ab[0].files?.getLang() {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path_url {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return (f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return f.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    func getMachinePath() -> String {
//        if let mt = emptyTag {
//            if mt.count > 0{
//                if let remote = mt[0].remote {
//                    return remote
//                }
//            }
//
//        }
        if let ab = machine {
            
            if ab.count == 0{
                return ""
            }
            else{
                
                print(991,ab.first?.files)
                if let remote = ab[0].remotes?.getFileLang() {
                    return remote
                }
                
                
                if let m = ab[0].files?.getLang() {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path_url {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return (f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return f.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func getBackgroundPath() -> String {

        if let ab = background {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remotes?.getFileLang() {
                    return remote
                }
                
                
                if let m = ab[0].files?.getLang() {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path_url {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return (f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return f.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    
}

struct FilePath : Codable {
    var id: String?
    var remote : String?
    var remotes : Lang?
    var files : Files?
    var tags : [String]?
    var title: String?
    var titles : Lang?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(remote, forKey: .remote)
        try container.encode(remotes, forKey: .remotes)
        try container.encode(files, forKey: .files)
        try container.encode(tags, forKey: .tags)
        try container.encode(id, forKey: .id)
        try container.encode(title,forKey: .title)
        try container.encode(titles,forKey: .titles)
        
    }
    func getUrlString() -> String {
        if let remote = remotes?.getFileLang() {
            return remote
        }
        
        if let m = files?.getLang() {
            if let ab = m.first {
                switch ab.path_url {
                case .string(let fr):
                    return fr?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                case .arrayString(let fr):
                    if let mt = fr?.first {
                        return mt?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                    }
                    
                default:
                    ()
                }
                
            }
        }
        
        return ""
        
        
    }
    
    
}

struct compareSection {
    var title:String
    var array : [NewSpecField]
}

enum filterDataMode {
    case lessThan
    case inBetween
    case equal
    case greaterThan
}
struct filterSection {
    var title:String
    var code: String
    var val: [filterData]
    var unit:String
    

}
struct filterData{
    var mode:filterDataMode
    var value:String
}
