//
//  ProductIntroController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductIntroController : SampleController {
    let activityController = UIActivityIndicatorView(style: .whiteLarge)
    let tableview = UITableView(frame: .zero, style: .plain)
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        background_view.image = #imageLiteral(resourceName: "background_production")
        //topview.isHidden = true
       // titleviewanchor.leading?.constant = 333.calcvaluex()
        titleview.label.text = "產品介紹".localized
        view.addSubview(tableview)
        tableview.anchor(top: topview.bottomAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 669.calcvaluex(), height: 0))
        tableview.contentInset = .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0)
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        //tableview.clipsToBounds = true
        tableview.dataSource = self
        tableview.delegate = self
        
        view.addSubview(activityController)
        activityController.backgroundColor = #colorLiteral(red: 0.8848801295, green: 0.8848801295, blue: 0.8848801295, alpha: 1)
        activityController.constrainWidth(constant: 60.calcvaluex())
        activityController.constrainHeight(constant: 60.calcvaluey())
        activityController.centerYInSuperview()
        activityController.centerXAnchor.constraint(equalTo: tableview.centerXAnchor).isActive = true
        activityController.hidesWhenStopped = true
        
    }
    var series = [Series]() {
        didSet{
            
            DispatchQueue.main.async {
                self.tableview.reloadData()
                self.tableview.isHidden = false
                self.activityController.stopAnimating()
            }
            
        }
    }
    override func changeLang() {
        super.changeLang()
        self.tableview.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        activityController.startAnimating()
        self.tableview.isHidden = true

            NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
                if let json = json {


                    self.series = json.data?.filter({ (sr) -> Bool in
                        return sr.status == 1
                    }).sorted(by: { (sr, sr1) -> Bool in
                        return  (sr.order ?? 0) < (sr1.order ?? 0)
                    }) ?? []
                    for i in self.series {
                        if UserDefaults.standard.getUpdateToken(id: i.id ?? "") == ""{
                            UserDefaults.standard.saveUpdateToken(token: i.update_token ?? "", id: i.id ?? "")
                        }
                    }
                }
                else{
                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                    
                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                    
                                    self.present(alert, animated: true, completion: nil)
                                    return
                }

            }
       
        


            
        
    }
}
extension ProductIntroController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if series[indexPath.row].is_disable == 1{
            return
        }
        DispatchQueue.main.async {
            let vd = InnerProductIntroController()
            vd.modalPresentationStyle = .fullScreen
            switch self.series[indexPath.row].title {
            case "五軸加工中心機":
                vd.fitlerArr = FilterArray().val1
            case "多軸複合加工機":
                vd.fitlerArr = FilterArray().val2
            case "CNC車床":
                vd.fitlerArr = FilterArray().val3
            case "綜合加工機":
                vd.fitlerArr = FilterArray().val4
                
            case "鋁圈專用機":
                vd.fitlerArr = FilterArray().val5
            default:
                vd.fitlerArr = []
                vd.cusFilter.isHidden = true
            }
            vd.titleview.label.text = self.series[indexPath.row].titles?.getLang()
            vd.nameLabel.text = self.series[indexPath.row].titles?.getLang()
            self.activityController.startAnimating()
            if let category = UserDefaults.standard.getCatValueData(id: self.series[indexPath.row].id ?? ""){
                print(113,UserDefaults.standard.getUpdateToken(id: self.series[indexPath.row].id ?? ""))
                NetworkCall.shared.postCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")/check", dict: ["check_token":UserDefaults.standard.getUpdateToken(id: self.series[indexPath.row].id ?? "")], decoderType: UpdateClass.self) { (json) in
                    DispatchQueue.main.async {
                        print(665,json)
                        if let json = json {
                            
                            if !(json.data ?? false) {
                                
                                    NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
                                                                    if let json = json {

                                                                        if let dt = json.data?.first(where: { (st) -> Bool in
                                                                            return st.id == self.series[indexPath.row].id
                                                                        }) {
                                                                            UserDefaults.standard.saveUpdateToken(token: dt.update_token ?? "", id: dt.id ?? "")
                                                                            print(114,UserDefaults.standard.getUpdateToken(id: self.series[indexPath.row].id ?? ""))
                                                                    }
                                                                    }
                                                                    else{
                                                                                        let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                                
                                                                                        alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                                
                                                                                        self.present(alert, animated: true, completion: nil)
                                                                                        return
                                                                    }
                                
                                                                }
                                    
                                
                                
                                NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")", decoderType: topData.self) { (json) in
                                    if let json = json{
                                    do{
                                        let encode = try JSONEncoder().encode(json)
                                        UserDefaults.standard.saveCatValueData(data: encode, id: self.series[indexPath.row].id ?? "")
                                    }
                                    catch{
                                        
                                    }
                                    DispatchQueue.main.async {
                                        vd.series = json.data?.filter({ (sr) -> Bool in
                                            return sr.status == 1
                                        }) ?? []
                                        
                                        self.present(vd, animated: true, completion: nil)
                                    }
                                    }
                                    else{
                                        do{
                                            let json = try JSONDecoder().decode(topData.self, from: category)
                                            vd.series = json.data ?? []
                                            self.present(vd, animated: true, completion: nil)
                                        }
                                        catch let error{
                                            print(371,error)
                                        }
                                    }
                                }
                            }
                            else{
                                do{
                                    let json = try JSONDecoder().decode(topData.self, from: category)
                                    vd.series = json.data ?? []
                                    self.present(vd, animated: true, completion: nil)
                                }
                                catch let error{
                                    print(371,error)
                                }
                            }
                        }
                        else{
                                            do{
                                                let json = try JSONDecoder().decode(topData.self, from: category)
                                                vd.series = json.data?.filter({ (sr) -> Bool in
                                                    return sr.status == 1
                                                }) ?? []
                                                self.present(vd, animated: true, completion: nil)
                                            }
                                            catch let error{
                                                print(371,error)
                                            }
                        }
                    }
                }

            }
            else{

            NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")", decoderType: topData.self) { (json) in
                if let json = json{
                do{
                    let encode = try JSONEncoder().encode(json)
                    UserDefaults.standard.saveCatValueData(data: encode, id: self.series[indexPath.row].id ?? "")
                }
                catch{
                    
                }
                DispatchQueue.main.async {
                    vd.series = json.data?.filter({ (sr) -> Bool in
                        return sr.status == 1
                    }) ?? []
                    
                    self.present(vd, animated: true, completion: nil)
                }
                }
                else{
                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            }
            }

            

        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ProductIntroCell(style: .default, reuseIdentifier: "cell")
        cell.topLabel.text = series[indexPath.row].titles?.getLang()
        //cell.detailLabel.text = series[indexPath.row].titles?.en
  
        cell.img.sd_setImage(with: URL(string: series[indexPath.row].assets?.getMachinePath() ?? ""), completed: nil)
        if series[indexPath.row].is_disable == 1{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0.7)
        }
        else{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = tableview.indexPathsForVisibleRows?.last else {
            return false
        }
        return lastIndexPath == indexPath
    }

}

class ProductIntroCell:UITableViewCell{
    let container = UIView()
    let img = UIImageView()
    let topLabel = UILabel()
    let detailLabel = UILabel()
    let nextarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    var alphabackground = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        container.layer.cornerRadius = 15.calcvaluex()
        container.layer.shadowColor = UIColor.black.cgColor
        container.layer.shadowOffset = .init(width: 8.calcvaluex(), height: 0)
        container.layer.shadowRadius = 3.calcvaluex()
        container.layer.shadowOpacity = 0.1
        container.layer.shouldRasterize = true
        container.layer.rasterizationScale = UIScreen.main.scale
        contentView.addSubview(container)
        container.anchor(top: contentView.topAnchor, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 593.calcvaluex(), height: 108.calcvaluey()))
        
        img.contentMode = .scaleAspectFit
        container.addSubview(img)
        img.anchor(top: container.topAnchor, leading: nil, bottom: container.bottomAnchor, trailing: nil,size: .init(width: 119.calcvaluex(), height: 0))
        img.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        img.centerXAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        
        container.addSubview(nextarrow)
        nextarrow.contentMode = .scaleAspectFit
        nextarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        nextarrow.centerYInSuperview()
        container.addSubview(topLabel)
        //topLabel.text = "五軸加工機"
        topLabel.font = UIFont(name: "Roboto-Medium", size: 30.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        topLabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nextarrow.leadingAnchor,padding: .init(top: 0, left: 101.calcvaluex(), bottom: 0, right: 16.calcvaluex()))
        topLabel.centerYInSuperview()
        
        container.addSubview(detailLabel)
        //detailLabel.text = "5-Axis Machining Centers"
        detailLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        detailLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        detailLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        container.addSubview(alphabackground)
        //alphabackground.backgroundColor = UIColor.white.withAlphaComponent(0)
        alphabackground.layer.cornerRadius = 15.calcvaluex()
        alphabackground.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
