//
//  OceanDetailProductController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class oceanSelectedCompareItemView : UIView {
    var delegate : comparingDelegate?
    let cancelButton = UIButton(type: .custom)
    let imageview = UIImageView()
    let seriesLabel = UILabel()
    let productLabel = UILabel()
    var product : Product? {
        didSet{
            
            seriesLabel.text = product?.parent?.titles?.getLang()
            productLabel.text = product?.titles?.getLang()
            imageview.sd_setImage(with: URL(string: product?.parent?.assets?.getMachinePath() ?? ""), completed: nil)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.contentMode = .scaleAspectFit
        addSubview(imageview)
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 23.calcvaluey(), left: 6.calcvaluex(), bottom: 23.calcvaluey(), right: 0),size: .init(width: 85.calcvaluex(), height: 0))
        
        //seriesLabel.text = "River 3"
        seriesLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        seriesLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        seriesLabel.numberOfLines = 0
        
        productLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        productLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
       // productLabel.text = "River 系列"
        productLabel.numberOfLines = 0
        let vertstack = Vertical_Stackview(spacing:4.calcvaluey(),alignment: .leading)
        vertstack.addArrangedSubview(seriesLabel)
        vertstack.addArrangedSubview(productLabel)
        addSubview(vertstack)
        vertstack.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 10.calcvaluex()))
        vertstack.centerYInSuperview()
        cancelButton.setImage(#imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate), for: .normal)
        cancelButton.tintColor = .white
        cancelButton.backgroundColor = MajorColor().oceanColor
        
        cancelButton.contentVerticalAlignment = .fill
        cancelButton.contentHorizontalAlignment = .fill
        cancelButton.addTarget(self, action: #selector(removeProduct), for: .touchUpInside)
        addSubview(cancelButton)
        
        cancelButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 3.calcvaluey(), left: 6.calcvaluex(), bottom: 0, right: 0),size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        cancelButton.layer.cornerRadius = 10.calcvaluex()
    }
    @objc func removeProduct(){
        if let pr = product {
            delegate?.removeProduct(product: pr)
        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol comparingDelegate {
    func removeProduct(product:Product)
    func goCompare(products:[Product])
}
class oceanComparingView : UIView {
    var delegate : comparingDelegate?
    let topLabel = UILabel()
    let subLabel = UILabel()
    let compareButton = UIButton(type:.custom)
    var selectedProducts = [Product]() {
        didSet{
            verstackview.safelyRemoveArrangedSubviews()
            for i in selectedProducts {
                let v = oceanSelectedCompareItemView()
                v.product = i
                v.delegate = self.delegate
                
                verstackview.addArrangedSubview(v)
            }
            for _ in 0 ..< 3 - selectedProducts.count {
                verstackview.addArrangedSubview(UIView())
            }
            
        }
    }
    let verstackview = Vertical_Stackview(distribution:.fillEqually,spacing: 10.calcvaluey())
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addshadowColor()
        
        layer.cornerRadius = 15.calcvaluex()
        
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        topLabel.text = "產品比較".localized
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        subLabel.text = "最多比較三台機型".localized
        subLabel.font = UIFont(name: MainFont().Regular, size: 14.calcvaluex())
        subLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor ,padding: .init(top: 10.calcvaluey(), left: 14.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 27.calcvaluey()))
        
        addSubview(subLabel)
        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 7.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 19.calcvaluey()))
        
        compareButton.setTitleColor(.white, for: .normal)
        compareButton.setTitle("開始比較".localized, for: .normal)
        compareButton.backgroundColor = MajorColor().oceanColor
        
        compareButton.titleLabel?.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        compareButton.addTarget(self, action: #selector(goCompare), for: .touchUpInside)
        addSubview(compareButton)
        compareButton.anchor(top: nil, leading: topLabel.leadingAnchor, bottom: bottomAnchor, trailing: topLabel.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 14.calcvaluey(), right: 0),size: .init(width: 0, height: 61.calcvaluey()))
        
        addSubview(verstackview)
        verstackview.anchor(top: subLabel.bottomAnchor, leading: subLabel.leadingAnchor, bottom: compareButton.topAnchor, trailing: subLabel.trailingAnchor,padding: .init(top: 7.calcvaluey(), left: 0, bottom: 10.calcvaluey(), right: 0))
    }
    @objc func goCompare(){
        delegate?.goCompare(products: self.selectedProducts)
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class oceanCompareButton : UIView {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 33.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        label.centerYInSuperview()
        label.text = "加入比較".localized
        label.textColor = .white
        label.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        
        backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        constrainHeight(constant: 42.calcvaluey())
        layer.cornerRadius = 21.calcvaluey()
        
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum CompareMode {
    case Cancel
    case Compare
}
protocol OceanDetailProductDelegate {
    func productSelected(productIndex:Int,seriesIndex:Int)
}
class OceanDetailProductController: SampleController,OceanDetailProductDelegate,comparingDelegate {
    var removeProduct : Product? {
        didSet{
            if let seriesIndex = series.firstIndex(where: { sr in
                return sr.id == removeProduct?.parent?.id
            }), let productIndex = series[seriesIndex].products?.firstIndex(where: { pr in
                return pr.id == removeProduct?.id
            }) {
                productSelected(productIndex: productIndex, seriesIndex: seriesIndex)
            }
        }
    }
    func goCompare(products: [Product]) {
        if products.count == 0{
            let alert = UIAlertController(title: "提示".localized, message: "請至少選擇一台機型".localized, preferredStyle: .alert)
            
             alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        SpecArray.setCompareFields(products: products)
        let con = ComparingItemController()
        con.con = self
        con.selectedProductArray = products
        self.navigationController?.pushViewController(con, animated: true)
        
    }
    
    func removeProduct(product: Product) {
        if let seriesIndex = series.firstIndex(where: { sr in
            return sr.id == product.parent?.id
        }), let productIndex = series[seriesIndex].products?.firstIndex(where: { pr in
            return pr.id == product.id
        }) {
            productSelected(productIndex: productIndex, seriesIndex: seriesIndex)
        }
    }
    
    func productSelected(productIndex:Int,seriesIndex:Int) {
        if mode == .Compare {
            if comparingView.selectedProducts.count == 3 && !(series[seriesIndex].products?[productIndex].selected ?? false){
                let alert = UIAlertController(title: "提示".localized, message: "最對只能選擇三比產品".localized, preferredStyle: .alert)
                
                 alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
           let selected = series[seriesIndex].products?[productIndex].selected ?? false
            series[seriesIndex].products?[productIndex].selected = !selected
            var pr = series[seriesIndex].products?[productIndex]
            
            pr?.parent = series[seriesIndex]
            if pr?.selected ?? false {
                
                
                    comparingView.selectedProducts.append(pr!)
                
                
            }
            else{
                if let index = comparingView.selectedProducts.firstIndex(where: { pr1 in
                    return pr1.id == pr?.id
                }) {
                    comparingView.selectedProducts.remove(at: index)
                }
            }
            
            self.tableview.reloadData()
        }
        else{
            let vd2 = ProductInnerInfoController()
            vd2.modalPresentationStyle = .fullScreen
            
            vd2.titleview.label.text = series[seriesIndex].titles?.getLang()
            vd2.tabcon.selectedPreTitle = series[seriesIndex].products?[productIndex].titles?.getLang() ?? ""
            vd2.tabcon.selectedSeries = series[seriesIndex]
            if let product = series[seriesIndex].products?[productIndex] {
                vd2.tabcon.selectedProduct = product
                SpecArray.constructValue(field: product.specifications?.first?.fields ?? [])
            }
           
//            vd2.selectedProductArray = self.selectedProductArray
//            vd2.tabcon.productIndex = productIndex
//            vd2.tabcon.productSubIndex = subIndex
            //vd2.tabcon.product = products[seriesIndex][productIndex]
//            vd2.tabcon.product = seriesArray[seriesIndex].products
//
//            vd2.seriesIndex = seriesIndex
//
//            vd2.seriesArray = self.seriesArray
//            if let pr = seriesArray[seriesIndex].products[productIndex].array[subIndex]?.product {
//                setSpec(products: pr)
//            }
            
            self.navigationController?.pushViewController(vd2, animated: true)
        }
    }
    
    
    var mode = CompareMode.Cancel
    let backgroundView = UIView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var count = 0
    var series = [Series]() {
        didSet{
//            for i in series {
//                var product = [Product]()
//                for j in i.children ??  [] {
//                    for k in j.products ?? [] {
//                        product.append(k)
//                    }
//                }
//                products.append(product)
//
//                self.tableview.reloadData()
//
//            }
        }
    }
    //var products = [[Product]]()
    let compareButton = oceanCompareButton()
    var comparingView = oceanComparingView()
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        
        view.addSubview(backgroundView)
        backgroundView.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 28.calcvaluey(), left: 25.calcvaluex(), bottom: menu_bottomInset + 30.calcvaluey(), right: 25.calcvaluex()))

        backgroundView.layer.cornerRadius = 15.calcvaluex()
        backgroundView.backgroundColor = .white

        backgroundView.addshadowColor()
        
        backgroundView.addSubview(tableview)
        tableview.fillSuperview()
        tableview.layer.cornerRadius = 15.calcvaluex()
        tableview.backgroundColor = .clear
        
        tableview.delegate = self
        tableview.dataSource = self
        
        tableview.register(OceanProductDetailCell.self, forCellReuseIdentifier: "cell")
        tableview.contentInset.top = 20.calcvaluey()
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        
        view.addSubview(compareButton)
        compareButton.anchor(top: tableview.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        compareButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(comparingAction)))
    
        comparingView.delegate = self
        view.addSubview(comparingView)
        comparingView.anchor(top: compareButton.bottomAnchor, leading: view.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 15.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 179.calcvaluex(), height: 490.calcvaluex()))
    }
    @objc func comparingAction(){
        if mode == .Cancel {
            mode = .Compare
            compareButton.backgroundColor = MajorColor().oceanSubColor
            compareButton.label.text = "取消比較".localized
            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut) {
                self.comparingView.transform = .init(translationX: -179.calcvaluex(), y: 0)
            }

        }
        else{
            mode = .Cancel
            compareButton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
            compareButton.label.text = "加入比較".localized
            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut) {
                self.comparingView.transform = .identity
            }
        }
        self.tableview.beginUpdates()
        self.tableview.reloadData()
        self.tableview.endUpdates()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + 2) {
//            DispatchQueue.main.async {
//                self.count = 10
//                self.tableview.reloadData()
//            }
//
//        }
    }
}

extension OceanDetailProductController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OceanProductDetailCell
        cell.frame = tableView.bounds
        //cell.count = self.count
        cell.delegate = self
        cell.tag = indexPath.row
        cell.mode = self.mode
        cell.series = series[indexPath.row]
//        cell.products = products[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 209.calcvaluey()
    }
}
extension OceanProductDetailCell : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return series?.products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OceanProductCollectionCell
        let product = series?.products?[indexPath.item]
        cell.label.text = product?.titles?.getLang()
        if mode == .Compare {
            cell.imageview.isHidden = false
            if product?.selected ?? false {
                cell.setSelected()
            }
            else{
                cell.setUnSelected()
            }
        }
        else{
            cell.setUnSelected()
            cell.imageview.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        delegate?.productSelected(productIndex: indexPath.item, seriesIndex: self.tag)
        
        
    }
    
    
}
class OceanProductCollectionCell : UICollectionViewCell {
    let vd = UIView()
    let label = UILabel()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal"))
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        let h_stack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .center)
        vd.constrainHeight(constant: 32.calcvaluey())
        vd.widthAnchor.constraint(greaterThanOrEqualToConstant: 105.calcvaluex()).isActive = true
        vd.layer.cornerRadius = 32.calcvaluey()/2
        vd.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        label.textAlignment = .center
        //label.text = "River 300"
        label.textColor = .white
        label.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        vd.addSubview(label)
        label.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 17.calcvaluex(), bottom: 0, right: 17.calcvaluex()))
        label.centerYInSuperview()
        imageview.tintColor = MajorColor().oceanSubColor
        imageview.constrainWidth(constant: 18.calcvaluex())
        imageview.constrainHeight(constant: 18.calcvaluex())
        imageview.isHidden = true
        h_stack.addArrangedSubview(imageview)
        h_stack.addArrangedSubview(vd)
        addSubview(h_stack)
        h_stack.fillSuperview()
    }
    func setSelected(){
        vd.backgroundColor = MajorColor().oceanColor
        imageview.image = #imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate)
        
    }
    func setUnSelected(){
        vd.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        imageview.image = #imageLiteral(resourceName: "btn_check_box_normal")
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class OceanProductDetailCell : UITableViewCell {
    var series : Series? {
        didSet{
            headerLabel.text = series?.titles?.getLang()
            imageview.sd_setImage(with: URL(string: series?.assets?.getMachinePath() ?? ""), completed: nil)
            self.collectionview.reloadData()
            
            
        }
    }
    var mode = CompareMode.Cancel
//    var products : [Product]? {
//        didSet{
//            self.collectionview.reloadData()
//        }
//    }
    var delegate : OceanDetailProductDelegate?
    let horizontal_stackview = Horizontal_Stackview(spacing:56.calcvaluex(),alignment: .fill)
    let imageview = UIImageView()
    let headerLabel = UILabel()
    let dotView = UIImageView(image: #imageLiteral(resourceName: "ic_ point"))
    let collectionview = UICollectionView(frame: .zero,collectionViewLayout: CustomViewFlowLayout())
    var collectionviewheightAnchor : NSLayoutConstraint?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        let seperator = UIView()
        seperator.backgroundColor = #colorLiteral(red: 0.7672041059, green: 0.7639091611, blue: 0.7674782872, alpha: 1)
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 22.calcvaluex(), bottom: 0, right: 22.calcvaluex()),size: .init(width: 0, height: 0.8.calcvaluey()))
        contentView.addSubview(horizontal_stackview)
        horizontal_stackview.fillSuperview(padding: .init(top: 36.calcvaluey(), left: 28.calcvaluex(), bottom: 24.calcvaluey(), right: 200.calcvaluex()))
        imageview.contentMode = .scaleAspectFit
        //imageview.backgroundColor = .red
        imageview.constrainWidth(constant: 247.calcvaluex())
        imageview.constrainHeight(constant: 209.calcvaluey())
        
        horizontal_stackview.addArrangedSubview(imageview)
        
        
        let vert_stack = Vertical_Stackview(spacing:16.calcvaluey())
        //headerLabel.text = "River 系列"
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        
        dotView.constrainWidth(constant: 15.calcvaluex())
        dotView.constrainHeight(constant: 15.calcvaluex())
        let h_stack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .center)
        h_stack.addArrangedSubview(dotView)
        h_stack.addArrangedSubview(headerLabel)
        vert_stack.addArrangedSubview(h_stack)
        vert_stack.addArrangedSubview(collectionview)
       
        collectionview.backgroundColor = .clear
        
        horizontal_stackview.addArrangedSubview(vert_stack)
        
        (collectionview.collectionViewLayout as! CustomViewFlowLayout).estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        (collectionview.collectionViewLayout as! CustomViewFlowLayout).cellSpacing = 15.calcvaluex()
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.register(OceanProductCollectionCell.self, forCellWithReuseIdentifier: "cell")
        
//        collectionviewheightAnchor = collectionview.heightAnchor.constraint(equalToConstant: 0)
//        collectionviewheightAnchor?.isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
