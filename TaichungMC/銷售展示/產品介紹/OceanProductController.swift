//
//  OceanProductController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class OceanProductController: SampleController {
    let tableview = UITableView(frame: .zero, style: .grouped)
    var series = [Series]() {
        didSet{
            
            DispatchQueue.main.async {
                self.tableview.reloadData()
                self.tableview.isHidden = false
                //self.activityController.stopAnimating()
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "產品介紹".localized
        background_view.image = #imageLiteral(resourceName: "background_introdution")
        
        view.addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: menu_bottomInset + 12.calcvaluey(), right: 0),size: .init(width: 529.calcvaluex(), height: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(OceanProductCell.self, forCellReuseIdentifier: "cell")
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        tableview.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        //activityController.startAnimating()
        self.tableview.isHidden = true

            NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
                if let json = json {


                    self.series = json.data?.filter({ (sr) -> Bool in
                        return sr.status == 1
                    }).sorted(by: { (sr, sr1) -> Bool in
                        return  (sr.order ?? 0) < (sr1.order ?? 0)
                    }) ?? []
                    for i in self.series {
                        if UserDefaults.standard.getUpdateToken(id: i.id ?? "") == ""{
                            UserDefaults.standard.saveUpdateToken(token: i.update_token ?? "", id: i.id ?? "")
                        }
                    }
                }
                else{
                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                    
                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                    
                                    self.present(alert, animated: true, completion: nil)
                                    return
                }

            }
       
        


            
        
    }
}
extension OceanProductController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OceanProductCell
        cell.frame = .init(x: 0, y: 0, width: tableView.frame.width, height: 0)
        cell.toplabel.text = series[indexPath.row].titles?.en
        cell.bottomLabel.text = series[indexPath.row].titles?.zhTW
        print(771,series[indexPath.row].assets?.getMachinePath())
        cell.productImage.sd_setImage(with: URL(string: series[indexPath.row].assets?.getMachinePath() ?? ""), completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 4.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 258.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if series[indexPath.row].is_disable == 1{
            return
        }
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        let vd = OceanDetailProductController()
        vd.titleview.label.text = series[indexPath.row].titles?.getLang()
        if let category = UserDefaults.standard.getCatValueData(id: self.series[indexPath.row].id ?? ""){
            
            
            
            NetworkCall.shared.postCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")/check", dict: ["check_token":UserDefaults.standard.getUpdateToken(id: self.series[indexPath.row].id ?? "")], decoderType: UpdateClass.self) { (json) in
                DispatchQueue.main.async {
                    print(665,json)
                    if let json = json {
                        
                        if !(json.data ?? false) {
                            
                                NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
                                                                if let json = json {

                                                                    if let dt = json.data?.first(where: { (st) -> Bool in
                                                                        return st.id == self.series[indexPath.row].id
                                                                    }) {
                                                                        UserDefaults.standard.saveUpdateToken(token: dt.update_token ?? "", id: dt.id ?? "")
                                                                        
                                                                }
                                                                }
                                                                else{
                                                                    hud.dismiss()
                                                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                            
                                                                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                            
                                                                                    self.present(alert, animated: true, completion: nil)
                                                                                    return
                                                                }
                            
                                                            }
                                
                            
                            
                            NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")", decoderType: topData.self) { (json) in
                                if let json = json{
                                do{
                                    let encode = try JSONEncoder().encode(json)
                                    UserDefaults.standard.saveCatValueData(data: encode, id: self.series[indexPath.row].id ?? "")
                                }
                                catch{
                                    
                                }
                                DispatchQueue.main.async {
                                    hud.dismiss()
                                    vd.series = json.data?.filter({ (sr) -> Bool in
                                        return sr.status == 1
                                    }) ?? []
                                    
                                    self.navigationController?.pushViewController(vd, animated: true)
                                }
                                }
                                else{
                                    do{
                                        let json = try JSONDecoder().decode(topData.self, from: category)
                                        vd.series = json.data ?? []
                                        self.navigationController?.pushViewController(vd, animated: true)
                                    }
                                    catch let error{
                                        print(371,error)
                                    }
                                }
                            }
                        }
                        else{
                            do{
                                hud.dismiss()
                                let json = try JSONDecoder().decode(topData.self, from: category)
                                vd.series = json.data ?? []
                                self.navigationController?.pushViewController(vd, animated: true)
                            }
                            catch let error{
                                print(371,error)
                            }
                        }
                    }
                    else{
                                        do{
                                            hud.dismiss()
                                            let json = try JSONDecoder().decode(topData.self, from: category)
                                            vd.series = json.data?.filter({ (sr) -> Bool in
                                                return sr.status == 1
                                            }) ?? []
                                            self.navigationController?.pushViewController(vd, animated: true)
                                        }
                                        catch let error{
                                            print(371,error)
                                        }
                    }
                }
            }

        }
        else{

        NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.series[indexPath.row].id ?? "")", decoderType: topData.self) { (json) in
            if let json = json{
            do{
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveCatValueData(data: encode, id: self.series[indexPath.row].id ?? "")
            }
            catch{
                
            }
            DispatchQueue.main.async {
                hud.dismiss()
                vd.series = json.data?.filter({ (sr) -> Bool in
                    return sr.status == 1
                }) ?? []
                
                self.navigationController?.pushViewController(vd, animated: true)
            }
            }
            else{
                hud.dismiss()
                let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        }

    }
}

class OceanProductCell : UITableViewCell {
    let toplabel = UILabel()
    let bottomLabel = UILabel()
    let productImage = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        let vd = UIView()
        
        vd.backgroundColor = .white
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.addshadowColor()
        contentView.addSubview(vd)
        vd.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 0, right: 25.calcvaluex()),size: .init(width: 0, height: 159.calcvaluey()))
        vd.centerYInSuperview()
        
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_block"))
        vd.addSubview(imageview)
        imageview.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 248.calcvaluex(), height: 61.calcvaluey()))
        
        toplabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        toplabel.textColor = .white
        toplabel.textAlignment = .center
        toplabel.text = "Drilling EDM"
        toplabel.numberOfLines = 2
        imageview.addSubview(toplabel)
        toplabel.centerYInSuperview()
        toplabel.anchor(top: nil, leading: imageview.leadingAnchor, bottom: nil, trailing: imageview.trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        
        let bottomview = UIView()

        vd.addSubview(bottomview)
        bottomview.anchor(top: imageview.bottomAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: imageview.trailingAnchor)
        bottomLabel.text = "細孔放電加工機"
        bottomLabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        bottomLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        bottomLabel.textAlignment = .center
        bottomLabel.numberOfLines = 2
        bottomview.addSubview(bottomLabel)
        bottomLabel.centerYInSuperview()
        bottomLabel.anchor(top: nil, leading: bottomview.leadingAnchor, bottom: nil, trailing: bottomview.trailingAnchor)
        
        contentView.addSubview(productImage)
        
        productImage.contentMode = .scaleAspectFit
        productImage.anchor(top: contentView.topAnchor, leading: nil, bottom: contentView.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 204.calcvaluex(), height: 0))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
