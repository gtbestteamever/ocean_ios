//
//  MachineCollectionViewCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/17.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
protocol MachineCollectionViewDelegate {
    func showDetail()
}
class MachineCollectionViewCell: UICollectionViewCell,UITableViewDelegate,UITableViewDataSource {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let titlelabel = TitleView()
    let labelTableView = UITableView(frame: .zero, style: .grouped)
    let productdetailbutton = UIButton(type: .system)
    var delegate:MachineCollectionViewDelegate?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 18.yscalevalue()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = labelTableViewCell(style: .default, reuseIdentifier: "cellid")
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
                addSubview(imageview)
                imageview.contentMode = .scaleToFill
                imageview.clipsToBounds = true
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 12.yscalevalue(), left: 12.xscalevalue(), bottom: 200.yscalevalue(), right: 0))
        imageview.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1.5/3).isActive = true
        
        addSubview(titlelabel)
        titlelabel.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 24.xscalevalue(), left: 44.xscalevalue(), bottom: 0, right: 0),size: .init(width: 0, height: 80.yscalevalue()))
        
         addSubview(labelTableView)
        labelTableView.backgroundColor = .clear
        labelTableView.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: bottomAnchor, trailing: titlelabel.trailingAnchor,padding: .init(top: 18.yscalevalue(), left: 0, bottom: 200.yscalevalue(), right: 36.xscalevalue()))
                labelTableView.delegate = self
                labelTableView.dataSource = self
                labelTableView.separatorStyle = .none
        
        
        
        productdetailbutton.setTitle("產品介紹", for: .normal)
        productdetailbutton.setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
        productdetailbutton.backgroundColor = .white
        productdetailbutton.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        productdetailbutton.layer.borderWidth = 0.7
        
        addSubview(productdetailbutton)
        productdetailbutton.anchor(top: labelTableView.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: -36.yscalevalue(), left: 0, bottom: 0, right: 0),size: .init(width: 170.xscalevalue(), height: 55.yscalevalue()))
        productdetailbutton.layer.cornerRadius = 55.yscalevalue()/2
        productdetailbutton.centerXAnchor.constraint(equalTo: centerXAnchor,constant: 158.xscalevalue()/2).isActive = true
        productdetailbutton.addTarget(self, action: #selector(showdetail), for: .touchUpInside)
        
    }
    @objc func showdetail(){
        delegate?.showDetail()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
