//
//  ComparisonController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit


class ComparisonController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let backbutton = UIButton(type: .system)
    let stackview = UIStackView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    let filterbutton = UIButton(type: .system)
    override func viewDidLoad() {
        super.viewDidLoad()
        let topview = UIView()
         topview.isUserInteractionEnabled = true
         topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 86))
        let slideinview = UIView()
        view.addSubview(slideinview)
        slideinview.backgroundColor = #colorLiteral(red: 0.8205059171, green: 0.4463779926, blue: 0.01516667381, alpha: 1)
        
        slideinview.anchor(top: view.topAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.size.height))
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        backbutton.setImage(#imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysOriginal), for: .normal)
        topview.addSubview(backbutton)
        backbutton.contentMode = .scaleAspectFill
        backbutton.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.yscalevalue(), bottom: 0, right: 0),size: .init(width: 80.yscalevalue(), height: 80.yscalevalue()))
        backbutton.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.size.height/2).isActive = true
        backbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        
        
        topview.addSubview(filterbutton)
        filterbutton.setImage(#imageLiteral(resourceName: "ic_filter").withRenderingMode(.alwaysOriginal), for: .normal)
        filterbutton.addTarget(self, action: #selector(gofilterpage), for: .touchUpInside)
        filterbutton.contentMode = .scaleAspectFill
        filterbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.yscalevalue()),size: .init(width: 80.yscalevalue(), height: 80.yscalevalue()))
        filterbutton.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.size.height/2).isActive = true
        stackview.addArrangedSubview(ComparingInnerCell())
        for _ in 0...1{
            let vi = UIView()
            vi.backgroundColor = .clear
            let newview = UIView()
            vi.addSubview(newview)
            newview.backgroundColor = .white
            newview.anchor(top: vi.topAnchor, leading: vi.leadingAnchor, bottom: nil, trailing: vi.trailingAnchor,size: .init(width: 0, height: 120.yscalevalue()))
            newview.layer.shadowColor = UIColor.black.cgColor
            newview.layer.shadowOffset = .zero
            newview.layer.shadowRadius = 2
            newview.layer.shadowOpacity = 0.1
            newview.layer.shouldRasterize = true
            newview.layer.rasterizationScale = UIScreen.main.scale
            stackview.addArrangedSubview(vi)
        }
        view.addSubview(stackview)
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.spacing = 60.yscalevalue()
        stackview.centerXInSuperview()
        stackview.anchor(top: topview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 48.yscalevalue(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 294.yscalevalue()))
        stackview.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.5/2).isActive = true
        
        //tableview.layer.cornerRadius = 24
        tableview.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        tableview.contentInset.top = 4

        view.addSubview(tableview)
        tableview.anchor(top: stackview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 24.yscalevalue(), left: 0, bottom: 0, right: 0))
        tableview.showsVerticalScrollIndicator = false
        tableview.delegate = self
        tableview.dataSource = self
        
    }
    @objc func gofilterpage(){
        let vd = ComparisonFilterController()
        
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableview.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let f = UIView()
        f.backgroundColor = .clear
        
        return f
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewd = UIView()
        
        if section == 0 {
            
            viewd.layer.cornerRadius = 20
            if #available(iOS 11.0, *) {
                viewd.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            } else {
                // Fallback on earlier versions
            }
            viewd.layer.shadowColor = UIColor.black.cgColor
            viewd.layer.shadowRadius = 2
            viewd.layer.shadowOpacity = 0.1
            viewd.layer.shadowOffset = .init(width: 0, height: -2)
            viewd.layer.shouldRasterize = true
            viewd.layer.rasterizationScale = UIScreen.main.scale
            
        }
        let label = UILabel()
        label.text = "行程"
        let width = (self.view.frame.width * 0.5/2)/2
        let textwidth = (label.text?.width(withConstrainedHeight: .infinity, font: label.font!))!
        viewd.addSubview(label)
        
        
        label.anchor(top: nil, leading: viewd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: (width - textwidth)/2, bottom: 0, right: 0))
        
        label.centerYInSuperview()
        
        
        viewd.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        return viewd
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ComparisonTableViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
