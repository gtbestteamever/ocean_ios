//
//  ComparisonTableViewCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/19.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ComparisonTableViewCell: UITableViewCell {
    let label = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label.text = "X 軸行程"
        let width = (UIScreen.main.bounds.width * 0.5/2)/2
        let textwidth = (label.text?.width(withConstrainedHeight: .infinity, font: label.font!))!
        addSubview(label)
        
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: (width - textwidth)/2 + textwidth/4, bottom: 0, right: 0))
        label.centerYInSuperview()
        
        let stackview = UIStackView()
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.spacing = 60.yscalevalue()
        for _ in 0...2{
            let vd = UILabel()
            vd.text = "171 mm"
            vd.textAlignment = .center
            
            stackview.addArrangedSubview(vd)
        }
        addSubview(stackview)
        stackview.centerXInSuperview()
        
        stackview.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 1.5/2).isActive = true
        
        stackview.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
