//
//  ComparisonFilterController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/19.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CustomTextField:UITextField{

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 48.calcvaluex(), dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 48.calcvaluex(), dy: 0)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 48.calcvaluex(), dy: 0)
    }
}
enum FilterMode {
    case Selection
    case All
}
struct FilterSelection {
    var title : String
    var mode : FilterMode
    var array : [FilterOption]
    
    init(title:String,array:[NewSpecField]) {
        self.title = title
        self.array = []
        for i in array {
            self.array.append(FilterOption(data: i, selected: true))
        }
        self.mode = .All
    }
}
struct FilterOption {
    var data:NewSpecField
    var selected:Bool
}
enum HighFilterMode {
    case None
    case All
}
class ComparisonFilterController: SampleController,UITableViewDelegate,UITableViewDataSource,FilterOptionDelegate , UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == nil || textField.text == ""{
            filterOptionArray = originalFilter
        }
        else{
       filterOptionArray = originalFilter.filter { (ft) -> Bool in
        if ft.title.lowercased().contains((textField.text ?? "").lowercased()) {
                return true
            }
            
            let index = ft.array.firstIndex { (fa) -> Bool in
                return fa.data.title.lowercased().contains((textField.text ?? "").lowercased())
            }
            
            if index != nil{
                return true
            }
            else{
                return false
            }
        }
        }
        self.tableview.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        self.view.endEditing(true)
        return true
    }
    var originalFilter = [FilterSelection]()
    var mode : HighFilterMode? = .All
    func resetSelection(arr: [FilterOption], index: Int) {
        //先找filter陣列 在用filter後的陣列來找原先的陣列
        filterOptionArray[index].array = arr
        
        if let index = originalFilter.firstIndex(where: { (fm) -> Bool in
            return fm.title == filterOptionArray[index].title
        }) {
            originalFilter[index].array = arr
        }
        
        var count = true
        for m in originalFilter {
            for k in m.array {
                if !k.selected {
                    count = false
                }
            }
        }
        
        if count {
            mode = .All
        }
        else{
            mode = .None
        }
        print(count)
        self.tableview.reloadData()
        view.endEditing(true)
    }
    func resetSelectionMode(mode: FilterMode, index: Int) {
        
        filterOptionArray[index].mode = mode
        
        if let index = originalFilter.firstIndex(where: { (fm) -> Bool in
            return fm.title == filterOptionArray[index].title
        }) {
            originalFilter[index].mode = mode
        }
        self.tableview.reloadData()
        view.endEditing(true)
    }
    var specArray = SpecArray.shared.value
    var filterOptionArray = [FilterSelection]() {
        didSet{

        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterOptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ComparisonFilterOptionCell(style: .default, reuseIdentifier: "cellid")
        cell.selectionlabel.setTitle(filterOptionArray[indexPath.section].title, for: .normal)
        cell.mode = filterOptionArray[indexPath.section].mode
        cell.delegate = self
        cell.array = filterOptionArray[indexPath.section].array
        cell.tag = indexPath.section
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
        return 80
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        
        return vd
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40.yscalevalue()
    }
    @objc func selectAllItem(){
        mode = .All
        for (ind,j) in filterOptionArray.enumerated() {
            filterOptionArray[ind].mode = .All
            for (ind2,_) in j.array.enumerated() {
                filterOptionArray[ind].array[ind2].selected = true
            }
        }
        for (ind,j) in originalFilter.enumerated() {
            originalFilter[ind].mode = .All
            for (ind2,_) in j.array.enumerated() {
                originalFilter[ind].array[ind2].selected = true
            }
        }
        self.tableview.reloadData()
        self.view.endEditing(true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewd = UIView()
        viewd.backgroundColor = .clear
        //viewd.backgroundColor =
        let selectbutton = UIButton(type: .system)
        if mode == .All {
            selectbutton.setTitleColor(.white, for: .normal)
            selectbutton.backgroundColor = MajorColor().mainColor
            
        }
        else{
            selectbutton.setTitleColor(.white, for: .normal)
            selectbutton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        }
        selectbutton.setTitle("全選".localized, for: .normal)

       // selectbutton.setTitleColor(.white, for: .normal)
        selectbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        viewd.addSubview(selectbutton)
        selectbutton.addTarget(self, action: #selector(selectAllItem), for: .touchUpInside)
        
        selectbutton.centerYInSuperview()
        selectbutton.anchor(top: nil, leading: viewd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 160.calcvaluex(), height: 45.calcvaluey()))
        selectbutton.layer.cornerRadius = 45.calcvaluey()/2
        //selectbutton.backgroundColor = .white
        
        selectbutton.addshadowColor()
        return viewd
    }
    //let backbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .grouped)
    
    let searchbar = CustomTextField()
    weak var con: ComparingItemController?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    var saveButton : UIButton = {
        let sv = UIButton(type: .custom)
        sv.setTitle("儲存".localized, for: .normal)
        //sv.backgroundColor = .black
        
        sv.setTitleColor(.white, for: .normal)
        sv.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        sv.layer.cornerRadius = 19.calcvaluey()
        
        return sv
    }()
    @objc func goSave(){
        var ft = [compareSection]()
        
        for i in originalFilter {
            let dt = i.array.filter { (fot) -> Bool in
                return fot.selected
            }
            var abc = [NewSpecField]()
            for j in dt {
                abc.append(j.data)
            }
            if dt.count != 0{
                ft.append(compareSection(title: i.title, array: abc))
            }
        }
       
        print( ft )
        con?.filterOptionArray = originalFilter
        con?.compareDetailArray = ft
        con?.convertBottomDetail(origin: false)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        specArray = SpecArray.shared.value
        topview.addSubview(saveButton)
        saveButton.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        saveButton.addTarget(self, action: #selector(goSave), for: .touchUpInside)
        saveButton.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 100.calcvaluex(), height: 38.calcvaluey()))
        if filterOptionArray.count == 0 {
        filterOptionArray = []
            specArray = SpecArray.shared.value
        for i in specArray {

            filterOptionArray.append(FilterSelection(title: i.title, array: i.array))
        }
        }
        originalFilter = filterOptionArray
        var count = true
        for j in originalFilter {
            if j.mode == .Selection {
                count = false
            }
        }
        
        if count {
            self.mode = .All
        }
        else{
            self.mode = .None
        }
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        settingButton.isHidden = true
        titleview.label.text = "規格篩選".localized
        

        
        view.addSubview(searchbar)
        searchbar.returnKeyType = .search
        
        searchbar.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchbar.backgroundColor = .white
        searchbar.delegate = self
        searchbar.autocorrectionType = .no
        searchbar.autocapitalizationType = .none
        searchbar.clearButtonMode = .always
        searchbar.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        searchbar.layer.cornerRadius = 46.calcvaluey()/2
//        searchbar.layer.shadowColor = #colorLiteral(red: 0.9185375571, green: 0.9209107757, blue: 0.9264624119, alpha: 1)
//        searchbar.layer.shadowRadius = 2
//        searchbar.layer.shadowOffset = .zero
//        searchbar.layer.shadowOpacity = 1
        searchbar.attributedPlaceholder = NSAttributedString(string: "搜尋規格".localized, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
//        searchbar.layer.shouldRasterize = true
        
//        searchbar.layer.rasterizationScale = UIScreen.main.scale
        searchbar.addShadowColor(opacity: 0.05)
        searchbar.leftViewMode = .always
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_search_bar").withRenderingMode(.alwaysOriginal))
        //imageview.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        imageview.frame = .init(x: 16.calcvaluex(), y: 0, width: 30.calcvaluex(), height: 30.calcvaluex())
        imageview.contentMode = .scaleAspectFit
        let test = UIView(frame: .init(x: 16.calcvaluex(), y: 0, width: 30.calcvaluex(), height: 30.calcvaluex()))
        test.addSubview(imageview)
        searchbar.leftView = test
        
        
        view.addSubview(tableview)
        tableview.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        tableview.delegate = self
        tableview.dataSource = self
        //tableview.contentInset = .init(top: 45.calcvaluey(), left: 0, bottom: 0, right: 0)
        tableview.anchor(top: searchbar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 16.calcvaluex(), left: 26.calcvaluex(), bottom: 0, right: 0))

        tableview.separatorStyle = .none
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        tapgesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapgesture)
        
    }
    @objc func didTap(){
        self.view.endEditing(true)
    }

}
