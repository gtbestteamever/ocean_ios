//
//  DetailCustomTabBar.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/21.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class DetailCustomTabBar: UIView {
    let images : [UIImage] = [#imageLiteral(resourceName: "ic_tab_show_normal"),#imageLiteral(resourceName: "ic_tab_list_normal"),#imageLiteral(resourceName: "ic_tab_spec_normal"),#imageLiteral(resourceName: "ic_tab_optional_normal"),#imageLiteral(resourceName: "ic_tab_folder_normal")]
    let labels = ["機台展示","切削應用","產品規格","選配項目","技術文件"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        let stackview = UIStackView()
        for (index,i) in [#imageLiteral(resourceName: "ic_product_ic"),#imageLiteral(resourceName: "ic_tab_list_normal"),#imageLiteral(resourceName: "ic_quotation_normal"),#imageLiteral(resourceName: "ic_tab_optional_normal"),#imageLiteral(resourceName: "ic_tab_folder_normal")].enumerated() {
            let viewd = UIView()
            viewd.backgroundColor = .clear
            stackview.addArrangedSubview(viewd)
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 22)
            label.text = labels[index]
            label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            viewd.addSubview(label)
            
            let imageview = UIImageView(image: i)
            imageview.contentMode = .scaleAspectFill
            viewd.addSubview(imageview)
            imageview.anchor(top: nil, leading: nil, bottom: nil, trailing:nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 40.yscalevalue(), height: 40.yscalevalue()))
            imageview.centerYInSuperview()
            imageview.centerXAnchor.constraint(equalTo: viewd.centerXAnchor,constant: -48.yscalevalue()).isActive = true
            
            label.centerYInSuperview()
            label.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: imageview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 8, bottom: 0, right: 0))
            
            let seperator = UIView()
            seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
            viewd.addSubview(seperator)
            seperator.anchor(top: viewd.topAnchor, leading: nil, bottom: viewd.bottomAnchor, trailing: viewd.trailingAnchor,padding: .init(top: 16, left: 0, bottom: 16, right: 0),size: .init(width: 0.7, height: 0))
            
        }
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        addSubview(stackview)
        stackview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
