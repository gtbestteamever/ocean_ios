//
//  labelTableViewCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class labelTableViewCell: UITableViewCell {
    let circleview = UIView()
    let label = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(circleview)
        circleview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 8, left: 0, bottom: 0, right: 0),size: .init(width: 12, height: 12))
        
        circleview.layer.cornerRadius = 6
        circleview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        label.text = "雙內藏式車削主軸和帶內藏式軸的擺動式頭部在一台機台內實現 4 + 1 軸加工"
        label.numberOfLines = 2
        label.textColor = .black
        addSubview(label)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 24.yscalevalue())
        label.anchor(top: topAnchor, leading: circleview.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 12, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
