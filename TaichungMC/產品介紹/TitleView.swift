//
//  TitleView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class TitleView: UIView {
    let leftv = UIView()
    let header = UILabel()
    let subheader = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(leftv)
        leftv.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 8, right: 0),size: .init(width: 7, height: 0))
        leftv.layer.cornerRadius = 3.5
        leftv.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        header.text = "多軸複合加工機"
        header.textColor = .black
        header.font = UIFont.boldSystemFont(ofSize: 45.yscalevalue())
        addSubview(header)
        header.anchor(top: leftv.topAnchor, leading: leftv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: -4, left: 12, bottom: 0, right: 0))
        addSubview(subheader)
        subheader.text = "VMT-400"
        subheader.textColor = .black
        subheader.font = UIFont(name: "HelveticaNeue-Light", size: 30)
        subheader.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 4, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
