//
//  LoginController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import Network
import Reachability
import FirebaseMessaging
import MBProgressHUD

class iPhoneLoginController: UIViewController,UITextFieldDelegate,URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        DispatchQueue.main.async {

            self.hud.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
            
             
        }
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        DispatchQueue.main.async {
            self.hud.progress = 1
            if let data = try? Data(contentsOf: location) , let fileName = self.saveURL?.lastPathComponent,let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = documentsDirectory.appendingPathComponent("Welcome").appendingPathComponent(fileName)
                
                try? data.write(to: path)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.hud.hide(animated: true)
                self.goMain()
            })
            
        }

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == accountfield.textfield {
            accountDict["username"] = textField.text ?? ""
        }
        if textField == passwordfield.textfield{
            accountDict["password"] = textField.text ?? ""
        }
        print(accountDict)
        stackviewAnchor?.top?.constant = 49.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        stackviewAnchor?.top?.constant = -77.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    let statusview = UIView()
    override var preferredStatusBarStyle: UIStatusBarStyle {

            return .lightContent
        
    }
    let headerlabel = UILabel()
    let imageview = UIImageView()
    
    let accountfield = AccountField()
    let passwordfield = AccountField()
    
    let loginbutton = UIButton(type: .system)
    let language = LanguageSelection()
    let registerlabel = UILabel()
    var accountDict = ["username":"","password":"","firebase_token":Messaging.messaging().fcmToken ?? ""]
    let forgotpassword = UIButton(type: .custom)
    var stackviewAnchor:AnchoredConstraints?
    func resetLang(){
       
            headerlabel.text = "會員登入".localized
            accountfield.header.text = "帳號".localized
            forgotpassword.setTitle("忘記密碼?".localized, for: .normal)
            passwordfield.header.text = "密碼".localized
            passwordfield.textfield.attributedPlaceholder = NSAttributedString(string: "0-8位數密碼, 請區分大小寫".localized, attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!])
            
            loginbutton.setTitle("登入".localized, for: .normal)
        

    }
    var window = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resetLang()
        print(7789,accountDict)
        if UserDefaults.standard.getToken() != "" {

//            let vd = UINavigationController(rootViewController: ViewController())
            let vd = iPhoneMainTabBarController()
            vd.modalPresentationStyle = .fullScreen

            self.present(vd, animated: false, completion: nil)
            
            
        }
        else{
            UserDefaults.standard.saveShowPrice(bool: true)
            
        }
        view.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        navigationController?.navigationBar.isHidden = true
        view.addSubview(statusview)
        statusview.backgroundColor = MajorColor().oceanColor
        statusview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.height))
        
        
        headerlabel.font = UIFont(name: "Roboto-Medium", size: 30.calcvaluex())
        headerlabel.textAlignment = .center
//        headerlabel.constrainWidth(constant: UIScreen.main.bounds.width)
//        headerlabel.constrainHeight(constant: (headerlabel.text?.height(withConstrainedWidth: .infinity, font: headerlabel.font))!)
        
        //imageview.backgroundColor = .red
        imageview.contentMode = .scaleAspectFit
        imageview.image = #imageLiteral(resourceName: "ic_logo")
        imageview.constrainWidth(constant: 140.calcvaluex())
        imageview.constrainHeight(constant: 108.calcvaluey())
        
        
        accountfield.textfield.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        accountfield.textfield.autocorrectionType = .no
        accountfield.textfield.autocapitalizationType = .none
        accountfield.textfield.attributedPlaceholder = NSAttributedString(string: "example@gmail.com", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        //accountfield.constrainHeight(constant: 97.calcvaluey())
        accountfield.constrainWidth(constant: 500.calcvaluex())
        accountfield.textfield.delegate = self
        forgotpassword.setTitleColor(MajorColor().mainColor, for: .normal)
        
        forgotpassword.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        passwordfield.textfield.isSecureTextEntry = true
        passwordfield.textfield.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        passwordfield.textfield.delegate = self

        passwordfield.addSubview(forgotpassword)
        
        
        forgotpassword.anchor(top: passwordfield.topAnchor, leading: nil, bottom: passwordfield.textfield.topAnchor, trailing: passwordfield.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 6.calcvaluey(), right: 4.calcvaluex()))
        //forgotpassword.backgroundColor = .red
        forgotpassword.addTarget(self, action: #selector(gorecover), for: .touchUpInside)
        
       // passwordfield.constrainHeight(constant: 97.calcvaluex())
        passwordfield.constrainWidth(constant: 500.calcvaluex())
        
        
        loginbutton.backgroundColor = MajorColor().oceanSubColor
        
        loginbutton.setTitleColor(.white, for: .normal)
        loginbutton.constrainWidth(constant: 124.calcvaluex())
        loginbutton.constrainHeight(constant: 48.calcvaluey())
        loginbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        loginbutton.layer.cornerRadius = 48.calcvaluey()/2
        loginbutton.addTarget(self, action: #selector(gologin), for: .touchUpInside)
        //language.backgroundColor = .green
        language.constrainHeight(constant: 28.calcvaluey())
        language.constrainWidth(constant: UIScreen.main.bounds.width)
        language.mandarin.addTarget(self, action: #selector(changeLanguage), for: .touchUpInside)
        language.english.addTarget(self, action: #selector(changeLanguage), for: .touchUpInside)
        registerlabel.isHidden = true
        registerlabel.textAlignment = .center
        
        let attr = NSMutableAttributedString(string: "還沒有帳號?",attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        attr.append(NSAttributedString(string: "  註冊  ", attributes: [NSAttributedString.Key.foregroundColor : MajorColor().mainColor,NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!]))
        
        registerlabel.attributedText = attr
        registerlabel.isUserInteractionEnabled = true
        registerlabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goregister)))
        
        let stackview = UIStackView(arrangedSubviews: [headerlabel,imageview,accountfield,passwordfield,loginbutton,language,registerlabel,UIView()])
        stackview.axis = .vertical
        stackview.spacing = 34.yscalevalue()
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(8.calcvaluey(), after: stackview.arrangedSubviews[0])
            stackview.setCustomSpacing(38.calcvaluey(), after: stackview.arrangedSubviews[1])
            stackview.setCustomSpacing(26.calcvaluey(), after: stackview.arrangedSubviews[2])
            stackview.setCustomSpacing(22.calcvaluey(), after: stackview.arrangedSubviews[3])
            stackview.setCustomSpacing(22.calcvaluey(), after: stackview.arrangedSubviews[4])
            stackview.setCustomSpacing(62.calcvaluey(), after: stackview.arrangedSubviews[5])
        } else {
            // Fallback on earlier versions
        }
        
        stackview.distribution = .fill
        stackview.alignment = .center
        view.addSubview(stackview)
        stackviewAnchor = stackview.anchor(top: statusview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 49.calcvaluey(), left: 16.calcvaluex(), bottom: 0, right: 16.calcvaluex()))
        view.bringSubviewToFront(statusview)
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEdit))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)

        
        view.addSubview(window)
        window.backgroundColor = .white
        window.fillSuperview()
        window.isHidden = false
    }
    @objc func changeLanguage(sender:UIButton){
        var lang = ""
        if sender.tag == 0{
            lang = "zh-TW"
            language.setChinese()
        }
        else{
            lang = "en"
            language.setEnglish()
        }
        
        UserDefaults.standard.saveAppLanguage(lang: lang)
        
        resetLang()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Messaging.messaging().token { (str, err) in
            self.accountDict["firebase_token"] = str

        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.window.isHidden = true
        }
        resetLang()
        
            
            
            if UserDefaults.standard.getConvertedLanguage() == "en"{
                language.setEnglish()
            }
            else{
                language.setChinese()
            }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @objc func endEdit(){
        view.endEditing(true)
    }
    func goMain(){
//                                        let vd = UINavigationController(rootViewController: ViewController())
        let vd = iPhoneMainTabBarController()
                                        vd.modalPresentationStyle = .fullScreen
                                        
                                        self.present(vd, animated: true, completion: nil)
    }
    @objc func gologin(){
        view.endEditing(true)

        if accountDict["username"] == "" || accountDict["password"] == "" {
            let alert = UIAlertController(title: "請確認帳號密碼是否輸入正確".localized, message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "請確認有連接到網路".localized, message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
 

        let hud = JGProgressHUD()
        hud.textLabel.text = "登入中...".localized
        hud.show(in: self.view)
        print(881)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            print(self.accountDict)
            NetworkCall.shared.postCall(parameter: "api-victor/v1/auth/login", dict: self.accountDict, decoderType: Account.self) { (json) in
                DispatchQueue.main.async {
                    
                if let json = json{
                    FileManager.default.createDirectory(name: downloadFolder)
                    UserDefaults.standard.saveUserId(id: json.user.id)
                    UserDefaults.standard.saveToken(token: json.access_token)
                    hud.textLabel.text = "成功登入".localized
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    
                    hud.dismiss(afterDelay: 0.3, animated: true) {
                        self.fetchApi()
//                                let vd = UINavigationController(rootViewController: ViewController())
//                                vd.modalPresentationStyle = .fullScreen
//                                vd.view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
//                                self.present(vd, animated: true, completion: nil)
                    }
                }
                else{
                    hud.dismiss()
                    let alert = UIAlertController(title: "請確認帳號密碼是否輸入正確".localized, message: nil, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                    
                }
            }
        }


    }
    var saveURL : URL?
    func clearFolder(path:URL){
        do {
            let filePaths = try FileManager.default.contentsOfDirectory(atPath: path.path)
            for filePath in filePaths {
                try FileManager.default.removeItem(atPath: path.path + filePath)
            }
        }
        catch{
            
        }
    }
    var session : URLSession?
    var hud = MBProgressHUD()
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/setting", decoderType: SettingModel.self) { json in
            DispatchQueue.main.async {
                if let json = json{
                    if let url_str =  json.data?.apprelated?.main_url?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: url_str),let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let directoryPath = documentsDirectory.appendingPathComponent("Welcome")
                        let final_path = directoryPath.appendingPathComponent(url.lastPathComponent)
                        
                        self.saveURL = url
                        
                        print(661,final_path.path)
                        if !FileManager.default.fileExists(atPath: final_path.path) {
                            self.clearFolder(path: directoryPath)
                            
                                self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.hud.label.text = "下載中...".localized
                                self.hud.label.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
                                self.hud.mode = .annularDeterminate

                                self.hud.progress = 0
                                
                                
                                
                                let request = URLRequest(url: url)

                                self.session = URLSession(configuration: .background(withIdentifier: UUID().uuidString), delegate: self, delegateQueue: nil)
                                
                                self.session?.downloadTask(with: request).resume()
                            

                        }
                        else{
                            self.goMain()
                        }


                    }
                }
                else{
                    self.goMain()
                }
            }
        }
    }
    @objc func gorecover(){
        print(9993)
        let vd = ForgotPasswordController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    @objc func goregister(gesture:UITapGestureRecognizer){
        let text = "還沒有帳號?  註冊  "
        let termsRange = (text as NSString).range(of: "  註冊  ")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")

        if gesture.didTapAttributedTextInLabel(label: registerlabel, inRange: termsRange) {
            
            let vd = RegisterController()
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
//
//            //self.dismiss(animated: true, completion: nil)
//           // self.view.endEditing(true)
        }else {
            print("Tapped none")
        }
    }
}
 

