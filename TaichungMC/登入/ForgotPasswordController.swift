//
//  ForgotPasswordController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class StepView : UIView {
    let topview = UIView()
    
    let bottomlabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(topview)
        topview.backgroundColor = #colorLiteral(red: 0.9175441861, green: 0.917704761, blue: 0.9175459743, alpha: 1)
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 4, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 4))
        
        addSubview(bottomlabel)
        bottomlabel.text = "STEP1 輸入帳號"
        bottomlabel.textAlignment = .center
        bottomlabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        bottomlabel.textColor = MajorColor().oceanlessColor
        bottomlabel.anchor(top: topview.bottomAnchor, leading: topview.leadingAnchor
            , bottom: bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 12, left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SelectedView : UIView {
    let vd = UIView()
    let bl = UIView()
    var centerxconstraint:NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(vd)
        vd.backgroundColor = MajorColor().oceanSubColor
        vd.centerYInSuperview()
        vd.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size:.init(width: 0, height: 4))
        vd.layer.cornerRadius = 2
        
        
        vd.addSubview(bl)
        bl.isHidden = true
        bl.backgroundColor = .black
        bl.centerYInSuperview()
        centerxconstraint = bl.centerXAnchor.constraint(equalTo: vd.centerXAnchor)
        centerxconstraint.isActive = true
        bl.constrainWidth(constant: 106.calcvaluex())
        bl.constrainHeight(constant: 8.calcvaluey())
        bl.layer.cornerRadius = 8.calcvaluey()/2
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ForgotPasswordController: SampleController {
    let selectionview = SelectedView()
    var selectionanchor : AnchoredConstraints!
    var stackview = UIStackView()
    let button = UIButton(type: .system)
    var current = 0
    let toplabel = AccountField()
    let step2 = Step2View()
    var step2anchor : AnchoredConstraints!
    let step3 = Step3View()
    var step3anchor : AnchoredConstraints!
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        view.backgroundColor = .white
        titleview.label.text = "忘記密碼".localized

        button.setTitle("下一步".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        button.backgroundColor = MajorColor().oceanColor
        

        
        button.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        
        view.addSubview(toplabel)
        toplabel.header.text = "請輸入帳號".localized
        toplabel.textfield.attributedPlaceholder = NSAttributedString(string: "example@gmail.com", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])

        view.addSubview(button)
        //button.centerInSuperview()

        
        
        view.addSubview(step2)
        //step2.backgroundColor = .white

        step2.button.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        
        
        view.addSubview(step3)
        //step3.backgroundColor = .white
        step3.button.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        //stackview = UIStackView(arrangedSubviews: [StepView(),StepView(),StepView()])

        
        for (index,i) in ["STEP1 輸入Email".localized,"STEP2 輸入驗證碼".localized,"STEP3 重新設定密碼".localized].enumerated() {
            let stepview = StepView()
            stepview.bottomlabel.text = i
            if index == 0{
                stepview.bottomlabel.textColor = .black
            }
            stackview.addArrangedSubview(stepview)
        }
        stackview.axis = .horizontal
        stackview.spacing = 0
        stackview.distribution = .fillEqually
        
        view.addSubview(stackview)
        if UIDevice().userInterfaceIdiom == .pad {
        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 36.calcvaluex(), left: 48.calcvaluex(), bottom: 0, right: 48.calcvaluex()))
        }
        else{
            stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 36.calcvaluex(), left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        }
        toplabel.anchor(top: stackview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 120.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 570.calcvaluex() : UIScreen.main.bounds.width - 40.calcvaluex(), height: 97.calcvaluey()))
        toplabel.centerXInSuperview()
        button.constrainHeight(constant: 48.calcvaluey())
        button.constrainWidth(constant: 124.calcvaluex())
        button.layer.cornerRadius = 48.calcvaluey()/2
        button.anchor(top: toplabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 0))
        button.centerXInSuperview()
        
        step2anchor = step2.anchor(top: topview.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: UIScreen.main.bounds.width, height: 0))
        
        step3anchor = step3.anchor(top: topview.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: UIScreen.main.bounds.width, height: 0))
        view.addSubview(selectionview)
        let st = stackview.arrangedSubviews[0] as! StepView
        selectionanchor = selectionview.anchor(top: st.topAnchor, leading: st.leadingAnchor, bottom: nil, trailing: st.trailingAnchor,padding: .init(top: 2, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 8))
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popDismiss)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func showKeyboard(notification : Notification){
        //if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= topview.frame.height + 10.calcvaluey()
            }
       // }
    }
    @objc func hideKeyboard(notification : Notification){
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    @objc func popDismiss(){
        self.view.endEditing(true)
    }
    var userInfo = [String:String]()
    func getCode(){
        button.isEnabled = false
        let hud = JGProgressHUD()
        hud.show(in: self.toplabel.textfield)
        let username = toplabel.textfield.text ?? ""
        if username == "" {
            hud.dismiss()
            let alert = UIAlertController(title: "請輸入使用者帳號".localized, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            button.isEnabled = true
            return
        }
        
        userInfo["username"] = username
        NetworkCall.shared.postCall(parameter: "api-or/v1/password/forget", dict: ["username":"\(username)"], decoderType: ForgotCl.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                self.button.isEnabled = true
                if let json = json{
                    if json.data{
                       
                        let attributedString = NSMutableAttributedString(string: "已寄送驗證碼至".localized, attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
                        attributedString.append(NSAttributedString(string: " \(username)", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!, NSAttributedString.Key.foregroundColor : MajorColor().oceanColor ]))
                        self.step2.toplabel.header.attributedText = attributedString
                        self.goNext()
                    }
                    else{
                        let alert = UIAlertController(title: "您輸入的帳號不存在".localized, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else{
                    let alert = UIAlertController(title: "請確認您有連上網路".localized, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
        
        
       
    }
    
    func confirmCode(){
        step2.button.isEnabled = false
        let hud = JGProgressHUD()
        hud.show(in: self.step2.toplabel.textfield)
        let code = step2.toplabel.textfield.text ?? ""
        if code == "" {
            hud.dismiss()
            let alert = UIAlertController(title: "請輸入驗證碼".localized, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            step2.button.isEnabled = true
            return
        }
        
        userInfo["code"] = code
        NetworkCall.shared.postCallform(parameter: "api-or/v1/password/check", dict: ["username":userInfo["username"] ?? "","token":code], decoderType: ForgotCl.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                self.step2.button.isEnabled = true
                if let json = json {
                    if json.data{
                        self.goNext()
                    }
                    else{
                        let alert = UIAlertController(title: "您輸入的驗證碼不正確".localized, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    let alert = UIAlertController(title: "請確認您有連上網路".localized, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
        
        
    }
    
    func goNext(){
        if current < stackview.arrangedSubviews.count - 1{
            
            if current == 0{
                toplabel.isHidden = true
                button.isHidden = true
                step2anchor.leading?.constant = -UIScreen.main.bounds.width
                
            }
            else if current == 1{
                step2.isHidden = true
                step3anchor.leading?.constant = -UIScreen.main.bounds.width
            }
            let st = stackview.arrangedSubviews[current] as! StepView
        let st2 = stackview.arrangedSubviews[current+1] as! StepView
            selectionview.centerxconstraint.constant = (st.frame.width * CGFloat((current + 1)))/2
        selectionanchor.trailing?.constant = st2.frame.width * CGFloat((current + 1))
            self.backbutton.tintColor = UIColor.lightGray
        UIView.animate(withDuration: 0.7) {
            self.view.layoutIfNeeded()
            self.backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
            
            if self.current == 0{
                self.toplabel.isHidden = true
                self.button.isHidden = true
                
                
            }
            else if self.current == 1{
                self.step2.isHidden = true
               
            }
        } completion: { (com) in
            if com {
                for (index,i) in self.stackview.arrangedSubviews.enumerated() {
                    if let vv = i as? StepView {
                        if index == self.current {
                            vv.bottomlabel.textColor = .black
                        }
                        else{
                            vv.bottomlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
                        }
                        
                    }
                }
            }
        }
        current += 1
        }
    }
    
    
    func confirmPassword(){
        step3.button.isEnabled = false
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        let pass1 = step3.newpassword.textfield.text ?? ""
        let pass2 = step3.confirmpassword.textfield.text ?? ""
        if pass1 == ""{
            hud.dismiss()
            let alert = UIAlertController(title: "請輸入新密碼", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            step3.button.isEnabled = true
            return
        }
        
        if pass2 == ""{
            hud.dismiss()
            let alert = UIAlertController(title: "請輸入確認密碼", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            step3.button.isEnabled = true
            return
        }
        if pass1 != pass2 {
            hud.dismiss()
            let alert = UIAlertController(title: "請確認密碼有輸入正確", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            step3.button.isEnabled = true
            return
        }
        
        if pass1.count < 6{
            hud.dismiss()
            let alert = UIAlertController(title: "請輸入大於6個字元的密碼", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            step3.button.isEnabled = true
            return
        }
        
        userInfo["password"] = pass1
        NetworkCall.shared.postCallform(parameter: "api-or/v1/password/updatePassword", dict: ["username":userInfo["username"] ?? "","token":userInfo["code"] ?? "","password": pass1], decoderType: ForgotCl.self) { (json) in
            DispatchQueue.main.async {

                if let json = json {
                    if json.data{
                        hud.textLabel.text = "密碼已經成功更改"
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        
                        hud.dismiss(afterDelay: 1, animated: true) {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else{
                        hud.dismiss()
                        self.step3.button.isEnabled = true
                        let alert = UIAlertController(title: "您輸入的驗證碼不正確", message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    hud.dismiss()
                    self.step3.button.isEnabled = true
                    let alert = UIAlertController(title: "請確認您有連上網路", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
        
        
    }
    
    
    @objc func gonext(){
        print(113)
        print(current)
        self.view.endEditing(true)
        if current == 0{
            getCode()
        }
        else if current == 1{
            confirmCode()
        }
        else if current == 2{
            confirmPassword()
        }
  
        
    }
    override func popview() {
        
        if current == 0 {
            self.dismiss(animated: true, completion: nil)
            
        }
        else{
                if current == 1{

                    step2anchor.leading?.constant = UIScreen.main.bounds.width
                    
                }
                else if current == 2{
                    
                    step3anchor.leading?.constant = UIScreen.main.bounds.width
                }
            let st = stackview.arrangedSubviews[current] as! StepView
            
            let st2 = stackview.arrangedSubviews[current-1] as! StepView
                selectionview.centerxconstraint.constant = (st.frame.width * CGFloat((current - 1)))/2
            
            selectionanchor.trailing?.constant = st2.frame.width * CGFloat((current - 1))
            self.backbutton.tintColor = .white
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
                if self.current == 1{
                    self.backbutton.image = #imageLiteral(resourceName: "ic_back")
                    //self.backbutton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
                }
                else{
                    self.backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
                    //self.backbutton.setImage(#imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate), for: .normal)
                }
            }) { (com) in
                
                if self.current == 0{
                    
                    self.toplabel.isHidden = false
                    self.button.isHidden = false
                    
                }
                else if self.current == 1{
                    self.step2.isHidden = false
                    
                }
                
                if com {
                    for (index,i) in self.stackview.arrangedSubviews.enumerated() {
                        if let vv = i as? StepView {
                            if index == self.current {
                                vv.bottomlabel.textColor = .black
                            }
                            else{
                                vv.bottomlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
                            }
                            
                        }
                    }
                }
            }
            current -= 1
        }
    }
}

class Step2View: UIView {
    let toplabel = AccountField()
    let button = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        button.setTitle("下一步".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = MajorColor().oceanColor
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        

        
        //button.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        
        addSubview(toplabel)
        toplabel.header.text = "已寄送驗證碼至".localized
        toplabel.textfield.attributedPlaceholder = NSAttributedString(string: "請輸入收到的驗證碼".localized, attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        //toplabel.centerInSuperview()
        toplabel.centerXInSuperview()
        toplabel.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 180.calcvaluey(), left: 0, bottom: 0, right: 0))
        toplabel.constrainWidth(constant: UIDevice().userInterfaceIdiom == .pad ? 570.calcvaluex() : UIScreen.main.bounds.width - 40.calcvaluex())
        toplabel.constrainHeight(constant: 97.calcvaluey())
        addSubview(button)
        //button.centerInSuperview()
        button.constrainHeight(constant: 48.calcvaluey())
        button.constrainWidth(constant: 124.calcvaluex())
        button.layer.cornerRadius = 48.calcvaluey()/2
        button.anchor(top: toplabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 0))
        button.centerXInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class Step3View : UIView {
    let newpassword = AccountField()
    let confirmpassword = AccountField()
    let button = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let stackview = UIStackView(arrangedSubviews: [newpassword,confirmpassword,button,UIView()])
        
        newpassword.header.text = "設定新密碼".localized
        newpassword.textfield.attributedPlaceholder =  NSAttributedString(string: "請輸入新的密碼".localized, attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        
        confirmpassword.header.text = "再次輸入新密碼".localized
        confirmpassword.textfield.attributedPlaceholder =  NSAttributedString(string: "請再次輸入新的密碼".localized, attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        
        newpassword.constrainWidth(constant: UIDevice().userInterfaceIdiom == .pad ? 570.calcvaluex() : UIScreen.main.bounds.width - 40.calcvaluex())
        newpassword.constrainHeight(constant: 97.calcvaluey())
        
        confirmpassword.constrainWidth(constant: UIDevice().userInterfaceIdiom == .pad ? 570.calcvaluex() : UIScreen.main.bounds.width - 40.calcvaluex())
        confirmpassword.constrainHeight(constant: 97.calcvaluey())
        stackview.axis = .vertical
        stackview.alignment = .center
        stackview.spacing = 24.calcvaluey()
        
        
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(66.calcvaluey(), after: stackview.arrangedSubviews[1])
        } else {
            // Fallback on earlier versions
        }
        button.setTitle("完成".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = MajorColor().oceanColor
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        button.constrainHeight(constant: 48.calcvaluey())
        button.constrainWidth(constant: 124.calcvaluex())
        button.layer.cornerRadius = 48.calcvaluey()/2
        
        
        addSubview(stackview)
        
        stackview.fillSuperview(padding: .init(top: 160.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
