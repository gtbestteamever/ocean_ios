//
//  OuterView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/1/7.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class OuterView: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath)
        cell.backgroundColor = .red
        return cell
    }
    
    var collectionview : UICollectionView!
    var topview = UIView()
    let bottomview = UIView()
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width * (1-0.33)) - 52 - 18
        return .init(width: (width - 26.xscalevalue()*5)/4.2, height: 74.yscalevalue())
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 16.xscalevalue()
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 26.xscalevalue(), bottom: 0, right: 26.xscalevalue())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 26.xscalevalue()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
        backgroundColor = .clear
        addSubview(topview)
        topview.addSubview(collectionview)
        collectionview.fillSuperview()
        topview.backgroundColor = .white
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 122.yscalevalue()))
        collectionview.delegate = self
        collectionview.backgroundColor = .clear
        collectionview.dataSource = self
        collectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellid")
        topview.layer.cornerRadius = 12
        topview.layer.addShadow()
        
        bottomview.backgroundColor = .white
        bottomview.layer.cornerRadius = 12
        bottomview.layer.addShadow()
        addSubview(bottomview)
        bottomview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 18.yscalevalue(), left: 0, bottom: -18.yscalevalue(), right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
