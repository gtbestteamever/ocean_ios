//
//  OuterView2Cell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/13.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class OuterView2Cell: UICollectionViewCell {
    let label = UILabel()
    let smallsep = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = false
        addSubview(label)
        label.centerInSuperview()
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.text = "多軸複合加工機"
        
        addSubview(smallsep)
        smallsep.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        smallsep.layer.cornerRadius = 3
        smallsep.centerXInSuperview()
        smallsep.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 60, height: 6))
        smallsep.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        smallsep.isHidden = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
