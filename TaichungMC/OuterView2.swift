//
//  OuterView2.swift
//  TaichungMC
//
//  Created by Wilson on 2020/1/9.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class OuterView2: UIView ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    let button = UIButton(type: .system)
    let selectionview = UIView()
    var selectioncollectionview : UICollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let topview = UIView()
        topview.isUserInteractionEnabled = true
        topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addSubview(topview)
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 86))
        button.setImage(#imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysTemplate), for: .normal)
        //button.image = #imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysTemplate)
        button.tintColor = .white
        topview.addSubview(button)
        layoutIfNeeded()
        button.isUserInteractionEnabled = true
        let buttonheight = topview.frame.height-UIApplication.shared.statusBarFrame.height-16
        button.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.xscalevalue(), bottom: 0, right: 0),size: .init(width: buttonheight, height: buttonheight))
        button.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
       // button.layer.zPosition = 2
        let collectionviewflowlayout = UICollectionViewFlowLayout()
        collectionviewflowlayout.scrollDirection = .horizontal
        selectioncollectionview = UICollectionView(frame: .zero, collectionViewLayout: collectionviewflowlayout)
        addSubview(selectionview)
        selectionview.backgroundColor = .white
        selectionview.layer.shadowColor = UIColor.black.cgColor
        selectionview.layer.shadowRadius = 2
        selectionview.layer.shadowOpacity = 0.3
        selectionview.layer.shadowOffset = .init(width: 0, height: 1)
        selectionview.layer.shouldRasterize = true
        selectionview.layer.rasterizationScale = UIScreen.main.scale
        selectionview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 80))
        
        
        addSubview(selectioncollectionview)
        selectioncollectionview.alwaysBounceHorizontal = true
        selectioncollectionview.delegate = self
        selectioncollectionview.dataSource = self
        selectioncollectionview.backgroundColor = .clear
        selectioncollectionview.register(OuterView2Cell.self, forCellWithReuseIdentifier: "cell")
        selectioncollectionview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 83))
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OuterView2Cell
        cell.backgroundColor = .clear
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OuterView2Cell
        cell.smallsep.isHidden = false
        cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (self.frame.width)/5, height: 83)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    @objc func showmenu(){
        print(728)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
