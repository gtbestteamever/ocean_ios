//
//  SampleController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SampleTitleView : UIView {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MajorColor().oceanColor
        layer.cornerRadius = 45.calcvaluey()/2
        self.constrainHeight(constant: 45.calcvaluey())
        
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_mark"))
        imageview.contentMode = .scaleToFill
        addSubview(imageview)
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 7.calcvaluex(), bottom: 0, right: 0),size: .init(width: 27.83.calcvaluex(), height: 34.29.calcvaluey()))
        
        label.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        label.textColor = .white
        addSubview(label)
        label.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 30.calcvaluex()))
        label.centerYInSuperview()
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SampleController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let topview = UIView()
    let backbutton = UIImageView(image: #imageLiteral(resourceName: "ic_back"))
    let titleview = SampleTitleView()
    //var titleviewanchor:AnchoredConstraints!
    var topviewanchor : AnchoredConstraints?
    var originCon : ViewController?
        let settingButton = UIButton(type: .custom)
    let background_imageview = UIImageView(image: #imageLiteral(resourceName: "background_content"))
    let horizontalStackview = Horizontal_Stackview(spacing:26.calcvaluex(),alignment: .center)
    //let titletextview = SampleTitleView()
    let background_view = UIImageView()
    let extrabutton_stackview = Horizontal_Stackview(spacing:19.calcvaluex(),alignment: .center)
    let slideinview = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        background_imageview.contentMode = .scaleAspectFill
        view.addSubview(background_imageview)
        background_imageview.fillSuperview()
        
        view.addSubview(background_view)
        //background_view.clipsToBounds = true
        background_view.contentMode = .scaleAspectFill
        background_view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: -100.calcvaluey(), left: 0, bottom: -100.calcvaluey(), right: 100.calcvaluex()),size: .init(width: 444.calcvaluex(), height: 0))
         topview.isUserInteractionEnabled = true
        //topview.backgroundColor = MajorColor().mainColor
        view.addSubview(topview)
        
        topviewanchor = topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: UIDevice().userInterfaceIdiom == .pad ? 86.calcvaluey() : 110.calcvaluey()))
        topview.addSubview(horizontalStackview)
        horizontalStackview.anchor(top: nil, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        
        view.addSubview(slideinview)
        slideinview.backgroundColor = MajorColor().oceanColor
        
        slideinview.anchor(top: view.topAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.size.height))
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        horizontalStackview.addArrangedSubview(backbutton)
        backbutton.constrainWidth(constant: 41.calcvaluex())
        backbutton.constrainHeight(constant: 41.calcvaluex())
        
        horizontalStackview.addArrangedSubview(titleview)

        extrabutton_stackview.addArrangedSubview(UIView())
        horizontalStackview.addArrangedSubview(extrabutton_stackview)
        horizontalStackview.addArrangedSubview(settingButton)
        settingButton.constrainWidth(constant: 48.calcvaluex())
        settingButton.constrainHeight(constant: 48.calcvaluex())
//        topview.addSubview(backbutton)
//        backbutton.anchor(top: nil, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 17.calcvaluey(), right: 0),size: .init(width: 41.calcvaluex(), height: 41.calcvaluey()))
        backbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        backbutton.isUserInteractionEnabled = true
        backbutton.isHidden = true
//        topview.addSubview(titleview)
//        //titleview.label.text = "常見問題"
//        titleview.label.textColor = .white
//        titleview.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
//        titleviewanchor = titleview.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 80.calcvaluex(), bottom: 17.calcvaluey(), right: 0))
//        titleview.centerYAnchor.constraint(equalTo: backbutton.centerYAnchor).isActive = true
        //titleview
        
        settingButton.setImage(#imageLiteral(resourceName: "ic_setting"), for: .normal)
        settingButton.contentHorizontalAlignment = .fill
        settingButton.contentVerticalAlignment = .fill
        
//        topview.addSubview(settingButton)
//        settingButton.isHidden = true
//        settingButton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 48.calcvaluex(), height: 48.calcvaluex()))
//        settingButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        
        settingButton.addTarget(self, action: #selector(goSetting), for: .touchUpInside)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeLang), name: Notification.Name(rawValue : "switchLanguage"), object: nil)
        (tabBarController as? MainTabBarController)?.menuview.isHidden = false
    }
    @objc func changeLang(){
        
        titleview.label.text = titleview.label.text?.localized
    }
    @objc func goSetting(){
        let con = SettingController()
        con.originCon = self.originCon
        con.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        con.modalPresentationStyle = .overFullScreen
        
        self.present(con, animated: false, completion: nil)
    }
    @objc func popview(){
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}
