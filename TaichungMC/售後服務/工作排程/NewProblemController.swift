//
//  NewProblemController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/24/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class NewProblemController : UIViewController,FormTextViewDelegate {
    func textViewReloadHeight(height: CGFloat) {
        self.view.layoutIfNeeded()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        //
    }
    
    let con = SampleContainerView()
    let container = UIView()
    let problemField = NormalTextView(text: "問題描述".localized, placeholdertext: "輸入問題描述(最多255字)".localized, color: #colorLiteral(red: 0.1751932472, green: 0.1751932472, blue: 0.1751932472, alpha: 1))
    let doneButton = ActionButton(width: 100.calcvaluex())
    var workDetail : WorkDetail?
    weak var pre_con : WorkDetailController? {
        didSet{
            workDetail = pre_con?.workDetail
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(con)
        con.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 0))
        con.label.text = "派工問題描述".localized
        
        con.addSubview(container)
        container.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: con.bottomAnchor, trailing: con.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: 24.calcvaluey(), right: 24.calcvaluex()))
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
     //   let scrollview = UIScrollView()
//        scrollview.showsVerticalScrollIndicator = false
//        scrollview.addSubview(problemField)
//        problemField.fillSuperview()
//        problemField.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
//        problemField.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true

        problemField.textfield.mode = .ScheduleProblem
        problemField.textfield.t_delegate = self
        problemField.makeImportant()
        doneButton.backgroundColor = MajorColor().oceanColor
        doneButton.setTitle("送出".localized, for: .normal)
        doneButton.setTitleColor(.white, for: .normal)
        
        container.addSubview(doneButton)
        doneButton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil)
        doneButton.centerXInSuperview()
        
        container.addSubview(problemField)
        problemField.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: doneButton.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 18.calcvaluey(), right: 0))
        problemField.textfield.initialHeight = 150.calcvaluey()
        problemField.textfield.anchorHeight?.constant = 150.calcvaluey()
        
        doneButton.addTarget(self, action: #selector(goSend), for: .touchUpInside)
        
    }
    @objc func goSend(){
        if problemField.textfield.text == ""{
            let alert = UIAlertController(title: "請輸入問題描述", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler:  nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        let alert = UIAlertController(title: "確認派工有問題？".localized, message: nil, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "確定".localized, style: .default) { (_) in
            var param = [String:Any]()
            var workers_id = [String]()
            
            for i in self.workDetail?.service_personnels ?? [] {
                workers_id.append(i.id)
            }
            param["service_personnels"] = workers_id
            param["status"] = "cancelled"
            param["reason"] = self.problemField.textfield.text
            param["upkeep_id"] = self.workDetail?.upkeep?.id ?? ""
            param["scheduled"] = self.workDetail?.scheduled ?? ""
            param["description"] = self.workDetail?.description ?? ""
            let hud = JGProgressHUD()
            hud.show(in: self.container)
            NetworkCall.shared.postCallAny(parameter: "api-or/v1/upkeep-tasks/\(self.workDetail?.id ?? "")", param: param, decoderType: UpdatedWorkDetail.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss(afterDelay: 1, animated: true) {
                        self.pre_con?.workDetail = json?.data
                        print(995,json?.data)
                        self.dismiss(animated: false, completion: nil)
                    }
//                    if let json = json?.data {
//                        self.workDetail = json
//                    }
                }

            }
        }
        
        let action2 = UIAlertAction(title: "取消".localized, style: .cancel, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
        
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
