//
//  CalendarController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/13/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarTimeController : UIViewController,FSCalendarDelegate,FSCalendarDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let calendar = calendar , touch.view?.isDescendant(of: calendar) == true || touch.view?.isDescendant(of: container.xbutton) == true || touch.view?.isDescendant(of: confirm_button) == true {
            return false
        }
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT+8")
        formatter.dateFormat = "HH:mm:ss"
        timeString = formatter.string(from: timeField.datepicker.date)
        timeField.textfield.text = timeField.datepicker.date.convertTimeToChinese()
        
       // print(331,formatter.string(from: timeField.datepicker.date))
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT+8")
        formatter.dateFormat = "yyyy-MM-dd"
        dateString = formatter.string(from: date)
       // print(formatter.string(from: date))
    }
    
    let container = SampleContainerView()
    var calendar : FSCalendar?
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
    weak var con : ScheduleWorkController?
    weak var con2 : EditScheduleController?
    var data : WorkDetail?
    var selected_date : Date?
    var confirm_button = UIButton(type: .custom)
    let timeField = InterviewDateFormView(text: "時間".localized, placeholdertext: "請選擇時間".localized)
    var dateString = ""
    var timeString = ""
    weak var con_view : ScheduleSecondStep?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(container)
        container.label.text = "維修時間".localized

        container.centerInSuperview(size: .init(width: 620.calcvaluex(), height: 550.calcvaluey()))
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)

        confirm_button.backgroundColor = MajorColor().oceanSubColor
    
        confirm_button.setTitle("確定".localized, for: .normal)
        confirm_button.setTitleColor(.white, for: .normal)
        confirm_button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        

        confirm_button.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))

        
        confirm_button.layer.cornerRadius = 48.calcvaluey()/2
        
        container.addSubview(confirm_button)
        confirm_button.constrainWidth(constant: 124.calcvaluex())
        confirm_button.constrainHeight(constant: 48.calcvaluey())
        confirm_button.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 24.calcvaluey(), right: 0))
        confirm_button.addTarget(self, action: #selector(confirmTimeDate), for: .touchUpInside)
        confirm_button.centerXInSuperview()
        container.addSubview(timeField)
        timeField.tlabel.textColor = .black
        timeField.anchor(top: nil, leading: container.leadingAnchor, bottom: confirm_button.topAnchor, trailing: container.centerXAnchor,padding: .init(top: 0, left: 48.calcvaluex(), bottom: 16.calcvaluey(), right: 110.calcvaluex()),size: .init(width: 0, height: 92.calcvaluey()))
        timeField.datepicker.datePickerMode = .time
        timeField.format = "HH:mm:ss"
        //timeField.datepicker.minimumDate = Date()
        timeField.textfield.delegate = self
        calendar = FSCalendar()
        calendar?.placeholderType = .none
        calendar?.dataSource = self
        calendar?.delegate = self
        container.addSubview(calendar!)
        //calendar?.minimumDate =
        calendar?.select(self.selected_date)
        calendar?.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: timeField.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 16.calcvaluex(), bottom: 16.calcvaluey(), right: 16.calcvaluex()))
        calendar?.locale = Locale(identifier: UserDefaults.standard.getConvertedLanguage())
        calendar?.appearance.todayColor = .white
        calendar?.appearance.titleTodayColor = .black
        //calendar?.appearance.headerDateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM", options: 0, locale: Locale(identifier: "zh-TW"))
        calendar?.appearance.titleFont = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        calendar?.appearance.headerTitleFont = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        for (index,item) in (calendar?.calendarWeekdayView.weekdayLabels ?? []).enumerated() {
            item.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
            switch index {
            case 0:
                item.text = "週日".localized
            case 1:
                item.text = "週一".localized
            case 2:
                item.text = "週二".localized
            case 3:
                item.text = "週三".localized
            case 4:
                item.text = "週四".localized
            case 5:
                item.text = "週五".localized
            case 6:
                item.text = "週六".localized
            default: ()
            }
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEdit))
        tap.delegate = self
        container.addGestureRecognizer(tap)
    }
    @objc func endEdit(){
        self.view.endEditing(true)
    }
    func createAlert(message:String)
    {
        let con = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
        con.addAction(action)
        self.present(con, animated: true, completion: nil)
    }
    @objc func confirmTimeDate() {
        self.view.endEditing(true)
        if dateString == ""{
            createAlert(message: "請選擇日期".localized)
            return
        }
        if timeString == ""{
            createAlert(message: "請選擇時間".localized)
            return
        }
        
        //print("\(dateString) \(timeString)")
        data?.scheduled = "\(dateString) \(timeString)"
        con2?.data = data
        con_view?.date_array.append("\(dateString) \(timeString)")
        
        self.dismiss(animated: false, completion: nil)

    }
}

