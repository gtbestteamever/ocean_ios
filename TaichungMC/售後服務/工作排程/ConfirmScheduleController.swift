//
//  ConfirmScheduleController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/16/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class confirmTableViewCell : UITableViewCell {
    let companyLabel = confirmLabel(headerText: "報修公司".localized)
    let problemLabel = confirmLabel(headerText: "報修問題".localized)
    let locationLabel = confirmLabel(headerText: "地點".localized)
    let memberLabel = confirmLabel(headerText: "派工人員".localized)
    let fixTimeLabel = confirmLabel(headerText: "維修時間".localized)
    let descriptionLabel = confirmLabel(headerText: "維修內容".localized)
    func setPreLabel(set:Bool) {
        companyLabel.isHidden = set
        problemLabel.isHidden = set
        locationLabel.isHidden = set
        memberLabel.isHidden = set

    }
    var stackview : UIStackView?
    let seperator = UIView()
    var alreadySet = false
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: nil, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0),size: .init(width: 0, height: 1.calcvaluey()))
        seperator.backgroundColor = #colorLiteral(red: 0.8696024418, green: 0.8697279096, blue: 0.869574964, alpha: 1)
        stackview = UIStackView(arrangedSubviews: [companyLabel,problemLabel,locationLabel,memberLabel,fixTimeLabel,descriptionLabel])
        setPreLabel(set: true)
        stackview?.spacing = 12.calcvaluey()
        stackview?.axis = .vertical
        stackview?.alignment = .fill
        
        contentView.addSubview(stackview!)
        stackview?.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: seperator.topAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0))
        if let st = stackview?.arrangedSubviews.first as? confirmLabel {
            seperator.leadingAnchor.constraint(equalTo: st.valueLabel.leadingAnchor).isActive = true
        }
        
    }
    func setFixData(data:FixModel){
        setPreLabel(set: false)
        companyLabel.valueLabel.text = data.account?.name
        problemLabel.valueLabel.text = data.description
        locationLabel.valueLabel.text = data.appliance?.address
        if let user = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()), let json = try? JSONDecoder().decode(UserData.self, from: user) {
            memberLabel.valueLabel.text = json.data.name
        }
        
       
    }
    func setEmployeeData(work:NewWork) {
        fixTimeLabel.valueLabel.text = Date().convertToDateComponent(text: work.time,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        descriptionLabel.valueLabel.text = work.description
        
        var selected_worker = [ScheduleEmployee]()
        
        for i in work.worker {
            for j in i.workers {
                if j.isSelected == true && !selected_worker.contains(where: { (emp) -> Bool in
                    return emp.id == j.id
                }){
                    selected_worker.append(j)
                }
            }
            
        }
        for i in work.history {
            for j in i.workers {
                if j.isSelected == true && !selected_worker.contains(where: { (emp) -> Bool in
                    return emp.id == j.id
                }){
                    selected_worker.append(j)
                }
            }
        }
        if !alreadySet {
        for (index,i) in selected_worker.enumerated() {
            let worker_label = confirmLabel(headerText: "")
            if index == 0{
                worker_label.headerLabel.text = "\("維修人員".localized)："
            }
            else{
                worker_label.headerLabel.text = ""
            }
            worker_label.valueLabel.text = "\(i.service_groups.first?.titles?.getLang() ?? "") - \(i.name ?? "")"
            stackview?.addArrangedSubview(worker_label)
        }
        }
        alreadySet = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class confirmLabel : UIView {
    var headerLabel : UILabel = {
        let hL = UILabel()
        hL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        hL.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        hL.textAlignment = .right
        return hL
    }()
    
    var valueLabel : UILabel = {
        let hL = UILabel()
        hL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        hL.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        hL.numberOfLines = 0
        return hL
    }()
    init(headerText:String) {
        super.init(frame: .zero)
        headerLabel.text = "\(headerText)："
        let stackview = UIStackView(arrangedSubviews: [headerLabel,valueLabel])
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            headerLabel.constrainWidth(constant: 150.calcvaluex())
        }
        else{
            headerLabel.constrainWidth(constant: 140.calcvaluex())
        }
        
        
        stackview.alignment = .top
        stackview.spacing = 20.calcvaluex()
        
        addSubview(stackview)
        stackview.fillSuperview()
        
//        addSubview(headerLabel)
//        headerLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 140.calcvaluex(), height: 0))
//        addSubview(valueLabel)
//        valueLabel.anchor(top: topAnchor, leading: headerLabel.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ConfirmScheduleController : UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return works.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "confirm", for: indexPath) as! confirmTableViewCell
        if let data = fixModel , indexPath.row == 0{
            cell.setFixData(data: data)
        }
        
        cell.setEmployeeData(work: works[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    let con = SampleContainerView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    let confirmButton = UIButton(type: .custom)
    var fixModel : FixModel?
    var works = [NewWork]()
    var isPreview = false
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(con)
        if isPreview {
            con.label.text = "預覽派工資訊".localized
        }
        else{
            con.label.text = "確認派工資訊".localized
        }
        con.centerInSuperview(size: .init(width: 704.calcvaluex(), height: 600.calcvaluey()))
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
        confirmButton.setTitle("派工".localized, for: .normal)
        confirmButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.backgroundColor = MajorColor().oceanSubColor
        
        con.addSubview(confirmButton)
        confirmButton.anchor(top: nil, leading: nil, bottom: con.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 22.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        confirmButton.centerXInSuperview()
        confirmButton.layer.cornerRadius = 48.calcvaluey()/2
        if isPreview {
            confirmButton.isHidden = true
        }
        else{
            confirmButton.isHidden = false
        }
        con.addSubview(tableview)
        tableview.register(confirmTableViewCell.self, forCellReuseIdentifier: "confirm")
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: confirmButton.topAnchor, trailing: con.trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 64.calcvaluex(), bottom: 6.calcvaluey(), right: 64.calcvaluex()))
        tableview.showsVerticalScrollIndicator = false
        confirmButton.addTarget(self, action: #selector(sendConfirmation), for: .touchUpInside)
    }
    @objc func sendConfirmation(){
        var param = [[String:Any]]()
        for i in works {
            var n_param = [String:Any]()
            n_param["description"] = i.description
            n_param["scheduled"] = i.time
            n_param["upkeep_id"] = i.work_id
            
            var array = [String]()
            for j in i.worker {
                for k in j.workers {
                    if k.isSelected == true && !array.contains(k.id) {
                        array.append(k.id)
                    }
                }
            }
            n_param["service_personnels"] = array
            
            param.append(n_param)
        }
        print(321,param)
        let hud = JGProgressHUD()
        hud.textLabel.text = "建立派工中...".localized
        hud.show(in: self.con)
        NetworkCall.shared.postCallAny(parameter: "api-or/v1/upkeep-tasks", param: param, decoderType: WorkDetailModel.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    hud.textLabel.text = "建立成功".localized
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.dismiss(afterDelay: 1.5, animated: true) {
                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                    }
                    
                }
                else{
                    hud.textLabel.text = "建立失敗".localized
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.dismiss(afterDelay: 1.5, animated: true) {
                        
                    }
                }
            }

        }
        
        
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
