//
//  ServiceForm.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
import UIKit

struct SurveyModelData : Codable {
    var data : SurveyData
}
enum MyValue: Codable {
    case string(String)
    case innerItem([String])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode([String].self) {
            self = .innerItem(x)
            return
        }
        throw DecodingError.typeMismatch(MyValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .innerItem(let x):
            try container.encode(x)
        }
    }
}
struct SurveyData : Codable {
    var files : MyValue?
    var professional_radio : Int
    var polite_radio : Int
    var efficiency_radio : Int
    var arrange_radio : Int
    
    var description : String?
    var id : String
}
struct ServiceFormHistroyData : Codable{
    var data : [ServiceFormList]
}
struct ServiceFormList : Codable{
    var id : String
    var owner : WorkOwner?
    var status : String?
    var created_at : String?
}
struct ServiceData : Codable{
    var data : ServiceModel
}
struct ServiceModel : Codable {
    var is_maintenance : Int?
    var id : String
    var status : String?
    var upkeep_id : String?
    var account_code : String?
    var account_name : String?
    var account_contact : String?
    var account_tel : String?
    var account_address : String?
    var appliance_code : String?
    var appliance_name : String?
    var appliance_warranty : String?
    var appliance_description : String?
    var appliance_reason : String?
    var appliance_countermeasure : String?
    var files : [ServiceFile]?
    
    var times : [ServiceModelTime]?
    var components : [ServiceModelComponenet]?
    var costs : [ServiceModelExtraCost]?
    var discount : String?
    var total_price : String?
    var pay_method : String?
    var pay_remark : String?
    var engineers : [ServiceModelEngineer]?
    var grand_total : String?
    var created_at : String?
    var updated_at : String?
    var satisfaction : SurveyData?
    var owner : WorkOwner?
    var export_file : InterviewPreviewFile?
    var repair_contents : String?
}

struct ServiceModelEngineer : Codable{
    var id : String
    var code : String?
    var name : String?
    var service_points : String?
    var report_number : String?
    var warranty_date : String?
    var delivery : String?
    var workpiece_timing : String?
    var programming : String?
    var artifact : String?
    var project : String?
    var other : String?
    var staff_sign: String?
}
struct ServiceModelExtraCost : Codable {
    var price : InterviewStatusEnum?
    var rate : String?
}
struct ServiceModelComponenet : Codable {
    var id : String
    var status : String?
    var accessory_code : String?
    var fault_code : String?
    var description : String?
    var price : Int?
    var quantity : Int?
    var is_free : Int?
    var subtotal : Int?
    var owner : String?
}
struct ServiceModelTime : Codable{
    var id : String
    var start_time : String?
    var end_time : String?
    var hours : String?
    var files : [InterviewFile]?
    var round_trip_distance : String?
    
}

struct ServiceFile : Codable {
    var id : String?
    var path : String?
    var path_url : String?
}

struct ArrayString : Codable {
    var items : [String]
}
class ServiceForm {
    var selectOrInput : Int = 0
    var isMaintenance: Bool = false
    var id : String
    var didLoadMore : Bool
    var upkeep_id : String
    var status : String
    var customer : FormCustomer
    var machine : FormMachine
    var transporationCost : Int
    var workHoursCost : Int
    var components : [FormComponent]
    var priceGroup : FormPriceGroup
    var timeAndDuration : [FormTime]
    var collectAndOther : FormCollect
    var engineers : [FormEngineer]
    var attachment : [FormAttachment]
    var del_components : [String]
    var del_files : [String]
    var del_engineers : [String]
    init(){
        isMaintenance = false
        id = ""
        didLoadMore = false
        upkeep_id = ""
        status = ""
        customer = FormCustomer()
        machine = FormMachine()
        components = []
        priceGroup = FormPriceGroup()
        timeAndDuration = []
        collectAndOther = FormCollect()
        engineers = []
        attachment = []
        del_components = []
        del_files = []
        del_engineers = []
        transporationCost = 0
        workHoursCost = 0
        selectOrInput = 0
    }
    func setData(form_data:ServiceModel,workDetail:WorkDetail? = nil,mode:ServiceMode) {
        isMaintenance = form_data.is_maintenance == 1 ? true : false
        id = form_data.id
        upkeep_id = form_data.upkeep_id ?? ""
        status = "processing"
        let customer = FormCustomer()
        customer.id = form_data.account_code ?? ""
        customer.name = form_data.account_name ?? ""
        customer.phone = form_data.account_tel ?? ""
        customer.address = form_data.account_address ?? ""
        customer.contact = form_data.account_contact ?? ""
        self.customer = customer
        var n_time = [FormTime]()
        for i in form_data.times ?? []{
            let date = FormTime()
            date.id = i.id
            date.startTime = i.start_time ?? ""
            date.endTime = i.end_time ?? ""
            date.duration = i.hours ?? ""
            date.distance  = i.round_trip_distance ?? ""
            let signiture = FormSigniture()
            signiture.id = i.files?.first?.path_url
           // signiture.path = i.files?.first?.path
            signiture.path_url = i.files?.first?.path_url
            date.signiture = signiture
            n_time.append(date)
        }
        if mode != .Review && mode != .Edit {
        let tt_time = FormTime()
            print(66172,workDetail?.worked_at)
        tt_time.startTime = workDetail?.worked_at ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let start = dateFormatter.date(from: tt_time.startTime) ?? Date()
        let end = dateFormatter.date(from: tt_time.endTime) ?? Date()
        tt_time.duration = String(format: "%.1f",Double(end.minutes(from: start))/60.0)
        n_time.append(tt_time)
        
        }
        timeAndDuration = n_time
        let priceGroup = FormPriceGroup()
        var extra = [FormExtraWork]()
        
        for i in form_data.costs ?? [] {
            let ext = FormExtraWork()
            if let dt = i.price {
                switch dt {
                case .int(let x):
                    ext.price = String(x)
                case .string(let y):
                    ext.price = y

                }
            }
            
            ext.multiplier = i.rate ?? ""
            
            extra.append(ext)
        }
        if mode != .Review && mode != .Edit {
            let ext = FormExtraWork()
            ext.multiplier = "1.0"
            extra.append(ext)
        }
        priceGroup.extraGroup = extra
        priceGroup.discount = form_data.discount ?? ""
        priceGroup.total = form_data.total_price ?? ""
        priceGroup.total_component_price = form_data.grand_total ?? ""
        self.priceGroup = priceGroup
        
        var n_components = [FormComponent]()
        
        for i in form_data.components ?? [] {
            let cc = FormComponent()
            cc.id = i.id
            cc.component_code = i.accessory_code ?? ""
            cc.error_code = i.fault_code ?? ""
            cc.payment_status = i.is_free
            cc.price = "\(i.price ?? 0)"
            cc.quantity = "\(i.quantity ?? 0)"
            cc.reason = i.description ?? ""
            cc.status = i.status ?? ""
            cc.owner = i.owner ?? ""
            if let qt = cc.quantity.integerValue, let pr = cc.price.integerValue {
                cc.total_price = String(qt*pr)
            }
            
            n_components.append(cc)
            
        }
        self.components = n_components
        
        
        collectAndOther.status = form_data.pay_method
        collectAndOther.moreInfo = form_data.pay_remark ?? ""
        
        var n_engineers = [FormEngineer]()
        
        for i in form_data.engineers ?? [] {
            let eng = FormEngineer()
            eng.pre_id = i.id
            eng.id = i.code ?? ""
            eng.name = i.name ?? ""
            eng.point = i.service_points ?? ""
            eng.errorNo = i.report_number ?? ""
            eng.car = i.delivery ?? ""
            eng.design = i.programming ?? ""
            eng.insurance = i.warranty_date ?? ""
            eng.other = i.other ?? ""
            eng.special = i.project ?? ""
            eng.test = i.artifact ?? ""
            eng.duration = i.workpiece_timing ?? ""
            eng.signiture = FormSigniture()
            eng.signiture.path_url = i.staff_sign
            n_engineers.append(eng)
        }
        if mode != .Review && mode != .Edit {
            for i in workDetail?.service_personnels ?? [] {
                let eng = FormEngineer()
                
                eng.id = i.id
                eng.name = i.name ?? ""
                n_engineers.append(eng)
            }

            
        }
        self.engineers = n_engineers
        
        if let _ = workDetail?.upkeep?.appliance {
            selectOrInput = 0
        }
        if let name = workDetail?.upkeep?.appliance_name, !name.isEmpty {
            selectOrInput = 1
        }
        let machine = FormMachine()
        machine.code = form_data.appliance_code ?? ""
        machine.type = form_data.appliance_name ?? ""
        machine.maintainStatus = form_data.appliance_warranty ?? ""
        machine.description = form_data.appliance_description ?? ""
        machine.initial_reason = form_data.appliance_reason ?? ""
        machine.method = form_data.appliance_countermeasure ?? ""
        
        self.machine = machine
        
        
        var files = [FormAttachment]()
        for i in form_data.files ?? [] {
            let ff = FormAttachment()
            ff.id = i.id
            ff.path = i.path ?? ""
            ff.path_url = i.path_url ?? ""
            files.append(ff)
        }
        self.attachment = files
        


       
        let trimmedString = form_data.repair_contents?.trimmingCharacters(in: .init(charactersIn: "[]\""))

        // Split the string by commas to get individual string elements
        let arrayOfStrings = trimmedString?.components(separatedBy: "\",\"") ?? []
            
            
            print(7761,arrayOfStrings.count)
            
            var checkMode = [CheckMode]()
            for i in arrayOfStrings {
                if let mode = CheckMode(rawValue: i) {
                    checkMode.append(mode)
                }
            }
            
            machine.selection = checkMode
            

        
    }
    func checkValid() -> (Bool,Int) {
       
        
        if customer.contact == "" {
            return (false,0)
            
        }
        if customer.phone == "" {
            return (false,0)
        }
        
        if customer.address == ""{
            return (false,0)
        }
        
        if machine.description == ""{
            return (false,1)
        }
        if machine.initial_reason == ""{
            return (false,1)
        }
        if machine.method == ""{
            return (false,1)
        }
        if machine.selection.isEmpty {
            return (false,1)
        }
        for i in engineers {
            if i.id == "" || i.name == "" || i.signiture.path == nil || i.signiture.path == "" {
                
                
                return (false,6)
                
            }
        }
        
        
        return (true,0)
    }
    func getParam() -> [String:Any] {
        var newParam = [String:Any]()
        
        newParam["status"] = status
        if !isMaintenance {
            newParam["upkeep_id"] = upkeep_id
        }
        else{
            newParam["upkeep_id"] = ""
        }
        
        newParam["account_code"] = customer.id
        newParam["account_contact"] = customer.contact
        newParam["account_tel"] = customer.phone
        newParam["account_address"] = customer.address
        newParam["appliance_code"] = machine.code
        newParam["appliance_name"] = machine.type
        // newParam["appliance_warranty"]
        newParam["appliance_description"] = machine.description
        newParam["appliance_reason"] = machine.initial_reason
        newParam["appliance_countermeasure"] = machine.method
        newParam["appliance_warranty"] = machine.maintainStatus
        newParam["is_maintenance"] = isMaintenance
        newParam["account_name"] = customer.name
        var repairContents = [String]()
        for i in machine.selection {
            repairContents.append(i.rawValue)
        }
        newParam["repair_contents"] = repairContents
        var duration = [[String:Any]]()
        for i in timeAndDuration {
            var single = [String:Any]()
            if let id = i.id {
                single["id"] = id
            }
            single["start_time"] = i.startTime
            single["end_time"] = i.endTime
            single["hours"] = i.duration
            single["round_trip_distance"] = i.distance
            var file = [String:Any]()
            if let id = i.signiture.id {
                file["id"] = id
            }
            if i.signiture.path != ""{
                file["path"] = i.signiture.path
            }
            
            if i.signiture.path_url != ""{
                file["path_url"] = i.signiture.path_url
            }
            single["files"] = [file]
            if !del_files.isEmpty {
                single["del_files"] = i.del_files
            }
            
            duration.append(single)
        }
        newParam["transportation_cost"] = "\(transporationCost)"
        newParam["work_hours_cost"] = "\(workHoursCost)"
        
        newParam["times"] = duration
        
        var components_dict = [[String:Any]]()
        
        for i in components {
            var com = [String:Any]()
            if let id = i.id {
                com["id"] = id
            }
            com["accessory_code"] = i.component_code
            com["fault_code"] = i.error_code
            com["description"] = i.reason
            if let price = i.price.integerValue {
                com["price"] = price
            }
            if let quantity = i.quantity.integerValue {
                com["quantity"] = quantity
            }
           
            if let pay_status = i.payment_status {
                com["is_free"] = "\(pay_status)"
            }
            if let total_price = i.total_price.integerValue {
                com["subtotal"] = total_price
            }
            
            com["status"] = i.status
            components_dict.append(com)
        }
        newParam["components"] = components_dict
        
        var costs = [[String:Any]]()
        for i in priceGroup.extraGroup {
            var n_cost = [String:Any]()
            n_cost["price"] = i.price
            n_cost["rate"] = i.multiplier
            
            costs.append(n_cost)
        }
        if let grand = priceGroup.total_component_price.doubleValue {
            let grandTotal = (Int(grand)) + workHoursCost + transporationCost
            newParam["grand_total"] = "\(grandTotal)"
        }
        
        newParam["costs"] = costs
        print(3321,priceGroup.discount,priceGroup.total)
        newParam["discount"] = priceGroup.discount
        //newParam["total_price"] = priceGroup.total
        if let status = collectAndOther.status {
            newParam["pay_method"] = status
        }
        newParam["pay_remark"] = collectAndOther.moreInfo
        
        var eng_arr = [[String:Any]]()
        for i in engineers {
            
            var eng = [String:Any]()
            if let id = i.pre_id {
                eng["id"] = id
            }
            eng["code"] = i.id
            eng["name"] = i.name
            //            eng["service_points"] = i.point
            //            eng["report_number"] = i.errorNo
            //            eng["warranty_date"] = i.insurance
            //            eng["delivery"] = i.car
            //            eng["workpiece_timing"] = i.duration
            //            eng["programming"] = i.design
            //            eng["artifact"] = i.test
            //            eng["project"] = i.special
            //            eng["other"] = i.other
            eng["staff_sign"] = i.signiture.path
            //            var file = [String:Any]()
            //            if let id = i.signiture.id {
            //                file["id"] = id
            //            }
            //            if i.signiture.path != ""{
            //                file["path"] = i.signiture.path
            //            }
            //
            //            if i.signiture.path_url != ""{
            //                file["path_url"] = i.signiture.path_url
            //            }
            //            single["files"] = [file]
            //
            //            single["del_files"] = i.del_files
            eng_arr.append(eng)
        }
        newParam["engineers"] = eng_arr
        var files = [[String:Any]]()
        for i in attachment {
            var ff = [String:Any]()
            ff["path"] = i.path
            files.append(ff)
        }
        newParam["files"] = files
        
        if !del_files.isEmpty {
            newParam["del_files"] = self.del_files
        }
        if !del_components.isEmpty {
            newParam["del_components"] = self.del_components
        }
        if !del_engineers.isEmpty {
            newParam["del_engineers"] = self.del_engineers
        }
        
        return newParam
        
    }
}
class FormAttachment{
    var id : String?
    var path : String
    var path_url : String
    var image : UIImage?
    init()
    {
        id = nil
        image = nil
        path = ""
        path_url = ""
        
    }
}
class FormEngineer {
    var pre_id : String?
    var id : String
    var name : String
    var errorNo : String
    var point : String
    var insurance : String
    var car : String
    var duration : String
    var design : String
    var test : String
    var special : String
    var other : String
    var signiture : FormSigniture
    var del_files : [String]
    init(){
        pre_id = nil
        id = ""
        name = ""
        errorNo = ""
        point = ""
        insurance = ""
        car = ""
        duration = ""
        design = ""
        test = ""
        special = ""
        other = ""
        signiture = FormSigniture()
        del_files = []
    }
}
class FormCollect {
    var status : String?
    var moreInfo : String
    init(){
        status = nil
        moreInfo = ""
    }
}
class FormSigniture {
    var id : String?
    var signiture : UIImage?
    var path : String?
    var path_url : String?
    init(){
        id = nil
        signiture = nil
        path = nil
        path_url = nil
    }
}
class FormTime {
    var id : String?
    var startTime : String
    var endTime : String
    
    var duration : String
    var distance : String
    var signiture : FormSigniture
    var del_files : [String]
    init(){
        id = nil
        startTime = ""
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        endTime = formatter.string(from: currentDate)
        //endTime = Date().convertToDateComponent(text)
        duration = "0"
        signiture = FormSigniture()
        del_files = []
        distance = "0"
        
    }
}
class FormPriceGroup {
    var extraGroup : [FormExtraWork]
    var total_component_price : String
    var discount : String
    var total : String
    
    init(){
        extraGroup = []
        total_component_price = "0"
        discount = "0"
        total = "0"
    }
}
class FormExtraWork{
    var id : String?
    var price : String
    var multiplier : String
    
    init(){
        id = nil
        price = "0"
        multiplier = "1.0"
    }
}
class FormCustomer {
    var id : String
    var name : String
    var contact : String
    var phone : String
    var address : String
    
    init(){
        id = ""
        name = ""
        contact = ""
        phone = ""
        address = ""
    }
}

class FormMachine {
    var code : String
    var type : String
    var maintainStatus : String
    var description : String
    var initial_reason : String
    var method : String
    var selection : [CheckMode]
    
    init(){
        code = ""
        type = ""
        maintainStatus = ""
        description = ""
        initial_reason = ""
        method = ""
        selection = []
    }
}

class FormComponent {
    var id : String?
    var status : String?
    var error_code : String
    var component_code : String
    var quantity : String
    var payment_status : Int?
    var total_price : String
    var price : String
    var reason : String
    var owner : String
    init(){
        id = nil
        status = nil
        error_code = ""
        component_code = ""
        quantity = ""
        payment_status = nil
        total_price = ""
        reason = ""
       
        price = ""
        owner = ""
    }
    
}
