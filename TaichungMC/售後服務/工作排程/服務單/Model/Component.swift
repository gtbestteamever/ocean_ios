//
//  Component.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct ComponentData : Codable{
    var data : [Component]
}

struct Component : Codable {
    var id : String
    var code : String?

    var description : String?
    var price : String?
}
