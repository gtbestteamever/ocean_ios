//
//  ComponentError.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation


struct ComponentErrorData : Codable {
    var data : [ComponentError]
}
struct ComponentError : Codable{
    var id : String
    var code : String?
    var level : String?
    var description : String?
}
