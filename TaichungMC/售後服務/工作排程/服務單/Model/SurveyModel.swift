//
//  SurveyModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

struct SurveyModel : Codable{
    var data : [Survey_Data]
}
struct Survey_Data : Codable{
    var title : String?
    var type : String?
    var required : Int?
    var order : Int?
    var value : String?
}
