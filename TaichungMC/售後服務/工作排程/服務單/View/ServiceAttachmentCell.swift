//
//  ServiceAttachmentCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
import SDWebImage
class AttachmentView : UIView {
    var mode : ServiceMode?
    let imageview = UIImageView()
    let xbutton = UIButton(type: .custom)
    func setData(data:ServiceForm) {
        self.data = data
        if mode == .Review{
            xbutton.isHidden = true
        }
        else{
            xbutton.isHidden = false
        }
        let attachment = data.attachment[self.tag]
        if let image = attachment.image {
            
            self.imageview.image = image
        }
        
        else if let url = URL(string: attachment.path_url) {
           // let transformer = SDWebImag
           // self.imageview.sd_setImage(with: url, completed: nil)
//            let transformer = SDImageResizingTransformer(size: .init(width: 140.calcvaluex(), height: 100.calcvaluey()), scaleMode: .fill)
            DispatchQueue.global(qos: .userInitiated).async {
                self.imageview.sd_setImage(with: url, placeholderImage: nil, options: .scaleDownLargeImages, context: [:])
            }
            
//                url.getThumnailImageCom { (image) in
//
//
//
//                        self.imageview.image = image
//
//
//
//
//
//
//                }
            
            //imageview.image = url.getThumnailImage()
        }
        
    }
    var data : ServiceForm?
    var con : ServiceAttachmentCell?
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.contentMode = .scaleAspectFit
        imageview.layer.borderWidth = 1.calcvaluex()
        imageview.layer.borderColor = #colorLiteral(red: 0.8743426643, green: 0.8743426643, blue: 0.8743426643, alpha: 1)
        constrainWidth(constant: 140.calcvaluex())
        constrainHeight(constant: 100.calcvaluey())
        xbutton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        addSubview(imageview)
        imageview.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 12.calcvaluex()))
        addSubview(xbutton)
        
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        xbutton.centerYAnchor.constraint(equalTo: imageview.topAnchor).isActive = true
        xbutton.centerXAnchor.constraint(equalTo: imageview.trailingAnchor).isActive = true
        xbutton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
        
        imageview.isUserInteractionEnabled = true
        imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expandFile)))
    }
    @objc func expandFile(){
        
        let vd = ExpandFileController()
        
        vd.modalPresentationStyle = .overCurrentContext
        if let file = URL(string: data?.attachment[self.tag].path_url ?? "") {
            file.loadWebview(webview: vd.imgv)
        }
        else if let imgv = data?.attachment[self.tag].image {
            imgv.loadWebview(webview: vd.imgv)
        }
        
        General().getTopVc()?.present(vd, animated: false, completion: nil)
        
        
    }
    @objc func removeItem() {
        let dt = data?.attachment.remove(at: self.tag)
        if let id = dt?.id {
            data?.del_files.append(id)
        }
        if let con = con{
            UIView.setAnimationsEnabled(false)
            con.tableView?.reloadSections(IndexSet(integer: con.tag), with: .none)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.setAnimationsEnabled(true)
            }
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AttachmentScrollView : UIView{
    let h_stack = Horizontal_Stackview(spacing:12.calcvaluex())
    
    let scrollView = UIScrollView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceVertical = false
//        let vd = UIView()
//        vd.backgroundColor = .green
//
//        vd.constrainWidth(constant: 140.calcvaluex())
//        vd.constrainHeight(constant: 100.calcvaluey())
//        let vf = UIView()
//
//        vf.backgroundColor = .red
//        vf.constrainWidth(constant: 140.calcvaluex())
//        vf.constrainHeight(constant: 100.calcvaluey())
        
//        h_stack.addArrangedSubview(AttachmentView())
//        h_stack.addArrangedSubview(AttachmentView())
//        h_stack.addArrangedSubview(UIView())
        addSubview(scrollView)
        scrollView.fillSuperview()
        
        scrollView.addSubview(h_stack)
        h_stack.fillSuperview()
        //h_stack.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        //h_stack.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        h_stack.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceAttachmentCell : ServiceFormCell,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let attachment = FormAttachment()
        attachment.path = "\(UUID().uuidString).\(url.pathExtension)"
        attachment.path_url = url.absoluteString
        data?.attachment.append(attachment)
        UIView.setAnimationsEnabled(false)
        self.tableView?.reloadSections(IndexSet(arrayLiteral: self.tag), with: .none)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.setAnimationsEnabled(true)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(991,info)
        if #available(iOS 11.0, *) {
            if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1){
                if !FileManagerUility.checkIfFileSizeExceed(data: dt) {
                    let alertController = UIAlertController(title: "您的裝置的空間不足以上傳".localized, message: "請空出空間再上傳".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
                    alertController.addAction(action)
                    General().getTopVc()?.present(alertController, animated: true, completion: nil)
                    return
                }
                let path_url = info[UIImagePickerController.InfoKey.imageURL] as? URL
                
                let uuid = "\(UUID().uuidString).jpg"
                writeDataToFolder(data: dt, name: uuid)
                let attachMent = FormAttachment()
                attachMent.path = uuid
                attachMent.image = img
                attachMent.path_url = path_url?.absoluteString ?? ""
                data?.attachment.append(attachMent)
                picker.dismiss(animated: true, completion: {
                    UIView.setAnimationsEnabled(false)
                    self.tableView?.reloadSections(IndexSet(arrayLiteral: self.tag), with: .none)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        UIView.setAnimationsEnabled(true)
                    }
                })
            }
        } else {
            // Fallback on earlier versions
        }

    }
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    let addFile = AddFileButton2()
    let attachView = AttachmentScrollView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        stackview.addArrangedSubview(attachView)
        addFile.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addFile.image.image = #imageLiteral(resourceName: "ic_attach_file_message").withRenderingMode(.alwaysTemplate)
        addFile.titles.text = "附加檔案(最多上傳六個檔案)".localized
        addFile.titles.textColor = .white
        addFile.tintColor = .white
        addFile.layer.cornerRadius = 48.calcvaluey()/2
        addFile.constrainHeight(constant: 48.calcvaluey())
        addFile.constrainWidth(constant: 300.calcvaluex())
        stackview.addArrangedSubview(addFile)
        addFile.addTarget(self, action: #selector(addItem), for: .touchUpInside)
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        if data.attachment.count >= 6 || mode == .Review{
            addFile.isHidden = true
        }
        else{
            addFile.isHidden = false
        }
        if attachView.h_stack.arrangedSubviews.count != data.attachment.count + 1 {
            attachView.h_stack.safelyRemoveArrangedSubviews()
            for (index,i) in data.attachment.enumerated() {
                let vd = AttachmentView()
                vd.mode = self.mode
                vd.tag = index
                vd.setData(data:data)
                vd.con = self
                attachView.h_stack.addArrangedSubview(vd)
                
            }
            attachView.h_stack.addArrangedSubview(UIView())
            if (mode == .Edit || mode == .New) && data.attachment.count >= 5{
                self.layoutIfNeeded()
                            let bottomOffset = CGPoint(x: attachView.scrollView.contentSize.width - attachView.scrollView.bounds.size.width, y: 0)
                            attachView.scrollView.setContentOffset(bottomOffset, animated: false)
                            print(661,attachView.scrollView.contentSize.width)
            }

            
            

        }
        else{
            for (index,i) in attachView.h_stack.arrangedSubviews.enumerated() {
                if let vd = i as? AttachmentView {
                    vd.mode = self.mode
                    vd.tag = index
                    vd.con = self
                    vd.setData(data:data)
                }
            }
        }
    }
    @objc func addItem(){
        goSelectFile()
//        data?.attachment.append(FormAttachment())
//        UIView.setAnimationsEnabled(false)
//        self.tableView?.reloadSections(IndexSet(arrayLiteral: self.tag), with: .none)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            UIView.setAnimationsEnabled(true)
//        }
    }
    
    func goSelectFile() {
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
           documentPickerController.delegate = self
           documentPickerController.modalPresentationStyle = .fullScreen
            General().getTopVc()?.present(documentPickerController, animated: true, completion: nil)
            
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = addFile
                presenter.sourceRect = addFile.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(controller, animated: true, completion: nil)
    }
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(picker, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
