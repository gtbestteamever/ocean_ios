//
//  ServiceTimeField.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
class DurationView : UIView,SignitureControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate,FormTextViewDelegate  ,UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if let dV = "\(selectionTimeArray[0])".doubleValue {
                durationField.text = "\(dV)"
                data?.timeAndDuration[self.tag].duration = "\(dV)"
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectionTimeArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(selectionTimeArray[row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let dV = "\(selectionTimeArray[row])".doubleValue {
            durationField.text = "\(dV)"
            data?.timeAndDuration[self.tag].duration = "\(dV)"
            con?.tableView?.reloadData()
        }
        
    }
    
    
    var mode : ServiceMode?
    func textViewReloadHeight(height: CGFloat) {
        UIView.performWithoutAnimation {
            containerHeight?.constant = height
//            self.layoutIfNeeded()
            con?.tableView?.beginUpdates()
            con?.tableView?.endUpdates()
        }
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .Distance {
            if let val = text.doubleValue {
                distanceField.field.text = "\(val)"
                distanceFeeField.field.text = "\(val*6)"
                data?.timeAndDuration[tag].distance = "\(val)"
            }
            else{
                distanceField.field.text = nil
                distanceFeeField.field.text = nil
                data?.timeAndDuration[tag].distance = "0"
            }
            con?.tableView?.reloadData()
        }
        else{
            
            if let val = text.replacingOccurrences(of: "小時", with: "").doubleValue {
                durationField.text = "\(val)"
                data?.timeAndDuration[self.tag].duration = "\(val)"
            }
            else{
                durationField.text = "\(data?.timeAndDuration[self.tag].duration ?? "0")"
            }
        }

    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let uuid = "\(UUID().uuidString).\(url.pathExtension)"
        let signiture = FormSigniture()
         signiture.path = uuid
         signiture.path_url = url.absoluteString
         data?.timeAndDuration[self.tag].signiture = signiture

        UIView.setAnimationsEnabled(false)
        if let con = self.con {
            self.con?.tableView?.reloadData()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.setAnimationsEnabled(true)
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(991,info)
        if #available(iOS 11.0, *) {
            if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1), let path_url = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                if !FileManagerUility.checkIfFileSizeExceed(data: dt) {
                    let alertController = UIAlertController(title: "您的裝置的空間不足以上傳".localized, message: "請空出空間再上傳".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
                    alertController.addAction(action)
                    General().getTopVc()?.present(alertController, animated: true, completion: nil)
                    return
                }
                
                let uuid = "\(UUID().uuidString).jpg"
                writeDataToFolder(data: dt, name: uuid)
               let signiture = FormSigniture()
                signiture.path = uuid
                signiture.path_url = path_url.absoluteString
                data?.timeAndDuration[self.tag].signiture = signiture
                picker.dismiss(animated: true, completion: {
                    UIView.setAnimationsEnabled(false)
                    if let con = self.con{
                        con.tableView?.reloadData()
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        UIView.setAnimationsEnabled(true)
                    }
                })
            }
        } else {
            // Fallback on earlier versions
        }

    }
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    func sendImage(image: UIImage) {
        print(663,image)
        let uuid = "\(UUID().uuidString).jpg"
        print(3212,uuid)
        let n_data = image.jpegData(compressionQuality: 0.1)
        writeDataToFolder(data: n_data, name: uuid)
        data?.timeAndDuration[self.tag].signiture.path = uuid
        data?.timeAndDuration[self.tag].signiture.signiture = image
        self.con?.tableView?.reloadData()
    }
    let startField = FormTextView(placeText: "", mode: .collectInfo)
    let endField = FormTextView(placeText: "", mode: .collectInfo)
    let durationField = FormTextView(placeText: "", mode: .collectInfo)
    
    let signLabel = UILabel()
    let addSignView = AddSignView()
    let signView = FormSignView()
    var data : ServiceForm?
    let distanceField = FeeField(text: "來回距離(km):")
    let distanceFeeField = FeeField(text: "車馬費:")
    func setData(data:ServiceForm) {
        self.data = data
        self.layoutIfNeeded()
        let time = data.timeAndDuration[self.tag]
        if data.isMaintenance {
            startField.text = Date().getCurrentTime()
        }
        else{
            startField.text = Date().convertToDateComponent(text: time.startTime,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        }
        
        endField.text = Date().convertToDateComponent(text: time.endTime,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        
        //startField.textViewDidChange(startField)
        //endField.textViewDidChange(endField)
        if time.duration != "0" {
            durationField.text = time.duration
        }
        
        if time.distance != "0" {
            distanceField.field.text = time.distance
            if let distance = time.distance.doubleValue {
                distanceFeeField.field.text = "\(distance * 6)"
            }
            
        }
                    
        
        if let image = time.signiture.signiture {
        
                    addSignView.isHidden = true
                    signView.isHidden = false
                    signView.imgv.image = image
                }
        else if let path = time.signiture.path_url,let  url = URL(string: path) {
            addSignView.isHidden = true
            signView.isHidden = false
            url.getThumnailImageCom { (imag) in
                self.signView.imgv.image = imag
            }
            //signView.imgv.image = url.getThumnailImage()
        }
                else{
                    signView.isHidden = true
                    signView.imgv.image = nil
                    addSignView.isHidden = false
        
                }
        
        if mode == .Review {
            distanceField.isUserInteractionEnabled = false
            durationField.isUserInteractionEnabled = false
            signView.xbutton.isHidden = true
            if time.signiture.path_url == nil{
                addSignView.isHidden = true
                signView.isHidden = true
                signLabel.isHidden = true
            }
        }
        else{
            distanceField.isUserInteractionEnabled = true
            durationField.isUserInteractionEnabled = true
        }

    }
    weak var con : ServiceTimeField?
    let container = UIView()
    var containerHeight : NSLayoutConstraint?
    let sign_stack = Horizontal_Stackview(spacing:24.calcvaluex(),alignment: .top)
    let picker = UIPickerView()
    let selectionTimeArray = Array(1...30)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let h_stack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
       
        
        container.addSubview(startField)
        //startField.isHidden = true
        container.translatesAutoresizingMaskIntoConstraints = false
        //container.backgroundColor = .red

        let label = UILabel()
        label.text = "~"
        //label.isHidden = true
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        container.addSubview(label)
        container.addSubview(endField)
        label.centerInSuperview()
        //endField.isHidden = true
        startField.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: label.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 4.calcvaluex()))
        //startField.centerYInSuperview()
        startField.t_delegate = self
        endField.t_delegate = self
        startField.isUserInteractionEnabled = false
        endField.isUserInteractionEnabled = false
        endField.anchor(top: startField.topAnchor, leading: label.trailingAnchor, bottom: startField.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        durationField.textContainerInset = .init(top: 16.calcvaluey(), left: 10.calcvaluex(), bottom: 16.calcvaluey(), right: 10.calcvaluex())
        durationField.textAlignment = .right
        durationField.constrainWidth(constant: 120.calcvaluex())
        durationField.tintColor = .clear
        //durationField.t_delegate = self
        durationField.delegate = self
        durationField.inputView = picker
        picker.delegate = self
        picker.dataSource = self
        h_stack.addArrangedSubview(container)
        containerHeight = container.heightAnchor.constraint(equalToConstant: 52.calcvaluey())
        containerHeight?.isActive = true
        h_stack.addArrangedSubview(durationField)
        
        
        signLabel.text = "客戶簽名:"
        signLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())

        sign_stack.addArrangedSubview(signLabel)
        sign_stack.addArrangedSubview(addSignView)
        addSignView.addButton.addTarget(self, action: #selector(goAddSignature), for: .touchUpInside)
        addSignView.grabButton.addTarget(self, action: #selector(goSelectSigniture), for: .touchUpInside)
        signView.isHidden = true
        sign_stack.addArrangedSubview(signView)
        signView.xbutton.addTarget(self, action: #selector(removeSign), for: .touchUpInside)
        sign_stack.addArrangedSubview(UIView())
       // vert_stackview.addArrangedSubview(sign_stack)
        //addSubview(h_stack)
        //h_stack.fillSuperview()
        
        let v_stack = Vertical_Stackview(spacing:18.calcvaluey())
        v_stack.addArrangedSubview(h_stack)
        v_stack.addArrangedSubview(distanceField)
        distanceField.field.t_delegate = self
        v_stack.addArrangedSubview(distanceFeeField)
        distanceFeeField.isUserInteractionEnabled = false
        v_stack.addArrangedSubview(sign_stack)
        
        addSubview(v_stack)
        v_stack.fillSuperview()
        
    }
    func goSelectFile() {
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
           documentPickerController.delegate = self
           documentPickerController.modalPresentationStyle = .fullScreen
            General().getTopVc()?.present(documentPickerController, animated: true, completion: nil)
            
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
            presenter.sourceView = addSignView.grabButton
                presenter.sourceRect = addSignView.grabButton.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(controller, animated: true, completion: nil)
    }
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(picker, animated: true, completion: nil)
    }
    @objc func goSelectSigniture(){
        goSelectFile()
    }
    @objc func removeSign(){
        data?.timeAndDuration[self.tag].signiture.signiture = nil
        data?.timeAndDuration[self.tag].signiture.path = nil
        data?.timeAndDuration[self.tag].signiture.path_url = nil
        
        if let id = data?.timeAndDuration[self.tag].id {
            data?.timeAndDuration[self.tag].del_files.append(id)
        }
        self.con?.tableView?.reloadData()
    }
    @objc func goAddSignature(){
        let con = SignitureController()
        con.modalPresentationStyle = .fullScreen
        con.delegate = self
        
        General().getTopVc()?.present(con, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceTimeField : ServiceFormCell {
    let label : UILabel = {
       let label = UILabel()
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        label.text = "服務時間".localized
        return label
    }()
    //let durationView = DurationView()
    let vert_stack = Vertical_Stackview(spacing:20.calcvaluey())
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        stackview.addArrangedSubview(label)
    
        stackview.addArrangedSubview(vert_stack)
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)

        if data.timeAndDuration.count != vert_stack.arrangedSubviews.count {
            vert_stack.safelyRemoveArrangedSubviews()
        for (index,i) in data.timeAndDuration.enumerated() {
            let durationView = DurationView()
            durationView.mode = self.mode
            durationView.con = self
            durationView.tag = index
            if mode == .New || mode == .addOn{
            if isReassign {
                durationView.sign_stack.isHidden = false
            }
            else{
                durationView.sign_stack.isHidden = true
            }
            }
            else{
                durationView.sign_stack.isHidden = false
            }
            durationView.setData(data:data)
            vert_stack.addArrangedSubview(durationView)

        }
        }
        else{
            for (index,i) in vert_stack.arrangedSubviews.enumerated() {
                if let v = i as? DurationView {
                    v.mode = self.mode
                    v.con = self
                    v.tag = index
                    if mode == .New || mode == .addOn{
                    if isReassign {
                        v.sign_stack.isHidden = false
                    }
                    else{
                        v.sign_stack.isHidden = true
                    }
                    }
                    else{
                        v.sign_stack.isHidden = false
                    }
                    v.setData(data: data)
                }
            }
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
