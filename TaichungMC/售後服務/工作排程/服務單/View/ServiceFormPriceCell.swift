//
//  ServiceFormPriceCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/31/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class ServicePriceGroup : UIView,UITextFieldDelegate {
    var mode : ServiceMode?
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == multiplierField.textField {
            if let value = textField.text?.doubleValue {
                data?.priceGroup.extraGroup[self.tag].multiplier = String(value)
                textField.text = "\(value)"
            }
            else{
                textField.text = nil
            }
        }
        if textField == priceField.textField {
            if let value = textField.text?.integerValue {
                data?.priceGroup.extraGroup[self.tag].price = String(value)
            }
            else{
                textField.text = nil
            }
        }
        if let con = con {
            con.calculateTotal()
        }
//        UIView.setAnimationsEnabled(true)
//        if let con = self.con {
//            con.tableView?.reloadSections(IndexSet(integer: con.tag), with: .none)
//        }
//
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
//            UIView.setAnimationsEnabled(false)
//        }

    }
    let priceField = ServicePriceField(text: "技術費".localized, placeholder: "請輸入金額")
    let multiplierField = ServicePriceField(text: "假日服務".localized, placeholder: "請輸入服務加成".localized)
    var data : ServiceForm?
    func setData(data:ServiceForm) {
        self.data = data
        if mode == .Review{
            priceField.isUserInteractionEnabled = false
            multiplierField.isUserInteractionEnabled = false
            priceField.textField.placeholder = ""
            multiplierField.textField.placeholder = ""
            xbutton.isHidden = true
        }
        else{
            priceField.isUserInteractionEnabled = true
            multiplierField.isUserInteractionEnabled = true
            xbutton.isHidden = false
        }
        let priceGroup = data.priceGroup.extraGroup[self.tag]
        //priceField.textField.text = priceGroup.price
        //multiplierField.textField.text = priceGroup.multiplier
        print(8889,self.tag,priceGroup.price,priceGroup.multiplier)
        priceField.textField.text = priceGroup.price
        multiplierField.textField.text = priceGroup.multiplier
        print(priceGroup.price == "0")
//        if priceGroup.price != "0"{
//            priceField.textField.text = priceGroup.price
//        }
//        else{
//            priceField.textField.text = nil
//        }
//        if priceGroup.multiplier != "0"{
//            multiplierField.textField.text = priceGroup.multiplier
//        }
//        else{
//            priceField.textField.text = nil
//        }
        
        
    }
    let stackview = Horizontal_Stackview(spacing:8.calcvaluex(),alignment: .center)
    let xbutton = UIButton(type: .custom)
    weak var con : ServiceFormPriceCell?
    override init(frame: CGRect) {
        super.init(frame: frame)
        let container = UIView()
        let ximage = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
        ximage.constrainWidth(constant: 28.calcvaluex())
        ximage.constrainHeight(constant: 28.calcvaluex())
        container.addSubview(ximage)
        ximage.centerInSuperview()
      //  stackview.addArrangedSubview(priceField)
        container.addSubview(priceField)
        priceField.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: ximage.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 8.calcvaluex()))
        container.addSubview(multiplierField)
        multiplierField.anchor(top: container.topAnchor, leading: ximage.trailingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0))
        //stackview.addArrangedSubview(ximage)
        //stackview.addArrangedSubview(multiplierField)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        //xbutton.isHidden = true
        xbutton.constrainWidth(constant: 32.calcvaluex())
        xbutton.constrainHeight(constant: 32.calcvaluex())
        stackview.addArrangedSubview(container)
        stackview.addArrangedSubview(xbutton)
        xbutton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
        addSubview(stackview)
        stackview.fillSuperview()
        multiplierField.textField.delegate = self
        priceField.textField.delegate = self

    }
    @objc func removeItem(){
        data?.priceGroup.extraGroup.remove(at: self.tag)
        UIView.setAnimationsEnabled(true)
        con?.tableView?.reloadData()
        //con?.tableView?.reloadSections(IndexSet(integer: 4), with: .none)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            UIView.setAnimationsEnabled(false)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceTextField : UITextField {
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    
}
class ServicePriceField : UIView {
    let textField = ServiceTextField()
    init(text:String,placeholder:String) {
        super.init(frame: .zero)
        layer.cornerRadius = 15.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        constrainHeight(constant: 52.calcvaluey())
        backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        let seperator = UIView()
        
        addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        let label = UILabel()
        label.text = text
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        label.textAlignment = .right
        addSubview(label)
        
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 14.calcvaluex()),size: .init(width: UserDefaults.standard.getConvertedLanguage() == "en" ? 110.calcvaluex() : 80.calcvaluex(), height: 0))
       
        seperator.anchor(top: topAnchor, leading: label.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 9.calcvaluey(), left: 14.calcvaluex(), bottom: 9.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        addSubview(textField)
        textField.anchor(top: topAnchor, leading: seperator.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        textField.textAlignment = .right
        textField.placeholder = placeholder
        textField.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServicePriceView : UIView{

    let label : UILabel = {
       let label = UILabel()
        label.text = "費用計算".localized
        label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
        return label
    }()
    let subLabel : UILabel = {
        let label = UILabel()
        label.text = "假日服務說明：如是夜間、休息、例假，原技術費用乘以 150%、200%、300%".localized
         label.font = UIFont(name: "Roboto-Light", size: 14.calcvaluex())
         label.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
         return label
    }()
    let component_totalField = ServicePriceField(text: "零件費".localized, placeholder: "")
    let discountField = ServicePriceField(text: "折扣".localized, placeholder: "請輸入金額".localized)
    let loadMoreButton = UIButton(type: .custom)
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,
        NSAttributedString.Key.foregroundColor: UIColor.lightGray.cgColor,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    let addButton = addExtraField(text: "新增技術費".localized)
    let v_stack = Vertical_Stackview(spacing:22.calcvaluey())
    let buttonContainer = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        addSubview(subLabel)
        subLabel.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        let vert_stack = Vertical_Stackview(spacing:22.calcvaluey())
        addSubview(vert_stack)
        vert_stack.anchor(top: subLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        vert_stack.addArrangedSubview(v_stack)
        addButton.constrainWidth(constant: 42.calcvaluex() + (addButton.label.text?.widthOfString(usingFont: addButton.label.font) ?? 0))
        vert_stack.addArrangedSubview(addButton)
        component_totalField.isUserInteractionEnabled = false
        vert_stack.addArrangedSubview(component_totalField)
        vert_stack.addArrangedSubview(discountField)
        
        
        loadMoreButton.setAttributedTitle(NSAttributedString(string: "顯示更多", attributes: attrs), for: .normal)
        buttonContainer.isHidden = true
        buttonContainer.addSubview(loadMoreButton)
        //loadMoreButton.backgroundColor = .red
        loadMoreButton.anchor(top: buttonContainer.topAnchor, leading: nil, bottom: buttonContainer.bottomAnchor, trailing: nil,size: .init(width: 100.calcvaluex(), height: 40.calcvaluey()))
        loadMoreButton.centerXInSuperview()
        vert_stack.addArrangedSubview(buttonContainer)
 
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceFormPriceCell: ServiceFormCell,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == priceView.discountField.textField {
            if let _ = textField.text?.integerValue {
                data?.priceGroup.discount = textField.text ?? "0"
                textField.text = "$\(textField.text ?? "0")"
            }
            else{
                data?.priceGroup.discount = "0"
                textField.text = nil
            }

            
            calculateTotal()
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == priceView.discountField.textField {
            textField.text = textField.text?.replacingOccurrences(of: "$", with: "")
        }
    }
    let priceView = ServicePriceView()
    let label = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let leftspacer = UIView()
        let rightspacer = UIView()
        leftspacer.constrainWidth(constant: 36.calcvaluex())
        rightspacer.constrainWidth(constant: 36.calcvaluex())
        let h_stack = Horizontal_Stackview()
        h_stack.addArrangedSubview(leftspacer)
        h_stack.addArrangedSubview(priceView)
        h_stack.addArrangedSubview(rightspacer)
        //anchor?.bottom?.constant = 0
        anchor?.leading?.constant = 0
        anchor?.trailing?.constant = 0
        
        let vd = UIView()
        vd.addSubview(label)
        //label.backgroundColor = .red
        label.textAlignment = .right
        label.fillSuperview(padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()))
//        vd.layer.cornerRadius = container.layer.cornerRadius
//        if #available(iOS 11.0, *) {
//            vd.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        vd.constrainHeight(constant: 59.calcvaluey())
        vd.backgroundColor = MajorColor().oceanColor
        stackview.addArrangedSubview(h_stack)
        //stackview.addArrangedSubview(UIView())
        stackview.addArrangedSubview(vd)
        priceView.addButton.addButton.addTarget(self, action: #selector(addItem), for: .touchUpInside)
        priceView.addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addItem)))
        priceView.discountField.textField.delegate = self
        
        priceView.loadMoreButton.addTarget(self, action: #selector(loadMore), for: .touchUpInside)
    }
    @objc func loadMore(){
        data?.didLoadMore = true
        UIView.performWithoutAnimation {
            tableView?.reloadSections(IndexSet(integer: self.tag), with: .none)
        }
    }
    @objc func addItem(){
        data?.priceGroup.extraGroup.append(FormExtraWork())
        tableView?.reloadSections(IndexSet(integer: self.tag), with: .none)
        
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        print(221,data.priceGroup.discount)
        if mode == .Review{
            if let ff = data.priceGroup.discount.integerValue,ff != 0{
                priceView.discountField.isHidden = false
                
            }
            else{
                priceView.discountField.isHidden = true
            }
            
            priceView.addButton.isHidden = true
            priceView.discountField.isUserInteractionEnabled = false
            priceView.discountField.textField.placeholder = ""
        }
        else{

            
            

            
            priceView.addButton.isHidden = false
            priceView.discountField.isUserInteractionEnabled = true
            priceView.discountField.isHidden = false
            priceView.buttonContainer.isHidden = true
            if mode == .Edit && !data.didLoadMore{
                priceView.discountField.isHidden = true
                priceView.buttonContainer.isHidden = false
            }
        }
        priceView.discountField.textField.text = "$\(data.priceGroup.discount)"
        var total = 0
        
        
        for i in data.components {
            if let price = i.total_price.integerValue {
                total += price
            }
        }
        
  
        data.priceGroup.total_component_price = "\(total)"
        priceView.component_totalField.textField.text = "$\(total)"

        
        let arr = data.priceGroup.extraGroup
        if priceView.v_stack.arrangedSubviews.count != arr.count {
            //.arrangedSubviews.forEach { $0.removeFromSuperview() }
            priceView.v_stack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        }
     for (index,i) in arr.enumerated() {
        if priceView.v_stack.arrangedSubviews.count != arr.count {
            let dt = ServicePriceGroup()
            dt.mode = self.mode
            priceView.v_stack.addArrangedSubview(dt)
            
             dt.tag = index
            dt.con = self
                 //dt.con = self
             dt.setData(data: data)
        }
        else{
            if let vv = priceView.v_stack.arrangedSubviews[index] as? ServicePriceGroup {
                vv.mode = self.mode
                vv.tag = index
                vv.con = self
                vv.setData(data: data)
            }
        }

             


            
             
         }
        calculateTotal()
    }
    func calculateTotal() {
        var total = 0
        if let data = data?.priceGroup {
            for i in data.extraGroup {
                
                if let price = i.price.integerValue , let multiplier = i.multiplier.doubleValue {
                    let tt = Int(Double(price) * multiplier)
                    total += tt
                }
            }
            if let pt = data.total_component_price.integerValue {
                total += pt
            }
            
            if let pt = data.discount.integerValue {
                print(992,pt)
                total -= pt
            }

        }
        
        if let time = data?.timeAndDuration.last {
            if let distance = time.distance.doubleValue {
                let distanceFee = distance * 6
                data?.transporationCost = Int(distanceFee)
                total += Int(distanceFee)
            }
            
            if let hour = time.duration.doubleValue, let calcuationFee = GetStatus().getUpkeepOrderStatus() {
                if data?.isMaintenance == true  {
                    
                    let totalFee = (1 * calcuationFee.hours_of_work_range_fees_select_machine.one) + ((Int(hour) - 1) * calcuationFee.hours_of_work_range_fees_select_machine.two)
                    total += totalFee
                    data?.workHoursCost = totalFee
                }
                else{
                    if data?.selectOrInput == 0 {
                        let totalFee = (1 * calcuationFee.hours_of_work_range_fees_select_machine.one) + ((Int(hour) - 1) * calcuationFee.hours_of_work_range_fees_select_machine.two)
                        total += totalFee
                        data?.workHoursCost = totalFee
                    }
                    else{
                        let totalFee = (1 * calcuationFee.hours_of_work_range_fees_input_machine.one) + ((Int(hour) - 1) * calcuationFee.hours_of_work_range_fees_input_machine.two)
                        total += totalFee
                        data?.workHoursCost = totalFee
                    }
                }
            }
        }

        setTotal(total: total)
        
    }
    func setTotal(total:Int) {
        var attributedText = NSMutableAttributedString(string: "總價".localized,attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor : UIColor.white])
        attributedText.append(NSAttributedString(string: "        \(total)\("台幣".localized)", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 20.calcvaluex())!,NSAttributedString.Key.foregroundColor : UIColor.white]))
        
        label.attributedText = attributedText
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
