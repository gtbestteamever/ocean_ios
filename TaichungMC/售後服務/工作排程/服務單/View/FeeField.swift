//
//  FeeField.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2024/7/23.
//  Copyright © 2024 TaichungMC. All rights reserved.
//

import UIKit

class FeeField : UIView {
    let label = UILabel()
    let field = FormTextView(placeText: "", mode: .Distance)
    init(text:String) {
        super.init(frame: .zero)
        label.text = text
        label.textColor = .black
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        let hstack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .center)
        
        hstack.addArrangedSubview(label)
        hstack.addArrangedSubview(field)
        field.constrainWidth(constant: 180.calcvaluex())
        hstack.addArrangedSubview(UIView())
        addSubview(hstack)
        hstack.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
