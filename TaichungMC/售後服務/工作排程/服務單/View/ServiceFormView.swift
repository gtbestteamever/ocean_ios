//
//  ServiceFormView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
enum ServiceFieldType : String {
    case Customer = "customer"
    case Machine = "machine"
    case Attachment = "attachment"
    case Inventory = "inventory"
    case Price = "price"
    case Collect = "collect"
    case Time = "time"
    case Engineer = "engineer"
    
    var sectionHeight : CGFloat {
        switch self {
        case .Customer :
            return 0
        case .Collect :
            return 0
        case .Time :
            return 0
        default:
            return UITableView.automaticDimension
            
        }
    }
    var sectionTitle : String{
        switch self {
        case .Attachment :
            return "附檔".localized
        case .Engineer :
            return "工程確認".localized
        case .Inventory :
            return "換件項目".localized
        case .Price :
            return "費用及收款狀況".localized
        case .Machine :
            return "機台資訊".localized
        default:
            return ""
            
        }
    }
}
class ServiceFormView : UIView {
    var cachedCellHeights = [IndexPath: CGFloat]()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var isReassign = false
    var form_data = ServiceForm() {
        didSet{
            if mode == .Review{
                if form_data.attachment.count == 0{
                    fields = [.Customer,.Machine,
                              .Time,.Price,.Collect,.Engineer]
                }
            }
            else if mode == .New || mode == .addOn {
                if isReassign {
                    fields = [.Customer,.Machine,.Attachment,.Time,.Engineer]
                }

            }
           // else if mode ==
            

            self.tableview.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableview.contentOffset = .zero
                self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
            //
            
            
        }
    }
    var fields : [ServiceFieldType] = [.Customer,.Machine,.Attachment,.Time,.Price,.Collect,.Engineer]
    var mode : ServiceMode?
    override init(frame: CGRect) {
        super.init(frame: frame)
        tableview.backgroundColor = .clear
        addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        //tableview.bounces = false
        tableview.fillSuperview(padding: .init(top: 11.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.contentInset.top = 26.calcvaluey()
        tableview.contentInset.bottom = 26.calcvaluey()
        tableview.delegate = self
        tableview.separatorStyle = .none
        tableview.dataSource = self
        tableview.register(ServiceFormThirdSectionCell.self, forCellReuseIdentifier: ServiceFieldType.Inventory.rawValue)
        tableview.register(ServiceFormFirstSectionCell.self, forCellReuseIdentifier: ServiceFieldType.Customer.rawValue)
        tableview.register(ServiceFormSecondSectionCell.self, forCellReuseIdentifier: ServiceFieldType.Machine.rawValue)
        tableview.register(ServiceFormPriceCell.self, forCellReuseIdentifier: ServiceFieldType.Price.rawValue)
        //tableview.register(UITableViewCell.self, forCellReuseIdentifier: "default")
        tableview.register(ServiceCollectTypeCell.self, forCellReuseIdentifier: ServiceFieldType.Collect.rawValue)
        tableview.register(ServiceEngineerCell.self, forCellReuseIdentifier: ServiceFieldType.Engineer.rawValue)
        tableview.register(ServiceTimeField.self, forCellReuseIdentifier: ServiceFieldType.Time.rawValue)
        tableview.register(ServiceAttachmentCell.self, forCellReuseIdentifier: ServiceFieldType.Attachment.rawValue)


        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
