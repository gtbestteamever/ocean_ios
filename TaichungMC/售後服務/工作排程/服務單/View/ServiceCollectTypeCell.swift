//
//  ServiceCollectTypeCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class ServiceCollectTypeCell: ServiceFormCell,circularSelectionDelegate,FormTextViewDelegate {
    func textViewReloadHeight(height: CGFloat) {
        UIView.performWithoutAnimation {
            tableView?.beginUpdates()
            tableView?.endUpdates()
        }
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        data?.collectAndOther.moreInfo = text
    }
    
    func selectedIndex(index: Int) {
        data?.collectAndOther.status = GetStatus().getCollectStatus()[index]
    }
    
    let collectContainer = circularSelectionContainer(text: "收款方式".localized, array: ["現金".localized,"轉帳".localized,"支票".localized])
    let infoField = NormalTextView(text: "備註".localized, placeholdertext: "請輸入備註".localized, color: #colorLiteral(red: 0.29461658, green: 0.2946640253, blue: 0.2946062088, alpha: 1))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        stackview.spacing = 46.calcvaluey()
        stackview.addArrangedSubview(collectContainer)
       // collectContainer.stackview.addArrangedSubview(UIView())
        stackview.addArrangedSubview(infoField)
        infoField.textfield.mode = .collectInfo
        infoField.textfield.t_delegate = self
        collectContainer.delegate = self
        
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        self.layoutIfNeeded()
        let collect = data.collectAndOther
        if let status = collect.status,let key = GetStatus().getCollectStatus().first { (key,val) -> Bool in
            return val == status
        }?.key{
            collectContainer.selectedIndex = key
        }
        
        infoField.textfield.text = collect.moreInfo
       // infoField.textfield.textViewDidChange(infoField.textfield)
        
        if mode == .Review{
            collectContainer.isUserInteractionEnabled = false
            infoField.isUserInteractionEnabled = false
            infoField.textfield.placeholderLabel.text = ""
        }
        else{
            collectContainer.isUserInteractionEnabled = true
            infoField.isUserInteractionEnabled = true
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
