//
//  ServiceFormHeader.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class ServiceFormHeader: UIView {
    let label = UILabel()
    let container = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        container.backgroundColor = .white
        addSubview(container)
        container.fillSuperview(padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        container.addSubview(label)
        label.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 6.calcvaluey(), right: 0))
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        label.text = "服務時間".localized
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
