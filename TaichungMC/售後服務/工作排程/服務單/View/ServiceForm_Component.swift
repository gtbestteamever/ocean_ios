//
//  ServiceForm_Component.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/31/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class FormSignView : UIView {
    let imgv = UIImageView()
    let xbutton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        imgv.contentMode = .scaleAspectFit
        imgv.backgroundColor = .white
        imgv.addshadowColor()
        
        addSubview(imgv)
        imgv.fillSuperview()
        imgv.constrainWidth(constant: 250.calcvaluex())
        imgv.constrainHeight(constant: 150.calcvaluey())
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        addSubview(xbutton)
        xbutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 6.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddSignView : UIView{
    let addButton = UIButton(type: .custom)
    let grabButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        layer.cornerRadius = 8.calcvaluex()
        
        constrainWidth(constant: 250.calcvaluex())
        constrainHeight(constant: 150.calcvaluex())
        
        let horizontalStackview = Horizontal_Stackview(distribution:.fillEqually,spacing:24.calcvaluex(),alignment: .center)
        
        addSubview(horizontalStackview)
        horizontalStackview.fillSuperview(padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        
        addButton.constrainHeight(constant: 36.calcvaluey())
        grabButton.constrainHeight(constant: 36.calcvaluey())
        addButton.layer.cornerRadius = 36.calcvaluey()/2
        grabButton.layer.cornerRadius = 36.calcvaluey()/2
        
        horizontalStackview.addArrangedSubview(addButton)
        horizontalStackview.addArrangedSubview(grabButton)
        
        addButton.setTitle("新增", for: .normal)
        grabButton.setTitle("匯入", for: .normal)
        
        addButton.setTitleColor(.white, for: .normal)
        grabButton.setTitleColor(.white, for: .normal)
        
        addButton.backgroundColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        grabButton.backgroundColor = MajorColor().oceanColor
        
        addButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        grabButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        grabButton.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class InventoryListView : UIView,FormTextViewDelegate {

    
    func textViewReloadHeight(height: CGFloat) {
//        UIView.performWithoutAnimation {
//            con?.tableView?.beginUpdates()
//            con?.tableView?.endUpdates()
//        }
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        //
    }
    
    let seperator = UIView()
    let statusheaderlabel = StatusLabel(text: "轉交", color: #colorLiteral(red: 0.7563990951, green: 0.2861070335, blue: 0.3254902363, alpha: 1))
    let errorheaderlabel = UILabel()
    let inventoryheaderlabel = UILabel()
    let quantityheaderlabel = UILabel()
    
    
    let collectstatuslabel = UILabel()
    let totallabel = UILabel()
    let horizontalStackview = Horizontal_Stackview(spacing:24.calcvaluex(),alignment: .top)
    let vert_stackview = Vertical_Stackview(spacing:32.calcvaluey())
    let reasonField = FormTextView(placeText: "", mode: .Maintain)

    let deleteButton = UIButton(type: .custom)
    let editButton = UIButton(type: .custom)
    var ownerLabel : UILabel = {
       let label = UILabel()
        
        let anchor = label.widthAnchor.constraint(lessThanOrEqualToConstant: 300.calcvaluex())
        anchor.priority = .init(1000)
        anchor.isActive = true
        
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.numberOfLines = 0
        return label
    }()
    func setData(data:ServiceForm) {
        
        self.data = data
        if con?.mode == .Review{
            deleteButton.isHidden = true
            editButton.isHidden = true
        }
        else{
            deleteButton.isHidden = false
            editButton.isHidden = false
        }
        let dt = data.components[self.tag]
        ownerLabel.text = "\("零件持有人".localized): \(dt.owner)"
        errorheaderlabel.text = dt.error_code
        inventoryheaderlabel.text = "#\(dt.component_code)"
        quantityheaderlabel.text = dt.quantity
        collectstatuslabel.text = dt.payment_status == 0 ? "收費".localized : "不收費".localized
        totallabel.text = "$\(dt.total_price.integerValue ?? 0)"
        reasonField.text = "\("更換原因".localized): \(dt.reason)"
        if let status = dt.status, let status_arr = GetStatus().getUpkeepOrderStatus()?.component_statuses, let data = status_arr.first(where: { (st) -> Bool in
            return st.key == status
        }) {
            statusheaderlabel.text = data.getString()
            statusheaderlabel.layer.borderColor = UIColor().hexStringToUIColor(hex: data.color ?? "").cgColor
            statusheaderlabel.textColor = UIColor().hexStringToUIColor(hex: data.color ?? "")
           
        }
//        self.layoutIfNeeded()
//        reasonField.textViewDidChange(reasonField)
        
//        print(62,dt.signiture)
//        if let image = dt.signiture {
//            
//            addSignView.isHidden = true
//            signView.isHidden = false
//            signView.imgv.image = image
//        }
//        else{
//            signView.isHidden = true
//            signView.imgv.image = nil
//            addSignView.isHidden = false
//            
//        }

    }

    weak var con : ServiceFormThirdSectionCell?
    var data : ServiceForm?
    override init(frame: CGRect) {
        super.init(frame: frame)
        horizontalStackview.addArrangedSubview(statusheaderlabel)
        
        //setHeaderLabel(label: statusheaderlabel, text: "狀態",width:80.calcvaluex())
        //statusheaderlabel.removeAllConstraints()
        statusheaderlabel.constrainWidth(constant: 80.calcvaluex())
        
        statusheaderlabel.numberOfLines = 2
        setHeaderLabel(label: errorheaderlabel, text: "",width: 100.calcvaluex())
        setHeaderLabel(label: inventoryheaderlabel, text: "",width: 100.calcvaluex())
        setHeaderLabel(label: quantityheaderlabel, text: "",width: UserDefaults.standard.getConvertedLanguage() == "en" ? 80.calcvaluex() : 60.calcvaluex())
        setHeaderLabel(label: collectstatuslabel, text: "",width: 80.calcvaluex())
        setHeaderLabel(label: totallabel, text: "")
        editButton.setImage(#imageLiteral(resourceName: "edit").withRenderingMode(.alwaysTemplate), for: .normal)
        editButton.tintColor = .black
        editButton.contentVerticalAlignment = .fill
        editButton.contentHorizontalAlignment = .fill
        //editButton.isHidden = true
       //deleteButton.isHidden = true
        editButton.addTarget(self, action: #selector(goEdit), for: .touchUpInside)
        editButton.constrainWidth(constant: 24.calcvaluex())
        editButton.constrainHeight(constant: 24.calcvaluex())
        deleteButton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        deleteButton.tintColor = .black
        deleteButton.constrainWidth(constant: 24.calcvaluex())
        deleteButton.constrainHeight(constant: 24.calcvaluex())
        deleteButton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
        
        horizontalStackview.addArrangedSubview(editButton)
        horizontalStackview.addArrangedSubview(deleteButton)
        if #available(iOS 11.0, *) {
            horizontalStackview.setCustomSpacing(0, after: deleteButton)
        } else {
            // Fallback on earlier versions
        }

        //horizontalStackview.addArrangedSubview(UIView())
        seperator.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        seperator.constrainHeight(constant: 0.5.calcvaluey())
        vert_stackview.addArrangedSubview(seperator)
        vert_stackview.addArrangedSubview(horizontalStackview)
        let h_stack = Horizontal_Stackview(spacing:8.calcvaluex())
        h_stack.addArrangedSubview(ownerLabel)
        
        ownerLabel.setContentHuggingPriority(.init(1000), for: .horizontal)
        h_stack.addArrangedSubview(reasonField)
        reasonField.setContentHuggingPriority(.init(1), for: .horizontal)
        vert_stackview.addArrangedSubview(h_stack)
        //reasonField.text = "更換原因: 更換零件"
        reasonField.mode = .changeReason
        reasonField.isUserInteractionEnabled = false
        reasonField.t_delegate = self
        
        addSubview(vert_stackview)
        vert_stackview.fillSuperview()
        
    }
    @objc func goEdit(){
        let vd = AddServiceComponentController()
        if let data = data {
            vd.componentData = data.components[self.tag]
        }
        vd.view.tag = self.tag
        vd.isEdit = true
        vd.modalPresentationStyle = .overCurrentContext
        vd.con = self.con
        General().getTopVc()?.present(vd, animated: false, completion: nil)
    }
    @objc func removeItem(){
        let ct = data?.components.remove(at: self.tag)
        if let id = ct?.id {
            data?.del_components.append(id)
        }
        self.con?.tableView?.reloadData()
    }

    func setHeaderLabel(label:UILabel,text:String,width:CGFloat = 0) {
        //label.backgroundColor = .red
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = text
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        if width != 0{
            //label.widthAnchor.constraint(greaterThanOrEqualToConstant: text.widthOfString(usingFont: label.font)).isActive = true
            label.constrainWidth(constant: width)
        }
        horizontalStackview.addArrangedSubview(label)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ServiceFormThirdSectionCell : ServiceFormCell {
    let statusheaderlabel = UILabel()
    let errorheaderlabel = UILabel()
    let inventoryheaderlabel = UILabel()
    let quantityheaderlabel = UILabel()
    
    
    let collectstatuslabel = UILabel()
    let totallabel = UILabel()
    let headerStackview = Horizontal_Stackview(spacing:24.calcvaluex())
    let seperator = UIView()
    let addExtraButton = addExtraField(text: "新增換件項目".localized)
    let inventory_vert_stack = Vertical_Stackview(spacing: 20.calcvaluey())
    let extra_spacer = UIView()
    let buttonView = Horizontal_Stackview()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        anchor?.trailing?.constant = -12.calcvaluex()
        
        setHeaderLabel(label: statusheaderlabel, text: "狀態".localized,width:80.calcvaluex())
        setHeaderLabel(label: errorheaderlabel, text: "故障代碼".localized,width: 100.calcvaluex())
        setHeaderLabel(label: inventoryheaderlabel, text: "零件編號".localized,width: 100.calcvaluex())
        setHeaderLabel(label: quantityheaderlabel, text: "數量".localized,width: UserDefaults.standard.getConvertedLanguage() == "en" ? 80.calcvaluex(): 60.calcvaluex())
        setHeaderLabel(label: collectstatuslabel, text: "收費狀況".localized,width: 80.calcvaluex())
        setHeaderLabel(label: totallabel, text: "總價".localized)
       
        extra_spacer.constrainWidth(constant: 72.calcvaluex())
        //extra_spacer.isHidden = true
        headerStackview.addArrangedSubview(extra_spacer)
        stackview.addArrangedSubview(headerStackview)
        
        stackview.addArrangedSubview(inventory_vert_stack)
        stackview.addArrangedSubview(seperator)
        seperator.constrainHeight(constant: 0.5.calcvaluey())
        seperator.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        addExtraButton.constrainWidth(constant: 42.calcvaluex() + (addExtraButton.label.text?.widthOfString(usingFont: addExtraButton.label.font) ?? 0))
       
        buttonView.addArrangedSubview(addExtraButton)
        
        buttonView.addArrangedSubview(UIView())
        stackview.addArrangedSubview(buttonView)
        addExtraButton.addButton.addTarget(self, action: #selector(addItem), for: .touchUpInside)
        addExtraButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addItem)))
        
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        
        self.layoutIfNeeded()
        if mode == .Review{
            buttonView.isHidden = true
            extra_spacer.isHidden = true
        }
        else{
            buttonView.isHidden = false
            extra_spacer.isHidden = false
        }
            let arr = data.components
       // print(arr.count,inventory_vert_stack.arrangedSubviews.count)
//            if inventory_vert_stack.arrangedSubviews.count == arr.count {
//
//                return
//            }
        if inventory_vert_stack.arrangedSubviews.count != arr.count {
            //v_stackview.arrangedSubviews.forEach { $0.removeFromSuperview() }
            inventory_vert_stack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        }
           //inventory_vert_stack.safelyRemoveArrangedSubviews()
        for (index,i) in arr.enumerated() {
            if inventory_vert_stack.arrangedSubviews.count != arr.count {
                let dt = InventoryListView()
                inventory_vert_stack.addArrangedSubview(dt)
            dt.tag = index
            
                dt.con = self
            dt.setData(data: data)
                
            }
            else{
                if let vv = inventory_vert_stack.arrangedSubviews[index] as? InventoryListView {
                    vv.tag = index
                    vv.con = self
                    vv.setData(data: data)
                }
            }

               
                
            }
        

        

    }

    @objc func addItem(){
        let vd = AddServiceComponentController()
        
        vd.modalPresentationStyle = .overCurrentContext
        vd.con = self
        General().getTopVc()?.present(vd, animated: false, completion: nil)
    }
    func setHeaderLabel(label:UILabel,text:String,width:CGFloat = 0) {
       // label.backgroundColor = .red
        label.textAlignment = .center
        label.text = text
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        if width != 0{
            label.constrainWidth(constant: width)
        }
        headerStackview.addArrangedSubview(label)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
