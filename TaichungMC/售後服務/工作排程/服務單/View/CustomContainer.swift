//
//  CustomContainer.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2024/7/21.
//  Copyright © 2024 TaichungMC. All rights reserved.
//

import UIKit
class CustomContainer: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15
        layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setupLayers()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayers()
    }

    private func setupLayers() {
            // Content layer (white background and corner radius)
//            let contentLayer = CALayer()
//            contentLayer.frame = bounds
//            contentLayer.backgroundColor = UIColor.white.cgColor
//            contentLayer.cornerRadius = 15 // Adjust corner radius as needed
//            layer.addSublayer(contentLayer)

            // Shadow layer for top, left, and right sides
            let shadowLayer = CALayer()
            shadowLayer.frame = bounds
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowOpacity = 0.08
            shadowLayer.shadowOffset = CGSize(width: 0, height: -3) // Top shadow
            shadowLayer.shadowRadius = 3

            // Create a path for the shadow to appear only on top, left, and right sides with rounded corners
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 10, y: 0)) // Start slightly below the top-left corner
        shadowPath.addLine(to: CGPoint(x: bounds.maxX - 10, y: 0)) // Top-right corner
        shadowPath.addLine(to: CGPoint(x: bounds.maxX - 10, y: bounds.maxY)) // Bottom-right corner
        shadowPath.addLine(to: CGPoint(x: 10, y: bounds.maxY)) // Bottom-left corner
        shadowPath.close()

        // Append rounded corners to the shadow path
        let roundedCornersPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10)
        shadowPath.append(roundedCornersPath)

            shadowLayer.shadowPath = shadowPath.cgPath

            layer.insertSublayer(shadowLayer, at: 0) //Insert shadow layer behind content layer
        
                    let contentLayer = CALayer()
                    contentLayer.frame = bounds
                    contentLayer.backgroundColor = UIColor.white.cgColor
                    contentLayer.cornerRadius = 15
        contentLayer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]// Adjust corner radius as needed
                    layer.insertSublayer(contentLayer, above: shadowLayer)
        }
}
