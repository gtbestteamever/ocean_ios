//
//  ServiceFormCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

extension UITableViewCell {
    var tableView: UITableView? {
        return parentView(of: UITableView.self)
    }
}
class Horizontal_Stackview : UIStackView {
    init(distribution:UIStackView.Distribution = .fill,spacing : CGFloat = 0, alignment: UIStackView.Alignment = .fill) {
        super.init(frame: .zero)
        self.distribution = distribution
        self.spacing = spacing
        self.alignment = alignment
        self.axis = .horizontal
        
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class Vertical_Stackview : UIStackView {
    init(distribution:UIStackView.Distribution = .fill,spacing : CGFloat = 0, alignment: UIStackView.Alignment = .fill) {
        super.init(frame: .zero)
        self.distribution = distribution
        self.spacing = spacing
        self.alignment = alignment
        self.axis = .vertical
        
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceFormCell : UITableViewCell {
    let container = UIView()
    let stackview = Vertical_Stackview(spacing:20.calcvaluey())
    var data : ServiceForm?
    var anchor : AnchoredConstraints?
    var mode : ServiceMode?
    var isReassign = false
    func maskConer(mask:CACornerMask) {
        container.layer.maskedCorners = mask
    }
    let blueLine = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = .white
        
        container.layer.cornerRadius = 15.calcvaluex()
        
       
        container.addSubview(stackview)
        anchor = stackview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 26.calcvaluey(), right: 36.calcvaluex()))

       
        blueLine.backgroundColor = #colorLiteral(red: 0.02700095996, green: 0.002926921472, blue: 0.2853620648, alpha: 1)
        container.addSubview(blueLine)
        blueLine.anchor(top: nil, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 4.calcvaluex()),size: .init(width: 0, height: 3.calcvaluey()))
        
        contentView.addSubview(container)
        container.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        
        
            
        
    }
    

    func setData(data:ServiceForm) {
        self.data = data
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ServiceFormFirstSectionCell : ServiceFormCell,FormTextViewDelegate {
    func textViewReloadHeight(height: CGFloat) {
        UIView.performWithoutAnimation {
            self.tableView?.beginUpdates()
            self.tableView?.endUpdates()
        }

    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        switch mode {
        case .ContactAdress:
            data?.customer.address = text
        case .ContactPhone:
            data?.customer.phone = text
        case .ContactName:
            data?.customer.contact = text
        default:
            ()
        }
    }
    
    let idField = NormalTextView(text: "客戶編號".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let companyNameField = NormalTextView(text: "客戶名稱".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let contactNameField = NormalTextView(text: "聯絡人".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let contactPhoneField = NormalTextView(text: "電話".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let addressField = NormalTextView(text: "地址".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let horizontalStackview = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
        idField.constrainWidth(constant: 200.calcvaluex())
        contactNameField.makeImportant()
        contactPhoneField.makeImportant()
        addressField.makeImportant()
        horizontalStackview.addArrangedSubview(idField)
        horizontalStackview.addArrangedSubview(companyNameField)
        stackview.addArrangedSubview(horizontalStackview)
        let horizontalStackview2 = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
        horizontalStackview2.addArrangedSubview(contactNameField)
        horizontalStackview2.addArrangedSubview(contactPhoneField)
        contactNameField.constrainWidth(constant: 200.calcvaluex())
       
       stackview.addArrangedSubview(horizontalStackview2)
        
        stackview.addArrangedSubview(addressField)
        
        idField.textfield.mode = .InitalField
        idField.isUserInteractionEnabled = false
        companyNameField.textfield.mode = .InitalField
        companyNameField.isUserInteractionEnabled = false
        idField.textfield.t_delegate = self
        companyNameField.textfield.t_delegate = self
        
        contactNameField.textfield.mode = .ContactName
        contactPhoneField.textfield.mode = .ContactPhone
        addressField.textfield.mode = .ContactAdress
        contactNameField.textfield.t_delegate = self
        contactPhoneField.textfield.t_delegate = self
        addressField.textfield.t_delegate = self
        stackview.layoutIfNeeded()
    }

    override func setData(data: ServiceForm) {
        super.setData(data: data)
        self.layoutIfNeeded()
            let customer = data.customer
            idField.textfield.text = customer.id
            companyNameField.textfield.text = customer.name
            contactNameField.textfield.text = customer.contact
            contactPhoneField.textfield.text = customer.phone
            addressField.textfield.text = customer.address
        
//        idField.textfield.textViewDidChange(idField.textfield)
//        companyNameField.textfield.textViewDidChange(companyNameField.textfield)
//        contactNameField.textfield.textViewDidChange(contactNameField.textfield)
//        contactPhoneField.textfield.textViewDidChange(contactPhoneField.textfield)
//        addressField.textfield.textViewDidChange(addressField.textfield)
        
        if mode == .Review {
        [contactNameField,contactPhoneField,addressField].forEach { (nt) in
            nt.isUserInteractionEnabled = false
        }
        }
        else{
            [contactNameField,contactPhoneField,addressField].forEach { (nt) in
                nt.isUserInteractionEnabled = true
            }
        }
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ServiceFormSecondSectionCell : ServiceFormCell,FormTextViewDelegate {
    func textViewReloadHeight(height: CGFloat) {
        UIView.performWithoutAnimation {
           
            tableView?.beginUpdates()
            tableView?.endUpdates()
        }

    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .Initial {
            data?.machine.description = text
        }
        else if mode == .Reason {
            data?.machine.initial_reason = text
        }
        else if mode == .Method {
            data?.machine.method = text
        }
    }
    
    let machineNoField = NormalTextView(text: "機號".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let machineTypeField = NormalTextView(text: "機種".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let insuranceField = NormalTextView(text: "保固狀況".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let siteDescriptionField = NormalTextView(text: "現況描述".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let reasonField = NormalTextView(text: "原因初判".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let methodField = NormalTextView(text: "採取對策".localized, placeholdertext: "",color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let checkField = InfoCheckView(text: "維修內容".localized,color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let horizontalStackview = Horizontal_Stackview(distribution: .fillEqually,spacing:12.calcvaluex(),alignment: .top)
        siteDescriptionField.makeImportant()
        reasonField.makeImportant()
        methodField.makeImportant()
        machineNoField.isUserInteractionEnabled = false
        machineTypeField.isUserInteractionEnabled = false
        insuranceField.isUserInteractionEnabled = false
        machineNoField.textfield.mode = .InitalField
        machineTypeField.textfield.mode = .InitalField
        insuranceField.textfield.mode = .InitalField
        machineNoField.textfield.t_delegate = self
        machineTypeField.textfield.t_delegate = self
        machineNoField.textfield.t_delegate = self
        horizontalStackview.addArrangedSubview(machineNoField)
        horizontalStackview.addArrangedSubview(machineTypeField)
        horizontalStackview.addArrangedSubview(insuranceField)
        stackview.addArrangedSubview(horizontalStackview)
        stackview.addArrangedSubview(siteDescriptionField)
        stackview.addArrangedSubview(reasonField)
        stackview.addArrangedSubview(methodField)
        stackview.addArrangedSubview(checkField)
        
        siteDescriptionField.textfield.t_delegate = self
        reasonField.textfield.t_delegate = self
        methodField.textfield.t_delegate = self
        
        siteDescriptionField.textfield.mode = .Initial
        reasonField.textfield.mode = .Reason
        methodField.textfield.mode = .Method
//        idField.textfield.mode = .Maintain
//        idField.isUserInteractionEnabled = false
//        companyNameField.textfield.mode = .Maintain
//        companyNameField.isUserInteractionEnabled = false
//        idField.textfield.t_delegate = self
//        companyNameField.textfield.t_delegate = self
//
//        contactNameField.textfield.t_delegate = self
//        contactPhoneField.textfield.t_delegate = self
//        addressField.textfield.t_delegate = self
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        self.layoutIfNeeded()
        checkField.data = data
        machineNoField.textfield.text = data.machine.code
        machineTypeField.textfield.text = data.machine.type
        insuranceField.textfield.text = data.machine.maintainStatus
        siteDescriptionField.textfield.text = data.machine.description
        reasonField.textfield.text = data.machine.initial_reason
        methodField.textfield.text = data.machine.method
//machineNoField,machineTypeField,insuranceField,
        //,siteDescriptionField,reasonField,methodField
        [machineNoField,machineTypeField,insuranceField,siteDescriptionField,reasonField,methodField].forEach { (nt) in
            //nt.textfield.textViewDidChange(nt.textfield)
         }
        
        if mode == .Review {
            [machineNoField,machineTypeField,insuranceField,siteDescriptionField,reasonField,methodField,checkField].forEach { (nt) in
                nt.isUserInteractionEnabled = false
             }
        }
        else{
            [siteDescriptionField,reasonField,methodField,checkField].forEach { (nt) in
                nt.isUserInteractionEnabled = true
             }
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
