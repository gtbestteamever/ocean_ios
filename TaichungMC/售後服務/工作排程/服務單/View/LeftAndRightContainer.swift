//
//  LeftAndRightContainer.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2024/7/21.
//  Copyright © 2024 TaichungMC. All rights reserved.
//

import UIKit
class LeftAndRightContainer: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //setupLayers()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // setupLayers()
    }

    private func setupLayers() {
        // Content layer (white background)


        // Shadow layer for left and right sides
        let shadowLayer = CALayer()
        shadowLayer.frame = bounds
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowOpacity = 0.08
        shadowLayer.shadowOffset = CGSize(width: -3, height: 0) // Left shadow
        shadowLayer.shadowRadius = 3

        // Create a path for the shadow to appear only on left and right sides
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 0, y: 0)) // Top-left corner
        shadowPath.addLine(to: CGPoint(x: bounds.maxX, y: 0)) // Top-right corner
        shadowPath.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY)) // Bottom-right corner
        shadowPath.addLine(to: CGPoint(x: 0, y: bounds.maxY)) // Bottom-left corner
        shadowPath.close()

        shadowLayer.shadowPath = shadowPath.cgPath

        layer.addSublayer(shadowLayer)
        
        let contentLayer = CALayer()
        contentLayer.frame = bounds
        contentLayer.backgroundColor = UIColor.white.cgColor
        layer.insertSublayer(contentLayer, above: shadowLayer)// Add shadow layer directly to the view
    }
}
