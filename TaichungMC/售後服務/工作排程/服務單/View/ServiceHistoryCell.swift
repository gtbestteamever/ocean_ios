//
//  ServiceHistoryCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/7/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class ServiceHistoryCell : UITableViewCell {
    let container = UIView()
    let selectionView = UIView()
    let headerLabel : UILabel = {
       let label = UILabel()
        label.numberOfLines = 2
        label.textColor = MajorColor().oceanlessColor
        label.font = UIFont(name: MainFont().Medium, size: 18.calcvaluex())
        return label
    }()
    let subLabel : UILabel = {
        let label = UILabel()
        label.textColor = MajorColor().oceanlessColor
        label.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        return label
    }()
    let statusLabel : PaddedLabel2 = {
       let label = PaddedLabel2()
        if UserDefaults.standard.getConvertedLanguage() == "en" {
            label.constrainWidth(constant: 100.calcvaluex())
        }
        else{
            label.constrainWidth(constant: 72.calcvaluex())
        }
        label.constrainHeight(constant: 30.calcvaluey())
        
        label.layer.cornerRadius = 15.calcvaluey()
        label.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        label.layer.borderWidth = 1.calcvaluex()
        label.textAlignment = .center
        return label
    }()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 1.calcvaluey(), left: 1.calcvaluex(), bottom: 1.calcvaluey(), right: 1.calcvaluex()))
        container.layer.cornerRadius = 15.calcvaluex()
        container.backgroundColor = .white
        container.addshadowColor()
        
        selectionView.backgroundColor = .white
        selectionView.layer.cornerRadius = 15.calcvaluex()
        container.addSubview(selectionView)
        selectionView.fillSuperview(padding: .init(top: 0, left: 0, bottom: 0, right: 0))
//        if #available(iOS 11.0, *) {
//            selectionView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        container.addSubview(imageview)
        imageview.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 8.calcvaluex()),size: .init(width: 15.calcvaluex(), height: 15.calcvaluex()))
        imageview.centerYInSuperview()
        imageview.isHidden = true
        let h_stack = Horizontal_Stackview(spacing:10.calcvaluex(),alignment: .center)
        h_stack.addArrangedSubview(statusLabel)
        h_stack.addArrangedSubview(headerLabel)
        
        let v_stack = Vertical_Stackview(spacing:6.calcvaluey())
        v_stack.addArrangedSubview(h_stack)
        v_stack.addArrangedSubview(subLabel)
        
        selectionView.addSubview(v_stack)
        v_stack.anchor(top: selectionView.topAnchor, leading: selectionView.leadingAnchor, bottom: selectionView.bottomAnchor, trailing: imageview.leadingAnchor,padding: .init(top: 8.calcvaluey(), left: 22.calcvaluex(), bottom: 16.calcvaluey(), right: 4.calcvaluex()))
       // v_stack.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 22.calcvaluex(), bottom: 16.calcvaluey(), right: 8.calcvaluex()))
        

    }
    func didSelect(){
        imageview.isHidden = false
        //container.backgroundColor = #colorLiteral(red: 0.9296925068, green: 0.421582222, blue: 0.0175667014, alpha: 1)
    }
    func unSelect(){
        imageview.isHidden = true
        //container.backgroundColor = .white
    }
    func setData(data:ServiceFormList) {
        let status = GetStatus().getUpkeepOrderStatus()?.statuses
        
        if let first = status?.first(where: { (st) -> Bool in
            return st.key == data.status
        }) {
            statusLabel.text = first.getString()
            statusLabel.textColor = UIColor().hexStringToUIColor(hex: first.color ?? "")
            statusLabel.layer.borderColor = UIColor().hexStringToUIColor(hex: first.color ?? "").cgColor
        }
        
        headerLabel.text = data.owner?.name
        subLabel.text = Date().convertToDateComponent(text: data.created_at ?? "", format: "yyyy-MM-dd HH:mm:ss", addEight: false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
