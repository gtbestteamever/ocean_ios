//
//  ServiceEngineerCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class engineerView : UIView,FormTextViewDelegate,SignitureControllerDelegate {
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    func sendImage(image:UIImage) {
        let uuid = "\(UUID().uuidString).jpg"
        print(3212,uuid)
        let n_data = image.jpegData(compressionQuality: 0.1)
        writeDataToFolder(data: n_data, name: uuid)
        data?.engineers[self.tag].signiture.path = uuid
        data?.engineers[self.tag].signiture.signiture = image
       // data?.timeAndDuration[self.tag].signiture.path = uuid
       // data?.timeAndDuration[self.tag].signiture.signiture = image
        self.con?.tableView?.reloadData()
    }
    var mode : ServiceMode?
    func textViewReloadHeight(height: CGFloat) {
        print(889,height)
        UIView.performWithoutAnimation {
            
            con?.tableView?.beginUpdates()
            con?.tableView?.endUpdates()
        }
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        switch mode {
        case .EngineerId:
            data?.engineers[self.tag].id = text
        case .EngineerError:
            data?.engineers[self.tag].errorNo = text
        case .EngineerCar:
            data?.engineers[self.tag].car = text
        case .EngineerName:
            data?.engineers[self.tag].name = text
        case .EngineerTest:
            data?.engineers[self.tag].test = text
        case .EngineerPoint:
            data?.engineers[self.tag].point = text
        case .EngineerOther:
            data?.engineers[self.tag].other = text
        case .EngineerDesign:
            data?.engineers[self.tag].design = text
        case .EngineerInsurance:
            data?.engineers[self.tag].insurance = text
        case .EngineerDuration:
            data?.engineers[self.tag].duration = text
        case .EngineerSpecial:
            data?.engineers[self.tag].special = text
        default:
        ()
        }
    }
    
    let idField = NormalTextView(text: "工程師工號".localized, placeholdertext: "請輸入工程師工號".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let nameField = NormalTextView(text: "工程師姓名".localized, placeholdertext: "請輸入工程師姓名".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let errorField = NormalTextView(text: "不良報告書編號".localized, placeholdertext: "請輸入不良報告書編號".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let pointField = NormalTextView(text: "服務點數".localized, placeholdertext: "請輸入服務點數".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let insuranceField = NormalTextView(text: "保證期間".localized, placeholdertext: "請輸入保證期間".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let carField = NormalTextView(text: "交車".localized, placeholdertext: "請輸入交車".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let durationField = NormalTextView(text: "工件估時".localized, placeholdertext: "請輸入工件估時".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let designField = NormalTextView(text: "程式設計".localized, placeholdertext: "請輸入程式設計".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let testField = NormalTextView(text: "試車工件".localized, placeholdertext: "請輸入試車工件".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let specialField = NormalTextView(text: "專案處理".localized, placeholdertext: "請輸入專案處理".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let otherField = NormalTextView(text: "其他".localized, placeholdertext: "請輸入其他".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let xbutton = UIButton()
    
    var data : ServiceForm?
    var arr = [NormalTextView]()
    let addSignView = AddSignView()
    let signView = FormSignView()
    func setData(data:ServiceForm) {
        self.data = data
        self.layoutIfNeeded()
        let engineer = data.engineers[self.tag]
        idField.textfield.text = engineer.id
        nameField.textfield.text = engineer.name
        pointField.textfield.text = engineer.point
        errorField.textfield.text = engineer.errorNo
        insuranceField.textfield.text = engineer.insurance
        carField.textfield.text = engineer.car
        durationField.textfield.text = engineer.duration
        designField.textfield.text = engineer.design
        testField.textfield.text = engineer.test
        specialField.textfield.text = engineer.special
        otherField.textfield.text = engineer.other
        if let image = engineer.signiture.signiture {
        
                    addSignView.isHidden = true
                    signView.isHidden = false
                    signView.imgv.image = image
                    signView.xbutton.isHidden = false
                }
        else if let path = engineer.signiture.path_url,let  url = URL(string: path) {
            addSignView.isHidden = true
            signView.isHidden = false
            signView.xbutton.isHidden = true
            url.getThumnailImageCom { (imag) in
                self.signView.imgv.image = imag
            }
            //signView.imgv.image = url.getThumnailImage()
        }
                else{
                    signView.isHidden = true
                    signView.imgv.image = nil
                    addSignView.isHidden = false
        
                }
    
        
       // setDidChange(arr: arr)
        
        if mode == .Review {
            arr.forEach { (nt) in
                nt.isUserInteractionEnabled = false
                nt.textfield.placeholderLabel.text = ""
            }
            xbutton.isHidden = true
            
            signView.xbutton.isHidden = true
            if engineer.signiture.path_url == nil{
                addSignView.isHidden = true
                signView.isHidden = true
            }
        }
        else{
            xbutton.isHidden = false
        }
    }
    func setDidChange(arr:[NormalTextView]){
        for i in arr{
           // i.textfield.textViewDidChange(i.textfield)
        }
       
    }
    @objc func removeSign(){
        data?.engineers[self.tag].signiture.signiture = nil
        data?.engineers[self.tag].signiture.path = nil
        data?.engineers[self.tag].signiture.path_url = nil
        
        if let id = data?.engineers[self.tag].id {
            data?.engineers[self.tag].del_files.append(id)
        }
        self.con?.tableView?.reloadData()
    }
    weak var con : ServiceFormCell?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        arr = [idField,nameField,pointField,errorField,
               insuranceField,carField,durationField,designField,
               testField,specialField,otherField]
        arr.forEach { (nt) in
            nt.textfield.t_delegate = self
        }
        errorField.textfield.mode = .EngineerError
        idField.textfield.mode = .EngineerId
        nameField.textfield.mode = .EngineerName
        pointField.textfield.mode = .EngineerPoint
        insuranceField.textfield.mode = .EngineerInsurance
        carField.textfield.mode = .EngineerCar
        durationField.textfield.mode = .EngineerDuration
        designField.textfield.mode = .EngineerDesign
        testField.textfield.mode = .EngineerTest
        specialField.textfield.mode = .EngineerSpecial
        otherField.textfield.mode = .EngineerOther
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        xbutton.constrainWidth(constant: 32.calcvaluex())
        xbutton.constrainHeight(constant: 32.calcvaluex())
        let v_stack = Vertical_Stackview(spacing:22.calcvaluey())
        let h_stack = Horizontal_Stackview(distribution:.fillEqually,spacing:12.calcvaluex(),alignment: .top)
        h_stack.addArrangedSubview(idField)
        idField.makeImportant()
        h_stack.addArrangedSubview(nameField)
        nameField.makeImportant()
        pointField.isHidden = true
        h_stack.addArrangedSubview(pointField)
        pointField.makeImportant()
        
        let vv_stack = Vertical_Stackview(spacing:22.calcvaluey())
        
        let hh_stack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
        vv_stack.addArrangedSubview(h_stack)
        
        let ht = [[errorField,insuranceField],[carField,durationField],[designField,testField],[specialField,otherField]]
        for j in ht{
            let h_stack = Horizontal_Stackview(distribution:.fillEqually,spacing: 12.calcvaluex(),alignment: .top)
            for k in j {
               
                h_stack.addArrangedSubview(k)
            }
            h_stack.isHidden = true
            vv_stack.addArrangedSubview(h_stack)
        }
        hh_stack.addArrangedSubview(vv_stack)
        hh_stack.addArrangedSubview(xbutton)
        
        v_stack.addArrangedSubview(hh_stack)
        let n_stack = Horizontal_Stackview()
        n_stack.addArrangedSubview(addSignView)
        n_stack.addArrangedSubview(signView)
        signView.isHidden = true
        addSignView.addButton.addTarget(self, action: #selector(goAddSignature), for: .touchUpInside)
        
        signView.xbutton.addTarget(self, action: #selector(removeSign), for: .touchUpInside)
        n_stack.addArrangedSubview(UIView())
        v_stack.addArrangedSubview(n_stack)
        let seperator = UIView()
        
        seperator.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        seperator.constrainHeight(constant: 1.calcvaluey())
        v_stack.addArrangedSubview(seperator)
        addSubview(v_stack)
        v_stack.fillSuperview()
        xbutton.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
    }
    @objc func goAddSignature(){
        let con = SignitureController()
        con.modalPresentationStyle = .fullScreen
        con.delegate = self
        
        General().getTopVc()?.present(con, animated: true, completion: nil)
    }
    @objc func removeItem(){
       let ct = data?.engineers.remove(at: self.tag)
        if let id = ct?.pre_id {
            data?.del_engineers.append(id)
        }
        if let con = con{
            con.tableView?.reloadSections(IndexSet(integer: con.tag ), with: .none)
        }
        

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ServiceEngineerCell: ServiceFormCell {
    let addButton = addExtraField(text: "新增工程確認".localized)
    //let engView = engineerView()
    let v_stackview  = Vertical_Stackview(spacing:20.calcvaluey())
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //stackview.addArrangedSubview(engView)
        stackview.addArrangedSubview(v_stackview)
        stackview.addArrangedSubview(addButton)
        addButton.constrainWidth(constant: 42.calcvaluex() + (addButton.label.text?.widthOfString(usingFont: addButton.label.font) ?? 0))
        addButton.addButton.addTarget(self, action: #selector(addItem), for: .touchUpInside)
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addItem)))
    }
    @objc func addItem(){
        data?.engineers.append(FormEngineer())
        
        self.tableView?.reloadSections(IndexSet(integer: self.tag), with: .none)
        self.tableView?.scrollToRow(at: IndexPath(row: 0, section: self.tag), at: .bottom, animated: true)
    }
    override func setData(data: ServiceForm) {
        super.setData(data: data)
        if mode == .Review {
            addButton.isHidden = true
        }
        else{
            addButton.isHidden = false
        }
        if v_stackview.arrangedSubviews.count != data.engineers.count {
            v_stackview.safelyRemoveArrangedSubviews()
            for (index,i) in data.engineers.enumerated() {
            let engView = engineerView()
                engView.mode = self.mode
                engView.tag = index
                
                engView.con = self
            engView.setData(data: data)
//            engView.idField.textfield.text = i.id
//            engView.nameField.textfield.text = i.name
            v_stackview.addArrangedSubview(engView)
            
        }
        }
        else{
            for (index,i) in v_stackview.arrangedSubviews.enumerated() {
                if let v = i as? engineerView {
                    v.mode = self.mode
                    v.tag = index
                    v.con = self
                    
                    v.setData(data: data)
                }
            }
        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
