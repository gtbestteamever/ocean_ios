//
//  InfoCheckView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2024/7/22.
//  Copyright © 2024 TaichungMC. All rights reserved.
//

import UIKit

enum CheckMode : String{
    case Direct = "direct"
    case Change = "change_parts"
    case Return = "return_for_repair"
    case Purchase = "purchase_parts"
    case Other = "other"
}
class InfoCheckView:UIView {

    let tlabel = UILabel()
    
    var color = UIColor.white
    var isImportant = false
//    var mode : InterviewMode? {
//        didSet{
//            if mode == .Review{
//                textfield.placeholderLabel.text = ""
//            }
//        }
//    }
    func makeImportant(){
        isImportant = true
        let mutablestring = NSMutableAttributedString(string: "* ", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.8249840736, green: 0.1374278367, blue: 0.1237704381, alpha: 1)])
        
        mutablestring.append(NSAttributedString(string: tlabel.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:self.color]))
        tlabel.attributedText = mutablestring
    }
    let directFix = CheckView(text: "直接維修".localized, mode: .Direct)
    let changeComponent = CheckView(text: "更換零件".localized,mode: .Change)
    let returnComponent = CheckView(text: "退修".localized,mode: .Return)
    let otherComponent = CheckView(text: "另購零件".localized,mode: .Purchase)
    let otherFix = CheckView(text:"其他".localized,mode: .Other)
    var data: ServiceForm? {
        didSet{
            for i in [directFix,changeComponent,returnComponent,otherComponent,otherFix] {
                if data?.machine.selection.contains(i.mode) == true {
                    i.isCheck()
                }
                else{
                    i.unCheck()
                }
            }
        }
    }
    @objc func checkSelect(tap:UITapGestureRecognizer) {
        let vd = tap.view as? CheckView
        print(vd?.mode)
        for i in [directFix,changeComponent,returnComponent,otherComponent,otherFix] {
            if i.mode == vd?.mode {
                if i.isSelected {
                    i.unCheck()
                    data?.machine.selection.removeAll(where: { md in
                        return md == i.mode
                    })
                }
                else{
                    i.isCheck()
                    data?.machine.selection.append(i.mode)
                    
                }
            }
        }
        print(data?.machine.selection.count)
    }
    init(text:String,placeholdertext:String? = nil,color:UIColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)) {
        super.init(frame: .zero)
        self.color = color
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        tlabel.text = text
        tlabel.textColor = self.color
        
        let hstack = Horizontal_Stackview(distribution: .fillEqually, spacing:32.calcvaluex(),alignment: .top)
        hstack.addArrangedSubview(directFix)
        directFix.tag = 0
        hstack.addArrangedSubview(changeComponent)
        changeComponent.tag = 1
        hstack.addArrangedSubview(returnComponent)
        returnComponent.tag = 2
        hstack.addArrangedSubview(otherComponent)
        otherComponent.tag = 3
        hstack.addArrangedSubview(otherFix)
        otherFix.tag = 4
        
        directFix.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkSelect)))
        changeComponent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkSelect)))
        returnComponent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkSelect)))
        otherComponent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkSelect)))
        otherFix.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkSelect)))
        addSubview(hstack)
        hstack.anchor(top: tlabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class CheckView : UIView {
    let imageview = UIImageView(image: UIImage(named: "btn_check_box_normal")?.withRenderingMode(.alwaysOriginal))
    let label : UILabel = {
       let label = UILabel()
        label.text = "ABC"
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        return label
    }()
    var isSelected = false
    func isCheck(){
        imageview.image = UIImage(named: "btn_check_box_pressed")?.withRenderingMode(.alwaysTemplate)
        imageview.tintColor = MajorColor().oceanColor
        label.textColor = MajorColor().oceanColor
        isSelected = true
    }
    
    func unCheck(){
        imageview.image = UIImage(named: "btn_check_box_normal")?.withRenderingMode(.alwaysOriginal)
        label.textColor = .lightGray
        isSelected = false
    }
    var mode = CheckMode.Change
    init(text:String,mode:CheckMode) {
        super.init(frame: .zero)
        self.mode = mode
        label.text = text
        addSubview(imageview)
        imageview.contentMode = .scaleToFill
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        
        addSubview(label)
        label.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: 24.calcvaluey()).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
