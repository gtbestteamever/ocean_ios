//
//  SurveyController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
import JGProgressHUD
class SurveyController : SampleController {

    var survey_id : String? {
        didSet{
            sendButton.isHidden = true
            self.surveyView.survey_id = survey_id
        }
    }
    var id : String?
    let container = UIView()
    let surveyView = SurveyView()
    let sendButton = AddButton4()
    var isEdit = true
    weak var con : ServicePreviewController?
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        titleview.label.text = "顧客滿意度調查".localized
        
        
        container.addSubview(surveyView)
        surveyView.anchor(top: topview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 25.calcvaluex(), bottom: 26.calcvaluey(), right: 25.calcvaluex()))

        sendButton.titles.text = "送出".localized
        sendButton.backgroundColor = MajorColor().oceanlessColor
        extrabutton_stackview.addArrangedSubview(sendButton)
        sendButton.addTarget(self, action: #selector(sendData), for: .touchUpInside)
        
//        if let s_id = survey_id {
//            surveyView.serviceField.isUserInteractionEnabled = false
//            surveyView.mannerField.isUserInteractionEnabled = false
//            surveyView.fixField.isUserInteractionEnabled = false
//            surveyView.innerField.isUserInteractionEnabled = false
//            surveyView.moreCommentField.isUserInteractionEnabled = false
//            surveyView.moreCommentField.textfield.placeholderLabel.text = ""
//          //  surveyView.signView.addSignView.isHidden = true
//           // surveyView.signView.signView.isHidden = false
//
//            isEdit = false
//            sendButton.isHidden = true
//           // surveyView.signView.signView.xbutton.isHidden = true
//            fetchApi()
//        }
        
        surveyView.fetchApi()
    }

    @objc func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/order/satisfaction/\(survey_id ?? "")", decoderType: SurveyModelData.self) { (json) in
            DispatchQueue.main.async {
                if let json = json?.data {
                    self.surveyView.serviceField.selectedIndex = json.professional_radio - 1
                    self.surveyView.mannerField.selectedIndex = json.polite_radio - 1
                    self.surveyView.fixField.selectedIndex = json.efficiency_radio - 1
                    self.surveyView.innerField.selectedIndex = json.arrange_radio - 1
                    self.surveyView.moreCommentField.textfield.text = json.description
                   // self.surveyView.moreCommentField.textfield.textViewDidChange(self.surveyView.moreCommentField.textfield)

                    if let file = json.files {
                        switch file {
                        case .string(let x):
                            let ft = x.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "").replacingOccurrences(of: "\\", with: "")
                            
                            guard let url = URL(string: "\(AppURL().baseURL)uploads/\(ft)") else{return}
                            print(url.absoluteString)
                            //self.surveyView.signView.signView.imgv.image = url.getThumnailImage()
                        case .innerItem(let f) :
                            if let ft = f.first, let url = URL(string: "\(AppURL().baseURL)uploads/\(ft)") {
                               // self.surveyView.signView.signView.imgv.image = url.getThumnailImage()
                            }
                    }
                    }
                    
                }
            }

        }
    }
    func showAlert(text:String){
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    func checkData() -> Bool {
        
        for i in surveyView.v_stack.arrangedSubviews {
            if let v = i as? circularSelectionContainer , v.isImportant && v.selectedIndex == nil{
                self.showAlert(text: "請選擇\(v.top_text)")
                return false
            }
            else if let s = i as? NormalTextView, s.isImportant && s.textfield.text == "" {
                self.showAlert(text: "請輸入\(s.tlabel.text ?? "")")
                return false
            }
            else if let k = i as? SurveySignView, k.isImportant && k.data == nil {
                self.showAlert(text: "請\(k.label.text ?? "")")
                return false
            }
        }
        return true
    }
    func getParam() -> ([[String:Any]],[[String:Any]],[Data],[String],[String]){
        var data = [[String:Any]]()
        var file_data = [[String:Any]]()
        var files = [Data]()
        var exts = [String]()
        var name_arr = [String]()
        for i in surveyView.v_stack.arrangedSubviews {
            var ss = [String:Any]()
            var dt = [String:Any]()
            if let v = i as? circularSelectionContainer {
                ss["type"] = "1"
                ss["title"] = v.top_text
                ss["order"] = v.tag
                if let index = v.selectedIndex {
                    ss["value"] = index
                }
                
                
            }
            else if let v = i as? NormalTextView {
                ss["type"] = "2"
                ss["title"] = v.tlabel.text ?? ""
                ss["order"] = v.tag
                ss["value"] = v.textfield.text
                
            }
            
            else if let v = i as? SurveySignView {
                dt["type"] = "3"
                dt["title"] = v.label.text
                dt["order"] = v.tag
                if let data = v.data {
                    files.append(data)
                    exts.append(v.ext)
                    
                    name_arr.append("\(UUID().uuidString).\(v.ext)")
                }
            }
            if !ss.isEmpty {
                data.append(ss)
            }
            if !dt.isEmpty {
                file_data.append(dt)
            }
            
            
            
        }
        return (data,file_data,files,exts,name_arr)
    }
    @objc func sendData(){
        if !checkData() {
            
            return
        }
        
        
        let all_data = getParam()
        var form_data = [String:String]()
        guard let data = try? JSONSerialization.data(withJSONObject: all_data.0, options: .prettyPrinted) else {return}
        guard let file_data = try? JSONSerialization.data(withJSONObject: all_data.1, options: .prettyPrinted) else {return}
        
        form_data["data"] = String(data: data, encoding: .utf8)
        form_data["file_data"] = String(data: file_data, encoding: .utf8)
        
        let hud = JGProgressHUD()
        hud.textLabel.text = "上傳中...".localized
        hud.show(in: self.container)

        NetworkCall.shared.postCallMultipleData(parameter: "api-or/v1/upkeep/order/\(self.id ?? "")/satisfaction", param: form_data, data: all_data.2, dataParam: "files[]", dataName: all_data.4, memeString: all_data.3, decoderType: SurveyModel.self) { (json) in
            DispatchQueue.main.async {

                if let json = json?.data {

                print(881,json)
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                hud.textLabel.text = "上傳成功".localized
                hud.dismiss(afterDelay: 1, animated: true) {
                    if let con = self.con {
                        con.presentingViewController?.dismiss(animated: true, completion: nil)
                    }
                    else{
                        self.dismiss(animated: true, completion: nil)
                    }

                }



                }
            }
        }
//        NetworkCall.shared.postCallZip2(parameter: "api-or/v1/upkeep/order/\(self.id ?? "")/satisfaction", param: param, data: data, dataName: "\(UUID().uuidString)", dataParam: "files[]", memeString: "", decoderType: SurveyModelData.self) { (json) in
//
//
//        }
    }
}
class SurveySignView : UIView,SignitureControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate {
    var ext = ""
    func sendImage(image: UIImage) {
        ext = "jpeg"
        data = image.jpegData(compressionQuality: 0.1)
        addSignView.isHidden = true
        signView.isHidden = false
        signView.imgv.image = image
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        ext = url.pathExtension
        data = try? Data(contentsOf: url)
        addSignView.isHidden = true
        signView.isHidden = false
        signView.imgv.image = url.getThumnailImage()
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(991,info)
        if #available(iOS 11.0, *) {
            if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1), let path_url = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                if !FileManagerUility.checkIfFileSizeExceed(data: dt) {
                    let alertController = UIAlertController(title: "您的裝置的空間不足以上傳".localized, message: "請空出空間再上傳".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "確定".localized, style: .cancel, handler: nil)
                    alertController.addAction(action)
                    General().getTopVc()?.present(alertController, animated: true, completion: nil)
                    return
                }
                ext = "jpg"
                self.data = dt
                self.signView.isHidden = false
                self.addSignView.isHidden = true
                self.signView.imgv.image = img
                
                
                picker.dismiss(animated: true, completion: nil)
            }
        } else {
            // Fallback on earlier versions
        }

    }
    var data : Data?
    let label = UILabel()
    let h_stack = Horizontal_Stackview()
    let addSignView = AddSignView()
    let signView = FormSignView()
    var isImportant = false
    
    init(isImportant:Bool,text:String) {
        super.init(frame: .zero)
        self.isImportant = isImportant
        let attributedText = NSMutableAttributedString(string: isImportant == true ? "*" : "", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor: MajorColor().oceanColor])
        attributedText.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)]))
        label.attributedText = attributedText
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        
        addSubview(h_stack)
        
        h_stack.addArrangedSubview(addSignView)
        h_stack.addArrangedSubview(signView)
        h_stack.addArrangedSubview(UIView())
        signView.isHidden = true
        h_stack.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
        signView.xbutton.addTarget(self, action: #selector(removeSign), for: .touchUpInside)
        addSignView.addButton.addTarget(self, action: #selector(goAddSign), for: .touchUpInside)
        addSignView.grabButton.addTarget(self, action: #selector(importSign), for: .touchUpInside)
        
    }
    @objc func importSign(){
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
           documentPickerController.delegate = self
           documentPickerController.modalPresentationStyle = .fullScreen
            General().getTopVc()?.present(documentPickerController, animated: true, completion: nil)
            
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
            presenter.sourceView = addSignView.grabButton
            presenter.sourceRect = addSignView.grabButton.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(controller, animated: true, completion: nil)
    }
    func goCameraAlbumAction(type:Int) {
        let picker = UIImagePickerController()
        picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(picker, animated: true, completion: nil)
    }
    @objc func removeSign(){
        data = nil
        addSignView.isHidden = false
        signView.isHidden = true
        signView.imgv.image = nil
        ext = ""
    }
    @objc func goAddSign(){
        let vd = SignitureController()
        vd.modalPresentationStyle = .fullScreen
        vd.delegate = self
        
        General().getTopVc()?.present(vd, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SurveyView : UIView,FormTextViewDelegate {

    
    func textViewReloadHeight(height: CGFloat) {
        self.layoutIfNeeded()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        //
    }
    

    var survey_id : String?
    let v_stack = Vertical_Stackview(spacing: 36.calcvaluey(),alignment: .fill)
    let serviceField = circularSelectionContainer(isImportant: true, text: "1.對於服務人員的專業服務滿意度", array: ["非常滿意","滿意","尚可","不滿意","非常不滿意"])
    let mannerField = circularSelectionContainer(isImportant: true, text: "2.對於服務人員諮詢問題的措辭用語、禮貌度", array: ["非常滿意","滿意","尚可","不滿意","非常不滿意"])
    let fixField = circularSelectionContainer(isImportant: true, text: "3.叫修到維修完成的時效滿意度", array: ["非常滿意","滿意","尚可","不滿意","非常不滿意"])
    let innerField = circularSelectionContainer(isImportant: true, text: "4.對於台中精機顧客服務部，內、外勤整理服務滿意度", array: ["非常滿意","滿意","尚可","不滿意","非常不滿意"])
    let moreCommentField = NormalTextView(text: "5.對於台中精機的機台及整體支援須改善的整體意見", placeholdertext: "您的寶貴意見是我們努力的指標與前進的動力！", color: #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1))
    //let signView = SurveySignView()
    //var data : Data?
    var pre_data = [Survey_Data]()
    func fetchApi(){
        var urlString = ""
        if let id = survey_id {
            urlString = "api-or/v1/upkeep/order/satisfaction/\(id)"
        }
        else{
            urlString = "api-or/v1/upkeep/order/satisfaction/fields"
        }
        NetworkCall.shared.getCall(parameter: urlString, decoderType: SurveyModel.self) { (json) in
            DispatchQueue.main.async {
                if let json = json?.data {
                    self.pre_data = json.sorted(by: { (d1, d2) -> Bool in
                        if let or1 = d1.order, let or2 = d2.order {
                            return or1 < or2
                        }
                        return false
                    })
                    
                    self.createUI()
                }
            }
 
        }
    }
    func createUI(){
        let notEdit = survey_id == nil ? true : false
        for i in self.pre_data {
            
            switch i.type {
            case "1":
                
                let vd = circularSelectionContainer(isImportant: i.required == 1 ? true : false, text: i.title ?? "", array: ["非常滿意","滿意","尚可","不滿意","非常不滿意"])
                vd.tag = i.order ?? 0
                
                vd.isUserInteractionEnabled = notEdit
                if let val = i.value {
                    vd.selectedIndex = Int(val)
                }
                v_stack.addArrangedSubview(vd)
            case "2":
                let st = NormalTextView(text: i.title ?? "", placeholdertext: "", color: #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1))
                st.tag = i.order ?? 0
                if i.required == 1{
                    st.makeImportant()
                }
                st.isUserInteractionEnabled = notEdit
                st.textfield.initialHeight = 78.calcvaluey()
                st.textfield.anchorHeight?.constant = 78.calcvaluey()
                
                st.textfield.mode = .EngineerName
                st.textfield.t_delegate = self
                if let val = i.value {
                    st.textfield.text = val
                    //st.textfield.textViewDidChange(st.textfield)
                }
                v_stack.addArrangedSubview(st)
            case "3":
                let ff = SurveySignView(isImportant: i.required == 1 ? true : false, text: i.title ?? "")
                ff.tag = i.order ?? 0
                ff.isUserInteractionEnabled = notEdit
                ff.signView.isHidden = notEdit
                ff.signView.xbutton.isHidden = !notEdit
                ff.addSignView.isHidden = !notEdit
                if let val = i.value, let url = URL(string: val.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") {
                    ff.signView.imgv.image = url.getThumnailImage()
                }
                
 
                v_stack.addArrangedSubview(ff)
            default:
                ()
            }
            
        }
        v_stack.addArrangedSubview(UIView())
    }
    let label : UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        label.text = "維修後顧客滿意度調查".localized
        label.textColor = MajorColor().oceanlessColor
        
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 15.calcvaluex()
            backgroundColor = .white
        addshadowColor()
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 46.calcvaluex()))
        let scrollView = UIScrollView()
        scrollView.addSubview(v_stack)
        scrollView.showsVerticalScrollIndicator = false
        v_stack.fillSuperview()
        v_stack.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        //serviceField.stackview.addArrangedSubview(UIView())
//        v_stack.addArrangedSubview(serviceField)
//        v_stack.addArrangedSubview(mannerField)
//        v_stack.addArrangedSubview(fixField)
//        v_stack.addArrangedSubview(innerField)
//        v_stack.addArrangedSubview(moreCommentField)
//        v_stack.addArrangedSubview(signView)
//        v_stack.addArrangedSubview(UIView())
        
       

//        signView.signView.xbutton.addTarget(self, action: #selector(removeSign), for: .touchUpInside)
//        signView.addSignView.addButton.addTarget(self, action: #selector(goAddSign), for: .touchUpInside)
//        signView.addSignView.grabButton.addTarget(self, action: #selector(importSign), for: .touchUpInside)
        
        
        addSubview(scrollView)
        scrollView.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 26.calcvaluey(), right: 46.calcvaluex()))
        //scrollView.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 26.calcvaluey(), right: 46.calcvaluex()))
    }



    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
