//
//  ServiceFormHistoryController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/7/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import ZIPFoundation

extension ServiceFormHistoryController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex != indexPath.section {
            selectedIndex = indexPath.section
            tableView.reloadData()
            if selectedIndex == 0 {
             fetchOrigin()
            }
            else{
            fetchData()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "history", for: indexPath) as! ServiceHistoryCell
        if indexPath.section == selectedIndex {
            cell.didSelect()
        }
        else{
            cell.unSelect()
        }
        cell.setData(data:list[indexPath.section])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.calcvaluey()
    }
}
enum ServiceButton : String{
    case Preview = "預覽"
    case Completed = "結案"
    case Observing = "觀察中"
    case Working = "處理中"
    case Edit = "編輯"
    case fillSurvey = "填寫滿意度調查與簽名"
    case checkSurvey = "查看滿意度調查與簽名"
}
class ServiceFormHistoryController : SampleController {
    let container = UIView()
    var selectOrInput : Int = 0
    var id : String? {
        didSet{
            print(8819,id)
        }
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    let info_view = ServiceFormView()
    var list = [ServiceFormList]()
    var selectedIndex = 0
    let button_stack = Horizontal_Stackview(spacing:18.calcvaluex(),alignment: .bottom)
    var array : [ServiceButton] = []
    var button_mode : ServiceButton?
    var initialData : ServiceModel?
    @objc func goAction(sender:UIButton) {
        let mode = array[sender.tag]
        if mode == .Edit {
            let vd = ServiceFormController()
            vd.mode = .Edit
            if let data = initialData {
                let form_data = ServiceForm()
                form_data.selectOrInput = selectOrInput
                form_data.setData(form_data: data, mode: .Edit)
                vd.container.form_data = form_data
            }
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
        else if mode == .Preview{
            let vd = ServicePreviewController()
            print(1121,initialData?.export_file)
            if let file = initialData?.export_file?.pdf.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: file) {
                print(7771,file)
                url.loadWebview(webview: vd.webview)
            }
            vd.status = list.first?.status
            if let id = initialData?.id{
                print(3321,id)
                vd.survey_id = id
            }
            else{
            if let data = initialData {
                let form_data = ServiceForm()
                form_data.selectOrInput = selectOrInput
                form_data.setData(form_data: data, mode: .Review)
                
                vd.id = form_data.id
            }
            }
            
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
        else if mode == .Completed || mode == .Observing || mode == .Working{
            button_mode = mode
            removeDirectory()
            createDirectory()
            let form_data = ServiceForm()
            form_data.selectOrInput = selectOrInput
            if let dt = initialData {
                form_data.setData(form_data: dt, mode: .Review)
                var param = form_data.getParam()
                if mode == .Observing {
                    param["status"] = "observe"
                }
                else if mode == .Working {
                    param["status"] = "processing"
                }
                else{
                    param["status"] = "waiting"
                }
                
                let json = try? JSONSerialization.data(withJSONObject: param, options: [.prettyPrinted])
                if let dt = json {
                    let str = String(data: dt, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
                    let jsonstr = str?.data(using: .utf8)
                    writeDataToFolder(data: jsonstr, name: "api.json")
                    zipFile()
                }
            }
            
            

        }
        else if mode == .fillSurvey {
            let vd = SurveyController()
            if let data = initialData {
                let form_data = ServiceForm()
                form_data.selectOrInput = selectOrInput
                form_data.setData(form_data: data, mode: .Review)
                
                vd.id = form_data.id
            }
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
        else if mode == .checkSurvey {
            let vd = SurveyController()
            vd.survey_id = initialData?.id
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
    }
    func removeDirectory(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let upkeep_form = documentsDirectory.appendingPathComponent("upkeep_orders")
            let upkeep_zip = documentsDirectory.appendingPathComponent("upkeep_orders.zip")
            try? FileManager.default.removeItem(at: upkeep_form)
            try? FileManager.default.removeItem(at: upkeep_zip)
        }
    }
    func removeZip(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let upkeep_zip = documentsDirectory.appendingPathComponent("upkeep_orders.zip")
        try? FileManager.default.removeItem(at: upkeep_zip)
        }
    }
    let hud = JGProgressHUD()
    func zipFile(){
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        if button_mode == .Completed {
        hud.textLabel.text = "結案中..."
        }
        else if button_mode == .Observing {
            hud.textLabel.text = "觀察中..."
        }
        else{
            hud.textLabel.text = "處理中..."
        }
        hud.show(in: container)
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//            hud.textLabel.text = "上傳中..."
//            hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
//            hud.show(in: self.container)
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/")
            
            
                
               
                self.startObservingProgress()
                DispatchQueue.global().async {
                    try? FileManager.default.zipItem(at: dataPath, to: documentsDirectory.appendingPathComponent("upkeep_orders.zip"), shouldKeepParent: true,progress: self.progress)
                    self.stopObservingProgress()
                }

                
 
            
        }
    }
    
    @objc var progress : Progress?
    var isObservingProgress = false
    var progressViewKVOContext = 0
    func startObservingProgress()
    {
        guard !isObservingProgress else { return }

        progress = Progress()
        progress?.completedUnitCount = 0
        //self.indicator.progress = 0.0

        self.addObserver(self, forKeyPath: #keyPath(progress.fractionCompleted), options: [.new], context: &progressViewKVOContext)
        isObservingProgress = true
    }
    func stopObservingProgress()
    {
        guard isObservingProgress else { return }

        self.removeObserver(self, forKeyPath: #keyPath(progress.fractionCompleted))
        isObservingProgress = false
        //self.progress = nil
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(progress.fractionCompleted) {
            

                
                
                
                if self.progress?.isFinished == true {
                    if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let dt = try? Data(contentsOf: documentsDirectory.appendingPathComponent("upkeep_orders.zip"))
                            
                            if let dt = dt, let st = self.initialData{
                                let formData = ServiceForm()
                                formData.selectOrInput = self.selectOrInput
                                formData.setData(form_data: st, mode: .Review)
                                var urlString = "api-or/v1/upkeep/order/\(formData.id)"

                                
                                NetworkCall.shared.postCallZip(parameter: urlString, data: dt,fileName:"upkeep_orders.zip", decoderType: ServiceData.self) { (json) in
                                    DispatchQueue.main.async {
                                        if let json = json {
                                            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                                            if self.button_mode == .Completed {
                                                self.hud.textLabel.text = "結案成功"
                                            }
                                            else {
                                                self.hud.textLabel.text = ""
                                            }
                                            
                                            self.hud.dismiss(afterDelay: 1, animated: true) {
                                                self.selectedIndex = 0
                                                self.list = []
                                                self.fetchOrigin()
                                                self.isAppear = true
                                            }
                                            
                                            
                                        }
                                        else{
                                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                            if self.button_mode == .Completed {
                                                self.hud.textLabel.text = "結案失敗"
                                            }
                                            else {
                                                self.hud.textLabel.text = ""
                                            }
                                            
                                            self.hud.dismiss(afterDelay: 1, animated: true) {
                                                //
                                            }
                                        }
                                    }


        
                        
                                }
                                
                                       
                            }
                            else{
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.hud.textLabel.text = "結案失敗"
                                self.hud.dismiss(afterDelay: 1, animated: true) {
                                    //
                                }
                            }
                        }

  
                        
                    }
                }
            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }

    func createDirectory(){
        
           
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders")

                print(dataPath)
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)

            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    func createButton(){
        extrabutton_stackview.safelyRemoveArrangedSubviews()
        extrabutton_stackview.addArrangedSubview(UIView())
        for (index,i) in array.enumerated() {
//            var width = 100.calcvaluex()
//            if i == .checkSurvey || i == .fillSurvey{
//                width = i.rawValue.widthOfString(usingFont: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) + 40.calcvaluex()
//            }
            let button = AddButton4()

            
            button.titles.text = i.rawValue.localized
            button.tag = index
            
           button.addTarget(self, action: #selector(goAction), for: .touchUpInside)
            extrabutton_stackview.addArrangedSubview(button)
        }
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
//        topview.addSubview(button_stack)
//
//        button_stack.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 18.calcvaluex()))
        

        titleview.label.text = "服務單歷史紀錄".localized
        
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = .clear
        tableview.register(ServiceHistoryCell.self, forCellReuseIdentifier: "history")
        container.addSubview(tableview)
        tableview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 248.calcvaluex(), height: 0))
        tableview.contentInset.bottom = 26.calcvaluey()
        info_view.mode = .Review
        container.addSubview(info_view)
        info_view.tableview.contentInset.top = 0
        info_view.anchor(top: container.topAnchor, leading: tableview.trailingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 12.calcvaluex(), bottom: 26.calcvaluey(), right: 12.calcvaluex()))
        
        
    }
    var isAppear = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        selectedIndex = 0
        list = []
        fetchOrigin()
        isAppear = true
    }
    func fetchOrigin(){
        let hud = JGProgressHUD()
        hud.show(in: self.info_view)
        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/order/\(self.id ?? "")", decoderType: ServiceData.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                if let json = json?.data{
                    
                        if GetUser().isAfterSale() {
                            if json.status == "processing" {
                            //,.Preview
                                self.array = [.Completed,.Observing,.Edit]
                            }
                            else if json.status == "waiting" {
                                //.Preview,
                                self.array = [.fillSurvey]
                            }
                            else if json.status == "observe" {
                                self.array = [.Completed,.Working]
                            }
                            else {
                                //.Preview,
                                self.array = [.checkSurvey]
                            }
                        }
                        else{
                            //.Preview
                            self.array = []
                        }
                    self.initialData = json
                    self.createButton()
                    let data = ServiceForm()
                    data.selectOrInput = self.selectOrInput
                    data.setData(form_data: json, mode: .Review)
                    self.info_view.form_data = data
                    
                    if self.isAppear {
                    self.list.append(ServiceFormList(id: json.id, owner: json.owner, status: json.status, created_at: json.updated_at))
                    self.fetchApi()
                        self.isAppear = false
                    }
                    
                }
                
            }

        }
    }
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/order/\(self.id ?? "")/records", decoderType: ServiceFormHistroyData.self) { (json) in
            DispatchQueue.main.async {
                if let json = json?.data {
                    for i in json {
                        self.list.append(i)
                    }
                    //self.list = json
                    self.tableview.reloadData()
                    //self.fetchData()
                }
            }

        }
    }
    
    func fetchData(){
        let hud = JGProgressHUD()
        hud.show(in: self.info_view)
        
        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/order/record/\(self.list[selectedIndex].id)", decoderType: ServiceData.self) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                if let json = json?.data {
                    let data = ServiceForm()
                    data.selectOrInput = self.selectOrInput
                    data.setData(form_data: json, mode: .Review)
                    self.info_view.form_data = data

                }
            }

        }
    }
}
