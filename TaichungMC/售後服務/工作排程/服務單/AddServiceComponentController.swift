//
//  AddNewComponentController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
protocol circularSelectionDelegate {
    func selectedIndex(index:Int)
}
class circularSelectionContainer : UIView,CircleSelectionViewDelegate {
    func didSelect(mode: AddFixMode) {
        print(996)
        selectedIndex = mode.rawValue
        if let delegate = delegate {
            delegate.selectedIndex(index: mode.rawValue)
        }
    }
    var delegate: circularSelectionDelegate?
    let label = UILabel()
    var top_text : String = ""
    var arr = [CircleSelectionView]()
    let stackview = Horizontal_Stackview(spacing:40.calcvaluex())
    var selectedIndex : Int? {
        didSet{
        for (index,i) in arr.enumerated() {
            if let index2 = selectedIndex {
                if index == index2 {
                    i.didSet = true
                    //i.didSet = true
                }
                else{
                    i.didSet = false
                    i.selectionbutton.image = #imageLiteral(resourceName: "btn_radio_normal").withRenderingMode(.alwaysTemplate)
                    i.selectionbutton.tintColor = .black
                    i.selectlabel.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
                }
            }
        }
        }
    }
    var isImportant = false
    init(isImportant:Bool = false,text:String,array:[String]) {
        super.init(frame: .zero)
        self.isImportant = isImportant
        top_text = text
        if !isImportant {
        label.text = text
        }
        else{
            
            let attributedText = NSMutableAttributedString(string: "*", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8249840736, green: 0.1374278367, blue: 0.1237704381, alpha: 1)])
            attributedText.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor: MajorColor().oceanlessColor]))
            label.attributedText = attributedText
        }
        label.font = UIFont(name: MainFont().Medium, size: 18.calcvaluex())
        if !isImportant {
            label.textColor = MajorColor().oceanlessColor
        }
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
       
        for (index,i) in array.enumerated() {
            let vd = CircleSelectionView()
            vd.selectionbutton.image = #imageLiteral(resourceName: "btn_radio_normal").withRenderingMode(.alwaysTemplate)
            vd.selectionbutton.tintColor = .black
            vd.selectlabel.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
            vd.selectlabel.text = i
            vd.selectlabel.isUserInteractionEnabled = true
            vd.tag = index
            vd.delegate = self
            arr.append(vd)
            stackview.addArrangedSubview(vd)
        }
        stackview.addArrangedSubview(UIView())
        addSubview(stackview)
        stackview.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 7.calcvaluey(), left: 0, bottom: 0, right: 0))
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol selectedContainerDelegate {
    func finishEntering()
}
class selectedContainer : UIView{
    let button = UIButton(type: .custom)
    let imgv = UIImageView(image: #imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate))
    let textField = UITextField()
    
    init(text: String) {
    super.init(frame: .zero)
        backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        button.setTitle(text, for: .normal)
        button.backgroundColor = MajorColor().oceanColor
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 15.calcvaluex()
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        imgv.tintColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        addSubview(button)
        button.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 42.calcvaluey()))
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: button.topAnchor, trailing: trailingAnchor,padding: .init(top: 15.calcvaluey(), left: 65.calcvaluex(), bottom: 15.calcvaluey(), right: 65.calcvaluex()))
        
        addSubview(textField)
        textField.anchor(top: imgv.topAnchor, leading: leadingAnchor, bottom: imgv.bottomAnchor, trailing: trailingAnchor)
        textField.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluey())
        textField.textAlignment = .center
        constrainWidth(constant: 166.calcvaluex())
        constrainHeight(constant: 108.calcvaluey())
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getEnter)))
        button.addTarget(self, action: #selector(getEnter), for: .touchUpInside)
        
        
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
    }
    var delegate : selectedContainerDelegate?

    @objc func getEnter(){
       
        textField.becomeFirstResponder()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddServiceComponentController: UIViewController,SelectComponentErrorDelegate,FormTextViewDelegate,UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == quantityField.textField {
        if textField.text == "0" || textField.text == "" {
            textField.text = nil
            quantityField.imgv.isHidden = false
            totalField.textField.text = nil
            totalField.imgv.isHidden = false
            return
        }
        if let quantity = textField.text?.integerValue {
            
            if let price = priceField.textField.text?.replacingOccurrences(of: "$", with: "").integerValue {
               let total = quantity * price
                totalField.imgv.isHidden = true
                
                totalField.textField.text = "$\(total)"
            }
        }
        else{
            totalField.textField.text = nil
            totalField.imgv.isHidden = false
            textField.text = nil
            quantityField.imgv.isHidden = false
            print("Error")
        }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == quantityField.textField {

            quantityField.imgv.isHidden = true

        
        }
    }

    func textViewReloadHeight(height: CGFloat) {
        print(661,height)
        self.view.layoutIfNeeded()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .changeReason {
        componentData.reason = text
        }
        else{
            componentData.owner = text
        }
    }
    
    func setComponentError(type: Int, error: ComponentError?, component: Component?) {
        if type == 0{
            
            errorField.textfield.text = error?.code
           
        }
        else{
            
            componentField.textfield.text = component?.code
            
            priceField.imgv.isHidden = true
            priceField.textField.text = "$\(component?.price?.integerValue ?? 0)"
            
            if let quantity = quantityField.textField.text?.integerValue, let price = component?.price?.integerValue {
                let total = quantity * price
                 totalField.imgv.isHidden = true
                 
                 totalField.textField.text = "$\(total)"
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField != quantityField.textField {
        grabApi(type: textField.tag)
        return false
        }
        if priceField.textField.text == "" || priceField.textField.text == nil{
            
            return false
        }
        return true
    }
    func grabApi(type:Int) {
        let hud = JGProgressHUD()
        hud.show(in: container)

        if type == 0{
            NetworkCall.shared.getCall(parameter: "api-or/v1/faults", decoderType: ComponentErrorData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                       let vd = SelectComponentErrorController()
                        vd.modalPresentationStyle = .overCurrentContext
                        vd.type = 0
                        vd.error_array = json
                        vd.delegate = self
                        self.present(vd, animated: false, completion: nil)
                    }
                }

            }
        }
        else{
            NetworkCall.shared.getCall(parameter: "api-or/v1/accessories", decoderType: ComponentData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                        let vd = SelectComponentErrorController()
                         vd.modalPresentationStyle = .overCurrentContext
                         vd.type = 1
                         vd.component_array = json
                        vd.delegate = self
                         self.present(vd, animated: false, completion: nil)
                    }
                }

            }
        }

    }
    let container = SampleContainerView()
    var containerAnchor : AnchoredConstraints?
    let errorField = InterviewNormalFormView(text: "故障代碼".localized, placeholdertext: "可透過代碼搜尋".localized,color: #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1))
    let componentField = InterviewNormalFormView(text: "零件編號".localized, placeholdertext: "可透過代碼搜尋".localized,color: #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1))
    let ownerField = NormalTextView(text: "零件持有人".localized, placeholdertext: "請輸入零件持有人".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let reasonField = NormalTextView(text: "更換原因".localized, placeholdertext: "請輸入更換原因".localized, color: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1))
    let priceField = selectedContainer(text: "零件售價".localized)
    let quantityField = selectedContainer(text: "數量".localized)
    let totalField = selectedContainer(text: "總計".localized)
    let gatherView = circularSelectionContainer(isImportant : true,text: "收費狀況".localized, array: ["收費".localized,"不收費".localized])
    let maintainStatusView = circularSelectionContainer(isImportant : true,text: "維修狀況".localized, array: [UserDefaults.standard.getConvertedLanguage() == "en" ? "completed" : "完成",UserDefaults.standard.getConvertedLanguage() == "en" ? "forward" : "轉交中",UserDefaults.standard.getConvertedLanguage() == "en" ? "processing" : "處理中"])
    let doneButton = UIButton(type: .custom)

    var componentData = FormComponent()
    var isEdit = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        container.label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        container.label.text = "新增換件項目".localized
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.layer.cornerRadius = 15.calcvaluex()
        container.backgroundColor = .white
        view.addSubview(container)
        containerAnchor = container.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 660.calcvaluey()).isActive = true
        let h_stack = Horizontal_Stackview(distribution:.fillEqually,spacing: 12.calcvaluex())
        errorField.makeImportant()
        componentField.makeImportant()
        errorField.textfield.delegate = self
        errorField.textfield.tag = 0
        componentField.textfield.tag = 1
        componentField.textfield.delegate = self

        errorField.constrainHeight(constant: 80.calcvaluey())
        componentField.constrainHeight(constant: 80.calcvaluey())
        h_stack.addArrangedSubview(errorField)
        h_stack.addArrangedSubview(componentField)

        let p_stack = Horizontal_Stackview(spacing:46.calcvaluex(),alignment: .center)
        p_stack.addArrangedSubview(priceField)
        priceField.isUserInteractionEnabled = false
        let xLabel = UILabel()
        xLabel.text = "x"
        xLabel.font = UIFont(name: "Roboto-Regular", size: 30.calcvaluex())
        xLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        p_stack.addArrangedSubview(xLabel)
        xLabel.constrainWidth(constant: 28.calcvaluex())
        xLabel.constrainHeight(constant: 28.calcvaluex())
        p_stack.addArrangedSubview(quantityField)
        quantityField.textField.delegate = self
        let equalLabel = UILabel()
        equalLabel.text = "="
        equalLabel.textColor = #colorLiteral(red: 0.6101056337, green: 0.6101958156, blue: 0.610085845, alpha: 1)
        equalLabel.font = UIFont(name: "Roboto-Regular", size: 40.calcvaluex())
        equalLabel.constrainWidth(constant: 28.calcvaluex())
        equalLabel.constrainHeight(constant: 28.calcvaluex())
        p_stack.addArrangedSubview(equalLabel)
        totalField.isUserInteractionEnabled = false
        p_stack.addArrangedSubview(totalField)

      //  p_stack.addArrangedSubview(UIView())
        let g_stack = Horizontal_Stackview(spacing:UserDefaults.standard.getConvertedLanguage() == "en" ? 10.calcvaluex(): 70.calcvaluex())
        g_stack.addArrangedSubview(gatherView)
        g_stack.addArrangedSubview(UIView())
        g_stack.addArrangedSubview(maintainStatusView)
        
        doneButton.setTitle("新增".localized, for: .normal)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        doneButton.backgroundColor = MajorColor().oceanColor
        
        doneButton.constrainWidth(constant: 124.calcvaluex())
        doneButton.constrainHeight(constant: 48.calcvaluey())
        doneButton.layer.cornerRadius = 48.calcvaluey()/2
        let bt_container = UIView()
        bt_container.addSubview(doneButton)
        doneButton.centerXInSuperview()
        doneButton.anchor(top: bt_container.topAnchor, leading: nil, bottom: bt_container.bottomAnchor, trailing: nil)
        let v_stack = Vertical_Stackview(spacing:28.calcvaluey())
        v_stack.addArrangedSubview(h_stack)
        reasonField.textfield.mode = .changeReason
        reasonField.textfield.t_delegate = self
        let n_h_stack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
        ownerField.constrainWidth(constant: 200.calcvaluex())
        ownerField.textfield.mode = .componentOwner
        ownerField.textfield.t_delegate = self
        n_h_stack.addArrangedSubview(ownerField)
        n_h_stack.addArrangedSubview(reasonField)
        v_stack.addArrangedSubview(n_h_stack)
        v_stack.addArrangedSubview(p_stack)
        v_stack.addArrangedSubview(g_stack)
        v_stack.addArrangedSubview(bt_container)
        v_stack.addArrangedSubview(UIView())
//        container.addSubview(v_stack)
//
//        v_stack.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 46.calcvaluey(), left: 141.calcvaluex(), bottom: 39.calcvaluey(), right: 139.calcvaluex()))
        
        
        let scrollview = UIScrollView()
        scrollview.showsVerticalScrollIndicator = false
        scrollview.addSubview(v_stack)
        
        container.addSubview(scrollview)
        scrollview.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 46.calcvaluey(), left: 141.calcvaluex(), bottom: 39.calcvaluey(), right: 139.calcvaluex()))
        v_stack.fillSuperview()
        v_stack.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        
        
        
        self.view.layoutIfNeeded()
        container.xbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
        
        doneButton.addTarget(self, action: #selector(setData), for: .touchUpInside)
        

    }
    func showAlert(text:String) {
        let con = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        con.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
        self.present(con, animated: true, completion: nil)
    }
    weak var con : ServiceFormThirdSectionCell?
    @objc func setData(){
        self.view.endEditing(true)
        componentData.error_code = self.errorField.textfield.text ?? ""
        componentData.component_code = self.componentField.textfield.text ?? ""
        componentData.reason = self.reasonField.textfield.text ?? ""
        
        componentData.owner = self.ownerField.textfield.text ?? ""
        componentData.total_price = self.totalField.textField.text?.replacingOccurrences(of: "$", with: "") ?? "0"
        componentData.quantity = self.quantityField.textField.text ?? "0"
        if let index = maintainStatusView.selectedIndex, let status = GetStatus().getUpkeepOrderStatus()?.component_statuses  {
            let maintainView = maintainStatusView.arr[index]
            let ff = status.first { (st) -> Bool in
                return st.getString() == maintainView.selectlabel.text
            }
            componentData.status = ff?.key
        }
        //componentData.status = maintainStatusView.selectedIndex
        componentData.payment_status = gatherView.selectedIndex
        componentData.price = priceField.textField.text?.replacingOccurrences(of: "$", with: "") ?? "0"
        if componentData.error_code == ""{
            showAlert(text: "請選擇故障代碼")
            return
        }
        
        if componentData.component_code == ""{
            showAlert(text: "請選擇零件編號")
            return
        }
        if componentData.total_price == "0" || componentData.total_price == ""{
            showAlert(text: "請輸入零件數量")
            return
        }
        if componentData.payment_status == nil{
            showAlert(text: "請選擇收費狀況")
            return
        }
        if componentData.status == nil{
            showAlert(text: "請選擇維修狀況")
            return
        }
        
        
        UIView.setAnimationsEnabled(false)
        if isEdit {
            print(3313)
            con?.data?.components[self.view.tag] = componentData
            con?.tableView?.reloadData()
        }
        else{
        con?.data?.components.append(componentData)
       
        con?.tableView?.reloadData()
            if let con = con {
                con.tableView?.scrollToRow(at: IndexPath(row: 0, section: con.tag), at: .bottom, animated: false)
            }
        
        }
        self.dismiss(animated: false, completion: {
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                UIView.setAnimationsEnabled(true)
            }
            
        })
        
        
    }
    @objc func popView(){
        containerAnchor?.top?.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        } completion: { (com) in
            if com {
                self.dismiss(animated: false, completion: nil)
            }
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        errorField.textfield.text = componentData.error_code
        componentField.textfield.text = componentData.component_code
        reasonField.textfield.text = componentData.reason
       // reasonField.textfield.textViewDidChange(reasonField.textfield)
        if componentData.owner == "" {
            ownerField.textfield.text = GetUser().getUser()?.data.name
        }
        else{
        ownerField.textfield.text = componentData.owner
        }
       // ownerField.textfield.textViewDidChange(ownerField.textfield)
        if componentData.price != ""{
            priceField.imgv.isHidden = true
            priceField.textField.text = "$\(componentData.price)"
        }
        if componentData.quantity != ""{
            quantityField.imgv.isHidden = true
            quantityField.textField.text = "\(componentData.quantity)"
        }
        if componentData.total_price != ""{
            totalField.imgv.isHidden = true
            totalField.textField.text = "$\(componentData.total_price)"
        }
        if let pay_status = componentData.payment_status {
            gatherView.selectedIndex = pay_status

        }
        if let status_arr = GetStatus().getUpkeepOrderStatus()?.component_statuses,
           let status = componentData.status,
           let first = status_arr.first(where: { (st) -> Bool in
            return st.key == status
        }),
           let index = maintainStatusView.arr.firstIndex(where: { (cr) -> Bool in
            return cr.selectlabel.text == first.value
        }) {
            maintainStatusView.selectedIndex = index
        }
//        if let status = componentData.status {
//            maintainStatusView.selectedIndex = status
//
//        }
        if isEdit {
            container.label.text = "編輯換件項目"
            doneButton.setTitle("更新", for: .normal)
        }
        containerAnchor?.top?.constant = 0 - container.frame.height + 13.calcvaluey()
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}
