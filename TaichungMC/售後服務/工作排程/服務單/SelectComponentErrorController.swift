//
//  SelectComponentErrorController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol SelectComponentErrorDelegate {
    func setComponentError(type:Int,error:ComponentError?,component:Component?)
}
class SelectComponentErrorController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let txt = textField.text, txt != "" {
            if type == 0{
                fil_error_array = error_array.filter({ (err) -> Bool in
                    return (err.code?.contains(txt) ?? false) || (err.description?.contains(txt) ?? false)
                })
            }
            else {
                fil_component_array = component_array.filter({ (err) -> Bool in
                    return (err.code?.contains(txt) ?? false) || (err.description?.contains(txt) ?? false)
                })
            }
        }
        else {
            fil_error_array = error_array
            fil_component_array = component_array
        }
        
        self.tableview.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == 0{
            return fil_error_array.count
        }
        return fil_component_array.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if type == 0{
            delegate?.setComponentError(type: type, error: fil_error_array[indexPath.row], component: nil)
            
        }
        else{
            delegate?.setComponentError(type: type, error: nil, component: fil_component_array[indexPath.row])
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    var delegate : SelectComponentErrorDelegate?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        cell.textLabel?.numberOfLines = 0
        var str = ""
        if type == 0 {
            let com = fil_error_array[indexPath.row]
            str = "\("編號".localized): \(com.code ?? "")\n\n\(com.description ?? "")"
        }
        else{
            let com = fil_component_array[indexPath.row]
            str = "\("編號".localized):\(com.code ?? "")\n\n\(com.description ?? "")"
        }
        
        cell.textLabel?.text = str
        
        return cell
    }
    var type : Int = 0
    var error_array = [ComponentError]() {
        didSet{
            fil_error_array = error_array
        }
    }
    
    var component_array = [Component]() {
        didSet{
            fil_component_array = component_array
        }
    }
    var fil_error_array = [ComponentError]()
    var fil_component_array = [Component]()
    let con = SampleContainerView()
    let searchField = SearchTextField()
    let tableview = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        if type == 0{
            con.label.text = "選擇故障代碼".localized
            searchField.placeholder = "搜尋故障代碼".localized
        }
        else{
            con.label.text = "選擇零件編號".localized
            searchField.placeholder = "搜尋零件編號".localized
        }
        view.addSubview(con)
        con.centerInSuperview(size: .init(width: 500.calcvaluex(), height: 600.calcvaluey()))
        
        view.addSubview(searchField)
        searchField.delegate = self
        searchField.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: nil, trailing: con.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 52.calcvaluey()))
        searchField.layer.borderWidth = 1.calcvaluex()
        searchField.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        searchField.layer.cornerRadius = 6.calcvaluex()
        tableview.showsVerticalScrollIndicator = false
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(tableview)
        tableview.anchor(top: searchField.bottomAnchor, leading: searchField.leadingAnchor, bottom: con.bottomAnchor, trailing: searchField.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
