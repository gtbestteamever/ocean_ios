//
//  ServiceFormController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import ZIPFoundation
import JGProgressHUD
enum ServiceMode {
    case New
    case Review
    case Edit
    case addOn
}
class ServiceFormController: SampleController {
    
    var isReassign = false {
        didSet{
            container.isReassign = self.isReassign
        }
    }
    var workDetail : WorkDetail?
    let container = ServiceFormView()
    let button = AddButton4()
    var mode : ServiceMode? {
        didSet{
            container.mode = mode
        }
    }
    let hud = JGProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        titleview.label.text = "服務單".localized
        //view.backgroundColor = .clear
        //background_view.image = #imageLiteral(resourceName: "background_afterservice3")
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 80.calcvaluex(), bottom: 0, right: 80.calcvaluex()))
        
        button.titles.text = "送出".localized
        
        
        extrabutton_stackview.addArrangedSubview(button)
        button.addTarget(self, action: #selector(sendData), for: .touchUpInside)
        createDirectory()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mode == .Edit{
            titleview.label.text = "編輯服務單".localized
        }
        else{
            titleview.label.text = "新增服務單".localized
        }
    }
    override func popview() {
        super.popview()
        
        removeDirectory()
        self.dismiss(animated: true, completion: nil)
    }
    func removeDirectory(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let upkeep_form = documentsDirectory.appendingPathComponent("upkeep_orders")
            let upkeep_zip = documentsDirectory.appendingPathComponent("upkeep_orders.zip")
            try? FileManager.default.removeItem(at: upkeep_form)
            try? FileManager.default.removeItem(at: upkeep_zip)
        }
    }
    func removeZip(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let upkeep_zip = documentsDirectory.appendingPathComponent("upkeep_orders.zip")
        try? FileManager.default.removeItem(at: upkeep_zip)
        }
    }
    func createDirectory(){
        
           
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders")

                print(dataPath)
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)

            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func writeDataToFolder(data:Data?,name:String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/\(name)")
            
            do{
                try data?.write(to: dataPath)

                
            }
            catch let er{
                print(er)
            }
            
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    @objc func sendData(){
        self.view.endEditing(true)
        print(991,container.form_data.getParam())
        let valid = container.form_data.checkValid()
        if !valid.0 {
            var msg = ""
            if valid.1 == 0 {
                msg = "請確認聯絡人 電話 地址有完整輸入".localized
            }
            else if valid.1 == 1 {
                msg = "請確認現況描述 原因初判 採取對策 維修內容有完整輸入".localized
            }
            else if valid.1 == 6{
                msg = "請確認工程工號 姓名 簽名有完整輸入".localized
            }
            let alert = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
                
                
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            var index = 0
            if valid.1 == 0{
                index = 0
            }
            else if valid.1 == 1{
                index = 1
            }
            else{
                index = self.container.fields.count - 1
            }
            self.container.tableview.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            return
        }
        
        print(77746,container.form_data.getParam())
        let json = try? JSONSerialization.data(withJSONObject: container.form_data.getParam(), options: [.prettyPrinted])
        if let dt = json {
            let str = String(data: dt, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
            let jsonstr = str?.data(using: .utf8)
            writeDataToFolder(data: jsonstr, name: "api.json")
            zipFile()
        }
//        for i in container.form_data.components {
//            print(778,i.signiture)
//        }
        
    }
    
    func zipFile(){
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            hud.textLabel.text = "上傳中...".localized
            hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            hud.show(in: self.container)
            let dataPath = documentsDirectory.appendingPathComponent("upkeep_orders/")
            
            
                
            print(112,dataPath.absoluteString)
                self.startObservingProgress()
                DispatchQueue.global().async {
                    do {
                        try FileManager.default.zipItem(at: dataPath, to: documentsDirectory.appendingPathComponent("upkeep_orders.zip"), shouldKeepParent: true,progress: self.progress)
                    }
                    catch let error {
                        print(error)
                    }
                    
                    self.stopObservingProgress()
                }

                
 
            
        }
    }
    
    @objc var progress : Progress?
    var isObservingProgress = false
    var progressViewKVOContext = 0
    func startObservingProgress()
    {
        guard !isObservingProgress else { return }

        progress = Progress()
        progress?.completedUnitCount = 0
        //self.indicator.progress = 0.0

        self.addObserver(self, forKeyPath: #keyPath(progress.fractionCompleted), options: [.new], context: &progressViewKVOContext)
        isObservingProgress = true
    }
    func stopObservingProgress()
    {
        guard isObservingProgress else { return }

        self.removeObserver(self, forKeyPath: #keyPath(progress.fractionCompleted))
        isObservingProgress = false
        //self.progress = nil
    }
    func updateStatus(){
        var param = [String:Any]()
        var workers_id = [String]()
        for i in self.workDetail?.service_personnels ?? [] {
            workers_id.append(i.id)
        }
        param["service_personnels"] = workers_id
        if isReassign {
            param["status"] = "reassigned"
        }
        else{
            param["status"] = "completed"
        }
        
        param["upkeep_id"] = self.workDetail?.upkeep?.id ?? ""
        param["scheduled"] = self.workDetail?.scheduled ?? ""
        param["description"] = self.workDetail?.description ?? ""

        NetworkCall.shared.postCallAny(parameter: "api-or/v1/upkeep-tasks/\(self.workDetail?.id ?? "")", param: param, decoderType: UpdatedWorkDetail.self) { (json) in
            DispatchQueue.main.async {
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.textLabel.text = "上傳成功".localized
                self.hud.dismiss(afterDelay: 1, animated: true) {
                   self.popview()
                }

            }

        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(progress.fractionCompleted) {
            

                
                
                
                if self.progress?.isFinished == true {
                    if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let dt = try? Data(contentsOf: documentsDirectory.appendingPathComponent("upkeep_orders.zip"))
                            if let dt = dt{
                                print(7789,dt)
                                var urlString = ""
                                if self.mode == .addOn || self.mode == .Edit{
                                    urlString = "api-or/v1/upkeep/order/\(self.container.form_data.id)"
                                }
                                else{
                                    urlString = "api-or/v1/upkeep/\(self.container.form_data.upkeep_id)/order"
                                }
                                
                                NetworkCall.shared.postCallZip(parameter: urlString, data: dt,fileName:"upkeep_orders.zip", decoderType: ServiceData.self) { (json) in
                                    DispatchQueue.main.async {
                                        if let json = json {
                                            if self.mode == .New || self.mode == .addOn {
                                                self.updateStatus()
                                            }
                                            else{
                                                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                                                self.hud.textLabel.text = "上傳成功".localized
                                                self.hud.dismiss(afterDelay: 1, animated: true) {
                                                   self.popview()
                                                }
                                            }
                                            
                                        }
                                        else{
                                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                            self.hud.textLabel.text = "上傳失敗".localized
                                            self.hud.dismiss(afterDelay: 1, animated: true) {
                                                self.removeZip()
                                                
                                            }
                                        }
                                        
                                    }


        
                        
                                }
                                
                                       
                            }
                            else{
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.hud.textLabel.text = "上傳失敗".localized
                                self.hud.dismiss(afterDelay: 1, animated: true) {
                                    //self.removeZip()
                                    
                                }
                            }
                        }

  
                        
                    }
                }
            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}



