//
//  ServiceFormExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/29/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
extension ServiceFormView : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cachedCellHeights[indexPath] = cell.frame.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mode = fields[indexPath.section]
        
        let cell = tableview.dequeueReusableCell(withIdentifier: mode.rawValue, for: indexPath) as! ServiceFormCell
        cell.tag = indexPath.section
        cell.isReassign = self.isReassign
        cell.mode = self.mode
        cell.frame = .init(x: 0, y: 0, width: tableView.frame.width, height: 0)
        cell.setData(data: self.form_data)
        if(indexPath.section == 0){
            cell.blueLine.isHidden = false
            cell.maskConer(mask: [.layerMinXMinYCorner,.layerMaxXMinYCorner])
        }
        else if(indexPath.section == fields.count - 1) {
            cell.blueLine.isHidden = true
            cell.maskConer(mask: [.layerMinXMaxYCorner,.layerMaxXMaxYCorner])
        }
        else{
            cell.blueLine.isHidden = false
            cell.maskConer(mask: [])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cachedCellHeights[indexPath] {
            
                    return height
                }
        return 200.calcvaluey()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return fields.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return fields[section].sectionHeight
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = ServiceFormHeader()
        vd.label.text = fields[section].sectionTitle
        return vd
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
