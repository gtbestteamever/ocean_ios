//
//  ServicePreviewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class ServicePreviewController : SampleController {
    let button = AddButton4()
    var id : String?
    var status : String?
    var survey_id : String?
    let webview = WKWebView()
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingButton.isHidden = true
        self.backbutton.isHidden = false
        titleview.label.text = "服務單預覽".localized
        
        
        extrabutton_stackview.addArrangedSubview(button)
        if status == "waiting" {
            button.titles.text = "填寫滿意度調查與簽名".localized
        }
        else if status == "completed" || status == "review" {
            button.titles.text = "查看滿意度調查與簽名".localized
            
        }
        else{
            button.isHidden = true
        }
        
        button.addTarget(self, action: #selector(goAction), for: .touchUpInside)
        
        view.addSubview(webview)
        webview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0))
        
//        if let url = URL(string: "http://customer.urb2b.com/~e199288/upkeep_order.pdf") {
//            print(3321,url.absoluteString)
//            webview.loadRequest(URLRequest(url: url))
//        }
        
    }
    
    @objc func goAction(){
        let vd = SurveyController()
        vd.con = self
        if let id = survey_id {
            print(992,id)
            vd.survey_id = id
        }
        else{
        vd.id = self.id
        }
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
}
