//
//  WorkListCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/11.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class WorkListCell: UITableViewCell {
    let container = UIView()
    let statuslabel = PaddedLabel2()
    let topheader = UILabel()
    let subheader = UILabel()
    let upkeep_status = UILabel()
    let location = UILabel()
    let maintaintime = UILabel()
    let donetime = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        container.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        container.backgroundColor = .white
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 12.calcvaluey(), right: 2))
        
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 38.calcvaluex(), bottom: 0, right: 0),size: .init(width: 100.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.layer.cornerRadius = 30.calcvaluey()/2
       // statuslabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30.calcvaluey()).isActive = true
        statuslabel.centerYInSuperview()
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        //statuslabel.textColor = #colorLiteral(red: 0.9296925068, green: 0.421582222, blue: 0.0175667014, alpha: 1)
        //statuslabel.text = "已完成"
        statuslabel.textAlignment = .center
        //statuslabel.layer.cornerRadius = 5.calcvaluex()
        //statuslabel.layer.borderColor = #colorLiteral(red: 0.9296925068, green: 0.421582222, blue: 0.0175667014, alpha: 1)
        statuslabel.layer.borderWidth = 1.calcvaluex()
        statuslabel.numberOfLines = 2
        
        container.addSubview(topheader)
        topheader.anchor(top: container.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 38.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        //topheader.text = "久大寰宇科技股份有限公司"
        topheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        topheader.numberOfLines = 2
        topheader.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
       // subheader.text = "工具機問題"
        subheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        //subheader.text = "A"
        subheader.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(subheader)
        subheader.anchor(top: topheader.bottomAnchor, leading: topheader.leadingAnchor, bottom: nil, trailing: topheader.trailingAnchor)
        subheader.numberOfLines = 0
        

        //upkeep_status.centerYAnchor.constraint(equalTo: subheader.centerYAnchor).isActive = true
        [upkeep_status,location,maintaintime,donetime].forEach { (lb) in
            lb.font = UIFont(name: "Roboto-Light", size: 16.calcvaluey())
            lb.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
            
            container.addSubview(lb)
        }
        
       
        upkeep_status.anchor(top: subheader.bottomAnchor, leading: subheader.leadingAnchor, bottom: nil, trailing: subheader.trailingAnchor,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
       
        location.numberOfLines = 0

        
        location.anchor(top: upkeep_status.bottomAnchor, leading: upkeep_status.leadingAnchor, bottom: nil, trailing: upkeep_status.trailingAnchor,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        maintaintime.anchor(top: location.bottomAnchor, leading: subheader.leadingAnchor, bottom: nil, trailing: subheader.trailingAnchor,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        donetime.anchor(top: maintaintime.bottomAnchor, leading: subheader.leadingAnchor, bottom: container.bottomAnchor, trailing: subheader.trailingAnchor,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 16.calcvaluey(), right: 0))
        
        
    }
    func setData(data:WorkDetail) {
        let status_array = GetStatus().getUpKeepTaskStatus()
        if let status  = status_array.first(where: { (st) -> Bool in
            return st.key == data.status
        }) {
            statuslabel.text = status.getString()
            statuslabel.textColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff")
            statuslabel.layer.borderColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff").cgColor
        }

//        upkeep_status.textColor = UIColor().hexStringToUIColor(hex: upkeep_st?.color ?? "#fffff")

        if let com = data.completed_at {
            donetime.text = "\("完成時間".localized): \(Date().convertToDateComponent(text: com,format: "yyyy-MM-dd HH:mm:ss",addEight: false))"
        }
        else{
            donetime.text = nil
        }
        
        topheader.text = data.upkeep?.account?.name
        
        let upkeep_st = GetStatus().getUpKeep(text: data.upkeep?.status ?? "")
//       // upkeep_status.text = upkeep_st?.value
//        let nsattr = NSMutableAttributedString(string: data.upkeep?.subject ?? "", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Medium", size: 20.calcvaluex())!])
//        nsattr.append(NSAttributedString(string: "     \(upkeep_st?.value ?? "")", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Bold", size: 18.calcvaluex())!]))
//        subheader.attributedText = nsattr
        subheader.text = data.upkeep?.subject
        upkeep_status.text = "\("報修狀態".localized): \(upkeep_st?.getString() ?? "")"
        if let appliance = data.upkeep?.appliance {
            location.text = "\("報修地點".localized): \(appliance.address ?? "")"
        }
        else{
            location.text = nil
        }
        maintaintime.text = "\("維修時間".localized): \(Date().convertToDateComponent(text: data.created_at ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false))"
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
