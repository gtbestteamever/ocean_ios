//
//  EditScheduleController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/3/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class EditScheduleController: UIViewController,FormTextViewDelegate,UITextViewDelegate {
    @objc func sendData(){
        var param = [String:Any]()
        var people_arr = [String]()
        
        for i in self.data?.service_personnels ?? [] {
            people_arr.append(i.id)
        }
        param["service_personnels"] = people_arr
        param["upkeep_id"] = self.data?.upkeep?.id ?? ""
        param["scheduled"] = self.data?.scheduled ?? ""
        param["status"] = self.data?.status ?? ""
        param["description"] = self.data?.description ?? ""
        print(param)
        let hud = JGProgressHUD()
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        hud.show(in: self.scrollView)
        hud.textLabel.text = "修改中..."
        
        NetworkCall.shared.postCall(parameter: "api-or/v1/upkeep-tasks/\(self.data?.id ?? "")", param: param, decoderType: UpdatedWorkDetail.self) { (json) in
            DispatchQueue.main.async {
                
                if let json = json?.data {
                    print(331,json)
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.textLabel.text = "修改成功"
                    hud.dismiss(afterDelay: 1.5, animated: true) {
                        self.contr?.workDetail = json
                        self.popView()
                    }
                    
                    
                }
                else{
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.textLabel.text = "修改失敗"
                    hud.dismiss(afterDelay: 1.5, animated: true)
                }
            }

        }
        
    }
    func didBegin(mode: FormTextViewMode) {
        if mode == .EditTime {
            let vd = CalendarTimeController()
            vd.con2 = self
            
            vd.data = self.data
            vd.modalPresentationStyle = .overCurrentContext
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let n_date = format.date(from: data?.scheduled ?? "") , n_date.timeIntervalSince1970 > Date().timeIntervalSince1970{
                
            vd.selected_date = format.date(from: data?.scheduled ?? "")
                
                vd.timeField.text = n_date.convertTimeToChinese()
            }
            self.present(vd, animated: false, completion: nil)
        }
        else{
            let vd = ScheduleEmployeeController()
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            vd.date = format.date(from: data?.scheduled ?? "")
            vd.pre_data = self.data
            vd.con = self
            vd.modalPresentationStyle = .overCurrentContext
            self.present(vd, animated: false, completion: nil)
        }
    }
    func textViewReloadHeight(height: CGFloat) {
        self.view.layoutIfNeeded()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        data?.description = text
    }
    
    var data : WorkDetail? {
        didSet{
            
            timeField.textfield.text = Date().convertToDateComponent(text: data?.scheduled ?? "",onlyDate: false,addEight: false)
            var total_str = ""
            for (index,i) in (data?.service_personnels ?? []).enumerated() {
            
                total_str += "\(i.service_groups.first?.titles?.getLang() ?? "") - \(i.name ?? "")"
                if index != (data?.service_personnels.count ?? 0) - 1 {
                    total_str += "\n"
                }
            }
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 8.calcvaluey()
            let attributes = [NSAttributedString.Key.paragraphStyle : style,NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!]
            memberField.textfield.attributedText = NSAttributedString(string: total_str,attributes: attributes)
            memberField.textfield.textViewDidChange(memberField.textfield)
            descriptionField.textfield.text = data?.description
            descriptionField.textfield.textViewDidChange(descriptionField.textfield)
        }
    }
    let con = SampleContainerView()
    let timeField = NormalTextView(text: "\("維修時間".localized):", placeholdertext: "選擇維修時間".localized)
    let memberField = NormalTextView(text: "\("維修人員".localized):", placeholdertext: "")
    let descriptionField = NormalTextView(text: "\("維修內容".localized):", placeholdertext: "")
    let doneButton = UIButton(type: .custom)
    let scrollView = UIScrollView()
    weak var contr : WorkDetailController?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(con)
        con.label.text = "編輯派工".localized
        con.centerInSuperview(size: .init(width: 600.calcvaluex(), height: 0))
        con.heightAnchor.constraint(lessThanOrEqualToConstant: UIScreen.main.bounds.height - 100.calcvaluey()).isActive = true
        timeField.textfield.t_delegate = self
        timeField.textfield.mode = .EditTime
        memberField.textfield.t_delegate = self
        memberField.textfield.mode = .EditEmployee
        
        scrollView.bounces = false
        con.addSubview(scrollView)
       scrollView.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: con.bottomAnchor, trailing: con.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 26.calcvaluey(), right: 36.calcvaluex()))
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        doneButton.setTitle("送出".localized, for: .normal)
        doneButton.backgroundColor = #colorLiteral(red: 0.9351186156, green: 0.5506169796, blue: 0.04444750398, alpha: 1)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        doneButton.constrainWidth(constant: 100.calcvaluex())
        doneButton.constrainHeight(constant: 46.calcvaluey())
        doneButton.layer.cornerRadius = 46.calcvaluey()/2
        doneButton.addTarget(self, action: #selector(sendData), for: .touchUpInside)
        let container = UIView()
        
        container.addSubview(doneButton)
        doneButton.anchor(top: container.topAnchor, leading: nil, bottom: container.bottomAnchor, trailing: nil)
        doneButton.centerXInSuperview()
        let stackview = Vertical_Stackview(spacing:18.calcvaluex(),alignment: .fill)
        stackview.addArrangedSubview(timeField)
        stackview.addArrangedSubview(memberField)
        stackview.addArrangedSubview(descriptionField)
        descriptionField.textfield.initialHeight = 150.calcvaluey()
        descriptionField.textfield.anchorHeight?.constant = 150.calcvaluey()
        descriptionField.textfield.mode = .EditDescription
        descriptionField.textfield.t_delegate = self
        stackview.addArrangedSubview(container)
//        con.addSubview(stackview)
//        stackview.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: con.bottomAnchor, trailing: con.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 26.calcvaluey(), right: 26.calcvaluex()))
        scrollView.addSubview(stackview)
        stackview.fillSuperview()
        stackview.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        stackview.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
