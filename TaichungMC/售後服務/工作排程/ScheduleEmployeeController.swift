//
//  ScheduleEmployeeController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 8/19/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class ScheduleEmployeeController : UIViewController,UITableViewDelegate,UITableViewDataSource,ScheduleThirdStepDelegate {
    func didSelectStep(didSelect: Bool, id: String) {
        for (index,i) in group_array.enumerated() {
            for (index2,j) in i.workers.enumerated() {
                if j.id == id {
                    j.isSelected = didSelect
                    let cell = tableview.cellForRow(at: IndexPath(row: index2, section: index)) as! ScheduleThirdStepSelectionCell
                    cell.setDate(data: j)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            if section == 0{
                return 63.calcvaluey()
            }
            else{
                return 40.calcvaluey()
            }

        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScheduleThirdStepSelectionCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.setDate(data: group_array[indexPath.section].workers[indexPath.row])
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vd = UIView()
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.text = group_array[section].names?.getLang()
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
            label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 2, bottom: 12.calcvaluey(), right: 0))
        
        return vd
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
            return group_array.count
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if group_array.count > 0{
            return group_array[section].workers.count
            }
            else{
                return 0
            }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 92.calcvaluey()

    }
    let container = SampleContainerView()
    var group_array = [Worker_Group]()
    var date : Date?
    var pre_data : WorkDetail?
    weak var con : EditScheduleController?
    func fetchWorker(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)
        group_array = []
        let currentDateString = date?.getCurrentTime(format: "yyyy-MM-dd")
        NetworkCall.shared.getCall(parameter: "api-or/v1/service-personnels?start_date=\(currentDateString ?? "")&end_date=\(currentDateString ?? "")", decoderType: ScheduleEmployeeModel.self ) { (json) in
            DispatchQueue.main.async {
                hud.dismiss()
                if let json = json {
                    self.createGroup(data: json.data)
                    

                }
            }

        }
    }
    func createGroup(data:[ScheduleEmployee]) {
        for i in data{
            for j in i.service_groups {
                if !group_array.contains(where: { (group) -> Bool in
                    return group.code == j.code
                }) {
                    group_array.append(Worker_Group(code: j.code, name: j.title ?? "", names:j.titles,workers: []))
                }

            }
        }
        
        for i in data{
            for s in group_array {
                if i.service_groups.contains(where: { (group) -> Bool in
                    return group.code == s.code
                }) {
                    s.workers.append(i)
                }
            }
        }
        for i in pre_data?.service_personnels ?? [] {
            for j in group_array {
                for d in j.workers {
                    if i.id == d.id {
                        d.isSelected = true
                    }
                }
            }
        }
        self.tableview.reloadData()

//        for i in group_array {
//            print(i.name)
//            for j in i.workers {
//                print(j.name)
//            }
//            print("----")
//        }
        
    }
    let doneButton = UIButton(type: .custom)
    let tableview = UITableView(frame: .zero, style: .grouped)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.label.text = "派工人員".localized
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 700.calcvaluey()))
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        doneButton.setTitle("確定".localized, for: .normal)
        doneButton.backgroundColor = #colorLiteral(red: 0.9351186156, green: 0.5506169796, blue: 0.04444750398, alpha: 1)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())

        doneButton.layer.cornerRadius = 46.calcvaluey()/2
        container.addSubview(doneButton)
        doneButton.centerXInSuperview()
        doneButton.addTarget(self, action: #selector(setSelection), for: .touchUpInside)
        doneButton.constrainWidth(constant: 100.calcvaluex())
        doneButton.constrainHeight(constant: 46.calcvaluey())
        doneButton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 24.calcvaluey(), right: 0))
        
        container.addSubview(tableview)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        tableview.register(ScheduleThirdStepSelectionCell.self, forCellReuseIdentifier: "cell")
        tableview.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: doneButton.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 36.calcvaluex(), bottom: 12.calcvaluey(), right: 36.calcvaluex()))
        tableview.backgroundColor = .clear
        fetchWorker()
    }
    @objc func setSelection(){
        var dt = [ScheduleEmployee]()
        for i in group_array {
            for j in i.workers {
                if !dt.contains(where: { (emp) -> Bool in
                    return emp.id == j.id
                }) && (j.isSelected ?? false){
                    dt.append(j)
                }
            }
        }
        con?.data?.service_personnels = dt
        
        self.dismiss(animated: false, completion: nil)
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
