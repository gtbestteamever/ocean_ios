//
//  ScheduleWorkController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension UIColor {
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
class SelectGroupTextField:UITextField{
    var rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(rightimage)
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        rightimage.centerYInSuperview()
        
        let picker = UIPickerView()
        inputView = picker
        
        self.tintColor = .clear
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 34.calcvaluex(), dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 34.calcvaluex(), dy: 0)
    }
}
class ScheduleSelectTextField:UITextField {
     var rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    var padding : CGFloat = 0
    init(padding:CGFloat) {
        super.init(frame: .zero)
        self.padding = padding
        self.backgroundColor = .white
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(rightimage)
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        rightimage.centerYInSuperview()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: self.padding, bottom: 0, right: 0))
    }
}
class ScheduleWorkController: SampleController {
    let container = UIView()
    var containeranchor:AnchoredConstraints!
    let selectgroup = SelectGroupTextField()
    let selectzone = SelectGroupTextField()
    let statuscollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let tableview = UITableView(frame: .zero, style: .grouped)
    let addNewScheduleButton = AddButton4()
    var rightview = UIView()
    var rightviewanchor:AnchoredConstraints!
    weak var maincontroller:ViewController!
    var shrink = false
    let rightviewtoplabel = UILabel()
    let bottomtableview = UITableView(frame: .zero, style: .plain)
    var isManager = false
    var current_work_array = [WorkDetail]()
    var group_array = [Worker_Group]()
    var selected_index = [Int:Int]()
    let calenderButton = UIButton(type:.custom)
    override func changeLang() {
        super.changeLang()
        titleview.label.text = date.getCurrentMandarinTime()
        rightviewtoplabel.text = "派工列表".localized
        addNewScheduleButton.titles.text = "建立派工".localized
        tableview.reloadData()
        bottomtableview.reloadData()
    }
    @objc func selectDate(){
        let calendar = CalendarController()
        calendar.selected_date = self.date
        calendar.con = self
        calendar.modalPresentationStyle = .overCurrentContext
        self.present(calendar, animated: false, completion: nil)
    }
    var date = Date()

    override func viewDidLoad() {
        super.viewDidLoad()
        background_view.image = #imageLiteral(resourceName: "background_afterservice2")
        settingButton.isHidden = false
        isManager = GetUser().isSaleManager()
        print(771,isManager)
        //print(661,GetStatus().getUpKeepTaskStatus())
        titleview.label.text = date.getCurrentMandarinTime()
        titleview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectDate)))
        titleview.isUserInteractionEnabled = true
        calenderButton.setImage(#imageLiteral(resourceName: "ic_date_pressed").withRenderingMode(.alwaysTemplate), for: .normal)
        calenderButton.tintColor = .white
        
        topview.addSubview(calenderButton)
        calenderButton.anchor(top: nil, leading: titleview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        calenderButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        calenderButton.addTarget(self, action: #selector(selectDate), for: .touchUpInside)
       
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        view.addSubview(container)
        container.backgroundColor = .clear
        containeranchor = container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 0, right: 0),size: .init(width: 353.calcvaluex(), height: 0))
        
//        selectgroup.text = "全部組別"
//        selectgroup.addshadowColor(color: #colorLiteral(red: 0.8776529431, green: 0.8799210191, blue: 0.885224998, alpha: 1))
//        container.addSubview(selectgroup)
//        selectgroup.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 1, bottom: 0, right: 1),size: .init(width: 0, height: 46.calcvaluey()))
//
//        selectgroup.layer.cornerRadius = 46.calcvaluey()/2
//
//        selectzone.text = "全部已派地區"
//        selectzone.addshadowColor(color: #colorLiteral(red: 0.8776529431, green: 0.8799210191, blue: 0.885224998, alpha: 1))
//        container.addSubview(selectzone)
//        selectzone.anchor(top: selectgroup.bottomAnchor, leading: selectgroup.leadingAnchor, bottom: nil, trailing: selectgroup.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
//        selectzone.layer.cornerRadius = 46.calcvaluey()/2
//
//        (statuscollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
//
//        container.addSubview(statuscollectionview)
//        statuscollectionview.backgroundColor = .clear
//        statuscollectionview.delegate = self
//        statuscollectionview.dataSource = self
//        statuscollectionview.anchor(top: selectzone.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,size: .init(width: 0, height: 50.calcvaluey()))
//        statuscollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        container.addSubview(tableview)
        
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        tableview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.backgroundColor = .clear
        tableview.register(ScheduleWorkControllerCell.self, forCellReuseIdentifier: "cell")
        tableview.register(WorkListCell.self, forCellReuseIdentifier: "work")
        tableview.delegate = self
        tableview.dataSource = self
        
        if isManager {
            tableview.isHidden = false
        }
        else{
            tableview.isHidden = true
        }
        
        

        
        
        view.addSubview(rightview)
        rightview.addshadowColor()
        rightview.backgroundColor = .white
        rightview.layer.cornerRadius = 15.calcvaluex()
        
        rightview.anchor(top: tableview.topAnchor, leading: isManager == true ?  tableview.trailingAnchor : view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: menu_bottomInset + 24.calcvaluey(), right: 25.calcvaluex()))
        rightview.isHidden = true
        
        
        rightview.addSubview(rightviewtoplabel)
        
        rightviewtoplabel.text = "派工列表".localized
        rightviewtoplabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        rightviewtoplabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        rightviewtoplabel.anchor(top: topview.bottomAnchor, leading: rightview.leadingAnchor, bottom: nil, trailing: rightview.trailingAnchor,padding: .init(top: 32.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 0))
        
        rightview.addSubview(bottomtableview)
        bottomtableview.register(WorkListCell.self, forCellReuseIdentifier: "work")
        bottomtableview.separatorStyle = .none
        bottomtableview.backgroundColor = .clear
        bottomtableview.anchor(top: rightviewtoplabel.bottomAnchor, leading: rightview.leadingAnchor, bottom: rightview.bottomAnchor, trailing: rightview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 31.calcvaluex()))
        bottomtableview.delegate = self
        bottomtableview.dataSource = self
        
        
        view.addSubview(addNewScheduleButton)
        addNewScheduleButton.titles.text = "建立派工".localized
        addNewScheduleButton.backgroundColor = MajorColor().oceanlessColor
        extrabutton_stackview.addArrangedSubview(addNewScheduleButton)
        
        addNewScheduleButton.addTarget(self, action: #selector(showNewSchedule), for: .touchUpInside)
        
        if isManager {
            addNewScheduleButton.isHidden = false
        }
        else{
            addNewScheduleButton.isHidden = true
        }
    }
    @objc func showNewSchedule(){
        let vd = NewScheduleController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            //selected_index = [0:0]
        
        
        fetchApi()
    }
    let hud = JGProgressHUD()
    func fetchApi(){

        print(77111)
        
        if isManager {
            fetchWorker()

        }
        else{
            fetchWork()

        }
        

    }
    
    func fetchWorker(){
        hud.show(in: self.view)
        group_array = []
        let currentDateString = date.getCurrentTime(format: "yyyy-MM-dd")
        print(661,currentDateString)
        NetworkCall.shared.getCall(parameter: "api-or/v1/service-personnels?start_date=\(currentDateString)&end_date=\(currentDateString)", decoderType: ScheduleEmployeeModel.self ) { (json) in
            DispatchQueue.main.async {
                
                if let json = json {
                    
                    self.createGroup(data: json.data)
                    
                    if !self.selected_index.isEmpty {
                        //self.selected_index = [0:0]
                        self.fetchWork()
                    }
                    else{
                        self.hud.dismiss()
                    }
                }
            }

        }
    }
    func createGroup(data:[ScheduleEmployee]) {
        print(992,data)
        for i in data{
            for j in i.service_groups {
                if !group_array.contains(where: { (group) -> Bool in
                    return group.name == j.title
                }) {
                    group_array.append(Worker_Group(code: j.code, name: j.title ?? "",names: j.titles, workers: []))
                }

            }
        }
        
        for i in data{
            for s in group_array {
                if i.service_groups.contains(where: { (group) -> Bool in
                    return group.title == s.name
                }) {
                    s.workers.append(i)
                }
            }
        }
        self.tableview.reloadData()
//        for i in group_array {
//            print(i.name)
//            for j in i.workers {
//                print(j.name)
//            }
//            print("----")
//        }
        
    }
    func fetchWork(){
        hud.show(in: self.view)
        current_work_array = []
        let currentDateString = date.getCurrentTime(format: "yyyy-MM-dd")
        var id = ""
        if isManager {
            
            if let section = selected_index.first?.0, let index = selected_index.first?.1 {
                id = group_array[section].workers[index].id
            }
            
        }
        else{
            id = GetUser().getUser()?.data.id ?? ""
        }
      
        NetworkCall.shared.getCall(parameter: "api-or/v1/service-personnels/\(id)/upkeep-tasks?start_date=\(currentDateString)&end_date=\(currentDateString)", decoderType: WorkDetailModel.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    //print(661,json)
                    self.hud.dismiss()
                    self.rightview.isHidden = false
                    self.current_work_array = json.data
                    
                    if self.isManager {
                        self.bottomtableview.reloadData()
                    }
                    else{
                        self.tableview.reloadData()
                    }

                }
                
            }

        }
    }
    override func popview() {
        

        
        maincontroller.showcircle()
        

        
        containeranchor.leading?.constant = 333.calcvaluex()
        containeranchor.width?.constant = 665.calcvaluex()
        statuscollectionview.collectionViewLayout.invalidateLayout()
        rightviewanchor.leading?.constant = 0
        rightview.isHidden = true
        shrink = false
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        selected_index = [:]
        UIView.setAnimationsEnabled(false)
        self.tableview.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.setAnimationsEnabled(true)
        }
    }
}

extension ScheduleWorkController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .clear
        let button = UIButton(type: .system)
        cell.addSubview(button)

        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        button.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: nil, trailing: cell.trailingAnchor,padding: .init(top: 12.calcvaluey(), left:0, bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        button.layer.cornerRadius = 38.calcvaluey()/2
        
        if indexPath.item == 0{
            button.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            button.setTitle("全部", for: .normal)
        }
        else{
            button.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            button.setTitle("出勤", for: .normal)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: shrink == true ? 102.calcvaluex():166.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if shrink {
            return 26.calcvaluex()
        }
        return 66.calcvaluex()
    }
}

extension ScheduleWorkController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tableview && isManager {
        let vd = UIView()
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
            label.text = group_array[section].names?.getLang()
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
            label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 2, bottom: 12.calcvaluey(), right: 0))
        
        return vd
        }
        return UIView()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableview && isManager {
            return group_array.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableview && isManager {
            if group_array.count > 0{
            return group_array[section].workers.count
            }
            else{
                return 0
            }
        }
        print(889,current_work_array.count)
        return current_work_array.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableview && isManager {
        return 92.calcvaluey()
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableview && isManager{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScheduleWorkControllerCell 
        cell.selectionStyle = .none
            
            cell.setData(data: group_array[indexPath.section].workers[indexPath.row])
            if selected_index[indexPath.section] == indexPath.row {
                cell.setData()
            }
            else{
                cell.unSetData()
            }
//        if indexPath.item == 0{
//            cell.rightimage.isHidden = false
//
//        }
//        else if indexPath.item == 3{
//            cell.restlabel.isHidden = false
//        }
//        else{
//            cell.rightimage.isHidden = true
//            cell.restlabel.isHidden = true
//        }
        return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "work", for: indexPath) as! WorkListCell
            cell.selectionStyle = .none
            cell.setData(data:current_work_array[indexPath.row])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableview && isManager{
            if section == 0{
                return 63.calcvaluey()
            }
            else{
                return 40.calcvaluey()
            }
        }
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if tableView == tableview && isManager {
            selected_index = [:]
            selected_index[indexPath.section] = indexPath.row
            tableview.reloadData()
            fetchWork()
            

            
        }
        else{
            let vd = WorkDetailController()
            //print(self.current_work_array[indexPath.row])
            vd.id = self.current_work_array[indexPath.row].id
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
    }
}

class ScheduleWorkControllerCell:UITableViewCell {
    let container = UIView()
    let numberlabel = UILabel()
    let seperator = UIView()
    let titlelabel = UILabel()
    let zonelabel = UILabel()
    let rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_error"))
    let restlabel = UILabel()
    let selectionview = UIView()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionview.backgroundColor = .white
        
        contentView.addSubview(selectionview)
        selectionview.layer.cornerRadius = 15.calcvaluex()
        selectionview.addshadowColor(color: #colorLiteral(red: 0.8698186278, green: 0.8720664382, blue: 0.877323091, alpha: 1))
        selectionview.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 2, bottom: 6.calcvaluey(), right: 2))
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        //container.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        selectionview.addSubview(container)
        container.fillSuperview(padding: .init(top: 0, left: 11.calcvaluex(), bottom: 0, right: 0))
        container.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            container.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        numberlabel.text = "3"
        numberlabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        numberlabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        
        container.addSubview(numberlabel)
        numberlabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 23.calcvaluex(), bottom: 0, right: 0))
        numberlabel.centerYInSuperview()
        
        container.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.anchor(top: nil, leading: numberlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0.7.calcvaluex(), height: 52.calcvaluey()))
        seperator.centerYInSuperview()
        
        //titlelabel.text = "廖漢祥 - "
        titlelabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        titlelabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        container.addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYInSuperview()
        
        //zonelabel.text = "西屯區"
        zonelabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        zonelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        container.addSubview(zonelabel)
        zonelabel.anchor(top: nil, leading: titlelabel.trailingAnchor, bottom: nil, trailing: nil)
        zonelabel.centerYInSuperview()
        
        container.addSubview(rightimage)
        rightimage.isHidden = true
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 40.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        rightimage.centerYInSuperview()
        
        container.addSubview(restlabel)
        restlabel.isHidden = true
        restlabel.textAlignment = .center
        restlabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        restlabel.text = "休假"
        restlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        restlabel.layer.cornerRadius = 5.calcvaluex()
        restlabel.layer.borderWidth = 1.calcvaluex()
        restlabel.layer.borderColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        restlabel.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 56.calcvaluex(), height: 30.calcvaluey()))
        restlabel.centerYInSuperview()
        
        selectionview.addSubview(imageview)
        imageview.anchor(top: nil, leading: nil, bottom: nil, trailing: selectionview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 15.calcvaluex(), height: 15.calcvaluex()))
        imageview.centerYInSuperview()
        imageview.isHidden = true
        
    }
    func setData(){
        imageview.isHidden = false
    }
    func unSetData(){
        imageview.isHidden = true
    }
    func setData(data:ScheduleEmployee) {
        numberlabel.text = "\(data.workload)"
        titlelabel.text = data.name
        
        if data.is_vacation == 1{
            numberlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            restlabel.isHidden = false
        }
        else{
            numberlabel.textColor = MajorColor().oceanlessColor
            titlelabel.textColor = MajorColor().oceanlessColor
            restlabel.isHidden = true
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
