//
//  NewScheduleController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/16.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class ScheduleFirstStepCell : UITableViewCell {
    let containercell = UIView()
    let statuslabel = UILabel()
    let toplabel = UILabel()
    let sublabel = UILabel()
    let timelabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        contentView.addSubview(containercell)
        containercell.layer.cornerRadius = 15
        containercell.backgroundColor = .white
        containercell.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        containercell.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2, left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 104.calcvaluey()))
        
        containercell.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: containercell.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.text = "處理中"
        statuslabel.textAlignment = .center
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1.calcvaluex()
        
        containercell.addSubview(toplabel)
        toplabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        toplabel.text = "機台運作問題"
        toplabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        toplabel.anchor(top: containercell.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        
        
        containercell.addSubview(sublabel)
        sublabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        sublabel.text = "久大行銷股份有限公司"
        sublabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        sublabel.anchor(top: toplabel.bottomAnchor, leading: toplabel.leadingAnchor, bottom:nil , trailing: nil,padding: .init(top: 3, left: 0, bottom: 0, right: 0))
        
        containercell.addSubview(timelabel)
        timelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        timelabel.text = "10分鐘前"
        timelabel.anchor(top: sublabel.bottomAnchor, leading: sublabel.leadingAnchor, bottom:nil , trailing: nil,padding: .init(top: 3, left: 0, bottom: 0, right: 0))
        timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleFirstStep : UIView,UITableViewDelegate,UITableViewDataSource {
    let searchtext = SearchTextField()
    let selectlocation = ScheduleSelectTextField(padding: 59.calcvaluex())
    let tableview = UITableView(frame: .zero, style: .grouped)
    
    let right_info_view = FixInfoView()
    var c_fix_data : FixData?
    var current_page = 1
    var fix_array = [FixModel]()
    var isRefreshing = false
    var selectedIndex : Int = 0
    var nameCode : String?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(searchtext)
        
        searchtext.attributedPlaceholder = NSAttributedString(string: "搜尋報修主旨或內容".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchtext.backgroundColor = .white
        searchtext.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 2, left: 2, bottom: 0, right: 0),size: .init(width: 411.calcvaluex(), height: 46.calcvaluey()))
        searchtext.layer.cornerRadius = 46.calcvaluey()/2
        searchtext.addshadowColor(color: #colorLiteral(red: 0.8803046346, green: 0.8825793862, blue: 0.8878995776, alpha: 1))
        

        
        addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.anchor(top: searchtext.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing:nil,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 412.calcvaluex(), height: 0))
        tableview.delegate = self
        tableview.contentInset.bottom = 46.calcvaluey()
        tableview.dataSource = self
        tableview.register(FixRecordCell.self, forCellReuseIdentifier: "fix")
        let rightview = UIView()
        addSubview(rightview)
        rightview.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        rightview.backgroundColor = .white
        rightview.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            rightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        rightview.anchor(top: topAnchor, leading: tableview.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 2, left: 18.calcvaluex()-2, bottom: 0, right: 2))
        let scrollview = UIScrollView()
        rightview.addSubview(scrollview)
        scrollview.fillSuperview()
        scrollview.addSubview(right_info_view)
        right_info_view.fillSuperview()
        right_info_view.commentheader.threedotoption.isHidden = true
        right_info_view.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        right_info_view.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        scrollview.contentInset.bottom = 92.calcvaluey()
        scrollview.showsVerticalScrollIndicator = false
//        righttableview.backgroundColor = .white
//        righttableview
//        righttableview.bounces = true
//        righttableview.contentInset.bottom = 70.calcvaluey()
//        righttableview.fillSuperview()
        
        fetchApi()
    }
    var fixModel : FixModel?
    func fetchDetailInfo(){
        
        
            let hud = JGProgressHUD()
            hud.show(in: right_info_view)
            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(self.fix_array[selectedIndex].id)", decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                        self.fixModel = json
                        self.right_info_view.commentheader.data = json

                    }
                }

            }

        
    }
    func fetchApi(){
        var urlString = "api-or/v1/upkeeps?statuses=[\"standby\",\"assigned\",\"reassign\",\"new\"]&page=\(current_page)&pens=30"
        if let name = nameCode {
            urlString += "&name=\(name)"
        }
        
        NetworkCall.shared.getCall(parameter: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), decoderType: FixData.self) { (json) in
                DispatchQueue.main.async {
                    self.isRefreshing = false
                    if let json = json {
                        self.c_fix_data = json
                        for i in json.data {
                            self.fix_array.append(i)
                        }
                        if json.data.count > 0{
                        self.fetchDetailInfo()
                        }
                       
                        self.tableview.reloadData()

                    }
                }

            }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 4 {
            return self.fix_array.count + 1
        }
        return self.fix_array.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        
        return UIView()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 && indexPath.section == self.fix_array.count && !isRefreshing{
                isRefreshing = true
                current_page += 1
                fetchApi()
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 && indexPath.section == self.fix_array.count {
            
            return RefreshTableCell(style: .default, reuseIdentifier: "refresh")
        }
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "fix", for: indexPath) as! FixRecordCell
        cell.selectionStyle = .none
        if selectedIndex == indexPath.section {
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        cell.setData(data:self.fix_array[indexPath.section])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.section
        tableview.reloadData()
        fetchDetailInfo()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStepCell:UITableViewCell {
    let container = UIView()
    let datelabel = UILabel()
    let timelabel = UILabel()
    let deletebutton = UIView()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_delete"))
    //let imageview = UIImageView(image: #imageLiteral(resourceName: <#T##String#>))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor(color: #colorLiteral(red: 0.8722851872, green: 0.8745393753, blue: 0.8798108697, alpha: 1))
        
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 26.calcvaluex(), bottom: 2, right: 26.calcvaluex()))
        
        container.addSubview(datelabel)
        //datelabel.text = "2019年12月15日"
        datelabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        datelabel.textColor = MajorColor().oceanlessColor
        datelabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
        datelabel.centerYInSuperview()
        
        container.addSubview(timelabel)
       // timelabel.text = "下午9:00"
        timelabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        timelabel.textColor = MajorColor().oceanlessColor
        timelabel.anchor(top: nil, leading: datelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 66.calcvaluex(), bottom: 0, right: 0))
        timelabel.centerYInSuperview()
        
        container.addSubview(deletebutton)
        deletebutton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        deletebutton.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            deletebutton.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        deletebutton.anchor(top: container.topAnchor, leading: nil, bottom: container.bottomAnchor, trailing: container.trailingAnchor,size: .init(width: 104.calcvaluex(), height: 0))
        
        deletebutton.addSubview(imageview)
        imageview.centerInSuperview(size: .init(width: 48.calcvaluex(), height: 48.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStepAddTimeCell:UITableViewCell {
    let addButton = AddButton()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(addButton)
        selectionStyle = .none
        addButton.backgroundColor = MajorColor().oceanlessColor
        addButton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addButton.titles.text = "新增時間".localized
        addButton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 2, left: 0, bottom: 2, right: 0),size: .init(width: 0, height: 0))
        addButton.centerXInSuperview()
        addButton.layer.cornerRadius = (48.calcvaluey()-4)/2
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStep : UIView,UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return date_array.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScheduleSecondStepCell
        cell.datelabel.text = Date().convertToDateComponent(text: date_array[indexPath.section], format: "yyyy-MM-dd HH:mm:ss", addEight: false)
        cell.selectionStyle = .none
        cell.deletebutton.isUserInteractionEnabled = true
        cell.deletebutton.tag = indexPath.section
        cell.deletebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteTime)))
        return cell
    }
    @objc func deleteTime(gesture:UITapGestureRecognizer){
        print(gesture.view?.tag)
        if let index = gesture.view?.tag {
            self.date_array.remove(at: index)
            //self.tableview.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 26.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        
        return vd
    }
    let addButton = AddButton()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var date_array = [String](){
        didSet{
            self.tableview.reloadData()
            if date_array.count > 0{
            self.tableview.scrollToRow(at: IndexPath(item: 0, section: date_array.count - 1), at: .bottom, animated: false)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        addSubview(addButton)
     //   selectionStyle = .none
        addButton.backgroundColor = MajorColor().oceanlessColor
        addButton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addButton.titles.text = "新增時間".localized
        addButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 2, left: 0, bottom: 116.calcvaluey(), right: 0),size: .init(width: 0, height: 48.calcvaluey()))
        addButton.centerXInSuperview()
        addButton.layer.cornerRadius = 48.calcvaluey()/2
        addButton.addTarget(self, action: #selector(selectDateTime), for: .touchUpInside)
        addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = .clear
        tableview.anchor(top: topAnchor, leading: leadingAnchor, bottom: addButton.topAnchor, trailing: trailingAnchor,padding: .init(top: 29.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0))
        tableview.register(ScheduleSecondStepCell.self, forCellReuseIdentifier: "cell")
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
    }
    @objc func selectDateTime(){
        let date_con = CalendarTimeController()
        date_con.con_view = self
        date_con.modalPresentationStyle = .overCurrentContext
        
        General().getTopVc()?.present(date_con, animated: false, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStepCell:UICollectionViewCell {
    let container = UIView()
    let datelabel = UILabel()
    let timelabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8618018627, green: 0.8640291095, blue: 0.8692371845, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        datelabel.text = "2019年12月15日"
        datelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        datelabel.textColor = .white
        
        //timelabel.text = "上午9:00"
        timelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        timelabel.textColor = .white
        //container.isUserInteractionEnabled = true
        container.addSubview(datelabel)
        datelabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        datelabel.centerYInSuperview()
        container.addSubview(timelabel)
        timelabel.anchor(top: nil, leading: datelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0))
        timelabel.centerYInSuperview()

    }

    func setColor(){
        container.backgroundColor = MajorColor().oceanlessColor
    }
    func setUnColor(){
        container.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol ScheduleThirdStepDelegate{
    func didSelectStep(didSelect:Bool,id:String)
}
class ScheduleThirdStepSelectionCell:UITableViewCell {
    let container = UIView()
    let checkimage = UIButton(type: .custom)
    let seperator = UIView()
    let numlabel = UILabel()
    let vsep = UIView()
    let namelabel = UILabel()
    let zonelabel = UILabel()
    let statuslabel = UILabel()
    var data:ScheduleEmployee?
    var delegate:ScheduleThirdStepDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        container.backgroundColor = .clear
        container.layer.cornerRadius = 15.calcvaluex()
        contentView.addSubview(container)
        container.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectEmployee)))
        container.isUserInteractionEnabled = true
        container.fillSuperview()
        container.addSubview(checkimage)
        checkimage.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 51.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        checkimage.contentVerticalAlignment = .fill
        checkimage.contentHorizontalAlignment = .fill
        checkimage.centerYInSuperview()
        checkimage.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        checkimage.setImage(#imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate), for: .selected)
        checkimage.tintColor = MajorColor().oceanlessColor
        checkimage.isUserInteractionEnabled = false
        
        
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 56.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        container.addSubview(seperator)
        seperator.anchor(top: nil, leading:  container.leadingAnchor, bottom:  container.bottomAnchor, trailing:  container.trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        numlabel.font = UIFont(name: "Roboto-Bold", size: 24.calcvaluex())
        numlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
       //numlabel.text = "20"
       // numlabel.backgroundColor = .red
        container.addSubview(numlabel)
        numlabel.anchor(top: nil, leading: checkimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 30.calcvaluex(), height: 0))
        numlabel.centerYInSuperview()
        
        vsep.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        container.addSubview(vsep)
        vsep.anchor(top: nil, leading: numlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 52.calcvaluey()))
        vsep.centerYInSuperview()
        
       // namelabel.text = "廖漢祥 - "
        namelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        namelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        container.addSubview(namelabel)
        namelabel.anchor(top: nil, leading: vsep.trailingAnchor, bottom: nil, trailing: statuslabel.leadingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        namelabel.centerYInSuperview()
        
     //   zonelabel.text = "西屯區"
//        zonelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//        zonelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
//        container.addSubview(zonelabel)
//        zonelabel.anchor(top: nil, leading: namelabel.trailingAnchor, bottom: nil, trailing: nil)
//        zonelabel.centerYInSuperview()
        
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        statuslabel.text = "休假"
        statuslabel.layer.cornerRadius = 5.calcvaluex()
        statuslabel.layer.borderWidth = 1.calcvaluex()
        statuslabel.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        statuslabel.textAlignment = .center
        

    }
    @objc func didSelectEmployee(){
        self.data?.isSelected = !(self.data?.isSelected ?? false)
        self.checkimage.isSelected = self.data?.isSelected ?? false
        delegate?.didSelectStep(didSelect: self.data?.isSelected ?? false, id: self.data?.id ?? "")
    }

    func setDate(data:ScheduleEmployee) {
        self.data = data
        numlabel.text = "\(data.workload)"
        namelabel.text = data.name
        checkimage.isSelected = data.isSelected ?? false
        if data.is_vacation == 1{
            statuslabel.isHidden = false
            
        }
        else{
            statuslabel.isHidden = true
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStepBottomView: UIView,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,ScheduleThirdStepDelegate {
    var machine_code : String?
    func didSelectStep(didSelect: Bool, id: String) {
        if isHistory {
            for (index,i) in (work_array?.history ?? []).enumerated() {
                for var (index2,j) in i.workers.enumerated() {
                    if j.id == id {
                        j.isSelected = didSelect
                        let cell = tableview.cellForRow(at: IndexPath(row: index2, section: index)) as! ScheduleThirdStepSelectionCell
                        cell.setDate(data: j)
                    }
                }
            }
        }
        else{
        for (index,i) in (work_array?.worker ?? []).enumerated() {
            for var (index2,j) in i.workers.enumerated() {
                if j.id == id {
                    j.isSelected = didSelect
                    let cell = tableview.cellForRow(at: IndexPath(row: index2, section: index)) as! ScheduleThirdStepSelectionCell
                    cell.setDate(data: j)
                }
            }
        }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
       
        if textView.text != ""{
            textview_placeholder.isHidden = true
        }

    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textview_placeholder.isHidden = false
        }
        work_array?.description = textView.text
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if isHistory {
            return work_array?.history.count ?? 0
        }
        return work_array?.worker.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isHistory && work_array?.history.count == 0{
            return 0
        }
        else{
        if work_array?.worker.count == 0{
            return 0
        }
        }
        if isHistory {
            return work_array?.history[section].workers.count ?? 0
        }
        else{
            return work_array?.worker[section].workers.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ct", for: indexPath) as! ScheduleThirdStepSelectionCell
        cell.delegate = self
        cell.backgroundColor = .clear
        if isHistory {
            if let data = work_array?.history[indexPath.section].workers {
                cell.setDate(data: data[indexPath.row])
                
            }
        }
        else{
        if let data = work_array?.worker[indexPath.section].workers {
            cell.setDate(data: data[indexPath.row])
            
        }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 63.calcvaluey()
        }
        return 40.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        
        let label = UILabel()
        label.text = work_array?.worker[section].names?.getLang()
        
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        vd.addSubview(label)
        label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0))
        return vd
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.calcvaluey()
    }
    
    let allbutton = UIButton(type: .system)
    let historybutton = UIButton(type: .system)
    let zonebutton = UIButton(type: .system)
    let searchfield = SearchTextField()
    let selectallbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .grouped)
    let stackview = UIStackView()
    var stackviewanchor:AnchoredConstraints!
    let right_textview = UITextView()
    let textview_placeholder = UILabel()
    var work_array : NewWork?
    var selected_index = 0
    var date: String? {
        didSet{
            if work_array?.description == "" {
                textview_placeholder.isHidden = false
            }
            else{
                textview_placeholder.isHidden = true
            }
            right_textview.text = work_array?.description
            fetchApi()
        }
    }
    func fetchApi(){
        tableview.isHidden = true
        let hud = JGProgressHUD()
        hud.show(in: self.tableview)
        if let date = date, let format_chr = date.split(separator: " ").first {
                let format_date = String(format_chr)
            
            var urlString = ""
            if isHistory {
                urlString = "api-or/v1/appliances/\(machine_code ?? "")/service-personnels?start_date=\(format_date)&end_date=\(format_date)"
            }
            else{
                urlString = "api-or/v1/service-personnels?start_date=\(format_date)&end_date=\(format_date)"
            }
            NetworkCall.shared.getCall(parameter: urlString, decoderType: ScheduleEmployeeModel.self ) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    self.tableview.isHidden = false
                    if let json = json {
                        
                        self.createGroup(data: json.data)
                    }
                }

            }
            }
       
    }
    func createGroup(data:[ScheduleEmployee]) {
        var group_array = [Worker_Group]()
        if isHistory {
            group_array = work_array?.history ?? []
        }
        else{
            group_array = work_array?.worker ?? []
        }
        
        
        for i in data{
            for j in i.service_groups {
                if !group_array.contains(where: { (group) -> Bool in
                    return group.code == j.code
                }) {
                    print(2231)
                    group_array.append(Worker_Group(code: j.code, name: j.title ?? "",names: j.titles, workers: []))
                }

            }
        }
        
        for i in data{
            for s in group_array {
 
                
                
                if i.service_groups.contains(where: { (group) -> Bool in
                    return group.code == s.code
                }) {

                    if var first = s.workers.first(where: { (emp) -> Bool in
                        return emp.id == i.id
                    })
                    {
                        i.isSelected = first.isSelected
                        first = i
                    }
                    else{
                        s.workers.append(i)
                    }
                    
                    
                }
                if isHistory {
                    if let wr = work_array?.worker.first(where: { (gr) -> Bool in
                        return gr.code == s.code
                    }) , let wid = wr.workers.first(where: { (emp) -> Bool in
                        return emp.id == i.id
                    }), let ss = s.workers.firstIndex(where: { (emp) -> Bool in
                        return emp.id == i.id
                    }){
                        
                        i.isSelected = wid.isSelected
                        s.workers[ss] = i
                        
                    }
                }
                else{
                if let wr = work_array?.history.first(where: { (gr) -> Bool in
                    return gr.code == s.code
                })  , let wid = wr.workers.first(where: { (emp) -> Bool in
                    return emp.id == i.id
                }), let ss = s.workers.firstIndex(where: { (emp) -> Bool in
                    return emp.id == i.id
                }){
                    i.isSelected = wid.isSelected
                    s.workers[ss] = i
                }
                }
            }
        }
        if isHistory {
//            for i in group_array {
//                for j in i.workers {
//                    print(j.name,j.isSelected)
//                }
//            }
            work_array?.history = group_array
        }
        else{
            work_array?.worker = group_array
        }
        
        self.tableview.reloadData()
        if isHistory {
            if (work_array?.history.count ?? 0) > 0{
                self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
        }
        else{
        if (work_array?.worker.count ?? 0) > 0{
            self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
        }
    }
    @objc func showAll(){
        isHistory = false
    }
    @objc func showHistory(){
        isHistory = true
    }
    var isHistory = false {
        didSet{
            if isHistory {
                allbutton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
                historybutton.backgroundColor = MajorColor().oceanlessColor
                
            }
            else{
                allbutton.backgroundColor = MajorColor().oceanlessColor
                historybutton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
            }
            fetchApi()
        }

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        addshadowColor(color: #colorLiteral(red: 0.8581020236, green: 0.8603197336, blue: 0.8655055165, alpha: 1))
        
        allbutton.setTitle("全部".localized, for: .normal)
        allbutton.setTitleColor(.white, for: .normal)
        allbutton.backgroundColor = MajorColor().oceanlessColor
        allbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        allbutton.constrainWidth(constant: 102.calcvaluex())
        allbutton.constrainHeight(constant: 38.calcvaluey())
        allbutton.layer.cornerRadius = 38.calcvaluey()/2
        allbutton.addTarget(self, action: #selector(showAll), for: .touchUpInside)
        historybutton.addTarget(self, action: #selector(showHistory), for: .touchUpInside)
        historybutton.setTitle("歷史人員".localized, for: .normal)
        historybutton.setTitleColor(.white, for: .normal)
        historybutton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        historybutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        historybutton.constrainWidth(constant: 138.calcvaluex())
        historybutton.layer.cornerRadius = 38.calcvaluey()/2
        historybutton.constrainHeight(constant: 38.calcvaluey())
        zonebutton.setTitle("地區人員", for: .normal)
        zonebutton.setTitleColor(.white, for: .normal)
        zonebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        zonebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        zonebutton.constrainWidth(constant: 138.calcvaluex())
        zonebutton.layer.cornerRadius = 38.calcvaluey()/2
        
        searchfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        searchfield.constrainWidth(constant: 264.calcvaluex())
        searchfield.attributedPlaceholder = NSAttributedString(string: "搜尋派工人員", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        searchfield.layer.cornerRadius = 48.calcvaluey()/2
        searchfield.constrainHeight(constant: 48.calcvaluey())
        
        selectallbutton.setTitle("套用全部日期".localized, for: .normal)
        selectallbutton.setTitleColor(.white, for: .normal)
        selectallbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        selectallbutton.backgroundColor = MajorColor().oceanlessColor
        selectallbutton.layer.cornerRadius = 38.calcvaluey()/2
        selectallbutton.constrainWidth(constant: 144.calcvaluex())
        selectallbutton.constrainHeight(constant: 38.calcvaluey())
        for i in [allbutton,historybutton,UIView(),selectallbutton] {
            stackview.addArrangedSubview(i)
            
        }

        stackview.alignment = .top
        stackview.axis = .horizontal
        stackview.spacing = 26.calcvaluex()
        
        addSubview(stackview)
        stackviewanchor = stackview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 0.calcvaluey()))
        
        tableview.backgroundColor = .clear
        tableview.register(ScheduleThirdStepSelectionCell.self, forCellReuseIdentifier: "ct")
        addSubview(tableview)
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: bottomAnchor, trailing: centerXAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -25.calcvaluex()))
        tableview.contentInset.bottom = 92.calcvaluey()
        tableview.showsVerticalScrollIndicator = false
        right_textview.layer.borderWidth = 1.calcvaluex()
        right_textview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        right_textview.layer.cornerRadius = 12.calcvaluex()
        
        addSubview(right_textview)
        right_textview.anchor(top: tableview.topAnchor, leading: tableview.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 12.calcvaluex(), bottom: 104.calcvaluey(), right: 12.calcvaluex()))
        
        right_textview.addSubview(textview_placeholder)
        textview_placeholder.anchor(top: right_textview.topAnchor, leading: right_textview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        textview_placeholder.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textview_placeholder.text = "請輸入報修內容".localized
        right_textview.delegate = self
        right_textview.textContainerInset = .init(top: 18.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex())
        right_textview.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        right_textview.autocorrectionType = .no
        right_textview.autocapitalizationType = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStep:UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return date_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ScheduleThirdStepCell
        cell.backgroundColor = .clear
        cell.datelabel.text = Date().convertToDateComponent(text: date_array[indexPath.item], format: "yyyy-MM-dd HH:mm:ss", addEight: false)
        if indexPath.item == selected_index {
            cell.setColor()
        }
        else{
            cell.setUnColor()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 290.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selected_index = indexPath.item
        self.topview.reloadData()
        self.topview.scrollToItem(at: IndexPath(row: indexPath.item, section: 0), at: .left, animated: false)
        
    }
    let topview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let bottomview = ScheduleThirdStepBottomView()
    var date_array = [String]() {
        didSet{
            topview.reloadData()
            work_array = []
            bottomview.machine_code = self.machine_code
            print(3321,id)
            for i in date_array {
                work_array.append(NewWork(time: i, work_id: id,worker : [],history: []))
            }
            selected_index = 0
        }
    }
    var id : String = ""

    var selected_index = 0 {
        didSet{
            //work_array[selected_index].worker = []
            bottomview.work_array = work_array[selected_index]
            bottomview.date = date_array[selected_index]
        }
    }
    var work_array = [NewWork]()
    var machine_code : String?
    override init(frame: CGRect) {
        super.init(frame: frame)

        topview.backgroundColor = .clear
        (topview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        topview.showsHorizontalScrollIndicator = false
        topview.delegate = self
        topview.dataSource = self
        topview.register(ScheduleThirdStepCell.self, forCellWithReuseIdentifier: "cell")

        
        addSubview(topview)
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 52.calcvaluey()))
        addSubview(bottomview)
        bottomview.selectallbutton.addTarget(self, action: #selector(setAll), for: .touchUpInside)
        bottomview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    @objc func setAll(){
        for j in work_array {
            j.worker = self.bottomview.work_array?.worker ?? []
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol NewScheduleCompleteViewDelegate {
    func closeCompleteView()
    func closeConfirmView()
}
class NewScheduleCompleteView:UIView {
    let title = UILabel()
    let doneview = ScheduleThirdStep()
    let closebutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    var delegate:NewScheduleCompleteViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        title.text = "已選維修人員列表"
        title.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        title.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        addSubview(title)
        title.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        title.centerXInSuperview()
        
        addSubview(closebutton)
        closebutton.isUserInteractionEnabled = true
        closebutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        closebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closingview)))
        doneview.bottomview.stackviewanchor.height?.constant = 0
        doneview.bottomview.stackviewanchor.top?.constant = 0
        doneview.bottomview.stackview.isHidden = true
        addSubview(doneview)
        doneview.anchor(top: title.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    @objc func closingview(){
        delegate.closeCompleteView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ScheduleConfirmView : UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ScheduleConfirmViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 311.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    let title = UILabel()
    let sendbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .grouped)
    let closebutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    var delegate:NewScheduleCompleteViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        title.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        title.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(title)
        title.text = "確認派工資訊"
        title.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        title.centerXInSuperview()
        
        addSubview(closebutton)
        closebutton.isUserInteractionEnabled = true
        closebutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        closebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeView)))
        
        sendbutton.setTitle("派工", for: .normal)
        sendbutton.setTitleColor(.white, for: .normal)
        sendbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        sendbutton.backgroundColor = MajorColor().oceanSubColor
        sendbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        addSubview(sendbutton)
        sendbutton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        sendbutton.layer.cornerRadius = 48.calcvaluey()/2
        sendbutton.centerXInSuperview()
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.anchor(top: title.bottomAnchor, leading: leadingAnchor, bottom: sendbutton.topAnchor, trailing: trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 64.calcvaluex(), bottom: 25.calcvaluey(), right: 0))
        
    }
    @objc func closeView(){
        delegate.closeConfirmView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleConfirmViewCell:UITableViewCell {
    //久大行銷股份有限公司 機台運作問題 台中市西屯區文心路三段241號17樓 葉德雄 2019年9月22日 上午9:00 交機組 - 廖漢祥 機械組 - 黃書賢
    let labelarray = ["報修公司:","報修問題:","地點:","派工人員:","維修時間:","維修人員:"]
    let contentarray = ["久大行銷股份有限公司","機台運作問題","台中市西屯區文心路三段241號17樓","葉德雄","2019年9月22日 上午9:00","交機組 - 廖漢祥\n\n機械組 - 黃書賢"]
    let seperator = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let stackview = UIStackView()
        stackview.axis = .vertical
        stackview.spacing = 20.calcvaluey()
        
        for i in labelarray {
            let label = UILabel()
            label.text = i
            label.textAlignment = .right
            label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
            label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//
//            let content = UILabel()
//            content.text = contentarray[i]
//            content.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
//            content.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//
//            let hstack = UIStackView(arrangedSubviews: [label,content])
//            hstack.axis = .horizontal
//            hstack.spacing = 10.calcvaluex()
            
            
            stackview.addArrangedSubview(label)
        }
        stackview.addArrangedSubview(UIView())
        addSubview(stackview)
        stackview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 140.calcvaluex(), height: 0))
        
        seperator.backgroundColor = #colorLiteral(red: 0.8704903722, green: 0.8706433177, blue: 0.8704920411, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 64.calcvaluex()),size: .init(width: 426.calcvaluex(), height: 1.calcvaluey()))
        let stackview2 = UIStackView()
        stackview2.axis = .vertical
        stackview2.spacing = 20.calcvaluey()
               for i in contentarray {
                    let label = UILabel()
                    label.text = i
                    
                    label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
                    label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
                label.numberOfLines = 0
                    
                    
                    stackview2.addArrangedSubview(label)
                }
        addSubview(stackview2)
        stackview2.addArrangedSubview(UIView())
        stackview2.anchor(top: stackview.topAnchor, leading: stackview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NewScheduleController: SampleController,NewScheduleCompleteViewDelegate {
    func closeConfirmView() {
        self.view.insertSubview(self.completeview, aboveSubview: self.fullview)
        self.view.bringSubviewToFront(nextview)
        confirmview.alpha = 0
        fullview.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()

        })
        currentcomplete = false
    }
    
    func closeCompleteView() {
        fullview.alpha = 0
        completeviewanchor.top?.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        currentcomplete = false
    }
    
    let selectedview = SelectedView()
    var selectedviewanchor:AnchoredConstraints!
    let nextview = GoNextView()
    var current = 0
    let stackview = UIStackView()
    let step1 = ScheduleFirstStep()
    var step1anchor:AnchoredConstraints!
    let step2 = ScheduleSecondStep()
    var step2anchor:NSLayoutConstraint?
    let step3 = ScheduleThirdStep()
    var step3anchor:NSLayoutConstraint?
    var fullview = UIView()
    var completeview = NewScheduleCompleteView()
    var completeviewanchor:AnchoredConstraints!
    var currentcomplete = false
    var confirmview = ScheduleConfirmView()
    var isEdit = false
    var selected_fix : FixModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "建立派工".localized
        backbutton.isHidden = false
        settingButton.isHidden = true
        
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        
        for i in ["STEP1 選擇報修問題".localized,"STEP2 選擇維修時間".localized,"STEP3 選擇維修人員&維修內容".localized] {
            let step = StepView()
            step.bottomlabel.text = i
            stackview.addArrangedSubview(step)
        }
        
        view.addSubview(stackview)
        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 34.calcvaluey()))
        
        
        view.addSubview(selectedview)
        let st = stackview.arrangedSubviews[0] as! StepView
        st.bottomlabel.textColor = MajorColor().oceanSubColor
        selectedviewanchor = selectedview.anchor(top: nil, leading: st.leadingAnchor, bottom: nil, trailing: st.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 8.calcvaluey()))
        selectedview.centerYAnchor.constraint(equalTo: st.topview.centerYAnchor).isActive = true
        
        step1.backgroundColor = .clear
        view.addSubview(step1)
        step1anchor = step1.anchor(top: stackview.bottomAnchor, leading:view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 27.calcvaluex(), bottom: 0, right: 0),size: .init(width: 971.calcvaluex(), height: 588.calcvaluey()))
        
        view.addSubview(step2)

        step2.anchor(top: step1.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 972.calcvaluex(), height: 0))
        step2anchor = step2.leadingAnchor.constraint(equalTo: view.trailingAnchor)
        step2anchor?.isActive = true
       
        view.addSubview(step3)
        step3.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        step3.anchor(top: step2.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 972.calcvaluex(), height: 0))
        step3anchor = step3.leadingAnchor.constraint(equalTo: view.trailingAnchor)
        step3anchor?.isActive = true
        fullview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(fullview)
        fullview.fillSuperview()
        fullview.alpha = 0
        
        view.addSubview(completeview)
        completeview.delegate = self
        completeviewanchor = completeview.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 567.calcvaluey()))
        view.addSubview(nextview)
        nextview.backgroundColor = .white
        nextview.addtopshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        nextview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 92.calcvaluey()))
        nextview.nextbutton.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        nextview.nextbutton.isUserInteractionEnabled = true
        nextview.previewButton.addTarget(self, action: #selector(goPreview), for: .touchUpInside)

        view.addSubview(confirmview)
        confirmview.centerInSuperview(size: .init(width: 704.calcvaluex(), height: 600.calcvaluey()))
        confirmview.alpha = 0
        confirmview.delegate = self
        
        if isEdit {
            self.view.layoutIfNeeded()
            self.gonext()
        }
        
    }
    @objc func goPreview(){
        for (index,i) in step3.work_array.enumerated() {
            //for j in i.worker {
                
            if !checkWorkerDidSelect(data: i.worker) {
                    
                let alert = UIAlertController(title: "\(Date().convertToDateComponent(text: i.time, format: "yyyy-MM-dd HH:mm:ss", addEight: false))\("未選擇派工人員".localized)", message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
                        self.step3.collectionView(self.step3.topview, didSelectItemAt: IndexPath(item: index, section: 0))
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
          

        }
        let con = ConfirmScheduleController()
        
        con.isPreview = true
        if isEdit {
            con.fixModel = self.selected_fix
        }
        else{
        con.fixModel = self.step1.fix_array[self.step1.selectedIndex]
        }
        con.works = self.step3.work_array
        con.modalPresentationStyle = .overCurrentContext
        self.present(con, animated: false, completion: nil)
        
    }
    override func popview() {
        current -= 1
        if current < 0 || (isEdit && current == 0){
            
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        if current == 0{
            backbutton.image = #imageLiteral(resourceName: "ic_back")
            step2anchor?.isActive = false
            step2anchor = step2.leadingAnchor.constraint(equalTo: view.trailingAnchor)
            step2anchor?.isActive = true
        }
        else if current == 1{
            
            step3anchor?.isActive = false
            step3anchor = step3.leadingAnchor.constraint(equalTo: view.trailingAnchor)
            step3anchor?.isActive = true
        }
        let st = stackview.arrangedSubviews[current] as! StepView
        let st2 = stackview.arrangedSubviews[current+1] as! StepView
        selectedview.centerxconstraint.constant = (st.frame.width * CGFloat((current)))/2
        selectedviewanchor.trailing?.constant = st2.frame.width * CGFloat((current))
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()

        }) {
            _ in
            st.bottomlabel.textColor = MajorColor().oceanSubColor
            st2.bottomlabel.textColor = MajorColor().oceanlessColor
            if self.current == 0{
                self.nextview.hinttext.text = "請選擇一筆報修服務".localized
                self.step1.isHidden = false
                
                //self.step2.isHidden = true
                
            }
            else if self.current == 1{
                self.nextview.hinttext.text = "可新增多個維修時間".localized
               // self.step3.isHidden = true
                self.step2.isHidden = false
                self.nextview.previewButton.isHidden = true
               
                
            }
            self.nextview.nextbutton.setTitle("下一步".localized, for: .normal)
        }
        
    }
    func checkWorkerDidSelect(data:[Worker_Group]) -> Bool{
        
       
        for i in data {
            for j in i.workers{
                if j.isSelected == true{
                    return true
                    
                }
            }

        }
        return false
    }
    @objc func gonext(){
       
        
        if current < stackview.arrangedSubviews.count - 1{

            if current == 0 {
                if step1.fix_array.count == 0{
                    let alert = UIAlertController(title: "請選擇一筆報修".localized, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                step2anchor?.isActive = false
                step2anchor = step2.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 26.calcvaluex())
                step2anchor?.isActive = true
                
            }
            else if current == 1{
                if step2.date_array.count == 0{
                    let alert = UIAlertController(title: "請增加比派工時間".localized, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                step3anchor?.isActive = false
                step3anchor = step3.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 26.calcvaluex())
                step3anchor?.isActive = true
                step3.id = step1.fix_array[step1.selectedIndex].id ?? ""
                step3.machine_code = step1.fixModel?.appliance != nil ? step1.fixModel?.appliance?.code : step1.fixModel?.appliance_name
                step3.date_array = step2.date_array

                
            }
        
        let st = stackview.arrangedSubviews[current] as! StepView
        let st2 = stackview.arrangedSubviews[current+1] as! StepView
        selectedview.centerxconstraint.constant = (st.frame.width * CGFloat((current + 1)))/2
        selectedviewanchor.trailing?.constant = st2.frame.width * CGFloat((current + 1))
            selectedviewanchor.leading?.constant = st2.frame.width * CGFloat((current + 1))
            self.backbutton.tintColor = MajorColor().oceanlessColor
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
                self.backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
//                self.backbutton.setImage(#imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate), for: .normal)
            }) { (_) in
                st.bottomlabel.textColor = MajorColor().oceanlessColor
                st2.bottomlabel.textColor = MajorColor().oceanSubColor
                
                if self.current == 0{
                    self.nextview.hinttext.text = "請選擇一筆報修服務".localized
                    
                }
                else if self.current == 1{
                    self.nextview.hinttext.text = "可新增多個維修時間".localized
                    
                    self.step1.isHidden = true
                   
                    
                }
                else{
                    self.nextview.previewButton.isHidden = false
                    self.nextview.hinttext.text = "請選擇維修人員".localized
                    self.nextview.nextbutton.setTitle("確認派工".localized, for: .normal)
                    self.step2.isHidden = true
                }
            }

        current += 1
        }
        else{

            for (index,i) in step3.work_array.enumerated() {
                //for j in i.worker {
                    
                if !checkWorkerDidSelect(data: i.worker) && !checkWorkerDidSelect(data: i.history){
                    let alert = UIAlertController(title: "\(Date().convertToDateComponent(text: i.time, format: "yyyy-MM-dd HH:mm:ss", addEight: false))\("未選擇派工人員".localized)", message: nil, preferredStyle: .alert)
                    let action = UIAlertAction(title: "確定".localized, style: .default) { (_) in
                        self.step3.collectionView(self.step3.topview, didSelectItemAt: IndexPath(item: index, section: 0))
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                    }

              

            }
            let con = ConfirmScheduleController()
            if isEdit {
                con.fixModel = self.selected_fix
            }
            else{
            con.fixModel = self.step1.fix_array[self.step1.selectedIndex]
            }
            con.works = self.step3.work_array
            con.modalPresentationStyle = .overCurrentContext
            self.present(con, animated: false, completion: nil)

        }
    }
}
