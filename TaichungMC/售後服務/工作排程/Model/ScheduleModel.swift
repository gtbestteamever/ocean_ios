//
//  ScheduleModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/13/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct ScheduleEmployeeModel : Codable{
    var data : [ScheduleEmployee]
}

class ScheduleEmployee : Codable {
    var id : String
    var username : String?
    var name : String?
    var is_vacation : Int
    var workload : Int
    var service_groups : [ScheduleGroup]
    var isSelected : Bool?
}

struct ScheduleGroup : Codable {
    var code : String
    var title : String?
    var titles : Lang?
}


struct WorkDetailModel : Codable {
    var data : [WorkDetail]
}
struct UpdatedWorkDetail : Codable {
    var data : WorkDetail?
}
struct WorkDetail : Codable {
    var id : String
    var reason : String?
    var status : String?
    var owner : WorkOwner?
    var service_personnels : [ScheduleEmployee]
    var description : String?
    var account : Customer?
    var upkeep : FixModel?
    var created_at : String?
    var completed_at : String?
    var worked_at : String?
    var scheduled: String?
}
struct WorkOwner : Codable {
    var username : String?
    var name : String?
}

class Worker_Group  {
    var code : String
    var name : String
    var names : Lang?
    var workers : [ScheduleEmployee]
    
    init(code:String,name:String,names:Lang? = nil,workers: [ScheduleEmployee]) {
        self.code = code
        self.name = name
        self.names = names
        self.workers = workers
    }
}

class NewWork {
    var time: String
    var work_id:String
    var worker : [Worker_Group]
    var history : [Worker_Group]
    var description : String = ""
    init(time:String,work_id:String,worker : [Worker_Group],history : [Worker_Group]) {
        self.time = time
        self.work_id = work_id
        self.worker = worker
        self.history = history
    }
}
