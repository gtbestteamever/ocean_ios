//
//  CalendarController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/13/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import FSCalendar
protocol selectTimeDelegate {
    func select(date:Date,index:Int)
}
class CalendarController : UIViewController,FSCalendarDelegate,FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT+8")
        formatter.dateFormat = "yyyy-MM-dd"
//        let dc = formatter.date(from: formatter.string(from: date))
        
        con?.titleview.label.text = date.getCurrentMandarinTime()
        con?.date = date
        
        con?.fetchApi()
//        if con?.shrink == true{
//            con?.fetchWork()
//        }
        if let delegate = delegate {
            delegate.select(date: date, index: self.view.tag)
        }
        self.dismiss(animated: false, completion: nil)
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
        
        return min_date ?? calendar.minimumDate
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        return max_date ?? calendar.maximumDate
    }
    let container = SampleContainerView()
    var calendar : FSCalendar?
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
    weak var con : ScheduleWorkController?
    var selected_date : Date?
    var delegate : selectTimeDelegate?
    var min_date : Date?
    var max_date : Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(container)
        container.label.text = "日曆".localized
        container.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 620.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 400.calcvaluey()))
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        calendar = FSCalendar()
        calendar?.dataSource = self
        //print(992,min_date,max_date)
        //calendar?.dataSource = self

            calendar?.delegate = self
        
       
        container.addSubview(calendar!)
        
        calendar?.select(self.selected_date)
        calendar?.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 16.calcvaluex(), bottom: 16.calcvaluey(), right: 16.calcvaluex()))
        calendar?.locale = Locale(identifier: UserDefaults.standard.getConvertedLanguage())
        //calendar?.appearance.headerDateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM", options: 0, locale: Locale(identifier: "zh-TW"))
        calendar?.appearance.titleFont = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        calendar?.appearance.headerTitleFont = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        for (index,item) in (calendar?.calendarWeekdayView.weekdayLabels ?? []).enumerated() {
            item.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
            switch index {
            case 0:
                item.text = "週日".localized
            case 1:
                item.text = "週一".localized
            case 2:
                item.text = "週二".localized
            case 3:
                item.text = "週三".localized
            case 4:
                item.text = "週四".localized
            case 5:
                item.text = "週五".localized
            case 6:
                item.text = "週六".localized
            default: ()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        calendar?.select(selected_date)
        calendar?.reloadData()

        
    }
}
