//
//  CustomView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/13.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
//class GoNextButton:UIButton {
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        //UIView.setAnimationsEnabled(false)
//    }
//}
class TopViewButton : UIButton {
    init(text:String) {
        super.init(frame: .zero)
        backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        self.setTitle(text, for: .normal)
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        self.contentEdgeInsets = .init(top: 0, left: 34.calcvaluex(), bottom: 0, right: 34.calcvaluex())
        self.layer.cornerRadius = 38.calcvaluey()/2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class LeftStackView : UIStackView {
    init(textarray:[String]) {
        super.init(frame: .zero)
        axis = .vertical
        distribution = .fill

        for (index,st) in textarray.enumerated() {
            let bt = NewTypeButton(type: .custom)
            bt.newlabel.text = st
            bt.tag = index
            //bt.addTarget(self, action: #selector(buttonselected), for: .touchUpInside)
            if index == 0{
                bt.backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
                bt.newlabel.textColor = .white
                bt.newlabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
                
            }
            
            //typeButtonarray.append(bt)

            addArrangedSubview(bt)
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PriceLabel : UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        self.font = UIFont(name: "Roboto-Bold", size: 24.calcvaluex())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CreatTimeLabel: UILabel {
    
    init(text:String) {
        super.init(frame: .zero)
        self.text = text
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        self.font = UIFont(name: "Roboto-Light", size: 14.calcvaluex())
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class QuoteLabel:UILabel {
    init(text:String) {
        super.init(frame: .zero)
        self.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        self.text = text
        self.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        self.textAlignment = .center
        self.clipsToBounds = true
    }
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 13.calcvaluex())))
    }
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return .init(width: size.width + 12.calcvaluex() + 14.calcvaluex(), height: size.height)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class MachineLabel: UILabel {
    init(text:String) {
        super.init(frame: .zero)
        self.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        self.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.text = text
        self.clipsToBounds = true
        self.textAlignment = .center
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
    }
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 32.calcvaluex())))
    }
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return .init(width: size.width + 64.calcvaluex(), height: size.height)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class StatusLabel : UILabel {
    override var intrinsicContentSize: CGSize {
        let contentsize = super.intrinsicContentSize
        return .init(width: contentsize.width + 32.calcvaluex(), height: contentsize.height + 16.calcvaluey())
    }
    init(text:String,color:UIColor) {
        super.init(frame: .zero)
        self.textAlignment = .center
        self.layer.borderWidth = 1.calcvaluex()
        self.layer.borderColor = color.cgColor
        
        self.text = text
        self.textColor = color
        self.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        self.layer.cornerRadius = 15.calcvaluey()
        
        //self.constrainWidth(constant: 80.calcvaluex())
        self.constrainHeight(constant: 30.calcvaluey())
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NumberOfSelectionView:UIView {
    let label = UILabel()
    var PaymentArray = [PaymentSelectionView]()
    init(title:String,texts:[String],widths:[CGFloat],number:Int){
        super.init(frame: .zero)
        addSubview(label)
        label.text = title
        label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        for i in 0...number-1 {
            let pay = PaymentSelectionView()
            pay.selectlabel.text = texts[i]
            addSubview(pay)
            if i == 0{
            pay.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: widths[i], height: 25.calcvaluey()))
            }
            else{
                pay.anchor(top: nil, leading: PaymentArray[i-1].trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: widths[i], height: 25.calcvaluey()))
                pay.centerYAnchor.constraint(equalTo: PaymentArray[i-1].centerYAnchor).isActive = true
                
            }
            PaymentArray.append(pay)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomSelectionView:UIView{
    let label = UILabel()
    let firstselect = PaymentSelectionView()
    let secondselect = PaymentSelectionView()
    let thirdselect = PaymentSelectionView()
    init(widths:[CGFloat],texts:[String],title:String) {
        super.init(frame: .zero)
        addSubview(label)
        label.text = title
        label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        
        firstselect.selectlabel.text = texts[0]
        secondselect.selectlabel.text = texts[1]
            thirdselect.selectlabel.text = texts[2]
        addSubview(firstselect)
        firstselect.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: widths[0].calcvaluex(), height: 25.calcvaluey()))
        
        addSubview(secondselect)
        secondselect.anchor(top: nil, leading: firstselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: widths[1].calcvaluex(), height: 25.calcvaluey()))
        secondselect.centerYAnchor.constraint(equalTo: firstselect.centerYAnchor).isActive = true
        
        addSubview(thirdselect)
        thirdselect.anchor(top: nil, leading: secondselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: widths[2].calcvaluex(), height: 25.calcvaluey()))
        thirdselect.centerYAnchor.constraint(equalTo: secondselect.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CalcSelectionView: UIView {
    let label = UILabel()
    let firstselect = PaymentSelectionView()
    let secondselect = PaymentSelectionView()
    let thirdselect = PaymentSelectionView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        firstselect.selectlabel.text = "現金"
        secondselect.selectlabel.text = "轉帳"
            thirdselect.selectlabel.text = "支票"
        addSubview(firstselect)
        firstselect.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
        
        addSubview(secondselect)
        secondselect.anchor(top: nil, leading: firstselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
        secondselect.centerYAnchor.constraint(equalTo: firstselect.centerYAnchor).isActive = true
        
        addSubview(thirdselect)
        thirdselect.anchor(top: nil, leading: secondselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
        thirdselect.centerYAnchor.constraint(equalTo: secondselect.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension UIImageView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.05, animations: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (_) in
            UIView.animate(withDuration: 0.05) {
                self.transform = .identity
            }
        }
    }
}
extension UIButton {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.05, animations: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (_) in
            UIView.animate(withDuration: 0.05) {
                self.transform = .identity
            }
        }
    }
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return bounds.insetBy(dx: -10.calcvaluex(), dy: -10.calcvaluex()).contains(point)
    }
}
class SignButtonView: UIView {
    let newbutton = UIButton(type: .system)
    let importbutton = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        newbutton.setTitle("新增", for: .normal)
        newbutton.setTitleColor(.white, for: .normal)
        newbutton.backgroundColor = .black
        newbutton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        importbutton.setTitle("匯入", for: .normal)
        importbutton.setTitleColor(.white, for: .normal)
        importbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        importbutton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
//        addSubview(newbutton)
//        newbutton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 9.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 32.calcvaluey()))
        newbutton.layer.cornerRadius = 32.calcvaluey()/2
        //newbutton.centerYInSuperview()
//        addSubview(importbutton)
//        importbutton.anchor(top: nil, leading: newbutton.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 32.calcvaluey()))
//        importbutton.centerYInSuperview()
        importbutton.layer.cornerRadius = 32.calcvaluey()/2
        
        newbutton.constrainHeight(constant: 32.calcvaluey())
        importbutton.constrainHeight(constant: 32.calcvaluey())
        let stackview = UIStackView(arrangedSubviews: [newbutton,importbutton])
        stackview.axis = .horizontal
        stackview.spacing = 8.calcvaluex()
        stackview.distribution = .fillEqually
        stackview.alignment = .center
        addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 0, left: 9.calcvaluex(), bottom: 0, right: 9.calcvaluex()))
        
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SignView : UIView {
    let signlabel = UILabel()
    let signcontent = SignButtonView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        signlabel.text = "客戶簽名"
        signlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        signlabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        signlabel.textAlignment = .center
        addSubview(signlabel)
        
        signlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        addSubview(signcontent)
        
        signcontent.anchor(top: signlabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CalcView : UIView {
    let showview = UIView()
    
    let labelview = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)

        showview.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        showview.layer.cornerRadius = 15.calcvaluex()

        addSubview(showview)
        showview.fillSuperview()

        labelview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        labelview.clipsToBounds = true
        labelview.layer.cornerRadius = 15.calcvaluex()
        labelview.textColor = .white
        labelview.textAlignment = .center
        labelview.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(labelview)
        labelview.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 42.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ComponentView: UIView {
    let titlelabel = UILabel()
    let contentLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        titlelabel.text = "故障代碼"
        titlelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        titlelabel.textColor = globolcolor
        addSubview(titlelabel)
        titlelabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        contentLabel.isHidden = true
        addSubview(contentLabel)
        contentLabel.numberOfLines = 2
        contentLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        contentLabel.textColor = globolcolor
        contentLabel.text = "A0347"
        contentLabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 28.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class NormalFormView: UIView {
    let tlabel = UILabel()
    let textfield = FormTextField()
    var textfieldanchor:AnchoredConstraints!
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.textColor = #colorLiteral(red: 0.1155835316, green: 0.07696569711, blue: 0.07334197313, alpha: 1)
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        addSubview(textfield)
        textfieldanchor = textfield.anchor(top: tlabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 52.calcvaluey()))
        textfield.layer.cornerRadius = 52.calcvaluey()/2
        textfield.addToolBar()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol FormTextViewDelegate {
    func textViewReloadHeight(height:CGFloat)
    func sendTextViewData(mode:FormTextViewMode,text:String)
    func didBegin(mode:FormTextViewMode)
}
extension FormTextViewDelegate {
    func didBegin(mode:FormTextViewMode) {

    }
}
enum FormTextViewMode {
    case MoreInfo
    case DiscountInfo
    case DiscountPrice
    case CreateCustomer
    case FirstDouble
    case SecondDouble
    case EquipModel
    case EquipBrand
    case EquipYear
    case Industry
    case Date
    case Reply
    case Info
    case Maintain
    case MaintainInfo
    case AutoFix
    case Customer
    case Address
    
    case InitalField
    case ContactName
    case ContactPhone
    case ContactAdress
    
    case Reason
    case Initial
    case Method
    
    case changeReason
    case componentOwner
    case collectInfo
    
    case EngineerError
    case EngineerId
    case EngineerName
    case EngineerPoint
    case EngineerInsurance
    case EngineerCar
    case EngineerDuration
    case EngineerDesign
    case EngineerTest
    case EngineerSpecial
    case EngineerOther
    
    case EditTime
    case EditEmployee
    case EditDescription
    
    case ScheduleProblem
    
    case InterviewLocation
    case InterviewMoreInfo
    case InterviewCustomer
    
    case Distance
    case DistanceFee
    
    case SelectAddress
}
protocol InfoTextViewDelegate {
    func textViewReloadHeight(height:CGFloat,mode:FormTextViewMode?)
   
}
class FormInfoField : UITextView,UITextViewDelegate {
    var n_width : CGFloat = 0
    var attributedPlaceholder : NSAttributedString?
    var placeholderLabel : UILabel = {
       let pL = UILabel()
        pL.textColor = .black
        pL.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //pL.text = "請輸入"
        
        return pL
    }()
    override var text: String!{
        didSet{
            if text != "" {
                placeholderLabel.isHidden = true
            }
            else{
                placeholderLabel.isHidden = false
            }

            
            
        }
    }
    var mode : FormTextViewMode?
    var anchorHeight:NSLayoutConstraint?
    var t_delegate:FormTextViewDelegate?
    var q_delegate : InfoTextViewDelegate?
    var minusSignLabel : UILabel = {
       let pL = UILabel()
        pL.textColor = .black
        pL.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        pL.text = "-"
        
        return pL
    }()
    init(placeText:String,mode:FormTextViewMode,setMinus : Bool = false) {
        super.init(frame: .zero, textContainer: nil)
        backgroundColor = .red
        self.mode = mode
        self.textContainerInset = .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 16.calcvaluey(), right: 26.calcvaluex())
        self.textContainer.lineBreakMode = .byWordWrapping
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)

        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        //self.isScrollEnabled = false
       // self.delegate = self

        addSubview(placeholderLabel)
        placeholderLabel.text = placeText
        placeholderLabel.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 28.calcvaluex(), bottom: 16.calcvaluey(), right: 26.calcvaluex()))
        if setMinus{
        addSubview(minusSignLabel)
        minusSignLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        minusSignLabel.centerYInSuperview()
        }
//        self.translatesAutoresizingMaskIntoConstraints = false
//        self.anchorHeight = self.heightAnchor.constraint(equalToConstant: 52.calcvaluey())
//        self.anchorHeight?.isActive = true
       self.delegate = self
    }
//    override init(frame: CGRect, textContainer: NSTextContainer?) {
//
//
//    }
    let picker = UIDatePicker()
    var industries = [Industry]()
    var con : CreateCustomerDetailView?
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if mode == .Industry{
            print(331)
            let vd = IndustryTypeSelectionController()
            vd.customersList = self.industries
            vd.modalPresentationStyle = .overCurrentContext
            vd.con = self.con
            
            General().getTopVc()?.present(vd, animated: false, completion: nil)
            return false
        }

        if mode == .Date{
            picker.datePickerMode = .date
            tintColor = .clear
            self.inputView = picker
            
            
        }
        

        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != ""{
        placeholderLabel.isHidden = true
        }
       
        let usedWidth : CGFloat = n_width
//        print(221,usedWidth)
//        if mode == .MoreInfo{
//            usedWidth = textViewInfoWidth
//        }
//        else if mode == .DiscountInfo{
//            usedWidth = textViewDiscountInfoWidth
//        }
//        else if mode == .DiscountPrice{
//            usedWidth = 180.calcvaluex()
//        }
//        else if mode == .CreateCustomer{
//            usedWidth = textViewCustomerDetailWidth
//        }
        
        let size = CGSize(width: usedWidth, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        self.anchorHeight?.constant = estimatedSize.height
        
        q_delegate?.textViewReloadHeight(height:estimatedSize.height,mode: self.mode)
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""{
            placeholderLabel.isHidden = false
        }
        else{
            placeholderLabel.isHidden = true
        }
        if mode == .Date{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let st = formatter.string(from: picker.date)
            
            t_delegate?.sendTextViewData(mode: mode ?? .Date, text: st)
            return
        }
        if let txt = textView.text,let mode = self.mode {
            t_delegate?.sendTextViewData(mode: mode, text: txt)
        }
    }
  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FormTextView:UITextView,UITextViewDelegate{
    var attributedPlaceholder : NSAttributedString?
    var placeholderLabel : UILabel = {
       let pL = UILabel()
        pL.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        pL.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //pL.text = "請輸入"
        
        return pL
    }()
    override var text: String!{
        didSet{
            
            if text != ""{
                placeholderLabel.isHidden = true
            }
            else{
                placeholderLabel.isHidden = false
            }

            
            
        }
    }
    var button : UIButton?
    var mode : FormTextViewMode?
    var anchorHeight:NSLayoutConstraint?
    var t_delegate:FormTextViewDelegate?
    var pre_delegate : EditingInterviewDelegate?
    var minusSignLabel : UILabel = {
       let pL = UILabel()
        pL.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        pL.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        pL.text = "-"
        
        return pL
    }()
    var initialHeight : CGFloat = 0
    init(placeText:String,mode:FormTextViewMode,setMinus : Bool = false) {
        super.init(frame: .zero, textContainer: nil)
        self.mode = mode
        self.textContainerInset = .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 16.calcvaluey(), right: 26.calcvaluex())
        self.textContainer.lineBreakMode = .byWordWrapping
        backgroundColor = .white
        
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8133818507, green: 0.8101347685, blue: 0.8136510849, alpha: 1)
        
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        self.isScrollEnabled = false
       // self.delegate = self

        addSubview(placeholderLabel)
        placeholderLabel.text = placeText
        placeholderLabel.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 28.calcvaluex(), bottom: 16.calcvaluey(), right: 26.calcvaluex()))
        if setMinus{
        addSubview(minusSignLabel)
        minusSignLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        minusSignLabel.centerYInSuperview()
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        initialHeight = 52.calcvaluey()
        layer.cornerRadius = 52.calcvaluey()/2
        self.anchorHeight = self.heightAnchor.constraint(greaterThanOrEqualToConstant: initialHeight)
        self.anchorHeight?.isActive = true
       self.delegate = self
    }
//    override init(frame: CGRect, textContainer: NSTextContainer?) {
//
//
//    }
    let picker = UIDatePicker()
    var industries = [Industry]()
    var con : CreateCustomerDetailView?
    var con2 : AddNewServiceController?
    var isExtend = false
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if mode == .Industry{
            print(331)
            let vd = IndustryTypeSelectionController()
            vd.customersList = self.industries
            vd.modalPresentationStyle = .overCurrentContext
            vd.con = self.con
            
            General().getTopVc()?.present(vd, animated: false, completion: nil)
            return false
        }
        if mode == .InterviewCustomer {
            pre_delegate?.chooseCustomers(textField: UITextField())
            return false
        }
        if mode == .AutoFix{
            let vd = SelectFixController()
            vd.container.con = con2
            vd.modalPresentationStyle = .overCurrentContext
            con2?.present(vd, animated: false, completion: nil)
            return false
        }
        if mode == .Customer{
            let vd = CustomerSelectionView()
            vd.getCustomersList()
            vd.con2 = self.con2
//            vd.customersList = self.customersList
//            vd.con = self
            //vd.customerList = self.customersList
            vd.modalPresentationStyle = .overCurrentContext
            
            con2?.present(vd, animated: false, completion: nil)
            return false
        }
        if mode == .Date{
            picker.datePickerMode = .date
            tintColor = .clear
            self.inputView = picker
            
            
        }
        if let mode = mode, mode == .EditTime || mode == .EditEmployee {
            
            self.t_delegate?.didBegin(mode: mode)
            return false
        }
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if mode == .ScheduleProblem{
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 255    // 10 Limit Value
        }
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        
        if mode == .ScheduleProblem{
            textView.text = String(textView.text.prefix(255))
        }
        if textView.text != ""{
            button?.tintColor = MajorColor().oceanlessColor
            button?.isUserInteractionEnabled = true
        }
        else{
            button?.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            button?.isUserInteractionEnabled = false
        }
        if textView.text != "" {
        placeholderLabel.isHidden = true
        }
//        textView.sizeToFit()
//        t_delegate?.textViewReloadHeight(height: 0)
        var usedWidth : CGFloat = 0
        if mode == .MoreInfo{
            usedWidth = textViewInfoWidth
        }
        else if mode == .DiscountInfo{
            usedWidth = textViewDiscountInfoWidth
        }
        else if mode == .DiscountPrice{
            usedWidth = 180.calcvaluex()
        }
        else if mode == .CreateCustomer{
            usedWidth = textViewCustomerDetailWidth
        }

        else if mode == .Maintain || mode == .MaintainInfo || mode == .Address{

            usedWidth = textView.frame.width

        }
        else{
            usedWidth = textView.frame.width
        }
        
        let size = CGSize(width: textView.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)


        if estimatedSize.height > initialHeight + 5.calcvaluey() {
            isExtend = true
            self.anchorHeight?.constant = estimatedSize.height
            t_delegate?.textViewReloadHeight(height:estimatedSize.height)
        }
        else{
            if isExtend {
                isExtend = false
                self.anchorHeight?.constant = initialHeight
                t_delegate?.textViewReloadHeight(height: initialHeight)
            }
        }
        
        

        
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""{
            placeholderLabel.isHidden = false
        }
        else{
            placeholderLabel.isHidden = true
        }
        if mode == .Date{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let st = formatter.string(from: picker.date)
            
            t_delegate?.sendTextViewData(mode: mode ?? .Date, text: st)
            return
        }
        if let txt = textView.text,let mode = self.mode {
            t_delegate?.sendTextViewData(mode: mode, text: txt)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NormalTextView:UIView {

    let tlabel = UILabel()
    let textfield = FormTextView(placeText: "", mode: .MoreInfo)
    var color = UIColor.white
    var isImportant = false
    var mode : InterviewMode? {
        didSet{
            if mode == .Review{
                textfield.placeholderLabel.text = ""
            }
        }
    }
    func makeImportant(){
        isImportant = true
        let mutablestring = NSMutableAttributedString(string: "* ", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.8249840736, green: 0.1374278367, blue: 0.1237704381, alpha: 1)])
        
        mutablestring.append(NSAttributedString(string: tlabel.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:self.color]))
        tlabel.attributedText = mutablestring
    }
    let stackview = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
    init(text:String,placeholdertext:String? = nil,color:UIColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)) {
        super.init(frame: .zero)
        self.color = color
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        tlabel.text = text
        tlabel.textColor = self.color
        textfield.autocapitalizationType = .none
        textfield.autocorrectionType = .no
        textfield.placeholderLabel.text = placeholdertext
        
        addSubview(stackview)
        stackview.addArrangedSubview(textfield)
        stackview.anchor(top: tlabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SalesFormView : NormalFormView {
    init(text:String) {
        super.init(frame: .zero)
        tlabel.text = text
        tlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        tlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        textfieldanchor.height?.constant = 61.calcvaluey()
        textfield.isUserInteractionEnabled = false
        textfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        textfield.layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SalesHeaderView : UIView {
    let img = UIImageView(image: #imageLiteral(resourceName: "ic_user_message"))
    let nameLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        img.contentMode = .scaleAspectFill
        addSubview(img)
        img.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 48.calcvaluex(), height: 48.calcvaluex()))
        img.centerXInSuperview()
        
        addSubview(nameLabel)
        nameLabel.text = "盧致宏"
        nameLabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        nameLabel.font = UIFont(name: "Roboto-Medium", size: 28.calcvaluex())
        
        nameLabel.anchor(top: img.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 40.calcvaluey()))
        
        nameLabel.centerXInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TimeFormView: UIView {
    let tlabel = UILabel()
    let fromtextfield = FormTextField()
    let symbol = UILabel()
    let totextfield = FormTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.textColor = #colorLiteral(red: 0.1155835316, green: 0.07696569711, blue: 0.07334197313, alpha: 1)
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        

        addSubview(fromtextfield)
        fromtextfield.anchor(top: tlabel.bottomAnchor, leading: tlabel.leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 6.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 261.calcvaluex(), height: 52.calcvaluey()))
        
        addSubview(symbol)
        symbol.text = "～"
        symbol.textColor = #colorLiteral(red: 0.1155835316, green: 0.07696569711, blue: 0.07334197313, alpha: 1)
        symbol.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        symbol.anchor(top: nil, leading: fromtextfield.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0))
        symbol.centerYAnchor.constraint(equalTo: fromtextfield.centerYAnchor).isActive = true
        addSubview(totextfield)
        totextfield.anchor(top: fromtextfield.topAnchor, leading: symbol.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 261.calcvaluex(), height: 52.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol FormToolBarDelegate {
    func toolbarDonePressed()
}
class FormTextField:UITextField{
    var toolBarDelegate:FormToolBarDelegate!
    var padding : CGFloat?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        //layer.cornerRadius = 15.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
        self.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
    }
    
    override func donePressed() {
        super.donePressed()
        if toolBarDelegate != nil{
        toolBarDelegate.toolbarDonePressed()
        }
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: padding == nil ? 23.calcvaluex() : padding!, bottom: 0, right: padding == nil ? 23.calcvaluex() : padding!))
        //return bounds.insetBy(dx: padding == nil ? 23.calcvaluex() : padding!, dy: 0)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: padding == nil ? 23.calcvaluex() : padding!, bottom: 0, right: padding == nil ? 23.calcvaluex() : padding!))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: padding == nil ? 23.calcvaluex() : padding!, bottom: 0, right: padding == nil ? 23.calcvaluex() : padding!))
    }
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.clearButtonRect(forBounds: bounds)
        return rect.offsetBy(dx: -23.calcvaluex(), dy: 0)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddressFormField:UITextField {
    let tlabel = UILabel()
    let citylabel = FilterTextField()
    let zonelabel = FilterTextField()
    let addresslabel = FormTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.textColor = #colorLiteral(red: 0.1155835316, green: 0.07696569711, blue: 0.07334197313, alpha: 1)
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        citylabel.padding = 26.calcvaluex()
        zonelabel.padding = 26.calcvaluex()
        citylabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        citylabel.text = "縣市"
        zonelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        zonelabel.text = "鄉鎮市區"
        
        addresslabel.attributedPlaceholder = NSAttributedString(string: "詳細地址", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.1155835316, green: 0.07696569711, blue: 0.07334197313, alpha: 1)])
        
        citylabel.backgroundColor = .white
        addSubview(citylabel)
        citylabel.anchor(top: tlabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 7.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 52.calcvaluey()))
        
        addSubview(zonelabel)
        zonelabel.backgroundColor = .white
        zonelabel.anchor(top: citylabel.topAnchor, leading: citylabel.trailingAnchor, bottom: citylabel.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 159.calcvaluex(), height: 0))
        
        addSubview(addresslabel)
        addresslabel.anchor(top: zonelabel.topAnchor, leading: zonelabel.trailingAnchor, bottom: zonelabel.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 387.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
