//
//  ServiceFormInfoView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/12.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
let globolcolor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
extension String{
    func importantstring()->NSMutableAttributedString {
        
        let mustring = NSMutableAttributedString(string: "*", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)])
        mustring.append(NSAttributedString(string: self, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)]))
        return mustring
    }
}
protocol ServiceFormInfoViewDelegate {
    func scrolling(indexarray:[IndexPath])
    func openAddComponent()
}
class ServiceFormInfoView: UIView {
    let infolabel = UILabel()
    let rightinfoimage = UIImageView(image: #imageLiteral(resourceName: "ic_info"))
    let tableview = UITableView(frame: .zero, style: .grouped)
    var delegate:ServiceFormInfoViewDelegate!
    var buttontap = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        infolabel.text = "如服務內容如有異議，請於七日內向我們反應，避免影響您的權益，謝謝。"
        infolabel.textColor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
        infolabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        addSubview(infolabel)
        
        infolabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        addSubview(rightinfoimage)
        rightinfoimage.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        rightinfoimage.centerYAnchor.constraint(equalTo: infolabel.centerYAnchor).isActive = true
        
        tableview.backgroundColor = .clear
        
        addSubview(tableview)
        tableview.anchor(top: infolabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.contentInset.bottom = 26.calcvaluey()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ServiceFormInfoView : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 432.calcvaluey()
        case 1:
            return 433.calcvaluey()
        case 2:
            return 154.calcvaluey()
        case 3:
            return 456.calcvaluey()
        case 4:
            return 533.calcvaluey()
        case 5:
            return 199.calcvaluey()
        case 6:
            return 323.calcvaluey()
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 53.calcvaluey()
        case 2:
            return 52.calcvaluey()
        case 3:
            return 57.calcvaluey()
        case 4:
            return 52.calcvaluey()
        case 5:
            return 52.calcvaluey()
        case 6:
            return 52.calcvaluey()
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        
        vd.backgroundColor = .clear
        
        let label = UILabel()
        switch section {
        case 1:
            label.text = "機台資訊"
        case 2:
            label.text = "換件項目列表"
        case 3:
            label.text = "費用及收款狀況".localized
        case 4:
            label.text = "工程確認"
        case 5:
            label.text = "確認簽名"
        case 6:
            label.text = "客戶意見欄"
        default:
             label.text = nil
        }
       
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        vd.addSubview(label)
        label.textColor = globolcolor
        label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 11.calcvaluey(), right: 0))
        return vd
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        let cell = FirstSectionCell(style: .default, reuseIdentifier: "cellid")
            cell.timeformview.tlabel.attributedText = "服務時間".localized.importantstring()
        cell.idformview.tlabel.text = "客戶編號"
            cell.nameformview.tlabel.attributedText = "客戶名稱".importantstring()
            cell.contactformview.tlabel.attributedText = "聯絡人".importantstring()
        cell.phoneformview.tlabel.text = "電話"
            cell.addressformview.tlabel.attributedText = "地址".importantstring()
        return cell
        }
        else if indexPath.section == 1{
            let cell = SecondSectionCell(style: .default, reuseIdentifier: "cellid2")
            cell.machinecode.tlabel.attributedText = "機號".importantstring()
            cell.machinetype.tlabel.text = "機種"
            cell.insuranceSituation.tlabel.text = "保固狀況"
            cell.currentstatus.tlabel.text = "現況描述"
            cell.reasonstatus.tlabel.text = "原因出判"
            cell.waytodo.tlabel.text = "採取對策"
            return cell
        }
        else if indexPath.section == 2{
            let cell = ThirdSectionCell(style: .default, reuseIdentifier: "cellid3")
            cell.addbutton.addTarget(self, action: #selector(addComponent), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 3{
            let cell = FourthSectionCell(style: .default, reuseIdentifier: "cellid4")
            return cell
        }
        else if indexPath.section == 4{
            let cell = FifthSectionCell(style: .default, reuseIdentifier: "cellid5")
            return cell
        }
        else if indexPath.section == 5{
            let cell = SixthSectionCell(style: .default, reuseIdentifier: "cellid6")
            return cell
        }
        else if indexPath.section == 6{
            let cell = SevenSectionCell(style: .default, reuseIdentifier: "cellid7")
            return cell
        }
        else{
            let cell = NewServieFormCell(style: .default, reuseIdentifier: "cc")
            
            return cell
        }
    }

    @objc func addComponent(){
        delegate.openAddComponent()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !buttontap {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            delegate.scrolling(indexarray: [IndexPath(item: 0, section: 6)])
        }
        else{
        delegate.scrolling(indexarray: self.tableview.indexPathsForVisibleRows!)
        }
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if buttontap {
            buttontap = false
        }
    }
}

class NewServieFormCell: UITableViewCell {
    let container = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        container.backgroundColor = .white
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class FirstSectionCell: NewServieFormCell {
    let timeformview = TimeFormView()
    let idformview = NormalFormView()
    let nameformview = NormalFormView()
    let contactformview = NormalFormView()
    let phoneformview = NormalFormView()
    let addressformview = AddressFormField()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        container.addSubview(timeformview)
        timeformview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 572.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(idformview)
        idformview.anchor(top: timeformview.bottomAnchor, leading: timeformview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(nameformview)
        nameformview.anchor(top: idformview.topAnchor, leading: idformview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 558.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(contactformview)
        contactformview.anchor(top: nameformview.bottomAnchor, leading: idformview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(phoneformview)
        phoneformview.anchor(top: contactformview.topAnchor, leading: contactformview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 558.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(addressformview)
        addressformview.anchor(top: contactformview.bottomAnchor, leading: contactformview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluex(), height: 81.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SecondSectionCell: NewServieFormCell {
    let machinecode = NormalFormView()
    let machinetype = NormalFormView()
    let insuranceSituation = NormalFormView()
    let currentstatus = NormalFormView()
    let reasonstatus = NormalFormView()
    let waytodo = NormalFormView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        container.addSubview(machinecode)
        machinecode.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 223.calcvaluex(), height: 81.calcvaluey()))
        
        container.addSubview(machinetype)
        machinetype.anchor(top: machinecode.topAnchor, leading: machinecode.trailingAnchor, bottom: machinecode.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 223.calcvaluex(), height: 0))
        
        container.addSubview(insuranceSituation)
        insuranceSituation.anchor(top: machinetype.topAnchor, leading: machinetype.trailingAnchor, bottom: machinetype.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 223.calcvaluex(), height: 0))
        
        container.addSubview(currentstatus)
        currentstatus.anchor(top: machinecode.bottomAnchor, leading: machinecode.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluey(), height: 81.calcvaluey()))
        
        container.addSubview(reasonstatus)
        reasonstatus.anchor(top: currentstatus.bottomAnchor, leading: currentstatus.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluey(), height: 81.calcvaluey()))
        
        container.addSubview(waytodo)
        waytodo.anchor(top: reasonstatus.bottomAnchor, leading: reasonstatus.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluey(), height: 81.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ThirdSectionCell: NewServieFormCell {
    let addbutton = AddButton4()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let stackview = UIStackView()
        stackview.axis = .horizontal
        stackview.spacing = 48.calcvaluex()
        stackview.alignment = .top
        addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 30.calcvaluey(), right: 46.calcvaluex()))
        
        for a in ["故障代碼","零件編號","零件價格","更換原因","收費狀況","維修狀況"]{
            let vd = ComponentView()
            
            stackview.addArrangedSubview(vd)
            vd.titlelabel.text = a
            vd.constrainWidth(constant: 64.calcvaluex())
            
            //之後使用
//            if a == "更換原因"
//            {
//
//               //vd.constrainWidth(constant: 126.calcvaluex())
//                vd.contentLabel.text = "零件正常磨損更換消耗品零件"
//                if #available(iOS 11.0, *) {
//                    stackview.setCustomSpacing(46.calcvaluex(), after: vd)
//                } else {
//                    // Fallback on earlier versions
//                }
//
//            }
//            else{
//              vd.constrainWidth(constant: 64.calcvaluex())
//            }
            
            addbutton.titles.text = "新增換件項目"
            addSubview(addbutton)
            addbutton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 26.calcvaluey(), right: 0),size: .init(width: 198.calcvaluex(), height: 38.calcvaluey()))
            addbutton.layer.cornerRadius = 38.calcvaluey()/2
            
        }
        stackview.addArrangedSubview(UIView())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FourthSectionCell: NewServieFormCell {
    let pricecallabel = UILabel()
    let pricecalsublabel = UILabel()
    let techicfee = CalcView()
    let holidayfee = CalcView()
    let componentfee = CalcView()
    let multiplyvalue = UILabel()
    let totallabel = CalcView()
    
    let paymentselect = CalcSelectionView()
    let paymenttypelabel = UILabel()
    
    let cashselect = PaymentSelectionView()
    let transferselect = PaymentSelectionView()
    let checkselect = PaymentSelectionView()
    
    let restOfPayment = NormalFormView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        pricecallabel.text = "費用計算".localized
        pricecalsublabel.text = "假日服務說明：如是夜間、休息、例假，原技術費用乘以 150%、200%、300%".localized
        
        pricecallabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        pricecallabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        pricecalsublabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        pricecalsublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        container.addSubview(pricecallabel)
        pricecallabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 23.calcvaluey(), left: 36.calcvaluey(), bottom: 0, right: 0))
        
        container.addSubview(pricecalsublabel)
        pricecalsublabel.anchor(top: pricecallabel.bottomAnchor, leading: pricecallabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        container.addSubview(techicfee)
        techicfee.labelview.text = "技術費"
        techicfee.anchor(top: pricecalsublabel.bottomAnchor, leading: pricecalsublabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 126.calcvaluex(), height: 108.calcvaluey()))
        
        let bracketview = UILabel()
        bracketview.text = "（"
        bracketview.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(bracketview)
        bracketview.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
        bracketview.anchor(top: nil, leading: nil, bottom: nil, trailing: techicfee.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 4.calcvaluex()))
        bracketview.centerYAnchor.constraint(equalTo: techicfee.centerYAnchor).isActive = true
        
        let multiplyview = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
//        multiplyview.text = "X"
//        multiplyview.textAlignment = .center
//        multiplyview.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(multiplyview)
      //  multiplyview.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
        multiplyview.anchor(top: nil, leading: techicfee.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        multiplyview.centerYAnchor.constraint(equalTo: techicfee.centerYAnchor).isActive = true
        
        container.addSubview(holidayfee)
        holidayfee.labelview.text = "假日服務".localized
        holidayfee.anchor(top: techicfee.topAnchor, leading: multiplyview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 126.calcvaluex(), height: 108.calcvaluey()))
        let addview = UIImageView(image: #imageLiteral(resourceName: "ic_addition"))
        
//        addview.text = "+"
//        addview.textAlignment = .center
//        addview.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(addview)
  //      addview.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
        addview.anchor(top: nil, leading: holidayfee.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addview.centerYAnchor.constraint(equalTo: holidayfee.centerYAnchor).isActive = true
        
        container.addSubview(componentfee)
        componentfee.labelview.text = "零件費"
        componentfee.anchor(top: holidayfee.topAnchor, leading: addview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 126.calcvaluex(), height: 108.calcvaluey()))
        
        let endbracketview = UILabel()
        endbracketview.text = "）"
        endbracketview.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(endbracketview)
        endbracketview.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
        endbracketview.anchor(top: nil, leading: componentfee.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        endbracketview.centerYAnchor.constraint(equalTo: componentfee.centerYAnchor).isActive = true
        
        let multiplyview2 = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
//        multiplyview2.text = "X"
//        multiplyview2.textAlignment = .center
//        multiplyview2.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(multiplyview2)
//        multiplyview2.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
        multiplyview2.anchor(top: nil, leading: endbracketview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: -12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        multiplyview2.centerYAnchor.constraint(equalTo: endbracketview.centerYAnchor).isActive = true
        
        container.addSubview(multiplyvalue)
        multiplyvalue.font = UIFont(name: "Roboto-Black", size: 20.calcvaluex())
        multiplyvalue.text = "1.05"
        multiplyvalue.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        
        multiplyvalue.anchor(top: nil, leading: multiplyview2.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
        multiplyvalue.centerYAnchor.constraint(equalTo: multiplyview2.centerYAnchor).isActive = true
        
        let equalview = UIImageView(image: #imageLiteral(resourceName: "ic_equal"))
//        equalview.text = "="
//        equalview.font = UIFont(name: "Roboto-Black", size: 24.calcvaluex())
//        equalview.textAlignment = .center
//        equalview.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(equalview)
        equalview.anchor(top: nil, leading: multiplyvalue.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        equalview.centerYAnchor.constraint(equalTo: multiplyvalue.centerYAnchor).isActive = true
        
        totallabel.labelview.text = "總計(含稅)"
        container.addSubview(totallabel)
        totallabel.anchor(top: nil, leading: equalview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 126.calcvaluex(), height: 108.calcvaluey()))
        totallabel.centerYAnchor.constraint(equalTo: equalview.centerYAnchor).isActive = true
        
        
        paymentselect.label.text = "收款方式".localized
        addSubview(paymentselect)
        paymentselect.anchor(top: techicfee.bottomAnchor, leading: techicfee.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 272.calcvaluex(), height: 53.calcvaluey()))
        
        
//        paymenttypelabel.text = "收款方式"
//
//
//        paymenttypelabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
//        paymenttypelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
//
//
//
//        container.addSubview(paymenttypelabel)
//        paymenttypelabel.anchor(top: techicfee.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 54.calcvaluey(), left: 36.calcvaluey(), bottom: 0, right: 0))
//
//
//        cashselect.selectlabel.text = "現金"
//        transferselect.selectlabel.text = "轉帳"
//        checkselect.selectlabel.text = "支票"
//
//        container.addSubview(cashselect)
//        cashselect.anchor(top: paymenttypelabel.bottomAnchor, leading: paymenttypelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
//
//        container.addSubview(transferselect)
//        transferselect.anchor(top: nil, leading: cashselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
//        transferselect.centerYAnchor.constraint(equalTo: cashselect.centerYAnchor).isActive = true
//        container.addSubview(checkselect)
//        checkselect.anchor(top: nil, leading: transferselect.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 40.calcvaluex(), bottom: 0, right: 0),size: .init(width: 64.calcvaluex(), height: 25.calcvaluey()))
//        checkselect.centerYAnchor.constraint(equalTo: cashselect.centerYAnchor).isActive = true
        
        restOfPayment.tlabel.text = "未收款金額"
        restOfPayment.tlabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        restOfPayment.tlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(restOfPayment)
        restOfPayment.anchor(top: paymentselect.bottomAnchor, leading: paymentselect.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluey(), height: 80.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FifthSectionCell: NewServieFormCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let vstack = UIStackView()
        vstack.axis = .vertical
        vstack.spacing = 20.calcvaluey()
        vstack.distribution = .fillEqually
        
        addSubview(vstack)
        vstack.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 26.calcvaluey(), right: 36.calcvaluey()))
        
        for a in [["工程師工號","工程師姓名","不良報告書編號"],["服務點數","保證時間"],["交車","工件估時"],["程式設計","試車工件"],["專案處理","其他"]]{
            let hstackview = UIStackView()
            hstackview.axis = .horizontal
            hstackview.distribution = .fillEqually
            hstackview.spacing = 12.calcvaluex()
            
            for j in a{
                let bt = NormalFormView()
                bt.tlabel.text = j
                hstackview.addArrangedSubview(bt)
                
            }
            
            vstack.addArrangedSubview(hstackview)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SixthSectionCell: NewServieFormCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let stackview = UIStackView()
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        stackview.spacing = 11.calcvaluex()
        for j in ["客戶簽名","服務員簽名","業務承辦簽名","服務主管簽名"]{
            let v = SignView()
            v.signlabel.text = j
            stackview.addArrangedSubview(v)
        }
        addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 34.calcvaluey(), left: 26.calcvaluex(), bottom: 34.calcvaluey(), right: 26.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SevenSectionCell: NewServieFormCell {
    let cselect1 = CustomSelectionView(widths: [100,64,82], texts: ["非常滿意","滿意","不滿意"],title: "產品滿意度")
    let cselect2 = CustomSelectionView(widths: [100,64,82], texts: ["非常滿意","滿意","不滿意"],title: "產品滿意度")
    let commentView = NormalFormView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(cselect1)
        cselect1.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluex(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 326.calcvaluex(), height: 53.calcvaluey()))
        
        addSubview(cselect2)
        cselect2.anchor(top: cselect1.bottomAnchor, leading: cselect1.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 326.calcvaluex(), height: 53.calcvaluey()))
        
        commentView.tlabel.text = "意見欄"
        commentView.tlabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        commentView.tlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(commentView)
        commentView.anchor(top: cselect2.bottomAnchor, leading: cselect2.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 39.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluex(), height: 80.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


