//
//  NewServiceFormController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/12.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class NewTypeButton : UIButton {
    let newlabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        newlabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        newlabel.textColor = .white
        backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        
        addSubview(newlabel)
        newlabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 68.calcvaluex(), bottom: 0, right: 0))
        newlabel.centerYInSuperview()
        constrainHeight(constant: 65.calcvaluey())
        layer.cornerRadius = 65.calcvaluey()/2
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            self.roundCorners2([.layerMaxXMinYCorner,.layerMaxXMaxYCorner], radius: 65.calcvaluey()/2)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NewServiceFormController: SampleController {
    let buttonarray = ["基本資訊","機台資訊","換件項目列表","費用及收款狀況","工程確認","確認簽名","客戶意見欄"]
    var typeButtonarray = [NewTypeButton]()
    var currentIndex = 0
    var rightView = ServiceFormInfoView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        let stackview = UIStackView()
        stackview.axis = .vertical
        stackview.distribution = .fillEqually

        for (index,st) in buttonarray.enumerated() {
            let bt = NewTypeButton(type: .system)
            bt.newlabel.text = st
            bt.tag = index
            bt.addTarget(self, action: #selector(buttonselected), for: .touchUpInside)
            if index == 0{
                bt.backgroundColor = .black
                bt.newlabel.textColor = .white
                bt.newlabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
                
            }
            typeButtonarray.append(bt)

            stackview.addArrangedSubview(bt)
        }
        view.addSubview(stackview)
        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 33.calcvaluey(), left: -36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 226.calcvaluex(), height: 455.calcvaluey()))
        
        view.addSubview(rightView)
        rightView.delegate = self
        rightView.anchor(top: topview.bottomAnchor, leading: stackview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 42.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        view.sendSubviewToBack(rightView)
    }
    @objc func buttonselected(sender:UIButton) {
        
        let pbt = typeButtonarray[currentIndex]
        pbt.backgroundColor = .clear
        pbt.newlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        pbt.newlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        let bt = typeButtonarray[sender.tag]
        bt.backgroundColor = .black
        bt.newlabel.textColor = .white
        bt.newlabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        currentIndex = sender.tag
        rightView.buttontap = true
        rightView.tableview.scrollToRow(at: IndexPath(item: 0, section: sender.tag), at: .middle, animated: true)
    }
}
extension NewServiceFormController:ServiceFormInfoViewDelegate {
    func openAddComponent() {
        let vc = AddNewComponentController()
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.present(vc, animated: false, completion: nil)
    }
    
    func scrolling(indexarray: [IndexPath]) {
        let pbt = typeButtonarray[currentIndex]
        pbt.backgroundColor = .clear
        pbt.newlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        pbt.newlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        var bt:NewTypeButton?
        if indexarray.count == 3{
            currentIndex = indexarray[1].section
            bt = typeButtonarray[indexarray[1].section]
        }
        else{
            currentIndex = indexarray[0].section
            bt = typeButtonarray[indexarray[0].section]
        }
        bt!.backgroundColor = .black
        bt!.newlabel.textColor = .white
        bt!.newlabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
    }
    
    
}
