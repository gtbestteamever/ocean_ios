//
//  WorkDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/11.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class WorkDetailView : UIView {
    let topview = UIView()
    let titlelabel = UILabel()
    var titlelabelAnchor:NSLayoutConstraint?
    let seperator = UIView()
    var topviewanchor : AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor()
        layer.cornerRadius = 15.calcvaluex()
        backgroundColor = .white
        addSubview(topview)
        topview.backgroundColor = .clear
        
        topview.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            topview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            topview.roundCorners2([.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 15.calcvaluex())
        }
        topviewanchor = topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 80.calcvaluey()))
        topview.addSubview(titlelabel)
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 26.calcvaluex())
        titlelabel.textColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        titlelabel.centerYInSuperview()
        titlelabelAnchor = titlelabel.leadingAnchor.constraint(equalTo: topview.leadingAnchor, constant: 24.calcvaluex())
        titlelabelAnchor?.isActive = true
        
        seperator.backgroundColor = MajorColor().oceanColor
        topview.addSubview(seperator)
        seperator.anchor(top: nil, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 18.calcvaluex()),size: .init(width: 0, height: 2.calcvaluey()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BottomRightView:WorkDetailView {
//    let commentbar = CommentBar()
//    var rightviewtableview = CustomTableView(frame: .zero, style: .grouped)
    let infoview = FixInfoView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        topviewanchor?.height?.constant = 48.calcvaluey()
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        topview.backgroundColor = #colorLiteral(red: 0.8707719445, green: 0.9054962993, blue: 1, alpha: 1)
        seperator.isHidden = true
        addSubview(infoview)
        infoview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
//        addSubview(commentbar)
//        commentbar.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 68.calcvaluey()))
//
//
//        addSubview(rightviewtableview)
//        rightviewtableview.anchor(top:topview.bottomAnchor, leading: leadingAnchor, bottom: commentbar.topAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 2, right: 0))


    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum ScheduleMode : String{
    case New = "new"
    case Working = "working"
    case Completed = "completed"
    case Problem = "cancelled"
    case ReAssign = "reassigned"
    case Delete = "delete"
    case Edit = "edit"
    var tag : Int{
        switch self{
        case .New: return 0
        case .Working: return 1
        case .Completed: return 2
        case .Problem: return 3
        case .ReAssign: return 4
        case .Delete: return 5
        case .Edit: return 6
        }
    }
    var name : String{
        switch self{
        case .New: return "新工作".localized
        case .Working: return "工作中".localized
        case .Completed: return "已完成".localized
        case .Problem: return "有問題".localized
        case .ReAssign: return "再指派".localized
        case .Delete: return "刪除派工".localized
        case .Edit: return "編輯派工".localized
        }
    }
    var button_name: String{
        switch self{
        case .New: return "新工作".localized
        case .Working: return "開始工作".localized
        case .Completed: return "完成工作".localized
        case .Problem: return "有問題".localized
        case .ReAssign: return "再指派".localized
        case .Delete: return "刪除派工".localized
        case .Edit: return "編輯派工".localized
        }
    }
}
class TopLeftView:WorkDetailView {
    weak var con : WorkDetailController?
    let topimage = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user").withRenderingMode(.alwaysTemplate))
    let statusLabel = UILabel()
    let namelabel = UILabel()
    let posttimelabel = UILabel()
    let servicecrew = TopLeftDetail()
    let maintaintime = TopLeftDetail()
    let maintaindetail = TopLeftDetail()
    let problemlabel = TopLeftDetail()
    var mode : ScheduleMode? {
        didSet{
            con?.mode = mode
        }
    }
    var workDetail : WorkDetail? {
        didSet{
            namelabel.text = workDetail?.owner?.name
            mode = ScheduleMode(rawValue: workDetail?.status ?? "")
            //statusLabel.text = mode?.name
            let status_array = GetStatus().getUpKeepTaskStatus()
            if let status  = status_array.first(where: { (st) -> Bool in
                return st.key == workDetail?.status
            }) {
                statusLabel.text = status.getString()
                statusLabel.textColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff")
                statusLabel.layer.borderColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff").cgColor
            }
            posttimelabel.text = Date().convertToDateComponent(text: workDetail?.created_at ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false)
            maintaindetail.sublabel.text = workDetail?.description
            maintaintime.sublabel.text = Date().convertToDateComponent(text: workDetail?.scheduled ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false)
            let workers_array = workDetail?.service_personnels ?? []
            var workers = ""
            for (index,i) in workers_array.enumerated() {
                workers += "\(i.service_groups.first?.title ?? "") - \(i.name ?? "")"
                if index != workers_array.count - 1{
                    workers += " / "
                }
            }
            servicecrew.sublabel.text = workers
            
            if let reason = workDetail?.reason,reason != "" {
                problemlabel.isHidden = false
                problemlabel.sublabel.text = reason
            }
        
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        topviewanchor?.height?.constant = 48.calcvaluey()
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        topview.backgroundColor = #colorLiteral(red: 0.8707719445, green: 0.9054962993, blue: 1, alpha: 1)
        seperator.isHidden = true
        topimage.tintColor = #colorLiteral(red: 0.8486602306, green: 0.8487828374, blue: 0.8486333489, alpha: 1)
        
        addSubview(topimage)
        topimage.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 22.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        namelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
       // namelabel.text = "葉德雄"
        namelabel.textColor = MajorColor().oceanColor
        
        addSubview(namelabel)
        namelabel.anchor(top: topimage.topAnchor, leading: topimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(statusLabel)
        statusLabel.layer.borderWidth = 1.calcvaluex()
        statusLabel.layer.borderColor = UIColor.clear.cgColor
       // statusLabel.layer.borderColor = #colorLiteral(red: 0.9411922693, green: 0.5160663128, blue: 0.006000845693, alpha: 1)
        //statusLabel.layer.cornerRadius = 5.calcvaluex()
        statusLabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statusLabel.textAlignment = .center
        //statusLabel.textColor = #colorLiteral(red: 0.9411922693, green: 0.5160663128, blue: 0.006000845693, alpha: 1)
        statusLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 72.calcvaluex(), height: 0))
        statusLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30.calcvaluey()).isActive = true
        statusLabel.layer.cornerRadius = 30.calcvaluey()/2
        statusLabel.numberOfLines = 2
        statusLabel.centerYAnchor.constraint(equalTo: namelabel.bottomAnchor).isActive = true
       // posttimelabel.text = "5分鐘前"
        posttimelabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        posttimelabel.textColor = MajorColor().oceanlessColor
        
        addSubview(posttimelabel)
        posttimelabel.anchor(top: namelabel.bottomAnchor, leading: namelabel.leadingAnchor, bottom: topimage.bottomAnchor, trailing: nil)
        
        let scrollview = UIScrollView()
        let stackview = UIStackView()
        stackview.spacing = 15.calcvaluey()
        stackview.axis = .vertical
        stackview.alignment = .fill
        scrollview.addSubview(stackview)
        scrollview.showsVerticalScrollIndicator = false
        stackview.fillSuperview()
        stackview.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        //stackview.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        addSubview(scrollview)
    
        
        scrollview.anchor(top: topimage.bottomAnchor, leading: topimage.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 26.calcvaluey(), right: 26.calcvaluex()))
        stackview.addArrangedSubview(servicecrew)
        stackview.addArrangedSubview(maintaintime)
        stackview.addArrangedSubview(maintaindetail)
        stackview.addArrangedSubview(problemlabel)
        //addSubview(servicecrew)
        
        servicecrew.titlelabel.text = "服務人員".localized
        //servicecrew.sublabel.text = "交機組 - 廖漢祥 / 機械組 - 黃偉利 / 交機組 - 廖漢祥 / 機械組 - 黃偉利"
        //servicecrew.anchor(top: topimage.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        
        maintaintime.tinyimage.image = #imageLiteral(resourceName: "ic_content_time").withRenderingMode(.alwaysTemplate)
        maintaintime.titlelabel.text = "維修時間".localized
       // maintaintime.sublabel.text = "9月20日 下午4:30"
       // addSubview(maintaintime)
       // maintaintime.anchor(top: servicecrew.bottomAnchor, leading: servicecrew.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 15.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        
        maintaindetail.tinyimage.image = #imageLiteral(resourceName: "ic_content_work").withRenderingMode(.alwaysTemplate)
        maintaindetail.titlelabel.text = "工作內容".localized
       // maintaindetail.sublabel.text = "請記得攜帶工具包 fasdf af asfklajs falkf asdflksajf asflksa faskdf asflks afkdjsa faskdf "
       // addSubview(maintaindetail)
       // maintaindetail.anchor(top: maintaintime.bottomAnchor, leading: maintaintime.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 15.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        problemlabel.tinyimage.image = #imageLiteral(resourceName: "ic_error").withRenderingMode(.alwaysTemplate)
        problemlabel.tinyimage.tintColor = MajorColor().oceanColor
        problemlabel.titlelabel.text = "派工問題描述".localized
        problemlabel.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopRightView:WorkDetailView {
    let headerlabel = UILabel()
    let location = TopRightDetail()
    let phone = TopRightDetail()
    let member = TopRightDetail()
    let rightarraow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        topviewanchor?.height?.constant = 48.calcvaluey()
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        topview.backgroundColor = #colorLiteral(red: 0.8707719445, green: 0.9054962993, blue: 1, alpha: 1)
        seperator.isHidden = true
        addSubview(headerlabel)
        headerlabel.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        headerlabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
       // headerlabel.text = "久大行銷股份有限公司"
        headerlabel.textColor = #colorLiteral(red: 0.123444654, green: 0.08487261087, blue: 0.07682070881, alpha: 1)
        headerlabel.numberOfLines = 0
        
        let scrollview = UIScrollView()
        let stackview = UIStackView()
        stackview.spacing = 15.calcvaluey()
        stackview.axis = .vertical
        stackview.alignment = .fill
        scrollview.addSubview(stackview)
        scrollview.showsVerticalScrollIndicator = false
        stackview.fillSuperview()
        stackview.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        addSubview(scrollview)
        scrollview.anchor(top: headerlabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 46.calcvaluex(), bottom: 26.calcvaluey(), right: 46.calcvaluex()))
        stackview.addArrangedSubview(location)
        stackview.addArrangedSubview(phone)
        stackview.addArrangedSubview(member)
       // addSubview(location)
        location.titlelabel.text = "\("地點".localized)："
      //  location.sublabel.text = "台中市西屯區文心路三段241號17樓 afsdf asflksajdf asfl sadfkljas dflksa jfdldsfj askdflj asdflj asdflj asdkfl asdfl "
       // location.anchor(top: headerlabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 329.calcvaluex(), height: 24.calcvaluey()))
       // addSubview(phone)
        phone.tinyimage.image = #imageLiteral(resourceName: "ic_content_call").withRenderingMode(.alwaysTemplate)
        phone.titlelabel.text = "\("電話".localized)："
        //phone.sublabel.text = "04-1234567#21"
       // phone.anchor(top: location.bottomAnchor, leading: location.leadingAnchor, bottom: nil, trailing: location.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        //addSubview(member)
        member.tinyimage.image = #imageLiteral(resourceName: "ic_content_user").withRenderingMode(.alwaysTemplate)
        member.titlelabel.text = "\("承辦人員".localized)："
       // member.sublabel.text = "葉德雄"
       // member.tinyimage.image = #imageLiteral(resourceName: "ic_content_user")
                member.anchor(top: phone.bottomAnchor, leading: phone.leadingAnchor, bottom: nil, trailing: phone.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        rightarraow.isHidden = true
        rightarraow.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(rightarraow)
        rightarraow.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 79.calcvaluey(), right: 16.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopRightDetail:UIView {
    let tinyimage = UIImageView(image: #imageLiteral(resourceName: "ic_content_address").withRenderingMode(.alwaysTemplate))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        tinyimage.tintColor = MajorColor().oceanColor
        addSubview(tinyimage)
        tinyimage.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        
        addSubview(titlelabel)
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        titlelabel.textColor = MajorColor().oceanlessColor
        titlelabel.anchor(top: nil, leading: tinyimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYAnchor.constraint(equalTo: tinyimage.centerYAnchor).isActive = true
        addSubview(sublabel)
        
        sublabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        sublabel.textColor = MajorColor().oceanlessColor
        sublabel.anchor(top: titlelabel.topAnchor, leading: titlelabel.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        sublabel.numberOfLines = 0
        sublabel.heightAnchor.constraint(greaterThanOrEqualTo: titlelabel.heightAnchor).isActive = true
        //sublabel.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopLeftDetail:UIView {
    let tinyimage = UIImageView(image: #imageLiteral(resourceName: "ic_content_user").withRenderingMode(.alwaysTemplate))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        tinyimage.tintColor = MajorColor().oceanColor
        addSubview(tinyimage)
        tinyimage.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        titlelabel.textColor = MajorColor().oceanColor
        addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: tinyimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYAnchor.constraint(equalTo: tinyimage.centerYAnchor).isActive = true
        addSubview(sublabel)
        sublabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        sublabel.numberOfLines = 0
        sublabel.textColor = MajorColor().oceanlessColor
        sublabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BottomLeftView:WorkDetailView {
    let addservicebutton = AddButton3(type: .system)
    let noserviceview = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(addservicebutton)
        addservicebutton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        addservicebutton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        
        addservicebutton.titles.text = "新增服務單"
        
        addservicebutton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 14.calcvaluex(), bottom: 14.calcvaluey(), right: 14.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        addservicebutton.layer.cornerRadius = 38.calcvaluey()/2
        
        addSubview(noserviceview)
        noserviceview.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        noserviceview.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        noserviceview.text = "尚無服務單"
        noserviceview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 25.calcvaluey(), left: 56.calcvaluex(), bottom: 0, right: 0))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class WorkDetailController: SampleController {
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    var stackview = UIStackView()
    let topleftview = TopLeftView()
    let bottomleftivew = TopRightView()
    //let toprightview = TopRightView()
    let bottomrightview = BottomRightView()
    var id : String?
    var workDetail : WorkDetail? {
        didSet{
            topleftview.con = self
            topleftview.workDetail = self.workDetail
            bottomrightview.infoview.commentheader.data = workDetail?.upkeep
            
            bottomleftivew.headerlabel.text = workDetail?.upkeep?.account?.name
            bottomleftivew.location.sublabel.text = workDetail?.upkeep?.account?.address
            bottomleftivew.phone.sublabel.text = workDetail?.upkeep?.account?.tel
            bottomleftivew.member.sublabel.text = workDetail?.upkeep?.account?.sales?.name
        }
    }
    var mode : ScheduleMode? {
        didSet{
            print(331,mode)
            var mode_array = [ScheduleMode]()
            if mode == .New{
                if workDetail?.owner?.username == GetUser().getUser()?.data.username {
                    //,.Problem,.Working
                    mode_array = [.Edit,.Delete,.Problem,.Working]
                }
                else if workDetail?.service_personnels.contains(where: { (emp) -> Bool in
                    return emp.id == GetUser().getUser()?.data.id
                }) ?? false{
                    mode_array = [.Problem,.Working]
                    
                }
            }
            else if mode == .Working{
                if workDetail?.owner?.username == GetUser().getUser()?.data.username || (workDetail?.service_personnels.contains(where: { (emp) -> Bool in
                    return emp.id == GetUser().getUser()?.data.id
                }) ?? false) {
                    
                    mode_array = [.ReAssign,.Completed]
                }
                
            }
            setStackview(array: mode_array)
            
        }
    }
    func setStackview(array: [ScheduleMode]) {
        print(661,array.count)
        extrabutton_stackview.safelyRemoveArrangedSubviews()
        extrabutton_stackview.addArrangedSubview(UIView())
        for i in array {
            
            
            let button = AddButton4()
            button.titles.text = i.button_name
//            button.backgroundColor = .black
//            button.setTitleColor(.white, for: .normal)
//            button.setTitle(i.button_name, for: .normal)
            button.mode = i
            
            button.addTarget(self, action: #selector(updateStatus), for: .touchUpInside)
           extrabutton_stackview.addArrangedSubview(button)
        }
    }
    func createFormData() -> ServiceForm {
       
        let data = ServiceForm()
        data.upkeep_id = workDetail?.upkeep?.id ?? ""
        data.status = "processing"
        let customer = FormCustomer()
        customer.id = workDetail?.upkeep?.account?.code ?? ""
        customer.name = workDetail?.upkeep?.account?.name ?? ""
        customer.phone = workDetail?.upkeep?.account?.tel ?? ""
        customer.address = workDetail?.upkeep?.account?.address ?? ""
        customer.contact = workDetail?.upkeep?.account?.contact ?? ""
        let date = FormTime()
        date.startTime = workDetail?.scheduled ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let start = dateFormatter.date(from: date.startTime) ?? Date()
        let end = dateFormatter.date(from: date.endTime) ?? Date()
        date.duration = String(format: "%.1f",Double(end.minutes(from: start))/60.0)
        print(992,date.endTime)
        let machine = FormMachine()
//        print(332,workDetail?.upkeep?.appliance?.code ?? "")
        if let app = workDetail?.upkeep?.appliance {
            data.selectOrInput = 0
            machine.code = app.code ?? ""
            machine.type = app.reference?.title ?? ""
            machine.maintainStatus = app.in_warranty_period == 1 ? "保固中".localized : "過保".localized
        }
        else{
            data.selectOrInput = 1
            machine.code = workDetail?.upkeep?.appliance_name ?? ""
        }
        machine.description = workDetail?.upkeep?.subject ?? ""
        machine.initial_reason = workDetail?.description ?? ""
        let extrawork = FormExtraWork()
        extrawork.multiplier = "1.0"
       
        let engineer = FormEngineer()
        let user = GetUser().getUser()
        engineer.id = user?.data.id ?? ""
        engineer.name = user?.data.name ?? ""
        //user?.data?.id
        data.customer = customer
        data.timeAndDuration = [date]
        data.engineers = [engineer]
        data.machine = machine
        data.priceGroup.extraGroup = [extrawork]
        print(991,data.customer.name)
        return data
    }
    @objc func updateStatus(sender:AddButton4) {
        var message = ""
        switch sender.mode {
 
        case .Working :
            message += "確認開始工作？".localized
        case .Completed:
            message += "確認完成工作？".localized
        case .Problem:
            message += "確認派工有問題？".localized
            let vd = NewProblemController()
            vd.pre_con = self
            vd.modalPresentationStyle = .overCurrentContext
            self.present(vd, animated: false, completion: nil)
            return
        case .ReAssign:
            message += "確認更改為再指派？".localized
        case .Delete:
            message += "確認是否刪除此筆派工?".localized
        default:
            let con = EditScheduleController()
            con.data = workDetail
            con.contr = self
            con.modalPresentationStyle = .overCurrentContext
            
            self.present(con, animated: false, completion: nil)
            return
        }
        
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "確定".localized, style: .default, handler: { (_) in
            if sender.mode == .Completed || sender.mode == .ReAssign{
                let vd = ServiceFormController()
                if sender.mode == .ReAssign{
                    vd.isReassign = true
                }
                vd.workDetail = self.workDetail
                if let ft = self.workDetail?.upkeep?.upkeep_order {
                    vd.mode = .addOn
                    let hud = JGProgressHUD()
                    hud.show(in: self.view)
                    NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/order/\(ft.id)", decoderType: ServiceData.self) { json in
                        DispatchQueue.main.async {
                            hud.dismiss()
                            if let json = json?.data {
                                let data = ServiceForm()
                                data.setData(form_data: json,workDetail: self.workDetail,mode: .addOn)
                                vd.container.form_data = data
                                vd.modalPresentationStyle = .fullScreen
                                self.present(vd, animated: true, completion: nil)
                            }
                        }

                    }
                    return
                    
                }
                else{
                    vd.mode = .New
                    vd.container.form_data = self.createFormData()
                }
                
                vd.modalPresentationStyle = .fullScreen
                self.present(vd, animated: true, completion: nil)
                return
            }
            if sender.mode == .Delete {
            let hud = JGProgressHUD()
            hud.textLabel.text = "刪除中".localized
            hud.show(in: self.view)
            NetworkCall.shared.getDelete(parameter: "api-or/v1/upkeep-tasks/\(self.workDetail?.id ?? "")", decoderType: UpdateClass.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json?.data,json {
                        hud.textLabel.text = "刪除成功".localized
                        hud.dismiss(afterDelay: 1, animated: true) {
                            General().getTopVc()?.dismiss(animated: true, completion: nil)
                        }
                    }
                }

            }
            }
            else{
                            var param = [String:Any]()
                            var workers_id = [String]()
                            for i in self.workDetail?.service_personnels ?? [] {
                                workers_id.append(i.id)
                            }
                            param["service_personnels"] = workers_id
                            param["status"] = sender.mode.rawValue
                            param["upkeep_id"] = self.workDetail?.upkeep?.id ?? ""
                            param["scheduled"] = self.workDetail?.scheduled ?? ""
                            param["description"] = self.workDetail?.description ?? ""
                            let hud = JGProgressHUD()
                            hud.show(in: self.view)
                            NetworkCall.shared.postCallAny(parameter: "api-or/v1/upkeep-tasks/\(self.workDetail?.id ?? "")", param: param, decoderType: UpdatedWorkDetail.self) { (json) in
                                DispatchQueue.main.async {
                                    hud.dismiss()
                                    if let json = json?.data {
                                        self.workDetail = json
                                    }
                                }
                
                            }
            }
        }))
        alert.addAction(UIAlertAction(title: "取消".localized, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        titleview.label.text = "派工明細".localized
//        stackview.axis = .horizontal
//        stackview.spacing = 18.calcvaluex()
//        stackview.alignment = .center
//        extrabutton_stackview.addArrangedSubview(stackview)
       //topview.addSubview(stackview)
        //stackview.alignment = .bottom
//        stackview.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 18.calcvaluex()))
        view.backgroundColor = #colorLiteral(red: 0.9763614535, green: 0.9765318036, blue: 0.9763634801, alpha: 1)
        
        topleftview.titlelabel.text = "派工單".localized
        view.addSubview(topleftview)
        topleftview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 412.calcvaluex(), height: 351.calcvaluey()))
        
        bottomleftivew.titlelabel.text = "客戶資訊".localized
        view.addSubview(bottomleftivew)
        
        bottomleftivew.anchor(top: topleftview.bottomAnchor, leading: topleftview.leadingAnchor, bottom: view.bottomAnchor, trailing: topleftview.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 26.calcvaluey(), right: 0))
        
//        toprightview.titlelabel.text = "客戶資訊"
//        view.addSubview(toprightview)
//        toprightview.anchor(top: topleftview.topAnchor, leading: topleftview.trailingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 235.calcvaluey()))
        
        if #available(iOS 11.0, *) {
            bottomrightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        bottomrightview.titlelabel.text = "報修內容".localized
        view.addSubview(bottomrightview)
        bottomrightview.anchor(top: topleftview.topAnchor, leading: topleftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        
        
        
        //bottomleftivew.addservicebutton.addTarget(self, action: #selector(newserviceform), for: .touchUpInside)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep-tasks/\(id ?? "")", decoderType: UpdatedWorkDetail.self) { (json) in
            DispatchQueue.main.async {
                if let json = json?.data {
                    self.workDetail = json
                }
            }

        }
    }
    @objc func newserviceform(){
        let vd = NewServiceFormController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
}
