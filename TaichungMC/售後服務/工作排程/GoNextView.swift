//
//  GoNextView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/16.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class GoNextView: UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_product_normal2").withRenderingMode(.alwaysTemplate))
    let hintlabel = UILabel()
    let seperator = UIView()
    let hinttext = UILabel()
    let previewButton = UIButton(type: .custom)
    let nextbutton = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.tintColor = MajorColor().oceanSubColor
        addSubview(imageview)
        imageview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 20.calcvaluex(), height: 20.calcvaluey()))
        imageview.centerYInSuperview()
        
        hintlabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        hintlabel.text = "小提示".localized
        hintlabel.textColor = MajorColor().oceanlessColor
        addSubview(hintlabel)
        hintlabel.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        hintlabel.centerYInSuperview()
        
        addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.anchor(top: nil, leading: hintlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 58.calcvaluey()))
        seperator.centerYInSuperview()
        
        addSubview(hinttext)
        hinttext.text = "請選擇一筆報修服務".localized
        hinttext.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        hinttext.textColor = MajorColor().oceanlessColor
        hinttext.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        hinttext.centerYInSuperview()
        
//        previewButton.layer.borderWidth = 1.calcvaluex()
//        previewButton.layer.borderColor = Major
        previewButton.setTitle("預覽選擇結果".localized, for: .normal)
        previewButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        previewButton.setTitleColor(.white, for: .normal)
        addSubview(previewButton)
        previewButton.backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        previewButton.anchor(top: nil, leading: hinttext.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 172.calcvaluex(), height: 38.calcvaluey()))
        previewButton.centerYInSuperview()
        previewButton.layer.cornerRadius = 38.calcvaluey()/2
        previewButton.isHidden = true
        addSubview(nextbutton)
        nextbutton.setTitle("下一步".localized, for: .normal)
        nextbutton.setTitleColor(.white, for: .normal)
        nextbutton.backgroundColor = MajorColor().oceanColor
        nextbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        nextbutton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
