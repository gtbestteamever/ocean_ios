//
//  MaintainCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/9.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class MaintainCell: UITableViewCell {
    let containercell = UIView()
    let statuslabel = UILabel()
    let toplabel = UILabel()
    let sublabel = UILabel()
    let timelabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        
        contentView.addSubview(containercell)
        containercell.layer.cornerRadius = 15
        containercell.backgroundColor = .white
        containercell.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        containercell.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        
        containercell.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: containercell.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.text = "處理中"
        statuslabel.textAlignment = .center
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1.calcvaluex()
        
        containercell.addSubview(toplabel)
        toplabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        toplabel.text = "機台運作問題"
        toplabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        toplabel.anchor(top: containercell.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        
        
        containercell.addSubview(sublabel)
        sublabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        sublabel.text = "久大行銷股份有限公司"
        sublabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        sublabel.anchor(top: toplabel.bottomAnchor, leading: toplabel.leadingAnchor, bottom:nil , trailing: nil)
        
        containercell.addSubview(timelabel)
        timelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        timelabel.text = "10分鐘前"
        timelabel.anchor(top: sublabel.bottomAnchor, leading: sublabel.leadingAnchor, bottom:nil , trailing: nil)
        timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
