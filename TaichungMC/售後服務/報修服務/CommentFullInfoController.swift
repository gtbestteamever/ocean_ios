//
//  CommentFullInfoController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import MobileCoreServices
class CommentFullInfoController : UIViewController {
    weak var pop_con : FixPopInfoController?
    weak var con : MaintainController?
    let container = commentInfoView()
    var containerAnchor : NSLayoutConstraint?
    var id : String?
    var data = [FixMessage]() {
        didSet{
            container.data = data.reversed()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pop_con?.fetchStatus()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(container)
        
        container.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 573.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 0))
        containerAnchor = container.heightAnchor.constraint(equalToConstant: 610.calcvaluey())
        containerAnchor?.isActive = true
        container.label.text = "留言".localized
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        container.commentbar.sendButton.addTarget(self, action: #selector(sendComment), for: .touchUpInside)
        //self.container.tableview.isHidden = true
        
    }
    @objc func sendComment(){
        self.view.endEditing(true)
        
        if let id = id {
            let hud = JGProgressHUD()
            
            hud.show(in: self.container)
            var parameter = [String:String]()
            parameter["description"] = container.commentbar.ctextfield.text
            var data = [Data]()
            var dataName = [String]()
            var memeString = [String]()
            if let ss = container.file, let dt = ss.data {
                data.append(dt)
                dataName.append(ss.name)
                memeString.append(ss.extend)
            }
            NetworkCall.shared.postCallMultipleData(parameter: "api-or/v1/upkeepMessage/\(id)/store", param: parameter, data: data, dataParam: "files[]", dataName: dataName, memeString: memeString, decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                        
                        self.container.attachView.isHidden = true
                        self.container.file = nil
                        self.container.commentbar.sendButton.isUserInteractionEnabled = false
                        self.container.commentbar.sendButton.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
                        self.container.commentbar.ctextfield.text = nil
                        self.container.commentbar.ctextfield.anchorHeight?.constant = 52.calcvaluey()
                        self.data = json.messages ?? []
                       //self.container.tableview.isHidden = false
                        self.container.tableview.scrollToRow(at: IndexPath(row: self.data.count - 1 , section: 0), at: .bottom, animated: false)
                        self.con?.fetchDetailInfo()
                        self.pop_con?.callApiAndOpen()
                    }
                }

            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if data.count > 0{
        self.container.tableview.scrollToRow(at: IndexPath(row: data.count - 1 , section: 0), at: .bottom, animated: false)
            self.container.tableview.isHidden = false
        }
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
class commentInfoView : SampleContainerView {
    let commentbar = CommentBar()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var data = [FixMessage]() {
        didSet{
            print(661,data.count)
            self.tableview.reloadData()
        }
    }
    let stackview = UIStackView()
    let attachView = UIView()
    var file : MaintainFile?
    let imageview = UIImageView()
    let cancelImagebutton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(commentbar)
        commentbar.imageview.isUserInteractionEnabled = true
        commentbar.imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectFile)))
        commentbar.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        addSubview(stackview)
        stackview.axis = .vertical
        stackview.alignment = .fill
        stackview.distribution = .fill
        stackview.anchor(top: nil, leading: leadingAnchor, bottom: commentbar.topAnchor, trailing: trailingAnchor)
        addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = .clear
        tableview.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: stackview.topAnchor, trailing: trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.contentInset.bottom = 10.calcvaluey()
        tableview.register(CommentCell.self, forCellReuseIdentifier: "cell")
        tableview.delegate = self
        tableview.dataSource = self
        tableview.reloadData()
        
        attachView.isHidden = true
        attachView.backgroundColor = #colorLiteral(red: 0.9646412495, green: 0.9646412495, blue: 0.9646412495, alpha: 1)
        stackview.addArrangedSubview(attachView)
        attachView.constrainHeight(constant: 80.calcvaluey())
        attachView.addshadowColor()
        
        imageview.contentMode = .scaleAspectFit
        imageview.layer.borderWidth = 1.calcvaluex()
        imageview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        attachView.addSubview(imageview)
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: attachView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 120.calcvaluex(), height: 60.calcvaluey()))
        imageview.isUserInteractionEnabled = true
        imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFile)))
        attachView.addSubview(cancelImagebutton)
        cancelImagebutton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        cancelImagebutton.contentVerticalAlignment = .fill
        cancelImagebutton.contentHorizontalAlignment = .fill
        cancelImagebutton.anchor(top: imageview.topAnchor, leading: nil, bottom: nil, trailing: imageview.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 4.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        cancelImagebutton.addTarget(self, action: #selector(deSelectImage), for: .touchUpInside)
    }
    @objc func openFile(){
        if let file = file, let data = file.path, let url = URL(string: data) {
            OpenFile().open(url: url)
        }
    }
    @objc func deSelectImage(){
        file = nil
        attachView.isHidden = true
    }
    @objc func selectFile(){
        if file != nil {
                        let alertController = UIAlertController(title: "請先移除目前的附檔", message: nil, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            General().getTopVc()?.present(alertController, animated: true, completion: nil)
            
            return
        }
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            General().getTopVc()?.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
            presenter.sourceView = commentbar.imageview
                presenter.sourceRect = commentbar.imageview.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(controller, animated: true, completion: nil)
//        attachView.isHidden = false
//
//        let bottomOffset = CGPoint(x: 0, y: tableview.contentSize.height + tableview.bounds.height + tableview.contentInset.bottom )
//        tableview.contentOffset = bottomOffset
    }
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(picker, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension commentInfoView: UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1){
            var imageURL : URL?
            if #available(iOS 11.0, *) {
                imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
            } else {
                // Fallback on earlier versions
            }
            
            file = MaintainFile(name: "\(UUID().uuidString).jpg", image: img, extend: "jpg", data: dt,path: imageURL?.path)
           
            //createExtraStack()
            
            General().getTopVc()?.dismiss(animated: true, completion: {
                self.attachView.isHidden = false
                self.imageview.image = img
                let bottomOffset = CGPoint(x: 0, y: self.tableview.contentSize.height + self.tableview.bounds.height + self.tableview.contentInset.bottom )
                self.tableview.contentOffset = bottomOffset
            })
        }
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var image : UIImage = UIImage()
        image = url.getThumnailImage() ?? UIImage()

        let data = try? Data(contentsOf: url)
        file = MaintainFile(name: "\(UUID().uuidString).\(url.pathExtension)",image: image, extend: url.pathExtension, data: data,path: url.path)
        
        
        self.attachView.isHidden = false
        self.imageview.image = image
        let bottomOffset = CGPoint(x: 0, y: self.tableview.contentSize.height + tableview.bounds.height + tableview.contentInset.bottom )
        self.tableview.contentOffset = bottomOffset
       // createExtraStack()
        
    }
    
}
extension commentInfoView: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentCell
        
        cell.selectionStyle = .none
        let dt = data[indexPath.row]
        cell.setData(data:dt)
 
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
}
