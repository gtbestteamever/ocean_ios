//
//  FixInfoController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class FixInfoView : UIView {
    let commentbar = CommentBar()
    let commentheader = CommentHeader()
    weak var con : MaintainController? {
        didSet{
            commentheader.con = self.con
        }
    }
    weak var pop_con : FixPopInfoController? {
        didSet{
            commentheader.pop_con = self.pop_con
        }
    }
    weak var s_con : OceanProductRepairController?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commentbar.backgroundColor = .white
        commentbar.addtopshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        addSubview(commentbar)
        commentbar.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        addSubview(commentheader)
        commentheader.anchor(top: topAnchor, leading: leadingAnchor, bottom: commentbar.topAnchor, trailing: trailingAnchor)
        
        commentbar.ctextfield.isUserInteractionEnabled = false
        commentbar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(seeFullComment)))
        commentheader.threedotoption.isUserInteractionEnabled = true
        commentheader.threedotoption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionAction)))
    }
    func goEditFix(){
        let con11 = AddNewServiceController()
        con11.data = self.commentheader.data
        con11.con2 = con
        con11.con = s_con
        con11.pop_con = self.pop_con
        con11.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        con11.modalPresentationStyle = .overCurrentContext
        
        General().getTopVc()?.present(con11, animated: false, completion: nil)
    }
    @objc func deleteFix(){
        let alertCon = UIAlertController(title: "您確定要刪除這項報修嗎?", message: nil, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "確定", style: .default) { (_) in
            
            if let id = self.commentheader.data?.id {
                let hud = JGProgressHUD()
                hud.textLabel.text = "刪除中..."
                hud.show(in: General().getTopVc()?.view ?? UIView())
                NetworkCall.shared.getDelete(parameter: "api-or/v1/upkeep/destroy/\(id)", decoderType: UpdateClass.self) { (json) in
                    DispatchQueue.main.async {
                        hud.textLabel.text = "刪除成功"
                        hud.dismiss(afterDelay: 1, animated: true)
                        if let json = json?.data,json {
                            
                            self.con?.viewWillAppear(true)
                            
                            if let con = self.pop_con {
                                con.dismiss(animated: false) {
                                    self.s_con?.current_page = 1
                                    self.s_con?.fix_array = []
                                    

                                }
                            }
                        }
                    }
 
                }
            }
        }
        
        let action2 = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        alertCon.addAction(action1)
        alertCon.addAction(action2)
        
        General().getTopVc()?.present(alertCon, animated: true, completion: nil)

        
    }
    @objc func goAssignFix(){
        let controller = NewScheduleController()
        controller.isEdit = true
        controller.selected_fix = self.commentheader.data
        controller.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(controller, animated: true, completion: nil)
//        let hud = JGProgressHUD()
//        hud.show(in: General().getTopVc()?.view ?? UIView())
//        if let id = self.commentheader.data?.id {
//            NetworkCall.shared.postCallMultipleData(parameter: "api-or/v1/upkeep/update/\(id)", param: ["status":"standby"], data: [], dataParam: "", dataName: [], memeString: [], decoderType: ReturnFixModel.self) { (json) in
//                DispatchQueue.main.async {
//                    if let json = json?.data {
//                        hud.dismiss()
//                        self.con?.viewWillAppear(true)
//
//                        if let con = self.pop_con {
//                            con.dismiss(animated: false) {
//                                self.s_con?.current_page = 1
//                                self.s_con?.fix_array = []
//
//                                self.s_con?.fetchStatus()
//                            }
//                        }
//
//                    }
//                }
//
//            }
//        }
    }
    @objc func completeFix(){
        let hud = JGProgressHUD()
        hud.show(in: General().getTopVc()?.view ?? UIView())
        if let id = self.commentheader.data?.id {
            NetworkCall.shared.postCallMultipleData(parameter: "api-or/v1/upkeep/update/\(id)", param: ["status":"completed"], data: [], dataParam: "", dataName: [], memeString: [], decoderType: ReturnFixModel.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json?.data {
                        hud.dismiss()
                        self.con?.viewWillAppear(true)
                        
                        if let con = self.pop_con {
                            con.dismiss(animated: false) {
                                self.s_con?.current_page = 1
                                self.s_con?.fix_array = []
                                
                                
                            }
                        }
                        
                    }
                }

            }
        }
    }
    @objc func optionAction(){
        let alert = UIAlertController(title: "報修設定", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "編輯報修", style: .default) { (_) in
            self.goEditFix()
        }
        let action4 = UIAlertAction(title: "派工", style: .default) { (_) in
            self.goAssignFix()
        }
        let action2 = UIAlertAction(title: "完成報修", style: .default) { (_) in
            self.completeFix()
        }
        let action3 = UIAlertAction(title: "刪除報修", style: .default) { (_) in
            self.deleteFix()
        }
       // alert.addAction(action1)
//        if GetUser().isSaleManager() {
//        alert.addAction(action4)
//        }
       // alert.addAction(action2)
        alert.addAction(action3)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = commentheader.threedotoption
                presenter.sourceRect = commentheader.threedotoption.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(alert, animated: true, completion: nil)
    }
    @objc func seeFullComment(){
        let infoCon = CommentFullInfoController()
        infoCon.pop_con = self.pop_con
        infoCon.con = self.con
        infoCon.id = commentheader.data?.id
        infoCon.data = commentheader.data?.messages ?? []
        infoCon.modalPresentationStyle = .overCurrentContext
        General().getTopVc()?.present(infoCon, animated: false, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
