//
//  AddNewServiceController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class FilterActionController: UIViewController {
    let container = UIView()
    let filtertitle = UILabel()
    let clearbutton = UILabel()
    let confirmbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .plain)
    let closebutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        container.layer.cornerRadius = 15
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 726.calcvaluey()))
        container.addSubview(filtertitle)
        filtertitle.text = "篩選".localized
        filtertitle.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        filtertitle.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        filtertitle.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        filtertitle.centerXInSuperview()
        
        container.addSubview(clearbutton)
        clearbutton.text = "清除".localized
        clearbutton.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        clearbutton.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        clearbutton.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        
        container.addSubview(confirmbutton)
        confirmbutton.setTitleColor(.white, for: .normal)
        confirmbutton.setTitle("確定", for: .normal)
        confirmbutton.addshadowColor(color: #colorLiteral(red: 0.8783326149, green: 0.8784868121, blue: 0.878334403, alpha: 1))
        confirmbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        confirmbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        confirmbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        confirmbutton.centerXInSuperview()
        confirmbutton.layer.cornerRadius = 48.calcvaluey()/2
        
        container.addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.anchor(top: filtertitle.bottomAnchor, leading: container.leadingAnchor, bottom: confirmbutton.topAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        
        container.addSubview(closebutton)
        closebutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        closebutton.isUserInteractionEnabled = true
        closebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closing)))
    }
    @objc func closing(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension FilterActionController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0{
            let cell = FilterLocationCell(style: .default, reuseIdentifier: "cellid")
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
        let cell = FilterActionCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.calcvaluey()
    }
}
