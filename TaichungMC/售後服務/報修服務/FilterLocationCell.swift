//
//  FilterLocationCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class FilterLocationCell: UITableViewCell {
    let header = UILabel()
    let country = FilterTextField()
    let city = FilterTextField()
    let zone = FilterTextField()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        header.text = "地區"
               header.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
               header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
               addSubview(header)
               header.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 44.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(country)
        country.text = "國家"
        country.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 154.calcvaluex(), height: 61.calcvaluey()))
        
        addSubview(city)
        city.text = "縣市"
        city.anchor(top: country.topAnchor, leading: country.trailingAnchor, bottom: country.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 154.calcvaluex(), height: 0))
        
        addSubview(zone)
        zone.text = "城市/鄉鎮"
        zone.anchor(top: city.topAnchor, leading: city.trailingAnchor, bottom: city.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 154.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
