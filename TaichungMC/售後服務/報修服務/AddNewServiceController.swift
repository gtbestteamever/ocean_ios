//
//  AddNewServiceController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
import JGProgressHUD
class PaymentSelectionView:UIView {
    let selectionbutton = UIImageView(image: #imageLiteral(resourceName: "btn_radio_normal").withRenderingMode(.alwaysTemplate))
    let selectlabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(selectionbutton)

        
        selectionbutton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        selectionbutton.centerYInSuperview()
        selectionbutton.tintColor = .black
        addSubview(selectlabel)
        selectlabel.text = "選擇機台".localized
        selectlabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        selectlabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        selectlabel.anchor(top: nil, leading: selectionbutton.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        selectlabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum AddFixMode : Int{
    case Auto = 0
    case Manual = 1
    case Fix = 2
    case No = 3
    case NoNo = 4
}
protocol CircleSelectionViewDelegate {
    func didSelect(mode:AddFixMode)
}
class CircleSelectionView:UIView {
    let selectionbutton = UIImageView(image: #imageLiteral(resourceName: "btn_radio_normal"))
    let selectlabel = UILabel()
    var delegate:CircleSelectionViewDelegate?
    var didSet : Bool = false {
        didSet{
        if didSet{
            selectionbutton.image = #imageLiteral(resourceName: "btn_radio_pressed")
            selectlabel.textColor = .black
        }
        else{
            selectionbutton.image = #imageLiteral(resourceName: "btn_radio_normal")
            selectlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(selectionbutton)

        
        selectionbutton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        selectionbutton.centerYInSuperview()
        
        addSubview(selectlabel)
        selectlabel.text = "選擇機台".localized
        selectlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        selectlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        selectlabel.anchor(top: topAnchor, leading: selectionbutton.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        //selectlabel.centerYInSuperview()
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectType)))
    }
    @objc func selectType(){
        if let mode = AddFixMode(rawValue: self.tag) , !didSet {
            delegate?.didSelect(mode: mode)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class newContainer : UIView {
    override var intrinsicContentSize: CGSize {
        let bound = super.intrinsicContentSize
        print(221,bound)
        return bound
    }
}
class AddNewServiceController: UIViewController {
    let selectMachine = CircleSelectionView()
    let container = newContainer()
    let titlelabel = UILabel()
    let customerlabel = NormalTextView(text: "客戶".localized, placeholdertext: "請選擇客戶".localized)
    let timelabel = NormalTextView(text: "報修機台".localized,placeholdertext: "請選擇報修機台".localized)
    let subjectlabel = NormalTextView(text: "報修主旨".localized, placeholdertext: "請輸入報修主旨".localized)
    let sublabel = NormalTextView(text: "報修內容".localized,placeholdertext: "請輸入報修內容".localized)
    let addressField = NormalTextView(text: "報修地址".localized, placeholdertext: "請輸入報修地址".localized)
    let contactField = NormalTextView(text: "聯絡人".localized, placeholdertext: "請輸入聯絡人".localized)
    let contactNoField = NormalTextView(text: "聯絡人電話".localized, placeholdertext: "請輸入聯絡人電話".localized)
    let addfilebutton = AddFileButton()
    let donebutton = UIButton(type: .system)
    var containeranchor:AnchoredConstraints!
    var extravstack = UIStackView()
    var extravstackanhor:AnchoredConstraints!
    
    let xbutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    
    let enterMachine = CircleSelectionView()
    var mode : AddFixMode = .Auto
    let scrollview = UIScrollView()
    var collectionview : UICollectionView?
    var collectionviewHeightAnchor: NSLayoutConstraint?
    var maintainTime : String?
    var maintainInfo : String?
    var files_arr = [MaintainFile]()
    var enter_code : String?
    var selected_code : String?
    let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    weak var con : OceanProductRepairController?
    weak var con2 : MaintainController?
    weak var pop_con : FixPopInfoController?
    weak var con3 : iPhoneOceanProductRepairController?
    weak var con4 : iPhoneMaintainController?
    var preloadHeight : CGFloat = 0
    var del_array = [String]()
    var edit_account_code : String?
    var auto_account_code : String?
    var auto_address : String?
    var manual_address : String?
    var customer : Customer?
    var auto_department_code : String?
    var data : FixModel? {
        didSet{

            
            
        }
    }
    var isPortal = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let account_data = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()) {
        let json = try? JSONDecoder().decode(UserData.self, from: account_data)
            isPortal = json?.data.is_portal ?? 0
           print(992,isPortal)
            if isPortal == 0{
//                if mode == .Auto {
//                    customerlabel.isHidden = true
//                }
//                else{
//                    customerlabel.isHidden = false
//                }
                
 
//                if mode == .Auto {
//                    arrow.isHidden = false
//                }
//                else{
//                    arrow.isHidden = true
//                }
            }
            else{
               // customerlabel.isHidden = true
//                if mode == .Auto {
//                    arrow.isHidden = false
//                    //customerlabel.isHidden = true
//                }
//                else{
//                    arrow.isHidden = true
//                    //customerlabel.isHidden = false
//
//                }
                edit_account_code = json?.data.account?.code ?? ""

            }
        }
        
        scrollview.contentInset.bottom = 30.calcvaluey()
        scrollview.bounces = false
        scrollview.showsVerticalScrollIndicator = false
        let hstack = UIStackView()
        hstack.axis = .horizontal
        hstack.spacing = 56.calcvaluex()
        hstack.alignment = .fill
        hstack.addArrangedSubview(selectMachine)
        hstack.addArrangedSubview(enterMachine)
        hstack.addArrangedSubview(UIView())
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        
        container.layer.cornerRadius = 15
        
        view.addSubview(container)
        container.centerInSuperview()
        containeranchor = container.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 573.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 0))
        container.heightAnchor.constraint(lessThanOrEqualToConstant: UIScreen.main.bounds.height - 60.calcvaluey()).isActive = true
//        if let data = data{
//            titlelabel.text = "編輯報修"
//        }
//        else{
//            titlelabel.text = "新增報修"
//        }
        titlelabel.text = "新增報修".localized
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        titlelabel.textAlignment = .center
        container.addSubview(titlelabel)
        titlelabel.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        titlelabel.centerXInSuperview()
    
        
        selectMachine.didSet = true
        
        selectMachine.tag = 0
        
        selectMachine.delegate = self
        let v_stack = UIStackView()
        v_stack.spacing = 26.calcvaluey()
        v_stack.alignment = .center
        v_stack.axis = .vertical
        v_stack.addArrangedSubview(hstack)
        customerlabel.constrainWidth(constant: 486.calcvaluex())
        customerlabel.textfield.mode = .Customer
        customerlabel.makeImportant()
        customerlabel.textfield.con2 = self
        customerlabel.isHidden = true
        hstack.constrainWidth(constant: 486.calcvaluex())
        let spacer = UIView()
        spacer.constrainHeight(constant: 36.calcvaluey())
        hstack.addArrangedSubview(spacer)
        timelabel.constrainWidth(constant: 486.calcvaluex())
        
        timelabel.textfield.text = selected_code
        
        
        timelabel.makeImportant()
        addressField.textfield.mode = .Address
        addressField.makeImportant()
        addressField.constrainWidth(constant: 486.calcvaluex())
        subjectlabel.textfield.mode = .Maintain
        subjectlabel.makeImportant()
        subjectlabel.constrainWidth(constant: 486.calcvaluex())
        v_stack.addArrangedSubview(customerlabel)
        v_stack.addArrangedSubview(timelabel)
        v_stack.addArrangedSubview(addressField)
        v_stack.addArrangedSubview(contactField)
        contactField.makeImportant()
        contactNoField.makeImportant()
        v_stack.addArrangedSubview(contactNoField)
        v_stack.addArrangedSubview(subjectlabel)
        timelabel.textfield.con2 = self
        timelabel.textfield.t_delegate = self
        sublabel.textfield.t_delegate = self
        v_stack.addArrangedSubview(sublabel)
        sublabel.constrainWidth(constant: 486.calcvaluex())
        
        contactField.constrainWidth(constant: 486.calcvaluex())
        contactNoField.constrainWidth(constant: 486.calcvaluex())
        
        
        v_stack.addArrangedSubview(addfilebutton)
        addfilebutton.constrainWidth(constant: 486.calcvaluex())
        addfilebutton.constrainHeight(constant: 48.calcvaluey())


        donebutton.constrainWidth(constant: 124.calcvaluex())
        donebutton.constrainHeight(constant: 48.calcvaluey())
        
        
        scrollview.addSubview(v_stack)
        v_stack.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        let contraint = v_stack.heightAnchor.constraint(equalTo: scrollview.heightAnchor)
        contraint.priority = .init(1)
        contraint.isActive = true
        v_stack.fillSuperview()
        container.addSubview(scrollview)
        scrollview.anchor(top: titlelabel.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        
        enterMachine.tag = 1
        enterMachine.delegate = self
        enterMachine.selectlabel.text = "自行輸入機台".localized
        
        timelabel.textfield.mode = .AutoFix
        
        timelabel.textfield.addSubview(arrow)
        arrow.contentMode = .scaleAspectFit
        arrow.centerYInSuperview()
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: timelabel.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        

        sublabel.textfield.mode = .MaintainInfo

        addfilebutton.image.image = #imageLiteral(resourceName: "ic_btn_attach_file")
        addfilebutton.titles.text = "附加檔案(最多上傳六個檔案)".localized
        addfilebutton.titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        addfilebutton.titles.textColor = .white
        addfilebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        

        addfilebutton.layer.cornerRadius = 48.calcvaluey()/2
        addfilebutton.addTarget(self, action: #selector(addingfile), for: .touchUpInside)

        
        donebutton.backgroundColor = MajorColor().oceanSubColor
        
        donebutton.setTitle("新增".localized, for: .normal)
        
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        donebutton.addTarget(self, action: #selector(sendData), for: .touchUpInside)
        donebutton.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))

        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        collectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        collectionview?.delegate = self
        collectionview?.dataSource = self
        collectionview?.showsVerticalScrollIndicator = false
        collectionview?.isScrollEnabled = false
        
        
        collectionview?.isHidden = true
        
        collectionview?.register(maintainFileCell.self, forCellWithReuseIdentifier: "cell")
        v_stack.addArrangedSubview(collectionview!)
        collectionview?.constrainWidth(constant: 486.calcvaluex())
        print(991,preloadHeight)
        collectionviewHeightAnchor = collectionview?.heightAnchor.constraint(equalToConstant: preloadHeight)
        collectionviewHeightAnchor?.isActive = true
        collectionview?.backgroundColor = .clear
        v_stack.addArrangedSubview(donebutton)
        v_stack.addArrangedSubview(UIView())
        extravstack.isHidden = true
        
        container.addSubview(xbutton)
        xbutton.isUserInteractionEnabled = true
        xbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        addressField.textfield.textViewDidChange(addressField.textfield)
        subjectlabel.textfield.textViewDidChange(subjectlabel.textfield)
        sublabel.textfield.textViewDidChange(sublabel.textfield)
    }
    func createAlert(message:String) {
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func confirmSend() {
        var paramter = [String:String]()
       
        if mode == .Auto {
            
            paramter["appliance_code"] = selected_code ?? ""
            paramter["account_code"] = auto_account_code ?? ""
            if auto_department_code == nil || auto_department_code == ""{
                paramter["department_code"] = department_code
            }
            else{
                paramter["department_code"] = auto_department_code ?? ""
            }
            
            
        }
        else{
            
            paramter["appliance_name"] =  enter_code ?? ""
            paramter["account_code"] = edit_account_code ?? ""
            paramter["department_code"] = department_code
            
        }
        paramter["address"] = addressField.textfield.text ?? ""
        paramter["subject"] = subjectlabel.textfield.text ?? ""
        paramter["description"] = maintainInfo ?? ""
        paramter["contact_name"] = contactField.textfield.text ?? ""
        paramter["contact_tel"] = contactNoField.textfield.text ?? ""
        print(878,paramter)
        var dt = [Data]()
        var name = [String]()
        var memeString = [String]()
        for i in files_arr {
            if (i.path?.contains("http://") ?? false) || (i.path?.contains("https://") ?? false) {
                continue
            }
            if let dtt = i.data {
                dt.append(dtt)
            }
            
            name.append(i.name)
            
            memeString.append(i.extend)
        
        }
        if del_array.count > 0{
            paramter["del_files"] = del_array.description
        }
        let hud = JGProgressHUD()
        
        hud.show(in: self.container)
        var urlString = ""
        if let dt = data{
            paramter["status"] = dt.status
            urlString = "api-or/v1/upkeep/update/\(dt.id)"
            hud.textLabel.text = "更新報修中...".localized
        }
        else{
            paramter["status"] = "new"
            urlString = "api-or/v1/upkeep/store"
            hud.textLabel.text = "建立報修中...".localized
        }
        print(331,paramter)
        NetworkCall.shared.postCallMultipleData(parameter: urlString, param: paramter, data: dt, dataParam: "files[]", dataName: name, memeString: memeString, decoderType: ReturnFixModel.self) { (json) in
            DispatchQueue.main.async {
                if let _ = json {
                    if let dt = self.data{
                        hud.textLabel.text = "更新成功".localized
                    }
                    else{
                        hud.textLabel.text = "建立成功".localized
                    }
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.dismiss(afterDelay: 1, animated: true) {
//                        self.con?.selectedIndex = 0
//                        self.con?.fix_array = []
//                        self.con?.current_page = 1
//                        self.con?.fetchApi2()
                        self.con?.fix_array = []
                        self.con?.current_page = 1
                        self.con?.fetchApi2(scrollToTop:true)
                        
                        self.con3?.fix_array = []
                        self.con3?.current_page = 1
                        self.con3?.fetchApi2(scrollToTop:true)
                        
                        self.con4?.fix_array = []
                        self.con4?.current_page = 1
                        self.con4?.fetchApi2(scrollToTop:true)
                        
                        self.pop_con?.fetchStatus()
                        self.con2?.viewWillAppear(true)
                        self.dismiss(animated: false, completion: nil)
                    }
                }
                else{
                    if let dt = self.data{
                        hud.textLabel.text = "更新失敗".localized
                    }
                    else{
                        hud.textLabel.text = "建立失敗".localized
                    }
                    
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.dismiss(afterDelay: 1)
                }
            }

        }
    }
    @objc func sendData(){
        
        if mode == .Auto {
            if selected_code == nil || selected_code == "" {
                createAlert(message: "請選擇報修機台".localized)
                return
            }
        }
        else{
            if edit_account_code == nil{
                createAlert(message: "請選擇客戶".localized)
                return
            }
            if enter_code == nil || enter_code == "" {
                createAlert(message: "請輸入報修機台".localized)
                return
            }
        }

        if addressField.textfield.text == nil || addressField.textfield.text == "" {
            createAlert(message: "請輸入報修地址".localized)
            return
        }
        
         if contactField.textfield.text == nil || contactField.textfield.text == ""{
             createAlert(message: "請輸入聯絡人".localized)
             return
         }
         if contactNoField.textfield.text == nil || contactNoField.textfield.text == "" {
             createAlert(message: "請輸入聯絡人電話".localized)
             return
         }
//        if maintainInfo == nil || maintainInfo == ""{
//            createAlert(message: "請輸入報修內容")
//            return
//        }
        if subjectlabel.textfield.text == nil || subjectlabel.textfield.text == "" {
            createAlert(message: "請輸入報修主旨".localized)
            return
        }

        
        let controller = UIAlertController(title: "\("請務必確認報修地址".localized):\(addressField.textfield.text ?? "")\("是否正確，如客服人員無法解決，將派維修人員前往此地址進行維修。".localized)", message: nil, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "確定".localized, style: .default, handler: { (_) in
            self.confirmSend()
        }))
        controller.addAction(UIAlertAction(title: "取消".localized, style: .cancel, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let data = data {
            selectMachine.isUserInteractionEnabled = false
            enterMachine.isUserInteractionEnabled = false
            if let dt = data.appliance {

                mode = .Auto
                selectMachine.didSet = true
                enterMachine.didSet = false
                timelabel.textfield.text = dt.code
                selected_code = dt.code
                auto_address = dt.address
                addressField.textfield.text = dt.address
                auto_department_code = dt.department_code
                timelabel.textfield.mode = .AutoFix
                auto_account_code = dt.account_code
                arrow.isHidden = false
            }
            else{
                mode = .Manual
                selectMachine.didSet = false
                enterMachine.didSet = true
                timelabel.textfield.text = data.appliance_name
                enter_code = data.appliance_name
                manual_address = data.account?.address
                
                customerlabel.textfield.text = data.account?.name
                edit_account_code = data.account?.code
                timelabel.textfield.mode = .Maintain
                
                customerlabel.textfield.text = data.account?.name
                if isPortal == 0{
                    customerlabel.isHidden = false
                }
                arrow.isHidden = true

            }
            
            if let customer = data.account {
                self.customer = customer
               
            }
            addressField.textfield.text = data.address

            
            subjectlabel.textfield.text = data.subject

            maintainInfo = data.description
            sublabel.textfield.text = data.description

            for i in data.files ?? [] {
                if let url = URL(string: i.path_url ?? "") {
                    files_arr.append(MaintainFile(name: "\(UUID().uuidString).\(url.pathExtension)", image: url.getThumnailImage(), extend: url.pathExtension, data: try? Data(contentsOf: url),path: i.path_url,file_path:i.file))
                }
            }
            if files_arr.count > 0{
                self.collectionview?.isHidden = false
                self.collectionview?.reloadData()
                let ss = files_arr.count / 4
                preloadHeight = CGFloat(ss + 1) * 80.calcvaluey() + CGFloat(ss) * 8.calcvaluey()
                print(331,preloadHeight)


            }
            else{
                self.collectionview?.isHidden = true
            }
            donebutton.setTitle("更新".localized, for: .normal)
        }
    }
    @objc func popview(){
        self.dismiss(animated: false, completion: nil)
    }
    @objc func addingfile(){
        if files_arr.count == 6 {
            let alertController = UIAlertController(title: "一次最多只能上傳6個檔案".localized, message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
            
            return
        }
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = addfilebutton
                presenter.sourceRect = addfilebutton.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
//        containeranchor.height?.constant = 744.calcvaluey()
//
//        extravstack.isHidden = false
//        UIView.animate(withDuration: 0.2) {
//            self.view.layoutIfNeeded()
//        }
    }
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true, completion: nil)
    }
}
extension AddNewServiceController : FormTextViewDelegate {

    
    func textViewReloadHeight(height: CGFloat) {
        //self.view.layoutIfNeeded()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .Maintain {
            enter_code = text
        }

        else if mode == .Address {
            if mode == .AutoFix {
                auto_address = text
            }
            else{
                manual_address = text
            }
        }
        else{
                maintainInfo = text
            
        }
        //maintainInfo = text
    }
    
    
}
extension AddNewServiceController: CircleSelectionViewDelegate {
    func didSelect(mode: AddFixMode) {
        print(mode)
        self.mode = mode
        if mode == .Auto {
            selectMachine.didSet = true
            enterMachine.didSet = false
            timelabel.textfield.placeholderLabel.text = "請選擇報修機台"
            timelabel.textfield.mode = .AutoFix
            timelabel.textfield.text = selected_code
           addressField.textfield.text = auto_address
            arrow.isHidden = false
            
            customerlabel.isHidden = true
//            if isPortal == 1{
//            containeranchor.height?.constant -= 90.calcvaluey()
//            }
            
        }
        else{
            selectMachine.didSet = false
            enterMachine.didSet = true
            timelabel.textfield.placeholderLabel.text = "請輸入報修機台".localized
            timelabel.textfield.mode = .Maintain
            timelabel.textfield.text = enter_code
         addressField.textfield.text = manual_address
            arrow.isHidden = true
            if isPortal == 1{
            customerlabel.isHidden = true
           // containeranchor.height?.constant += 90.calcvaluey()
            }
            else{
                customerlabel.isHidden = false
            }
        }
    }
}
extension AddNewServiceController : UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return files_arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! maintainFileCell
        cell.imageView.image = files_arr[indexPath.item].image
        cell.xbutton.tag = indexPath.item
        cell.xbutton.addTarget(self, action: #selector(deleteItem), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let files = files_arr[indexPath.item]
        if let data = files.path, let url = URL(string: data){
            OpenFile().open(url: url)
        }
    }
    @objc func deleteItem(sender:UIButton) {
        let ft = files_arr.remove(at: sender.tag)
        if let ss = ft.file_path {
            del_array.append(ss)
        }
        self.reloadCollectionView()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 2 * 8.calcvaluex())/3, height: 80.calcvaluey())
    }
    func reloadCollectionView(){
        if files_arr.count == 0{
            self.collectionview?.isHidden = true
            self.collectionview?.reloadData()
            self.collectionviewHeightAnchor?.constant = 0
          //  self.containeranchor.height?.constant = 610.calcvaluey()
            
        }
        else{
        self.collectionview?.isHidden = false
        self.collectionview?.reloadData()
        let height = (self.collectionview?.collectionViewLayout as? UICollectionViewFlowLayout)?.collectionViewContentSize.height ?? 0
        self.collectionviewHeightAnchor?.constant = height
       // self.containeranchor.height?.constant = 610.calcvaluey() + height
        }
       

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1){
            var imageURL : URL?
            if #available(iOS 11.0, *) {
                imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
            } else {
                // Fallback on earlier versions
            }
            let ss = MaintainFile(name: "\(UUID().uuidString).jpg", image: img, extend: "jpg", data: dt,path: imageURL?.path)
            files_arr.append(ss)
            //createExtraStack()
            
            self.dismiss(animated: true, completion: {
                self.reloadCollectionView()
            })
        }
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var image : UIImage = UIImage()
        if url.pathExtension.lowercased() == "pdf" {
            image = Thumbnail().pdfThumbnail(url: url) ?? UIImage()
        }
        else if url.pathExtension.lowercased() == "mp4"{
            image = Thumbnail().getThumbnailImage(forUrl: url) ?? UIImage()
        }
        else if url.pathExtension.lowercased() == "csv" || url.pathExtension.lowercased() == "xlsx" || url.pathExtension.lowercased() == "xls" {
            image = #imageLiteral(resourceName: "ic_preset_excel")
        }
        else if url.pathExtension.lowercased() == "doc" || url.pathExtension.lowercased() == "docx" {
            image = #imageLiteral(resourceName: "ic_preset_word")
        }
        else if url.pathExtension.lowercased() == "ppt" || url.pathExtension.lowercased() == "pptx"{
            image = #imageLiteral(resourceName: "ic_preset_ppt")
        }
        let data = try? Data(contentsOf: url)
        let ss = MaintainFile(name: "\(UUID().uuidString).\(url.pathExtension)",image: image, extend: url.pathExtension, data: data,path: url.path)
        
        files_arr.append(ss)
       reloadCollectionView()
       // createExtraStack()
        
    }
}
