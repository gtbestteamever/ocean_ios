//
//  FixStatusModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation


struct FixStatusModel : Codable {
    var data : FixStatusData?
}

struct FixStatusData : Codable {
    var statuses : [FixStatus]
}

struct FixStatus : Codable {
    var key : String
    var value : String
    var color : String
}
