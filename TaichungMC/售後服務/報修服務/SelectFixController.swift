//
//  SelectFixController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class SelectFixController : UIViewController {
    let container = selectFixContainer()
    var isRefreshing = false
    
    var current_data : UnitData?
    
    var total_data = [UnitModel]()
    var isEdit = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(container)
        if isEdit {
            container.label.text = "選擇客戶".localized
        }
        else{
            container.label.text = "選擇報修機台".localized
        }
        container.centerInSuperview(size: .init(width: 500.calcvaluex(), height: 600.calcvaluey()))
        container.xbutton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        //fetchApi()
    }
    
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
}

class selectFixContainer : SampleContainerView,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            name = nil
        }
        else{
            name = textField.text
        }
        
        current_page = 1
        total_data = []
        self.tableview.reloadData()
        self.fetchApi()
    }
    var current_page = 1
    var con: AddNewServiceController?
    var isRefreshing = false
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if total_data.count > 9 && ((current_data?.meta.current_page ?? 0) < (current_data?.meta.last_page ?? 0)){
            return total_data.count + 1
        }
        return total_data.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == total_data.count {
            if !isRefreshing && ((current_data?.meta.current_page ?? 0) < (current_data?.meta.last_page ?? 0)){
                current_page += 1
                fetchApi()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        con?.timelabel.textfield.text = total_data[indexPath.row].code
        con?.selected_code = total_data[indexPath.row].code
        con?.auto_account_code = total_data[indexPath.row].account?.code
        con?.addressField.textfield.text = total_data[indexPath.row].account?.address
        con?.auto_address = total_data[indexPath.row].account?.address
        con?.auto_department_code = total_data[indexPath.row].department_code
        con?.contactField.textfield.text = total_data[indexPath.row].account?.contact
        con?.contactNoField.textfield.text = total_data[indexPath.row].account?.tel
        General().getTopVc()?.dismiss(animated: false, completion: nil)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if current_page < (current_data?.meta.last_page ?? 0) && total_data.count > 9 && indexPath.item == total_data.count {
            
            return RefreshTableCell(style: .default, reuseIdentifier: "refresh")
        }
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = total_data[indexPath.row].code
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    var name : String?
    var total_data = [UnitModel]()
    var current_data : UnitData?
    let searchcon = SearchTextField()
    var isEdit = false
    let tableview = UITableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(searchcon)
        searchcon.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluex(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0 , height: 52.calcvaluey()))
        searchcon.layer.cornerRadius = 6.calcvaluex()
        searchcon.layer.backgroundColor = #colorLiteral(red: 0.975933447, green: 0.975933447, blue: 0.975933447, alpha: 1)
        searchcon.layer.borderWidth = 1.calcvaluex()
        searchcon.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        if isEdit {
            searchcon.placeholder = "搜尋客戶"
        }
        else{
            searchcon.placeholder = "搜尋機台".localized
        }
        searchcon.delegate = self
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.contentOffset = .init(x: 0, y: -12.calcvaluey())
        tableview.contentInset = .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0)
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableview.anchor(top: searchcon.bottomAnchor, leading: searchcon.leadingAnchor, bottom: bottomAnchor, trailing: searchcon.trailingAnchor)
        tableview.showsVerticalScrollIndicator = false
        fetchApi()
    }
    
    func fetchApi(){
        self.isRefreshing = true
        var urlString = "api-or/v1/appliances?pens=30&page=\(current_page)"
        if let nt = name {
            urlString += "&name=\(nt)"
        }
        NetworkCall.shared.getCall(parameter: urlString, decoderType: UnitData.self) { (json) in
            
            if let json = json {
                self.current_data = json
                for i in json.data {
                    self.total_data.append(i)
                }
                DispatchQueue.main.async {
                    
                    self.isRefreshing = false
                    self.tableview.reloadData()
                    
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
