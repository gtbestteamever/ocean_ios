//
//  FixFilteringController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

struct FilterButtonClass {
    var mode : maintainFilterMode
    var text : String?
    var value : String
    init(mode:maintainFilterMode,value:String) {
        self.mode = mode
        
        self.value = value
    }
}
class FilterClass {
    var isInterview : Bool = true
    
    var employee : Employer?
    var customer : Customer?
    var status : Status?
    var startTime : Date?
    var endTime : Date?
    
    var statusArray : [Status]
    init(){
        employee = nil
        customer = nil
        status = nil
        startTime = nil
        endTime = nil
        statusArray = []
        

    }
    func getArray() -> [FilterButtonClass] {
        var array = [FilterButtonClass]()
        if let customer = customer {
            var ct = FilterButtonClass(mode: .Customer, value: "\("客戶".localized):")
            ct.text = customer.name
            array.append(ct)
        }
        
        if let status = status {
            var ct = FilterButtonClass(mode: .Status, value: "\("狀態".localized):")
            ct.text = status.getString() ?? ""
            array.append(ct)
        }

        if let employee = employee {
            var ct = FilterButtonClass(mode: .Employee, value: "\("員工".localized):")
            ct.text = employee.name ?? ""
            array.append(ct)
        }
        if statusArray.count != 0{
            var ct = FilterButtonClass(mode: .StatusArray, value: "\("狀態".localized):")
            var text = ""
            
            var n_statusArray = isInterview ? GetStatus().getInterviewStatus() : GetStatus().getUpKeepStatus()
            print(551,isInterview,statusArray)
            print(3321,n_statusArray)
            n_statusArray = n_statusArray.filter({ st in
                return statusArray.contains { ss in
                    return ss.key == st.key
                }
            })
            for (index,i) in n_statusArray.enumerated() {
                text += " \(i.getString() ?? "")"
                
                if index != n_statusArray.count - 1{
                    text += ","
                }

            }
            
            ct.text = text
            array.append(ct)
            
        }
        if let start = startTime , let end = endTime {
            var ct = FilterButtonClass(mode: .startAndEndTime, value: "\("日期".localized):")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            ct.text = "\(dateFormatter.string(from: start))~\(dateFormatter.string(from: end))"
            array.append(ct)
        }
        else if let start = startTime {
            var ct = FilterButtonClass(mode: .startTime, value: "\("起始日".localized):")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            ct.text = dateFormatter.string(from: start)
            array.append(ct)
        }
        else if let end = endTime {
            var ct = FilterButtonClass(mode: .endTime, value: "\("結束日".localized):")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            ct.text = dateFormatter.string(from: end)
            array.append(ct)
        }
        
        return array
    }
}
class FilterLabel : UIView {
    let label = UILabel()
    let seperator = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        seperator.backgroundColor = #colorLiteral(red: 0.919366804, green: 0.919366804, blue: 0.919366804, alpha: 1)
        
        addSubview(seperator)
        
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: seperator.topAnchor, trailing: trailingAnchor)
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol StatusSelectionDelegate {
    func didSelectStatus(status:Status)
}
class FilterStatusView : UIView{
    var status = [Status](){
        didSet{
            hstack.safelyRemoveArrangedSubviews()
            for (index,i) in status.enumerated() {
                let vd = FilterLabel()
                vd.constrainHeight(constant: 50.calcvaluey())
                vd.label.text = i.value
                if index == status.count - 1{
                    vd.seperator.isHidden = true
                }
                vd.tag = index
                vd.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addStatus)))
                hstack.addArrangedSubview(vd)
            }
        }
    }
    @objc func addStatus(gesture:UITapGestureRecognizer) {
        if let index = gesture.view?.tag {
            delegate?.didSelectStatus(status: status[index])
        }
    }
    var delegate : StatusSelectionDelegate?
    let hstack = Vertical_Stackview()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(lessThanOrEqualToConstant: 220.calcvaluey()).isActive = true
       // constrainHeight(constant: 220.calcvaluey())
        backgroundColor = .white
        addshadowColor()
        let scrollview = UIScrollView()
        scrollview.addSubview(hstack)
        scrollview.showsVerticalScrollIndicator = false
        scrollview.bounces = false
        //addSubview(hstack)
        hstack.fillSuperview()
        hstack.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        //hstack.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        addSubview(scrollview)
        scrollview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol FixFilterDelegate {
    func selectedFilter(filter:FilterClass)
}
class FixFilteringController: UIViewController,UITextFieldDelegate,StatusSelectionDelegate,selectTimeDelegate,customerDelegate,filterStatusDelegate {
    func setFilterStatus(view: filterStatusItemView) {
        if let status = view.status {
            if view.isSelected {
                data.statusArray.append(status)
            }
            else{
                data.statusArray = data.statusArray.filter({ st in
                    return st.key != status.key
                })
            }
        }
        
        
    }
    
    func didSelect(data: Customer) {
        customer = data
        companyField.textfield.text = customer?.name
    }
    var data = FilterClass()
    func select(date: Date, index: Int) {
       let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let d_string = formatter.string(from: date)
        
        if index == 0{
            startDate = date
            startField.textfield.text = Date().convertToDateComponent(text: d_string, onlyDate: true)
        }
        else {
            endDate = date
            endField.textfield.text = Date().convertToDateComponent(text: d_string, onlyDate: true)
        }
        
    }
    
    func didSelectStatus(status: Status) {
        selectedStatus = status
        statusField.textfield.text = status.value
        removeStatusView()
    }
    var customer : Customer?
    var startDate : Date?
    var endDate : Date?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  if let data = data {
            customer = data.customer
            //selectedStatus = data.status
            startDate = data.startTime
            endDate = data.endTime
            statusView.preSelectedStatus = data.statusArray
            self.companyField.textfield.text = data.customer?.name
            //self.statusField.textfield.text = data.status?.value
            if let start = data.startTime {
                select(date: start, index: 0)
            }
            if let end = data.endTime {
                select(date: end, index: 1)
            }
        //}
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == companyField.textfield {
            removeStatusView()
            let vd = CustomerSelectionView()
            //vd.con3 = self
            vd.delegate = self
            vd.getCustomersList()
            
            vd.modalPresentationStyle = .overCurrentContext
            General().getTopVc()?.present(vd, animated: false, completion: nil)
            
        }
        if textField == statusField.textfield {
            //print(221,GetStatus().getUpKeepStatus())
            if !isShowing {
            addStatusView(textField: textField)
            }
        }
        if textField == startField.textfield {
            showCalendar(text: textField.text ?? "", index: 0)

        }
        
        if textField == endField.textfield {
            showCalendar(text: textField.text ?? "", index: 1)

        }
        return false
    }
    func showCalendar(text:String,index:Int) {
        let vd = CalendarController()
        vd.view.tag = index
        vd.delegate = self
        vd.modalPresentationStyle = .overCurrentContext
        
        if text != ""{

            
            if index == 0{
                if let end_date = endDate {
                    vd.max_date = end_date
                }
                vd.selected_date = startDate
            }
            else{
              
                if let st_date = startDate {
                    print(991,st_date)
                    vd.min_date = st_date
                }
                vd.selected_date = endDate
            }
            
        }
        else{
            if index == 0{
                if let end_date = endDate {
                    vd.max_date = end_date
                }
            }
            else{
                if let st_date = startDate {
                    print(991,st_date)
                    vd.min_date = st_date
                }
            }
        }
        General().getTopVc()?.present(vd, animated: false, completion: nil)
    }
    func addStatusView(textField:UITextField){
        isShowing = true
//        filterStatusView.status = GetStatus().getUpKeepStatus()
//        filterStatusView.delegate = self
//        con.addSubview(filterStatusView)
//        filterStatusView.anchor(top: textField.bottomAnchor, leading: textField.leadingAnchor, bottom: nil, trailing: textField.trailingAnchor)
    }
    func removeStatusView(){
        isShowing = false
        //filterStatusView.removeFromSuperview()
    }
    var selectedStatus : Status?
    var isShowing = false
    let con = SampleContainerView()
    let companyField = InterviewNormalFormView(text: "客戶公司".localized, placeholdertext: "選擇客戶公司".localized)
    let statusField = InterviewNormalFormView(text: "報修狀態", placeholdertext: "選擇狀態")
    let startField = InterviewNormalFormView(text: "報修開始日期".localized, placeholdertext: "選擇開始日期".localized)
    let endField = InterviewNormalFormView(text: "報修結束日期".localized, placeholdertext: "選擇結束日期".localized)
    
    let donebutton = UIButton(type: .custom)
    //let filterStatusView = FilterStatusView()
    let clearButton = UIButton(type: .custom)
    let statusView = filterStatusView(isInterview: false)
    @objc func clearFilter(){
        customer = nil
        selectedStatus = nil
        startDate = nil
        endDate = nil
        
        companyField.textfield.text = nil
        statusField.textfield.text = nil
        startField.textfield.text = nil
        endField.textfield.text = nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        con.label.text = "篩選".localized
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        companyField.arrow.isHidden = false
        statusField.arrow.isHidden = false
        startField.arrow.isHidden = false
        endField.arrow.isHidden = false
        clearButton.setTitle("清除".localized, for: .normal)
        clearButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        clearButton.setTitleColor(.black, for: .normal)
        con.addSubview(clearButton)
        clearButton.anchor(top: con.label.centerYAnchor, leading: con.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: -25.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        clearButton.addTarget(self, action: #selector(clearFilter), for: .touchUpInside)
        let stackview = UIStackView()
        stackview.spacing = 24.calcvaluey()
        stackview.axis = .vertical
        stackview.alignment = .fill
        companyField.textfield.delegate = self
        stackview.addArrangedSubview(companyField)
        //companyField.backgroundColor = .red
        companyField.constrainHeight(constant: 92.calcvaluey())
        stackview.addArrangedSubview(statusView)
        statusView.delegate = self
        let sp = UIView()
        //sp.constrainHeight(constant: 12.calcvaluey())
        stackview.addArrangedSubview(sp)
        //statusView.constrainHeight(constant: 92.calcvaluey())
        //statusView.textfield.delegate = self
        
        let timeStackview = UIStackView()
        
        timeStackview.spacing = 12.calcvaluex()
        timeStackview.distribution = .fillEqually
        timeStackview.alignment = .fill
        timeStackview.addArrangedSubview(startField)
        timeStackview.addArrangedSubview(endField)
        
        startField.constrainHeight(constant: 92.calcvaluey())
        startField.textfield.delegate = self
        endField.constrainHeight(constant: 92.calcvaluey())
        endField.textfield.delegate = self
        stackview.addArrangedSubview(timeStackview)
        
        donebutton.constrainWidth(constant: 124.calcvaluex())
        donebutton.constrainHeight(constant: 48.calcvaluey())
        donebutton.backgroundColor = MajorColor().oceanSubColor
    
        donebutton.setTitle("確定".localized, for: .normal)
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
       // donebutton.addTarget(self, action: #selector(filtering), for: .touchUpInside)

        donebutton.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))
        let spacer = UIView()
        spacer.constrainHeight(constant: 12.calcvaluey())
        stackview.addArrangedSubview(spacer)
        
        let c_view = UIView()
        c_view.addSubview(donebutton)
        donebutton.centerInSuperview()
        donebutton.anchor(top: c_view.topAnchor, leading: nil, bottom: c_view.bottomAnchor, trailing: nil)
        stackview.addArrangedSubview(c_view)
        stackview.addArrangedSubview(UIView())
        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        
        
        con.addSubview(stackview)
        stackview.anchor(top: con.label.bottomAnchor, leading: con.leadingAnchor, bottom: con.bottomAnchor, trailing: con.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 26.calcvaluey(), right: 26.calcvaluex()))
        
        
        view.addSubview(con)
        con.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 526.calcvaluex() : 450.calcvaluex(), height: 0))
        
        donebutton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
    }
    var delegate : FixFilterDelegate?
    @objc func goFilter(){
        
        data.customer = customer
        //filterC.status = selectedStatus
        
        if let st = startDate {
            data.startTime = st
        }
        if let nt = endDate {
            data.endTime = nt
        }
        delegate?.selectedFilter(filter: data)
        
        popView()
    }
    
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}
