//
//  FilterActionCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class EditView : UIView {
    let header = UILabel()
    let newtextfield = FilterTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        header.text = "地區"
        header.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(header)
        header.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        newtextfield.text = "選擇機台"

        
        addSubview(newtextfield)
        
        newtextfield.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 61.calcvaluey()))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FilterTextField:UITextField {
    let newtextfielddownarrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    var padding : CGFloat?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(newtextfielddownarrow)
        newtextfielddownarrow.contentMode = .scaleAspectFit
        newtextfielddownarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 6.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        newtextfielddownarrow.centerYInSuperview()
        
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        self.layer.cornerRadius = 15.calcvaluex()
        self.layer.borderWidth = 1.calcvaluex()
        self.layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
        
        self.inputView = UIPickerView()
        tintColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding == nil ?36.calcvaluex():self.padding!, dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding == nil ?36.calcvaluex():self.padding!, dy: 0)
    }

}
class FilterActionCell: UITableViewCell {
    let header = UILabel()
    let newtextfield = FilterTextField()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        header.text = "地區"
        header.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addSubview(header)
        header.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 44.calcvaluex(), bottom: 0, right: 0))
        
        newtextfield.text = "選擇機台"

        
        addSubview(newtextfield)
        
        newtextfield.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 61.calcvaluey()))
        

       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
