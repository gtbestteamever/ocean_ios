//
//  CommentCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user").withRenderingMode(.alwaysTemplate))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    let datelabel = UILabel()
    let attachStackView = UIStackView()
    let attachFileView = UIImageView()
    var message:FixMessage?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imageview.tintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        contentView.addSubview(imageview)
        imageview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        
        
        titlelabel.text = "葉德雄"
        titlelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        contentView.addSubview(titlelabel)
        titlelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        titlelabel.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 12.calcvaluex(), bottom: 0, right: 0))
        contentView.addSubview(datelabel)
        datelabel.text = "10月22日 下午02:04"
        datelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        datelabel.anchor(top: nil, leading: nil, bottom: titlelabel.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()))
        contentView.addSubview(sublabel)
        sublabel.text = "機台會在一開始時閃爍"
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        sublabel.numberOfLines = 0
        sublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        sublabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 36.calcvaluex()))
        
        contentView.addSubview(attachStackView)
        attachStackView.axis = .vertical
        attachStackView.alignment = .leading
        
        attachFileView.isHidden = true
        attachFileView.contentMode = .scaleAspectFit
        
        attachStackView.addArrangedSubview(attachFileView)
        attachFileView.constrainWidth(constant: 300.calcvaluex())
        attachFileView.constrainHeight(constant: 150.calcvaluex())
        attachFileView.layer.borderWidth = 1.calcvaluex()
        attachFileView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        attachFileView.isUserInteractionEnabled = true
        attachFileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFile)))
        attachStackView.anchor(top: sublabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 36.calcvaluex()))
        
    }
    func setData(data:FixMessage) {
        message = data
        titlelabel.text = data.owner?.text
        if let is_portal = data.owner?.is_portal, is_portal == 1{
            imageview.tintColor = .lightGray
        }
        else{
            imageview.tintColor = MajorColor().oceanSubColor
        }
        sublabel.text = data.description
        datelabel.text = Date().convertToDateComponent(text: data.updated_at ,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        if let ff = data.files.first?.path_url , let url = URL(string: ff){
            attachFileView.isHidden = false
            attachFileView.image = url.getThumnailImage()
        }
        else{
            attachFileView.isHidden = true
            attachFileView.image = nil
        }
    }
    @objc func openFile(){
        if let data = message?.files.first?.path_url, let url = URL(string: data) {
            OpenFile().open(url: url)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
