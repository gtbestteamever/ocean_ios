//
//  CommentHeader.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import SDWebImage
extension URL {
    func getThumnailImageCom(com:@escaping (UIImage) -> Void) {
        if self.pathExtension.lowercased() == "pdf" {
            com(Thumbnail().pdfThumbnail(url: self) ?? UIImage())
           // return Thumbnail().pdfThumbnail(url: self) ?? UIImage()
        }
        else if self.pathExtension.lowercased() == "mp4"{
            com(Thumbnail().getThumbnailImage(forUrl: self) ?? UIImage())
           //return Thumbnail().getThumbnailImage(forUrl: self) ?? UIImage()
        }
        else if self.pathExtension.lowercased() == "csv" || self.pathExtension.lowercased() == "xlsx" || self.pathExtension.lowercased() == "xls" {
            com(#imageLiteral(resourceName: "ic_preset_excel"))
            //return #imageLiteral(resourceName: "ic_preset_excel")
        }
        else if self.pathExtension.lowercased() == "doc" || self.pathExtension.lowercased() == "docx" {
            com(#imageLiteral(resourceName: "ic_preset_word"))
           // return #imageLiteral(resourceName: "ic_preset_word")
        }
        else if self.pathExtension.lowercased() == "ppt" || self.pathExtension.lowercased() == "pptx"{
            com(#imageLiteral(resourceName: "ic_preset_ppt"))
            //return #imageLiteral(resourceName: "ic_preset_ppt")
        }
        else{
            SDWebImageManager.shared.loadImage(with: self, options: .scaleDownLargeImages, progress: nil) { (image, _, _, _, _, _) in
               
                com((image ?? UIImage()))
            }
            
        }
    }
    func getThumnailImage() -> UIImage?{
        if self.pathExtension.lowercased() == "pdf" {
            return Thumbnail().pdfThumbnail(url: self) ?? UIImage()
        }
        else if self.pathExtension.lowercased() == "mp4"{
           return Thumbnail().getThumbnailImage(forUrl: self) ?? UIImage()
        }
        else if self.pathExtension.lowercased() == "csv" || self.pathExtension.lowercased() == "xlsx" || self.pathExtension.lowercased() == "xls" {
            return #imageLiteral(resourceName: "ic_preset_excel")
        }
        else if self.pathExtension.lowercased() == "doc" || self.pathExtension.lowercased() == "docx" {
            return #imageLiteral(resourceName: "ic_preset_word")
        }
        else if self.pathExtension.lowercased() == "ppt" || self.pathExtension.lowercased() == "pptx"{
            return #imageLiteral(resourceName: "ic_preset_ppt")
        }
        else{
            if let data = try? Data(contentsOf: self) {
                return UIImage(data: data)
            }
            
        }
        return nil
    }
}
class CommentPreviewView : UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user").withRenderingMode(.alwaysTemplate))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    let datelabel = UILabel()
    let attachStackView = UIStackView()
    let attachFileView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.tintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addSubview(imageview)
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        //imageview.centerYInSuperview()
        
        //titlelabel.text = "葉德雄"
        titlelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        addSubview(titlelabel)
        titlelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        titlelabel.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 12.calcvaluex(), bottom: 0, right: 0))
        addSubview(datelabel)
        datelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        datelabel.anchor(top: nil, leading: nil, bottom: titlelabel.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()))
        addSubview(sublabel)
//        sublabel.text = "機台會在一開始時閃爍 dfasdf asdfjasdkfl asjf asdflj sadflkjas dflsjadf askfdladfdf asfdlajsdf fasdf asflkjaf l safkljas flaksd falfk as cvjas fdalk askfj va slkfja sv asdkfj asl vasdkjfas flkj vaksdj falkjsf va jf alkfj lkasjfa v askdfj asflk lkajdf dksa "
        sublabel.numberOfLines = 0
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        sublabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 36.calcvaluex()))
        
        addSubview(attachStackView)
        attachStackView.axis = .vertical
        attachStackView.alignment = .leading
        
        attachFileView.isHidden = true
        attachFileView.contentMode = .scaleAspectFit
        
        attachStackView.addArrangedSubview(attachFileView)
        attachFileView.constrainWidth(constant: 200.calcvaluex())
        attachFileView.constrainHeight(constant: 100.calcvaluex())
        attachFileView.layer.borderWidth = 1.calcvaluex()
        attachFileView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        attachStackView.anchor(top: sublabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 36.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CommentHeader: UIView {
    let topview = UIView()
    let seperator = UIView()
    let seperator2 = UIView()
    let commentview = UIView()
    let pagecontrol = UIPageControl()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let imageviewscroll = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let machinesublabel = UILabel()
    let machinesubjectlabel = UILabel()
    let addresslabel = UILabel()
    let machinetitle = UILabel()
    let contactLabel = UILabel()
    let contactPhoneLabel = UILabel()
    let statuslabel = StatusLabel(text: "", color: .clear)
    let titlelabel = UILabel()
    let datelabel = UILabel()
    let numberofcomment = UILabel()
    let threedotoption = UIImageView(image: #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysOriginal))
    let commentCell = CommentPreviewView()
    var collectionview : UICollectionView?
    var collectionviewHeightAnchor: NSLayoutConstraint?
    var formButton = UIButton(type: .custom)
    weak var con : MaintainController?
    weak var pop_con : FixPopInfoController?
    var last_url : URL?
    var data : FixModel? {
        didSet{
            
            if let upkeep = data?.upkeep_order {
                formButton.isHidden = false
            }
            else{
                formButton.isHidden = true
            }
            let status_array = GetStatus().getUpKeepStatus()
            if let status  = status_array.first(where: { (st) -> Bool in
                return st.key == data?.status
            }) {
                statuslabel.text = status.getString()
                statuslabel.textColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff")
                statuslabel.layer.borderColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff").cgColor
            }
            titlelabel.text = data?.account?.name
            datelabel.text = Date().convertToDateComponent(text: data?.created_at ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false)
            if let appliance = data?.appliance {
                machinetitle.text = "\("機台".localized) : \(appliance.code ?? "")"
                
            }
            else{
                machinetitle.text = "\("機台".localized) : \(data?.appliance_name ?? "")"

            }
            
            addresslabel.text = "\("地址".localized) : \(data?.address ?? "")"
            machinesubjectlabel.text = "\("主旨".localized) : \(data?.subject ?? "")"
            
            machinesublabel.text = "\("內容".localized) : \(data?.description ?? "")"
            contactLabel.text = "\("聯絡人".localized) : \(data?.contact_name ?? "")"
            contactPhoneLabel.text = "\("聯絡人電話".localized) : \(data?.contact_tel ?? "")"
            files = data?.files ?? []
            if data?.status == "new" && (GetUser().isAfterSale() || GetUser().isPortal() == 1) && data?.messages?.count == 0{
                threedotoption.isHidden = false
            }
            else{
                threedotoption.isHidden = true
            }
            threedotoption.isHidden = true
            if let dt = data?.messages?.reversed() {
                numberofcomment.text = "\(dt.count)\("則留言".localized)"
                if dt.count > 0, let fr = dt.last{
                    if let is_portal = fr.owner?.is_portal, is_portal == 1{
                        commentCell.imageview.tintColor = .lightGray
                    }
                    else{
                        commentCell.imageview.tintColor = MajorColor().oceanSubColor
                    }
                    commentCell.isHidden = false
                    commentCell.titlelabel.text = fr.owner?.text
                    commentCell.sublabel.text = fr.description
                    commentCell.datelabel.text = Date().convertToDateComponent(text: fr.updated_at,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
                    if let kk = fr.files.first, let url = URL(string: kk.path_url ?? "") {
                        last_url = url
                        commentCell.attachFileView.isHidden = false
                        commentCell.attachFileView.image = url.getThumnailImage()
                        commentCell.attachFileView.isUserInteractionEnabled = true
                        commentCell.attachFileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showAttach)))
                    }
                    else{
                        commentCell.attachFileView.isHidden = true
                    }
                }
                else{
                    commentCell.isHidden = true
                }
            }

            else{
                commentCell.isHidden = true
                numberofcomment.text = "0\("則留言".localized)"
            }
            
        }
    }
    @objc func showAttach(){
        if let url = last_url {
            OpenFile().open(url: url)
        }
    }
    var files = [MaintenanceFile]() {
        didSet{
            collectionview?.reloadData()
        }
    }
    let commentStackView = UIStackView()
    @objc func goFormHistory(){
        let vd = ServiceFormHistoryController()
        if let _ = data?.appliance {
            vd.selectOrInput = 0
        }
        if let name = data?.appliance_name, !name.isEmpty {
            vd.selectOrInput = 1
        }
        vd.id = data?.upkeep_order?.id
        vd.modalPresentationStyle = .fullScreen
        General().getTopVc()?.present(vd, animated: true, completion: nil)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        formButton.addTarget(self, action: #selector(goFormHistory), for: .touchUpInside)
        formButton.isHidden = true
        formButton.backgroundColor = MajorColor().oceanlessColor
        formButton.layer.cornerRadius = 48.calcvaluey()/2
        formButton.constrainWidth(constant: 100.calcvaluex())
        formButton.constrainHeight(constant: 48.calcvaluey())
        formButton.setTitle("服務單".localized, for: .normal)
        formButton.isHidden = true
        formButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        formButton.setTitleColor(.white, for: .normal)
        let stackview = UIStackView(arrangedSubviews: [titlelabel,datelabel])
        datelabel.numberOfLines = 2
        titlelabel.numberOfLines = 2
        stackview.axis = .vertical
        stackview.alignment = .fill
        
        stackview.spacing = 1
        let v_setting_stack = Vertical_Stackview(spacing:12.calcvaluey(),alignment: .center)
        v_setting_stack.addArrangedSubview(threedotoption)
        v_setting_stack.addArrangedSubview(formButton)
        v_setting_stack.addArrangedSubview(UIView())
        let hstack = UIStackView(arrangedSubviews: [statuslabel,stackview,v_setting_stack])

        hstack.alignment = .center
        hstack.spacing = 26.calcvaluex()
        
        threedotoption.isHidden = true
        threedotoption.constrainWidth(constant: 36.calcvaluex())
        threedotoption.constrainHeight(constant: 36.calcvaluey())
        
        let spacer = UIView()
        spacer.constrainHeight(constant: 40.calcvaluey())
        let spacer2 = UIView()
        spacer2.constrainHeight(constant: 8.calcvaluey())
        let spacer3 = UIView()
        spacer3.constrainHeight(constant: 40.calcvaluey())
        let spacer4 = UIView()
        spacer4.constrainHeight(constant: 8.calcvaluey())
        let spacer5 = UIView()
        spacer5.constrainHeight(constant: 8.calcvaluey())
        let spacer6 = UIView()
        spacer6.constrainHeight(constant: 8.calcvaluey())
        let spacer7 = UIView()
        spacer7.constrainHeight(constant: 8.calcvaluey())
        let vstack = UIStackView(arrangedSubviews: [hstack,spacer,machinetitle,spacer2,addresslabel,spacer5,contactLabel,spacer6,contactPhoneLabel,spacer7,machinesubjectlabel,spacer4,machinesublabel,spacer3])
        vstack.axis = .vertical
        vstack.alignment = .fill
    
        commentStackView.addArrangedSubview(seperator)
        commentStackView.addArrangedSubview(commentview)
        commentStackView.addArrangedSubview(seperator2)
        commentStackView.addArrangedSubview(commentCell)
        commentCell.isHidden = true
//        let commentStackView = UIStackView(arrangedSubviews: [seperator,commentview,seperator2])
        commentStackView.axis = .vertical
        commentStackView.distribution = .fill
     
        commentStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(seeFullComment)))
        seperator.constrainHeight(constant: 1.calcvaluey())
        seperator2.constrainHeight(constant: 1.calcvaluey())
        
        commentview.constrainHeight(constant: 50.calcvaluey())
        
        addSubview(commentStackView)
        commentStackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)

        seperator2.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)

        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)

        let scrollView = UIScrollView()
        scrollView.contentInset.bottom = 40.calcvaluey()
        scrollView.addSubview(vstack)
        vstack.fillSuperview()
        
        vstack.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true

        addSubview(scrollView)
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        

        scrollView.bounces = false

        scrollView.anchor(top: topAnchor, leading: leadingAnchor, bottom: seperator.topAnchor, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 46.calcvaluex()))

        
        machinesubjectlabel.font = UIFont(name: "Roboto-Bold", size: 18.calcvaluex())
        machinesubjectlabel.numberOfLines = 0
        machinesubjectlabel.textColor = #colorLiteral(red: 0.2519185713, green: 0.2544128145, blue: 0.2544128145, alpha: 1)
        
        addresslabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        addresslabel.numberOfLines = 0
        addresslabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        contactLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        contactLabel.numberOfLines = 0
        contactLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        contactPhoneLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        contactPhoneLabel.numberOfLines = 0
        contactPhoneLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        machinesublabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        machinesublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
       //machinesublabel.text = "機台運作問題"
        machinesublabel.numberOfLines = 0
        machinetitle.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        machinetitle.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        //machinetitle.text = "VMT-X400"

        //titlelabel.text = "久大行銷股份有限公司"
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
       
        titlelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)

        
 
       // datelabel.text = "10分鐘前"
        datelabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        

        
        commentview.addSubview(numberofcomment)
        numberofcomment.centerInSuperview()
        //numberofcomment.text = "7則留言"
        numberofcomment.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        numberofcomment.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        

        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        collectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        collectionview?.delegate = self
        collectionview?.dataSource = self
        collectionview?.showsVerticalScrollIndicator = false
        collectionview?.isScrollEnabled = false
        collectionview?.isHidden = false
        collectionview?.register(maintainFileCell.self, forCellWithReuseIdentifier: "cell")
        vstack.addArrangedSubview(collectionview!)
        collectionview?.constrainHeight(constant: 168.calcvaluey())
        //collectionview?.constrainWidth(constant: 486.calcvaluex())
//        collectionviewHeightAnchor = collectionview?.heightAnchor.constraint(equalToConstant: 0)
     //   collectionviewHeightAnchor?.isActive = true
        collectionview?.backgroundColor = .clear
        
        vstack.addArrangedSubview(UIView())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func seeFullComment(){
        let infoCon = CommentFullInfoController()
        infoCon.pop_con = self.pop_con
        infoCon.con = self.con
        infoCon.id = data?.id
        infoCon.data = data?.messages ?? []
        infoCon.modalPresentationStyle = .overCurrentContext
        General().getTopVc()?.present(infoCon, animated: false, completion: nil)
    }
}
extension CommentHeader : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return files.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! maintainFileCell
        if let url = URL(string: files[indexPath.item].path_url ?? "") {
            cell.imageView.image = url.getThumnailImage()
        }
        else{
            cell.imageView.image = nil
        }
        cell.xbutton.isHidden = true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 2 * 8.calcvaluex())/3, height: 80.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let url = URL(string: files[indexPath.item].path_url ?? "") {
            OpenFile().open(url: url)
        }
    }
}
