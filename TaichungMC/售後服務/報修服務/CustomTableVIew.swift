//
//  CustomTableVIew.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/12.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class CustomTableView:UITableView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 3
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
            return 1
        
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
            return 0
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 89.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        vd.backgroundColor = .clear
        
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 499.calcvaluey()
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CommentHeader()
        vd.layer.cornerRadius = 15
        vd.backgroundColor = .white
        if #available(iOS 11.0, *) {
            vd.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        return vd
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = CommentCell(style: .default, reuseIdentifier: "cellidd")
            cell.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
            return cell
        

    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        separatorStyle = .none
       layer.cornerRadius = 15
        bounces = false
        if #available(iOS 11.0, *) {
           layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

