//
//  MaintainController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/5.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class CommentBar: UIView{
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_attach_file_message"))
    let ctextfield = FormTextView(placeText: "留言".localized, mode: .Maintain)
    let sendButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addtopshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))

        
        

        ctextfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        

        ctextfield.layer.cornerRadius = 49.calcvaluey()/2
        ctextfield.placeholderLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        let stackview = UIStackView()
        stackview.alignment = .center
        stackview.addArrangedSubview(imageview)
        imageview.constrainWidth(constant: 28.calcvaluex())
        imageview.constrainHeight(constant: 28.calcvaluey())
        ctextfield.button = sendButton
        sendButton.setImage(#imageLiteral(resourceName: "ic_faq_next").withRenderingMode(.alwaysTemplate), for: .normal)
        sendButton.isUserInteractionEnabled = false
        sendButton.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        sendButton.contentVerticalAlignment = .fill
        sendButton.contentHorizontalAlignment = .fill
        sendButton.constrainWidth(constant: 36.calcvaluex())
        sendButton.constrainHeight(constant: 36.calcvaluex())
        stackview.addArrangedSubview(ctextfield)
        stackview.spacing = 24.calcvaluex()
        stackview.addArrangedSubview(sendButton)
        addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 24.calcvaluex(), bottom: 12.calcvaluey(), right: 24.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class maintainFilterItemView : UIView {
    var label : UILabel = {
        let label = UILabel()
        
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.text = "Asadasd x"
        return label
    }()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        xbutton.tintColor = .white
        
        return xbutton
    }()
    var data : FilterButtonClass? {
        didSet{
            label.text = "\(data?.value ?? "") \(data?.text ?? "")"
        }
    }
    var delegate: filterItemViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //addSubview(label)
        //label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: )
        backgroundColor = #colorLiteral(red: 0.1119983569, green: 0.1871647537, blue: 0.2257367671, alpha: 1)
        self.constrainHeight(constant: 40.calcvaluey())
        self.layer.cornerRadius = 20.calcvaluey()
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        xbutton.centerYInSuperview()
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: xbutton.leadingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        xbutton.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
    }
    @objc func removeSelf(){
        self.removeFromSuperview()
        if let data = data {
            delegate?.removeItem(mode: data.mode)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol MaintainFilterViewDelegate {
    func reloadFilter()
}
class MaintainFilterView : UIView,filterItemViewDelegate {
    var delegate: MaintainFilterViewDelegate?
    func removeItem(mode: maintainFilterMode) {
        switch mode {
        case .Status :
            data.status = nil
        case .Customer :
            data.customer = nil
        case .startTime :
            data.startTime = nil
        case .endTime :
            data.endTime = nil
        case .startAndEndTime :
            data.startTime = nil
            data.endTime = nil
        case .Employee :
            data.employee = nil
        case .StatusArray :
            data.statusArray = []
        default:
            ()
        }
        delegate?.reloadFilter()
    }
    
    var data = FilterClass() {
        didSet{
            stackview.safelyRemoveArrangedSubviews()
            
            for i in data.getArray() {
                let vd = maintainFilterItemView()
                vd.data = i
                vd.delegate = self
                stackview.addArrangedSubview(vd)
            }
            stackview.addArrangedSubview(UIView())
        }
    }
    let stackview = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .center)
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        let scrollView = UIScrollView()
        
        scrollView.addSubview(stackview)
        scrollView.showsHorizontalScrollIndicator = false
        stackview.fillSuperview()
        //stackview.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        stackview.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
        addSubview(scrollView)
        scrollView.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class MaintainController: SampleController , UITextFieldDelegate,FixFilterDelegate,MaintainFilterViewDelegate{
    func reloadFilter() {
        if filterData?.getArray().count == 0{
            filter_heightAnchor?.constant = 0.calcvaluey()
            tableviewanchor?.top?.constant = 26.calcvaluey()
            self.view.layoutIfNeeded()
        }
        fix_array = []
        current_page = 1
        fetchApi2(scrollToTop: true)
    }
    
    func selectedFilter(filter: FilterClass) {
        
        if filter.getArray().count != 0{
            filterData = filter
            filter_heightAnchor?.constant = 60.calcvaluey()
            tableviewanchor?.top?.constant = 0
        }
        else{
            filterData = nil
            filter_heightAnchor?.constant = 0.calcvaluey()
            tableviewanchor?.top?.constant = 26.calcvaluey()
        }
        filterData?.isInterview = false
        filterView.data = filterData ?? FilterClass()
        
        self.view.layoutIfNeeded()
        hud.show(in: self.view)
        fix_array = []
        current_page = 1
        
        fetchApi2(scrollToTop: true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            nameCode = nil
        }
        else{
            nameCode = textField.text
        }
        hud.show(in: self.view)
        current_page = 1
        fix_array = []
        fetchApi2(scrollToTop: true)
    }
    let searchController = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .plain)
    let filterbutton = ActionButton(width: 100.calcvaluex())
    let beginningview = UIView()
    var beginningviewanchor:AnchoredConstraints!
    var rightview = UIView()
    var rightviewanchor : AnchoredConstraints!
    var rightviewtableview = CustomTableView(frame: .zero, style: .grouped)
    var fixinfoview = FixInfoView()
    weak var mainview:ViewController?
    
    
    var addfix = AddButton4()
    var c_fix_data : FixData?
    var current_page = 1
    var fix_array = [FixModel]()
    var isRefreshing = false
    var selectedIndex : Int? = nil
    var isExpand = false
    var nameCode : String?
    var filterData : FilterClass?
    var filterView = MaintainFilterView()
    var filter_heightAnchor : NSLayoutConstraint?
    var tableviewanchor : AnchoredConstraints?
    let hud = JGProgressHUD()
    override func changeLang() {
        super.changeLang()
        searchController.attributedPlaceholder = NSAttributedString(string: "搜尋報修主旨或內容".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        filterbutton.setTitle("篩選".localized, for: .normal)
        addfix.titles.text = "新增報修".localized
        
        if let data = filterData {
            filterView.data = data
        }
        
        tableview.reloadData()
        fixinfoview.commentheader.data = fixinfoview.commentheader.data
        fixinfoview.commentbar.ctextfield.placeholderLabel.text = "留言".localized
        fixinfoview.commentheader.formButton.setTitle("服務單".localized, for: .normal)
        //fixinfoview.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print(221)
        settingButton.isHidden = false
        titleview.label.text = "報修服務".localized.replacingOccurrences(of: "\n", with: " ")
        background_view.image = #imageLiteral(resourceName: "background_afterservice3")
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        view.addSubview(beginningview)
        
        beginningviewanchor = beginningview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 0, right: 0),size: .init(width: 487.calcvaluex(), height: 0))
//        searchController.placeholder = "搜尋標題或客戶名稱"
        
        searchController.attributedPlaceholder = NSAttributedString(string: "搜尋報修主旨或內容".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchController.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        searchController.delegate = self
        searchController.clearButtonMode = .unlessEditing
        beginningview.addSubview(searchController)
        searchController.anchor(top: beginningview.topAnchor, leading: beginningview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        searchController.layer.cornerRadius = 46.calcvaluey()/2
        
        searchController.backgroundColor = .white
        beginningview.addSubview(filterView)
        filterView.delegate = self
        filterView.anchor(top: searchController.bottomAnchor, leading: searchController.leadingAnchor, bottom: nil, trailing: nil)
        filter_heightAnchor = filterView.heightAnchor.constraint(equalToConstant: 0)
        filter_heightAnchor?.isActive = true
        beginningview.addSubview(tableview)
        tableview.showsVerticalScrollIndicator = false
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        //tableview.backgroundColor = .red
        tableview.backgroundColor = .clear
        tableview.register(FixRecordCell.self, forCellReuseIdentifier: "fix")
        tableviewanchor = tableview.anchor(top: filterView.bottomAnchor, leading: beginningview.leadingAnchor, bottom: beginningview.bottomAnchor, trailing: beginningview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        filterView.trailingAnchor.constraint(equalTo: tableview.trailingAnchor).isActive = true
       
        filterbutton.isUserInteractionEnabled = true
        filterbutton.addTarget(self, action: #selector(filteraction), for: .touchUpInside)
        filterbutton.setTitle("篩選".localized, for: .normal)
        beginningview.addSubview(filterbutton)
        filterbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: beginningview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        filterbutton.centerYAnchor.constraint(equalTo: searchController.centerYAnchor).isActive = true
        
        searchController.trailingAnchor.constraint(equalTo: filterbutton.leadingAnchor,constant: -16.calcvaluex()).isActive = true
        
        view.addSubview(rightview)
        
        rightview.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        rightview.backgroundColor = .white
        rightview.layer.cornerRadius = 15
//        if #available(iOS 11.0, *) {
//            rightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        rightviewanchor = rightview.anchor(top: topview.bottomAnchor, leading: beginningview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 13.calcvaluex(), bottom: menu_bottomInset + 24.calcvaluey(), right: 13.calcvaluex()),size: .init(width: 0, height: 0))
        rightview.isHidden = true
        self.fixinfoview.con = self
        rightview.addSubview(fixinfoview)
        fixinfoview.fillSuperview()

//        rightviewtableview.anchor(top: rightview.topAnchor, leading: rightview.leadingAnchor, bottom: commentbar.topAnchor, trailing: rightview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 2, right: 0))
        
        
        addfix.backgroundColor = MajorColor().oceanlessColor
       
        addfix.titles.text = "新增報修".localized
        extrabutton_stackview.addArrangedSubview(addfix)
        addfix.addTarget(self, action: #selector(newserviceaction), for: .touchUpInside)
        
//        if GetUser().isAfterSale() || GetUser().isPortal() == 1{
//            addfix.isHidden = false
//        }
//        else{
//            addfix.isHidden = true
//        }
    }

    func fetchDetailInfo(){
        
        if let selectedIndex = selectedIndex {
            let hud = JGProgressHUD()
            hud.show(in: self.fixinfoview)
            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(self.fix_array[selectedIndex].id)", decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                        self.rightview.isHidden = false
                        self.fixinfoview.commentheader.data = json

                    }
                }

            }

        }
    }
    func fetchApi2(scrollToTop:Bool = false){
        var urlString = "api-or/v1/upkeeps?page=\(current_page)&pens=30"
        if let name = nameCode {
            urlString += "&name=\(name)"
        }
        if let data = filterData {
            if let customer = data.customer {
                
                urlString += "&account_code=\(customer.code ?? "")"
            }
            if let status = data.status {
                
                urlString += "&statuses=[\"\(status.key ?? "")\"]"
            }
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd"
            
            if let start = data.startTime {
                urlString += "&start_date=\(dateformatter.string(from: start))"
            }
            if let end = data.endTime {
                urlString += "&end_date=\(dateformatter.string(from: end))"
            }
            if data.statusArray.count != 0{
                var text = "["
                for (index,i) in data
                    .statusArray.enumerated() {
                    text += "\"\(i.key ?? "")\""
                    if index != data.statusArray.count - 1{
                        text += ","
                    }
                }
                text += "]"
                urlString += "&statuses=\(text)"
            }
            
        }
        NetworkCall.shared.getCall(parameter: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), decoderType: FixData.self) { (json) in
                DispatchQueue.main.async {
                    self.hud.dismiss()
                    self.isRefreshing = false
                    if let json = json {
                        self.c_fix_data = json

                        for i in json.data {
                            self.fix_array.append(i)
                        }
                        if json.data.count > 0{
                        self.fetchDetailInfo()
                        }

                        self.tableview.isHidden = false
                        self.tableview.reloadData()
                        if scrollToTop {
                            if self.fix_array.count > 0{
                        self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                            self.tableview.contentOffset = .zero
                            }
                        }
                    }
                }

            }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isExpand {
        selectedIndex = 0
        }
        current_page = 1
        fix_array = []
        tableview.isHidden = true
        hud.show(in: self.view)
        fetchApi2(scrollToTop: true)
        
    }
    @objc func newserviceaction(){
        let vd = AddNewServiceController()
        vd.con2 = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    @objc func filteraction(){
        let vd = FixFilteringController()
        vd.modalPresentationStyle = .overCurrentContext
        //vd.delegate = self
        if let data = self.filterData {
            vd.data = data
        }
        
        vd.delegate = self
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: false, completion: nil)
    }
    override func popview() {
        isExpand = false
        let pre_index = selectedIndex
        selectedIndex = nil
        //tableview.reloadData()
        mainview?.showcircle()
        
                beginningviewanchor.leading?.constant = 333.calcvaluex()
        beginningviewanchor.width?.constant = 665.calcvaluex()

                rightview.isHidden = true
                rightviewanchor.leading?.constant = 0
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        } completion: { (done) in
            if done {
                self.tableview.reloadSections(IndexSet(integer: pre_index ?? 0), with: .none)
                //self.tableview.reloadData()
                //self.tableview.scrollToRow(at: IndexPath(row: 0, section: pre_index ?? 0), at: .middle, animated: false)
            }
        }

    }
   
}
extension MaintainController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {

        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 {
            return self.fix_array.count + 1
        }
        return self.fix_array.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

        return 12.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 && indexPath.section == self.fix_array.count {
            return 50.calcvaluey()
        }
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        vd.backgroundColor = .clear
        
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 && indexPath.section == self.fix_array.count {
            
            return RefreshTableCell(style: .default, reuseIdentifier: "refresh")
        }
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "fix", for: indexPath) as! FixRecordCell
        if selectedIndex == indexPath.section {
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
//        cell.statuslabel.text = statusArray.first(where: { (st) -> Bool in
//            return st.key == self.fix_array[indexPath.section].status
//        })?.value
        if indexPath.section < self.fix_array.count {
        cell.setData(data:self.fix_array[indexPath.section])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count > 5 && indexPath.section == self.fix_array.count && !isRefreshing{
                isRefreshing = true
                current_page += 1
                fetchApi2()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.section
        
        self.tableview.reloadData()
        self.fetchDetailInfo()

        
    }


    
}
