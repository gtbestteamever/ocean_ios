//
//  ProductManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension UIView {
    func addShadowColor(opacity:Float) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = opacity
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    func addshadowColor(color:UIColor = .white) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.08
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    func addtopshadowColor(color:UIColor) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = .init(width: 0, height: -2)
        self.layer.shadowOpacity = 1
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
class SearchTextField : UITextField{
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.leftViewMode = .unlessEditing
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_search_bar").withRenderingMode(.alwaysOriginal))
        //imageview.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        imageview.frame = .init(x: 26.calcvaluex(), y: 0, width: 24.calcvaluex(), height: 24.calcvaluex())
        imageview.contentMode = .scaleAspectFit
        let test = UIView(frame: .init(x: 26.calcvaluex(), y: 0, width: 24.calcvaluex(), height: 24.calcvaluex()))
        test.addSubview(imageview)
        self.leftView = test
        if UIDevice().userInterfaceIdiom == .pad {
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        }
        else{
            self.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        }
        self.returnKeyType = .search
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.clearButtonMode = .always
        
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 58.calcvaluex(), dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 32.calcvaluex()))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {

        
        return bounds.inset(by: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 32.calcvaluex()))
    }
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        let originalrect = super.clearButtonRect(forBounds: bounds)
        return originalrect.offsetBy(dx: -10.calcvaluex(), dy: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CGFloat {
    func calcvaluey() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
        return UIScreen.main.bounds.height * self/768
        }
        
        return UIScreen.main.bounds.height * self/936
        
    }
    func calcvaluex() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
        return UIScreen.main.bounds.width * self/1024
        }
        
        return UIScreen.main.bounds.width * self/468
    }
}
extension Int {
    func calcvaluey() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
        return UIScreen.main.bounds.height * CGFloat(self)/768
        }
        
        return UIScreen.main.bounds.height * CGFloat(self)/936
        
    }
    func calcvaluex() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
        return UIScreen.main.bounds.width * CGFloat(self)/1024
        }
        
        return UIScreen.main.bounds.width * CGFloat(self)/468
    }
}
extension Double {
    func calcvaluey() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            return UIScreen.main.bounds.height * CGFloat(self)/768
        }
        
        return UIScreen.main.bounds.height * CGFloat(self)/936
        
    }
    func calcvaluex() -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            return UIScreen.main.bounds.width * CGFloat(self)/1024
        }
        
        return UIScreen.main.bounds.width * CGFloat(self)/468
    }
}
protocol ProductManagmentDelegate {
    func expand()
    func collapse()
}
enum ProductOrientation {
    case Vertical
    case Horizontal
}
protocol filterItemViewDelegate {
    func removeItem(mode:maintainFilterMode)
}
class filterItemView : UIView {
    var label : UILabel = {
        let label = UILabel()
        
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.text = "Asadasd x"
        return label
    }()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        xbutton.tintColor = .white
        
        return xbutton
    }()
    var mode : maintainFilterMode?
    var delegate: filterItemViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //addSubview(label)
        //label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: )
        backgroundColor = MajorColor().oceanlessColor
        self.constrainHeight(constant: 40.calcvaluey())
        self.layer.cornerRadius = 20.calcvaluey()
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        xbutton.centerYInSuperview()
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: xbutton.leadingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        xbutton.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
    }
    @objc func removeSelf(){
        self.removeFromSuperview()
        if let mode = mode {
            delegate?.removeItem(mode: mode)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum maintainFilterMode {
    case CompanyCode
    case Customer
    case Status
    case startTime
    case endTime
    case startAndEndTime
    case Employee
    case Warranty
    case StatusArray
}
struct maintainFilterItem {
    var mode : maintainFilterMode = .CompanyCode
    var value : Any?
}
class filteringView : UIView,filterItemViewDelegate {
    weak var con : ProductManagementController?
    weak var con2 : iPhoneProductManagementController?
    func removeItem(mode: maintainFilterMode) {
        if let index = data.firstIndex(where: { (mm) -> Bool in
            return mm.mode == mode
        }) {
            data.remove(at: index)
        }
    }
    let scrollview = UIScrollView()
    let stackview = UIStackView()
    var heightContstraint : NSLayoutConstraint?
    var data = [maintainFilterItem]() {
        didSet{
            stackview.safelyRemoveArrangedSubviews()
            for i in data {
                let ff = filterItemView()
                ff.mode = i.mode
                var pretext = ""
                if i.mode == .CompanyCode {
                    pretext = "\("客戶".localized): "
                    ff.label.text = "\(pretext)\((i.value as? Customer)?.name ?? "")"
                }
                else if i.mode == .Warranty {
                    pretext = "\("保固狀態".localized): "
                    ff.label.text = "\(pretext)\((i.value as? WarrantyModel)?.name.localized ?? "")"
                }
               
                ff.delegate = self
                stackview.addArrangedSubview(ff)
            }
            stackview.addArrangedSubview(UIView())
            if data.count != 0{
            heightContstraint?.constant = 40.calcvaluey()
            }
            else{
                heightContstraint?.constant = 0
            }
            self.con?.hud.show(in: self.con!.container)
            self.con?.current_page = 1
            self.con?.total_data = []
            self.con?.fetchApi(scrollToTop: true)
            
            self.con2?.hud.show(in: self.con2?.view ?? UIView())
            self.con2?.current_page = 1
            self.con2?.total_data = []
            self.con2?.fetchApi(scrollToTop: true)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(scrollview)
        scrollview.fillSuperview()
        scrollview.addSubview(stackview)
        stackview.fillSuperview()
        stackview.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        stackview.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        
        stackview.axis = .horizontal
        stackview.spacing = 12.calcvaluex()
        stackview.alignment = .center
        
        heightContstraint = self.heightAnchor.constraint(equalToConstant: 0)
        heightContstraint?.isActive = true
        //let ss = filterItemView()
        //stackview.addArrangedSubview(ss)
        //stackview.addArrangedSubview(UIView())
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductManagementController: SampleController,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.current_page = 1
        if textField.text == "" || textField.text == nil{
            searchCode = nil
        }
        else{
        searchCode = textField.text
        }
        
        self.total_data = []
        self.fetchApi(scrollToTop: true)
    }
    var searchCode : String? = nil
    let container = UIView()
    var containeranchor: AnchoredConstraints!
    var searchbar = SearchTextField()
    var topcolorview = UIView()
    var bottomcolorview = UIView()
    var selectionview : UICollectionView!
    var dataview : UICollectionView!
    //let registerbutton = AddButton()
    let changeorientation = UIImageView(image: #imageLiteral(resourceName: "ic_filter").withRenderingMode(.alwaysTemplate))
    var delegate:ProductManagmentDelegate!
    var isExpand = false
    var orientation:ProductOrientation = .Vertical
    var dataviewanchor:AnchoredConstraints!
    let settingbutton = UIImageView(image: #imageLiteral(resourceName: "ic_set"))
    var settingbuttonanchor:AnchoredConstraints!
    var current_page = 1
    var current_data : UnitData?
    
    var total_data = [UnitModel]()
    var isRefreshing = false
    let filterButton = ActionButton(width: 90.calcvaluex())
    let filterview = filteringView()
    override func viewDidLoad() {
        super.viewDidLoad()
        background_view.image = #imageLiteral(resourceName: "background_afterservice1")
        filterButton.setTitle("篩選".localized, for: .normal)
        settingButton.isHidden = false
        titleview.label.text = "產品管理".localized.replacingOccurrences(of: "\n", with: " ")
        
      // topview.addSubview(settingbutton)
        
//        settingbuttonanchor = settingbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
//        settingbutton.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
//        settingbutton.isHidden = true
//        settingbutton.isUserInteractionEnabled = true
//        settingbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showSetting)))
        view.addSubview(topcolorview)
        topcolorview.backgroundColor = .clear
//        view.addSubview(bottomcolorview)
//        bottomcolorview.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        topcolorview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        //bottomcolorview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        backbutton.image = #imageLiteral(resourceName: "ic_menu")
        
        view.backgroundColor = .white
        view.addSubview(container)
        containeranchor = container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing:nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 478.calcvaluex(), height: 0))
        //container.backgroundColor = .red
        container.addSubview(searchbar)
        searchbar.backgroundColor = .white
        searchbar.clearButtonMode = .unlessEditing
        searchbar.addshadowColor(color: #colorLiteral(red: 0.9489133954, green: 0.9490793347, blue: 0.948915422, alpha: 1))
        searchbar.delegate = self
        searchbar.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        searchbar.layer.cornerRadius = 46.calcvaluey()/2
        searchbar.attributedPlaceholder = NSAttributedString(string: "搜尋機台編號".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
//        container.addSubview(filterButton)
//        filterButton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 38.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
//        filterButton.centerYAnchor.constraint(equalTo: searchbar.centerYAnchor).isActive = true
        
        //searchbar.trailingAnchor.constraint(equalTo: filterButton.leadingAnchor,constant: -12.calcvaluex()).isActive = true
        //changeorientation.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        filterButton.layer.cornerRadius = 46.calcvaluey()/2
//        changeorientation.anchor(top: nil, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 38.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        
        //changeorientation.centerYAnchor.constraint(equalTo: searchbar.centerYAnchor).isActive = true
       // stackview.axis = .horizontal
       // stackview.spacing = 12.calcvaluex()
        //stackview.alignment = .fill
       // filterview.data = []
        container.addSubview(filterview)
        filterview.con = self
        filterview.anchor(top: searchbar.bottomAnchor, leading: searchbar.leadingAnchor, bottom: nil, trailing: searchbar.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        

        
        let newflowlayout = UICollectionViewFlowLayout()
        newflowlayout.scrollDirection = .vertical
        
        dataview = UICollectionView(frame: .zero, collectionViewLayout: newflowlayout)
        dataview.delegate = self
        dataview.dataSource = self
        dataview.showsVerticalScrollIndicator = false
        dataview.backgroundColor = .clear
        dataview.register(ProductManagerCell.self, forCellWithReuseIdentifier: "dcell")
        dataview.register(ProductManagerVerticalCell.self, forCellWithReuseIdentifier: "cell2")
        dataview.register(RefreshCell.self, forCellWithReuseIdentifier: "refresh")
        container.addSubview(dataview)
        dataviewanchor = dataview.anchor(top:filterview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 30.calcvaluey(), right: 0))
        
        

        filterButton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
        //changeorientation.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(goFilter) ))
        //changeorientation.isUserInteractionEnabled = true
        
        
        
    }
    @objc func goFilter(){
        let vd = FilterServiceController()
        vd.con.con = self
        vd.con.data = filterview.data
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
    let hud = JGProgressHUD()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hud.show(in: container)
        current_page = 1
        self.total_data = []
        dataview.isHidden = true
        fetchApi(scrollToTop: true)
    }
    func fetchApi(scrollToTop:Bool = false){
        self.isRefreshing = true
        var urlString = "api-or/v1/appliances?pens=30&page=\(current_page)"
        if let code = searchCode {
            urlString += "&name=\(code)"
        }
        if let dt = filterview.data.first(where: { mt in
            return mt.mode == .CompanyCode
        }), let company = dt.value as? Customer{
            urlString += "&account_code=\(company.code ?? "")"
        }
        if let dt = filterview.data.first(where: { mt in
            return mt.mode == .Warranty
        }), let warranty = dt.value as? WarrantyModel{
            urlString += "&in_warranty_period=\(warranty.status)"
        }
        NetworkCall.shared.getCall(parameter: urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "", decoderType: UnitData.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
            if let json = json {
                print(111,json)
                self.current_data = json
                for i in json.data {
                    self.total_data.append(i)
                }
                
                    self.isRefreshing = false
                    self.dataview.isHidden = false
                    self.dataview.reloadData()
                    if scrollToTop && self.total_data.count != 0{
                        self.dataview.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
                        self.dataview.contentOffset = .zero
                    }
                
            }
            }
        }
    }
    
    func didscrollin(){
      //  settingbuttonanchor.trailing?.constant = -26.calcvaluex()
       // settingbutton.isHidden = false
       
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    override func popview() {
        isExpand = false
        delegate.collapse()
    }
    
    override func changeLang() {
        super.changeLang()
        searchbar.attributedPlaceholder = NSAttributedString(string: "搜尋機台編號".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        filterButton.setTitle("篩選".localized, for: .normal)
        dataview.reloadData()
        
        filterview.data = filterview.data
    }
}
extension ProductManagementController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == total_data.count {
            if !isRefreshing && (current_page < (current_data?.meta.last_page ?? 0)){
                current_page += 1
                fetchApi()
            }
        }
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if(self.dataview.contentOffset.y >= (self.dataview.contentSize.height - self.dataview.bounds.size.height)) {
//            print(3321)
////                if !isPageRefreshing {
////                    isPageRefreshing = true
////                    print(page)
////                    page = page + 1
////                    YourApi(page1: page)
////                }
//            }
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if total_data.count >= 5 && ((current_data?.meta.current_page ?? 0) < (current_data?.meta.last_page ?? 0)){
            return total_data.count + 1
        }
        return total_data.count
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == dataview {
            if indexPath.item == total_data.count && total_data.count >= 5 && ((current_data?.meta.current_page ?? 0) < (current_data?.meta.last_page ?? 0)){
                return .init(width: collectionView.frame.width, height: 50.calcvaluey())
            }
            if orientation == .Horizontal {
            return .init(width: 274.calcvaluex(), height: collectionView.frame.height-8.calcvaluey())
            }
            else{
                return .init(width: collectionView.frame.width, height: 108.calcvaluey())
            }
        }
        else{
         
        return .init(width: 204.calcvaluex(), height: 42.calcvaluey())
            
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == dataview {
            if orientation == .Horizontal {
            return 34.calcvaluex()
            }
            else{
                return 22.calcvaluey()
            }
            

        }
        return 18.calcvaluex()
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        if collectionView == dataview {
//            if orientation == .Vertical {
//                return 0
//            }
//        }
//        return 0
//    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dataview {
            if indexPath.item == total_data.count && total_data.count >= 5 && ((current_data?.meta.current_page ?? 0) < (current_data?.meta.last_page ?? 0)){
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "refresh", for: indexPath)
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! ProductManagerVerticalCell
                if indexPath.item < total_data.count {
                cell.setData(data:total_data[indexPath.item])
                }
                return cell
            }

            
        }
        else{
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)

        cell.addshadowColor(color: #colorLiteral(red: 0.8900358081, green: 0.8945220113, blue: 0.9055349231, alpha: 1))
        
        cell.layer.cornerRadius = 42.calcvaluey()/2
        let label = UILabel()
            label.text = "久大行銷股份有限公司"
            label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
            cell.addSubview(label)
            label.centerInSuperview()
            
            
            if indexPath.item == 0 {
                cell.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
                label.textColor = .white
            }
            else {
                cell.backgroundColor = .white
                label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            }
        
        return cell
            

        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dataview {
//            if !isExpand {
//            delegate.expand()
//            self.didscrollin()
//            self.isExpand = true
//            }
           // else{
                
//                let vd = ProductInfoController()
//                vd.data = total_data[indexPath.item]
//                vd.modalPresentationStyle = .fullScreen
//                self.present(vd, animated: true, completion: nil)
            
                let sd = OceanProductManageTabController()
            sd.data = total_data[indexPath.item]
                sd.modalPresentationStyle = .fullScreen
            
            self.present(sd, animated: true, completion: nil)
            //}
        }
    }

}
