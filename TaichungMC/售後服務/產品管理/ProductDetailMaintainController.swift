//
//  ProductDetailMaintainController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class CustomCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !(__CGSizeEqualToSize(bounds.size,self.intrinsicContentSize)){
            self.invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
class N_CollectionViewCell : UICollectionViewCell {
    let imageview = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.contentMode = .scaleAspectFill
        imageview.clipsToBounds = true
        addSubview(imageview)
        imageview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ProductMaintainCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let spacing = 8.calcvaluex() * 2
        return .init(width: (collectionView.frame.width - spacing)/3, height: 100.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! N_CollectionViewCell
//        cell.imageview.sd_setImage(with: URL(string: data?.files[indexPath.item].path_url?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let url = URL(string: data?.files[indexPath.item].path_url ?? "") {
//            OpenFile().open(url: url)
//        }
    }
}
class ProductMaintainCell : UITableViewCell {
    weak var con : ProductDetailMaintainController?
    weak var con2 : iPhoneProductDetailMaintainController?
    let label = UILabel()
    let detailLabel = UILabel()
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let checkbox = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal"))
    let droparrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate))
    var collectionviewHeightAnchor : NSLayoutConstraint?
    let detailContainer = UIView()
    let settingButton = UIImageView(image: #imageLiteral(resourceName: "ic_more"))
    
    @objc func goRecord(){
        let vd = ServiceFormHistoryController()
        vd.selectOrInput = 0
        vd.id = data?.id
        vd.modalPresentationStyle = .fullScreen
        
        General().getTopVc()?.present(vd, animated: true)
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        let container = UIView()
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 2.calcvaluey(), left: 2.calcvaluex(), bottom: 18.calcvaluey(), right: 2.calcvaluex()))
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 80.calcvaluey()).isActive = true

        let horizontalStack = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .center)
        checkbox.isHidden = true
        checkbox.constrainWidth(constant: 30.calcvaluex())
        checkbox.constrainHeight(constant: 30.calcvaluex())
        horizontalStack.addArrangedSubview(checkbox)
        
        
        label.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        label.textColor = MajorColor().oceanlessColor
        label.text = "12月12日 下午3:25"
        label.setContentHuggingPriority(.required, for: .horizontal)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        
       
        droparrow.constrainWidth(constant: 30.calcvaluex())
        droparrow.constrainHeight(constant: 30.calcvaluex())
        
        horizontalStack.addArrangedSubview(label)
        horizontalStack.addArrangedSubview(droparrow)
        
        let header_container = UIView()
        
        header_container.addSubview(horizontalStack)
        horizontalStack.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 24.calcvaluex(), bottom: 12.calcvaluey(), right: 24.calcvaluex()))

        let vertstack = Vertical_Stackview()
        vertstack.addArrangedSubview(header_container)
        header_container.constrainHeight(constant: 80.calcvaluey())
        //vertstack.addArrangedSubview(UIView())
        
        detailContainer.backgroundColor = #colorLiteral(red: 0.9563082553, green: 0.9563082553, blue: 0.9563082553, alpha: 1)
        vertstack.addArrangedSubview(detailContainer)
 
        detailContainer.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            detailContainer.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            detailContainer.roundCorners2([.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 15.calcvaluex())
        }

        let dot = UIView()
        dot.backgroundColor = MajorColor().oceanSubColor
        dot.layer.cornerRadius = 10.calcvaluex()/2

        let dotContainer = UIView()
        dotContainer.addSubview(dot)
    
        dot.fillSuperview(padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
        dot.constrainWidth(constant: 10.calcvaluex())
        dot.constrainHeight(constant: 10.calcvaluex())
        
        settingButton.constrainWidth(constant: 30.calcvaluex())
        settingButton.constrainHeight(constant: 30.calcvaluex())
        settingButton.isUserInteractionEnabled = true
        settingButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(maintainOption)))
        
        detailLabel.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        detailLabel.numberOfLines = 0
        detailLabel.isUserInteractionEnabled = true

        detailLabel.setContentHuggingPriority(.required, for: .horizontal)
        detailLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        detailLabel.setContentHuggingPriority(.required, for: .vertical)
        detailLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        detailLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goRecord)))
        let h_stack2 = Horizontal_Stackview(spacing:12.calcvaluex(),alignment: .top)
        h_stack2.addArrangedSubview(dotContainer)
        h_stack2.addArrangedSubview(detailLabel)
        h_stack2.addArrangedSubview(settingButton)
        settingButton.isHidden = true
        let vertstack2 = Vertical_Stackview(distribution:.fill,spacing:12.calcvaluey())
        detailContainer.addSubview(vertstack2)

        vertstack2.fillSuperview(padding: .init(top: 12.calcvaluey(), left: 30.calcvaluex(), bottom: 12.calcvaluey(), right: 24.calcvaluex()))
        vertstack2.addArrangedSubview(h_stack2)
        
        vertstack2.addArrangedSubview(collectionview)
        
//        detailContainer.addSubview(h_stack2)
//        h_stack2.anchor(top: detailContainer.topAnchor, leading: detailContainer.leadingAnchor, bottom: nil, trailing: detailContainer.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 30.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
//
//        detailContainer.addSubview(collectionview)
//        collectionview.anchor(top: h_stack2.bottomAnchor, leading: h_stack2.leadingAnchor, bottom: detailContainer.bottomAnchor, trailing: h_stack2.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        detailContainer.isHidden = true
        collectionview.backgroundColor = .clear
//        collectionview.translatesAutoresizingMaskIntoConstraints = false
        collectionviewHeightAnchor = collectionview.heightAnchor.constraint(equalToConstant: 0)
        collectionviewHeightAnchor?.isActive = true
        collectionview.delegate = self
        collectionview.dataSource = self

        collectionview.register(N_CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        container.addSubview(vertstack)
        
        vertstack.fillSuperview(padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        droparrow.tintColor = MajorColor().oceanSubColor
        droparrow.isUserInteractionEnabled = true
        droparrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dropDown)))
        
        checkbox.tintColor = MajorColor().oceanSubColor
        checkbox.isUserInteractionEnabled = true
        checkbox.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getSelected)))
    }
    func editMaintain(){
//        let vd = AddNewRecordController()
//       // vd.data = data
//        vd.con = self.con
//        vd.con2 = self.con2
//        vd.main_data = self.data
//        vd.modalPresentationStyle = .overFullScreen
//        vd.modalTransitionStyle = .crossDissolve
//        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        General().getTopVc()?.present(vd, animated: true, completion: nil)
//        //self.present(vd, animated: true, completion: nil)
    }
    func deleteMaintain(){
//        let alertCon = UIAlertController(title: "您確定要刪除這項保養嗎?".localized, message: nil, preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "確定".localized, style: .default) { (_) in
//            let hud = JGProgressHUD()
//            hud.show(in: General().getTopVc()?.view ?? UIView())
//            if let data = self.data{
//                NetworkCall.shared.getDelete(parameter: "api-or/v1/appliances/\(data.appliance_code)/maintenances/\(data.id)", decoderType: UpdateClass.self) { (json) in
//                    DispatchQueue.main.async {
//                        hud.dismiss()
//                        if let json = json?.data,json {
//                            self.con?.fetchApi()
//                            self.con2?.fetchApi()
//                        }
//                    }
//                    
//                }
//                
//            }
//        }
//        
//        let action2 = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//        
//        alertCon.addAction(action1)
//        alertCon.addAction(action2)
//        General().getTopVc()?.present(alertCon, animated: true, completion: nil)

    }
    @objc func maintainOption(){
        let alert = UIAlertController(title: "保養設定".localized, message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "編輯保養".localized, style: .default) { (_) in
            self.editMaintain()
        }

        let action3 = UIAlertAction(title: "刪除保養".localized, style: .default) { (_) in
            self.deleteMaintain()
        }
        //alert.addAction(action1)
        //alert.addAction(action2)
        alert.addAction(action3)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = settingButton
            presenter.sourceRect = settingButton.bounds
            presenter.permittedArrowDirections = .any
            }
        General().getTopVc()?.present(alert, animated: true, completion: nil)
    }
    @objc func getSelected(){
        data?.isSelected = !(data?.isSelected ?? false)
        if data?.isSelected ?? false{
            checkbox.image = #imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate)
        }
        else{
            checkbox.image = #imageLiteral(resourceName: "btn_check_box_normal")
        }
        self.tableView?.reloadRows(at: [IndexPath(row: self.tag, section: 0)], with: .none)
    }
    @objc func dropDown(){
        
        
        data?.isExpand = !(data?.isExpand ?? false)
        if data?.isExpand ?? false{
            droparrow.image = #imageLiteral(resourceName: "ic_arrow_drop_up").withRenderingMode(.alwaysTemplate)
        }
        else{
            droparrow.image = #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate)
        }
        self.tableView?.reloadRows(at: [IndexPath(row: self.tag, section: 0)], with: .none)
    }
    var data : Maintain?
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.data = nil
//        collectionview.reloadData()
//        collectionview.layoutIfNeeded()
//        self.layoutIfNeeded()
//    }
    func setData(data:Maintain) {
        self.data = data
        //print(77721,data.maintained_at,data.files)
        self.label.text = Date().convertToDateShortComponent(text: data.maintained_at ?? "",format: "yyyy-MM-dd HH:mm:ss")
        self.detailLabel.text = data.order_code
        
        if data.isExpand ?? false{
            
            self.detailContainer.isHidden = false
            droparrow.image = #imageLiteral(resourceName: "ic_arrow_drop_up").withRenderingMode(.alwaysTemplate)
        }
        else{
           
            self.detailContainer.isHidden = true
            droparrow.image = #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate)
        }
        
        if data.isSelected ?? false{
            checkbox.image = #imageLiteral(resourceName: "btn_check_box_pressed").withRenderingMode(.alwaysTemplate)
        }
        else{
            checkbox.image = #imageLiteral(resourceName: "btn_check_box_normal")
        }
        
//        collectionview.reloadData()
//        collectionview.layoutIfNeeded()
////        self.layoutIfNeeded()
//        if data.files.count == 0{
//            collectionviewHeightAnchor?.constant = 0
//        }
//        else{
//            print(33321,collectionview.collectionViewLayout.collectionViewContentSize.height)
//            collectionviewHeightAnchor?.constant = collectionview.collectionViewLayout.collectionViewContentSize.height
//        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ProductDetailMaintainController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductMaintainCell
        cell.con = self
        cell.tag = indexPath.item
        cell.setData(data: contentData[indexPath.item])
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentData.count
    }
}
class ProductDetailMaintainController : SampleController {
    var newMaintain = AddButton4()
    let shareButton = UIButton(type: .custom)
    var data : UnitModel? {
        didSet{
            topLabel.text = data?.code ?? ""
            if let dt = data?.reference{
                bottomLabel.text = dt.titles?.getLang()
                imageview.sd_setImage(with: dt.getImage(), completed: nil)
            }
        }
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let imageview = UIImageView()
    var contentData = [Maintain]()
    let container = UIView()
    @objc func addMaintain(){
        
        let vd = ServiceFormController()
        let form = ServiceForm()
        form.selectOrInput = 0
        form.status = "processing"
        
        let time = FormTime()
        let date = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        time.startTime = dateformatter.string(from: date)
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        time.endTime = dateformatter.string(from: date)
        form.timeAndDuration = [time]
        form.upkeep_id = data?.account?.code ?? ""
        form.isMaintenance = true
        form.customer.name = data?.account?.name ?? ""
        form.customer.id = data?.account?.code ?? ""
        form.customer.address = data?.address ?? ""
        form.machine.code = data?.code ?? ""
        form.machine.type = data?.reference?.titles?.getLang() ?? ""
        form.machine.maintainStatus = data?.in_warranty_period == 1 ? "保固中".localized : "已過保".localized
        vd.container.form_data = form
        
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleview.label.text = "產品管理".localized
        self.backbutton.isHidden = false
        self.settingButton.isHidden = true
        newMaintain.backgroundColor = MajorColor().oceanlessColor
        newMaintain.titles.text = "新增保養".localized
        newMaintain.addTarget(self, action: #selector(addMaintain), for: .touchUpInside)
        extrabutton_stackview.addArrangedSubview(newMaintain)
        shareButton.setImage(#imageLiteral(resourceName: "ic_tab_bar_share").withRenderingMode(.alwaysTemplate), for: .normal)
        shareButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sharing)))
        shareButton.tintColor = MajorColor().oceanlessColor
        shareButton.contentVerticalAlignment = .fill
        shareButton.contentHorizontalAlignment = .fill
        shareButton.constrainHeight(constant: 50.calcvaluex())
        shareButton.constrainWidth(constant: 50.calcvaluex())
        extrabutton_stackview.addArrangedSubview(shareButton)
       
        
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: productMenuTabHeight + 24.calcvaluey(), right: 24.calcvaluex()))
        

        container.addSubview(topLabel)
        //topLabel.text = "River 3"
        topLabel.textColor = MajorColor().oceanlessColor
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        container.addSubview(bottomLabel)
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.centerXAnchor,padding: .init(top: 48.calcvaluey(), left: 60.calcvaluex(), bottom: 0, right: 8.calcvaluex()))
        topLabel.setContentHuggingPriority(.required, for: .vertical)
        topLabel.setContentCompressionResistancePriority(.required, for: .vertical)

       // bottomLabel.text = "River 系列"
        bottomLabel.setContentHuggingPriority(.required, for: .vertical)
        bottomLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        bottomLabel.textColor = #colorLiteral(red: 0.7278061509, green: 0.727912426, blue: 0.7277830243, alpha: 1)
        bottomLabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        container.addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        imageview.contentMode = .scaleAspectFit
        container.addSubview(imageview)
      
        imageview.anchor(top: bottomLabel.bottomAnchor, leading: bottomLabel.leadingAnchor, bottom: container.bottomAnchor, trailing: container.centerXAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0))
        
        
       tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        container.addSubview(tableview)
        tableview.anchor(top: container.topAnchor, leading: container.centerXAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 8.calcvaluex(), bottom: 24.calcvaluey(), right: 24.calcvaluex()))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(ProductMaintainCell.self, forCellReuseIdentifier: "cell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    func removeDirectory(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let interview_record = documentsDirectory.appendingPathComponent("maintain_record")

            do {
                try FileManager.default.removeItem(at: interview_record)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func getShareDate()->String{
        let format = DateFormatter()
         format.dateFormat = "yyyyMMdd"
        
        return format.string(from: Date())
        
    }
    func writeToFolder(fileName:String){
        guard let url = Bundle.main.url(forResource: "SampleMaintain", withExtension: "xlsx") else {return}
        if let data = try? Data(contentsOf: url), let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,let _ = try? FileManager.default.createDirectory(at: documentsDirectory.appendingPathComponent("maintain_record"), withIntermediateDirectories: true, attributes: nil) {
            
        
        let dataPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(fileName)_\(getShareDate()).xlsx")
            

                try? data.write(to: dataPath)
            
        
        }
        

    }
    @objc func sharing(){
        removeDirectory()
        var selectedDetailArray = [Maintain]()
        for i in contentData {
            if i.isSelected == true {
                selectedDetailArray.append(i)
            }
        }
        if selectedDetailArray.count == 0{
            let alertController = UIAlertController(title: "請至少選擇一個紀錄", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let name = self.data?.code {
               
               self.writeToFolder(fileName: name)

                let documentPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(name)_\(self.getShareDate()).xlsx")
                print(documentPath)
                let spreadSheet = BRAOfficeDocumentPackage.open(documentPath.path)
                
                let worksheet : BRAWorksheet = spreadSheet?.workbook.worksheets[0] as! BRAWorksheet
                

                
                for (index,i) in selectedDetailArray.enumerated() {
                    let cell = worksheet.cell(forCellReference: "A\(index + 1)", shouldCreate: true)
                    let cell2 = worksheet.cell(forCellReference: "B\(index + 1)", shouldCreate: true)
                    
                    cell?.setStringValue(i.maintained_at )
                    cell2?.setStringValue(i.order_code ?? "")

                }

  
                spreadSheet?.save()
                let activity = UIActivityViewController(activityItems: [documentPath], applicationActivities: nil)
                
                activity.popoverPresentationController?.sourceView = self.shareButton
                activity.popoverPresentationController?.sourceRect = self.shareButton.frame
                activity.popoverPresentationController?.permittedArrowDirections = .any
   
                General().getTopVc()?.present(activity, animated: true, completion: nil)
            }
        }
    }
    func fetchApi(){
        let hud = JGProgressHUD()
        hud.show(in: self.container)
        tableview.isHidden = true
        if let dt = data, let code = dt.code
        {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(code)/maintenances?pens=100000&page=\(1)", decoderType: MaintenanceData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.maintain_orders {
                        self.contentData = json
                        self.tableview.isHidden = false
                        self.tableview.reloadData()
                    }
                }

            }
        }

    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}
