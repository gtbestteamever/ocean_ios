//
//  ProductDetailMaintainController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD



extension iPhoneProductDetailMaintainController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductMaintainCell
        //cell.con = self
        cell.tag = indexPath.row
        cell.con2 = self
        cell.setData(data: contentData[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentData.count
    }
}
class iPhoneProductDetailMaintainController : SampleController {
    var newMaintain = AddButton4()
    let shareButton = UIButton(type: .custom)
    var data : UnitModel? {
        didSet{
            topLabel.text = data?.code ?? ""
            if let dt = data?.reference{
                bottomLabel.text = dt.titles?.getLang()
                imageview.sd_setImage(with: dt.getImage(), completed: nil)
            }
        }
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let imageview = UIImageView()
    var contentData = [Maintain]()
    let container = UIView()
    @objc func addMaintain(){
        let vd = AddNewRecordController()
        vd.data = data
        //vd.con = self
        vd.con2 = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleview.label.text = "產品管理".localized
        self.backbutton.isHidden = false
        self.settingButton.isHidden = true
        newMaintain.backgroundColor = MajorColor().oceanlessColor
        if UIDevice().userInterfaceIdiom == .phone {
            newMaintain.titles.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        }
        newMaintain.titles.text = "新增保養".localized

        newMaintain.addTarget(self, action: #selector(addMaintain), for: .touchUpInside)
        extrabutton_stackview.addArrangedSubview(newMaintain)
//        shareButton.setImage(#imageLiteral(resourceName: "ic_tab_bar_share").withRenderingMode(.alwaysTemplate), for: .normal)
//        shareButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sharing)))
//        shareButton.tintColor = MajorColor().oceanlessColor
//        shareButton.contentVerticalAlignment = .fill
//        shareButton.contentHorizontalAlignment = .fill
//        shareButton.constrainHeight(constant: 50.calcvaluex())
//        shareButton.constrainWidth(constant: 50.calcvaluex())
//        extrabutton_stackview.addArrangedSubview(shareButton)
       
        
        container.backgroundColor = .clear
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: productMenuTabHeight + 24.calcvaluey(), right: 0))
        

        container.addSubview(topLabel)
        //topLabel.text = "River 3"
        topLabel.textColor = MajorColor().oceanlessColor
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        container.addSubview(bottomLabel)
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        topLabel.setContentHuggingPriority(.required, for: .vertical)
        topLabel.setContentCompressionResistancePriority(.required, for: .vertical)

       // bottomLabel.text = "River 系列"
        bottomLabel.setContentHuggingPriority(.required, for: .vertical)
        bottomLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        bottomLabel.textColor = #colorLiteral(red: 0.7278061509, green: 0.727912426, blue: 0.7277830243, alpha: 1)
        bottomLabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        container.addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        imageview.contentMode = .scaleAspectFit
        
        container.addSubview(imageview)
        imageview.centerXInSuperview()
        imageview.anchor(top: bottomLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0),size: .init(width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.width/2))
        
        
       tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        container.addSubview(tableview)
        tableview.anchor(top: imageview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(ProductMaintainCell.self, forCellReuseIdentifier: "cell")
        tableview.contentInset.bottom = 60.calcvaluey()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    func removeDirectory(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let interview_record = documentsDirectory.appendingPathComponent("maintain_record")

            do {
                try FileManager.default.removeItem(at: interview_record)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    func getShareDate()->String{
        let format = DateFormatter()
         format.dateFormat = "yyyyMMdd"
        
        return format.string(from: Date())
        
    }
    func writeToFolder(fileName:String){
        guard let url = Bundle.main.url(forResource: "SampleMaintain", withExtension: "xlsx") else {return}
        if let data = try? Data(contentsOf: url), let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,let _ = try? FileManager.default.createDirectory(at: documentsDirectory.appendingPathComponent("maintain_record"), withIntermediateDirectories: true, attributes: nil) {
            
        
        let dataPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(fileName)_\(getShareDate()).xlsx")
            

                try? data.write(to: dataPath)
            
        
        }
        

    }
    @objc func sharing(){
        removeDirectory()
        var selectedDetailArray = [Maintain]()
        for i in contentData {
            if i.isSelected == true {
                selectedDetailArray.append(i)
            }
        }
        if selectedDetailArray.count == 0{
            let alertController = UIAlertController(title: "請至少選擇一個紀錄", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let name = self.data?.code {
               
               self.writeToFolder(fileName: name)

                let documentPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(name)_\(self.getShareDate()).xlsx")
                print(documentPath)
                let spreadSheet = BRAOfficeDocumentPackage.open(documentPath.path)
                
                let worksheet : BRAWorksheet = spreadSheet?.workbook.worksheets[0] as! BRAWorksheet
                

                
                for (index,i) in selectedDetailArray.enumerated() {
                    let cell = worksheet.cell(forCellReference: "A\(index + 1)", shouldCreate: true)
                    let cell2 = worksheet.cell(forCellReference: "B\(index + 1)", shouldCreate: true)
                    
                    cell?.setStringValue(i.maintained_at )
                    cell2?.setStringValue(i.order_code ?? "")

                }

  
                spreadSheet?.save()
                let activity = UIActivityViewController(activityItems: [documentPath], applicationActivities: nil)
                
                activity.popoverPresentationController?.sourceView = self.shareButton
                activity.popoverPresentationController?.sourceRect = self.shareButton.frame
                activity.popoverPresentationController?.permittedArrowDirections = .any
   
                General().getTopVc()?.present(activity, animated: true, completion: nil)
            }
        }
    }
    func fetchApi(){
        let hud = JGProgressHUD()
        hud.show(in: self.container)
        tableview.isHidden = true
        if let dt = data, let code = dt.code
        {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(code)/maintenances?pens=100000&page=\(1)", decoderType: MaintenanceData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.maintain_orders {
                        self.contentData = json
                        self.tableview.isHidden = false
                        self.tableview.reloadData()
                    }
                }

            }
        }

    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}

