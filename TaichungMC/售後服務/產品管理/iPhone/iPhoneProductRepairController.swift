//
//  OceanProductRepairController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension iPhoneOceanProductRepairController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count {
            
            return RefreshTableCell(style: .default, reuseIdentifier: "refresh")
        }
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FixRecordCell

        cell.setData(data:self.fix_array[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count {
            return 50.calcvaluey()
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 {
            return self.fix_array.count + 1
        }
        return self.fix_array.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count && !isRefreshing{
                isRefreshing = true
                current_page += 1
                fetchApi2()
            }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        callApiAndOpen(index: indexPath.item)
    }
}
class iPhoneOceanProductRepairController : SampleController{
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let imageview = UIImageView()
    var newMaintain = AddButton4()
    var data : UnitModel? {
        didSet{
            topLabel.text = data?.code ?? ""
            if let dt = data?.reference{
                bottomLabel.text = dt.titles?.getLang()
                imageview.sd_setImage(with: dt.getImage(), completed: nil)
            }
        }
    }
    var isRefreshing = false
    let container = UIView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var current_page = 1
    @objc func addNewRecord(){
        let vd = AddNewServiceController()
        vd.con3 = self
        vd.selected_code = data?.code
        vd.auto_address = data?.address
        vd.addressField.textfield.text = data?.address
        vd.auto_account_code = data?.account?.code
        vd.auto_department_code = data?.department_code

       // vd.con = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleview.label.text = "產品管理".localized
        self.backbutton.isHidden = false
        self.settingButton.isHidden = true
        newMaintain.backgroundColor = MajorColor().oceanlessColor
        if UIDevice().userInterfaceIdiom == .phone {
            newMaintain.titles.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        }
        newMaintain.titles.text = "新增報修".localized
        extrabutton_stackview.addArrangedSubview(newMaintain)
        newMaintain.addTarget(self, action: #selector(addNewRecord), for: .touchUpInside)
       
        
        container.backgroundColor = .clear
        container.layer
            .cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: productMenuTabHeight + 24.calcvaluey(), right: 0))
        

        container.addSubview(topLabel)
        //topLabel.text = "River 3"
        topLabel.textColor = MajorColor().oceanlessColor
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        topLabel.setContentHuggingPriority(.required, for: .vertical)
        topLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        bottomLabel.setContentHuggingPriority(.required, for: .vertical)
        bottomLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        container.addSubview(bottomLabel)
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
       // bottomLabel.text = "River 系列"
        bottomLabel.textColor = #colorLiteral(red: 0.7278061509, green: 0.727912426, blue: 0.7277830243, alpha: 1)
        bottomLabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        container.addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        imageview.contentMode = .scaleAspectFit

        
        container.addSubview(imageview)
        imageview.centerXInSuperview()
        imageview.anchor(top: bottomLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0),size: .init(width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.width/2))
        
        
       tableview.backgroundColor = .clear
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        container.addSubview(tableview)
        tableview.anchor(top: imageview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(FixRecordCell.self, forCellReuseIdentifier: "cell")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        current_page = 1
        self.fix_array = []
        fetchApi2()
    }
    func callApiAndOpen(index:Int){
        let hud = JGProgressHUD()
        hud.show(in: self.view)

        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(self.fix_array[index].id)", decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
//                        self.fixinfoview.commentheader.data = json
                        let ft = FixPopInfoController()
                       // ft.infoview.s_con = self
                        //ft.infoview.
                        ft.infoview.commentheader.data = json
                        ft.modalPresentationStyle = .overCurrentContext
                        self.present(ft, animated: false, completion: nil)
                    }
                }

            }

        
        
    }
    var c_fix_data : FixData?
    var fix_array = [FixModel]()
    func fetchApi2(scrollToTop:Bool = false){
        if let code = data?.code {
            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeeps?page=\(current_page)&pens=10&appliance_code=\(code)", decoderType: FixData.self) { (json) in
                DispatchQueue.main.async {
                    self.isRefreshing = false
                    if let json = json {
                        
                        self.c_fix_data = json
                        for i in json.data {
                            self.fix_array.append(i)
                        }
                        self.tableview.reloadData()
                        if scrollToTop {
                            if self.fix_array.count > 0{
                        self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                            self.tableview.contentOffset = .zero
                            }
                        }
                    }
                }

            }
        }
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}

