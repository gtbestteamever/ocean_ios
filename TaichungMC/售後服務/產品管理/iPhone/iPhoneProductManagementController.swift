//
//  iPhoneProductManagementController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/19/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class iPhoneProductCell : UITableViewCell {
    let container = UIView()
    let statuslabel = PaddedLabel2()
    let imageview = UIImageView()
    let sublabel = UILabel()
    let titlelabel = UILabel()
    let thirdLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        
        container.backgroundColor = .white
        container.layer.cornerRadius = 8.calcvaluex()
        container.addshadowColor()
        
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 1.calcvaluey(), left: 1.calcvaluex(), bottom: 12.calcvaluey(), right: 1.calcvaluex()))
        
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 90.calcvaluey()).isActive = true
        
        container.addSubview(imageview)
        imageview.contentMode = .scaleAspectFit
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0 ),size: .init(width: 60.calcvaluex(), height: 60.calcvaluex()))
        
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 18.calcvaluex()))
        statuslabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        statuslabel.textAlignment = .center
        statuslabel.layer.borderWidth = 1.calcvaluex()
        statuslabel.layer.cornerRadius = 5
        statuslabel.setContentHuggingPriority(.required, for: .horizontal)
        statuslabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        statuslabel.centerYInSuperview()
        
        
        titlelabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        titlelabel.numberOfLines = 0
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        sublabel.numberOfLines = 0
        thirdLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        thirdLabel.numberOfLines = 0
        let stackview = Vertical_Stackview(spacing:6.calcvaluey())
        stackview.addArrangedSubview(titlelabel)
        stackview.addArrangedSubview(sublabel)
        stackview.addArrangedSubview(thirdLabel)
        
        container.addSubview(stackview)
        stackview.anchor(top: container.topAnchor, leading: imageview.trailingAnchor, bottom: container.bottomAnchor, trailing: statuslabel.leadingAnchor,padding: .init(top: 12.calcvaluey(), left: 11.calcvaluex(), bottom: 12.calcvaluey(), right: 11.calcvaluex()))
    }
    func setData(data:UnitModel) {
        if data.in_warranty_period == 0 {
            statuslabel.text = "已過保".localized
            statuslabel.textColor = #colorLiteral(red: 0.7169483304, green: 0.7170530558, blue: 0.7169254422, alpha: 1)
            statuslabel.layer.borderColor = #colorLiteral(red: 0.7169483304, green: 0.7170530558, blue: 0.7169254422, alpha: 1)
        }
        else{
            statuslabel.text = "保固中".localized
            statuslabel.textColor = MajorColor().oceanSubColor
            statuslabel.layer.borderColor = MajorColor().oceanSubColor.cgColor
        }
        
        titlelabel.text = data.reference?.titles?.getLang()
        sublabel.text = data.code
        thirdLabel.text = data.account?.name
        
        if let dt = data.reference {
           
            if let files = dt.files, let file = files.getLang()?.first?.path_url {

                switch file {
                case .arrayString(let ss) :
                    if let fm = ss?.first {
                        imageview.sd_setImage(with: URL(string: fm?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
                    }
                    else{
                        imageview.image = nil
                    }
                case .string(let st):
                    if let fm = st {
                        imageview.sd_setImage(with: URL(string: fm.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
                    }
                    else{
                        imageview.image = nil
                    }
                }
            }
            else{
                imageview.image = nil
            }
            
        }
        else{
            imageview.image = nil
            titlelabel.text = nil
        }
        
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class iPhoneProductManagementController : SampleController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.current_page = 1
        if textField.text == "" || textField.text == nil{
            searchCode = nil
        }
        else{
        searchCode = textField.text
        }
        print(3321,searchCode)
        self.hud.show(in: self.view)
        self.total_data = []
        self.fetchApi(scrollToTop: true)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == total_data.count - 1 {
            if !isRefreshing && (current_page < (current_data?.meta.last_page ?? 0)){
                current_page += 1
                fetchApi()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return total_data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! iPhoneProductCell
        cell.setData(data: total_data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sd = OceanProductManageTabController()
        print(992,total_data.count)
        sd.data = total_data[indexPath.item]
        sd.modalPresentationStyle = .fullScreen
    
        self.present(sd, animated: true, completion: nil)
    }
    var searchbar = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var current_page = 1
    var current_data : UnitData?
    
    var total_data = [UnitModel]()
    var isRefreshing = false
    var searchCode : String? = nil
    let filterButton = ActionButton(width: 90.calcvaluex())
    let filterview = filteringView()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "產品管理".localized
        view.addSubview(filterButton)
        filterButton.layer.cornerRadius = 46.calcvaluey()/2
        filterButton.setTitle("篩選".localized, for: .normal)
        filterButton.anchor(top: topview.bottomAnchor, leading: nil, bottom: nil, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        filterButton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
        view.addSubview(searchbar)
        searchbar.backgroundColor = .white
        searchbar.clearButtonMode = .unlessEditing
        searchbar.addshadowColor()
        searchbar.attributedPlaceholder = NSAttributedString(string: "搜尋機台編號".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchbar.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: nil, trailing: filterButton.leadingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchbar.layer.cornerRadius = 46.calcvaluey()/2
        searchbar.delegate = self
        view.addSubview(filterview)
        view.addSubview(tableview)
        filterview.con2 = self
        filterview.anchor(top: searchbar.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: tableview.topAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        
        tableview.backgroundColor = .clear
        tableview.anchor(top: filterview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: menu_bottomInset + 12.calcvaluey(), right: 0))
        
        tableview.showsVerticalScrollIndicator = false
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(iPhoneProductCell.self, forCellReuseIdentifier: "cell")
    }
    @objc func goFilter(){
        let vd = FilterServiceController()
        vd.con.con2 = self
        vd.con.data = filterview.data
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
    let hud = JGProgressHUD()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        current_page = 1
        total_data = []
        tableview.isHidden = true
        fetchApi(scrollToTop: true)
        hud.show(in: self.view)
    }
    
    func fetchApi(scrollToTop:Bool = false){
        self.isRefreshing = true
        var urlString = "api-or/v1/appliances?pens=30&page=\(current_page)"
        if let code = searchCode {
            urlString += "&name=\(code)"
        }
        if let dt = filterview.data.first(where: { mt in
            return mt.mode == .CompanyCode
        }), let company = dt.value as? Customer{
            urlString += "&account_code=\(company.code ?? "")"
        }
        if let dt = filterview.data.first(where: { mt in
            return mt.mode == .Warranty
        }), let warranty = dt.value as? WarrantyModel{
            urlString += "&in_warranty_period=\(warranty.status)"
        }
        NetworkCall.shared.getCall(parameter: urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "", decoderType: UnitData.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
            if let json = json {
                print(111,json)
                self.current_data = json
                for i in json.data {
                    self.total_data.append(i)
                }
                
                    self.isRefreshing = false
                    self.tableview.isHidden = false
                    self.tableview.reloadData()
                    if scrollToTop && self.total_data.count != 0{
                        self.tableview.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
                        self.tableview.contentOffset = .zero
                    }
                
            }
                else{
                    self.tableview.isHidden = false
                    self.tableview.reloadData()
                }
            }
        }
    }
}
