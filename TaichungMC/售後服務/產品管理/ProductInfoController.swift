//
//  ProductInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD

struct ProductInfo {
    var title:String
    var value:String
    var color:UIColor
    
    init(title:String,value:String,color:UIColor = UIColor.black) {
        self.title = title
        self.value = value
        self.color = color
    }
}
class ProductInfoController: SampleController {
    var data : UnitModel? {
        didSet{
            if let dt = data?.reference{
                imageview.sd_setImage(with: dt.getImage(), completed: nil)
            }
           
            for (index,j) in infoarray.enumerated() {
                let titlelabel = UILabel()
                titlelabel.text = j.title
                titlelabel.font = UIFont(name: "Roboto-Light", size: 20.calcvaluex())
                if UserDefaults.standard.getConvertedLanguage() == "en" {
                    titlelabel.constrainWidth(constant: 110.calcvaluex())
                    titlelabel.numberOfLines = 2
                }
                else{
                    titlelabel.constrainWidth(constant: 80.calcvaluex())
                }
                
                let valuelabel = UILabel()
                
                
                valuelabel.numberOfLines = 2
                valuelabel.textColor = j.color
                switch index {
                case 0:
                    valuelabel.text = data?.code
                case 1:
                    if let mt = data?.installation_date {
                        valuelabel.text = Date().convertToDateShortComponent(text: mt)
                    }
                    
                case 2:
                    if let st = data?.warranty_date {
                        valuelabel.text = Date().convertToDateShortComponent(text: st)
                    }
                    
                case 3:
                    valuelabel.text = (data?.address ?? "")
                case 4:
                    valuelabel.text = data?.department?.title
                default:
                    ()
                }
                
                valuelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
                let hstack = UIStackView(arrangedSubviews: [titlelabel,valuelabel,UIView()])
                hstack.axis = .horizontal
                hstack.alignment = .top
                hstack.spacing = 18.calcvaluex()
                
                infoarrayvstack.addArrangedSubview(hstack)
            }
        }
    }
    let shadowcontainer = UIView()
    var selectionview : UICollectionView!
    let leftline = UIView()
    let largetitle = UILabel()
    let sublabel = UILabel()
    let datacontainer = UIView()
    let newfix = UIButton(type: .system)
    let newmaintain = UIButton(type: .system)
    let imageview = UIImageView()
    
    let infoarray = [ProductInfo(title: "機台序號".localized, value: ""),ProductInfo(title: "購入日期".localized, value: ""),ProductInfo(title: "保固期限".localized, value: ""),ProductInfo(title: "安裝地點".localized, value: ""),ProductInfo(title: "服務部門".localized, value: ""),ProductInfo(title: "業務員".localized, value: ""),ProductInfo(title: "代理商".localized, value: "")]
    var infoarrayvstack = UIStackView()
    
    var datacontaineranchor:AnchoredConstraints!
    var imageviewanchor:AnchoredConstraints!
    
    var slideinrow = UITableView(frame: .zero, style: .plain)
    var slideinrowanchor: AnchoredConstraints!
    var isExpandarray = [false,false,false,false,false,false,false]
    var isShare = false
    
    var sharebutton = UIImageView(image: #imageLiteral(resourceName: "ic_tab_bar_share"))
    var sharebuttonanchor:AnchoredConstraints!
    
    var sharingbuttonstackview = UIStackView()
    var addButton = AddButton()
    var addButtonanchor:AnchoredConstraints!
    var isFixMode = false
    var selectedIndex = 0
    
    var maintain_arr = [ReConstructMaintain]()
    var c_fix_data : FixData?
    var current_page = 1
    var fix_array = [FixModel]()
    var isRefreshing = false
    var history_data = [HistoryData]()
    var history_anchor : AnchoredConstraints?
    var history_label = UILabel()
    var history_heightAnchor : NSLayoutConstraint?
    var isHistory = false
    override func viewDidLoad() {
        super.viewDidLoad()

        shadowcontainer.layer.shadowColor = #colorLiteral(red: 0.8723884225, green: 0.8800089955, blue: 0.8848493099, alpha: 1)
        shadowcontainer.layer.shadowRadius = 2
        shadowcontainer.layer.shadowOffset = .init(width: 0, height: 4)
        shadowcontainer.layer.shadowOpacity = 1
        shadowcontainer.backgroundColor = .white
        shadowcontainer.layer.shouldRasterize = true
        shadowcontainer.layer.rasterizationScale = UIScreen.main.scale
        view.addSubview(shadowcontainer)
        shadowcontainer.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 65.calcvaluey()))
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        
        selectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        
        view.addSubview(selectionview)
        selectionview.backgroundColor = .clear
        selectionview.anchor(top: shadowcontainer.topAnchor, leading: shadowcontainer.leadingAnchor, bottom: shadowcontainer.bottomAnchor, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 80.calcvaluex(), bottom: -3, right: 0))
        selectionview.register(SelectionViewCell.self, forCellWithReuseIdentifier: "cell")
        selectionview.delegate = self
        
        selectionview.dataSource = self
        
        
        view.addSubview(leftline)
        leftline.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        leftline.anchor(top: shadowcontainer.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 66.calcvaluey(), left: 56.calcvaluex(), bottom: 0, right: 0),size: .init(width: 6, height: 56.calcvaluey()))
        leftline.layer.cornerRadius = 3
        
        view.addSubview(largetitle)
        
        largetitle.text = data?.reference?.title
        largetitle.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluex())
        largetitle.anchor(top: leftline.topAnchor, leading: leftline.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: -4, left: 24.calcvaluex(), bottom: 0, right: 0))
        
        view.addSubview(sublabel)
       
        sublabel.text = data?.code
        sublabel.font = UIFont(name: "Roboto-Light", size: 24.calcvaluex())
        sublabel.anchor(top: largetitle.bottomAnchor, leading: largetitle.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 0, bottom: 0, right: 0))
        
        view.addSubview(datacontainer)
        datacontainer.addshadowColor(color: #colorLiteral(red: 0.8441283107, green: 0.8483836651, blue: 0.858827889, alpha: 1))
        datacontainer.backgroundColor = .white
        datacontaineranchor = datacontainer.anchor(top: sublabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 44.calcvaluey(), left: -150.calcvaluex(), bottom: 0, right: 0),size: .init(width: 662.calcvaluex(), height: 0))
        
        datacontainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 318.calcvaluey()).isActive = true
        datacontainer.layer.cornerRadius = 318.calcvaluey()/2

        view.addSubview(newfix)
        newfix.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        newfix.setTitle(UserDefaults.standard.getConvertedLanguage() == "en" ? "+ Repair" : "新增保養", for: .normal)
        newfix.setTitleColor(.white, for: .normal)
        newfix.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        newfix.addTarget(self, action: #selector(addFixRecord), for: .touchUpInside)
        newfix.anchor(top: shadowcontainer.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 59.calcvaluex()),size: .init(width: 144.calcvaluex(), height: 48.calcvaluey()))
        newfix.layer.cornerRadius = 48.calcvaluey()/2
        newfix.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        view.addSubview(newmaintain)
        newmaintain.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        newmaintain.setTitle(UserDefaults.standard.getConvertedLanguage() == "en" ? "+ Maintenance" : "新增保養", for: .normal)
        newmaintain.addTarget(self, action: #selector(addNewRecord), for: .touchUpInside)
        newmaintain.setTitleColor(.white, for: .normal)
        newmaintain.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        newmaintain.anchor(top: shadowcontainer.bottomAnchor, leading: nil, bottom: nil, trailing: newfix.leadingAnchor,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 144.calcvaluex(), height: 48.calcvaluey()))
        newmaintain.layer.cornerRadius = 48.calcvaluey()/2
        newmaintain.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        
        view.addSubview(imageview)
        imageview.contentMode = .scaleAspectFit
       imageviewanchor = imageview.anchor(top: nil, leading: nil, bottom: nil, trailing:view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 46.calcvaluex()),size: .init(width: 341.calcvaluex(), height: 312.calcvaluey()))
        imageview.centerYAnchor.constraint(equalTo: datacontainer.centerYAnchor).isActive = true
       

        infoarrayvstack.axis = .vertical
        infoarrayvstack.spacing = 14.calcvaluey()
        

        
        datacontainer.addSubview(infoarrayvstack)
        

        
        infoarrayvstack.anchor(top: datacontainer.topAnchor, leading: datacontainer.leadingAnchor, bottom: datacontainer.bottomAnchor, trailing: datacontainer.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 206.calcvaluex(), bottom: 24.calcvaluey(), right: -500.calcvaluex()))

        slideinrow.contentInset.top = 26.calcvaluey()

        
        slideinrow.backgroundColor = .clear
        slideinrow.separatorStyle = .none
        slideinrow.delegate = self
        slideinrow.dataSource = self
        slideinrow.register(SlideinRowCell.self, forCellReuseIdentifier: "maintain")
        slideinrow.register(FixRecordCell.self, forCellReuseIdentifier: "fix")
        slideinrow.register(HistoryFormCell.self, forCellReuseIdentifier: "history")
        slideinrow.showsVerticalScrollIndicator = false
        view.addSubview(history_label)
        history_label.text = "僅呈現兩年內資料".localized
        history_anchor = history_label.anchor(top: shadowcontainer.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor)
        history_heightAnchor = history_label.heightAnchor.constraint(equalToConstant: 0)
        history_heightAnchor?.isActive = true
        view.addSubview(slideinrow)
        slideinrow.contentInset.bottom = 50.calcvaluey()
        slideinrowanchor = slideinrow.anchor(top: history_label.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 462.calcvaluex(), height: 0))
        history_label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        history_label.leadingAnchor.constraint(equalTo: slideinrow.leadingAnchor).isActive = true
        view.addSubview(sharebutton)
        sharebuttonanchor = sharebutton.anchor(top: nil, leading: nil, bottom: nil, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        sharebutton.centerYAnchor.constraint(equalTo: shadowcontainer.centerYAnchor).isActive = true
        sharebutton.isUserInteractionEnabled = true
        sharebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sharing)))
        
        sharingbuttonstackview.axis = .horizontal
        sharingbuttonstackview.spacing = 26.calcvaluex()
        sharingbuttonstackview.alignment = .center
        sharingbuttonstackview.distribution = .fill
        for (index,k) in ["取消".localized,"全選".localized,"分享".localized].enumerated() {
            let button = UIButton(type: .custom)
           
            button.tag = index
            button.setTitle(k, for: .normal)
            
            button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
            button.setTitleColor(.black, for: .normal)
            if index == 0{
            button.addTarget(self, action: #selector(sharingaction), for: .touchUpInside)
            }
            else if index == 1{
                button.addTarget(self, action: #selector(selectAllMaintain), for: .touchUpInside)
            }
            else{
                button.addTarget(self, action: #selector(shareSelectedMaintain), for: .touchUpInside)
            }
            sharingbuttonstackview.addArrangedSubview(button)
            
        }
        view.addSubview(sharingbuttonstackview)
        
        
        sharingbuttonstackview.anchor(top: nil, leading: nil, bottom: nil, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        sharingbuttonstackview.centerYAnchor.constraint(equalTo: shadowcontainer.centerYAnchor).isActive = true
        sharingbuttonstackview.isHidden = true
        view.addSubview(addButton)
        addButton.image.backgroundColor = .black
        addButton.addshadowColor(color: #colorLiteral(red: 0.7947732806, green: 0.7968279123, blue: 0.801630199, alpha: 1))
        addButton.titles.text = "新增保養".localized
        
        addButton.titles.textColor = .white
        addButton.backgroundColor = .black
        
        addButtonanchor = addButton.anchor(top: nil, leading: nil, bottom: self.view.bottomAnchor, trailing: slideinrow.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 2),size: .init(width: 0, height: 48.calcvaluey()))
        addButton.layer.cornerRadius = 48.calcvaluey()/2
        addButton.addTarget(self, action: #selector(addNewRecord), for: .touchUpInside)
        view.sendSubviewToBack(slideinrow)
        fetchPreloadData()
        fetchApi()
        fetchApi2()
        fetchApi3()
  
    }
    func fetchPreloadData(){
        if let id = data?.id {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(id)", decoderType: UnitPreciseData.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json?.data{
                        let department_index = self.infoarray.firstIndex { pr in
                            return pr.title == "服務部門".localized
                        }
                        let sales_index = self.infoarray.firstIndex { pr in
                            return pr.title == "業務員".localized
                        }
                        let vendor_index = self.infoarray.firstIndex { pr in
                            return pr.title == "代理商".localized
                        }
                        if let index = department_index {
                            let vd = self.infoarrayvstack.arrangedSubviews[index] as? UIStackView
                            let st = vd?.arrangedSubviews[1] as? UILabel
                            st?.text = json.department?.title
                        }
                        
                        if let index = sales_index {
                            let vd = self.infoarrayvstack.arrangedSubviews[index] as? UIStackView
                            let st = vd?.arrangedSubviews[1] as? UILabel
                            st?.text = json.sales?.name
                        }
                        if let index = vendor_index {
                            let vd = self.infoarrayvstack.arrangedSubviews[index] as? UIStackView
                            let st = vd?.arrangedSubviews[1] as? UILabel
                            st?.text = json.vendor?.name
                        }
                    }
                }


            }
        }
    }
    func fetchApi3(){
        if let code = data?.code {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(code)/services", decoderType: HistoryFormData.self) { (json) in
                if let json = json {
                    self.history_data = json.data
                }

            }
        }
    }
    func fetchApi2(){
        if let code = data?.code {
            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeeps?page=\(current_page)&pens=10&appliance_code=\(code)", decoderType: FixData.self) { (json) in
                DispatchQueue.main.async {
                    self.isRefreshing = false
                    if let json = json {
                        self.c_fix_data = json
                        for i in json.data {
                            self.fix_array.append(i)
                        }
                        self.slideinrow.reloadData()
                    }
                }

            }
        }
    }
    @objc func selectAllMaintain(){
        let bt = sharingbuttonstackview.arrangedSubviews[1] as? UIButton
        
            if bt?.titleLabel?.text == "全選" {
            
            for (index,_) in maintain_arr.enumerated() {
                maintain_arr[index].isSelected = true
            }
                bt?.setTitle("取消全選", for: .normal)
            }
            else{
                for (index,_) in maintain_arr.enumerated() {
                    maintain_arr[index].isSelected = false
                }
                    bt?.setTitle("全選", for: .normal)
            }

            self.slideinrow.reloadData()
        

    }
    func fetchApi(){
        //self.maintain_arr = []
        if let dt = data, let code = dt.code
        {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(code)/maintenances?pens=100000&page=\(current_page)", decoderType: MaintenanceData.self) { (json) in
                DispatchQueue.main.async {
                    var arr = [ReConstructMaintain]()
                    if let json = json?.maintain_orders {
                        print(776,json.count)
                        for i in json {
                            let st = self.maintain_arr.first { (mm) -> Bool in
                                return mm.data?.id == i.id
                            }
                            arr.append(ReConstructMaintain(data: i, isSelected: st?.isSelected ?? false, isExpand: st?.isExpand ?? false))
                           // self.maintain_arr.append()
                        }
                        self.maintain_arr = arr
                    }
                    self.slideinrow.reloadData()
                }

            }
        }

    }
    @objc func addFixRecord(){
        let vd = AddNewServiceController()
        
        vd.selected_code = data?.code
        vd.auto_address = data?.address
        vd.addressField.textfield.text = data?.address
        vd.auto_account_code = data?.account?.code
        vd.auto_department_code = data?.department_code

       // vd.con = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    @objc func addNewRecord(){
        if isFixMode {
            addFixRecord()
            
            return 
        }
        let vd = AddNewRecordController()
        vd.data = data
        //vd.con = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    @objc func sharingaction(){
        removeDirectory()
        for (index,_) in maintain_arr.enumerated() {
            maintain_arr[index].isSelected = false
        }
        isShare = false
        self.slideinrow.beginUpdates()
        self.slideinrow.reloadData()
        self.slideinrow.endUpdates()
        sharingbuttonstackview.isHidden = true
        sharebutton.isHidden = false
        
        addButtonanchor.bottom?.constant = -26.calcvaluey()
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func sharing(){
        addButtonanchor.bottom?.constant = 74.calcvaluey()
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        isShare = true
        
        self.slideinrow.beginUpdates()
        self.slideinrow.reloadData()
        self.slideinrow.endUpdates()
        sharingbuttonstackview.isHidden = false
        sharebutton.isHidden = true
        
    }
}
extension ProductInfoController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectionViewCell
        //cell.backgroundColor = .blue
        cell.label.text = ["詳細資訊".localized,"保養紀錄".localized,"報修紀錄".localized,"歷史服務單".localized][indexPath.item]
        if indexPath.item == selectedIndex{
            cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            cell.vd.isHidden = false
        }
        else{
            cell.label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            cell.vd.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 78.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 80.calcvaluex(), height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex == indexPath.item {
            return
        }
        selectedIndex = indexPath.item
        selectionview.reloadItems(at: selectionview.indexPathsForVisibleItems)
        
        if indexPath.item == 1{
            isHistory = false
            
            history_anchor?.top?.constant = 0
            history_heightAnchor?.constant = 0
            if GetUser().isAfterSale() || GetUser().isPortal() == 1 {
            addButton.isHidden = false
            }
            else{
                addButton.isHidden = true
            }
            datacontaineranchor.leading?.constant = -datacontainer.frame.width
            imageviewanchor.trailing?.constant = -627.calcvaluex()
            newfix.isHidden = true
            newmaintain.isHidden = true
            slideinrowanchor.leading?.constant = -slideinrow.frame.width - 26.calcvaluex()
            sharebuttonanchor.trailing?.constant = -26.calcvaluex()
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            isFixMode = false
            addButton.titles.text = "新增保養".localized
            slideinrow.reloadData()

            
        }
        else if indexPath.item == 0{
            datacontaineranchor.leading?.constant = -150.calcvaluex()
            imageviewanchor.trailing?.constant = -46.calcvaluex()

            slideinrowanchor.leading?.constant = 0
            sharebuttonanchor.trailing?.constant = 28.calcvaluex()

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                if GetUser().isAfterSale() || GetUser().isPortal() == 1 {
                self.newfix.isHidden = false
                self.newmaintain.isHidden = false
                }
                else{
                    self.newfix.isHidden = true
                    self.newmaintain.isHidden = true
                }
            }

        }
        else if indexPath.item == 2{
            history_anchor?.top?.constant = 0
            history_heightAnchor?.constant = 0
            if GetUser().isAfterSale() || GetUser().isPortal() == 1 {
            addButton.isHidden = false
            }
            else{
                addButton.isHidden = true
            }
            sharebuttonanchor.trailing?.constant = 28.calcvaluex()
            datacontaineranchor.leading?.constant = -datacontainer.frame.width
            imageviewanchor.trailing?.constant = -627.calcvaluex()
            newfix.isHidden = true
            newmaintain.isHidden = true
            slideinrowanchor.leading?.constant = -slideinrow.frame.width - 26.calcvaluex()
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            isFixMode = true
            isHistory = false
           
            addButton.titles.text = "新增報修".localized
            slideinrow.reloadData()

            
        }
        else if indexPath.item == 3{
            history_anchor?.top?.constant = 24.calcvaluey()
            history_heightAnchor?.constant = 20.calcvaluey()
            sharebuttonanchor.trailing?.constant = 28.calcvaluex()
            datacontaineranchor.leading?.constant = -datacontainer.frame.width
            imageviewanchor.trailing?.constant = -627.calcvaluex()
            newfix.isHidden = true
            newmaintain.isHidden = true
            slideinrowanchor.leading?.constant = -slideinrow.frame.width - 26.calcvaluex()
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            isFixMode = false
            isHistory = true
            addButton.isHidden = true
            slideinrow.reloadData()
        }
    }
    
}
extension ProductInfoController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFixMode {
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 {
                return self.fix_array.count + 1
            }
            return self.fix_array.count
        }
        if isHistory {
            return history_data.count
        }
        return self.maintain_arr.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isFixMode {
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count && !isRefreshing{
                isRefreshing = true
                current_page += 1
                fetchApi2()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFixMode {
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count {
                
                return RefreshTableCell(style: .default, reuseIdentifier: "refresh")
            }
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "fix", for: indexPath) as! FixRecordCell

            cell.setData(data:self.fix_array[indexPath.section])
            return cell
        }
        else if isHistory {
            let cell = tableView.dequeueReusableCell(withIdentifier: "history", for: indexPath) as! HistoryFormCell
            let data = history_data[indexPath.section]
            cell.machineheader.text = data.code
            cell.timelabel.text = data.completed_at
            cell.subheader.text = data.note
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "maintain", for: indexPath) as! SlideinRowCell
            cell.con = self
            cell.expandbutton.tag = indexPath.section
            cell.expandbutton.addTarget(self, action: #selector(expanding), for: .touchUpInside)
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if isShare{
            cell.selectbutton.isHidden = false
           
        }
        else{
            cell.selectbutton.isHidden = true
        }
            cell.setData(data: self.maintain_arr[indexPath.section])

            
        return cell
        }
    }
    @objc func expanding(sender:UIButton) {
        
        if maintain_arr[sender.tag].isExpand == true {
            maintain_arr[sender.tag].isExpand = false
        }
        else{
            maintain_arr[sender.tag].isExpand = true
        }
        //isExpand = true
       // slideinrow.beginUpdates()
        slideinrow.reloadData()
        //slideinrow.endUpdates()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFixMode {
            if current_page < (c_fix_data?.meta.last_page ?? 0) && fix_array.count >= 5 && indexPath.section == self.fix_array.count {
                return 50.calcvaluey()
            }
            return 104.calcvaluey()
        }
        else if isHistory {
            return 104.calcvaluey()
        }
        else{
        if self.maintain_arr[indexPath.section].isExpand == true
        {
            return UITableView.automaticDimension
        }
        else{
        return 80.calcvaluey()
        }
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFixMode {
            callApiAndOpen(index:indexPath.section)
        }
        //tableView.deselectRow(at: indexPath, animated: false)

    }
    func getShareDate()->String{
        let format = DateFormatter()
         format.dateFormat = "yyyyMMdd"
        
        return format.string(from: Date())
        
    }
    func callApiAndOpen(index:Int){
        let hud = JGProgressHUD()
        hud.show(in: self.view)

        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(self.fix_array[index].id)", decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
//                        self.fixinfoview.commentheader.data = json
                        let ft = FixPopInfoController()
                        //ft.infoview.s_con = self
                        //ft.infoview.
                        ft.infoview.commentheader.data = json
                        ft.modalPresentationStyle = .overCurrentContext
                        self.present(ft, animated: false, completion: nil)
                    }
                }

            }

        
        
    }
    
    func writeToFolder(fileName:String){
        guard let url = Bundle.main.url(forResource: "SampleMaintain", withExtension: "xlsx") else {return}
        if let data = try? Data(contentsOf: url), let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,let _ = try? FileManager.default.createDirectory(at: documentsDirectory.appendingPathComponent("maintain_record"), withIntermediateDirectories: true, attributes: nil) {
            
        
        let dataPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(fileName)_\(getShareDate()).xlsx")
            

                try? data.write(to: dataPath)
            
        
        }
        

    }
    func removeDirectory(){
        
            
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let interview_record = documentsDirectory.appendingPathComponent("maintain_record")

            do {
                try FileManager.default.removeItem(at: interview_record)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            }
        
    }
    
    @objc func shareSelectedMaintain() {
        removeDirectory()
        var selectedDetailArray = [ReConstructMaintain]()
        for i in maintain_arr {
            if i.isSelected == true {
                selectedDetailArray.append(i)
            }
        }
        if selectedDetailArray.count == 0{
            let alertController = UIAlertController(title: "請至少選擇一個紀錄", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let name = self.data?.code {
               
               self.writeToFolder(fileName: name)

                let documentPath = documentsDirectory.appendingPathComponent("maintain_record/maintenance_\(name)_\(self.getShareDate()).xlsx")
                print(documentPath)
                let spreadSheet = BRAOfficeDocumentPackage.open(documentPath.path)
                
                let worksheet : BRAWorksheet = spreadSheet?.workbook.worksheets[0] as! BRAWorksheet
                

                
                for (index,i) in selectedDetailArray.enumerated() {
                    let cell = worksheet.cell(forCellReference: "A\(index + 1)", shouldCreate: true)
                    let cell2 = worksheet.cell(forCellReference: "B\(index + 1)", shouldCreate: true)
                    
                    cell?.setStringValue(i.data?.maintained_at ?? "")
                    cell2?.setStringValue(i.data?.order_code ?? "")

                }

  
                spreadSheet?.save()
                let activity = UIActivityViewController(activityItems: [documentPath], applicationActivities: nil)
                
                activity.popoverPresentationController?.sourceView = self.sharingbuttonstackview.arrangedSubviews[1]
                activity.popoverPresentationController?.sourceRect = self.sharingbuttonstackview.arrangedSubviews[1].frame
                activity.popoverPresentationController?.permittedArrowDirections = .right
   
                self.present(activity, animated: true, completion: nil)
            }
        }

    }
}
class SelectionViewCell:UICollectionViewCell {
    let label = UILabel()
    let vd = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.text = "詳細資訊"
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        addSubview(label)
        label.centerInSuperview()
        
        vd.isHidden = true
        vd.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        
        addSubview(vd)
        vd.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,size: .init(width: 50.calcvaluex(), height: 6))
        vd.centerXInSuperview()
        vd.layer.cornerRadius = 3
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton : UIButton {
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 32.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton2 : UIButton {
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 20.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton3 : UIButton{
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 134.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton4: UIButton {
        let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    var mode : ScheduleMode = .New
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.4861037731, green: 0.5553061962, blue: 0.705704987, alpha: 1)
        addshadowColor()
        constrainHeight(constant: 50.calcvaluey())
        layer.cornerRadius = 50.calcvaluey()/2
//        addSubview(image)
//        image.centerYInSuperview()
//        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        addSubview(titles)
        titles.textAlignment = .center
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        titles.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 32.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SlideinRowCell:UITableViewCell {
    weak var con : ProductInfoController?
    let container = UIView()
    let datelabel = UILabel()
    let expandbutton = UIButton(type: .custom)
    let selectbutton = UIButton(type: .custom)
    var selectbuttonanchor:AnchoredConstraints!
    var datelabelanchor : AnchoredConstraints!
    var extracontainer = UIView()
    var extracontaineranchor:AnchoredConstraints!
    let circleview = UIView()
    let extralabel = UILabel()
    let extravstack = UIStackView()
    let editimage = UIImageView(image: #imageLiteral(resourceName: "ic_more"))
    var data:ReConstructMaintain?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(container)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8527267575, green: 0.8549305797, blue: 0.8600837588, alpha: 1))
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.layer.cornerRadius = 15
        //container.clipsToBounds = true
        datelabel.text = "12月12日 下午3:25"
        datelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        let s_stack = UIStackView()
        let leadSpacer = UIView()
        leadSpacer.constrainWidth(constant: 26.calcvaluex())
        s_stack.addArrangedSubview(leadSpacer)
        s_stack.addArrangedSubview(selectbutton)
        selectbutton.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        selectbutton.setImage(#imageLiteral(resourceName: "btn_check_box_pressed"), for: .selected)
        selectbutton.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
        selectbutton.contentVerticalAlignment = .fill
        selectbutton.contentHorizontalAlignment = .fill
        selectbutton.constrainWidth(constant: 24.calcvaluex())
        selectbutton.constrainHeight(constant: 24.calcvaluey())
        selectbutton.isHidden = true
        
        s_stack.addArrangedSubview(datelabel)
        s_stack.addArrangedSubview(expandbutton)
        expandbutton.setImage(#imageLiteral(resourceName: "ic_arrow_down"), for: .normal)
//        expandbutton.contentHorizontalAlignment = .fill
//        expandbutton.contentVerticalAlignment = .fill
        expandbutton.constrainWidth(constant: 50.calcvaluex())
        expandbutton.constrainHeight(constant: 50.calcvaluex())
        

        s_stack.spacing = 12.calcvaluex()
        s_stack.axis = .horizontal
        s_stack.alignment = .fill
       // s_stack.constrainHeight(constant: 80.calcvaluey())
        extracontainer.backgroundColor = #colorLiteral(red: 0.9842038751, green: 0.9843754172, blue: 0.9842056632, alpha: 1)
        extracontainer.isHidden = true
        let v_stack = UIStackView()
        v_stack.axis = .vertical

        
        
        
        v_stack.addArrangedSubview(s_stack)
        v_stack.addArrangedSubview(extracontainer)
        v_stack.addArrangedSubview(UIView())
        if #available(iOS 11.0, *) {
            v_stack.setCustomSpacing(26.calcvaluey(), after: v_stack.arrangedSubviews.first ?? UIView())
        } else {
            // Fallback on earlier versions
        }
        extracontainer.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            extracontainer.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        container.addSubview(v_stack)
        v_stack.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        let extra_h_stack = UIStackView()
        extra_h_stack.spacing = 12.calcvaluex()
        extra_h_stack.alignment = .top
        extra_h_stack.axis = .horizontal
        let circleCon = UIView()
        circleCon.addSubview(circleview)
        circleview.anchor(top: circleCon.topAnchor, leading: circleCon.leadingAnchor, bottom: nil, trailing: circleCon.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 10.calcvaluex(), height: 10.calcvaluex()))
        circleCon.constrainWidth(constant: 10.calcvaluex())
        
        extra_h_stack.addArrangedSubview(circleCon)
        circleview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)

        circleview.layer.cornerRadius = 10.calcvaluex()/2
       // extralabel.backgroundColor = .red
        extralabel.numberOfLines = 0
        extralabel.sizeToFit()
        extralabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        extra_h_stack.addArrangedSubview(extralabel)
        
        extra_h_stack.addArrangedSubview(editimage)
        editimage.constrainWidth(constant: 24.calcvaluex())
        editimage.constrainHeight(constant: 24.calcvaluey())
        editimage.isHidden = true

        
        
        let vv_stack = UIStackView()
        vv_stack.spacing = 12.calcvaluey()
        vv_stack.axis = .vertical
        vv_stack.alignment = .fill
        
        vv_stack.addArrangedSubview(extra_h_stack)
        
        extracontainer.addSubview(vv_stack)
        vv_stack.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 26.calcvaluey(), right: 26.calcvaluex()))
        extravstack.isHidden = true
        extravstack.spacing = 8.calcvaluey()
        extravstack.axis = .vertical
        extravstack.distribution = .fillEqually
        extravstack.alignment = .fill

       vv_stack.addArrangedSubview(extravstack)
        
        editimage.isUserInteractionEnabled = true
       // editimage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(maintainOption)))
        
    }
    
//    func deleteMaintain(){
//        let alertCon = UIAlertController(title: "您確定要刪除這項報修嗎?", message: nil, preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "確定", style: .default) { (_) in
//            let hud = JGProgressHUD()
//            hud.show(in: General().getTopVc()?.view ?? UIView())
//            if let data = self.data?.data{
//                NetworkCall.shared.getDelete(parameter: "api-or/v1/appliances/\(data.appliance_code)/maintenances/\(data.id)", decoderType: UpdateClass.self) { (json) in
//                    hud.dismiss()
//                    if let json = json?.data,json {
//                        self.con?.fetchApi()
//                    }
//                }
//                
//            }
//        }
//        
//        let action2 = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//        
//        alertCon.addAction(action1)
//        alertCon.addAction(action2)
//        General().getTopVc()?.present(alertCon, animated: true, completion: nil)
//
//    }
//    func editMaintain(){
//        let vd = AddNewRecordController()
//       // vd.data = data
//       // vd.con = self.con
//        vd.main_data = data?.data
//        vd.modalPresentationStyle = .overFullScreen
//        vd.modalTransitionStyle = .crossDissolve
//        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        General().getTopVc()?.present(vd, animated: true, completion: nil)
//        //self.present(vd, animated: true, completion: nil)
//    }
//    @objc func maintainOption(){
//        let alert = UIAlertController(title: "保養設定".localized, message: "", preferredStyle: .actionSheet)
//        let action1 = UIAlertAction(title: "編輯保養".localized, style: .default) { (_) in
//            self.editMaintain()
//        }
//
//        let action3 = UIAlertAction(title: "刪除保養".localized, style: .default) { (_) in
//            self.deleteMaintain()
//        }
//        alert.addAction(action1)
//        //alert.addAction(action2)
//        alert.addAction(action3)
//        if let presenter = alert.popoverPresentationController {
//            presenter.sourceView = editimage
//            presenter.sourceRect = editimage.bounds
//            presenter.permittedArrowDirections = .any
//            }
//        General().getTopVc()?.present(alert, animated: true, completion: nil)
//    }
    @objc func didSelect(){
        selectbutton.isSelected = !selectbutton.isSelected
        self.data?.isSelected = selectbutton.isSelected
    }
//    @objc func openFile(gesture:UITapGestureRecognizer){
//        if let tag = gesture.view?.tag, let data = data?.data, let url = URL(string: data.files[tag].path_url ?? "") {
//            OpenFile().open(url: url)
//        }
//    }
    func setData(data:ReConstructMaintain) {
        self.data = data
        self.selectbutton.isSelected = data.isSelected ?? false
        extralabel.text = data.data?.order_code
        datelabel.text = Date().convertToDateComponent(text: data.data?.maintained_at ?? "",format: "yyyy-MM-dd HH:mm:ss",addEight: false)
        if data.isExpand == true{
            extracontainer.isHidden = false
        }
        else{
            extracontainer.isHidden = true
            return
        }
        
//        if let ss = data.data?.files {
//            if ss.count == 0{
//                extravstack.isHidden = true
//                return
//            }
//            if ss.count > 0{
//                extravstack.isHidden = false
//                extravstack.safelyRemoveArrangedSubviews()
//
//            let gt = ss.count / 4
//                print(661,gt)
//            for i in 0...gt {
//                let hstack = UIStackView()
//                hstack.axis = .horizontal
//                hstack.spacing = 8.calcvaluex()
//                hstack.distribution = .fillEqually
//                hstack.alignment = .fill
//                for tt in 0...2 {
//                    let count = (3 * i) + tt
//                    if count >= ss.count {
//                        hstack.addArrangedSubview(UIView())
//                        continue
//                    }
//
//                    let image = UIImageView()
//                    image.isUserInteractionEnabled = true
//                    image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFile)))
//                    image.tag = count
//                    image.layer.borderWidth = 1.calcvaluex()
//                    image.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//                    guard let url = URL(string: ss[count].path_url ?? "") else {return}
//                    image.image = url.getThumnailImage()
//                    
//                    image.contentMode = .scaleAspectFit
//
//
//                    image.clipsToBounds = true
//                   
//
//                    hstack.addArrangedSubview(image)
//                }
//                hstack.constrainHeight(constant: 90.calcvaluey())
//                extravstack.addArrangedSubview(hstack)
//                
//
//            }
//            }
//        }
//        else{
//            extravstack.isHidden = true
//        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FixRecordCell:UITableViewCell {
    let container = UIView()
    let statuslabel = PaddedLabel2()
    let machineheader = UILabel()
    let subheader = UILabel()
    let timelabel = UILabel()
    let selectionview = UIView()
    func setColor(){
       // selectionview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        imageview.isHidden = false
    }
    func unsetColor(){
        imageview.isHidden = true
       // selectionview.backgroundColor = .white
    }
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_arrow"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //contentView.backgroundColor = .red
        backgroundColor = .clear
        selectionStyle = .none
        selectionview.backgroundColor = .white
        
        contentView.addSubview(selectionview)
        selectionview.layer.cornerRadius = 15.calcvaluex()
        selectionview.addshadowColor(color: #colorLiteral(red: 0.8698186278, green: 0.8720664382, blue: 0.877323091, alpha: 1))
        selectionview.fillSuperview(padding: .init(top: 2.calcvaluex(), left: 2.calcvaluex(), bottom: 12.calcvaluey(), right: 2.calcvaluex()))
        selectionview.addSubview(container)
       
        container.backgroundColor = .white
        
        //container.addshadowColor(color: #colorLiteral(red: 0.8527267575, green: 0.8549305797, blue: 0.8600837588, alpha: 1))
        container.fillSuperview(padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        container.layer.cornerRadius = 15.calcvaluex()
//        if #available(iOS 11.0, *) {
//            container.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        statuslabel.layer.cornerRadius = 30.calcvaluey()/2
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1
        
        //statuslabel.text = "處理中"
        statuslabel.textAlignment = .center
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 46.calcvaluex() , bottom: 0, right: 46.calcvaluex()),size: .init(width: 0, height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        
        //machineheader.text = "機台運作問題"
        machineheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex() )
        machineheader.numberOfLines = 0
       // subheader.text = "久大行銷股份有限公司"
        subheader.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex() )
        subheader.numberOfLines = 0
        //timelabel.text = "10分鐘前"
        timelabel.font = UIFont(name: "Roboto-Light", size:  16.calcvaluex() )
        timelabel.numberOfLines = 0
        let stackview = UIStackView(arrangedSubviews: [machineheader,subheader,timelabel])
        stackview.axis = .vertical
        stackview.alignment = .leading
        
        stackview.spacing = 1
        
        container.addSubview(stackview)
        stackview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: statuslabel.leadingAnchor,padding: .init(top: 12.calcvaluey(), left:  46.calcvaluex() , bottom: 12.calcvaluey(), right: 26.calcvaluex()))
        //stackview.centerYInSuperview()
        
        selectionview.addSubview(imageview)
        imageview.anchor(top: nil, leading: nil, bottom: nil, trailing: selectionview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        imageview.centerYInSuperview()
        imageview.isHidden = true
        
        selectionview.heightAnchor.constraint(greaterThanOrEqualToConstant: 90.calcvaluey()).isActive = true
    }
    func setData(data:FixModel) {
        let status_array = GetStatus().getUpKeepStatus()
        if let status  = status_array.first(where: { (st) -> Bool in
            return st.key == data.status
        }) {
            statuslabel.text = status.getString()
            statuslabel.textColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff")
            statuslabel.layer.borderColor = UIColor().hexStringToUIColor(hex: status.color ?? "#fffff").cgColor
        }
        machineheader.text = data.subject
        subheader.text = data.account?.name
        timelabel.text = Date().convertToDateComponent(text: data.created_at,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class HistoryFormCell : UITableViewCell {
    let container = UIView()
    let machineheader = UILabel()
    let subheader = UILabel()
    let timelabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(container)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8527267575, green: 0.8549305797, blue: 0.8600837588, alpha: 1))
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.layer.cornerRadius = 15
        
        let stackview = UIStackView(arrangedSubviews: [machineheader,timelabel,subheader])
        stackview.axis = .vertical
        stackview.spacing = 4.calcvaluey()
        stackview.alignment = .leading
        
        container.addSubview(stackview)
        stackview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        stackview.centerYInSuperview()
        machineheader.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        timelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        subheader.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
