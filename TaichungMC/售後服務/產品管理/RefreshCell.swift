//
//  RefreshCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class RefreshCell: UICollectionViewCell {
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    override init(frame: CGRect) {
        super.init(frame: frame)
        activityIndicator.color = .black
        addSubview(activityIndicator)
        activityIndicator.centerInSuperview(size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        activityIndicator.startAnimating()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class RefreshTableCell: UITableViewCell {
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        activityIndicator.color = .black
        contentView.addSubview(activityIndicator)
        activityIndicator.centerInSuperview(size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        activityIndicator.startAnimating()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
