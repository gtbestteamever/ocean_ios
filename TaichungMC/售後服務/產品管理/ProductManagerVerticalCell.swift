//
//  ProductManagerVerticalCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductManagerVerticalCell: UICollectionViewCell {
    let shadowcontainer = UIView()
    let statuslabel = PaddedLabel2()
    let imageview = UIImageView()
    let sublabel = UILabel()
    let titlelabel = UILabel()
    let thirdLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        shadowcontainer.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        shadowcontainer.backgroundColor = .white
        addSubview(shadowcontainer)
        shadowcontainer.centerYInSuperview()
        shadowcontainer.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 2.calcvaluex()),size: .init(width: 0, height: 104.calcvaluey()))
        shadowcontainer.layer.cornerRadius = 15
        
        shadowcontainer.addSubview(statuslabel)
        //statuslabel.text = "保固中"
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderWidth = 1
        statuslabel.textAlignment = .center
        
        statuslabel.centerYInSuperview()
        statuslabel.anchor(top: nil, leading: shadowcontainer.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 30.calcvaluey()))
        statuslabel.layer.cornerRadius = 30.calcvaluey()/2
        imageview.image = nil
        shadowcontainer.addSubview(imageview)
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 48.43.calcvaluex(), bottom: 0, right: 0),size: .init(width: 78.23.calcvaluex(), height: 70.56.calcvaluey()))
        
       
        titlelabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        
        thirdLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        let stackview = UIStackView(arrangedSubviews: [titlelabel,sublabel,thirdLabel,UIView()])
        stackview.axis = .vertical
        stackview.spacing = 6.calcvaluey()
        stackview.alignment = .leading
        shadowcontainer.addSubview(stackview)
        stackview.centerYInSuperview()
        stackview.anchor(top: shadowcontainer.topAnchor, leading: imageview.trailingAnchor, bottom: shadowcontainer.bottomAnchor, trailing: nil,padding: .init(top: 12.calcvaluey(), left: 29.33.calcvaluex(), bottom: 12.calcvaluey(), right: 0))
    }
    func setData(data:UnitModel) {
        if data.in_warranty_period == 0 {
            statuslabel.text = "已過保".localized
            statuslabel.textColor = #colorLiteral(red: 0.7169483304, green: 0.7170530558, blue: 0.7169254422, alpha: 1)
            statuslabel.layer.borderColor = #colorLiteral(red: 0.7169483304, green: 0.7170530558, blue: 0.7169254422, alpha: 1)
        }
        else{
            statuslabel.text = "保固中".localized
            statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        
        titlelabel.text = data.reference?.titles?.getLang()
        sublabel.text = data.code
        thirdLabel.text = data.account?.name
        
        if let dt = data.reference {
           
            if let files = dt.files, let file = files.getLang()?.first?.path_url {

                switch file {
                case .arrayString(let ss) :
                    if let fm = ss?.first {
                        imageview.sd_setImage(with: URL(string: fm?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
                    }
                    else{
                        imageview.image = nil
                    }
                case .string(let st):
                    if let fm = st {
                        imageview.sd_setImage(with: URL(string: fm.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
                    }
                    else{
                        imageview.image = nil
                    }
                }
            }
            else{
                imageview.image = nil
            }
            
        }
        else{
            imageview.image = nil
            titlelabel.text = nil
        }
        
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
