//
//  OceanProductDetailController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/26/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class OceanPrDetailCell : UITableViewCell {
    let label = UILabel()
    let valueLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        contentView.addSubview(label)
        label.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 20.calcvaluey(), right: 0))
        label.text = "機台序號"
        label.font = UIFont(name: MainFont().Regular, size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.2772052288, green: 0.2772502899, blue: 0.2771953344, alpha: 1)
        let sep = UILabel()
        sep.text = "/"
        sep.textColor = MajorColor().oceanColor
        sep.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        contentView.addSubview(sep)
        sep.anchor(top: nil, leading: label.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        sep.centerYAnchor.constraint(equalTo: label.centerYAnchor,constant: 2.calcvaluey()).isActive = true
        valueLabel.text = "River-10578321"
        valueLabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        valueLabel.textColor = MajorColor().oceanlessColor
        
        contentView.addSubview(valueLabel)
        valueLabel.numberOfLines = 0
        valueLabel.anchor(top: label.topAnchor, leading: sep.trailingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 8.calcvaluex(), bottom: 20.calcvaluey(), right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension OceanProductDetailController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OceanPrDetailCell
        let data = infoarray[indexPath.row]
        cell.label.text = data.title
        cell.valueLabel.text = data.value
        return cell
    }
}
class OceanProductDetailController : SampleController {
    var data : UnitModel? {
        didSet{
            topLabel.text = data?.code ?? ""
            if let dt = data?.reference{
                bottomLabel.text = dt.titles?.getLang()
                imageview.sd_setImage(with: dt.getImage(), completed: nil)
            }
            
            infoarray[0].value = data?.code ?? ""
            if let mt = data?.installation_date {
                infoarray[1].value = Date().convertToDateShortComponent(text: mt)
            }
            infoarray[3].value = (data?.address ?? "")
            infoarray[4].value = data?.department?.title ?? ""
           
            self.tableview.reloadData()
        }
    }
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    
    var infoarray = [ProductInfo(title: "機台序號".localized, value: ""),ProductInfo(title: "購入日期".localized, value: ""),ProductInfo(title: "保固期限".localized, value: ""),ProductInfo(title: "安裝地點".localized, value: ""),ProductInfo(title: "服務部門".localized, value: ""),ProductInfo(title: "業務員".localized, value: ""),ProductInfo(title: "代理商".localized, value: "")]
    var addMaintain = AddButton4()
    var addRepair = AddButton4()
    let imageview = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleview.label.text = "產品管理".localized
        self.backbutton.isHidden = false
        self.settingButton.isHidden = true
        let container = UIView()

        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor()
        view.addSubview(container)
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: productMenuTabHeight + 24.calcvaluey(), right: 24.calcvaluex()))
        
        container.addSubview(topLabel)
        //topLabel.text = "River 3"
        topLabel.textColor = MajorColor().oceanlessColor
        topLabel.font = UIFont(name: MainFont().Medium, size: 20.calcvaluex())
        container.addSubview(bottomLabel)
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.centerXAnchor,padding: .init(top: 48.calcvaluey(), left: 60.calcvaluex(), bottom: 0, right: 8.calcvaluex()))
        
       // bottomLabel.text = "River 系列"
        bottomLabel.textColor = #colorLiteral(red: 0.7278061509, green: 0.727912426, blue: 0.7277830243, alpha: 1)
        bottomLabel.font = UIFont(name: MainFont().Regular, size: 16.calcvaluex())
        container.addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.backgroundColor = .clear
        
        container.addSubview(tableview)
        tableview.anchor(top: bottomLabel.bottomAnchor, leading: bottomLabel.leadingAnchor, bottom: container.bottomAnchor, trailing: bottomLabel.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.register(OceanPrDetailCell.self, forCellReuseIdentifier: "cell")
        
        let rightview = UIView()
        container.addSubview(rightview)
        rightview.anchor(top: topLabel.topAnchor, leading: container.centerXAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 24.calcvaluey(), right: 8.calcvaluex()))
        let button_stacks = Horizontal_Stackview(spacing:16.calcvaluex(),alignment: .fill)
        button_stacks.addArrangedSubview(addMaintain)
        addMaintain.titles.text = "新增保養".localized
        addMaintain.backgroundColor = MajorColor().oceanlessColor
        addMaintain.addTarget(self, action: #selector(addNewRecord), for: .touchUpInside)

        button_stacks.addArrangedSubview(addRepair)
        addRepair.addTarget(self, action: #selector(addFixRecord), for: .touchUpInside)
        addRepair.titles.text = "新增報修".localized
        addRepair.backgroundColor = MajorColor().oceanlessColor
        rightview.addSubview(button_stacks)
        button_stacks.anchor(top: rightview.topAnchor, leading: nil, bottom: nil, trailing: nil)
        button_stacks.centerXInSuperview()
        imageview.contentMode = .scaleAspectFit
        
        rightview.addSubview(imageview)
        imageview.anchor(top: button_stacks.bottomAnchor, leading: rightview.leadingAnchor, bottom: rightview.bottomAnchor, trailing: rightview.trailingAnchor,padding: .init(top: 32.calcvaluey(), left: 32.calcvaluex(), bottom: 32.calcvaluey(), right: 32.calcvaluex()))
        fetchPreloadData()
    }
    func fetchPreloadData(){
        if let id = data?.id {
            NetworkCall.shared.getCall(parameter: "api-or/v1/appliances/\(id)", decoderType: UnitPreciseData.self) { (json) in
                DispatchQueue.main.async {
                    
                    if let json = json?.data{
                        if let mt = json.installation_date {
                            print(9921,mt)
                            self.infoarray[1].value = Date().convertToDateShortComponent(text: mt)
                        }
                        if let mt = json.warranty_date {
                            self.infoarray[2].value = Date().convertToDateShortComponent(text: mt)
                        }
                        self.infoarray[5].value = json.sales?.name ?? ""
                        self.infoarray[4].value = json.department?.title ?? ""
                        self.infoarray[6].value = json.vendor?.name ?? ""
                        self.tableview.reloadData()
                    }
                }


            }
        }
    }
    @objc func addNewRecord(){
//        let vd = AddNewRecordController()
//        vd.data = data
//        //vd.con = self
//        vd.modalPresentationStyle = .overFullScreen
//        vd.modalTransitionStyle = .crossDissolve
//        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        self.present(vd, animated: true, completion: nil)
        
        let vd = ServiceFormController()
        let form = ServiceForm()
        form.selectOrInput = 0
        form.status = "processing"
        
        let time = FormTime()
        let date = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        time.startTime = dateformatter.string(from: date)
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        time.endTime = dateformatter.string(from: date)
        form.timeAndDuration = [time]
        form.upkeep_id = data?.account?.code ?? ""
        form.isMaintenance = true
        form.customer.name = data?.account?.name ?? ""
        form.customer.id = data?.account?.code ?? ""
        form.customer.address = data?.address ?? ""
        form.machine.code = data?.code ?? ""
        form.machine.type = data?.reference?.titles?.getLang() ?? ""
        form.machine.maintainStatus = data?.in_warranty_period == 1 ? "保固中".localized : "已過保".localized
        vd.container.form_data = form
        
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true)
    }
    @objc func addFixRecord(){
        let vd = AddNewServiceController()
        
        vd.selected_code = data?.code
        vd.auto_address = data?.address
        vd.addressField.textfield.text = data?.address
        vd.auto_account_code = data?.account?.code
        vd.auto_department_code = data?.department_code

        //vd.con = self
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}
