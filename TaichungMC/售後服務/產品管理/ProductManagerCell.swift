//
//  ProductManagerCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductManagerCell: UICollectionViewCell {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let shadowbackground = UIView()
    let statuslabel = UILabel()
    let sublabel = UILabel()
    let titlelabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        shadowbackground.backgroundColor = .white
        shadowbackground.layer.shadowColor = #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1)
        shadowbackground.layer.shadowOffset = .init(width: 0, height: 5)
        shadowbackground.layer.shadowRadius = 2
        shadowbackground.layer.shadowOpacity = 1
        shadowbackground.layer.cornerRadius = 12
        shadowbackground.layer.shouldRasterize = true
        shadowbackground.layer.rasterizationScale = UIScreen.main.scale
        addSubview(shadowbackground)
        shadowbackground.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 2, bottom: 2, right: 0),size: .init(width: 0, height: 294.calcvaluey()))
        
        addSubview(imageview)
        imageview.centerXInSuperview()
        imageview.centerYAnchor.constraint(equalTo: shadowbackground.topAnchor,constant: 24).isActive = true
        imageview.constrainWidth(constant: 145.calcvaluex())
        imageview.constrainHeight(constant: 132.calcvaluey())
        
        statuslabel.text = "保固中"
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderWidth = 1
        statuslabel.textAlignment = .center
        shadowbackground.addSubview(statuslabel)
        statuslabel.centerXInSuperview()
        statuslabel.anchor(top: nil, leading: nil, bottom: shadowbackground.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 38.calcvaluey(), right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        titlelabel.text = "立式加工中心機"
        titlelabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        sublabel.text = "Vcenter-85B/102B"
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        let stackview = UIStackView(arrangedSubviews: [titlelabel,sublabel])
        stackview.axis = .vertical
        stackview.alignment = .leading
        shadowbackground.addSubview(stackview)
        stackview.centerXInSuperview()
        stackview.anchor(top: nil, leading: nil, bottom: statuslabel.topAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 24.calcvaluey(), right: 0),size: .init(width: 0, height: 49.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
