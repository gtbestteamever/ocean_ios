//
//  FixPopInfoController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class FixPopInfoController : UIViewController {
    let container = SampleContainerView()
    let infoview = FixInfoView()
    var statusArray = [FixStatus]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.label.text = "報修內容"
        self.view.addSubview(container)
        if UIDevice().userInterfaceIdiom == .pad {
        container.fillSuperview(padding: .init(top: 30.calcvaluey(), left: 200.calcvaluex(), bottom: 30.calcvaluey(), right: 200.calcvaluex()))
        }
        else{
            container.fillSuperview(padding: .init(top: 45.calcvaluey(), left: 15.calcvaluex(), bottom: 45.calcvaluey(), right: 15.calcvaluex()))
        }
        container.xbutton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        container.clipsToBounds = true
        container.addSubview(infoview)
        infoview.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        infoview.pop_con = self
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
    func fetchStatus(){

            NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/setting", decoderType: FixStatusModel.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json?.data {
                        self.statusArray = json.statuses
                        self.callApiAndOpen()
                        //self.fetchApi2(scrollToTop:true)
                        
                    }
                }

            }
        
    }
    func callApiAndOpen(){
        let hud = JGProgressHUD()
        hud.show(in: self.view)

        NetworkCall.shared.getCall(parameter: "api-or/v1/upkeep/detail/\(infoview.commentheader.data?.id ?? "")", decoderType: FixInfoData.self) { (json) in
                DispatchQueue.main.async {
                    hud.dismiss()
                    if let json = json?.data {
                        self.infoview.commentheader.statuslabel.text = self.statusArray.first(where: { (st) -> Bool in
                            return st.key == json.status
                        })?.value
                        
                        self.infoview.commentheader.data = json

                    }
                }

            }

        
        
    }
}
