//
//  AddNewRecordController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/26.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
import JGProgressHUD
class AddFileButton : UIButton {
    let image = UIImageView()
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 112.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddFileButton2: UIButton {
    let image = UIImageView()
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 216.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddNewRecordController: UIViewController {
    var data : UnitModel?
    var isEdit = false
    var preloadHeight : CGFloat = 0
    var main_data : MaintenanceModel? {
        didSet{
            isEdit = true
            maintainTime = self.main_data?.maintained_at
            maintainInfo = self.main_data?.note
            self.timelabel.textfield.text = Date().convertToDateComponent(text: maintainTime ?? "", format: "yyyy-MM-dd HH:mm:ss", addEight: false)
            self.sublabel.textfield.text = maintainInfo
            self.sublabel.textfield.textViewDidChange(self.sublabel.textfield)
            for i in main_data?.files ?? [] {
                if let url = URL(string: i.path_url ?? "") {
                    files_arr.append(MaintainFile(name: "\(UUID().uuidString).\(url.pathExtension)", image: url.getThumnailImage(), extend: url.pathExtension, data: try? Data(contentsOf: url),path: i.path_url,file_path: i.file))
                }
            }
            if files_arr.count > 0{
                self.collectionview?.isHidden = false
                self.collectionview?.reloadData()
                let ss = files_arr.count / 4
                preloadHeight = CGFloat(ss + 1) * 80.calcvaluey() + CGFloat(ss) * 8.calcvaluey()
                

            }
            else{
                self.collectionview?.isHidden = true
            }
        }
    }
    let container = UIView()
    let titlelabel = UILabel()
    let timelabel = InterviewDateFormView(text: "保養時間".localized, placeholdertext: "請選擇時間".localized, formatter: "yyyy-MM-dd HH:mm:ss")
    let sublabel = NormalTextView(text: "備註說明".localized,placeholdertext: "請輸入備註".localized)
    let addfilebutton = AddFileButton()
    let donebutton = UIButton(type: .system)
    var containeranchor:AnchoredConstraints!
    var extravstack = UIStackView()
    var extravstackanhor:AnchoredConstraints!
    
    let xbutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    let scrollview = UIScrollView()
    var files_arr = [MaintainFile]()
    var collectionview : UICollectionView?
    var collectionviewHeightAnchor: NSLayoutConstraint?
    var maintainTime : String?
    var maintainInfo : String?
    var con : ProductDetailMaintainController?
    var con2 : iPhoneProductDetailMaintainController?
    var del_array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollview.bounces = false
        scrollview.contentInset.bottom = 30.calcvaluey()
        scrollview.showsVerticalScrollIndicator = false
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        
        container.layer.cornerRadius = 15
        
        view.addSubview(container)
        container.centerInSuperview()
        containeranchor = container.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 573.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 477.calcvaluey() + preloadHeight))

        let v_stack = UIStackView()
        v_stack.spacing = 26.calcvaluey()
        v_stack.alignment = .center
        v_stack.axis = .vertical
        timelabel.constrainWidth(constant: 486.calcvaluex())
        timelabel.constrainHeight(constant: 90.calcvaluey())
        v_stack.addArrangedSubview(timelabel)
        timelabel.delegate = self
        sublabel.textfield.t_delegate = self
        v_stack.addArrangedSubview(sublabel)
        sublabel.constrainWidth(constant: 486.calcvaluex())
        v_stack.addArrangedSubview(addfilebutton)
        addfilebutton.constrainWidth(constant: 486.calcvaluex())
        addfilebutton.constrainHeight(constant: 48.calcvaluey())


        donebutton.constrainWidth(constant: 124.calcvaluex())
        donebutton.constrainHeight(constant: 48.calcvaluey())
        
        //extravstack.constrainWidth(constant: 486.calcvaluex())
        scrollview.addSubview(v_stack)
        v_stack.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        v_stack.fillSuperview()
        if isEdit {
            titlelabel.text = "修改保養紀錄".localized
        }
        else{
            titlelabel.text = "新增保養紀錄".localized
        }
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        titlelabel.textAlignment = .center

        container.addSubview(titlelabel)
        titlelabel.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        titlelabel.centerXInSuperview()
        container.addSubview(scrollview)
        scrollview.anchor(top: titlelabel.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
//        timelabel.header.text = "保養時間"
//        timelabel.header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//        timelabel.header.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        
        timelabel.textfield.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())

        


        sublabel.textfield.mode = .Maintain

        
        
        addfilebutton.image.image = #imageLiteral(resourceName: "ic_btn_attach_file")
        addfilebutton.titles.text = "附加檔案(最多上傳六個檔案)".localized
        addfilebutton.titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        addfilebutton.titles.textColor = .white
        addfilebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        

        addfilebutton.layer.cornerRadius = 48.calcvaluey()/2
        addfilebutton.addTarget(self, action: #selector(addingfile), for: .touchUpInside)
        addfilebutton.centerXInSuperview()
        
        donebutton.backgroundColor = MajorColor().oceanSubColor
    
        donebutton.setTitle(isEdit == true ?  "更新".localized : "新增".localized, for: .normal)
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        

        donebutton.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))

        //donebutton.centerXInSuperview()
        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        
//        extravstack.isHidden = true
//        extravstack.spacing = 8.calcvaluey()
//        extravstack.axis = .vertical
//        extravstack.distribution = .fill
//        extravstack.alignment = .fill
        
//        scrollview.addSubview(extravstack)
//        extravstackanhor = extravstack.anchor(top: addfilebutton.bottomAnchor, leading: addfilebutton.leadingAnchor, bottom: nil, trailing: addfilebutton.trailingAnchor,padding: .init(top: 9.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height:  197.calcvaluey()))
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        collectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        collectionview?.delegate = self
        collectionview?.dataSource = self
        collectionview?.showsVerticalScrollIndicator = false
        collectionview?.isScrollEnabled = false
        if isEdit {
            collectionview?.isHidden = false
        }
        else{
        collectionview?.isHidden = true
        }
        collectionview?.register(maintainFileCell.self, forCellWithReuseIdentifier: "cell")
        v_stack.addArrangedSubview(collectionview!)
        collectionview?.constrainWidth(constant: 486.calcvaluex())
        collectionviewHeightAnchor = collectionview?.heightAnchor.constraint(equalToConstant: preloadHeight)
        collectionviewHeightAnchor?.isActive = true
        collectionview?.backgroundColor = .clear
        v_stack.addArrangedSubview(donebutton)
        extravstack.isHidden = true
        
        container.addSubview(xbutton)
        xbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        xbutton.isUserInteractionEnabled = true
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
        
       
        donebutton.addTarget(self, action: #selector(sendData), for: .touchUpInside)
    }
    func createAlert(message:String) {
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func sendData(){
        
        
        if maintainTime == nil{
            createAlert(message: "請輸入保養時間")
            return
        }
        
        if maintainInfo == nil || maintainInfo == ""{
            createAlert(message: "請輸入備註說明")
            return
        }
        
        
        //if let code = data?.code {
            var urlString = ""
            
            if isEdit {
                if let code = main_data?.appliance_code, let id = main_data?.id {
                    urlString = "api-or/v1/appliances/\(code)/maintenances/\(id)"
                }
                else{
                    return
                }
                
            }
            else{
                if let code = data?.code {
                    urlString = "api-or/v1/appliances/\(code)/maintenances/store"
                }
                else{
                    return
                }
            }
            var dt = [Data]()
            var name = [String]()
            var memeString = [String]()
            for i in files_arr {
                if (i.path?.contains("http://") ?? false) || (i.path?.contains("https://") ?? false) {
                    continue
                }
                if let dtt = i.data {
                    dt.append(dtt)
                }
                
                name.append(i.name)
                
                memeString.append(i.extend)
            
            }
        var param = [String:String]()
        param = ["maintained_at" : maintainTime ?? "", "note" : maintainInfo ?? ""]
        if del_array.count > 0{
            param["del_files"] = del_array.description
        }
        
            let hud = JGProgressHUD()
        if isEdit {
            hud.textLabel.text = "修改保養紀錄中...".localized
        }
        else{
            hud.textLabel.text = "建立保養紀錄中...".localized
        }
            
        hud.show(in: self.container)
            NetworkCall.shared.postCallMultipleData(parameter: urlString, param: param, data: dt, dataParam: "files[]", dataName: name, memeString: memeString, decoderType: ReturnMaintain.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json {
                        if self.isEdit {
                            hud.textLabel.text = "修改成功".localized
                        }
                        else{
                         hud.textLabel.text = "建立成功".localized
                        }
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        hud.dismiss(afterDelay: 1, animated: true) {
                            self.con?.fetchApi()
                            self.con2?.fetchApi()
                            self.dismiss(animated: false, completion: nil)
                        }
                    }
                    else{
                        if self.isEdit {
                            hud.textLabel.text = "修改失敗".localized
                        }
                        else{
                            hud.textLabel.text = "建立失敗".localized
                        }
                       
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        hud.dismiss(afterDelay: 1)
                    }
                }

            }
       // }

    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true, completion: nil)
    }
    @objc func addingfile(){
        if files_arr.count == 6 {
            let alertController = UIAlertController(title: "一次最多只能上傳6個檔案".localized, message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定".localized, style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
            
            return
        }
        let controller = UIAlertController(title: "請選擇上傳方式".localized, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿".localized, style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        
        let cloudAction = UIAlertAction(title: "文件/雲端檔案".localized, style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = addfilebutton
                presenter.sourceRect = addfilebutton.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
//        containeranchor.height?.constant = 683.calcvaluey()
//
//        extravstack.isHidden = false
//        UIView.animate(withDuration: 0.2) {
//            self.view.layoutIfNeeded()
//        }
    }
}
extension AddNewRecordController : EditingInterviewDelegate,FormTextViewDelegate {
    func chooseAddressType(textview: UITextView) {
        ///
    }
    
    func setCustomerCode(text: String) {
        //
    }
    func setDeliveryDate(text: String) {
        //
    }
    func setOrderDate(text: String) {
        maintainTime = text

        timelabel.textfield.text = Date().convertToDateComponent(text: text,format: "yyyy-MM-dd HH:mm:ss",addEight: false)
    }
    
    func setInfo(text: String) {
        //
    }
    
    func goSelectFile(sender: UIButton) {
        //
    }
    
    func removeFile(index: Int) {
        //
    }
    
    func chooseCustomers(textField: UITextField) {
        //
    }
    
    func setVisitDate(text: String) {
        //
    }
    
    func chooseNotifyDate(textField: UITextField) {
        //
    }
    
    func sendRating(text: String) {
        //
    }
    
    func sendLocation(text: String) {
        //
    }
    
    func sendPeriodArray(array: [String]) {
        //
    }
    
    func showAttachFile(file: interViewFile) {
        //
    }
    
    func textViewReloadHeight(height: CGFloat) {
        //
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        maintainInfo = text
    }
    
    
}
extension AddNewRecordController : UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return files_arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! maintainFileCell
        cell.imageView.image = files_arr[indexPath.item].image
        cell.xbutton.tag = indexPath.item
        cell.xbutton.addTarget(self, action: #selector(deleteItem), for: .touchUpInside)
        return cell
    }
    @objc func deleteItem(sender:UIButton) {
        let ft = files_arr.remove(at: sender.tag)
        
        if let ss = ft.file_path {
            del_array.append(ss)
        }
        
        self.reloadCollectionView()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 2 * 8.calcvaluex())/3, height: 80.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let files = files_arr[indexPath.item]
        if let data = files.path, let url = URL(string: data){
            OpenFile().open(url: url)
        }
    }
    func reloadCollectionView(){
        if files_arr.count == 0{
            self.collectionview?.isHidden = true
            self.collectionview?.reloadData()
            self.collectionviewHeightAnchor?.constant = 0
            self.containeranchor.height?.constant = 477.calcvaluey()
            
        }
        else{
        self.collectionview?.isHidden = false
        self.collectionview?.reloadData()
        let height = (self.collectionview?.collectionViewLayout as? UICollectionViewFlowLayout)?.collectionViewContentSize.height ?? 0
        self.collectionviewHeightAnchor?.constant = height
        self.containeranchor.height?.constant = 477.calcvaluey() + height
        }
       

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1){
            var imageURL : URL?
            if #available(iOS 11.0, *) {
                imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
            } else {
                // Fallback on earlier versions
            }
            let ss = MaintainFile(name: "\(UUID().uuidString).jpg", image: img, extend: "jpg", data: dt,path: imageURL?.path)
            files_arr.append(ss)
            //createExtraStack()
            
            self.dismiss(animated: true, completion: {
                self.reloadCollectionView()
            })
        }
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var image : UIImage = UIImage()
        if url.pathExtension.lowercased() == "pdf" {
            image = Thumbnail().pdfThumbnail(url: url) ?? UIImage()
        }
        else if url.pathExtension.lowercased() == "mp4"{
            image = Thumbnail().getThumbnailImage(forUrl: url) ?? UIImage()
        }
        else if url.pathExtension.lowercased() == "csv" || url.pathExtension.lowercased() == "xlsx" || url.pathExtension.lowercased() == "xls" {
            image = #imageLiteral(resourceName: "ic_preset_excel")
        }
        else if url.pathExtension.lowercased() == "doc" || url.pathExtension.lowercased() == "docx" {
            image = #imageLiteral(resourceName: "ic_preset_word")
        }
        else if url.pathExtension.lowercased() == "ppt" || url.pathExtension.lowercased() == "pptx"{
            image = #imageLiteral(resourceName: "ic_preset_ppt")
        }
        let data = try? Data(contentsOf: url)
        let ss = MaintainFile(name: "\(UUID().uuidString).\(url.pathExtension)",image: image, extend: url.pathExtension, data: data,path: url.path)
        
        files_arr.append(ss)
       reloadCollectionView()
       // createExtraStack()
        
    }
}

class maintainFileCell : UICollectionViewCell {
    let imageView = UIImageView()
    let xbutton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.layer.borderWidth = 1.calcvaluex()
        imageView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        addSubview(imageView)
        imageView.fillSuperview()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        
        addSubview(xbutton)
        xbutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 4.calcvaluex()),size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
