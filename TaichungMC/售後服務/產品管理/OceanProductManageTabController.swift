//
//  OceanProductManageTabController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/26/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
var productMenuTabHeight = 80.calcvaluey()
class ButtonView : UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_tab_spec_normal").withRenderingMode(.alwaysTemplate))
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.tintColor = .white
        
        addSubview(imageview)
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: imageview.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        label.font = UIFont(name: MainFont().Regular, size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.6399407983, green: 0.6400350928, blue: 0.6399201155, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NewButton : UIButton {
    let vd = ButtonView()
    init(image:UIImage,text:String){
        super.init(frame: .zero)
        vd.imageview.image = image
        vd.label.text = text
        addSubview(vd)
        vd.centerInSuperview()
        
        vd.isUserInteractionEnabled = false
        let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8765595555, green: 0.8766859174, blue: 0.876531899, alpha: 1)
        addSubview(sep)
        sep.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 16.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
    }
    
    func setColor() {
        backgroundColor = MajorColor().oceanColor
        vd.imageview.image = vd.imageview.image?.withRenderingMode(.alwaysTemplate)
        vd.label.textColor = .white
    }
    func setUnColor(){
        backgroundColor = .white
        vd.imageview.image = vd.imageview.image?.withRenderingMode(.alwaysOriginal)
        vd.label.textColor = #colorLiteral(red: 0.6399407983, green: 0.6400350928, blue: 0.6399201155, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol NewTabBarDelegate{
    func didSelect(index:Int)
}
class NewTabBar : UIView {
    let stackview = Horizontal_Stackview(distribution:.fillEqually)
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        addshadowColor()
        
        addSubview(stackview)
        stackview.fillSuperview()
        
        for (index,i) in [(#imageLiteral(resourceName: "ic_tab_features_normal"),"詳細資訊".localized),(#imageLiteral(resourceName: "ic_tab_spec_normal"),"保養紀錄".localized),(#imageLiteral(resourceName: "ic_tab_optional_normal"),"報修紀錄".localized)].enumerated() {
            let bt = NewButton(image: i.0, text: i.1)
            bt.tag = index
            bt.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
            if index == 0{
                bt.setColor()
            }
            else{
                bt.setUnColor()
            }
            
            
            stackview.addArrangedSubview(bt)
        }
    }
    var delegate : NewTabBarDelegate?
    @objc func didSelect(sender:UIButton){
        let selectedindex = sender.tag
        
        for (index, i) in stackview.arrangedSubviews.enumerated() {
            if let ss = stackview.arrangedSubviews[index] as? NewButton {
                if selectedindex == index{
                    ss.setColor()
                }
                else{
                    ss.setUnColor()
                }
            }

        }
        
        delegate?.didSelect(index: selectedindex)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class OceanProductManageTabController: UITabBarController,NewTabBarDelegate {
    func didSelect(index: Int) {
        if selectedIndex != index{
            selectedIndex = index
        }
    }
    let menubar = NewTabBar()
    var data : UnitModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.isHidden = true
        
        view.addSubview(menubar)
        menubar.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: productMenuTabHeight))
        menubar.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIDevice().userInterfaceIdiom == .pad {
        let pr = OceanProductDetailController()
        pr.data = self.data
        let nr = ProductDetailMaintainController()
        nr.data = self.data
        let tr = OceanProductRepairController()
        tr.data = self.data
        viewControllers = [pr,nr,tr]
        }
        else{
            let pr = iPhoneOceanProductDetailController()
            pr.data = self.data
            let nr = iPhoneProductDetailMaintainController()
            nr.data = self.data
            let tr = iPhoneOceanProductRepairController()
            tr.data = self.data
            viewControllers = [pr,nr,tr]
        }
    }
}
