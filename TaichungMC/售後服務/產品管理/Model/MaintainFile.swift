//
//  MaintainFile.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/3/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct MaintainFile {
    var name : String
    var image : UIImage?
    var extend : String
    var data : Data?
    var path : String?
    var file_path:String?
}
