//
//  FixMoedl.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/4/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct FixInfoData: Codable {
    var data : FixModel?
}
struct FixData : Codable{
    var data : [FixModel]
    var meta : f_Meta
}

struct FixModel : Codable{
    var id : String
    var contact_name : String?
    var contact_tel : String?
    var status : String
    var address : String?
    var subject : String?
    var description : String?
    var updated_at : String
    var created_at : String
    var appliance : FixAppliance?
    var upkeep_order : ServiceModel?
    var appliance_name : String?
    var files : [MaintenanceFile]?
    var account : Customer?
    var messages : [FixMessage]?
}
struct FixMessage : Codable {
    var id : String
    var updated_at : String
    var description : String?
    var owner : MessageOwner?
    var files: [MaintenanceFile]
}
struct MessageOwner : Codable {
    var id : String
    var text : String
    var is_portal : Int
}

struct FixAppliance : Codable{
    var id : String
    var company_code : String?
    var code : String?
    var account_code : String?
    var department_code : String?
    var address : String?
    var reference : ApplianceReference?
    var in_warranty_period : Int?
    
}
struct ApplianceReference : Codable {
    var title : String?
}
struct ReturnFixModel : Codable {
    var data : FixModel?
}
