//
//  FixModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct UnitData : Codable {
    var data : [UnitModel]
    var meta : f_Meta
}
struct f_Meta : Codable {
    var current_page : Int
    
    
    var last_page : Int
}
struct UnitPreciseData : Codable{
    var data : UnitModel
}
struct UnitModel : Codable{
    var id : String
    
    var code : String?
    
    var reference : UnitProduct?
    
    var company_code : String?
    
    var department_code : String?
    var department : Unit_Department?
    var address : String?
    
    var installation_date : String?
    
    var warranty_date : String?
    var in_warranty_period : Int
    var is_verified : Int?
    var is_portal : Int?
    var vendor : Unit_Vendor?
    var sales : Sales?
    var account : Customer?
}
struct Unit_Vendor : Codable{
    var name : String?
}
struct Unit_Department : Codable {
    var title: String?
}
struct UnitProduct : Codable {
    
    var id : String?
    
    var code_prefix : String?
    
    var title : String?
    
    var titles : Lang?
    
    var files : Files?
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(String.self, forKey: .id)
        self.code_prefix = try? container.decodeIfPresent(String.self, forKey: .id)
        self.title = try? container.decodeIfPresent(String.self, forKey: .title)
        self.titles = try? container.decodeIfPresent(Lang.self, forKey: .titles)
        self.files = try? container.decodeIfPresent(Files.self, forKey: .files)
    }
    func getImage() -> URL?{
        if let files = files, let d_array = files.getLang()?.first?.path_url {
            switch d_array {
            case .arrayString(let ss) :
                if let fm = ss?.first {
                    return URL(string:fm?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "")
                }

            case .string(let st):
                if let fm = st {
                    return URL(string:fm.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "")
                }

            }
        }
        return nil
    }
}

struct HistoryFormData : Codable{
    var data : [HistoryData]
}
struct HistoryData : Codable {
    var code : String?
    var completed_at : String?
    var note : String?
}
