//
//  MaintenanceModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
struct MaintenanceData  : Codable {
    var data : [MaintenanceModel]?
    var meta : f_Meta?
    var maintain_orders : [Maintain]?
}
class Maintain : Codable {
    var id : String?
    var order_code : String?
    var maintained_at : String?
    var isExpand : Bool? = false
    
    var isSelected : Bool? = false
    init(){
        isExpand = false
        isSelected = false
    }
}

class ReConstructMaintain {
    var data : Maintain?
    var isSelected : Bool?
    var isExpand : Bool?
    
    init(data : Maintain?,isSelected:Bool?,isExpand:Bool?) {
        self.data = data
        self.isSelected = isSelected
        self.isExpand = isExpand
    }
}
class MaintenanceModel : Codable{
    var id : String
    var appliance_code : String
    
    var maintained_at : String
    
    var note : String?
    
    var files : [MaintenanceFile]
    
    var isExpand : Bool? = false
    
    var isSelected : Bool? = false
    
    init(){
        id = ""
        appliance_code = ""
        maintained_at = ""
        note = ""
        files = []
        isExpand = false
        isSelected = false
    }
}


struct MaintenanceFile : Codable {
    var path_url : String?
    var file : String?
}

struct ReturnMaintain : Codable {
    var data : MaintenanceModel?
}
