//
//  File.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 7/8/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class FilterServiceController : UIViewController {
    
    let con = FilterContainerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(con)
        
        con.centerInSuperview()
        con.constrainWidth(constant: UIDevice().userInterfaceIdiom == .pad ? 573.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex())
        
        con.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
    }
    @objc func popView(){
        self.dismiss(animated: false, completion: nil)
    }
}

class FilterContainerView : SampleContainerView,UITextFieldDelegate,WarrantyViewDelegate {
    func selectedStatus(string: String) {
        statusField.textfield.text = string
        if let first = status_array.first(where: { wt in
            return wt.name == string
        }) {
            if let index = data.firstIndex(where: { ft in
                return ft.mode == .Warranty
            }) {
                data[index].value = first
            }
            else {
                data.append(maintainFilterItem(mode: .Warranty, value: first))
            }
            
        }
        removeWarrantyView()
    }
    
    var con : ProductManagementController?
    var con2: iPhoneProductManagementController?
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == companyCodeField.textfield {
        let vd = CustomerSelectionView()
        vd.con3 = self
        vd.getCustomersList()
        vd.modalPresentationStyle = .overCurrentContext
        General().getTopVc()?.present(vd, animated: false, completion: nil)
            
        }
            
        if textField == statusField.textfield {
                addWarrantyView()
        }

        return false
    }
    func addWarrantyView(){
        if warranty_view == nil{
        warranty_view = WarrantyView()
            warranty_view?.delegate = self
        addSubview(warranty_view!)
        warranty_view?.anchor(top: statusField.bottomAnchor, leading: statusField.leadingAnchor, bottom: nil, trailing: statusField.trailingAnchor)
        }
    }
    func removeWarrantyView(){
        warranty_view?.removeFromSuperview()
        warranty_view = nil
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {

        textField.text = nil
        var index : Int = 0
        if textField.tag == 1{
            if data.count == 1{
                index = 0
            }
            else{
                index = 1
            }
        }
        print(data.count,index)
        data.remove(at: index)
        return false
        
    }
    var warranty_view : WarrantyView?
    let stackview = UIStackView()
    let companyCodeField = InterviewNormalFormView(text: "客戶".localized, placeholdertext: "請選擇客戶".localized)
    let statusField = InterviewNormalFormView(text: "保固狀態".localized, placeholdertext: "請選擇保固狀態".localized)
    let donebutton = UIButton(type: .custom)
    var data = [maintainFilterItem]() {
        didSet{
           
            if let first = data.first(where: { ffs in
                return ffs.mode == .CompanyCode
            }), let vv = first.value as? Customer {
                companyCodeField.textfield.text = vv.name
            }
            if let first = data.first(where: { ffs in
                return ffs.mode == .Warranty
            }), let vv = first.value as? WarrantyModel {
                statusField.textfield.text = vv.name
            }
        }
    }
    var status_array = [WarrantyModel(name: "保固中".localized, status: 1),WarrantyModel(name: "過保".localized, status: 0)]
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.label.text = "篩選".localized
        //addSubview(stackview)
        stackview.axis = .vertical
        stackview.spacing = 24.calcvaluey()
        stackview.alignment = .center
        
        
        stackview.addArrangedSubview(companyCodeField)
        stackview.addArrangedSubview(statusField)
        companyCodeField.constrainWidth(constant: 486.calcvaluex())
        //companyCodeField.constrainHeight(constant: 92.calcvaluey())
        statusField.textfield.delegate = self
        statusField.constrainWidth(constant: 486.calcvaluex())
        //statusField.constrainHeight(constant: 92.calcvaluey())
        
        stackview.addArrangedSubview(donebutton)
        donebutton.constrainWidth(constant: 124.calcvaluex())
        donebutton.constrainHeight(constant: 48.calcvaluey())
        donebutton.backgroundColor = MajorColor().oceanSubColor
    
        donebutton.setTitle("篩選".localized, for: .normal)
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        donebutton.addTarget(self, action: #selector(filtering), for: .touchUpInside)

        donebutton.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))

        
        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        
        let scrollview = UIScrollView()
        
        scrollview.addSubview(stackview)
        stackview.fillSuperview()
        stackview.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        stackview.heightAnchor.constraint(equalTo: scrollview.heightAnchor).isActive = true
        addSubview(scrollview)
        scrollview.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 12.calcvaluex(), bottom: 48.calcvaluey(), right: 12.calcvaluex()))
        
        companyCodeField.textfield.delegate = self
        companyCodeField.textfield.clearButtonMode = .unlessEditing
        
        statusField.textfield.clearButtonMode = .unlessEditing
        statusField.textfield.tag = 1
        
    }
    
    @objc func filtering(){
        self.con?.filterview.data = self.data
        self.con2?.filterview.data = self.data
        General().getTopVc()?.dismiss(animated: false, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct WarrantyModel {
    var name : String
    var status : Int
}
protocol WarrantyViewDelegate {
    func selectedStatus(string:String)
}
class WarrantyView : UIView {
    let v_stack = Vertical_Stackview()
    var delegate : WarrantyViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(lessThanOrEqualToConstant: 220.calcvaluey()).isActive = true
       // constrainHeight(constant: 220.calcvaluey())
        backgroundColor = .white
        addshadowColor()
        
        addSubview(v_stack)
        v_stack.fillSuperview()
        
        for (index,i) in ["保固中".localized,"過保".localized].enumerated(){
            let ss = FilterLabel()
            if index == 1 {
                ss.seperator.isHidden = true
            }
            else{
                ss.seperator.isHidden = false
            }
            ss.label.text = i
            ss.constrainHeight(constant: 50.calcvaluey())
            ss.isUserInteractionEnabled = true
            ss.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelect)))
            v_stack.addArrangedSubview(ss)
            
        }
        
    }
    @objc func didSelect(gesture:UITapGestureRecognizer) {
        if let vt = gesture.view as? FilterLabel, let st = vt.label.text {
            delegate?.selectedStatus(string: st)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
