//
//  FrequentFileController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 12/16/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class FrequentFileController : SampleController {
    let webview = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = true
        backbutton.isHidden = false
        
        view.addSubview(webview)
        webview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 12.calcvaluex(), bottom: 12.calcvaluey(), right: 12.calcvaluex()))
    }
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
}
