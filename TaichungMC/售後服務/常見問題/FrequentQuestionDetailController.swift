//
//  FrequentQuestionDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import WebKit
class CustomTitle:UIView {
    let lspacer = UIView()
    let header = UILabel()
    let subtitle = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        lspacer.isHidden = true
        lspacer.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addSubview(lspacer)
        lspacer.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 6.calcvaluex(), height: 56.calcvaluey()))
        lspacer.layer.cornerRadius = 6.calcvaluex()/2
        addSubview(header)
        header.anchor(top: topAnchor, leading: lspacer.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0))
       // header.text = "立式五軸加工中心機"
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        header.font = UIFont(name: "Roboto-Bold", size: 26.calcvaluex())
        
        addSubview(subtitle)
        subtitle.font = UIFont(name: "Roboto-Light", size: 18.calcvaluey())
        //subtitle.text = "VCENTER-AX380"
        subtitle.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        subtitle.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentLeftViewCell:UICollectionViewCell {
    let title = CustomTitle()
    //let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let hotspotview = hotSpotView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(title)
        title.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 83.calcvaluex()))
        
        addSubview(hotspotview)
        hotspotview.isFrequent = true
        hotspotview.anchor(top: title.bottomAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 353.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentLeftView:UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HotSpotDelegate{
    var delegate : FrequentQuestionDelegate?
    func sendHotspot(hotspot: Hotspot) {
        delegate?.getHotspot(hotspot: hotspot)
    }
    
    func sendFile(file: Files?) {
        //
    }
    
    func sendFile(remote: String) {
        //
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mapCollectionViewCell
        if indexPath.item == selectedIndex {
            cell.backgroundColor = #colorLiteral(red: 0.7637601495, green: 0.7638711333, blue: 0.7637358308, alpha: 1)
        }
        else{
            cell.backgroundColor = #colorLiteral(red: 0.9018597007, green: 0.902017653, blue: 0.9018613696, alpha: 1)
        }
        print(77,data[indexPath.item].image_url)
        cell.image.sd_setImage(with: URL(string: (data[indexPath.item].image_url ?? "").addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        self.collectionview.reloadData()
        let dt = data[indexPath.item]
        hotspotview.img.sd_setImage(with: URL(string: dt.image_url?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "")) { _, _, _, _ in
            self.hotspotview.hotspots = dt.hotspots ?? []
        }
        //title.header.text = dt.titles?.getLang()
        if let hotspot = dt.hotspots?.first {
            delegate?.getHotspot(hotspot: hotspot)
            
        }
        else{
            delegate?.getHotspot(hotspot: Hotspot(coordinate: nil, remote: nil, remotes: nil, files: nil, description: nil, descriptions: nil, articles: nil))
        }
        
    }
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    let leftarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_before"))
    let rightarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    let title = CustomTitle()
    let hotspotview = hotSpotView()
    var current = 0
    let itemcount = 5
    var selectedIndex = 0
    var data = [FrequentArray]() {
        didSet{
            if data.count == 0{
                hotspotview.isHidden = true
                collectionview.isHidden = true
                if UIDevice().userInterfaceIdiom == .pad{
                delegate?.getHotspot(hotspot: Hotspot(coordinate: nil, remote: nil, remotes: nil, files: nil, description: nil, descriptions: nil, articles: nil))
                }
                return
            }
            hotspotview.isHidden = false
            collectionview.isHidden = false
            selectedIndex = 0
            collectionview.reloadData()
            //title.header.text = data.first?.titles?.getLang()

            hotspotview.img.sd_setImage(with: URL(string: data.first?.image_url?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "")) { _, _, _, _ in
                if UIDevice().userInterfaceIdiom == .phone {
                self.hotspotview.selectedIndex = -1
                }
                self.hotspotview.hotspots = self.data.first?.hotspots ?? []
            }
            if UIDevice().userInterfaceIdiom == .pad{
            if let hotspot = data.first?.hotspots?.first {
                delegate?.getHotspot(hotspot: hotspot)
                
            }
            else{
                delegate?.getHotspot(hotspot: Hotspot(coordinate: nil, remote: nil, remotes: nil, files: nil, description: nil, descriptions: nil, articles: nil))
            }
            }
            
        }
    }
    override init(frame: CGRect) {
        super.init(frame:frame)
        backgroundColor = .clear
        
        addSubview(title)
        title.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 0, height: 83.calcvaluex()))
        addSubview(hotspotview)
        hotspotview.delegate = self
        hotspotview.isFrequent = true
        hotspotview.anchor(top: title.bottomAnchor, leading: title.leadingAnchor, bottom: nil, trailing: title.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 268.calcvaluey()))
//        addSubview(titleview)
//
//        titleview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 50.calcvaluey(), left: 48.calcvaluex(), bottom: 0, right: 48.calcvaluex()),size: .init(width: 0, height: 83.calcvaluex()))
//
//        addSubview(hotspotview)
//        hotspotview.backgroundColor = .red
//        hotspotview.anchor(top: titleview.bottomAnchor, leading: titleview.leadingAnchor, bottom: bottomAnchor, trailing: titleview.trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 0, bottom: 45.calcvaluey(), right: 0))
        addSubview(collectionview)
        collectionview.backgroundColor = .clear
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        addSubview(collectionview)
        collectionview.anchor(top: hotspotview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 40.calcvaluey(), left: 24.calcvaluex(), bottom: 40.calcvaluey(), right: 24.calcvaluex()))
        collectionview.delegate = self
        collectionview.dataSource = self
       // collectionview.isPagingEnabled = true
        collectionview.register(mapCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
//        addSubview(leftarrow)
//        leftarrow.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 255.calcvaluey(), right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
//        leftarrow.isUserInteractionEnabled = true
//        leftarrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scrolltoprevious)))
//        leftarrow.isHidden = true
//        addSubview(rightarrow)
//        rightarrow.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 255.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
//        rightarrow.isUserInteractionEnabled = true
//        rightarrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scrolltonext)))
//        rightarrow.isHidden = true
    }
    @objc func scrolltoprevious(){
        if current >= 0 {
        current -= 1
        
        collectionview.scrollToItem(at: IndexPath(item: current, section: 0), at: .centeredHorizontally, animated: true)
            rightarrow.isHidden = false
            if current == 0{
                leftarrow.isHidden = true
            }
        }
    }
    @objc func scrolltonext(){
        
        if current < itemcount {
        current += 1
        
        collectionview.scrollToItem(at: IndexPath(item: current, section: 0), at: .centeredHorizontally, animated: true)
            leftarrow.isHidden = false
            if current == itemcount - 1{
                rightarrow.isHidden = true
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        current = indexPath.item
        
        if current == 0 {
            leftarrow.isHidden = true
            rightarrow.isHidden = false
        }
        else if current == itemcount - 1{
            rightarrow.isHidden = true
            leftarrow.isHidden = false
        }
        else{
            rightarrow.isHidden = false
            leftarrow.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 220.calcvaluex(), height: collectionView.frame.height)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol FrequentQuestionDelegate {
    func getHotspot(hotspot:Hotspot)
}
class FrequentQuestionDetailController: SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FrequentQuestionDelegate {

    func getHotspot(hotspot: Hotspot) {
        print(331,hotspot)
        rightview.data = hotspot
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ct", for: indexPath) as! SelectionViewCell
        cell.label.text = ["客戶常見問題".localized,"內部技術文件".localized][indexPath.item]
        if indexPath.item == selectedIndex{
            cell.vd.isHidden = false
            //cell.label.text = "客戶常見文題"
            cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
            //cell.label.text = "內部技術文件"
            cell.vd.isHidden = true
            cell.label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 120.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 55.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex != indexPath.item {
        selectedIndex = indexPath.item
        
        
        //self.selectionview.reloadData()
            
            if selectedIndex == 0{
                self.leftview.data = frequent_data
            }
            else{
                self.leftview.data = inner_data
            }
        }
    }
    let whitespace = UIView()
    let selectionview = NewTabBar()
    let leftview = FrequentLeftView()
    let rightview = FrequentRightView()
    var id : String = ""
    var frequent_data = [FrequentArray]()
    var inner_data = [FrequentArray]()
    var delegate : FrequentQuestionDelegate?
    var selectedIndex = 0
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func didSelect(sender:NewButton) {
        if selectIndex == sender.tag {
            return
        }
        selectIndex = sender.tag
        if selectIndex == 0{
            self.leftview.data = frequent_data
        }
        else{
            self.leftview.data = inner_data
        }
        for (index,i) in selectionview.stackview.arrangedSubviews.enumerated() {
            let vd = selectionview.stackview.arrangedSubviews[index] as? NewButton
            
            if index == selectIndex {
                vd?.setColor()
            }
            else{
                vd?.setUnColor()
            }
        }
    }
    var selectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "常見問題".localized
        settingButton.isHidden = true
        backbutton.isHidden = false
        view.backgroundColor = .white
        

        if GetUser().isPortal() == 1 {
            selectionview.isHidden = true
        }
        selectionview.stackview.safelyRemoveArrangedSubviews()
        for (index,i) in [(#imageLiteral(resourceName: "ic_quotation_normal"),"客戶常見問題".localized),(#imageLiteral(resourceName: "ic_tab_features_normal"),"內部技術文件".localized)].enumerated() {
            let bt = NewButton(image: i.0, text: i.1)
            bt.tag = index
            bt.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
            if index == selectIndex{
                bt.setColor()
            }
            else{
                bt.setUnColor()
            }
            
            
            selectionview.stackview.addArrangedSubview(bt)
        }
        view.addSubview(selectionview)
//        selectionview.delegate = self
//        selectionview.dataSource = self
        selectionview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: productMenuTabHeight))
//        selectionview.backgroundColor = .clear
//        selectionview.register(SelectionViewCell.self, forCellWithReuseIdentifier: "ct")
        
        view.addSubview(whitespace)
        whitespace.backgroundColor = .white
        whitespace.layer.cornerRadius = 15.calcvaluex()
        whitespace.addshadowColor()
        whitespace.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: selectionview.topAnchor, trailing: view.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 25.calcvaluex(), bottom: 33.calcvaluey(), right: 25.calcvaluex()))
        whitespace.addSubview(leftview)
        leftview.delegate = self
        leftview.anchor(top: whitespace.topAnchor, leading: whitespace.leadingAnchor, bottom: whitespace.bottomAnchor, trailing: nil,size: .init(width: 542.calcvaluex(), height: 0))
        
        whitespace.addSubview(rightview)
        
        rightview.anchor(top: whitespace.topAnchor, leading: leftview.trailingAnchor, bottom: whitespace.bottomAnchor, trailing: whitespace.trailingAnchor)
        
//        view.bringSubviewToFront(whitespace)
//        view.bringSubviewToFront(selectionview)
        
        fetchApi()
    }
    
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/faq_independent/\(id)", decoderType: FrequentData.self) { json in
            DispatchQueue.main.async {
                if let json = json {
                    for i in json.data {
                        if let con = i.tags?.contains("faq"), let con2 = i.tags?.contains("internal-doc") {
                            if con {
                                self.frequent_data.append(i)
                            }
                            else if con2 {
                                self.inner_data.append(i)
                            }
                        }
                    }
                    
                    self.leftview.data = self.frequent_data
                }
            }

        }
    }

}

class FrequentRightView:UIView,UITableViewDelegate,UITableViewDataSource {
    var cellHeights = [IndexPath:CGFloat]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.articles?.count ?? 0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if expanding[indexPath.row] {
//            let cell = tableView.cellForRow(at: indexPath) as? FrequentRightViewCell
//            var descriptions = [String]()
//            if UserDefaults.standard.getConvertedLanguage() == "zh-TW" {
//                descriptions = data?.articles?[indexPath.row].descriptions?.zhTW ?? []
//            }
//            else{
//                descriptions = data?.articles?[indexPath.row].descriptions?.en ?? []
//            }
//            var text = ""
//            for (index,i) in descriptions.enumerated() {
//                text += "\(i)"
//                if index != descriptions.count - 1{
//                    text += "\n\n"
//                }
//            }
//
//            print(2213,cell?.textview.frame.width)
//            return text.height(withConstrainedWidth: (cell?.frame.width ?? 0) - 102.calcvaluex(), font: UIFont(name: "Roboto-Light", size: 18.calcvaluey())!) + 300.calcvaluey()
//        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ce", for: indexPath) as! FrequentRightViewCell
        //cell.frame = .init(x: 0, y: 0, width: tableview.frame.width, height: 0)
        //cell.backgroundColor = .clear
        cell.label.text = data?.articles?[indexPath.row].titles?.getLang()
        cell.descriptions = data?.articles?[indexPath.row].descriptions?.getArray() ?? []
        if expanding[indexPath.row] {
            cell.addView = true
//            cell.layoutIfNeeded()
//            //print(22131,cell.textview.frame.width)
//            cell.ctanchor.height?.constant = cell.textview.text.height(withConstrainedWidth: cell.textview.frame.width, font: UIFont(name: "Roboto-Light", size: 18.calcvaluey())!) + 300.calcvaluey()
//            //cell.extraviewanchor.height?.constant = 114.calcvaluey()
//            cell.extraview.isHidden = false
           
        }
        else{
            cell.addView = false
//           // cell.ctanchor.height?.constant = 84.calcvaluey()
//            //cell.extraviewanchor.height?.constant = 0
//            cell.extraview.isHidden = true
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if !expanding[indexPath.row] {
            expanding[indexPath.row] = true
        }
        else{
            expanding[indexPath.row] = false
        }
        
        UIView.setAnimationsEnabled(false)
        
            tableView.beginUpdates()
            tableView.reloadData()
            tableView.endUpdates()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.setAnimationsEnabled(true)
        }
        
    }
    let imageview = UIImageView()
    let subtitle = UILabel()
    let tableview = UITableView()
    let starray = ["漏氣","零件鬆脫處理辦法","調壓閥","零件有聲音"]
    var expanding = [Bool]()
    func showWebview(){
        imageview.isHidden = true
        subtitle.isHidden = true
        tableview.isHidden = true
        webview.isHidden = false
    }
    func showArticle(){
        imageview.isHidden = false
        subtitle.isHidden = false
        tableview.isHidden = false
        webview.isHidden = true
    }
    var data : Hotspot?{
        didSet{
            
            if let remote = data?.remotes?.getLang() {
                showWebview()
                guard let url = URL(string: remote.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") else {return}
                url.loadWebview(webview: webview)
            }
            
            else if let file = data?.files?.getLang()?.first?.path_url {
                showWebview()
                
                switch file {
                case .string(let x) :
                    guard let url = URL(string: x?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") else {return}
                    url.loadWebview(webview: webview)
                default :
                    webview.load(URLRequest(url: URL(string: "about:blank")!))
                }
                

                
            }
            else if let articles = data?.articles {
                showArticle()
                let files = data?.image
                imageview.sd_setImage(with: URL(string: files?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
                subtitle.text = data?.descriptions?.getArray()?.first
                expanding = []
                
                for _ in articles {
                    expanding.append(false)
                }
                
                self.tableview.reloadData()
            }
            else{
                imageview.isHidden = true
                subtitle.isHidden = true
                tableview.isHidden = true
                webview.isHidden = true
            }

        }
    }
    
    let webview = WKWebView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(imageview)
        imageview.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 56.calcvaluey(), left: 184.calcvaluex(), bottom: 0, right: 0),size: .init(width: 114.calcvaluex(), height: 110.calcvaluey()))
        imageview.centerXInSuperview()
        imageview.isHidden = true
        addSubview(subtitle)
        subtitle.isHidden = true
        subtitle.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        //subtitle.text = "油壓零件"
        subtitle.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        subtitle.anchor(top: imageview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
        subtitle.centerXAnchor.constraint(equalTo: imageview.centerXAnchor).isActive = true
        
        addSubview(tableview)
        tableview.isHidden = true
        tableview.backgroundColor = .clear
        tableview.anchor(top: subtitle.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 43.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        
        tableview.separatorStyle = .none
        tableview.register(FrequentRightViewCell.self, forCellReuseIdentifier: "ce")
        tableview.showsVerticalScrollIndicator = false
        //tableview.contentInset.top = 4.calcvaluey()
        tableview.contentInset.bottom = 4.calcvaluey()
        //tableview.contentInset.bottom = 150.calcvaluey()
        
        addSubview(webview)
        webview.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 16.calcvaluex(), bottom: 16.calcvaluey(), right: 16.calcvaluex()))
        webview.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentRightViewCell:UITableViewCell {
    let ct = UIView()
    var ctanchor:AnchoredConstraints!
    let label = UILabel()
    let dropdownimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_down"))
    let extraview = UIView()
    var extraviewanchor:AnchoredConstraints!
    let textview = UITextView()
    var addView = false{
        didSet{
            if self.addView {
                textview.isHidden = false

//            textview.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
//                textview.backgroundColor = .clear
//            textview.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
//            extraview.addSubview(textview)
//                textview.fillSuperview(padding: .init(top: 27.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 46.calcvaluex()))
            }
            else{
                textview.isHidden = true
               // textview.removeFromSuperview()
            }
        }
    }
    var descriptions = [String]() {
        didSet{
            var text = ""
            for (index,i) in descriptions.enumerated() {
                text += "\(i)"
                if index != descriptions.count - 1{
                    text += "\n\n"
                }
            }
            textview.text = text
        }
    }
    let expandview = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = .clear
        ct.addshadowColor()
        ct.backgroundColor = .white
        ct.layer.cornerRadius = 15.calcvaluex()
        contentView.addSubview(ct)
        ct.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 28.calcvaluex(), bottom: 14.calcvaluey(), right: 28.calcvaluex()))
        ct.heightAnchor.constraint(greaterThanOrEqualToConstant: 84.calcvaluey()).isActive = true
        let vertical_stack = Vertical_Stackview()
        
        ct.addSubview(vertical_stack)
        vertical_stack.fillSuperview()
        
        let hstack = Horizontal_Stackview(spacing:8.calcvaluex(),alignment: .center)
        
        hstack.addArrangedSubview(label)
        hstack.addArrangedSubview(dropdownimage)
        let labelcontainerview = UIView()
        //labelcontainerview.backgroundColor = .red
        labelcontainerview.addSubview(hstack)
        hstack.fillSuperview(padding: .init(top: 28.calcvaluey(), left: 24.calcvaluex(), bottom: 28.calcvaluey(), right: 24.calcvaluex()))
        
        vertical_stack.addArrangedSubview(labelcontainerview)
        //ct.addSubview(label)
        label.text = "漏氣"
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
       // label.anchor(top: ct.topAnchor, leading: ct.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 28.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        
        dropdownimage.constrainHeight(constant: 36.calcvaluey())
        dropdownimage.constrainWidth(constant: 36.calcvaluex())
        //expandview.isHidden = true
       // expandview.addSubview(textview)
        textview.isScrollEnabled = false
        textview.isUserInteractionEnabled = false
        textview.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        textview.backgroundColor = #colorLiteral(red: 0.9842038751, green: 0.9843754172, blue: 0.9842056632, alpha: 1)
        textview.textColor = #colorLiteral(red: 0.6665527681, green: 0.6731523004, blue: 0.6731523004, alpha: 1)
        
       
        textview.textContainerInset = .init(top: 27.calcvaluey(), left: 46.calcvaluex(), bottom: 12.calcvaluey(), right: 46.calcvaluex())
        textview.setContentCompressionResistancePriority(.required, for: .vertical)
        textview.setContentHuggingPriority(.required, for: .vertical)
        
        textview.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            textview.clipsToBounds = true
            textview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            textview.roundCorners2([.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 15.calcvaluex())
        }
        textview.isHidden = true
        vertical_stack.addArrangedSubview(textview)
       // ct.addSubview(dropdownimage)
       // dropdownimage.anchor(top: ct.topAnchor, leading: nil, bottom: nil, trailing: ct.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 24.26.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
//        ct.addSubview(extraview)
//        extraview.layer.cornerRadius = 15.calcvaluex()
//        if #available(iOS 11.0, *) {
//            extraview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
//        extraview.isHidden = true
  //      extraview.backgroundColor = #colorLiteral(red: 0.9842038751, green: 0.9843754172, blue: 0.9842056632, alpha: 1)
//        extraviewanchor = extraview.anchor(top: label.bottomAnchor, leading: ct.leadingAnchor, bottom: ct.bottomAnchor, trailing: ct.trailingAnchor,padding: .init(top: 28.calcvaluey(), left: 0, bottom: 0, right: 0))
//
//        textview.isUserInteractionEnabled = false
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
