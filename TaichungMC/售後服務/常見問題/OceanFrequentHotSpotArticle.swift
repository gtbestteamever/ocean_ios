//
//  OceanFrequentHotSpotFile.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 12/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class OceanFrequentHotSpotArticleView : UIViewController{
    let container = SampleContainerView()
    let rightview = FrequentRightView()
    @objc func popView(){
        self.dismiss(animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.xbutton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
        view.addSubview(container)
        container.centerInSuperview(size: .init(width: UIDevice().userInterfaceIdiom == .pad ? 400.calcvaluex() : UIScreen.main.bounds.width - 20.calcvaluex(), height: 650.calcvaluey()))
        
        container.addSubview(rightview)
        rightview.anchor(top: container.label.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor)
    }

}
