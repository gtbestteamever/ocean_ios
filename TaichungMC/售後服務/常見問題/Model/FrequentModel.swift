//
//  FrequentModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 9/15/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct FrequentModel : Codable {
    var data : [FrequentProduct]
}

struct FrequentProduct : Codable {
    var id : String
    var title : String?
    var titles : Lang?
    var descriptions : Lang?
    var image : String?
    var content_file : Files?
    var content_type : Int?
    var assets : [FrequentAsset]?
    var children : [FrequentProduct]?
    var maps : [Map]?
}

struct FrequentAsset : Codable {
    var remote : String?
    var remotes : Lang?
    var files : Files?
    
    
    func getUrlString() -> String {
        
        if remotes != nil{
            
            return remotes?.getLang() ?? ""
        }
        
        if let m = files?.getLang() {
            print(661,m.first)
            if let ab = m.first {
                switch ab.path_url {
                case .string(let fr):
                   
                    return fr?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                case .arrayString(let sr):
                    return sr?.first??.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                default:
                    ()
                }
                
            }
        }
        
        return ""
        
        
    }
}


struct FrequentData : Codable {
    var data : [FrequentArray]
}

struct FrequentArray : Codable {
    var id : String
    var tags : [String]?
    var image_url : String?
    var title : String?
    var titles : Lang?
    var description : [String]?
    var descriptions : LangArray?
    var hotspots : [Hotspot]?
}
