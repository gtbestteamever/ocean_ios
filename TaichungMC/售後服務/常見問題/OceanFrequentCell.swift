//
//  OceanFrequentCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 12/20/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
protocol OceanFrequentDelegate {
    func showMain(index:Int)
    func showHotspot(hotspot: Hotspot)
}
class OceanFrequentCell : UITableViewCell {
    let bottomview = UIView()
    let imageview = UIImageView()
    let titleLabel = UILabel()
    let subLabel = UILabel()
    let vert_stackview = Vertical_Stackview(spacing:16.calcvaluey(),alignment: .top)
    var searchText : String?
    var hotspot : [Hotspot] = [] {
        didSet{
           
            if let txt = searchText,txt != "" && hotspot.count != 0{
            let seperator = UIView()
            seperator.constrainHeight(constant: 12.calcvaluey())
            vert_stackview.addArrangedSubview(seperator)
                for (index,i) in hotspot.enumerated() {
                    let v = getHotSpotview(text: i.descriptions?.getArray()?.first ?? "")
                v.tag = index
                v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showHotspot)) )
                vert_stackview.addArrangedSubview(v)
            }
            let seperator2 = UIView()
            seperator2.constrainHeight(constant: 12.calcvaluey())
            vert_stackview.addArrangedSubview(seperator2)
            }
            else{
                vert_stackview.safelyRemoveArrangedSubviews()
            }
        }
    }
    override func prepareForReuse() {
        vert_stackview.safelyRemoveArrangedSubviews()
    }
    func getHotSpotview(text:String) -> UIView {
        
        let view = UIView()
       
        let circleview = UIView()
        circleview.constrainWidth(constant: 10.calcvaluex())
        circleview.constrainHeight(constant: 10.calcvaluex())
        circleview.layer.cornerRadius = 10.calcvaluex()/2
        circleview.backgroundColor = MajorColor().oceanSubColor
        view.addSubview(circleview)
        circleview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        let ttLabel = UILabel()
        ttLabel.text = text
       
        ttLabel.numberOfLines = 0
        ttLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        view.addSubview(ttLabel)
        ttLabel.anchor(top: view.topAnchor, leading: circleview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))

        ttLabel.heightAnchor.constraint(greaterThanOrEqualTo: circleview.heightAnchor,constant: 8.calcvaluey()).isActive = true
        return view
    }
    var delegate : OceanFrequentDelegate?
    @objc func showHotspot(gesture:UITapGestureRecognizer) {
        guard let index = gesture.view?.tag else {return}
        delegate?.showHotspot(hotspot: hotspot[index])
    }
    @objc func showMain(){
        delegate?.showMain(index: self.tag)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        let container = UIView()
        let stackview = Horizontal_Stackview(spacing:24.calcvaluex(),alignment: .center)
        container.addSubview(stackview)
        container.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showMain)) )
        stackview.fillSuperview(padding: .init(top: 18.calcvaluey(), left: 36.calcvaluex(), bottom: 18.calcvaluey(), right: 12.calcvaluex()))
        stackview.addArrangedSubview(imageview)
       // stackview.addArrangedSubview(UIView())
        
        let vertstack = Vertical_Stackview(spacing:12.calcvaluey())
        vertstack.addArrangedSubview(titleLabel)
        titleLabel.numberOfLines = 0
        vertstack.addArrangedSubview(subLabel)
        subLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        subLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())

        
        stackview.addArrangedSubview(vertstack)
        imageview.contentMode = .scaleAspectFit
        
        imageview.constrainHeight(constant: 80.calcvaluex())
        imageview.constrainWidth(constant: 80.calcvaluex())
        
        container.backgroundColor = .white
        container.addshadowColor()
        container.layer.cornerRadius = 8.calcvaluey()
        
        contentView.addSubview(container)
        contentView.addSubview(bottomview)
        container.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: bottomview.topAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 2.calcvaluex(), bottom: 0, right: 2.calcvaluex()))
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 90.calcvaluey()).isActive = true
        
        
        bottomview.backgroundColor = .white
        bottomview.addshadowColor()
        bottomview.roundCorners2([.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 10.calcvaluex())
        bottomview.anchor(top: container.bottomAnchor, leading: container.leadingAnchor, bottom: contentView.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 12.calcvaluey(), right: 12.calcvaluex()))
        
        bottomview.addSubview(vert_stackview)
        vert_stackview.fillSuperview(padding: .init(top: 0, left: 28.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
