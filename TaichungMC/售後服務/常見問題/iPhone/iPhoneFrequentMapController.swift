//
//  iPhoneFrequentMapController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/20/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit

class iPhoneFrequentMapController : SampleController {
    let rightview = FrequentRightView()
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.isHidden = false
        settingButton.isHidden = true
        titleview.label.text = "常見問題".localized
                view.addSubview(rightview)
        
        rightview.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: view.bottomAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        
    }
    override func popview() {
        self.dismiss(animated: true)
    }
}
