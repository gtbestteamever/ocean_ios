class iPhoneFrequentQuestionDetailController: SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FrequentQuestionDelegate {

    func getHotspot(hotspot: Hotspot) {
        print(331,hotspot)
       // rightview.data = hotspot
        
        let vd = iPhoneFrequentMapController()
        
        vd.modalPresentationStyle = .fullScreen
        
        vd.rightview.data = hotspot
        
        self.present(vd, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ct", for: indexPath) as! SelectionViewCell
        cell.label.text = ["客戶常見問題".localized,"內部技術文件".localized][indexPath.item]
        if indexPath.item == selectedIndex{
            cell.vd.isHidden = false
            //cell.label.text = "客戶常見文題"
            cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
            //cell.label.text = "內部技術文件"
            cell.vd.isHidden = true
            cell.label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 120.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 55.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex != indexPath.item {
        selectedIndex = indexPath.item
        
        
        //self.selectionview.reloadData()
            
            if selectedIndex == 0{
                self.leftview.data = frequent_data
            }
            else{
                self.leftview.data = inner_data
            }
        }
    }
    let whitespace = UIView()
    let selectionview = NewTabBar()
    let leftview = FrequentLeftView()
    let rightview = FrequentRightView()
    var id : String = ""
    var frequent_data = [FrequentArray]()
    var inner_data = [FrequentArray]()
    var delegate : FrequentQuestionDelegate?
    var selectedIndex = 0
    override func popview() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func didSelect(sender:NewButton) {
        if selectIndex == sender.tag {
            return
        }
        selectIndex = sender.tag
        if selectIndex == 0{
            self.leftview.data = frequent_data
        }
        else{
            self.leftview.data = inner_data
        }
        for (index,i) in selectionview.stackview.arrangedSubviews.enumerated() {
            let vd = selectionview.stackview.arrangedSubviews[index] as? NewButton
            
            if index == selectIndex {
                vd?.setColor()
            }
            else{
                vd?.setUnColor()
            }
        }
    }
    var selectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.label.text = "常見問題".localized
        settingButton.isHidden = true
        backbutton.isHidden = false
        view.backgroundColor = .white
        

        if GetUser().isPortal() == 1 {
            selectionview.isHidden = true
        }
        selectionview.stackview.safelyRemoveArrangedSubviews()
        for (index,i) in [(#imageLiteral(resourceName: "ic_quotation_normal"),"客戶常見問題".localized),(#imageLiteral(resourceName: "ic_tab_features_normal"),"內部技術文件".localized)].enumerated() {
            let bt = NewButton(image: i.0, text: i.1)
            bt.tag = index
            bt.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
            if index == selectIndex{
                bt.setColor()
            }
            else{
                bt.setUnColor()
            }
            
            
            selectionview.stackview.addArrangedSubview(bt)
        }
        view.addSubview(selectionview)
//        selectionview.delegate = self
//        selectionview.dataSource = self
        selectionview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: productMenuTabHeight))
//        selectionview.backgroundColor = .clear
//        selectionview.register(SelectionViewCell.self, forCellWithReuseIdentifier: "ct")
        
        view.addSubview(whitespace)
        whitespace.backgroundColor = .clear
        whitespace.layer.cornerRadius = 15.calcvaluex()
        whitespace.addshadowColor()
        whitespace.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: selectionview.topAnchor, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 46.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        whitespace.addSubview(leftview)
        leftview.delegate = self
        leftview.anchor(top: whitespace.topAnchor, leading: whitespace.leadingAnchor, bottom: whitespace.bottomAnchor, trailing: whitespace.trailingAnchor)
        
//        whitespace.addSubview(rightview)
//
//        rightview.anchor(top: whitespace.topAnchor, leading: leftview.trailingAnchor, bottom: whitespace.bottomAnchor, trailing: whitespace.trailingAnchor)
        
//        view.bringSubviewToFront(whitespace)
//        view.bringSubviewToFront(selectionview)
        
        fetchApi()
    }
    
    func fetchApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/faq_independent/\(id)", decoderType: FrequentData.self) { json in
            DispatchQueue.main.async {
                if let json = json {
                    for i in json.data {
                        if let con = i.tags?.contains("faq"), let con2 = i.tags?.contains("internal-doc") {
                            if con {
                                self.frequent_data.append(i)
                            }
                            else if con2 {
                                self.inner_data.append(i)
                            }
                        }
                    }
                    
                    self.leftview.data = self.frequent_data
                }
            }

        }
    }

}
