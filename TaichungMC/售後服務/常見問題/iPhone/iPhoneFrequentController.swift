//
//  iPhoneFrequentController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/19/22.
//  Copyright © 2022 TaichungMC. All rights reserved.
//

import UIKit
//
//  FrequentQuestionController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class iPhoneFrequentQuestionController: SampleController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,OceanFrequentDelegate {
    func showMain(index: Int) {
        goShowData(data: self.fileter_data[index])
    }
    
    func showHotspot(hotspot: Hotspot) {
        goShowHotspot(hotspot: hotspot)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let txt = textField.text ?? ""
        if txt != ""{
        self.fileter_data = self.data.filter({ pr in
            let gt = pr.maps?.first?.hotspots ?? []
            let filter_hotspot = gt.filter { sr in
                return (sr.descriptions?.getArray()?.first?.contains(txt) ?? false) || (sr.keywords?.getLang()?.contains(txt) ?? false)
            }
            
            return (pr.titles?.getLang()?.contains(txt) ?? false) || (pr.descriptions?.getLang()?.contains(txt) ?? false) || filter_hotspot.count != 0
        })
            for (index,i) in self.fileter_data.enumerated() {
                let gt = i.maps?.first?.hotspots ?? []
                if gt.isEmpty {
                    continue
                }
                var filter_hotspot = gt.filter { pr in
                    return (pr.descriptions?.getArray()?.first?.contains(txt) ?? false) || (pr.keywords?.getLang()?.contains(txt) ?? false)
                }
                if GetUser().isPortal() == 1{
                    filter_hotspot = filter_hotspot.filter({ ss in
                        return ss.type == "faq"
                    })
                }
                
                self.fileter_data[index].maps?[0].hotspots = filter_hotspot
            }
        }
        else{
            self.fileter_data = self.data
        }
        self.collectionview.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fileter_data.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OceanFrequentCell
        cell.tag = indexPath.item
        cell.searchText = searchfield.text
        let dt = self.fileter_data[indexPath.item]
        print(661,dt.titles?.getLang())
        cell.titleLabel.text = dt.titles?.getLang()
        cell.subLabel.text = dt.descriptions?.getLang()
        cell.hotspot = dt.maps?.first?.hotspots ?? []
        cell.imageview.sd_setImage(with: URL(string: dt.image?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""), completed: nil)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 4.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func goShowHotspot(hotspot:Hotspot) {
        if let remote = hotspot.remotes?.getLang() {
            let vd = FrequentFileController()
            vd.titleview.label.text = hotspot.descriptions?.getArray()?.first
            if let url = URL(string: remote.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") {
                url.loadWebview(webview: vd.webview)
            }
            
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
        else if let file = hotspot.files?.getLang()?.first?.path_url {
            let vd = FrequentFileController()
            vd.titleview.label.text = hotspot.descriptions?.getArray()?.first
            switch file {
            case .string(let x) :
                if let url = URL(string: x?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") {
                    url.loadWebview(webview: vd.webview)
                }
            default:
                ()
            }
            
            
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
        else if let article = hotspot.articles, article.count > 0 {
            let vd = OceanFrequentHotSpotArticleView()
            vd.container.label.text = ""
            vd.rightview.data = hotspot
            vd.modalPresentationStyle = .overCurrentContext
            self.tabBarController?.present(vd, animated: false, completion: nil)
        }
    }
    func goShowData(data:FrequentProduct) {
        if data.content_type == 0{
            let vd = FrequentFileController()
            vd.titleview.label.text = data.titles?.getLang()
            if let mt = data.content_file?.getLang()?.first?.path_url {
                switch mt {
                case .string(let x):
                    if let url = URL(string: x?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") {
                        url.loadWebview(webview: vd.webview)
                    }
                default :
                    ()
                }
            }
            
            
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)

        }
        else{
            let vd = iPhoneFrequentQuestionDetailController()
            vd.leftview.title.header.text = data.titles?.getLang()
            vd.leftview.title.subtitle.text = data.descriptions?.getLang()
            
            vd.id = data.id
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
    }

    override func popview() {
       shrink = false
        maincontroller.showcircle()
            
        searchfieldanchor.trailing?.constant = -26.calcvaluex()
        searchfieldanchor.width?.constant = 665.calcvaluex()
        
        UIView.animate(withDuration: 0.4) {
            //self.collectionview.collectionViewLayout.invalidateLayout()
            self.view.layoutIfNeeded()
        }
            


    }
    let searchfield = SearchTextField()
    var searchfieldanchor:AnchoredConstraints!
    let bottomview = UIView()
    let collectionview = UITableView(frame: .zero, style: .grouped)
    weak var maincontroller:ViewController!
    var shrink = false
    
    var data = [FrequentProduct]()
    var fileter_data = [FrequentProduct]()
    let hud = JGProgressHUD()
    override func changeLang() {
        super.changeLang()
        titleview.label.text = "常見問題".localized
        searchfield.attributedPlaceholder = NSAttributedString(string: "搜尋問題".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        fetchApi()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       // background_view.image = #imageLiteral(resourceName: "background_afterservice3")
        settingButton.isHidden = false
        titleview.label.text = "常見問題".localized
        
        searchfield.attributedPlaceholder = NSAttributedString(string: "搜尋問題".localized, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchfield.backgroundColor = .white
        searchfield.addshadowColor(color: #colorLiteral(red: 0.8889377713, green: 0.8912348151, blue: 0.8966072798, alpha: 1))
        searchfield.delegate = self
        view.addSubview(searchfield)
        searchfield.anchor(top: topview.bottomAnchor, leading: horizontalStackview.leadingAnchor, bottom: nil, trailing: horizontalStackview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        searchfield.layer.cornerRadius = 46.calcvaluey()/2
        
        bottomview.backgroundColor = .clear
        view.addSubview(bottomview)
        bottomview.anchor(top: searchfield.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        
 
        view.addSubview(collectionview)
        collectionview.backgroundColor = .clear
        collectionview.anchor(top: bottomview.topAnchor, leading: searchfield.leadingAnchor, bottom: view.bottomAnchor, trailing: searchfield.trailingAnchor,padding: .init(top: 0.calcvaluey(), left: 0, bottom: 0, right: 0))
        collectionview.contentInset.bottom = menu_bottomInset + 20.calcvaluey()
        collectionview.showsVerticalScrollIndicator = false
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.separatorStyle = .none
        collectionview.register(OceanFrequentCell.self, forCellReuseIdentifier: "cell")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    func fetchApi() {
        self.hud.show(in: self.view)
        NetworkCall.shared.getCall(parameter: "api-or/v1/faq_independent", decoderType: FrequentModel.self) { json in
            DispatchQueue.main.async {
                self.hud.dismiss()
                if let json = json?.data {
                    self.data = json.filter({ sr in
                        return sr.titles?.getLang() != nil
                    })
                    
                    self.textFieldDidEndEditing(self.searchfield)
                }
            }

        }
    }
}


